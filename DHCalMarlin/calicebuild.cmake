#from /afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ILCSOFT/v01-13-01/ILCSoft.cmake

SET( ILC_HOME "/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ILCSOFT/v01-16" CACHE PATH "Path to ILC Software" FORCE)
MARK_AS_ADVANCED( ILC_HOME )

SET( CMAKE_PREFIX_PATH 
	/afs/cern.ch/sw/lcg/external/mysql/5.1.45/x86_64-slc5-gcc43-opt;
	/afs/cern.ch/sw/lcg/external/Java/JDK/1.6.0/amd64;
	/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/CERNLIBS/2006-gfortran;
	/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ILCSOFT/v01-14/geant4/9.5.p01;
	${ILC_HOME}/ilcutil/v01-00;
	${ILC_HOME}/lcio/v02-03-01;
	${ILC_HOME}/gear/v01-02-02;
	${ILC_HOME}/KalTest/v01-05-01;
	${ILC_HOME}/KalDet/v01-11;
	${ILC_HOME}/CondDBMySQL/CondDBMySQL_ILC-0-9-5;
	${ILC_HOME}/lccd/v01-02;
	${ILC_HOME}/RAIDA/v01-06-02;
	${ILC_HOME}/CED/v01-07;
	${ILC_HOME}/Marlin/v01-04;
	${ILC_HOME}/MarlinUtil/v01-05-03;
	${ILC_HOME}/Mokka/mokka-08-00-03;
	${ILC_HOME}/MarlinReco/v01-05;
	${ILC_HOME}/PandoraPFANew/v00-09;
	${ILC_HOME}/PandoraAnalysis/v00-04;
	${ILC_HOME}/MarlinPandora/v00-09-02;
	${ILC_HOME}/LCFIVertex/v00-06-01;
	${ILC_HOME}/CEDViewer/v01-06-01;
	${ILC_HOME}/Overlay/v00-13;
	${ILC_HOME}/FastJet/2.4.2;
	${ILC_HOME}/FastJetClustering/v00-02;
	${ILC_HOME}/MarlinFastJet/v00-01;
	${ILC_HOME}/LCTuple/v01-01;
	${ILC_HOME}/MarlinKinfit/v00-01-02;
	${ILC_HOME}/MarlinTrk/v01-10-01;
	${ILC_HOME}/KiTrack/v01-04;
	${ILC_HOME}/KiTrackMarlin/v01-04;
	${ILC_HOME}/MarlinTrkProcessors/v01-09;
	${ILC_HOME}/Clupatra/v00-09-01;
	${ILC_HOME}/LCFIPlus/v00-05-02;
	${ILC_HOME}/ForwardTracking/v01-07;
	${ILC_HOME}/Eutelescope/v00-08-00-rc1;
	${ILC_HOME}/pathfinder/v00-02;
	${ILC_HOME}/MarlinTPC/v00-10;
	${ILC_HOME}/bbq/v00-01-02;
	${ILC_HOME}/Garlic/v2.10.1;
	/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/ROOT/v5-28-00f;
	/afs/cern.ch/sw/lcg/external/clhep/2.1.1.0/x86_64-slc5-gcc41-opt;
	/afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/GSL/1.14;
	/afs/cern.ch/sw/lcg/external/qt/4.7.4/x86_64-slc5-gcc43-opt;
CACHE PATH "CMAKE_PREFIX_PATH" FORCE )

#----------------------------------------------------------------------------------------
SET( ILCUTIL_DIR "${ILC_HOME}/ilcutil/v01-00" CACHE PATH "Path to ILCUTIL" FORCE)
MARK_AS_ADVANCED( ILCUTIL_DIR )

# set CMAKE_PREFIX_PATH to find ILCSOFT_CMAKE_MODULES ILCTEST and streamlog
# by setting this variable there is no need to set the correspondent PKG_DIR variables
LIST( APPEND CMAKE_PREFIX_PATH "${ILCUTIL_DIR}" CACHE PATH "CMAKE_PREFIX_PATH" FORCE)

# set CMAKE_MODULE_PATH for backwards compatibility directly to ILCUTIL_DIR/cmakemodules
SET( CMAKE_MODULE_PATH "${ILCUTIL_DIR}/cmakemodules" CACHE PATH "Path to CMakeModules" FORCE)



######################################################################


MESSAGE(STATUS "")
MESSAGE(STATUS "CMAKE_MODULE_PATH=${CMAKE_MODULE_PATH}")
#MESSAGE(STATUS "CALICE_USERLIB_HOME=${CALICE_USERLIB_HOME}")


######################################################################
#SET( CMAKE_INSTALL_PREFIX "xxx"
#     CACHE PATH "Path to calice instalation" FORCE )
#MARK_AS_ADVANCED( CMAKE_INSTALL_PATH )
