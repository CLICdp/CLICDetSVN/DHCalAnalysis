cd build
cmake -C ../calicebuild.cmake ..


###################
#  CALICE_TORSO   #
###################

This is a torso package to host private code within the CALICE software 
environment. Mainly, this will be a bunch of MARLIN processors, which can 
be compiled into a shared object library and passed to MARLIN during run-time.



  To build the library
=========================
 - modify calicebuild.cmake and set the CMAKE_INSTALL_PREFIX to the path where you want your software to be installed

 - create a build directory
$> mkdir build

 - update the paths in calicebuild.cmake. Note that relative paths have to be 
   relative to the newly created build-directory.
 - have a look at the BUILD_FLAG definitions in top-most
   CMakeLists.txt and make sure you link against all 
   packages required by your code. Either use ON/OFF in the
   CMakeLists.txt or
     cmake -DBUILD_WITH_USE_<package>=1/0 
   or use ccmake after calling cmake and before issuing gmake...

 - run CMake in the build directory
$> cd build
$> cmake -C ../calicebuild.cmake ../

 - Now you are ready to compile/install your code
$> gmake 
$> gmake install

 - Sometimes you might be interested in the full compiler command, for this use
$> gmake VERBOSE=1



  Include new code into the library
=====================================

 - All CMake scripts assume that header files are in the include/ subdirectory, 
   while code files reside in the src/ 

 - The scripts are set up such that you have to register new code to be 
   included into the library. The advantage 
   is that you can have development code that doesn't compile yet in your 
   src/ directory. Look for the code snippet 

  SET(  LIBRARY_SOURCES HelloWorldProcessor.cc
                        )

   in the src/CMakeLists.txt file and add any new routine to be included.



