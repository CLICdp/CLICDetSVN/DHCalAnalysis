/*
 * NNClusteringProcessor.cc
 *
 *  Created on: Feb 21, 2013
 *      Author: Christian Grefe, CERN
 */

#include "NNClusteringProcessor.hh"
#include "NNClustering.hh"
#include "DhcalMapping.hh"
#include "DhcalUtil.hh"

// lcio
#include "EVENT/Cluster.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/LCTOOLS.h"
#include "UTIL/BitSet32.h"

// marlin
#include "marlin/VerbosityLevels.h"

// logging
#include "streamlog/streamlog.h"

// c++
#include <map>
#include <iostream>
#include <set>

using EVENT::LCEvent;
using EVENT::CalorimeterHit;
using EVENT::Cluster;
using IMPL::ClusterImpl;
using IMPL::LCCollectionVec;

using std::map;
using std::set;
using std::string;
using std::vector;

namespace CALICE {

NNClusteringProcessor aNNClusteringProcessor;

NNClusteringProcessor::NNClusteringProcessor() :
		Processor("NNClusteringProcessor") {
	_description =
			"NNClusteringProcessor: Runs a nearest neighbour clustering algorithm on a calorimeter hit collection";

	registerInputCollections(LCIO::CALORIMETERHIT, "InputCollections", "Name of input collections",
			_inputCollectionNames, _inputCollectionNames);

	registerOutputCollection(LCIO::CLUSTER, "OutputCollection", "Name of output collection", _outputCollectionName,
			string("NNClusters"));

	registerProcessorParameter("IdentifierU", "Name of the id identifier for U", _uIdentifier, DhcalMapping::instance()->U_ID());
	registerProcessorParameter("IdentifierV", "Name of the id identifier for V", _vIdentifier, DhcalMapping::instance()->V_ID());
	registerProcessorParameter("IdentifierLayer", "Name of the id identifier for layer", _layerIdentifier, DhcalMapping::instance()->LAYER_ID());
	registerProcessorParameter("MaxDistanceUV", "Cut for distance between hits in U in number of cells", _uvLimit, 1);
	registerProcessorParameter("MaxDistanceLayer", "Cut for distance between hits in layer in number of cells", _layerLimit, 1);
	registerProcessorParameter("EnergyCut", "Cut on hit energy in GeV", _energyCut, (float) 0.0);

	_hitMap = 0;
	_idDecoder = 0;
}

NNClusteringProcessor::~NNClusteringProcessor() {
	if (_idDecoder) {
		delete _idDecoder;
	}
	if (_hitMap) {
		delete _hitMap;
	}
}

void NNClusteringProcessor::init() {
	printParameters();
	_hitMap = new LayerColumnRowMap<CalorimeterHit*>(DhcalMapping::instance()->CELL_ID_ENCODING(), _uIdentifier, _vIdentifier, _layerIdentifier);
	_idDecoder = new CellIDDecoder<CalorimeterHit>(DhcalMapping::instance()->CELL_ID_ENCODING());
}

void NNClusteringProcessor::processEvent(LCEvent* event) {
	vector<CalorimeterHit*> hits;
	vector<string>::const_iterator itCollectionName = _inputCollectionNames.begin();
	for (; itCollectionName != _inputCollectionNames.end(); ++itCollectionName) {
		LCCollection* collection = event->getCollection(*itCollectionName);
		for (int iHit = 0; iHit < collection->getNumberOfElements(); iHit++) {
			CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(collection->getElementAt(iHit));
			if (hit->getEnergy() >= _energyCut) {
				hits.push_back(hit);
			}
		}
	}

	_usedHits.clear();
	_hitMap->clear();
	_hitMap->fill(hits);

	LCCollectionVec* clusterCollection = new LCCollectionVec(EVENT::LCIO::CLUSTER);
	clusterCollection->setFlag(UTIL::set_bit(clusterCollection->getFlag(), EVENT::LCIO::CLBIT_HITS));

	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		if (_usedHits.find(hit) == _usedHits.end()) {
			ClusterImpl* cluster = new ClusterImpl();
			this->addHitWithNeighboursToCluster(hit, cluster);
			DhcalUtil::calculateClusterProperties(cluster);
			clusterCollection->addElement(cluster);
		}
		++itHit;
	}

	streamlog_out( DEBUG4 ) << "Found " << clusterCollection->getNumberOfElements() << " clusters" << std::endl;

	clusterCollection->parameters().setValue("MaxDistance_" + _uIdentifier + _vIdentifier, _uvLimit);
	clusterCollection->parameters().setValue("MaxDistance_" + _layerIdentifier, _layerLimit);
	// store the cell ID encoding to allow decoding of hit cell IDs without calorimeter hit collection
	clusterCollection->parameters().setValue(LCIO::CellIDEncoding, DhcalMapping::instance()->CELL_ID_ENCODING());

	event->addCollection(clusterCollection, _outputCollectionName);
}

void NNClusteringProcessor::end() {

}

void NNClusteringProcessor::addHitWithNeighboursToCluster(CalorimeterHit* hit, ClusterImpl* cluster) {
	cluster->addHit(hit, hit->getEnergy());
	_usedHits.insert(hit);
	std::vector<CalorimeterHit*> neighbours = _hitMap->getNeighbours(hit, _layerLimit, _uvLimit, _uvLimit);
	std::vector<CalorimeterHit*>::const_iterator itNeighbour = neighbours.begin();
	while (itNeighbour != neighbours.end()) {
		CalorimeterHit* neighbour = *itNeighbour;
		if (DhcalUtil::consistentInUV(hit, neighbour, _uvLimit, *_idDecoder, _uIdentifier, _vIdentifier)
				and _usedHits.find(neighbour) == _usedHits.end()) {
			this->addHitWithNeighboursToCluster(neighbour, cluster);
		}
		++itNeighbour;
	}
}

} /* namespace CALICE */
