//Wire Chamber Tracker Processor
//Used http://www.desy.de/~blobel/eBuch.pdf page 162

#include <WireChamberPlotter.hh>
#include <WireChamberUtil.hh>

//c++
#include <cmath>
#include <fstream>
#include <sstream>

//root
#include <TFile.h>
#include <TStyle.h>
#include <TROOT.h>
#include <TVector3.h>
#include <TMath.h>
#include <TF1.h>
#include <TH1F.h>

//lcio
#include "IMPL/LCCollectionVec.h"
#include "EVENT/LCCollection.h"
#include <UTIL/CellIDDecoder.h>
#include <IMPL/TrackImpl.h>

//misc
#include <ExtendedTrack.hh>

using namespace marlin;
using namespace lcio;
using namespace std;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::TrackerHit;


namespace CALICE{


WireChamberPlotter aWireChamberPlotter;

WireChamberPlotter::WireChamberPlotter() : Processor("WireChamberPlotter"){
	_description = "Draws Plots Using Wire Chamber Tracks";

	registerProcessorParameter("ZLocation", "Location in Z where we will find the XY Distribution", _zLocation, (float) 0.0);
	registerProcessorParameter("XBins", "Bin Number for X Axis", _xBins, 100);
	registerProcessorParameter("YBins", "Bin Number for Y Axis", _yBins, 100);
	registerProcessorParameter("XMin", "Minimum X value", _xMin, (float) -100);
	registerProcessorParameter("YMin", "Minimum Y value", _yMin, (float) -100);
	registerProcessorParameter("XMax", "Maximum X value", _xMax, (float) 100);
	registerProcessorParameter("YMax", "Maximum Y value", _yMax, (float) 100);

	registerProcessorParameter("XZbins", "Bin number for XZ slope plot", _XZ_bins,  100);
	registerProcessorParameter("YZbins", "Bin number for YZ slope plot", _YZ_bins,  100);
	registerProcessorParameter("XZMax", "Maximum XZ slope", _XZ_Max, (float) 0.01);
	registerProcessorParameter("XZMin", "Minimum XZ slope", _XZ_Min, (float) -0.01);
	registerProcessorParameter("YZMax", "Maximum YZ slope", _YZ_Max, (float) 0.01);
	registerProcessorParameter("YZMin", "Minimum YZ slope", _YZ_Min, (float) -0.01);
	
	registerProcessorParameter("Title", "Graph Titles are Run number if =1, Blank if =0, Description if other", _Title, 1);

	//gROOT->ProcessLine(".x ${HOME}/CLICStyle.C");

}

WireChamberPlotter::~WireChamberPlotter(){
	//Used because Marlin creates two instances of the processors, one for initiating others, and one for the real dirty work
	delete XY_Distribution;
	delete XZSlope;
	delete YZSlope;
	delete X0res;
	delete X1res;
	delete X2res;
	delete Y0res;
	delete Y1res;
	delete Y2res;
}

void WireChamberPlotter::init(){
	//gROOT->ProcessLine(".x ${HOME}/CLICStyle.C");

	XY_Distribution = new TH2F("XY_Distribution", "XY Distribution of incoming particles at Z = 0", _xBins, _xMin, _xMax, _yBins, _yMin, _yMax);
	XY_Distribution->Sumw2();


	XZSlope = new TH1F("XZ_Slope", "XZ Slope of the fit",_XZ_bins, _XZ_Min, _XZ_Max);
	YZSlope = new TH1F("YZ_Slope", "YZ Slope of the fit",_YZ_bins, _YZ_Min, _YZ_Max);
	XZSlope->Sumw2();
	YZSlope->Sumw2();


	_ResFitBounds =10;
	int ResMin = -_ResFitBounds;
	int ResMax = _ResFitBounds;


	// Chambers are read in the opposite order

	X0res = new TH1F("X0res", "X Resolution of the third wire chamber", 100, ResMin, ResMax);
	X1res = new TH1F("X1res", "X Resolution of the second wire chamber", 100, ResMin, ResMax);
	X2res = new TH1F("X2res", "X Resolution of the first wire chamber", 100, ResMin, ResMax);

	Y0res = new TH1F("Y0res", "Y Resolution of the third wire chamber", 100, ResMin, ResMax);
	Y1res = new TH1F("Y1res", "Y Resolution of the second wire chamber", 100, ResMin, ResMax);
	Y2res = new TH1F("Y2res", "Y Resolution of the first wire chamber", 100, ResMin, ResMax);

	X0res->Sumw2();
	X1res->Sumw2();
	X2res->Sumw2();

	Y0res->Sumw2();
	Y1res->Sumw2();
	Y2res->Sumw2();
}

void WireChamberPlotter::processRunHeader(LCRunHeader* run){
	WCRunNumber = dynamic_cast<stringstream *> (&(stringstream() << run->getRunNumber()))->str();
}

void WireChamberPlotter::processEvent(LCEvent* event) {

	string collectionName = "WireChamberTracks";
	LCCollection* WireChamberTracks;
	try {
		WireChamberTracks = event->getCollection(collectionName);
		if(not WireChamberTracks){
			return; //No Wire Good Wire Chamber Tracks
		}
	} catch(lcio::DataNotAvailableException e ) {
		streamlog_out(MESSAGE) << collectionName << " collection not available" << endl;
		return;
	}


	IMPL::ExtendedTrack* WireChamberTrack = dynamic_cast<IMPL::ExtendedTrack*>(WireChamberTracks->getElementAt(0));
	std::vector<TrackerHit*> HitsUsed = WireChamberTrack->getHitsUsed();

	
	vector<float> firstres = WireChamberUtil::ResolutionCalculator(HitsUsed[0], HitsUsed[1], HitsUsed[2]);
	X0res->Fill(firstres[0]);
	Y0res->Fill(firstres[1]);
	vector<float> secondres = WireChamberUtil::ResolutionCalculator(HitsUsed[1], HitsUsed[0], HitsUsed[2]);
	X1res->Fill(secondres[0]);
	Y1res->Fill(secondres[1]);
	vector<float> thirdres = WireChamberUtil::ResolutionCalculator(HitsUsed[2], HitsUsed[0], HitsUsed[1]);
	X2res->Fill(thirdres[0]);
	Y2res->Fill(thirdres[1]);
	const float *P_0 = WireChamberTrack->getReferencePoint();


	XY_Distribution->Fill(P_0[0], P_0[1]);


	XZSlope->Fill(atan(WireChamberTrack->getXZslope()));
	YZSlope->Fill(atan(WireChamberTrack->getYZslope()));
}


void WireChamberPlotter::end(){

	string ZLoc = dynamic_cast<stringstream *> (&(stringstream() << _zLocation))->str();
	string WCFileName = "WCTrackZ_" + ZLoc + "_" + WCRunNumber + ".root";  //What about files with multiple runs?

	TFile XY_Dis(WCFileName.c_str(), "RECREATE");


	string Title = "Run: " + WCRunNumber;

	if(_Title == 1){
		XY_Distribution->SetTitle(Title.c_str());
		XZSlope->SetTitle(Title.c_str());
		YZSlope->SetTitle(Title.c_str());
		X0res->SetTitle(Title.c_str());
		X1res->SetTitle(Title.c_str());
		X2res->SetTitle(Title.c_str());
		Y0res->SetTitle(Title.c_str());
		Y1res->SetTitle(Title.c_str());
		Y2res->SetTitle(Title.c_str());
	}else if(_Title == 0){
		XY_Distribution->SetTitle("");
		XZSlope->SetTitle("");
		YZSlope->SetTitle("");
		X0res->SetTitle("");
		X1res->SetTitle("");
		X2res->SetTitle("");
		Y0res->SetTitle("");
		Y1res->SetTitle("");
		Y2res->SetTitle("");
	}
	
	XY_Distribution->GetXaxis()->SetTitle("X Position (mm)");
	XY_Distribution->GetYaxis()->SetTitle("Y Position (mm)");


	XY_Distribution->Write();

	XZSlope->GetXaxis()->SetTitle("XZ Slope (rads)");
	YZSlope->GetXaxis()->SetTitle("YZ Slope (rads)");

	XZSlope->GetYaxis()->SetTitle("Normalized Events (%)");
	YZSlope->GetYaxis()->SetTitle("Normalized Events (%)");


	TF1* f1 = new TF1("f1", "gaus", 0, 1);
	f1->SetParNames("Amplitude", "Mean", "Width");

	TF1* xslope = new TF1("xslope", "gaus", XZSlope->GetMean() - 2*XZSlope->GetRMS(), XZSlope->GetMean() + 2*XZSlope->GetRMS());
	TF1* yslope = new TF1("yslope", "gaus", XZSlope->GetMean() - 2*XZSlope->GetRMS(), XZSlope->GetMean() + 2*XZSlope->GetRMS());
	xslope->SetParNames("Amplitude", "Mean", "Width");
	yslope->SetParNames("Amplitude", "Mean", "Width");


	XZSlope->Scale(1/XZSlope->GetEntries());
	YZSlope->Scale(1/YZSlope->GetEntries());
	XZSlope->Fit("f1", "", "", XZSlope->GetMean() - 2*XZSlope->GetRMS(), XZSlope->GetMean() + 2*XZSlope->GetRMS());
	YZSlope->Fit("f1", "", "", YZSlope->GetMean() - 2*YZSlope->GetRMS(), YZSlope->GetMean() + 2*YZSlope->GetRMS());

	XZSlope->Write();
	YZSlope->Write();
	
	X0res->Scale(1/X0res->GetEntries());
	X1res->Scale(1/X1res->GetEntries());
	X2res->Scale(1/X2res->GetEntries());
	Y0res->Scale(1/Y0res->GetEntries());
	Y1res->Scale(1/Y1res->GetEntries());
	Y2res->Scale(1/Y2res->GetEntries());

	// Reverse Order, see above

	X0res->GetXaxis()->SetTitle("Third Chamber X Resolution (mm)");
	X1res->GetXaxis()->SetTitle("Second Chamber X Resolution (mm)");
	X2res->GetXaxis()->SetTitle("First Chamber X Resolution (mm)");
	Y0res->GetXaxis()->SetTitle("Third Chamber Y Resolution (mm)");
	Y1res->GetXaxis()->SetTitle("Second Chamber Y Resolution (mm)");
	Y2res->GetXaxis()->SetTitle("First Chamber Y Resolution (mm)");

	X0res->GetYaxis()->SetTitle("Fraction of Events");
	X1res->GetYaxis()->SetTitle("Fraction of Events");
	X2res->GetYaxis()->SetTitle("Fraction of Events");
	Y0res->GetYaxis()->SetTitle("Fraction of Events");
	Y1res->GetYaxis()->SetTitle("Fraction of Events");
	Y2res->GetYaxis()->SetTitle("Fraction of Events");
	
	TF1* X0resfit = new TF1("X0resfit", "gaus", X0res->GetMean() - 2*X0res->GetRMS(), X0res->GetMean() + 2*X0res->GetRMS());
	X0resfit->SetParNames("Amplitude", "Mean", "Width");
	X0res->Fit("X0resfit", "R");
	X1res->Fit("f1", "","", X1res->GetMean() - 2*X1res->GetRMS(), X1res->GetMean() + 2*X1res->GetRMS());
	X2res->Fit("f1", "","", X2res->GetMean() - 2*X2res->GetRMS(), X2res->GetMean() + 2*X2res->GetRMS());
	Y0res->Fit("f1", "","", Y0res->GetMean() - 2*Y0res->GetRMS(), Y0res->GetMean() + 2*Y0res->GetRMS());
	Y1res->Fit("f1", "","", Y1res->GetMean() - 2*Y1res->GetRMS(), Y1res->GetMean() + 2*Y1res->GetRMS());
	Y2res->Fit("f1", "","", Y2res->GetMean() - 2*Y2res->GetRMS(), Y2res->GetMean() + 2*Y2res->GetRMS());
	
	X0res->Write();
	X1res->Write();
	X2res->Write();
	Y0res->Write();
	Y1res->Write();
	Y2res->Write();


	delete X0res;
	delete X1res;
	delete X2res;
	delete Y0res;
	delete Y1res;
	delete Y2res;

	delete XZSlope;
	delete YZSlope;

	delete XY_Distribution;

	delete xslope;
	delete yslope;

	cout << "Wrote XY Histogram to: " << XY_Dis.GetName() << endl;
	XY_Dis.Close();
}


}/*Namespace CALICE*/


