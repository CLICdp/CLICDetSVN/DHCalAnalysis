/* Muon Finder
 *
 * Aug 18 2013
 *
 * William Nash
 */

#include <MuonFinder.hh>
#include "DhcalUtil.hh"

//lcio
#include "IMPL/ClusterImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/ReconstructedParticleImpl.h"

using namespace std;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::Cluster;
using IMPL::ClusterImpl;
using IMPL::LCCollectionVec;

namespace CALICE {

MuonFinder aMuonFinder;

MuonFinder::MuonFinder() : Processor("MuonFinder"){

	  registerProcessorParameter("LayersConsidered", "Amount of Layers that are considered to qualify a Muon Event", _layersConsidered, 20);
	  registerProcessorParameter("ChiLimit", "Maximum Chi-Squared allowed for fit to be accepted", _chiLimit, (float) 5.0);
	  registerProcessorParameter("ClusterSize", "The maximum allowed size of a cluster in number of pads", _clusterSize, 3);
	  registerProcessorParameter("ConsecutiveSkipLimit", "The maximum allowed number of layers skipped consecutively", _consecutiveSkipLimit, 3);
	  registerProcessorParameter("SkipLimit", "Total amount of layers allowed to be skipped", _skipLimit, 8);
	  registerProcessorParameter("AverageClusters", "Average number of Clusters cut", _averageClusters, (float) 1.2);
	  registerInputCollection(LCIO::CLUSTER, "InputClusterCollection", "Input Cluster Collection used to check for Muons", _inputClusterCollection,string("LayerClusters"));
	  registerOutputCollection(LCIO::RECONSTRUCTEDPARTICLE,"MuonCollection", "Muon Collection used for Muons", _MuonCollection,string("Muons"));

}

MuonFinder::~MuonFinder(){

}

void MuonFinder::init(){
	_muonCount=0;
	_eventCount=0;
}

void MuonFinder::processEvent(LCEvent* event){
	_eventCount++;

	if(_layersConsidered < 1 or _layersConsidered > 54){
		streamlog_out(WARNING) << "Layers Considered is invalid" << endl;
		return;
	}
	LCCollection* inputClusterCollection;
	try{
		inputClusterCollection = event->getCollection(_inputClusterCollection);
		if(!inputClusterCollection){
			return;
		}
	}catch(lcio::DataNotAvailableException e){
		return;
	}
	map<int,vector<Cluster*> > LayerClusterMap = DhcalUtil::buildLayerClusterMap(inputClusterCollection);
	float TotalClusters=0;
	map<int,vector<Cluster*> >::iterator itLayerMap = LayerClusterMap.begin();
	while(itLayerMap != LayerClusterMap.end()){
		TotalClusters+=(*itLayerMap).second.size();
		++itLayerMap;
	}
	float AverageClusters = TotalClusters/LayerClusterMap.size();
	if(AverageClusters > _averageClusters){
		return;
	}


	int TotalSkipCount = 0;
	int ConsecutiveSkipCount = 0;
	vector<Cluster*> MuonClusters (0);
	for(int i =0; i< _layersConsidered; i++){
		if(LayerClusterMap[i].size() == 1){
			if(LayerClusterMap[i][0]->getCalorimeterHits().size() >abs(_clusterSize)){
				TotalSkipCount++;
				ConsecutiveSkipCount++;
				if(TotalSkipCount > _skipLimit or ConsecutiveSkipCount > _consecutiveSkipLimit){
					streamlog_out(DEBUG4) << "Too many skipped layers" << endl;
					return;
				}
			}else{
				ConsecutiveSkipCount = 0;
				MuonClusters.push_back(LayerClusterMap[i][0]);
			}
		}else{
			TotalSkipCount++;
			ConsecutiveSkipCount++;
			if(TotalSkipCount > _skipLimit or  ConsecutiveSkipCount > _consecutiveSkipLimit){
				streamlog_out(DEBUG4) << "Too many skipped layers" << endl;
				return;
			}
		}
	}

	EVENT::Track* DHCALMuonTrack = dynamic_cast<EVENT::Track*>(DhcalUtil::straightLineTrackFit(MuonClusters));

	if(DHCALMuonTrack->getChi2()/DHCALMuonTrack->getNdf() > _chiLimit){
		streamlog_out(DEBUG4) << "Chi-squared not met" << endl;
		return;
	}


	LCCollectionVec* muonCollection = new LCCollectionVec(LCIO::RECONSTRUCTEDPARTICLE);
	ReconstructedParticleImpl* muon = new ReconstructedParticleImpl();
	muon->setType(13); //From PDG
	vector<Cluster*>::iterator itMuonPart = MuonClusters.begin();
	while(itMuonPart != MuonClusters.end()){
		muon->addCluster(*itMuonPart);
		++itMuonPart;
	}
	muon->addTrack(DHCALMuonTrack);
	muonCollection->addElement(muon);

	streamlog_out( DEBUG4 ) << "Found a muon!" << endl;
	_muonCount++;
	event->addCollection(muonCollection, _MuonCollection);

}

void MuonFinder::end(){
	cout << 100.0*_muonCount/_eventCount <<"% of events found were muons" << endl;

}

} /*Namespace CALICE*/
