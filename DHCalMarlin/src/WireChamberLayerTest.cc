//Wire Chamber Tracker Processor

#include <WireChamberLayerTest.hh>
#include <WireChamberUtil.hh>
#include "DhcalUtil.hh"

//c++
#include <cmath>
#include <fstream>

//lcio
#include "IMPL/LCCollectionVec.h"
#include "EVENT/LCCollection.h"
#include <UTIL/CellIDDecoder.h>
#include <IMPL/TrackImpl.h>

//root
#include <TFile.h>
#include <TMath.h>
#include <TF1.h>

using namespace marlin;
using namespace lcio;
using namespace std;

using EVENT::LCCollection;
using EVENT::LCParameters;


namespace CALICE{

WireChamberLayerTester aWireChamberLayerTester;

WireChamberLayerTester::WireChamberLayerTester() : Processor("WireChamberLayerTester"){
	_description = "Compare Wire Chamber XY Distributions";
	registerProcessorParameter("FitBounds", "Bounds for the fit to be in", _FitBounds, 5);
	registerProcessorParameter("Title", "Graph Titles are Run number if =1, Blank if =0, Description if other", _Title, 1);
}

WireChamberLayerTester::~WireChamberLayerTester(){
	delete Layer0X;
	delete Layer0Y;
	delete Layer1X;
	delete Layer1Y;
	delete Layer2X;
	delete Layer2Y;
	delete X0vsX1;
	delete X0vsX2;
	delete X1vsX2;
	delete Y0vsY1;
	delete Y0vsY2;
	delete Y1vsY2;
	delete X0res;
	delete X1res;
	delete X2res;
	delete Y0res;
	delete Y1res;
	delete Y2res;
}

void WireChamberLayerTester::init(){

	int Max = 60;
	int Min = -60;


	//Layers are read in the reverse order

	Layer0X = new TH1F("Layer0X","Wire Chamber X Hit Distribution in Third Layer", 100, Min, Max);
	Layer0Y = new TH1F("Layer0Y","Wire Chamber Y Hit Distribution in Third Layer", 100, Min, Max);

	Layer1X = new TH1F("Layer1X","Wire Chamber X Hit Distribution in Second Layer", 100, Min, Max);
	Layer1Y = new TH1F("Layer1Y","Wire Chamber Y Hit Distribution in Second Layer", 100, Min, Max);

	Layer2X = new TH1F("Layer2X","Wire Chamber X Hit Distribution in First Layer", 100, Min, Max);
	Layer2Y = new TH1F("Layer2Y","Wire Chamber Y Hit Distribution in First Layer", 100, Min, Max);


	vector<TH1F*> Layer0 (2);
	vector<TH1F*> Layer1 (2);
	vector<TH1F*> Layer2 (2);

	Layer0[0] = Layer0X;
	Layer0[1] = Layer0Y;

	Layer1[0] = Layer1X;
	Layer1[1] = Layer1Y;

	Layer2[0] = Layer2X;
	Layer2[1] = Layer2Y;


	LayerMap[0] = Layer0;
	LayerMap[1] = Layer1;
	LayerMap[2] = Layer2;


	X0vsX1 = new TH2F("X0vsX1", "Comparision between X in the third layer and second", 100, Min, Max, 100, Min, Max);
	X0vsX2 = new TH2F("X0vsX2", "Comparision between X in the third layer and first", 100, Min, Max, 100, Min, Max);
	X1vsX2 = new TH2F("X1vsX2", "Comparision between X in the second layer and first", 100, Min, Max, 100, Min, Max);

	Y0vsY1 = new TH2F("Y0vsY1", "Comparision between Y in the third layer and second", 100, Min, Max, 100, Min, Max);
	Y0vsY2 = new TH2F("Y0vsY2", "Comparision between Y in the third layer and first", 100, Min, Max, 100, Min, Max);
	Y1vsY2 = new TH2F("Y1vsY2", "Comparision between Y in the second layer and first", 100, Min, Max, 100, Min, Max);

	X0vsX1->Sumw2();
	X0vsX2->Sumw2();
	X1vsX2->Sumw2();

	Y0vsY1->Sumw2();
	Y0vsY2->Sumw2();
	Y1vsY2->Sumw2();

	_overcount =0;
	_ResFitBounds =10;
	int ResMin = -_ResFitBounds;
	int ResMax = _ResFitBounds;

	X0res = new TH1F("X0res", "X Resolution of the third wire chamber", 100, ResMin, ResMax);
	X1res = new TH1F("X1res", "X Resolution of the second wire chamber", 100, ResMin, ResMax);
	X2res = new TH1F("X2res", "X Resolution of the first wire chamber", 100, ResMin, ResMax);

	Y0res = new TH1F("Y0res", "Y Resolution of the third wire chamber", 100, ResMin, ResMax);
	Y1res = new TH1F("Y1res", "Y Resolution of the second wire chamber", 100, ResMin, ResMax);
	Y2res = new TH1F("Y2res", "Y Resolution of the first wire chamber", 100, ResMin, ResMax);

}

void WireChamberLayerTester::processRunHeader(LCRunHeader* run){
	RunNumber = dynamic_cast<stringstream *> (&(stringstream() << run->getRunNumber()))->str();
}

void WireChamberLayerTester::processEvent(LCEvent* event) {

	int LayerCount = 3;
	vector <float> z (LayerCount);
	vector <vector<TrackerHit* > > HitList;
	HitList.resize(LayerCount); //We have 3 Layers

	LCCollection* hits = event->getCollection("CalibratedWireChamberHits");
	UTIL::CellIDDecoder<TrackerHit> iden(hits);
	map<int, vector<TrackerHit*> > HitLayerMap = WireChamberUtil::buildHitMap(hits, "layer");
	for(int i=0; i<hits->getNumberOfElements(); i++){
		TrackerHit* hit = dynamic_cast<TrackerHit*>(hits->getElementAt(i));
		int layer = int(iden(hit)["layer"].value());
		for(int x=0; x<LayerCount; x++){
			if(layer == x and hit->getPosition()[0] < 50 and hit->getPosition()[1] < 50 and hit->getPosition()[0] > -50 and hit->getPosition()[1] > -50){
				LayerMap[x][0]->Fill(hit->getPosition()[0]);
				LayerMap[x][1]->Fill(hit->getPosition()[1]);

				int High = _FitBounds;
				int Low = -_FitBounds;
				if(layer == 0){
					for(int j=0; j<hits->getNumberOfElements(); j++){
						TrackerHit* otherhit = dynamic_cast<TrackerHit*>(hits->getElementAt(j));

						int otherlayer = int(iden(otherhit)["layer"].value());
						//Third and Second Layer
						if(otherlayer == 1 and otherhit->getPosition()[0] < 50 and otherhit->getPosition()[1] < 50 and otherhit->getPosition()[0] > -50 and otherhit->getPosition()[1] > -50){
							float x0 = hit->getPosition()[0];
							float x1 = otherhit->getPosition()[0];

							float y0 = hit->getPosition()[1];
							float y1 = otherhit->getPosition()[1];
							if(x0- x1 < High and x0 - x1 > Low and y0 -y1 < High and y0 - y1 > Low){
								X0vsX1->Fill(otherhit->getPosition()[0], hit->getPosition()[0]);
								Y0vsY1->Fill(otherhit->getPosition()[1], hit->getPosition()[1]);
								//added For Noise/Non Noise
								/*
								LayerMap[layer][0]->Fill(hit->getPosition()[0]);
								LayerMap[otherlayer][0]->Fill(otherhit->getPosition()[0]);
								LayerMap[layer][1]->Fill(hit->getPosition()[1]);
								LayerMap[otherlayer][1]->Fill(otherhit->getPosition()[1]);
								*/
							}
						//Third and First Layer
						} else if(otherlayer == 2 and otherhit->getPosition()[0] < 50 and otherhit->getPosition()[1] < 50 and otherhit->getPosition()[0] > -50 and otherhit->getPosition()[1] > -50){
							float x0 = hit->getPosition()[0];
							float x2 = otherhit->getPosition()[0];

							float y0 = hit->getPosition()[1];
							float y2 = otherhit->getPosition()[1];


							if(x0- x2 < High and x0 - x2 > Low and y0 -y2 < High and y0 - y2 > Low){
								X0vsX2->Fill(otherhit->getPosition()[0], hit->getPosition()[0]);
								Y0vsY2->Fill(otherhit->getPosition()[1], hit->getPosition()[1]);
								//added
								/*
								LayerMap[layer][0]->Fill(hit->getPosition()[0]);
								LayerMap[otherlayer][0]->Fill(otherhit->getPosition()[0]);
								LayerMap[layer][1]->Fill(hit->getPosition()[1]);
								LayerMap[otherlayer][1]->Fill(otherhit->getPosition()[1]);
								*/
							}
						}
					}
				}
				//Second and First Layer
				if(layer == 1){
					for(int j=0; j<hits->getNumberOfElements(); j++){
						TrackerHit* otherhit = dynamic_cast<TrackerHit*>(hits->getElementAt(j));
						int otherlayer = int(iden(otherhit)["layer"].value());
						if(otherlayer == 2 and otherhit->getPosition()[0] < 50 and otherhit->getPosition()[1] < 50 and otherhit->getPosition()[0] > -50 and otherhit->getPosition()[1] > -50){
							float x1 = hit->getPosition()[0];
							float x2 = otherhit->getPosition()[0];

							float y1 = hit->getPosition()[1];
							float y2 = otherhit->getPosition()[1];

							if(x1- x2 < High and x1 - x2 > Low and y1 -y2 < High and y1 - y2 > Low){
								X1vsX2->Fill(otherhit->getPosition()[0], hit->getPosition()[0]);
								Y1vsY2->Fill(otherhit->getPosition()[1], hit->getPosition()[1]);
								 //added
								/*
								LayerMap[layer][0]->Fill(hit->getPosition()[0]);
								LayerMap[otherlayer][0]->Fill(otherhit->getPosition()[0]);
								LayerMap[layer][1]->Fill(hit->getPosition()[1]);
								LayerMap[otherlayer][1]->Fill(otherhit->getPosition()[1]);
								*/
							}
						}
					}
				}
			}
		}

	}
	vector<TrackerHit*>::iterator first =  HitLayerMap[0].begin();
	vector<TrackerHit*>::iterator second =  HitLayerMap[1].begin();
	vector<TrackerHit*>::iterator third =  HitLayerMap[2].begin();
	for( ; first != HitLayerMap[0].end(); first++){
	        for( ; second != HitLayerMap[1].end(); second++){
	                for( ; third != HitLayerMap[2].end(); third++){
	                        vector<float> firstres = WireChamberUtil::ResolutionCalculator((*first), (*second), (*third));
	                        X0res->Fill(firstres[0]);
	                        Y0res->Fill(firstres[1]);
	                        vector<float> secondres = WireChamberUtil::ResolutionCalculator((*second), (*first), (*third));
	                        X1res->Fill(secondres[0]);
	                        Y1res->Fill(secondres[1]);
	                        vector<float> thirdres = WireChamberUtil::ResolutionCalculator((*third), (*first), (*second));
	                        X2res->Fill(thirdres[0]);
	                        Y2res->Fill(thirdres[1]);
	                }
	        }
	}



}

 static Double_t doublegauss(Double_t *x, Double_t *par){
		Double_t arg1 = 0;
		Double_t arg2 = 0;
		if(par[1] != 0) arg1 = (x[0]-par[4])/par[1];
		if(par[2] != 0) arg2 = (x[0]-par[4])/par[3];
		Double_t fitval = par[0]*TMath::Exp(-0.5*arg1*arg1)+ par[2]*TMath::Exp(-0.5*arg2*arg2);
		return fitval;
	}

void WireChamberLayerTester::end(){
	map<int, vector<TH1F*> >::iterator iter;

	string FileName = "../steer/LayerComparision" + RunNumber + ".root";

	TFile *LayerComparision = new TFile(FileName.c_str(), "RECREATE");

	if(_Title == 1){
		string Title = "Run: " + RunNumber;
		Layer0X->SetTitle(Title.c_str());
		Layer0Y->SetTitle(Title.c_str());
		Layer1X->SetTitle(Title.c_str());
		Layer1Y->SetTitle(Title.c_str());
		Layer2X->SetTitle(Title.c_str());
		Layer2Y->SetTitle(Title.c_str());
		X0vsX1->SetTitle(Title.c_str());
		Y0vsY1->SetTitle(Title.c_str());
		X0vsX2->SetTitle(Title.c_str());
		Y0vsY2->SetTitle(Title.c_str());
		X1vsX2->SetTitle(Title.c_str());
		Y1vsY2->SetTitle(Title.c_str());
		X0res->SetTitle(Title.c_str());
		Y0res->SetTitle(Title.c_str());
		X1res->SetTitle(Title.c_str());
		Y1res->SetTitle(Title.c_str());
		X2res->SetTitle(Title.c_str());
		Y2res->SetTitle(Title.c_str());
	}else if(_Title == 0){
		Layer0X->SetTitle("");
		Layer0Y->SetTitle("");
		Layer1X->SetTitle("");
		Layer1Y->SetTitle("");
		Layer2X->SetTitle("");
		Layer2Y->SetTitle("");
		X0vsX1->SetTitle("");
		Y0vsY1->SetTitle("");
		X0vsX2->SetTitle("");
		Y0vsY2->SetTitle("");
		X1vsX2->SetTitle("");
		Y1vsY2->SetTitle("");
		X0res->SetTitle("");
		Y0res->SetTitle("");
		X1res->SetTitle("");
		Y1res->SetTitle("");
		X2res->SetTitle("");
		Y2res->SetTitle("");
	}

	for(iter = LayerMap.begin(); iter!=LayerMap.end(); iter++){
		vector<TH1F* > Graphs = iter->second;

		vector<TH1F*>::iterator iterer;
		Graphs[0]->GetXaxis()->SetTitle("X Position (mm)");
		Graphs[1]->GetXaxis()->SetTitle("Y Position (mm)");
		for(iterer = Graphs.begin(); iterer != Graphs.end(); iterer++){
			(*iterer)->GetYaxis()->SetTitle("Events");
			(*iterer)->Write();
		}
	}

	X0vsX1->GetXaxis()->SetTitle("X Position in Second Layer (mm)");
	X0vsX1->GetYaxis()->SetTitle("X Position in Third Layer (mm)");

	Y0vsY1->GetXaxis()->SetTitle("Y Position in Second Layer (mm)");
	Y0vsY1->GetYaxis()->SetTitle("Y Position in Third Layer (mm)");


	X0vsX2->GetXaxis()->SetTitle("X Position in First Layer (mm)");
	X0vsX2->GetYaxis()->SetTitle("X Position in Third Layer (mm)");

	Y0vsY2->GetXaxis()->SetTitle("Y Position in First Layer (mm)");
	Y0vsY2->GetYaxis()->SetTitle("Y Position in Third Layer (mm)");


	X1vsX2->GetXaxis()->SetTitle("X Position in First Layer (mm)");
	X1vsX2->GetYaxis()->SetTitle("X Position in Second Layer (mm)");

	Y1vsY2->GetXaxis()->SetTitle("Y Position in First Layer (mm)");
	Y1vsY2->GetYaxis()->SetTitle("Y Position in Second Layer (mm)");

	int FitLow = -40;
	int FitHigh = 40;
	
	TF1* f1 = new TF1("f1", "pol1", 0, 1);
	f1->SetParNames("Intercept", "Slope");

	X0vsX1->Fit("f1", "", "", FitLow, FitHigh);
	X0vsX2->Fit("f1", "", "", FitLow, FitHigh);
	X1vsX2->Fit("f1", "", "", FitLow, FitHigh);

	Y0vsY1->Fit("f1", "", "", FitLow, FitHigh);
	Y0vsY2->Fit("f1", "", "", FitLow, FitHigh);
	Y1vsY2->Fit("f1", "", "", FitLow, FitHigh);

	X0vsX1->Write();
	X0vsX2->Write();
	X1vsX2->Write();

	Y0vsY1->Write();
	Y0vsY2->Write();
	Y1vsY2->Write();

	X0res->Scale(1/X0res->GetEntries());
	X1res->Scale(1/X1res->GetEntries());
	X2res->Scale(1/X2res->GetEntries());

	Y0res->Scale(1/Y0res->GetEntries());
	Y1res->Scale(1/Y1res->GetEntries());
	Y2res->Scale(1/Y2res->GetEntries());

	X0res->GetXaxis()->SetTitle("Third Chamber X Resolution (mm)");
	X1res->GetXaxis()->SetTitle("Second Chamber X Resolution (mm)");
	X2res->GetXaxis()->SetTitle("First Chamber X Resolution (mm)");

	Y0res->GetXaxis()->SetTitle("Third Chamber Y Resolution (mm)");
	Y1res->GetXaxis()->SetTitle("Second Chamber Y Resolution (mm)");
	Y2res->GetXaxis()->SetTitle("First Chamber Y Resolution (mm)");

	X0res->GetYaxis()->SetTitle("Fraction of Events");
	X1res->GetYaxis()->SetTitle("Fraction of Events");
	X2res->GetYaxis()->SetTitle("Fraction of Events");

	Y0res->GetYaxis()->SetTitle("Fraction of Events");
	Y1res->GetYaxis()->SetTitle("Fraction of Events");
	Y2res->GetYaxis()->SetTitle("Fraction of Events");


	TF1 *doublegaus = new TF1("doublegaussian",CALICE::doublegauss, -10, 10, 5);
	doublegaus->SetParameters(0.3, 1 ,0.1, 5, 0);
	doublegaus->SetParNames("Resolution Amplitude", "Resolution Width", "Noise Amplitude", "Noise Width", "Mean");

	X0res->Fit("doublegaussian");
	X1res->Fit("doublegaussian");
	X2res->Fit("doublegaussian");

	Y0res->Fit("doublegaussian");
	Y1res->Fit("doublegaussian");
	Y2res->Fit("doublegaussian");

	X0res->Write();
	X1res->Write();
	X2res->Write();

	Y0res->Write();
	Y1res->Write();
	Y2res->Write();



	cout << "X Histograms" << endl;
	cout << "------------" << endl;
	cout << "Layer0X Mean: " << Layer0X->GetMean() << endl;
	cout << "Layer1X Mean: " << Layer1X->GetMean() << endl;
	cout << "Layer2X Mean: " << Layer2X->GetMean() << endl;
	cout << "------------" << endl;
	cout << "Y Histograms" << endl;
	cout << "------------" << endl;
	cout << "Layer0Y Mean: " << Layer0Y->GetMean() << endl;
	cout << "Layer1Y Mean: " << Layer1Y->GetMean() << endl;
	cout << "Layer2Y Mean: " << Layer2Y->GetMean() << endl;
	cout << "=============" << endl;
	cout << "The overcount is: " << _overcount << endl;

	delete X0res;
	delete X1res;
	delete X2res;
	delete Y0res;
	delete Y1res;
	delete Y2res;


	delete X0vsX1;
	delete X0vsX2;
	delete X1vsX2;

	delete Y0vsY1;
	delete Y0vsY2;
	delete Y1vsY2;


	delete Layer0X;
	delete Layer0Y;
	delete Layer1X;
	delete Layer1Y;
	delete Layer2X;
	delete Layer2Y;

	cout << "Wrote Histograms to: "<< LayerComparision->GetName() << endl;
	LayerComparision->Close();



}


} /*Namespace CALICE*/
