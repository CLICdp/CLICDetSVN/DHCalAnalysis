
/*Muon Tracker
 *
 * March 21 2013
 *
 * William Nash
 */

#include <MuonTracker.hh>
#include "DhcalUtil.hh"
#include "WireChamberUtil.hh"

//lcio
#include "IMPL/ClusterImpl.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include <EVENT/Track.h>

//c++
#include <fstream>
#include <sstream>

//root
#include <TFile.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TF1.h>
#include <TMath.h>
#include <TPad.h>
#include <TPaveStats.h>
#include <TLegend.h>

//misc
#include <ExtendedTrack.hh>

using namespace marlin;
using namespace lcio;
using namespace std;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::Cluster;

namespace CALICE{

MuonTracker aMuonTracker;

MuonTracker::MuonTracker() : Processor("MuonTracker"){
  _description = "Compares Track fit in Wire Chamber to Actual Tracks of Muon events";


  //Delta Distribution
  registerProcessorParameter("XBins", "Bin Number for X Axis", _xBins, 80);
  registerProcessorParameter("YBins", "Bin Number for Y Axis", _yBins, 80);
  registerProcessorParameter("XMin", "Minimum X value", _xMin, (float) -5);
  registerProcessorParameter("YMin", "Minimum Y value", _yMin, (float) -5);
  registerProcessorParameter("XMax", "Maximum X value", _xMax, (float) 5);
  registerProcessorParameter("YMax", "Maximum Y value", _yMax, (float) 5);

  //Muon Parameters
  registerProcessorParameter("LayersConsidered", "Amount of Layers that are considered to qualify a Muon Event", _layersConsidered, 10);
  registerProcessorParameter("ChiLimit", "Maximum Chi-Squared allowed for fit to be accepted", _chiLimit, (float) 5.0);
  registerProcessorParameter("ClusterSize", "The maximum allowed size of a cluster in number of pads", _clusterSize, 5);
  registerProcessorParameter("PercentEfficiency", "Estimated Efficiency ", _efficiency, (float) 0.80);
  registerProcessorParameter("SkipLimit", "Maximum allowed number of layers that can be skipped and still take the event", _skipLimit, 2);

  //Face hits
  registerProcessorParameter("MuonZ0DistXBins", "Bin Number for MuonZ0Dist X Axis", _MuonxBins, 50);
  registerProcessorParameter("MuonZ0DistYBins", "Bin Number for MuonZ0Dist Y Axis", _MuonyBins, 50);
  registerProcessorParameter("MuonZ0XMin", "Minimum MuonZ0Dist X value", _MuonxMin, (float) -100);
  registerProcessorParameter("MuonZ0YMin", "Minimum MuonZ0Dist Y value", _MuonyMin, (float) -100);
  registerProcessorParameter("MuonZ0XMax", "Maximum MuonZ0Dist X value", _MuonxMax, (float) 100);
  registerProcessorParameter("MuonZ0YMax", "Maximum MuonZ0Dist Y value", _MuonyMax, (float) 100);


  registerProcessorParameter("Title", "Graph Titles are Run number if =1, Blank if =0, Description if other", _Title, 1);
}

MuonTracker::~MuonTracker(){
	delete MuonDeltaDistribution;
	delete XDeltaDistribution;
	delete YDeltaDistribution;
	delete MuonZ0Dist;
	delete XWCError;
	delete YWCError;
	delete XDHCALError;
	delete YDHCALError;
}

void MuonTracker::init(){
	MuonDeltaDistribution = new TH2F("Muon Track Uncertainty", "Difference Between Calculated and Observed Muon Tracks at Z=0", _xBins, _xMin, _xMax, _yBins, _yMin, _yMax);
	XDeltaDistribution = new TH1F("X Track Uncertainty", "X Uncertainty in Muon Tracks at Z=0 over Expected Error", _xBins, _xMin, _xMax);
	YDeltaDistribution = new TH1F("Y Track Uncertainty", "Y Uncertainty in Muon Tracks at Z=0 over Expected Error", _yBins, _yMin, _yMax);
	MuonZ0Dist = new TH2F("Muon Positions","Tracked Muon Locations at the Front Face of the DHCAL", _MuonxBins, _MuonxMin, _MuonxMax, _MuonyBins, _MuonyMin, _MuonyMax);
	XWCError = new TH1F("Projected #sigma_{X_{W}}", "DWCs (#sigma_{X_{W}})",500,0,10);
	YWCError = new TH1F("Projected #sigma_{Y_{W}}", "DWCs (#sigma_{Y_{W}})",500,0,10);
	XDHCALError = new TH1F("Projected #sigma_{X}", "DHCAL (#sigma_{X})", 500,0,10);
	YDHCALError = new TH1F("Projected #sigma_{Y}", "DHCAL (#sigma_{Y})", 500,0,10);


	XWCError->Sumw2();
	YWCError->Sumw2();
	XDHCALError->Sumw2();
	YDHCALError->Sumw2();
	MuonDeltaDistribution->Sumw2();
	XDeltaDistribution->Sumw2();
	YDeltaDistribution->Sumw2();
	MuonZ0Dist->Sumw2();
}

void MuonTracker::processRunHeader(LCRunHeader* run){
	RunNumber = dynamic_cast<stringstream *> (&(stringstream() << run->getRunNumber()))->str();
}

void MuonTracker::processEvent(LCEvent* event){

	float HitCount= 0;
	int SkipCount = 0;

	if(_layersConsidered <= 0){
		streamlog_out(WARNING) << "No Layers Considered" << endl;
		return;
	}

	string collectionName = "WireChamberTracks";
	LCCollection* WireChamberTracks;
	try {
		WireChamberTracks = event->getCollection(collectionName);
		if(not WireChamberTracks){
			return; //No Wire Good Wire Chamber Tracks
		}
	} catch(lcio::DataNotAvailableException e ) {
		streamlog_out(WARNING0) << collectionName << " collection not available" << endl;
		return;
	}


	IMPL::ExtendedTrack* WireChamberTrack = dynamic_cast<IMPL::ExtendedTrack*>(WireChamberTracks->getElementAt(0));
	const float *WireChamberTrackedPosition = WireChamberTrack->getReferencePoint();

	LCCollection* Clusterz = event->getCollection("LayerClusters");
	map<int, vector<Cluster*> > LayerMap = DhcalUtil::buildLayerClusterMap(Clusterz, "layer");
	streamlog_out(DEBUG) << "The size of the layer map is:" << LayerMap.size() << endl;
	vector<Cluster*> MuonHits; //Vector of Clusters which will be used for the muon track fit

	for(int i=0; i< _layersConsidered; i++){
		if(LayerMap[i].size() == 1){
			if(LayerMap[i][0]->getCalorimeterHits().size() > abs(_clusterSize)){
				streamlog_out(DEBUG) << "Cluster Size too big in Layer " << i << ": " << LayerMap[i][0]->getCalorimeterHits().size() << " > " << abs(_clusterSize) << endl;
				SkipCount++;
			}else{
				MuonHits.resize(HitCount + 1);
				MuonHits[HitCount] = LayerMap[i][0];
				HitCount++;
				SkipCount = 0;
			}
		}else{
			SkipCount++;
			if(SkipCount > _skipLimit){
				streamlog_out(DEBUG) << "Too many skipped layers" << endl;
				return;
			}
		}
	}
	if(
			HitCount/_layersConsidered < _efficiency or HitCount < 3){  //If the efficiency is too low, or not enough points for a line fit
		return;
	}

	IMPL::ExtendedTrack* DHCALMuonTrack = dynamic_cast<IMPL::ExtendedTrack*>(WireChamberUtil::ExtendedLineFit(MuonHits));
	const float *DHCALMuonTrackedPosition = DHCALMuonTrack->getReferencePoint();

	if(DHCALMuonTrack->getXZchi2()/DHCALMuonTrack->getNdf() > _chiLimit/2 or DHCALMuonTrack->getYZchi2()/DHCALMuonTrack->getNdf() > _chiLimit/2){
		streamlog_out(DEBUG) << "Chi Squared not met" << endl;
		delete DHCALMuonTrack;
		return;
	}

	MuonZ0Dist->Fill(DHCALMuonTrackedPosition[0], DHCALMuonTrackedPosition[1]);

	XWCError->Fill(sqrt(WireChamberTrack->getXZintVar()));
	YWCError->Fill(sqrt(WireChamberTrack->getYZintVar()));
	XDHCALError->Fill(sqrt(DHCALMuonTrack->getXZintVar()));
	YDHCALError->Fill(sqrt(DHCALMuonTrack->getYZintVar()));

	float XSigma = sqrt(DHCALMuonTrack->getXZintVar() + WireChamberTrack->getXZintVar());
	float YSigma = sqrt(DHCALMuonTrack->getYZintVar() + WireChamberTrack->getYZintVar());


	float FitUncertaintyX = (DHCALMuonTrackedPosition[0] - WireChamberTrackedPosition[0])/XSigma;
	float FitUncertaintyY = (DHCALMuonTrackedPosition[1] - WireChamberTrackedPosition[1])/YSigma;

	streamlog_out( DEBUG0 ) << "The actual X value found is: " << MuonHits[0]->getPosition()[0] << endl;
	streamlog_out( DEBUG0 ) << "The Predicted X value is: " << WireChamberTrackedPosition[0] << endl;
	streamlog_out( DEBUG0 ) << "The actual Y value found is: " << MuonHits[0]->getPosition()[1] << endl;
	streamlog_out( DEBUG0 ) << "The Predicted Y value is: " << WireChamberTrackedPosition[1] << endl;

	streamlog_out( DEBUG4 ) << "X-Fit Uncertainty = " << FitUncertaintyX << endl;
	streamlog_out( DEBUG4 ) << "Y-Fit Uncertainty = " << FitUncertaintyY << endl;



	MuonDeltaDistribution->Fill(FitUncertaintyX, FitUncertaintyY);

	XDeltaDistribution->Fill(FitUncertaintyX);
	YDeltaDistribution->Fill(FitUncertaintyY);

}

TCanvas* MuonTracker::PlotOverlay(TH1F* Plot1, TH1F* Plot2, TString Title){
	gStyle->SetOptStat(1101);

	TLegend* Legend = new TLegend(0.65,0.77,0.83,0.87);
	Legend->AddEntry(Plot1, Plot1->GetTitle(), "le");
	Legend->AddEntry(Plot2, Plot2->GetTitle(), "le");

	TCanvas* Canvas = new TCanvas(Title,Title,1);
	TPad* pad1 = new TPad("pad1","",0,0,1,1);
	pad1->Draw();
	pad1->cd();
	Canvas->Update();
	Plot1->SetLineColor(4);
	Plot1->SetTitle("");
	Plot1->Draw();
	pad1->Update();
	Canvas->Update();
	Plot2->SetTitle("");
	Plot2->Draw("sames");
	pad1->Update();
	Canvas->Update();
	TPaveStats* Plot1Stats = (TPaveStats*)Plot1->GetListOfFunctions()->FindObject("stats");
	TPaveStats* Plot2Stats = (TPaveStats*)Plot2->GetListOfFunctions()->FindObject("stats");

	Plot1Stats->SetX1NDC(0.85);
	Plot1Stats->SetX2NDC(1.00);
	Plot1Stats->SetY1NDC(0.85);
	Plot1Stats->SetY2NDC(1.00);
	Plot1Stats->Draw();
	Plot2Stats->SetX1NDC(0.85);
	Plot2Stats->SetX2NDC(1.00);
	Plot2Stats->SetY1NDC(0.70);
	Plot2Stats->SetY2NDC(0.85);
	Plot2Stats->Draw();

	pad1->Update();
	Canvas->Update();
	Legend->Draw();
	pad1->Update();
	pad1->Modified();
	Canvas->cd();
	Canvas->Update();

	return Canvas;

}

void MuonTracker::end(){

	string FileName = "../steer/MuonTrackComparision" + RunNumber + ".root";
	TFile MuonComparison(FileName.c_str(), "RECREATE");

	string XAxis = "#DeltaX/#sigma_{#Delta X}";
	string YAxis = "#DeltaY/#sigma_{#Delta Y}";

	string Title = "Run: " + RunNumber;

	if(_Title == 1){
		MuonDeltaDistribution->SetTitle(Title.c_str());
		XDeltaDistribution->SetTitle(Title.c_str());
		YDeltaDistribution->SetTitle(Title.c_str());
		MuonZ0Dist->SetTitle(Title.c_str());
	}else if(_Title == 0){
		MuonDeltaDistribution->SetTitle("");
		XDeltaDistribution->SetTitle("");
		YDeltaDistribution->SetTitle("");
		MuonZ0Dist->SetTitle("");

	}

	MuonDeltaDistribution->GetXaxis()->SetTitle(XAxis.c_str());
	MuonDeltaDistribution->GetYaxis()->SetTitle(YAxis.c_str());
	MuonDeltaDistribution->Write();


	MuonZ0Dist->GetXaxis()->SetTitle("X Position (mm)");
	MuonZ0Dist->GetYaxis()->SetTitle("Y Position (mm)");
	MuonZ0Dist->Write();
	
	TF1* f1 = new TF1("f1", "gaus", 0,1);
	f1->SetParNames("Amplitude", "Mean", "Width");

	gStyle->SetOptFit(1111);
	XDeltaDistribution->GetXaxis()->SetTitle(XAxis.c_str());
	XDeltaDistribution->GetYaxis()->SetTitle("Fraction of Events");
	XDeltaDistribution->Scale(1/XDeltaDistribution->GetEntries());
	XDeltaDistribution->Fit("f1", "", "", XDeltaDistribution->GetMean() - 2*XDeltaDistribution->GetRMS(), XDeltaDistribution->GetMean() + 2*XDeltaDistribution->GetRMS());
	XDeltaDistribution->Write();

	YDeltaDistribution->GetXaxis()->SetTitle(YAxis.c_str());
	YDeltaDistribution->GetYaxis()->SetTitle("Fraction of Events");
	YDeltaDistribution->Scale(1/YDeltaDistribution->GetEntries());
	YDeltaDistribution->Fit("f1", "", "", YDeltaDistribution->GetMean() - 2*YDeltaDistribution->GetRMS(), YDeltaDistribution->GetMean() + 2*YDeltaDistribution->GetRMS());
	YDeltaDistribution->Write();

	XWCError->GetXaxis()->SetTitle("X Fit Error (mm)");
	YWCError->GetXaxis()->SetTitle("Y Fit Error (mm)");
	XDHCALError->GetXaxis()->SetTitle("X Fit Error (mm)");
	YDHCALError->GetXaxis()->SetTitle("Y Fit Error (mm)");
	XWCError->Scale(1./XWCError->GetEntries());
	YWCError->Scale(1./YWCError->GetEntries());
	XDHCALError->Scale(1./XDHCALError->GetEntries());
	YDHCALError->Scale(1./YDHCALError->GetEntries());
	XWCError->GetYaxis()->SetTitle("Normalized Events");
	YWCError->GetYaxis()->SetTitle("Normalized Events");
	XDHCALError->GetYaxis()->SetTitle("Normalized Events");
	YDHCALError->GetYaxis()->SetTitle("Normalized Events");

	TCanvas* XError = (TCanvas*)PlotOverlay(XWCError,XDHCALError, "XError");
	TCanvas* YError = (TCanvas*)PlotOverlay(YWCError,YDHCALError, "YError");

	XError->Write();
	YError->Write();

	streamlog_out( WARNING4 )  << "Muon Candidates Found: " << MuonZ0Dist->GetEntries() << endl;
	streamlog_out( WARNING4 ) << "Events used in Delta Distribution: " << MuonDeltaDistribution->GetEntries() << endl;

	delete XDeltaDistribution;
	delete YDeltaDistribution;
	delete MuonDeltaDistribution;
	delete MuonZ0Dist;
	delete XError;
	delete YError;
	delete XWCError;
	delete YWCError;
	delete XDHCALError;
	delete YDHCALError;


	cout << "Wrote XY Histogram to: " << MuonComparison.GetName() << endl;
	MuonComparison.Close();

}

} /*Namespace CALICE*/

