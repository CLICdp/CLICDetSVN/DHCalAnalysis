/*
 * LayerColumnRowMap.cc
 *
 *  Created on: Apr 9, 2014
 *      Author: Christian Grefe, CERN
 */

#include "LayerColumnRowMap.hh"
#include "DhcalMapping.hh"

// lcio
#include "EVENT/CalorimeterHit.h"
#include "EVENT/SimCalorimeterHit.h"

// c++
#include <stdexcept>

using std::string;
using std::out_of_range;

namespace CALICE {

/// Default constructor
template<class T>
LayerColumnRowMap<T>::LayerColumnRowMap() :
		_useDefaultMapping(true), _idDecoder("") {
}

/// Constructor using specific decoding
template<class T>
LayerColumnRowMap<T>::LayerColumnRowMap(const string& cellEncoding, const string& uIdentifier,
		const string& vIdentifier, const string& layerIdentifier) :
		_useDefaultMapping(false), _idDecoder(cellEncoding), _uIdentifier(uIdentifier), _vIdentifier(vIdentifier), _layerIdentifier(
				layerIdentifier) {
}

/// Destructor
template<class T>
LayerColumnRowMap<T>::~LayerColumnRowMap() {

}

/// Adds an object to the map using the given indices.
template<class T>
void LayerColumnRowMap<T>::fill(int layerIndex, int columnIndex, int rowIndex, T obj) {
	(*this)[layerIndex][columnIndex][rowIndex] = obj;
}

/// Checks if an object exist at the given indices.
template<class T>
bool LayerColumnRowMap<T>::contains(int layerIndex, int columnIndex, int rowIndex) const {
	try {
		get(layerIndex, columnIndex, rowIndex);
	} catch (out_of_range& e) {
		return false;
	}
	return true;
}

/// Checks if an object exists with the given cell ID.
template<class T>
bool LayerColumnRowMap<T>::contains(const CellID& cellID) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return contains(layer, column, row);
}

/// Returns the object with the given indices if it exists. Returns a null pointer otherwise.
template<class T>
const T& LayerColumnRowMap<T>::get(int layerIndex, int columnIndex, int rowIndex) const {
	return this->at(layerIndex).at(columnIndex).at(rowIndex);
}

/// Returns the object with the given cell ID if it exists. Returns a null pointer otherwise.
template<class T>
const T& LayerColumnRowMap<T>::get(const CellID& cellID) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return get(layer, column, row);
}

/// Returns all objects within the given limits if it exists.
template<class T>
std::vector<T> LayerColumnRowMap<T>::getAll(int minLayer, int maxLayer, int minColumn, int maxColumn, int minRow,
		int maxRow) const {
	std::vector<T> objects;
	for (int iLayer = minLayer; iLayer <= maxLayer; iLayer++) {
		for (int iColumn = minColumn; iColumn <= maxColumn; iColumn++) {
			for (int iRow = minRow; iRow <= maxRow; iRow++) {
				T obj = this->get(iLayer, iColumn, iRow);
				if (obj) {
					objects.push_back(obj);
				}
			}
		}
	}
	return objects;
}

/// Returns a vector containing all neighbouring objects to the given cell indices within the given limits.
template<class T>
std::vector<T> LayerColumnRowMap<T>::getNeighbours(int layerIndex, int columnIndex, int rowIndex, int layerDistance,
		int columnDistance, int rowDistance) const {
	std::vector<T> neighbours;
	for (int iLayer = layerIndex - layerDistance; iLayer <= layerIndex + layerDistance; iLayer++) {
		for (int iColumn = columnIndex - columnDistance; iColumn <= columnIndex + columnDistance; iColumn++) {
			for (int iRow = rowIndex - rowDistance; iRow <= rowIndex + rowDistance; iRow++) {
				T neighbour = this->get(iLayer, iColumn, iRow);
				if (neighbour and not (iLayer == layerIndex and iColumn == columnIndex and iRow == rowIndex)) {
					neighbours.push_back(neighbour);
				}
			}
		}
	}
	return neighbours;
}

/// Returns a vector containing all neighbouring objects to the given cell ID within the given limits.
template<class T>
std::vector<T> LayerColumnRowMap<T>::getNeighbours(const CellID& cellID, int layerDistance, int columnDistance,
		int rowDistance) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return this->getNeighbours(layer, column, row, layerDistance, columnDistance, rowDistance);
}

//
//
// Pointer specialization
//
//

/// Default constructor
template<class T>
LayerColumnRowMap<T*>::LayerColumnRowMap() :
		_useDefaultMapping(true), _idDecoder("") {
}

/// Constructor using specific decoding
template<class T>
LayerColumnRowMap<T*>::LayerColumnRowMap(const string& cellEncoding, const string& uIdentifier,
		const string& vIdentifier, const string& layerIdentifier) :
		_useDefaultMapping(false), _idDecoder(cellEncoding), _uIdentifier(uIdentifier), _vIdentifier(vIdentifier), _layerIdentifier(
				layerIdentifier) {
}

/// Destructor
template<class T>
LayerColumnRowMap<T*>::~LayerColumnRowMap() {

}

/// Adds an object to the map using the given indices.
template<class T>
void LayerColumnRowMap<T*>::fill(int layerIndex, int columnIndex, int rowIndex, T* obj) {
	(*this)[layerIndex][columnIndex][rowIndex] = obj;
}

/* Adds an object to the map using its cell ID to determine the indices.
 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
 */
template<class T>
void LayerColumnRowMap<T*>::fill(T* obj) {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(obj, column, row, layer);
	} else {
		_idDecoder.setValue(DhcalMapping::getCellID(obj));
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	(*this)[layer][column][row] = obj;
}

/* Adds all objects to the map using their cell IDs to determine the indices.
 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
 */
template<class T>
void LayerColumnRowMap<T*>::fill(const std::vector<T*>& objects) {
	typename std::vector<T*>::const_iterator it = objects.begin();
	while (it != objects.end()) {
		fill(*it);
		++it;
	}
}

/* Adds all objects from a collection to the map using their cell ID to determine the indices.
 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
 */
template<class T>
void LayerColumnRowMap<T*>::fill(const EVENT::LCCollection* collection) {
	for (int index = 0; index < collection->getNumberOfElements(); index++) {
		fill(dynamic_cast<T*>(collection->getElementAt(index)));
	}
}

/// Checks if an object exist at the given indices.
template<class T>
bool LayerColumnRowMap<T*>::contains(int layerIndex, int columnIndex, int rowIndex) const {
	return get(layerIndex, columnIndex, rowIndex) != NULL;
}

/// Checks if an object exists with the given cell ID.
template<class T>
bool LayerColumnRowMap<T*>::contains(const CellID& cellID) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return contains(layer, column, row);
}

/* Checks if an object exists by its cell ID.
 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
 */
template<class T>
bool LayerColumnRowMap<T*>::contains(const T* obj) const {
	return this->contains(DhcalMapping::getCellID(obj));
}

/// Returns the object with the given indices if it exists. Returns a null pointer otherwise.
template<class T>
T* LayerColumnRowMap<T*>::get(int layerIndex, int columnIndex, int rowIndex) const {
	//return this->at(layerIndex).at(columnIndex).at(rowIndex);
	typename std::map<int, std::map<int, std::map<int, T*> > >::const_iterator layerMapIt = this->find(layerIndex);
	if (layerMapIt == this->end()) {
		return NULL;
	}
	typename std::map<int, std::map<int, T*> >::const_iterator columnMapIt = layerMapIt->second.find(columnIndex);
	if (columnMapIt == layerMapIt->second.end()) {
		return NULL;
	}
	typename std::map<int, T*>::const_iterator rowMapIt = columnMapIt->second.find(rowIndex);
	if (rowMapIt == columnMapIt->second.end()) {
		return NULL;
	}
	return rowMapIt->second;
}

/// Returns the object with the given cell ID if it exists. Returns a null pointer otherwise.
template<class T>
T* LayerColumnRowMap<T*>::get(const CellID& cellID) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return get(layer, column, row);
}

/// Returns all objects within the given limits if it exists.
template<class T>
std::vector<T*> LayerColumnRowMap<T*>::getAll(int minLayer, int maxLayer, int minColumn, int maxColumn, int minRow,
		int maxRow) const {
	std::vector<T*> objects;
	for (int iLayer = minLayer; iLayer <= maxLayer; iLayer++) {
		for (int iColumn = minColumn; iColumn <= maxColumn; iColumn++) {
			for (int iRow = minRow; iRow <= maxRow; iRow++) {
				T* obj = this->get(iLayer, iColumn, iRow);
				if (obj) {
					objects.push_back(obj);
				}
			}
		}
	}
	return objects;
}

/// Returns a vector containing all neighbouring objects to the given cell indices within the given limits.
template<class T>
std::vector<T*> LayerColumnRowMap<T*>::getNeighbours(int layerIndex, int columnIndex, int rowIndex, int layerDistance,
		int columnDistance, int rowDistance) const {
	std::vector<T*> neighbours;
	for (int iLayer = layerIndex - layerDistance; iLayer <= layerIndex + layerDistance; iLayer++) {
		for (int iColumn = columnIndex - columnDistance; iColumn <= columnIndex + columnDistance; iColumn++) {
			for (int iRow = rowIndex - rowDistance; iRow <= rowIndex + rowDistance; iRow++) {
				T* neighbour = this->get(iLayer, iColumn, iRow);
				if (neighbour and not (iLayer == layerIndex and iColumn == columnIndex and iRow == rowIndex)) {
					neighbours.push_back(neighbour);
				}
			}
		}
	}
	return neighbours;
}

/// Returns a vector containing all neighbouring objects to the given cell ID within the given limits.
template<class T>
std::vector<T*> LayerColumnRowMap<T*>::getNeighbours(const CellID& cellID, int layerDistance, int columnDistance,
		int rowDistance) const {
	int layer, column, row;
	if (_useDefaultMapping) {
		DhcalMapping::instance()->getIndices(cellID, column, row, layer);
	} else {
		_idDecoder.setValue(cellID);
		layer = _idDecoder[_layerIdentifier];
		column = _idDecoder[_uIdentifier];
		row = _idDecoder[_vIdentifier];
	}
	return this->getNeighbours(layer, column, row, layerDistance, columnDistance, rowDistance);
}

/* Returns a vector containing all neighbouring objects to the given object within the given limits.
 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
 */
template<class T>
std::vector<T*> LayerColumnRowMap<T*>::getNeighbours(const T* obj, int layerDistance, int columnDistance,
		int rowDistance) const {
	return this->getNeighbours(DhcalMapping::getCellID(obj), layerDistance, columnDistance, rowDistance);
}

// Use explicit instantiation to create the various versions of the template.
// If additional template versions are needed, declare them in your cpp file
// and include this(!) file.
// #include "LayerColumnRowMap.cc"
// template class LayerColumnRowMap<YourType>;

template class LayerColumnRowMap<int> ;
template class LayerColumnRowMap<float> ;
template class LayerColumnRowMap<double> ;

template class LayerColumnRowMap<EVENT::CalorimeterHit*> ;
template class LayerColumnRowMap<EVENT::SimCalorimeterHit*> ;

}
;

