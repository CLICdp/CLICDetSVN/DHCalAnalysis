/*
 * RPCSimChargeSpreadModel.cc
 *
 *  Created on: Nov 1, 2013
 *      Author: Christian Grefe, CERN
 */

#include "RPCSimChargeSpreadModel.hh"

#include "TMath.h"
#include "TCanvas.h"

#include <algorithm>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <stdio.h>

namespace CALICE {

using std::find;
using std::string;
using std::stringstream;
using std::vector;

/// the global factory instance
RPCSimFactory* RPCSimFactory::_instance = 0;

/// default constructor
RPCSimFactory::RPCSimFactory() {
}

/// destructor
RPCSimFactory::~RPCSimFactory() {
	if (_instance) {
		delete _instance;
	}
}

/// access to the global factory instance
RPCSimFactory* RPCSimFactory::instance() {
	if (not _instance) {
		_instance = new RPCSimFactory();
	}
	return _instance;
}

/// create a new charge spread model by its type name
RPCSimChargeSpreadModel* RPCSimFactory::createChargeSpreadModel(const string& name) const {
	if (name.compare("RPCSim3") == 0) {
		return new RPCSim3Model();
	} else if (name.compare("RPCSim4") == 0) {
		return new RPCSim4Model();
	} else if (name.compare("RPCSim5") == 0) {
		return new RPCSim5Model();
	} else if (name.compare("RPCSim6") == 0) {
			return new RPCSim6Model();
	} else {
		stringstream message;
		message << "Unknown charge spread model: " << name;
		throw std::runtime_error(message.str());
	}
}

/// default constructor
RPCSimChargeSpreadModel::RPCSimChargeSpreadModel(const string& name,
		const vector<string>& parameterUnits) :
		name(name), parameterUnits(parameterUnits), maxRadius(1.) {
	chargeSpreadFunction = 0;
}

/// destructor
RPCSimChargeSpreadModel::~RPCSimChargeSpreadModel() {
	if (chargeSpreadFunction) {
		delete chargeSpreadFunction;
	}
}

/// calculate the charge deposited at a given radius
double RPCSimChargeSpreadModel::calculateCharge(double radius) const {
	return chargeSpreadFunction->Eval(radius);
}

/// sets all parameters by their index depending on their position in the vector
void RPCSimChargeSpreadModel::setParameters(const vector<float>& parameters) {
	if ((int) parameters.size() != chargeSpreadFunction->GetNpar()) {
		stringstream message;
		message << name << " requires " << chargeSpreadFunction->GetNpar() << " parameters" << std::endl;
		throw std::runtime_error(message.str());
	}
	for (unsigned index = 0; index < parameters.size(); index++) {
		chargeSpreadFunction->SetParameter(index, (double) parameters.at(index));
	}
	this->init();
}

/// sets a parameter by its name and re-initializes the model
void RPCSimChargeSpreadModel::setParameter(const string& name, float value) {
	chargeSpreadFunction->SetParameter(name.c_str(), value);
	this->init();
}

/// sets the maximum radius of the charge spread
void RPCSimChargeSpreadModel::setMaxRadius(double radius) {
	this->maxRadius = radius;
	this->init();
}

/// access to the underlying function
const TF1* RPCSimChargeSpreadModel::function() const {
	return chargeSpreadFunction;
}

/// string representation of the model
string RPCSimChargeSpreadModel::toString() const {
	stringstream message;
	message << name << ":" << std::endl;
	for (int index = 0; index < chargeSpreadFunction->GetNpar(); index++) {
		string unit = parameterUnits.at(index);
		message << "  " << chargeSpreadFunction->GetParName(index) << ": " << chargeSpreadFunction->GetParameter(index);
		if (not unit.empty()) {
			message << " [" << unit << "]";
		}
		message << std::endl;
	}
	return message.str();
}




/// Default constructor
RPCSim3Model::RPCSim3Model() :
		RPCSimChargeSpreadModel("RPCSim3", getParameterUnits()) {
	chargeSpreadFunction = new TF1("RPCSim3Model", "exp(-x/[0])*(1-[2]) + exp(-x/[1])*[2]", 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Slope1", "Slope2", "Ratio");
	chargeSpreadFunction->SetParameters(1.0, 1.0, 0.5);
}

/// Destructor
RPCSim3Model::~RPCSim3Model() {
}

/**
 * Helper method to pre-calculate normalization. Called by ::setParameter().
 */
void RPCSim3Model::init() {
	/// Get the parameters set by the user
	double slope1 = chargeSpreadFunction->GetParameter("Slope1");
	double slope2 = chargeSpreadFunction->GetParameter("Slope2");
	double ratio = chargeSpreadFunction->GetParameter("Ratio");

	/// Calculate integral of the first exponential
	double integral1 = 2 * TMath::Pi() * slope1 * (slope1 - TMath::Exp(-maxRadius/slope1) * (slope1 + maxRadius));

	/// Calculate integral of the second exponential
	double integral2 = 2 * TMath::Pi() * slope2 * (slope2 - TMath::Exp(-maxRadius/slope2) * (slope2 + maxRadius));

	/// Replace the model function with a new one that has the updated individual normalization
	char formula[999];
	sprintf(formula, "exp(-x / [0]) / %f * (1 - [2]) + exp(-x / [1]) / %f * [2]", integral1, integral2);
	delete chargeSpreadFunction;
	chargeSpreadFunction = new TF1("RPCSim3Model", formula, 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Slope1", "Slope2", "Ratio");
	chargeSpreadFunction->SetParameters(slope1, slope2, ratio);
}

/// Helper method to access the list of parameter units
vector<string> RPCSim3Model::getParameterUnits() {
	vector<string> parameterNames;
	parameterNames.push_back("mm");
	parameterNames.push_back("mm");
	parameterNames.push_back("");
	return parameterNames;
}




/// Default constructor
RPCSim4Model::RPCSim4Model() :
		RPCSimChargeSpreadModel("RPCSim4", getParameterUnits()) {
	chargeSpreadFunction = new TF1("RPCSim4Model", "exp(-x/[0]))*(1-[2]) + exp(-x/[1])*[2]", 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Slope");
	chargeSpreadFunction->SetParameter("Slope", 1.0);
}

/// Destructor
RPCSim4Model::~RPCSim4Model() {
}

/**
 * Helper method to pre-calculate normalization. Called by ::setParameter().
 */
void RPCSim4Model::init() {
	/// Get the parameters set by the user
	double slope = chargeSpreadFunction->GetParameter("Slope");

	/// Calculate integral of the exponential
	double integral = 2 * TMath::Pi() * slope * (slope - TMath::Exp(-maxRadius/slope) * (slope + maxRadius));

	/// Replace the model function with a new one that has the updated normalization
	char formula[999];
	sprintf(formula, "exp(-x / [0]) / %f", integral);
	delete chargeSpreadFunction;
	chargeSpreadFunction = new TF1("RPCSim4Model", formula, 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Slope");
	chargeSpreadFunction->SetParameter(0, slope);
}

/// Helper method to access the list of parameter units
vector<string> RPCSim4Model::getParameterUnits() {
	vector<string> parameterNames;
	parameterNames.push_back("mm");
	return parameterNames;
}




/// Default constructor
RPCSim5Model::RPCSim5Model() :
		RPCSimChargeSpreadModel("RPCSim5", getParameterUnits()) {
	chargeSpreadFunction = new TF1("RPCSim5Model", "exp(-x^2/(2*[0]^2))*(1-[2]) + exp(-x^2/(2*[1]^2))*[2]", 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Sigma1", "Sigma2", "Ratio");
	chargeSpreadFunction->SetParameters(1.0, 1.0, 0.5);
}

/// Destructor
RPCSim5Model::~RPCSim5Model() {

}

/**
 * Helper method to pre-calculate normalization. Called by ::setParameter().
 * Needs to override the base method to account for the two individual exponentials that are
 * normalized individually
 */
void RPCSim5Model::init() {
	/// Get the parameters set by the user
	double sigma1 = chargeSpreadFunction->GetParameter("Sigma1");
	double sigma2 = chargeSpreadFunction->GetParameter("Sigma2");
	double ratio = chargeSpreadFunction->GetParameter("Ratio");

	/// Calculate integral of the first Gaussian
	double integral1 = 2 * TMath::Pi() * sigma1 * sigma1 * (1 - TMath::Exp(-0.5 * TMath::Power(maxRadius/sigma1, 2)));

	/// Calculate integral of the second Gaussian
	double integral2 = 2 * TMath::Pi() * sigma2 * sigma2 * (1 - TMath::Exp(-0.5 * TMath::Power(maxRadius/sigma2, 2)));

	/// Replace the model function with a new one that has the updated individual normalization
	char formula[999];
	sprintf(formula, "exp(-x^2 / (2 * [0]^2)) / %f * (1 - [2]) + exp(-x^2 / (2 * [1]^2)) / %f * [2]", integral1, integral2);
	delete chargeSpreadFunction;
	chargeSpreadFunction = new TF1("RPCSim5Model", formula, 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("Sigma1", "Sigma2", "Ratio");
	chargeSpreadFunction->SetParameters(sigma1, sigma2, ratio);
}

/// Helper method to access the list of parameter units
vector<string> RPCSim5Model::getParameterUnits() {
	vector<string> parameterNames;
	parameterNames.push_back("mm");
	parameterNames.push_back("mm");
	parameterNames.push_back("");
	return parameterNames;
}




/// Default constructor
RPCSim6Model::RPCSim6Model() :
		RPCSimChargeSpreadModel("RPCSim6", getParameterUnits()) {
	chargeSpreadFunction = new TF1("RPCSim6Model", "([0] + x^2)^(-3./2.)", 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("a");
	chargeSpreadFunction->SetParameter("a", 1.0);
}

/// Destructor
RPCSim6Model::~RPCSim6Model() {

}

/**
 * Helper method to pre-calculate normalization. Called by ::setParameter().
 */
void RPCSim6Model::init() {
	/// Get the parameters set by the user
	double a = chargeSpreadFunction->GetParameter("a");

	/// Calculate integral of the exponential
	double integral = 2 * TMath::Pi() / TMath::Sqrt(a) * (1. - 1./TMath::Sqrt((1. + maxRadius * maxRadius / a)));

	/// Replace the model function with a new one that has the updated normalization
	char formula[999];
	sprintf(formula, "([0] + x^2)^(-3./2.) / %f", integral);
	delete chargeSpreadFunction;
	chargeSpreadFunction = new TF1("RPCSim6Model", formula, 0.0, maxRadius);
	chargeSpreadFunction->SetParNames("a");
	chargeSpreadFunction->SetParameter(0, a);
}

/// Helper method to access the list of parameter units
vector<string> RPCSim6Model::getParameterUnits() {
	vector<string> parameterNames;
	parameterNames.push_back("");
	return parameterNames;
}


} /* namespace CALICE */
