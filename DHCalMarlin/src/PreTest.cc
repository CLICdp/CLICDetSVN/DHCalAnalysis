#include <PreTest.h>

#include "Exceptions.h"

#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/BitField64.h"
#include "UTIL/CellIDDecoder.h"

#include "streamlog/streamlog.h"
#include <fstream>
#include <fstream>
#include <iostream>
#include "TStyle.h"
#include "TPad.h"



using EVENT::LCCollection;
using EVENT::LCEvent;
using EVENT::CalorimeterHit;
using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;

using std::string;



#include "IMPL/ClusterImpl.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include <fstream>
#include <vector>

//root
#include "TH1D.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include <algorithm>  
using namespace marlin;
using namespace lcio;
using namespace std;
#include "EventVariablesRootWriter.hh"
#include "DhcalUtil.hh"

#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"

#include <cmath>
#include <utility>
#include <sstream>
#include <stdio.h>
#include "DhcalMapping.hh"

using CALICE::DhcalUtil::calculateStandardDeviation;


using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;
using EVENT::Cluster;

using UTIL::CellIDDecoder;
using namespace CALICE;
using std::pair;
using std::map;
using std::sqrt;
using std::string;
using std::vector;

PreTest aPreTest;

PreTest::PreTest() : Processor("PreTest"){
  patternTFile=0;
  registerProcessorParameter("Collection", "The collection to be used", _inputCollection, string(""));
  clearAll();
  
}

PreTest::~PreTest(){}
void PreTest::init(){
  totalNumberOfEvents=0;
  patterHisto = new TH2D("patternHisto","patternHitso",96,0,96,96,0,96);
  for(int i=0;i<54;i++){
    ostringstream oss;
    oss<<i;
    TString s = "histo"+oss.str();
    patternMap.insert(pair<int,TH2D*>(i,(TH2D*)patterHisto->Clone(s)));
  }
}
void PreTest::processRunHeader(LCRunHeader* run) {
  
}



void PreTest::processEvent(LCEvent* event){
 
  totalNumberOfEvents++;
  EVENT::LCCollection* collectionDhc =event->getCollection(_inputCollection);
  double numberOfHits=collectionDhc->getNumberOfElements();
  
   vector<CalorimeterHit*> hits;
   CellIDDecoder<CalorimeterHit> idDecoder(collectionDhc);
   for (int iHit = 0; iHit < collectionDhc->getNumberOfElements(); iHit++){
     hits.push_back(dynamic_cast<CalorimeterHit*>(collectionDhc->getElementAt(iHit))); 
   }
   
   map<int, vector<CalorimeterHit*> > layerHitMap = CALICE::DhcalUtil::buildLayerHitMap(hits,idDecoder);
  
   for(int i=0; i<numberOfHits;i++){
     calohit=(EVENT::CalorimeterHit*)collectionDhc->getElementAt(i);
     int U = idDecoder(calohit)["i"];
     int V = idDecoder(calohit)["j"];
     int LAYER = idDecoder(calohit)["layer"];
     patternMap[LAYER]->Fill(U,V);
     
   } 
}
  
  void PreTest::clearAll(){
  }

void PreTest::end(){
  //cout<<"end function "<<endl;
  DhcalMapping * mapping =DhcalMapping::instance(); 
  //Here the the histograms for each laye is written to a TFile
  patternTFile =new TFile("patternFinder.root","RECREATE");
  ofstream myfile;
  myfile.open ("preTest.txt");   
  map<int,TH2D*>::iterator it;
  int counting = 0;
  //This loop writes to file the histograms and the file with all the cellids
  for(it=patternMap.begin();it!=patternMap.end();++it){
    it->second->GetXaxis()->SetTitle("x-axis");
    it->second->GetYaxis()->SetTitle("y-axis");
    //it->second->SetContour((sizeof(levels)/sizeof(Double_t)), levels);
    //it->second->Rebin2D(8,8);
    it->second->Write(); 
    ostringstream oss;
    oss<<counting;
    TString r = "Result"+oss.str();
    cout<<"detector layer "<<counting<<endl;
    TH2D * results = findHotCells(it->second,r,totalNumberOfEvents);
    //run over all bins in results to find the hot cells
    int binx=results->GetXaxis()->GetNbins();
    int biny=results->GetYaxis()->GetNbins();

    for(int i=1;i<binx+1;i++){
      for(int j=1;j<biny+1;j++){
	if(results->GetBinContent(i,j)==1){ 
  	  myfile<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl; 
	}
      }
    }


    myfile<<mapping->getCellIDFromIndices(0,22,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,23,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,52,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,53,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,55,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,56,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(1,52,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(1,53,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,84,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(0,85,counting)<<endl;  
    myfile<<mapping->getCellIDFromIndices(95,22,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,23,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,52,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,53,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,55,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,56,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(94,52,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(94,53,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,84,counting)<<endl; 
    myfile<<mapping->getCellIDFromIndices(95,85,counting)<<endl; 



    results->Write();
    delete results;
    counting++;
  }
  myfile.close();
  patternTFile->Write();
  patternTFile->Close();








  // cout<<"Total Number Of Events"<<totalNumberOfEvents<<endl;
}

TH2D * PreTest::findHotCells(TH2D * histToBeChecked,TString name, int totalNumberOfEvents){
  
  //cout<<"start "<<endl;
  TH2D * Results = new TH2D(name,name,96,0.5,96.5,96,0.5,96.5);
    
  double chipsInside[144][4] ={{0}};
  double chipsOutside[144][4]={{0}};
  int startx;
  int starty;
  
  for(int q=0;q<144;q++){
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    
    for(int i=startx;i<startx+8;i++){
      chipsInside[q][0]+=histToBeChecked->GetBinContent(i,starty);
      chipsOutside[q][0]+=histToBeChecked->GetBinContent(i,starty-1);
    }
    
    for(int j=starty;j<starty+8;j++){
      chipsInside[q][1]+=histToBeChecked->GetBinContent(startx+7,j);
      chipsOutside[q][1]+=histToBeChecked->GetBinContent(startx+8,j);
    }
    
    for(int i=startx;i<startx+8;i++){
      chipsInside[q][2]+=histToBeChecked->GetBinContent(i,starty+7);
      chipsOutside[q][2]+=histToBeChecked->GetBinContent(i,starty+8);
      
    }
    for(int j=starty;j<starty+8;j++){
      chipsInside[q][3]+=histToBeChecked->GetBinContent(startx,j);
      chipsOutside[q][3]+=histToBeChecked->GetBinContent(startx-1,j);
    }
  }




  double inside;
  double outside;
  int minNumberOfHits=100;

  for(int q=0;q<144;q++){
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    inside =chipsInside[q][0]+chipsInside[q][1]+chipsInside[q][2]+chipsInside[q][3];
    outside =chipsOutside[q][0]+chipsOutside[q][1]+chipsOutside[q][2]+chipsOutside[q][3];
    //To avoid comparing to other FeB which has different numbers of box Events;
    if((q>=0&&q<=11)||(q>=48&&q<=59)||(q>=96&&q<=107)){
      inside=inside-chipsInside[q][0];
      outside=outside-chipsOutside[q][0];
    }
    
    if(q%12==5||q%12==11){
      inside=inside-chipsInside[q][1];
      outside=outside-chipsOutside[q][1];
    }
    if((q>=36&&q<=47)||(q>=84&&q<=95)||(q>=132&&q<=143)){
      inside=inside-chipsInside[q][2];
      outside=outside-chipsOutside[q][2];
    }
    if(q%12==6||q%12==0){
      inside=inside-chipsInside[q][3];
      outside=outside-chipsOutside[q][3];
    }



    if(q==54){
      cout<<"inside "<<inside<<endl;
      cout<<"outside "<<outside<<endl;
    }

    if(1.0*inside/outside>4&&inside>minNumberOfHits&&inside>totalNumberOfEvents*0.0001){
      cout<<"er vi her "<<endl;
      //cout<<startx<<" "<<starty<<endl;
      for(int i=startx;i<startx+8;i++){
	for(int j=starty;j<starty+8;j++){
	  Results->SetBinContent(i,j,1);
	} 
      }
    }
  }

  //We don't want to comapre to other FeB


  return Results;
  //cout<<"end "<<endl;
}



