/*
 * CalorimeterHitComparator.cc
 *
 *  Created on: Mar 4, 2013
 *      Author: Christian Grefe, CERN
 */

#include "CalorimeterHitComparator.hh"

using EVENT::CalorimeterHit;
using UTIL::CellIDDecoder;

using std::vector;

namespace CALICE {

CalorimeterHitComparator::CalorimeterHitComparator() :
		_limit(0.) {
	_comparators.push_back(this);
}

CalorimeterHitComparator::~CalorimeterHitComparator() {
}

bool CalorimeterHitComparator::compare(const CalorimeterHit* hit, const CalorimeterHit* otherHit) const {
	vector<CalorimeterHitComparator*>::const_iterator itComparator = _comparators.begin();
	while (itComparator != _comparators.end()) {
		if (!(*itComparator)->myCompare(hit, otherHit)) {
			return false;
		}
		++itComparator;
	}
	return true;
}

bool CalorimeterHitComparator::consistent(const CalorimeterHit* hit, const CalorimeterHit* otherHit) const {
	vector<CalorimeterHitComparator*>::const_iterator itComparator = _comparators.begin();
	while (itComparator != _comparators.end()) {
		if (!(*itComparator)->myConsistent(hit, otherHit)) {
			return false;
		}
		++itComparator;
	}
	return true;
}

CalorimeterHitComparatorID::CalorimeterHitComparatorID() :
		CalorimeterHitComparator(), _idDecoder(0), _ownsIdDecoder(false) {
	;
}

CalorimeterHitComparatorID::~CalorimeterHitComparatorID() {
	cleanIdDecoder();
}

void CalorimeterHitComparatorID::setIdDecoder(EVENT::LCCollection* collection) {
	cleanIdDecoder();
	_idDecoder = new UTIL::CellIDDecoder<CalorimeterHit>(collection);
	_ownsIdDecoder = true;
}

void CalorimeterHitComparatorID::setIdDecoder(UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder) {
	cleanIdDecoder();
	_idDecoder = &idDecoder;
	_ownsIdDecoder = false;
}

void CalorimeterHitComparatorID::cleanIdDecoder() {
	if (_ownsIdDecoder) {
		delete _idDecoder;
		_idDecoder = 0;
		_ownsIdDecoder = false;
	}
}

CalorimeterHitComparatorLayer::CalorimeterHitComparatorLayer() :
		CalorimeterHitComparatorID(), _layerIdentifier(DhcalMapping::instance()->LAYER_ID()) {
}

CalorimeterHitComparatorLayer::~CalorimeterHitComparatorLayer() {
}

CalorimeterHitComparatorUV::CalorimeterHitComparatorUV() :
		CalorimeterHitComparatorID(), _uIdentifier(DhcalMapping::instance()->U_ID()), _vIdentifier(DhcalMapping::instance()->V_ID()) {
}

CalorimeterHitComparatorUV::~CalorimeterHitComparatorUV() {
}

CalorimeterHitComparatorXY::CalorimeterHitComparatorXY() :
		CalorimeterHitComparator() {
}

CalorimeterHitComparatorXY::~CalorimeterHitComparatorXY() {
}

CalorimeterHitComparatorXYZ::CalorimeterHitComparatorXYZ() :
		CalorimeterHitComparator() {
}

CalorimeterHitComparatorXYZ::~CalorimeterHitComparatorXYZ() {
}

}

