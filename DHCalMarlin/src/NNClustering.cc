/*
 * NNClustering.cc
 *
 *  Created on: Feb 21, 2013
 *      Author: Christian Grefe, CERN
 */

#include "NNClustering.hh"
#include "DhcalMapping.hh"
#include "DhcalUtil.hh"

// c++
#include <cmath>
#include <list>

using EVENT::CalorimeterHit;
using EVENT::Cluster;
using EVENT::LCCollection;
using IMPL::ClusterImpl;
using UTIL::CellIDDecoder;

using std::abs;
using std::list;
using std::map;
using std::make_pair;
using std::string;
using std::vector;

namespace CALICE {

NNClustering::NNClustering() :
		_uIdentifier(DhcalMapping::instance()->U_ID()), _vIdentifier(DhcalMapping::instance()->V_ID()), _layerIdentifier(DhcalMapping::instance()->LAYER_ID()), _energyCut(
				0.) {
	_idDecoder = 0;
}

NNClustering::~NNClustering() {
	if (_idDecoder) {
		delete _idDecoder;
	}
}

void NNClustering::addCollection(const LCCollection* collection) {
	// FIXME make sure that all collections use the same identifier string
	if (_collections.empty()) {
		_idDecoder = new CellIDDecoder<CalorimeterHit>(collection);
	}
	_collections.push_back(collection);
}

vector<Cluster*> NNClustering::findClusters(int uvLimit, int layerLimit) const {
	CalorimeterHitComparatorUV comparatorUV;
	comparatorUV.setIdDecoder(*_idDecoder);
	comparatorUV.setIdentifierU(_uIdentifier);
	comparatorUV.setIdentifierV(_vIdentifier);
	comparatorUV.setLimit(uvLimit);
	CalorimeterHitComparatorLayer comparatorLayer;
	comparatorLayer.setIdDecoder(*_idDecoder);
	comparatorLayer.setIdentifierLayer(_layerIdentifier);
	comparatorLayer.setLimit(layerLimit);
	comparatorUV.addComparator(&comparatorLayer);
	return findClusters(comparatorUV);
}

vector<Cluster*> NNClustering::findClusters(float xyLimit, int layerLimit) const {
	CalorimeterHitComparatorXY comparatorXY;
	comparatorXY.setLimit(xyLimit);
	CalorimeterHitComparatorLayer comparatorLayer;
	comparatorLayer.setIdDecoder(*_idDecoder);
	comparatorLayer.setIdentifierLayer(_layerIdentifier);
	comparatorLayer.setLimit(layerLimit);
	comparatorXY.addComparator(&comparatorLayer);
	return findClusters(comparatorXY);
}

vector<Cluster*> NNClustering::findClusters(float xyzLimit) const {
	CalorimeterHitComparatorXYZ comparatorXYZ;
	comparatorXYZ.setLimit(xyzLimit);
	return findClusters(comparatorXYZ);
}

vector<Cluster*> NNClustering::findClusters(const CalorimeterHitComparator& comparator) const {
	vector<CalorimeterHit*> hits;
	vector<const LCCollection*>::const_iterator itCollection = _collections.begin();
	while (itCollection != _collections.end()) {
		const LCCollection* collection = *itCollection;
		for (int iHit = 0; iHit < collection->getNumberOfElements(); iHit++) {
			hits.push_back(dynamic_cast<CalorimeterHit*>(collection->getElementAt(iHit)));
		}
		++itCollection;
	}
	DhcalUtil::applyEnergyCut(hits, _energyCut);
	return findClusters(hits, comparator);
}

vector<Cluster*> NNClustering::findClusters(const vector<CalorimeterHit*>& hits,
		const CalorimeterHitComparator& comparator) const {
	vector<ClusterImpl*> clusters;
	// create a cluster for every hit
	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		ClusterImpl* cluster = new ClusterImpl();
		cluster->addHit(hit, hit->getEnergy());
		clusters.push_back(cluster);
		++itHit;
	}

	return findClusters(clusters, comparator);
}

vector<Cluster*> NNClustering::findClusters(const vector<Cluster*>& clusters,
		const CalorimeterHitComparator& comparator) const {
	vector<ClusterImpl*> clustersToModify;
	vector<Cluster*>::const_iterator itCluster = clusters.begin();
	while (itCluster != clusters.end()) {
		clustersToModify.push_back(dynamic_cast<ClusterImpl*>(*itCluster));
		++itCluster;
	}
	return findClusters(clustersToModify, comparator);
}

vector<Cluster*> NNClustering::findClusters(const vector<ClusterImpl*>& clusters,
		const CalorimeterHitComparator& comparator) const {
	list<ClusterImpl*> openClusters(clusters.begin(), clusters.end());

	// re-cluster to find connections between clusters
	list<ClusterImpl*>::iterator itOpenCluster = openClusters.begin();
	vector<CalorimeterHit*>::const_iterator itClusterHit;
	vector<CalorimeterHit*>::const_iterator itOtherClusterHit;
	while (itOpenCluster != openClusters.end()) {
		ClusterImpl* openCluster = *itOpenCluster;
		list<ClusterImpl*>::iterator itOtherCluster = openClusters.begin();
		while (itOtherCluster != openClusters.end()) {
			ClusterImpl* otherCluster = *itOtherCluster;
			if (itOpenCluster == itOtherCluster) {
				// avoid checking the same cluster
				++itOtherCluster;
				continue;
			}
			bool clustersMatch = false;
			itClusterHit = openCluster->getCalorimeterHits().begin();
			vector<CalorimeterHit*> otherClusterHits = otherCluster->getCalorimeterHits();
			// check all hits of both clusters for consistency
			while (itClusterHit != openCluster->getCalorimeterHits().end()) {
				CalorimeterHit* clusterHit = *itClusterHit;
				itOtherClusterHit = otherClusterHits.begin();
				while (itOtherClusterHit != otherClusterHits.end()) {
					CalorimeterHit* otherClusterHit = *itOtherClusterHit;
					if (comparator.consistent(clusterHit, otherClusterHit)) {
						clustersMatch = true;
						break;
					}
					itOtherClusterHit++;
				}
				if (clustersMatch == true) {
					break;
				}
				itClusterHit++;
			}
			if (clustersMatch) {
				// merge the two clusters into first and remove the other cluster
				itOtherClusterHit = otherClusterHits.begin();
				while (itOtherClusterHit != otherClusterHits.end()) {
					CalorimeterHit* otherClusterHit = *itOtherClusterHit;
					openCluster->addHit(otherClusterHit, otherClusterHit->getEnergy());
					++itOtherClusterHit;
				}
				openClusters.erase(itOtherCluster++);
				delete otherCluster;
			} else {
				++itOtherCluster;
			}
		}
		itOpenCluster++;
	}

	vector<Cluster*> finalClusters;
	itOpenCluster = openClusters.begin();
	while (itOpenCluster != openClusters.end()) {
		ClusterImpl* cluster = *itOpenCluster;
		DhcalUtil::calculateClusterProperties(cluster);
		finalClusters.push_back(dynamic_cast<Cluster*>(cluster));
		++itOpenCluster;
	}
	return finalClusters;
}


string NNClustering::getCellIdEncoding() {
	string cellIdEncoding = "";
	if (_idDecoder) {
		cellIdEncoding = (*_idDecoder)(0).fieldDescription();
	}
	return cellIdEncoding;
}

} /* namespace CALICE */
