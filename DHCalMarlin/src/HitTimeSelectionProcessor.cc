/*
 * HitTimeSelectionProcessor.cc
 *
 *  Created on: Mar 17, 2013
 *      Author: Christian Grefe, CERN
 */

#include "HitTimeSelectionProcessor.hh"
#include "DhcalUtil.hh"

// lcio
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include "IMPL/LCCollectionVec.h"

// c++
#include <set>

using EVENT::CalorimeterHit;
using EVENT::LCCollection;
using EVENT::LCEvent;
using IMPL::LCCollectionVec;

using std::string;
using std::set;
using lcio::long64;

namespace CALICE {

HitTimeSelectionProcessor aHitTimeSelectionProcessort;

HitTimeSelectionProcessor::HitTimeSelectionProcessor() :
		Processor("HitTimeSelectionProcessor") {
	_description = "Selects calorimeter hits from a calorimeter hit collection based on their hit time.";

	registerInputCollection(LCIO::CALORIMETERHIT, "InputCollection", "", _inputCollectionName, string("DhcHits"));
	registerOutputCollection(LCIO::CALORIMETERHIT, "OutputCollection", "", _outputCollectionName,
			string("SelectedDhcHits"));

	registerProcessorParameter("MinHitTime", "Minimum time of a hit to be kept", _minHitTime, float(-1900.));
	registerProcessorParameter("MaxHitTime", "Maximum time of a hit to be kept", _maxHitTime, float(-1700.));

}

HitTimeSelectionProcessor::~HitTimeSelectionProcessor() {
	;
}

void HitTimeSelectionProcessor::init() {
	printParameters();
}

void HitTimeSelectionProcessor::processEvent(LCEvent* event) {
	LCCollection* inputCollection = event->getCollection(_inputCollectionName);

	LCCollectionVec* outputCollection = new LCCollectionVec(LCIO::CALORIMETERHIT);
	// keep track of the cell ID encoding
	outputCollection->parameters().setValue(LCIO::CellIDEncoding,
			inputCollection->getParameters().getStringVal(LCIO::CellIDEncoding));
	// store the minimum and maximum values used
	outputCollection->parameters().setValue("MinHitTime", _minHitTime);
	outputCollection->parameters().setValue("MaxHitTime", _maxHitTime);
	// selected hits stay with the original collection
	outputCollection->setSubset();

	set<long64> cellIDs;
	for (int iHit = 0; iHit < inputCollection->getNumberOfElements(); iHit++) {
		CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(inputCollection->getElementAt(iHit));
		float hitTime = hit->getTime();
		long64 cellID = DhcalUtil::getCellID(hit);
		// check if hit is within time window and there is no other hit with the same cell ID
		if (hitTime >= _minHitTime and hitTime <= _maxHitTime and cellIDs.find(cellID) == cellIDs.end()) {
			outputCollection->addElement(hit);
			cellIDs.insert(cellID);
		}
	}

	event->addCollection(outputCollection, _outputCollectionName);
}

void HitTimeSelectionProcessor::end() {
	;
}

} /* namespace CALICE */
