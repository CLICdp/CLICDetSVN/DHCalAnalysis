/*
 * DhcalMapping.cpp
 *
 *  Created on: Apr 5, 2013
 *      Author: Christian Grefe, CERN
 */

#include "DhcalMapping.hh"
#include "DhcalUtil.hh"
#include "Exceptions.h"

#include "marlin/VerbosityLevels.h"

#include "streamlog/streamlog.h"

#include <algorithm>
#include <stdexcept>
#include <sstream>

namespace CALICE {

using std::map;
using std::runtime_error;
using std::string;
using std::stringstream;

using EVENT::CalorimeterHit;
using EVENT::Exception;
using UTIL::BitField64;

using DhcalUtil::to_string;

DhcalMapping* DhcalMapping::_instance = 0;

DhcalMapping* DhcalMapping::instance() {
	if (!_instance) {
		_instance = new DhcalMapping();
	}
	return _instance;
}

DhcalMapping::Module::Module(int uMin, int uMax, int vMin, int vMax, double cellSizeU, double cellSizeV,
		TVector3 position) :
		uMin(uMin), uMax(uMax), vMin(vMin), vMax(vMax), cellSizeU(cellSizeU), cellSizeV(cellSizeV), position(position) {
	nCellsU = uMax - uMin + 1;
	nCellsV = vMax - vMin + 1;
	xMin = position.x() - (nCellsU * cellSizeU) / 2.;
	xMax = position.x() + (nCellsU * cellSizeU) / 2.;
	yMin = position.y() - (nCellsV * cellSizeV) / 2.;
	yMax = position.y() + (nCellsV * cellSizeV) / 2.;
}

DhcalMapping::DhcalMapping() {
	calibrationMap = new LayerColumnRowMap<float>;
	efficiencyMap = new LayerColumnRowMap<float>;
	multiplicityMap = new LayerColumnRowMap<float>;

	nLayers = 54;
	nCellsU = 96;
	nCellsV = 96;
	cellSizeU = 10.;
	cellSizeV = 10.;
	idU = "i";
	idV = "j";
	idLayer = "layer";
	tailCatcherStart = 39;

	_idDecoder = new BitField64("i:8,j:8,layer:8");

	// FIXME: read from data base instead of hard coded values
	modulePositionMap[Top] = Module(0, 95, 64, 95, cellSizeU, cellSizeV, TVector3(0., 325.2, 0.));
	modulePositionMap[Middle] = Module(0, 95, 32, 63, cellSizeU, cellSizeV, TVector3(0., 0., 0.));
	modulePositionMap[Bottom] = Module(0, 95, 0, 31, cellSizeU, cellSizeV, TVector3(0., -325.2, 0.));

	// FIXME: read from data base instead of hard coded values
	frontEndBoardPositionMap[TopLeft] = FrontEndBoard(0, 47, 64, 95, cellSizeU, cellSizeV, TVector3(-240., 325.2, 0.));
	frontEndBoardPositionMap[TopRight] = FrontEndBoard(48, 95, 64, 95, cellSizeU, cellSizeV, TVector3(240., 325.2, 0.));
	frontEndBoardPositionMap[MiddleLeft] = FrontEndBoard(0, 47, 32, 63, cellSizeU, cellSizeV, TVector3(-240., 0., 0.));
	frontEndBoardPositionMap[MiddleRight] = FrontEndBoard(48, 95, 32, 63, cellSizeU, cellSizeV, TVector3(240., 0., 0.));
	frontEndBoardPositionMap[BottomLeft] = FrontEndBoard(0, 47, 0, 31, cellSizeU, cellSizeV,
			TVector3(-240., -325.2, 0.));
	frontEndBoardPositionMap[BottomRight] = FrontEndBoard(48, 95, 0, 31, cellSizeU, cellSizeV,
			TVector3(240., -325.2, 0.));

	// FIXME: read from data base instead of hard coded values
	layerPositionMap[0] = TVector3(0., 0., 0);
	layerPositionMap[1] = TVector3(0., 0., 27);
	layerPositionMap[2] = TVector3(0., 0., 54);
	layerPositionMap[3] = TVector3(0., 0., 81);
	layerPositionMap[4] = TVector3(0., 0., 108);
	layerPositionMap[5] = TVector3(0., 0., 135);
	layerPositionMap[6] = TVector3(0., 0., 162);
	layerPositionMap[7] = TVector3(0., 0., 189);
	layerPositionMap[8] = TVector3(0., 0., 216);
	layerPositionMap[9] = TVector3(0., 0., 243);
	layerPositionMap[10] = TVector3(0., 0., 270);
	layerPositionMap[11] = TVector3(0., 0., 297);
	layerPositionMap[12] = TVector3(0., 0., 324);
	layerPositionMap[13] = TVector3(0., 0., 351);
	layerPositionMap[14] = TVector3(0., 0., 378);
	layerPositionMap[15] = TVector3(0., 0., 405);
	layerPositionMap[16] = TVector3(0., 0., 432);
	layerPositionMap[17] = TVector3(0., 0., 459);
	layerPositionMap[18] = TVector3(0., 0., 486);
	layerPositionMap[19] = TVector3(0., 0., 513);
	layerPositionMap[20] = TVector3(0., 0., 540);
	layerPositionMap[21] = TVector3(0., 0., 567);
	layerPositionMap[22] = TVector3(0., 0., 594);
	layerPositionMap[23] = TVector3(0., 0., 621);
	layerPositionMap[24] = TVector3(0., 0., 648);
	layerPositionMap[25] = TVector3(0., 0., 675);
	layerPositionMap[26] = TVector3(0., 0., 702);
	layerPositionMap[27] = TVector3(0., 0., 729);
	layerPositionMap[28] = TVector3(0., 0., 756);
	layerPositionMap[29] = TVector3(0., 0., 774.3);
	layerPositionMap[30] = TVector3(0., 0., 810);
	layerPositionMap[31] = TVector3(0., 0., 837);
	layerPositionMap[32] = TVector3(0., 0., 864);
	layerPositionMap[33] = TVector3(0., 0., 891);
	layerPositionMap[34] = TVector3(0., 0., 918);
	layerPositionMap[35] = TVector3(0., 0., 945);
	layerPositionMap[36] = TVector3(0., 0., 972);
	layerPositionMap[37] = TVector3(0., 0., 999);
	layerPositionMap[38] = TVector3(0., 0., 1026);
	layerPositionMap[39] = TVector3(0., 0., 1261);
	layerPositionMap[40] = TVector3(0., 0., 1314);
	layerPositionMap[41] = TVector3(0., 0., 1367);
	layerPositionMap[42] = TVector3(0., 0., 1420);
	layerPositionMap[43] = TVector3(0., 0., 1473);
	layerPositionMap[44] = TVector3(0., 0., 1526);
	layerPositionMap[45] = TVector3(0., 0., 1579);
	layerPositionMap[46] = TVector3(0., 0., 1632);
	layerPositionMap[47] = TVector3(0., 0., 1762);
	layerPositionMap[48] = TVector3(0., 0., 1892);
	layerPositionMap[49] = TVector3(0., 0., 2022);
	layerPositionMap[50] = TVector3(0., 0., 2152);
	layerPositionMap[51] = TVector3(0., 0., 2282);
	layerPositionMap[52] = TVector3(0., 0., 2412);
	layerPositionMap[53] = TVector3(0., 0., 2542);

	map<int, TVector3>::const_iterator itLayerPositions = layerPositionMap.begin();
	while (itLayerPositions != layerPositionMap.end()) {
		layerZPositions.push_back(itLayerPositions->second.z());
		++itLayerPositions;
	}
}

DhcalMapping::~DhcalMapping() {
	if (_idDecoder) {
		delete _idDecoder;
	}

	delete calibrationMap;
	delete efficiencyMap;
	delete multiplicityMap;
}

bool DhcalMapping::validPosition(const TVector3& position) const {
	try {
		getModuleFromPosition(position);
	} catch (Exception& e) {
		return false;
	}
	return true;
}

bool DhcalMapping::validCellID(const CellID& cellID) const {
	try {
		getModuleFromCellID(cellID);
	} catch (Exception& e) {
		return false;
	}
	return true;
}

CellID DhcalMapping::getCellIDFromIndices(int u, int v, int layer) const {
	try {
		_idDecoder->reset();
		(*_idDecoder)[idU] = u;
		(*_idDecoder)[idV] = v;
		(*_idDecoder)[idLayer] = layer;
		return _idDecoder->getValue();
	} catch (Exception& e) {
		throw Exception(
				string("Invalid indices in DhcalMapping::getCellIDFromIndices: ") + idU + "=" + to_string(u) + ", "
						+ idV + "=" + to_string(v) + ", " + idLayer + "=" + to_string(layer));
	}
}

CellID DhcalMapping::getCellIDFromPosition(const TVector3& position) const {
	try {
		const Module& module = getModuleFromPosition(position);
		int u = (int) ((position.x() - module.xMin) / module.cellSizeU) + module.uMin;
		if (u > module.uMax) {
			if (position.x() == module.xMax) {
				u -= 1;
			} else {
				throw Exception("");
			}
		}
		int v = (int) ((position.y() - module.yMin) / module.cellSizeV) + module.vMin;
		if (v > module.vMax) {
			if (position.y() == module.yMax) {
				v -= 1;
			} else {
				throw Exception("");
			}
		}
		int layer = getClosestLayerID(position.z());
		return getCellIDFromIndices(u, v, layer);
	} catch (Exception& e) {
		throw Exception(
				string("Invalid position in DhcalMapping::getCellIDFromPosition: x=") + to_string(position.x()) + ", y="
						+ to_string(position.y()) + ", z=" + to_string(position.z()));
	}
}

int DhcalMapping::getClosestLayerID(double zPosition) const {
	return (int) (std::lower_bound(layerZPositions.begin(), layerZPositions.end(), zPosition) - layerZPositions.begin());
}

TVector3 DhcalMapping::getPositionFromCellID(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	int vIndex = (*_idDecoder)[idV].value();
	int uIndex = (*_idDecoder)[idU].value();
	int layerIndex = (*_idDecoder)[idLayer].value();
	try {
		const Module& module = getModuleFromCellID(cellID);
		TVector3 positionOnModule((uIndex - module.uMin - (module.nCellsU - 1.) / 2.) * module.cellSizeU,
				(vIndex - module.vMin - (module.nCellsV - 1.) / 2.) * module.cellSizeV, 0.);
		return positionOnModule + module.position + getLayerPosition(layerIndex);
	} catch (std::exception& e) {
		throw Exception(
				string("Invalid cell ID in DhcalMapping::getPositionFromCellID: ") + idU + "=" + to_string(uIndex)
						+ ", " + idV + "=" + to_string(vIndex) + ", " + idLayer + "=" + to_string(layerIndex));
	}
}

TVector3 DhcalMapping::getPositionFromIndices(int u, int v, int layer) const {
	return getPositionFromCellID(getCellIDFromIndices(u, v, layer));
}

int DhcalMapping::getLayerNumberFromPosition(const TVector3& position) const {
	return getLayerNumberFromCellID(getCellIDFromPosition(position));
}

int DhcalMapping::getLayerNumberFromCellID(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	return (*_idDecoder)[idLayer].value();
}

ModulePosition DhcalMapping::getModulePositionFromCellID(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	int vIndex = (*_idDecoder)[idV].value();
	int uIndex = (*_idDecoder)[idU].value();
	map<ModulePosition, Module>::const_iterator it;
	for (it = modulePositionMap.begin(); it != modulePositionMap.end(); ++it) {
		const ModulePosition& position = it->first;
		const Module& module = it->second;
		if (uIndex >= module.uMin && uIndex <= module.uMax && vIndex >= module.vMin && vIndex <= module.vMax) {
			return position;
		}
	}
	throw Exception(
			string("Invalid cell ID in DhcalMapping::getModulePositionFromCellID: ") + idU + "=" + to_string(uIndex)
					+ ", " + idV + "=" + to_string(vIndex));
}

ModulePosition DhcalMapping::getModulePositionFromPosition(const TVector3& position) const {
	map<ModulePosition, Module>::const_iterator it;
	for (it = modulePositionMap.begin(); it != modulePositionMap.end(); ++it) {
		const ModulePosition& modulePosition = it->first;
		const Module& module = it->second;
		if (position.x() >= module.xMin && position.x() <= module.xMax && position.y() >= module.yMin
				&& position.y() <= module.yMax) {
			return modulePosition;
		}
	}
	return kModulePositions;
}

const DhcalMapping::Module& DhcalMapping::getModuleFromCellID(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	int vIndex = (*_idDecoder)[idV].value();
	int uIndex = (*_idDecoder)[idU].value();
	map<ModulePosition, Module>::const_iterator it;
	for (it = modulePositionMap.begin(); it != modulePositionMap.end(); ++it) {
		const Module& module = it->second;
		if (uIndex >= module.uMin && uIndex <= module.uMax && vIndex >= module.vMin && vIndex <= module.vMax) {
			return module;
		}
	}
	throw Exception(
			string("Invalid cell ID in DhcalMapping::getModuleFromCellID: ") + idU + "=" + to_string(uIndex) + ", "
					+ idV + "=" + to_string(vIndex));
}

const DhcalMapping::Module& DhcalMapping::getModuleFromPosition(const TVector3& position) const {
	map<ModulePosition, Module>::const_iterator it;
	for (it = modulePositionMap.begin(); it != modulePositionMap.end(); ++it) {
		const Module& module = it->second;
		if (position.x() >= module.xMin && position.x() <= module.xMax && position.y() >= module.yMin
				&& position.y() <= module.yMax) {
			return module;
		}
	}
	throw Exception(
			string("Invalid position in DhcalMapping::getModuleFromPosition: x=") + to_string(position.x()) + ", y="
					+ to_string(position.y()) + ", z=" + to_string(position.z()));
}

bool DhcalMapping::isModuleBoundary(const TVector3& position, int margin) const {
	return isModuleBoundary(getCellIDFromPosition(position), margin);
}

bool DhcalMapping::isModuleBoundary(const CellID& cellID, int margin) const {
	_idDecoder->setValue(cellID);
	int vIndex = (*_idDecoder)[idV].value();
	int uIndex = (*_idDecoder)[idU].value();
	try {
		const Module& module = getModuleFromCellID(cellID);
		if (uIndex >= module.uMax - margin || uIndex <= module.uMin + margin || vIndex >= module.vMax - margin
				|| vIndex <= module.vMin + margin) {
			return true;
		}
	} catch (Exception& e) {
		// nothing to do
	}
	return false;
}

bool DhcalMapping::isFrontEndBoardBoundary(const TVector3& position, int margin) const {
	return isFrontEndBoardBoundary(getCellIDFromPosition(position), margin);
}

TVector3 DhcalMapping::distanceToModuleBoundary(const TVector3& position) const {
	const Module& module = getModuleFromPosition(position);
	double dx =
			(position.x() - module.xMin < module.xMax - position.x()) ?
					position.x() - module.xMin : module.xMax - position.x();
	double dy =
			(position.y() - module.yMin < module.yMax - position.y()) ?
					position.y() - module.yMin : module.yMax - position.y();
	return TVector3(dx, dy, 0.);
}

TVector3 DhcalMapping::distanceToModuleBoundary(const CellID& cellID) const {
	return distanceToModuleBoundary(getPositionFromCellID(cellID));
}

bool DhcalMapping::isFrontEndBoardBoundary(const CellID& cellID, int margin) const {
	_idDecoder->setValue(cellID);
	int vIndex = (*_idDecoder)[V_ID()].value();
	int uIndex = (*_idDecoder)[U_ID()].value();
	map<FrontEndBoardPosition, FrontEndBoard>::const_iterator it;
	for (it = frontEndBoardPositionMap.begin(); it != frontEndBoardPositionMap.end(); ++it) {
		const FrontEndBoard& module = it->second;
		if (uIndex == module.uMax || uIndex == module.uMin || vIndex == module.vMax || vIndex == module.vMin) {
			return true;
		}
	}
	return false;
}

const TVector3& DhcalMapping::getLayerPosition(int layerIndex) const {
	return layerPositionMap.at(layerIndex);
}

// Check if the given cell ID is in the tail catcher
bool DhcalMapping::isTailCatcher(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	int layerIndex = (*_idDecoder)[LAYER_ID()].value();
	if (layerIndex >= tailCatcherStart) {
		return true;
	}
	return false;
}

// Check if the given position is in the tail catcher
bool DhcalMapping::isTailCatcher(const TVector3& position) const {
	isTailCatcher(getCellIDFromPosition(position));
}

bool DhcalMapping::isIgnoredCell(const CellID& cellID) const {
	if (ignoredCellIDs.find(cellID) != ignoredCellIDs.end()) {
		return true;
	} else {
		return false;
	}
}

bool DhcalMapping::isIgnoredCell(const TVector3& position) const {
	return isIgnoredCell(getCellIDFromPosition(position));
}

float DhcalMapping::getCalibration(const CellID& cellID) const {
	if (calibrationMap->contains(cellID)) {
		return calibrationMap->get(cellID);
	} else {
		return getModuleCalibration(getIndexLayer(cellID), getModulePositionFromCellID(cellID));
	}
}

float DhcalMapping::getCalibration(const TVector3& position) const {
	return getCalibration(getCellIDFromPosition(position));
}

float DhcalMapping::getModuleCalibration(int layerID, const ModulePosition& modulePosition) const {
	map<int, map<ModulePosition, float> >::const_iterator itLayer = moduleCalibrationMap.find(layerID);
	if (itLayer == moduleCalibrationMap.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID;
		throw Exception(msg.str());
	}
	map<ModulePosition, float>::const_iterator itModule = itLayer->second.find(modulePosition);
	if (itModule == itLayer->second.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID << ", module ID: " << modulePosition;
		throw Exception(msg.str());
	}
	return itModule->second;
}

float DhcalMapping::getEfficiency(const CellID& cellID) const {
	if (efficiencyMap->contains(cellID)) {
		return efficiencyMap->get(cellID);
	} else {
		return getModuleEfficiency(getIndexLayer(cellID), getModulePositionFromCellID(cellID));
	}
}

float DhcalMapping::getEfficiency(const TVector3& position) const {
	return getEfficiency(getCellIDFromPosition(position));
}

float DhcalMapping::getModuleEfficiency(int layerID, const ModulePosition& modulePosition) const {
	map<int, map<ModulePosition, float> >::const_iterator itLayer = moduleEfficiencyMap.find(layerID);
	if (itLayer == moduleEfficiencyMap.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID;
		throw Exception(msg.str());
	}
	map<ModulePosition, float>::const_iterator itModule = itLayer->second.find(modulePosition);
	if (itModule == itLayer->second.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID << ", module ID: " << modulePosition;
		throw Exception(msg.str());
	}
	return itModule->second;
}

float DhcalMapping::getMultiplicity(const CellID& cellID) const {
	if (multiplicityMap->contains(cellID)) {
		return multiplicityMap->get(cellID);
	} else {
		return getModuleMultiplicity(getIndexLayer(cellID), getModulePositionFromCellID(cellID));
	}
}

float DhcalMapping::getMultiplicity(const TVector3& position) const {
	return getMultiplicity(getCellIDFromPosition(position));
}

float DhcalMapping::getModuleMultiplicity(int layerID, const ModulePosition& modulePosition) const {
	map<int, map<ModulePosition, float> >::const_iterator itLayer = moduleMultiplicityMap.find(layerID);
	if (itLayer == moduleMultiplicityMap.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID;
		throw Exception(msg.str());
	}
	map<ModulePosition, float>::const_iterator itModule = itLayer->second.find(modulePosition);
	if (itModule == itLayer->second.end()) {
		stringstream msg;
		msg << "No calibration for layer ID: " << layerID << ", module ID: " << modulePosition;
		throw Exception(msg.str());
	}
	return itModule->second;
}

// direct access to id fields
int DhcalMapping::getIndexU(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	return (*_idDecoder)[idU];
}

int DhcalMapping::getIndexV(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	return (*_idDecoder)[idV];
}

int DhcalMapping::getIndexLayer(const CellID& cellID) const {
	_idDecoder->setValue(cellID);
	return (*_idDecoder)[idLayer];
}

void DhcalMapping::getIndices(const CellID& cellID, int& u, int& v, int& layer) const {
	_idDecoder->setValue(cellID);
	u = (*_idDecoder)[idU];
	v = (*_idDecoder)[idV];
	layer = (*_idDecoder)[idLayer];
}

void DhcalMapping::setIdEncoding(const std::string& encoding) {
	if (_idDecoder) {
		delete _idDecoder;
	}
	_idDecoder = new BitField64(encoding);
}

} /* namespace CALICE */
