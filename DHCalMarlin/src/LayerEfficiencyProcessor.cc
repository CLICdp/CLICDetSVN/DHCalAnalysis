/*
 * LayerEfficiencyProcessor.cc
 *
 *  Created on: Feb 28, 2013
 *      Author: Christian Grefe, CERN
 */

#include "LayerEfficiencyProcessor.hh"
#include "DhcalUtil.hh"
#include "StraightLine3D.hh"

// lcio
#include "EVENT/TrackerHit.h"
#include "IMPL/TrackerHitImpl.h"
#include "UTIL/CellIDDecoder.h"

// logging
#include "streamlog/streamlog.h"

// root
#include "TFile.h"
#include "TGraphAsymmErrors.h"
#include "TMath.h"

// c++
#include <algorithm>
#include <limits>
#include <sstream>
#include <vector>

using marlin::Processor;

using EVENT::Cluster;
using EVENT::LCEvent;
using EVENT::LCRunHeader;
using EVENT::TrackerHit;
using IMPL::TrackerHitImpl;
using UTIL::CellIDDecoder;

using std::map;
using std::pair;
using std::make_pair;
using std::sort;
using std::string;
using std::stringstream;
using std::vector;

namespace CALICE {

LayerEfficiencyProcessor aLayerEfficiencyProcessor;

LayerEfficiencyProcessor::LayerEfficiencyProcessor() :
		Processor("LayerEfficiencyProcessor") {
	_description = "";
	_mapping = DhcalMapping::instance();

	registerInputCollection(LCIO::CLUSTER, "LayerClusterCollection", "", _layerClusterCollection, string(""));

	registerProcessorParameter("IdentifierU", "Name of the id identifier for U", _uIdentifier, _mapping->U_ID());
	registerProcessorParameter("IdentifierV", "Name of the id identifier for V", _vIdentifier, _mapping->V_ID());
	registerProcessorParameter("IdentifierLayer", "Name of the id identifier for layer", _layerIdentifier,
			_mapping->LAYER_ID());

	registerProcessorParameter("MinLayer", "", _minLayer, 0);
	registerProcessorParameter("MaxLayer", "", _maxLayer, _mapping->N_LAYERS() - 1);
	registerProcessorParameter("MaxClusterSize", "", _maxClusterSize, 3);
	registerProcessorParameter("MaxTotalHits", "", _maxTotalHits, 150);
	registerProcessorParameter("MinActiveLayers", "", _minActiveLayers, 30);
	registerProcessorParameter("MaxChiSquare", "", _maxChisq, 5.);
	registerProcessorParameter("MaxDistance", "", _maxDistance, 20.);
	registerProcessorParameter("HistogramMaxClusterSize", "", _histogramMaxClusterSize, 50);
	registerProcessorParameter("NLayerNeighbours",
			"Number of neighbouring layers used to determine the layer efficiency", _nLayerNeighbours, 4);
	registerProcessorParameter("MinLayerNeighbourMIPs",
			"Minimum number of neighbouring layers that have a MIP-like cluster", _minLayerNeighbourMIPs,
			_nLayerNeighbours);
	registerProcessorParameter("RootFileName", "Name of the ROOT output file", _rootFileName,
			string("layerClusterSize.root"));
}

LayerEfficiencyProcessor::~LayerEfficiencyProcessor() {
	map<int, TH3F*>::iterator itMap = layerClusterSizeHistogramMapPositions.begin();
	while (itMap != layerClusterSizeHistogramMapPositions.end()) {
		delete itMap->second;
		++itMap;
	}
	layerClusterSizeHistogramMapPositions.clear();

	itMap = layerClusterSizeHistogramMapCellIDs.begin();
	while (itMap != layerClusterSizeHistogramMapCellIDs.end()) {
		delete itMap->second;
		++itMap;
	}
	layerClusterSizeHistogramMapCellIDs.clear();

	map<string, TH1*>::iterator itMapTH1 = debugHistograms.begin();
	while (itMapTH1 != debugHistograms.end()) {
		delete itMapTH1->second;
		++itMapTH1;
	}
	debugHistograms.clear();
}

void LayerEfficiencyProcessor::init() {
	printParameters();
	// sanity check
	if (_minLayer > _maxLayer) {
		std::swap(_minLayer, _maxLayer);
	}
	if (_maxLayer - _minLayer + 1 <= _nLayerNeighbours) {
		streamlog_out( WARNING )<< "Not enough active layers to extract layer efficiency." << std::endl;
	}
	for (int iLayer = _minLayer; iLayer <= _maxLayer; iLayer++) {
		layersToCheckMap.insert(make_pair(iLayer, findLayersToCheck(iLayer)));
		stringstream histogramName;
		histogramName << "ClusterSize_vs_Position_Layer" << iLayer;
		layerClusterSizeHistogramMapPositions.insert(
				make_pair(iLayer,
						new TH3F(histogramName.str().c_str(),
								(histogramName.str() + ";x [mm];y [mm];Cluster Size").c_str(), 200, -500., 500, 200,
								-500., 500, _histogramMaxClusterSize, -0.5, _histogramMaxClusterSize - 0.5)));
		histogramName.str("");
		histogramName.clear();
		histogramName << "ClusterSize_vs_Position_Cluster_Layer" << iLayer;
		layerClusterSizeHistogramMapPositionsCluster.insert(
				make_pair(iLayer,
						new TH3F(histogramName.str().c_str(),
								(histogramName.str() + ";x [mm];y [mm];Cluster Size").c_str(), 200, -500., 500, 200,
								-500., 500, _histogramMaxClusterSize, -0.5, _histogramMaxClusterSize - 0.5)));

		histogramName.str("");
		histogramName.clear();
		histogramName << "ClusterSize_vs_CellID_Layer" << iLayer;
		layerClusterSizeHistogramMapCellIDs.insert(
				make_pair(iLayer,
						new TH3F(histogramName.str().c_str(),
								(histogramName.str() + ";" + _mapping->U_ID() + ";" + _mapping->V_ID() + ";Cluster Size").c_str(),
								_mapping->N_CELLS_U(), -0.5, _mapping->N_CELLS_U() - 0.5, _mapping->N_CELLS_V(), -0.5,
								_mapping->N_CELLS_V() - 0.5, _histogramMaxClusterSize, -0.5,
								_histogramMaxClusterSize - 0.5)));
		histogramName.str("");
		histogramName.clear();
		histogramName << "ClusterSize_vs_CellID_Cluster_Layer" << iLayer;
		layerClusterSizeHistogramMapCellIDsCluster.insert(
				make_pair(iLayer,
						new TH3F(histogramName.str().c_str(),
								(histogramName.str() + ";" + _mapping->U_ID() + ";" + _mapping->V_ID() + ";Cluster Size").c_str(),
								_mapping->N_CELLS_U(), -0.5, _mapping->N_CELLS_U() - 0.5, _mapping->N_CELLS_V(), -0.5,
								_mapping->N_CELLS_V() - 0.5, _histogramMaxClusterSize, -0.5,
								_histogramMaxClusterSize - 0.5)));
	}

	debugHistograms["TrackResidualX"] = new TH1D("TrackResidualX", "Track Residual X;#Deltax [mm];Entries", 200, -100,
			100.);
	debugHistograms["TrackResidualY"] = new TH1D("TrackResidualY", "Track Residual Y;#Deltay [mm];Entries", 200, -100,
			100.);
	debugHistograms["TrackChi2"] = new TH1D("TrackChi2", "Track #chi^{2};#chi^{2};Entries", 500, 0., 50.);
	debugHistograms["TrackAngle"] = new TH1D("TrackAngle", "Track Angle;#theta_{track} [rad];Entries", 200, -0.5, 0.5);
}

void LayerEfficiencyProcessor::processRunHeader(LCRunHeader* run) {

}

void LayerEfficiencyProcessor::processEvent(LCEvent* event) {
	LCCollection* clusterCollection = event->getCollection(_layerClusterCollection);

	int totalHits = 0;
	for (int iCluster = 0; iCluster < clusterCollection->getNumberOfElements(); iCluster++) {
		totalHits += dynamic_cast<Cluster*>(clusterCollection->getElementAt(iCluster))->getCalorimeterHits().size();
	}

	if (totalHits > _maxTotalHits) {
		return;
	}

	map<int, vector<Cluster*> > layerClusterMap = DhcalUtil::buildLayerClusterMap(clusterCollection, _layerIdentifier);
	if ((int) layerClusterMap.size() < _minActiveLayers) {
		return;
	}

	for (int iLayer = _minLayer; iLayer <= _maxLayer; iLayer++) {
		const vector<Cluster*>& layerClusters = layerClusterMap[iLayer];
		double zRef = _mapping->getLayerPosition(iLayer).z();
		vector<Cluster*>::const_iterator itLayerCluster;
		vector<int>& layersToCheck = layersToCheckMap[iLayer];
		vector<vector<Cluster*> > mipStubCandidates;
		vector<vector<Cluster*> >::iterator itMipStubCandidate;
		vector<int>::iterator itLayerToCheck = layersToCheck.begin();
		for (itLayerToCheck = layersToCheck.begin(); itLayerToCheck != layersToCheck.end(); ++itLayerToCheck) {
			vector<Cluster*> checkLayerClusters = layerClusterMap[*itLayerToCheck];
			DhcalUtil::selectMaxHitsClusters(checkLayerClusters, _maxClusterSize);
			vector<vector<Cluster*> > myMipStubCandidates;
			while (!checkLayerClusters.empty()) {
				Cluster* layerCluster = checkLayerClusters.back();
				checkLayerClusters.pop_back();
				if (mipStubCandidates.empty()) {
					vector<Cluster*> mipStubCandidate;
					mipStubCandidate.push_back(layerCluster);
					myMipStubCandidates.push_back(mipStubCandidate);
				} else {
					itMipStubCandidate = mipStubCandidates.begin();
					for (; itMipStubCandidate != mipStubCandidates.end(); ++itMipStubCandidate) {
						vector<Cluster*> mipStubCandidate = *itMipStubCandidate;
						mipStubCandidate.push_back(layerCluster);
						myMipStubCandidates.push_back(mipStubCandidate);
					}
				}
			}
			mipStubCandidates.insert(mipStubCandidates.end(), myMipStubCandidates.begin(), myMipStubCandidates.end());
		}
		// start with longest candidate
		sort(mipStubCandidates.begin(), mipStubCandidates.end(), sortMipStubCandidates);
		itMipStubCandidate = mipStubCandidates.begin();
		for (; itMipStubCandidate != mipStubCandidates.end(); ++itMipStubCandidate) {
			vector<Cluster*> mipStubCandidate = *itMipStubCandidate;
			if ((int) mipStubCandidate.size() >= _minLayerNeighbourMIPs) {
				// sufficient of the reference layers have a MIP like cluster
				Track* mipTrack = DhcalUtil::straightLineTrackFit(mipStubCandidate);
				debugHistograms["TrackChi2"]->Fill(mipTrack->getChi2() / float(mipTrack->getNdf()));
				if (mipTrack->getChi2() / float(mipTrack->getNdf()) < _maxChisq) {
					StraightLine3D mipLine(mipTrack);
					debugHistograms["TrackAngle"]->Fill(
							mipLine.direction().Theta() * mipLine.direction().x()
									/ TMath::Abs(mipLine.direction().x()));
					TVector3 mipLayerIntersection = mipLine.getPositionAtZ(zRef);
					// track fit is good enough
					int clusterSize = 0;
					double minDistance = std::numeric_limits<double>::max();
					TVector3 clusterPosition;
					for (itLayerCluster = layerClusters.begin(); itLayerCluster != layerClusters.end();
							++itLayerCluster) {
						const Cluster* cluster = *itLayerCluster;
						clusterPosition = cluster->getPosition();
						TVector3 deltaPosition(clusterPosition - mipLayerIntersection);
						double distance = deltaPosition.Mag();
						debugHistograms["TrackResidualX"]->Fill(deltaPosition.x());
						debugHistograms["TrackResidualY"]->Fill(deltaPosition.y());
						if (distance < _maxDistance) {
							// only count the cluster closest to the intersection point
							if (distance < minDistance) {
								minDistance = distance;
								clusterSize = cluster->getCalorimeterHits().size();
							}
						}
					}
					ModulePosition modulePosition = _mapping->getModulePositionFromPosition(mipLayerIntersection);
					// check if the position is valid
					if (modulePosition != kModulePositions) {
						try {
							long64 cellID = _mapping->getCellIDFromPosition(mipLayerIntersection);
							long64 clusterCellID = cellID;
							if (clusterSize > 0) {
								long64 clusterCellID = _mapping->getCellIDFromPosition(clusterPosition);
							}
							layerClusterSizeHistogramMapCellIDs[iLayer]->Fill(_mapping->getIndexU(cellID),
									_mapping->getIndexV(cellID), clusterSize);
							layerClusterSizeHistogramMapCellIDsCluster[iLayer]->Fill(_mapping->getIndexU(clusterCellID),
									_mapping->getIndexV(clusterCellID), clusterSize);
							layerClusterSizeHistogramMapPositions[iLayer]->Fill(mipLayerIntersection.x(),
									mipLayerIntersection.y(), clusterSize);
							layerClusterSizeHistogramMapPositionsCluster[iLayer]->Fill(clusterPosition.x(),
									clusterPosition.y(), clusterSize);
							delete mipTrack;
							// we found a good track stub, only allow one per event
							break;
						} catch (Exception& e) {
							// nothing to do
						}
					}
				}
				delete mipTrack;
			}
		}
	}
}

void LayerEfficiencyProcessor::end() {
	TFile* rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
	map<int, TH3F*>::iterator itMap = layerClusterSizeHistogramMapPositions.begin();
	while (itMap != layerClusterSizeHistogramMapPositions.end()) {
		itMap->second->Write();
		++itMap;
	}

	itMap = layerClusterSizeHistogramMapPositionsCluster.begin();
	while (itMap != layerClusterSizeHistogramMapPositionsCluster.end()) {
		itMap->second->Write();
		++itMap;
	}

	itMap = layerClusterSizeHistogramMapCellIDs.begin();
	while (itMap != layerClusterSizeHistogramMapCellIDs.end()) {
		itMap->second->Write();
		++itMap;
	}

	itMap = layerClusterSizeHistogramMapCellIDsCluster.begin();
	while (itMap != layerClusterSizeHistogramMapCellIDsCluster.end()) {
		itMap->second->Write();
		++itMap;
	}

	map<string, TH1*>::iterator itTH1Map = debugHistograms.begin();
	while (itTH1Map != debugHistograms.end()) {
		itTH1Map->second->Write();
		++itTH1Map;
	}

	rootFile->Close();
	delete rootFile;
}

vector<int> LayerEfficiencyProcessor::findLayersToCheck(int layer) const {
	vector<int> layersToCheck;
	int layersBefore = _nLayerNeighbours / 2;
	int layersAfter = _nLayerNeighbours - layersBefore;
	// try to find sufficient neighboring layers for the efficiency extraction
	if (layer - layersBefore < _minLayer) {
		layersBefore = layer - _minLayer;
		layersAfter = _nLayerNeighbours - layersBefore;
	}
	if (layer + layersAfter > _maxLayer) {
		layersAfter = _maxLayer - layer;
		layersBefore = _nLayerNeighbours - layersAfter;
	}
	for (int deltaLayer = 1; deltaLayer <= layersBefore; deltaLayer++) {
		layersToCheck.push_back(layer - deltaLayer);
	}
	for (int deltaLayer = 1; deltaLayer <= layersAfter; deltaLayer++) {
		layersToCheck.push_back(layer + deltaLayer);
	}
	return layersToCheck;
}

} /* namespace CALICE */

