/*
 * StraightLine3D.cc
 *
 *  Created on: Mar 17, 2013
 *      Author: Christian Grefe, CERN
 */

#include "StraightLine3D.hh"

#include "TMath.h"

#include <cmath>
#include <limits>

using EVENT::Track;

namespace CALICE {

StraightLine3D::StraightLine3D(const TVector3& origin, double phi, double theta) :
		_origin(origin) {
	_direction = TVector3(0.,0.,1.);
	_direction.SetPhi(phi);
	_direction.SetTheta(theta);
	_direction = _direction.Unit();
}

StraightLine3D::StraightLine3D(const TVector3& origin, const TVector3& direction) :
		_origin(origin), _direction(direction.Unit()) {
}

StraightLine3D::StraightLine3D(const EVENT::Track* track) {
	_origin = TVector3(track->getReferencePoint());
	_direction = TVector3(1.,0.,0.);
	_direction.SetPhi(track->getPhi());
	_direction.SetTheta(TMath::PiOver2() - std::atan(track->getTanLambda()));
	_direction = _direction.Unit();
}

StraightLine3D::~StraightLine3D() {
}

double StraightLine3D::getDistanceOfClosestApproach(const TVector3& point) {
	TVector3 originToPoint(point - _origin);
	double lengthOfClosestApproach = originToPoint.Dot(_direction);
	return std::sqrt(originToPoint.Mag2() - lengthOfClosestApproach * lengthOfClosestApproach);
}

double StraightLine3D::getLengthToPointOfClosestApproach(const TVector3& point) {
	TVector3 originToPoint(point - _origin);
	// direction is a unit vector
	// originToPoint.Dot(_direction) / _direction.Mag2();
	return originToPoint.Dot(_direction);
}

TVector3 StraightLine3D::getPointOfClosestApproach(const TVector3& point) {
	return getPointAtLength(getLengthToPointOfClosestApproach(point));
}

TVector3 StraightLine3D::getPointAtLength(double length) {
	return _origin + length * _direction;
}

double StraightLine3D::getLengthToIntersectionWithPlane(const TVector3& planeOrigin, const TVector3& planeNormal) {
	TVector3 displacement = planeOrigin - _origin;
	double numerator = displacement.Dot(planeNormal);
	double denominator = _direction.Dot(planeNormal);
	if (denominator == 0.) {
		// line is parallel to plane
		return std::numeric_limits<double>::infinity();
	}
	// TODO: treat case where line lies in plane, e.g. 0 / 0
	return numerator / denominator;
}

TVector3 StraightLine3D::getIntersectionWithPlane(const TVector3& planeOrigin, const TVector3& planeNormal) {
	return getPointAtLength(getLengthToIntersectionWithPlane(planeOrigin, planeNormal));
}

TVector3 StraightLine3D::getPositionAtZ(double z) {
	return getIntersectionWithPlane(TVector3(0.,0.,z), TVector3(0.,0.,1.));
}

} /* namespace CALICE */
