#include "RPCSimProcessor.hh"

#include "EVENT/LCCollection.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/SimCalorimeterHit.h"
#include "EVENT/LCGenericObject.h"
#include "UTIL/CellIDDecoder.h"
#include "UTIL/CellIDEncoder.h"
#include "marlin/Exceptions.h"

#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/LCEventImpl.h"
#include "IMPL/LCRunHeaderImpl.h"
#include "IMPL/TrackerDataImpl.h"
#include "IMPL/TrackerHitImpl.h"
#include "IO/LCWriter.h"
#include "IOIMPL/LCFactory.h"
#include "UTIL/BitSet32.h"

#include "TROOT.h"
#include "TRandom3.h"
#include "TVector3.h"

#include <cmath>

/*
 // the famous 6 parameters
 #define _SLOPE1    0.0678  // _SLOPE1 of exponential charge distribution;    default = 0.0678
 #define _SLOPE2    0.671   // _SLOPE2 of exponential charge distribution;    default = 0.671
 #define _RATIO     0.345   // _RATIO of contribution from _SLOPE1 and _SLOPE2; default = 0.345
 #define T         0.3645  // Threshold (in pC);                            default = 0.3645
 #define DIST_CUT  0.262   // 0.262   // Minimum distance between hits in x,y plane;   default = 0.092
 #define Q0        0.201   // offset in charge; [pC]                        default = 0.201
 */

using namespace marlin;
using namespace lcio;
using namespace std;

namespace CALICE {

RPCSimProcessor aRPCSimProcessor;

/**********************************************************************/
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
RPCSimProcessor::RPCSimProcessor() :
		Processor("RPCSimProcessor") {
	_description = "Processor for RPC Digitisation for WDHCal";
	_mapping = DhcalMapping::instance();

	registerProcessorParameter("HcalCollectionName", "Name of input HCAL collection name", _hcalColName,
			std::string("hcalSD"));

	registerProcessorParameter("RootFileName", "Name of ROOT output file name", _rootFileName,
			std::string("rpcSim.root"));

	registerOptionalParameter("rootFileMode", "Mode for opening ROOT file", _rootFileMode, std::string("RECREATE"));

	registerProcessorParameter("DigitCollectionName", "Output collection name", _digitColName, std::string("DigiHits"));

	//w Adjustable Parameters through XML ADD UNITS TO THINGS
	registerProcessorParameter("Slope1", "Slope 1 used in simulation", _SLOPE1, (float) 0.0678);
	registerProcessorParameter("Slope2", "Slope 2 used in simulation", _SLOPE2, (float) 0.671);
	registerProcessorParameter("Ratio", "Ratio used in simulation", _RATIO, (float) 0.345);
	registerProcessorParameter("T", "T used in simulation", _T, (float) 0.3645);
	registerProcessorParameter("DistCut", "Distance Cut used in simulation", _DIST_CUT, (float) 0.092);
	registerProcessorParameter("Q0", "Signal Charge used in simulation (pC)", _Q0, (float) 0.201);
	registerProcessorParameter("MaxRadius", "Maximum radius of the extent of the charge (cm)", _MAXRADIUS, (float) 4.0);
}

/**********************************************************************/
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
RPCSimProcessor::~RPCSimProcessor() {
}

/**********************************************************************/
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
void RPCSimProcessor::init() {
	/*
	 ROOT stuff
	 */

	_hNContrib = new TH1F("nHits", "nHits", 21, 0, 21);

	_hHitLayer = new TH1F("hitLayer", "hitLayer", 40, 0, 40);

	_hHitMapXY = new TH2F("hitMapXY", "hitMapXY", 500, -500, 500, 500, -500, 500);
	_hHitMapIJ = new TH2F("hitMapIJ", "hitMapIJ", 100, 0, 100, 100, 0, 100);

	_hContribMapXY = new TH2F("ContribMapXY", "ContribMapXY", 100, -50, 50, 100, -50, 50);

	_pHitsPerLayer = new TProfile("avgHitsPerLayer", "avgHitsPerLayer", 40, 0, 40);

	_hContribDist = new TH1F("contribDist", "contribDist", 1000, 0, 2);

	_hDigiHitsMap = new TH2F("DigiHitsMap", "DigiHitsMap", 100, 0, 100, 100, 0, 100);

	_hNhits = new TH1F("nDigiHits", "nDigiHits", 300, 0, 300);

	_hCloudRadius = new TH1F("cloudRadius", "cloudRadius", 10000, 0, 10);

	_hContTime = new TH1F("contTime", "contTime", 100, 0, 100);

	_hPadCharge = new TH1F("padCharge", "padCharge", 500, 0, 1);

	_hR2 = new TH1F("r2", "r2", 500, 0, 35);

	_hCharge = new TH1F("Q", "Q", 90, 0, 15);

	_qintegral1 = new TH1F("qint1", "qint1", 500, 0, 1);
	_qintegral2 = new TH1F("qint2", "qint2", 500, 0, 1);
	_qintegral = new TH1F("qint", "qint", 100, 0, 1);
	_XYdep = new TH2F("xydep", "xydep", 200, -10, 120, 200, -10, 120);
	_radius = new TH1F("radius", "radius", 50, 0, 5);
	_nHits2d = new TH2F("nHits2d", "nHits2d", 96, 0, 96, 96, 0, 96);
	_qPadDeposit = new TH2F("qPadDeposit", "qPadDeposit", 96, 0, 96, 96, 0, 96);
	_qPadDepositProfile = new TProfile2D("qPadDepositProfile", "qPadDepositProfile", 96, 0, 96, 96, 0, 96);
	_CellHitLocations = new TH2F("CellHitLocations", "CellHitLocations", 500, 0, 1, 500, 0, 1);
	_CellHitLocations3D = new TH3F("CellHitLocations3d", "CellHitLocations3d", 500, 0, 1, 500, 0, 1, 9, 0, 9);
	_FirstLastCellLocation = new TH2F("FirstLastCellLocation", "FirstLastCellLocation", 500, -1, 1, 500, -1, 1);
	_HitContDif = new TH2F("HitContDif", "Difference between Hit cell and location of E Deposit in cell", 40, -2, 2, 40,
			-2, 2);
	_DigiHitVsMokkaCell = new TH2F("DigiHitVsMokkaCell", "Difference between Mokka and Output Cells", 20, -10, 10, 20,
			-10, 10);
	_DigiHitVsMokkaPos = new TH2F("DigiHitVsMokkaPos", "Difference between Mokka and Output Position", 200, -100, 100,
			200, -100, 100);

	_Layer0Hits = new TH2F("Layer0Hits", "Layer0Hits", 96, 0, 96, 96, 0, 96);
	_Layer1Hits = new TH2F("Layer1Hits", "Layer1Hits", 96, 0, 96, 96, 0, 96);
	_Layer2Hits = new TH2F("Layer2Hits", "Layer2Hits", 96, 0, 96, 96, 0, 96);
	_Layer3Hits = new TH2F("Layer3Hits", "Layer3Hits", 96, 0, 96, 96, 0, 96);
	_Layer4Hits = new TH2F("Layer4Hits", "Layer4Hits", 96, 0, 96, 96, 0, 96);
	_Layer5Hits = new TH2F("Layer5Hits", "Layer5Hits", 96, 0, 96, 96, 0, 96);
	_Layer6Hits = new TH2F("Layer6Hits", "Layer6Hits", 96, 0, 96, 96, 0, 96);
	_Layer7Hits = new TH2F("Layer7Hits", "Layer7Hits", 96, 0, 96, 96, 0, 96);
	_Layer8Hits = new TH2F("Layer8Hits", "Layer8Hits", 96, 0, 96, 96, 0, 96);
	_Layer9Hits = new TH2F("Layer9Hits", "Layer9Hits", 96, 0, 96, 96, 0, 96);
	_Layer10Hits = new TH2F("Layer10Hits", "Layer10Hits", 96, 0, 96, 96, 0, 96);
	_Layer11Hits = new TH2F("Layer11Hits", "Layer11Hits", 96, 0, 96, 96, 0, 96);
	_Layer12Hits = new TH2F("Layer12Hits", "Layer12Hits", 96, 0, 96, 96, 0, 96);
	_Layer13Hits = new TH2F("Layer13Hits", "Layer13Hits", 96, 0, 96, 96, 0, 96);
	_Layer14Hits = new TH2F("Layer14Hits", "Layer14Hits", 96, 0, 96, 96, 0, 96);
	_Layer15Hits = new TH2F("Layer15Hits", "Layer15Hits", 96, 0, 96, 96, 0, 96);
	_Layer16Hits = new TH2F("Layer16Hits", "Layer16Hits", 96, 0, 96, 96, 0, 96);
	_Layer17Hits = new TH2F("Layer17Hits", "Layer17Hits", 96, 0, 96, 96, 0, 96);
	_Layer18Hits = new TH2F("Layer18Hits", "Layer18Hits", 96, 0, 96, 96, 0, 96);
	_Layer19Hits = new TH2F("Layer19Hits", "Layer19Hits", 96, 0, 96, 96, 0, 96);
	_Layer20Hits = new TH2F("Layer20Hits", "Layer20Hits", 96, 0, 96, 96, 0, 96);
	_Layer21Hits = new TH2F("Layer21Hits", "Layer21Hits", 96, 0, 96, 96, 0, 96);
	_Layer22Hits = new TH2F("Layer22Hits", "Layer22Hits", 96, 0, 96, 96, 0, 96);
	_Layer23Hits = new TH2F("Layer23Hits", "Layer23Hits", 96, 0, 96, 96, 0, 96);
	_Layer24Hits = new TH2F("Layer24Hits", "Layer24Hits", 96, 0, 96, 96, 0, 96);
	_Layer25Hits = new TH2F("Layer25Hits", "Layer25Hits", 96, 0, 96, 96, 0, 96);
	_Layer26Hits = new TH2F("Layer26Hits", "Layer26Hits", 96, 0, 96, 96, 0, 96);
	_Layer27Hits = new TH2F("Layer27Hits", "Layer27Hits", 96, 0, 96, 96, 0, 96);
	_Layer28Hits = new TH2F("Layer28Hits", "Layer28Hits", 96, 0, 96, 96, 0, 96);
	_Layer29Hits = new TH2F("Layer29Hits", "Layer29Hits", 96, 0, 96, 96, 0, 96);
	_Layer30Hits = new TH2F("Layer30Hits", "Layer30Hits", 96, 0, 96, 96, 0, 96);
	_Layer31Hits = new TH2F("Layer31Hits", "Layer31Hits", 96, 0, 96, 96, 0, 96);
	_Layer32Hits = new TH2F("Layer32Hits", "Layer32Hits", 96, 0, 96, 96, 0, 96);
	_Layer33Hits = new TH2F("Layer33Hits", "Layer33Hits", 96, 0, 96, 96, 0, 96);
	_Layer34Hits = new TH2F("Layer34Hits", "Layer34Hits", 96, 0, 96, 96, 0, 96);
	_Layer35Hits = new TH2F("Layer35Hits", "Layer35Hits", 96, 0, 96, 96, 0, 96);
	_Layer36Hits = new TH2F("Layer36Hits", "Layer36Hits", 96, 0, 96, 96, 0, 96);
	_Layer37Hits = new TH2F("Layer37Hits", "Layer37Hits", 96, 0, 96, 96, 0, 96);
	_Layer38Hits = new TH2F("Layer38Hits", "Layer38Hits", 96, 0, 96, 96, 0, 96);
	LayerHitMap[0] = _Layer0Hits;
	LayerHitMap[1] = _Layer1Hits;
	LayerHitMap[2] = _Layer2Hits;
	LayerHitMap[3] = _Layer3Hits;
	LayerHitMap[4] = _Layer4Hits;
	LayerHitMap[5] = _Layer5Hits;
	LayerHitMap[6] = _Layer6Hits;
	LayerHitMap[7] = _Layer7Hits;
	LayerHitMap[8] = _Layer8Hits;
	LayerHitMap[9] = _Layer9Hits;
	LayerHitMap[10] = _Layer10Hits;
	LayerHitMap[11] = _Layer11Hits;
	LayerHitMap[12] = _Layer12Hits;
	LayerHitMap[13] = _Layer13Hits;
	LayerHitMap[14] = _Layer14Hits;
	LayerHitMap[15] = _Layer15Hits;
	LayerHitMap[16] = _Layer16Hits;
	LayerHitMap[17] = _Layer17Hits;
	LayerHitMap[18] = _Layer18Hits;
	LayerHitMap[19] = _Layer19Hits;
	LayerHitMap[20] = _Layer20Hits;
	LayerHitMap[21] = _Layer21Hits;
	LayerHitMap[22] = _Layer22Hits;
	LayerHitMap[23] = _Layer23Hits;
	LayerHitMap[24] = _Layer24Hits;
	LayerHitMap[25] = _Layer25Hits;
	LayerHitMap[26] = _Layer26Hits;
	LayerHitMap[27] = _Layer27Hits;
	LayerHitMap[28] = _Layer28Hits;
	LayerHitMap[29] = _Layer29Hits;
	LayerHitMap[30] = _Layer30Hits;
	LayerHitMap[31] = _Layer31Hits;
	LayerHitMap[32] = _Layer32Hits;
	LayerHitMap[33] = _Layer33Hits;
	LayerHitMap[34] = _Layer34Hits;
	LayerHitMap[35] = _Layer35Hits;
	LayerHitMap[36] = _Layer36Hits;
	LayerHitMap[37] = _Layer37Hits;
	LayerHitMap[38] = _Layer38Hits;

}

/**********************************************************************/
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
void RPCSimProcessor::processEvent(LCEvent* evt) {
	int evtNumber = evt->getEventNumber();
	m_out(DEBUG)<< "Event " << evtNumber << endl;

	LCCollection *col = NULL;
	// Output collection
	LCCollectionVec* digiHits = new LCCollectionVec(EVENT::LCIO::CALORIMETERHIT);
	digiHits->setFlag(UTIL::set_bit(digiHits->getFlag(), EVENT::LCIO::RCHBIT_LONG));  // store position with hits
	digiHits->setFlag(UTIL::set_bit(digiHits->getFlag(), EVENT::LCIO::RCHBIT_TIME));  // store time with hits

	try {
		col = evt->getCollection(_hcalColName);
	}/*end of try */
	catch (lcio::DataNotAvailableException& err) {
		//evt->addCollection(digiHits, "DigiHits");
		/*skip this event in case there is no HCAL collection in it*/
		throw SkipEventException(this);
		m_out(DEBUG)<<" Collection "<<_hcalColName<<" not available in event "<<evtNumber<<endl;
	}

	CellIDDecoder<SimCalorimeterHit> cellIDDecoder(col);
	int nEntries = col->getNumberOfElements();
	//m_out(DEBUG) << "nHits " << nEntries << endl;

	//------------------------------------------------------------------------------
	// Fill map
	//------------------------------------------------------------------------------
	map<int, vector<vector<float> > > mHitsPerLayer; //w Number of monte carlo energy depositions per layer structured
	map<int, vector<vector<int> > > MokkaCellsPerLayer;
	map<int, vector<vector<float> > > MokkaCellsPerLayerPosition;
	for (int i = 0; i < nEntries; i++) {
		SimCalorimeterHit *caloHit = dynamic_cast<SimCalorimeterHit*>(col->getElementAt(i));

		int hit_I = cellIDDecoder(caloHit)["I"] - 1;  //w THIS WAS CHANGED AUG 20th, to start everything at zero
		int hit_J = cellIDDecoder(caloHit)["J"] - 1;
		int layer = cellIDDecoder(caloHit)["K"] - 1; // start layer count at 0
		_hHitLayer->Fill(layer); //w This will be filled with the layer location of the energy deposition or "hit", shows where most particles shower
		_hHitMapIJ->Fill(hit_I, hit_J); //w This is the cell ID in i/j of the actual energy deposition (goes from 0 to 100), shows the beam profile until it showers

		vector<int> cell;
		cell.push_back(hit_I);
		cell.push_back(hit_J);
		MokkaCellsPerLayer[layer].push_back(cell);
		const float *position = caloHit->getPosition();
		float hit_x = position[0];
		float hit_y = position[1];
		float hit_z = position[2];
		_hHitMapXY->Fill(hit_x, hit_y); //w This is filled with the position of the energy deposition in X and Y (-500mm to 500mm, bin size is 10 X 10 mm^2)

		vector<float> layerposition;
		layerposition.push_back(hit_x);
		layerposition.push_back(hit_y);
		MokkaCellsPerLayerPosition[layer].push_back(layerposition);
		//m_out(DEBUG) << layer << "\t" << hit_x << "\t" << hit_y << "\t" << hit_z << endl;

		int nContrib = caloHit->getNMCContributions(); //w Calo hit is composed of many monte carlo contributions, which are these
		for (int k = 0; k < nContrib; k++) {
			const float *contPos = caloHit->getStepPosition(k); //w location of energy deposition (step) that occurred in the simhit
			float cont_x = contPos[0] / 10.; //w mm->cm
			float cont_y = contPos[1] / 10.;

			float cont_t = caloHit->getTimeCont(k); //w time that the energy deposition actually happened
			_hContTime->Fill(cont_t); //w probably starts at 35ish b/c thats how long it takes the particles to get to the DHCAL, maybe in nanoseconds? 10/3E8 = 3.4E-8 s = 34 ns?

			//w WILL CHANGED THIS, was 'floor(whatever)'
			int ContCell_X = (int) floor(cont_x) + 48;
			int ContCell_Y = (int) floor(cont_y) + 48;
			_HitContDif->Fill(ContCell_X - hit_I, ContCell_Y - hit_J);

			float tmpPosArr[4] = { cont_x, cont_y, cont_t, hit_z }; //,hit_x,hit_y}; //w Assumes if energy is deposited in one layer, doesn't consider different locations in Z i.e. cont_z
			vector<float> tmpPosVec(&tmpPosArr[0], &tmpPosArr[0] + 4); //w Fills the vector with the array

			mHitsPerLayer[layer].push_back(tmpPosVec); //Adds the hit contribution to the layer

			_hContribMapXY->Fill(cont_x, cont_y);
			_pHitsPerLayer->Fill(layer, nContrib);
		}
		_hNContrib->Fill(nContrib);

	}
	//________________________________________________________________________________

	//------------------------------------------------------------------------------
	// Distance cut
	//------------------------------------------------------------------------------
	map<int, vector<vector<float> > >::iterator itMap;
	for (itMap = mHitsPerLayer.begin(); itMap != mHitsPerLayer.end(); itMap++)  //w Iterates through layers
			{
		//int layer = itMap->first;
		vector<vector<float> > &posVec = (itMap->second);

		//m_out(DEBUG) << "layer " << layer  << endl;
		//m_out(DEBUG) << "  posVecSize before " << posVec.size() << endl;

		for (unsigned int pos1 = 0; pos1 < posVec.size(); pos1++) //w Iterates through all the hits in the layer
				{
			vector<float> &itVec1 = (posVec[pos1]); //w Makes a vector of the X and Y positions found at position 1
			float x1 = itVec1[0];
			float y1 = itVec1[1];

			//m_out(DEBUG) << x1 << "\t" << y1 << endl;

			for (unsigned int pos2 = pos1 + 1; pos2 < posVec.size(); pos2++) //w Looks at all hits that happen after position1 in the layer list
					{
				vector<float> &itVec2 = (posVec[pos2]);
				float x2 = itVec2[0];
				float y2 = itVec2[1];

				float dist = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2));
				/*w Compares the distance between position 1 with all other positions in the layermap and finds the distance between the two in X and Y
				 * This algorithm covers all bases because position 1 will only need to be compared to the ones after it because
				 * it will have already been compared to those before it from its structure
				 */
				_hContribDist->Fill(dist);
				if (dist < _DIST_CUT) //w If the distance is less than the distance cut we choose, throws out the one at position 2
						{
					posVec.erase(posVec.begin() + pos2);
					pos2--;
				}

			}
		}

		//m_out(DEBUG) << "  posVecSize after  " << posVec.size() << endl;

		//_pHitsPerLayer->Fill(layer,posVec.size());
	}
	//________________________________________________________________________________

	double par[3];
	par[0] = 0.00001;
	par[1] = 2.167;
	par[2] = 0.0008;
	// data par /0.00057,1.735,0.0012/  ! 6.2 kV
	// data par /0.00007,1.951,0.0010/  ! 6.3 kV
	// data par /0.00001,2.167,0.0008/  ! 6.4 kV
	// units are cm

	int n_int = 10000;    // Number of points in distributed charge
	//double radius_max = 3.0; //0.001; // 4.0; // Maximum extent of charge, default = 3.0, unit is cm

	double x_crack[2], x_scaler, y_crack[4], y_scaler;
	y_crack[0] = 0.005;											//CHANGE/CHECK THESE
	y_crack[1] = 32.525;
	y_crack[2] = 65.065;
	y_crack[3] = 97.595;											// Cracks in y

	TRandom3 r;
	r.SetSeed(time(NULL));

	//w This is the area that the signal charge is generated in?

	double q_integral1 = 6.283 * pow(_SLOPE1, 2) * (1. - exp(-1. * _MAXRADIUS / _SLOPE1) * (1. + _MAXRADIUS / _SLOPE1));
	double q_integral2 = 6.283 * pow(_SLOPE2, 2) * (1. - exp(-1. * _MAXRADIUS / _SLOPE2) * (1. + _MAXRADIUS / _SLOPE2));

	//w 6.283 = 2pi, these are both the sum of the signal charges produced by the points

	// initialize arrays
	int hit[_mapping->N_CELLS_U()][_mapping->N_CELLS_V()][_mapping->N_LAYERS()];
	double q_pad[_mapping->N_CELLS_U()][_mapping->N_CELLS_V()][_mapping->N_LAYERS()];
	float Time[_mapping->N_CELLS_U()][_mapping->N_CELLS_V()][_mapping->N_LAYERS()];
	float zPos[_mapping->N_CELLS_U()][_mapping->N_CELLS_V()][_mapping->N_LAYERS()];
	vector<vector<vector<vector<TVector3> > > > CellEnergyDepositLocations; //cell x id, cell y id, cell z id, amount of energy deposits, location in cell
	CellEnergyDepositLocations.resize(_mapping->N_CELLS_U());
	for (unsigned int i = 0; i < _mapping->N_CELLS_U(); i++) {
		CellEnergyDepositLocations[i].resize(_mapping->N_CELLS_V());
		for (unsigned int j = 0; j < _mapping->N_CELLS_V(); j++) {
			CellEnergyDepositLocations[i][j].resize(_mapping->N_LAYERS());
			for (unsigned int k = 0; k < _mapping->N_LAYERS(); k++) {
				hit[i][j][k] = 0;
				q_pad[i][j][k] = 0.;
				Time[i][j][k] = 0.;
				zPos[i][j][k] = 0.;

			}
		}
	}

	//ADDED

	for (unsigned int i = 0; i < mHitsPerLayer[0].size(); i++) {
		for (unsigned int j = 0; j < mHitsPerLayer[_mapping->N_LAYERS()].size(); j++) {
			_FirstLastCellLocation->Fill(mHitsPerLayer[0][i][0] - mHitsPerLayer[_mapping->N_LAYERS()][j][0],
					mHitsPerLayer[0][i][1] - mHitsPerLayer[_mapping->N_LAYERS()][j][1]);
		}
	}

	for (itMap = mHitsPerLayer.begin(); itMap != mHitsPerLayer.end(); itMap++) {
		unsigned int layer = (unsigned int) itMap->first;
		vector<vector<float> > posVec = (itMap->second);
		for (unsigned int pos = 0; pos < posVec.size(); pos++) //w Goes through all the places where energy was deposited in a layer
				{

			vector<float> itVec = (posVec[pos]);
			float x = itVec[0]; //w Finds the location of the energy deposit in x and y, the time it happened, and the z location of the hit
			float y = itVec[1];
			TVector3 CellHitLocation(x - floor(x), y - floor(y), 0);
			_CellHitLocations->Fill(x - floor(x), y - floor(y));

			if (floor(x) + 48 >= _mapping->N_CELLS_U() or floor(x) + 48 < 0 or floor(y) + 48 >= _mapping->N_CELLS_V() or floor(y) + 48 < 0) {
				cout << "ERROR: X-Cell: " << floor(x) + 48 << " - Y-Cell: " << floor(y) + 48
						<< " is not a valid cell ID" << endl;
				CellEnergyDepositLocations[48][48][layer].push_back(CellHitLocation);

			} else {
				CellEnergyDepositLocations[(int) floor(x) + 48][(int) floor(y) + 48][layer].push_back(CellHitLocation);
			}

			float time = itVec[2];
			float hit_z = itVec[3];

			// determine minimum distance to boundaries in x and y
			x_crack[0] = x + 48.0;
			x_crack[1] = 96.0 - (x + 48.0);
			x_scaler = (x_crack[0] < x_crack[1] ? x_crack[0] : x_crack[1]); //w Shortest distance to the crack between pads
			y_scaler = 999.;
			for (int i = 0; i < 4; i++) {
				double d = abs(y - y_crack[i] + 48.8);
				if (d <= y_scaler) {
					y_scaler = d;
				}
			}

			// generate charge: Distributed like RABBIT data
			//w This is the total charge that gets made by the point
			double Q = 0.;
			double r1, r2, r3;
			r1 = 0.;
			r2 = 0.;
			r3 = 0.;
			while (r2 <= r3) {
				r1 = r.Rndm() * 12000.;
				r2 = par[0] * pow(r1, par[1]) * exp(-r1 * par[2]);
				r3 = r.Rndm() * 40.;
				Q = r1 / 1100.;  //w Maxes out at about 3, dies off by 10

				_hR2->Fill(r2);

			}
			_hCharge->Fill(Q); //w Mean is currently 3.5 pC

			// Introduce offset
			Q = Q - _Q0;  //w Q made by both muons and pions is the same

			// Adjust charge close to boundary

			if (x_scaler < 10.0)
				Q = Q * pow(x_scaler / 10.0, 0.7);
			if (y_scaler < 10.0)
				Q = Q * pow(y_scaler / 10.0, 0.25);
			if (y_scaler < 1.0)
				Q = Q / 6.;

			double x_int = 0.0;
			double y_int = 0.0;
			double x_deposit, y_deposit;
			double QQQ;
			double qint1, qint2;
			double q_int;
			double rmax, radius;
			int i_x = -1;
			int i_y = -1;
			// Distribute charge according to STAR measurements - integrate with n_int points
			for (int i_int = 0; i_int < n_int; i_int++) {
				radius = 1000.;
				while (radius > _MAXRADIUS) //w Makes sure that the radius taken is inside the area of max radius
				{
					x_int = 2. * _MAXRADIUS * (r.Rndm() - .5);
					y_int = 2. * _MAXRADIUS * (r.Rndm() - .5);
					radius = sqrt(pow(x_int, 2) + pow(y_int, 2));

				}

				rmax = 3.142 * pow(_MAXRADIUS, 2); //w Total area of max radius charge distribution

				_radius->Fill(radius);  //w Evenly distributed between 0 and radius_max mostly, low at 0
				// calculate charge
				QQQ = Q / ((double) (n_int));
				qint1 = exp(-radius / _SLOPE1) * (1. - _RATIO) * (QQQ / q_integral1) * rmax; //w First to die off
				qint2 = exp(-radius / _SLOPE2) * (_RATIO) * (QQQ / q_integral2) * rmax; //w Lasts longest
				_qintegral1->Fill(qint1);
				_qintegral2->Fill(qint2);
				q_int = qint1 + qint2;
				//_qintegral->Fill(q_int);

				// deposit charge on corresponding pad
				x_deposit = x + x_int + 48.0; // x of deposited charge in cm, centered onto plane, x is energy deposit loc, x_int is spread, 48, centers it
				y_deposit = y + y_int + 48.8; // y of deposited charge in cm, centered onto plane
				_XYdep->Fill(x_deposit, y_deposit);

				if (x_deposit < 0.0 || x_deposit > 96.0) {
					q_int = 0.;
				}

				i_x = ((int) (floor(x_deposit))); // pad index in x

				//m_out(DEBUG) << x_deposit << "\t" << y_deposit << endl;

				if (y_deposit < 0.27)
					q_int = 0.;
				else if (y_deposit > 32.27 && y_deposit < 32.8)
					q_int = 0.;
				else if (y_deposit > 64.8 && y_deposit < 65.33)
					q_int = 0.;
				else if (y_deposit > 97.33)
					q_int = 0.;
				else if (y_deposit < 32.27)
					i_y = ((int) (floor(y_deposit - 0.27)));
				else if (y_deposit < 64.80)
					i_y = ((int) (floor(y_deposit - 0.8)));
				else if (y_deposit < 97.33)
					i_y = ((int) (floor(y_deposit - 1.33)));
				else
					i_y = -1;

				//m_out(DEBUG) << i_x << "\t" << i_y << endl;

				if (i_x >= 0 && i_x < _mapping->N_CELLS_U() && i_y >= 0 && i_y < _mapping->N_CELLS_V() && layer < _mapping->N_LAYERS()) {
					_hCloudRadius->Fill(radius);

					q_pad[i_x][i_y][layer] += q_int;

					Time[i_x][i_y][layer] = time;
					zPos[i_x][i_y][layer] = hit_z;
				}
			}
		}
	}

	// determine pads hit
	int nthits = 0;
	for (unsigned int i = 0; i < _mapping->N_CELLS_U(); i++) {
		for (unsigned int j = 0; j < _mapping->N_CELLS_V(); j++) {
			for (unsigned int k = 0; k < _mapping->N_LAYERS(); k++) {
				_hPadCharge->Fill(q_pad[i][j][k]);
				_qPadDeposit->Fill(i, j, q_pad[i][j][k]);
				_qPadDepositProfile->Fill(i, j, q_pad[i][j][k]);
				if (q_pad[i][j][k] > _T) //Draw q_Pad
						{
					hit[i][j][k] = 1;
					nthits++;

					for (unsigned int g = 0; g < MokkaCellsPerLayer[k].size(); g++) {
						int CellDeltaX = MokkaCellsPerLayer[k][g][0] - i;
						int CellDeltaY = MokkaCellsPerLayer[k][g][1] - j;
						_DigiHitVsMokkaCell->Fill(CellDeltaX, CellDeltaY);
					}

					LayerHitMap[k]->Fill(i, j);
					_nHits2d->Fill(i, j);
				}
			}
		}
	}

	m_out(DEBUG)<< "nHits\t" << nthits << endl;

	_hNhits->Fill(nthits);

	for (int i = 1; i < 94; i++) {
		for (int j = 1; j < 94; j++) {
			for (unsigned int k = 0; k < _mapping->N_LAYERS(); k++) {
				int NeighborCount = 0;
				NeighborCount = (hit[i - 1][j + 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i - 1][j][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i - 1][j - 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i][j + 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i][j - 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i + 1][j + 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i + 1][j][k] == 1 ? NeighborCount + 1 : NeighborCount);
				NeighborCount = (hit[i + 1][j - 1][k] == 1 ? NeighborCount + 1 : NeighborCount);
				vector<TVector3>::iterator itCellDep;
				for (itCellDep = CellEnergyDepositLocations[i][j][k].begin();
						itCellDep != CellEnergyDepositLocations[i][j][k].end(); ++itCellDep) {
					_CellHitLocations3D->Fill((*itCellDep).x(), (*itCellDep).y(), NeighborCount);
				}
			}
		}
	}

	// ID Encoder string
	UTIL::CellIDEncoder<CalorimeterHitImpl> idEncoder("i:8,j:8,layer:8", digiHits);

	// write out hits
	if (nthits > 0) {
		for (unsigned int k = 0; k < _mapping->N_LAYERS(); k++) {
			int N = 0;
			for (unsigned int j = 0; j < _mapping->N_CELLS_V(); j++) {
				for (unsigned int i = 0; i < _mapping->N_CELLS_U(); i++) {
					if (hit[i][j][k] == 1) {

						//m_out(DEBUG) << "zPos " << zPos[i][j][k] << " time  " << Time[i][j][k] << endl;

						_hDigiHitsMap->Fill(i, j);
						N++;

						CalorimeterHitImpl* digiHit = new CalorimeterHitImpl();
						float position[3] = { (float) (i * 10. - 475.), (float) (j * 10. - 475.), //THIS WAS CHANGED AUG 20th
								(zPos[i][j][k]) }; // put caloHit z-position

						for (unsigned int g = 0; g < MokkaCellsPerLayerPosition[k].size(); g++) {
							int CellDeltaX = MokkaCellsPerLayerPosition[k][g][0] - position[0];
							int CellDeltaY = MokkaCellsPerLayerPosition[k][g][1] - position[1];
							_DigiHitVsMokkaPos->Fill(CellDeltaX, CellDeltaY);
						}

						digiHit->setPosition(position);
						digiHit->setTime(Time[i][j][k]); // put first contribution time

						idEncoder.reset();
						idEncoder["i"] = i;
						idEncoder["j"] = j;
						idEncoder["layer"] = k;
						idEncoder.setCellID(digiHit);

						digiHits->addElement(digiHit);

					}
				}
			}
			_pHitsPerLayer->Fill(k, N);
		}

		evt->addCollection(digiHits, "DigiHits");

	}
}

/**********************************************************************/
/*                                                                    */
/*                                                                    */
/*                                                                    */
/**********************************************************************/
/*
 bool EfficientHit(int layer){
 float efficiency = 0.9;//change this
 TRandom3 r;
 r.SetSeed(time(NULL));
 bool hasHit=(r.Rndm()<efficiency ? true : false);
 return hasHit;
 }
 */

void RPCSimProcessor::end() {

	_rootFile = gROOT->GetFile(_rootFileName.c_str());
	if (_rootFile == NULL) {
		_rootFile = new TFile(_rootFileName.c_str(), _rootFileMode.c_str());
	}

	/*--------------------------------------------------------*/
	_rootFile->mkdir("hcal");
	_rootFile->cd("hcal");

	_hHitLayer->GetXaxis()->SetTitle("Layer Number");
	_hHitLayer->GetYaxis()->SetTitle("Events");
	_hHitLayer->Write();
	_hHitMapXY->Write();
	_hHitMapIJ->Write();
	_hContribMapXY->Write();
	_pHitsPerLayer->GetXaxis()->SetTitle("Layer Number");
	_pHitsPerLayer->GetYaxis()->SetTitle("Total Hits");
	_pHitsPerLayer->Write();
	_hNContrib->Write();
	_hContribDist->Write();
	_hDigiHitsMap->Write();
	_hNhits->Write();
	_hCloudRadius->Write();
	_hContTime->Write();
	_hPadCharge->Write();
	_hR2->Write();
	_hCharge->Write();
	_qintegral->Write();
	_qintegral1->Write();
	_qintegral2->Write();
	_XYdep->Write();
	_nHits2d->Write();
	_radius->Write();
	_qPadDeposit->Write();
	_qPadDepositProfile->Write();
	_CellHitLocations->Write();
	_FirstLastCellLocation->Write();
	std::map<int, TH2F*>::iterator itLayer = LayerHitMap.begin();
	for (; itLayer != LayerHitMap.end(); ++itLayer) {
		(*itLayer).second->Write();
		delete (*itLayer).second;
	}
	_CellHitLocations3D->GetXaxis()->SetTitle("X Position in Cell (cm)");
	_CellHitLocations3D->GetYaxis()->SetTitle("Y Position in Cell (cm)");
	_CellHitLocations3D->GetZaxis()->SetTitle("Neighbors");
	_CellHitLocations3D->Write();

	_HitContDif->GetXaxis()->SetTitle("X Cell Difference (cells)");
	_HitContDif->GetYaxis()->SetTitle("Y Cell Difference (cells)");
	_HitContDif->Write();

	_DigiHitVsMokkaCell->GetXaxis()->SetTitle("X Cell Difference (cells)");
	_DigiHitVsMokkaCell->GetYaxis()->SetTitle("Y Cell Difference (cells)");
	_DigiHitVsMokkaCell->Write();

	_DigiHitVsMokkaPos->GetXaxis()->SetTitle("X Difference (mm)");
	_DigiHitVsMokkaPos->GetYaxis()->SetTitle("X Difference (mm)");
	_DigiHitVsMokkaPos->Write();

	delete _FirstLastCellLocation;
	delete _radius;
	delete _nHits2d;
	delete _hHitLayer;
	delete _hHitMapIJ;
	delete _hHitMapXY;
	delete _hContribMapXY;
	delete _pHitsPerLayer;
	delete _hNContrib;
	delete _hNhits;
	delete _hDigiHitsMap;
	delete _hCloudRadius;
	delete _hContTime;
	delete _hPadCharge;
	delete _hR2;
	delete _hCharge;
	delete _qintegral;
	delete _qintegral1;
	delete _qintegral2;
	delete _XYdep;
	delete _qPadDeposit;
	delete _qPadDepositProfile;
	delete _CellHitLocations;
	delete _CellHitLocations3D;
	delete _HitContDif;
	delete _DigiHitVsMokkaCell;
	delete _DigiHitVsMokkaPos;

	_rootFile->Close();
	//delete _rootFile;

}

};

