//Wire Chamber Tracker Processor

#include <WireChamberTracker.hh>
#include "WireChamberUtil.hh"

//c++
#include <cmath>
#include <fstream>

//lcio
#include "IMPL/LCCollectionVec.h"
#include "EVENT/LCCollection.h"
#include <UTIL/CellIDDecoder.h>
#include "UTIL/BitSet32.h"
#include <IMPL/TrackImpl.h>

//misc
#include <ExtendedTrack.hh>

//
//root
#include <TH1F.h>

using namespace marlin;
using namespace lcio;
using namespace std;

using EVENT::LCCollection;
using EVENT::LCParameters;


namespace CALICE{

WireChamberTracker aWireChamberTracker;

WireChamberTracker::WireChamberTracker() : Processor("WireChamberTracker"){
	_description = "Fits Tracks from Wire Chamber Hits. Stores only the best fitted track.";

	registerInputCollection(LCIO::TRACKERHIT, "InputCollection", "Name of input collection name",
			_inputCollection, string("WireChamberHits"));

	registerOutputCollection(LCIO::TRACK, "OutputCollection", "Name of output collection name",
			_outputCollection, string("WireChamberTracks"));

	registerProcessorParameter("MaxChi2", "Maximum Chi-Squared allowed for fit to be accepted", _maxChi2, (float) 20.0);


	_Total0Count = 0;
	_Total1Count = 0;
	_Total2Count = 0;
	_Layer0Count = 0;
	_Layer1Count = 0;
	_Layer2Count = 0;

	_NumFits = 0;
	_TotalCount = 0;
}

WireChamberTracker::~WireChamberTracker(){}

void WireChamberTracker::init(){}

void WireChamberTracker::processEvent(LCEvent* event) {

	int LayerCount = 3;
	vector <float> z (LayerCount);
	vector <vector<TrackerHit* > > HitList;
	HitList.resize(LayerCount); //We have 3 Layers

	LCCollection* hits = event->getCollection(_inputCollection);
	LCCollectionVec* wireChamberTracks = new LCCollectionVec(LCIO::TRACK);
	wireChamberTracks->setFlag(UTIL::set_bit(wireChamberTracks->getFlag(), LCIO::TRBIT_HITS));

	UTIL::CellIDDecoder<TrackerHit> iden(hits);
	for(int i=0; i<hits->getNumberOfElements(); i++){
		TrackerHit* hit = dynamic_cast<TrackerHit*>(hits->getElementAt(i));
		int layer = int(iden(hit)["layer"].value());
		for(int x=0; x<LayerCount; x++){
			if(layer == x){
				HitList[x].push_back(hit); //Locations[i][j][k] i is the layer, j is the hit number [k] is the TrackerHit
				z[x] = hit->getPosition()[2]; //Probably not the most efficient way to do it
			}
		}
	}

	if(HitList[0].size() > 0 and HitList[1].size() > 0){
		_Total2Count++;
		if(HitList[2].size() == 0){
			_Layer2Count++;
		}
	}
	if(HitList[1].size() > 0 and HitList[2].size() > 0){
		_Total0Count++;
		if(HitList[0].size() == 0 ){
			_Layer0Count++;
		}
	}
	if(HitList[0].size() > 0 and HitList[2].size() > 0){
		_Total1Count++;
		if(HitList[1].size() == 0){
			_Layer1Count++;
		}
	}
	
	if(HitList[0].size() > 0 and HitList[1].size() > 0 and HitList[2].size() > 0){
		_TotalCount++;
	}
	
	if(HitList[0].size() < 1 or HitList[1].size() < 1 or HitList[2].size() <1){ //If an event has no hits in a layer
		//return;
	}



	IMPL::ExtendedTrack* BestFit = new ExtendedTrack();
	BestFit->setXZchi2(1e7);  //Big number to ensure some fit
	BestFit->setYZchi2(1e7);
	BestFit->setChi2(1e7);
	BestFit->setNdf(4); //Number of degrees of freedom for wirechamber

	for(unsigned int i=0; i< HitList[0].size(); i++){
		for(unsigned int j=0; j<HitList[1].size(); j++){
			for(unsigned int k=0; k<HitList[2].size(); k++){
				vector<TrackerHit*> HitCombination (3);
				HitCombination[0] = HitList[0][i];
				HitCombination[1] = HitList[1][j];
				HitCombination[2] = HitList[2][k];

				streamlog_out( DEBUG0 ) << "======Positions of Particles========" << endl;
				streamlog_out( DEBUG0 ) << HitCombination[0]->getPosition()[0] << " " << HitCombination[0]->getPosition()[1] << " " << HitCombination[0]->getPosition()[2] << endl;
				streamlog_out( DEBUG0 ) << HitCombination[1]->getPosition()[0] << " " << HitCombination[1]->getPosition()[1] << " " << HitCombination[1]->getPosition()[2] << endl;
				streamlog_out( DEBUG0 ) << HitCombination[2]->getPosition()[0] << " " << HitCombination[2]->getPosition()[1] << " " << HitCombination[2]->getPosition()[2] << endl;

				ExtendedTrack* TrackFit = dynamic_cast<ExtendedTrack*> (WireChamberUtil::ExtendedLineFit(HitCombination));
				if(TrackFit->getXZchi2() <= BestFit->getXZchi2() and TrackFit->getYZchi2() <= BestFit->getYZchi2() and TrackFit->getXZslope() < 0.005
						and TrackFit->getXZslope() > -0.005 and TrackFit->getYZslope() < 0.005 and TrackFit->getYZslope() > -0.005 ){
					delete BestFit;
					BestFit = TrackFit;
				} else {
					delete TrackFit;
				}
			}
		}
	}

	if(BestFit->getChi2()/BestFit->getNdf() < _maxChi2){

		_NumFits++;
		streamlog_out( DEBUG4 ) << "======Track Parameters in Event: " << event->getEventNumber() << "======" << endl;
		streamlog_out( DEBUG4 ) << "X0 = " << BestFit->getReferencePoint()[0] << endl;
		streamlog_out( DEBUG4 ) << "Y0 = " << BestFit->getReferencePoint()[1] << endl;
		streamlog_out( DEBUG4 ) << "Z0 = " << BestFit->getReferencePoint()[2] << endl;
		streamlog_out( DEBUG4 ) << "Fit Chisquared = " << BestFit->getChi2() << endl;
		streamlog_out( DEBUG4 ) << "Fit Chisquared/NDF = " << BestFit->getChi2()/BestFit->getNdf() << endl;
		streamlog_out( DEBUG4 ) << "Phi = " << BestFit->getPhi() << endl;
		streamlog_out( DEBUG4 ) << "tanlambda = " << BestFit->getTanLambda() << endl;

		wireChamberTracks->addElement(BestFit);
	} else {
		delete BestFit;
	}
	event->addCollection(wireChamberTracks, _outputCollection);
}


void WireChamberTracker::end(){
}


} /*Namespace CALICE*/
