#include <HelloWorldProcessor.hh>

#include "IMPL/ClusterImpl.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"

#include <fstream>
#include <vector>

using namespace marlin;
using namespace lcio;
using namespace std;

HelloWorldProcessor aHelloWorldProcessor;

HelloWorldProcessor::HelloWorldProcessor() : Processor("HelloWorldProcessor"){
  _description = "Prints 'Hello World' for every event";
}

HelloWorldProcessor::~HelloWorldProcessor(){}

void HelloWorldProcessor::init(){}

void HelloWorldProcessor::processRunHeader(LCRunHeader* run) {
	cout << "Hello run: " << run->getRunNumber() << endl;
}

void HelloWorldProcessor::processEvent(LCEvent* event){
	cout << "Hello event: " << event->getEventNumber() << endl;
}

void HelloWorldProcessor::end(){}
