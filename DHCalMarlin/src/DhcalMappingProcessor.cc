/*
 * DhcalMappingProcessor.cc
 *
 *  Created on: Aug 23, 2013
 *      Author: Christian Grefe, CERN
 */

#include "DhcalMappingProcessor.hh"

#include <fstream>
#include <iostream>

namespace CALICE {

using std::string;
using std::vector;

DhcalMappingProcessor aDhcalMappingProcessor;

DhcalMappingProcessor::DhcalMappingProcessor() :
		Processor("DhcalMappingProcessor") {
	_description = "Processor to set up the DhcalMapping";
	_dhcalMapping = DhcalMapping::instance();

	registerProcessorParameter("IgnoreCellIDFiles", "File name with the list of cell IDs", _ignoreCellIdFileNames,
			vector<string>());
	registerProcessorParameter("IgnoreColumnRowLayerFiles",
			"File name with the list of ignored column, row and layer indices", _ignoreColumnRowLayerFileNames,
			vector<string>());
	registerProcessorParameter("ModuleCalibrationFiles", "Files with the module calibration values",
			_moduleCalibrationFileNames, vector<string>());
	registerProcessorParameter("LocalCalibrationFiles", "Files with the local calibration values",
			_localCalibrationFileNames, vector<string>());
	registerProcessorParameter("N_CELLS_U", "Number of cells in horizontal direction", _dhcalMapping->nCellsU, 96);
	registerProcessorParameter("N_CELLS_V", "Number of cells in vertical direction", _dhcalMapping->nCellsV, 96);
	registerProcessorParameter("N_LAYERS", "Number of layers", _dhcalMapping->nLayers, 54);
	registerProcessorParameter("CELL_SIZE_U", "Cell size in horizontal direction (in mm)", _dhcalMapping->cellSizeU,
			10.);
	registerProcessorParameter("CELL_SIZE_V", "Cell size in vertical direction (in mm)", _dhcalMapping->cellSizeV, 10.);
	registerProcessorParameter("ID_U", "Identifier for cell ID in horizontal direction", _dhcalMapping->idU,
			string("i"));
	registerProcessorParameter("ID_V", "Identifier for cell ID in vertical direction", _dhcalMapping->idV, string("j"));
	registerProcessorParameter("ID_LAYER", "Identifier for cell ID of layers", _dhcalMapping->idLayer, string("layer"));
	registerProcessorParameter("CELL_ID_ENCODING", "Cell ID encoding string", _cellIdEncoding,
			string("i:8,j:8,layer:8"));
}

DhcalMappingProcessor::~DhcalMappingProcessor() {
	// TODO Auto-generated destructor stub
}

void DhcalMappingProcessor::init() {
	printParameters();

	_dhcalMapping->setIdEncoding(_cellIdEncoding);

	// ignored cell IDs
	vector<string>::const_iterator itFileName = _ignoreCellIdFileNames.begin();
	while (itFileName != _ignoreCellIdFileNames.end()) {
		std::ifstream inputFile;
		inputFile.open(itFileName->c_str());
		CellID cellID = 0LL;
		if (inputFile.is_open()) {
			while (inputFile.good()) {
				inputFile >> cellID;
				_dhcalMapping->ignoredCellIDs.insert(cellID);
			}
		} else {
			streamlog_out(ERROR) << "Unable to read cell ID list from " << *itFileName << std::endl;
		}
		++itFileName;
	}

	itFileName = _ignoreColumnRowLayerFileNames.begin();
	while (itFileName != _ignoreColumnRowLayerFileNames.end()) {
		std::ifstream inputFile;
		inputFile.open(itFileName->c_str());
		int column, row, layer;
		if (inputFile.is_open()) {
			while (inputFile.good()) {
				inputFile >> column;
				inputFile >> row;
				inputFile >> layer;
				_dhcalMapping->ignoredCellIDs.insert(_dhcalMapping->getCellIDFromIndices(column, row, layer));
			}
		} else {
			streamlog_out(ERROR) << "Unable to read cell ID list from " << *itFileName << std::endl;
		}
		++itFileName;
	}

	// module calibration values
	for (vector<string>::const_iterator it = _moduleCalibrationFileNames.begin(); it != _moduleCalibrationFileNames.end(); it++) {
		string line;
		std::ifstream inputFile;
		inputFile.open(it->c_str());
		int layerNumber;
		int moduleNumber;
		float calibrationValue;
		float efficiency;
		float multiplicity;
		if (inputFile.is_open()) {
			while (inputFile.good()) {
				inputFile >> layerNumber;
				inputFile >> moduleNumber;
				inputFile >> calibrationValue;
				inputFile >> efficiency;
				inputFile >> multiplicity;
				ModulePosition position = static_cast<ModulePosition>(moduleNumber);
				_dhcalMapping->moduleCalibrationMap[layerNumber][position] = calibrationValue;
				_dhcalMapping->moduleEfficiencyMap[layerNumber][position] = efficiency;
				_dhcalMapping->moduleMultiplicityMap[layerNumber][position] = multiplicity;
			}
		} else {
			streamlog_out(ERROR) << "Unable to read calibration values from " << *it << std::endl;
		}
	}

	// local calibration values
	for (vector<string>::const_iterator it = _localCalibrationFileNames.begin(); it != _localCalibrationFileNames.end(); it++) {
		string line;
		std::ifstream inputFile;
		inputFile.open(it->c_str());
		int layerNumber, moduleNumber, columnNumber;
		float calibrationValue, efficiency, multiplicity;
		if (inputFile.is_open()) {
			while (inputFile.good()) {
				inputFile >> layerNumber;
				inputFile >> moduleNumber;
				inputFile >> columnNumber;
				inputFile >> calibrationValue;
				inputFile >> efficiency;
				inputFile >> multiplicity;
				ModulePosition modulePosition = static_cast<ModulePosition>(moduleNumber);
				DhcalMapping::Module module = _dhcalMapping->modulePositionMap[modulePosition];
				for (int rowNumber = module.vMin; rowNumber <= module.vMax; rowNumber++) {
					_dhcalMapping->calibrationMap->fill(layerNumber, columnNumber, rowNumber, calibrationValue);
					_dhcalMapping->efficiencyMap->fill(layerNumber, columnNumber, rowNumber, efficiency);
					_dhcalMapping->multiplicityMap->fill(layerNumber, columnNumber, rowNumber, multiplicity);
				}
			}
		} else {
			streamlog_out(ERROR) << "Unable to read calibration values from " << *it << std::endl;
		}
	}
}

} /* namespace CALICE */
