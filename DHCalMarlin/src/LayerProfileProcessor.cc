/*
 * LayerProfileProcessor.cc
 *
 *  Created on: Mar 19, 2014
 *      Author: cgrefe
 */

#include "LayerProfileProcessor.hh"

// LCIO
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"

// ROOT
#include "TFile.h"

// C++
#include <sstream>

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;

using std::string;
using std::stringstream;
using std::map;

namespace CALICE {

LayerProfileProcessor aLayerProfileProcessor;

LayerProfileProcessor::LayerProfileProcessor() :
		Processor("LayerProfileProcessor") {
	_description = "Processor to fill 2D profiles of all hits.";
	_mapping = DhcalMapping::instance();

	registerInputCollection(LCIO::CALORIMETERHIT, "HitCollection", "Name of the input collection", _collectionName,
			string("DhcHits"));

	registerProcessorParameter("RootFileName", "Name of the output ROOT file", _rootFileName, string(""));
	registerProcessorParameter("HistogramName", "Base name of the histograms", _histogramName, string("HitProfile"));

	_totalHistogram = 0;
}

LayerProfileProcessor::~LayerProfileProcessor() {
	// nothing to do
}

void LayerProfileProcessor::init() {
	_totalHistogram = new TH2D(_histogramName.c_str(),
			(_histogramName + ";" + _mapping->U_ID() + ";" + _mapping->V_ID() + ";Hits").c_str(), _mapping->N_CELLS_U(), 0,
			_mapping->N_CELLS_U(), _mapping->N_CELLS_V(), 0, _mapping->N_CELLS_V());
	for (int iLayer = 0; iLayer < _mapping->N_LAYERS(); iLayer++) {
		stringstream s;
		s << _histogramName << "_" << _mapping->LAYER_ID() << iLayer;
		_layerHistograms[iLayer] = new TH2D(s.str().c_str(),
				(s.str() + ";" + _mapping->U_ID() + ";" + _mapping->V_ID() + ";Hits").c_str(), _mapping->N_CELLS_U(), 0,
				_mapping->N_CELLS_U(), _mapping->N_CELLS_V(), 0, _mapping->N_CELLS_V());
	}
}

void LayerProfileProcessor::processEvent(EVENT::LCEvent* event) {
	LCCollection* hitCollection = event->getCollection(_collectionName);
	for (int iHit = 0; iHit < hitCollection->getNumberOfElements(); iHit++) {
		CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(hitCollection->getElementAt(iHit));
		int u = _mapping->getIndexU(hit);
		int v = _mapping->getIndexV(hit);
		int layer = _mapping->getIndexLayer(hit);
		_totalHistogram->Fill(u, v);
		_layerHistograms[layer]->Fill(u, v);
	}
}

void LayerProfileProcessor::end() {
	TFile* f = TFile::Open(_rootFileName.c_str(), "recreate");
	_totalHistogram->Write();
	map<int, TH2D*>::iterator it = _layerHistograms.begin();
	while (it != _layerHistograms.end()) {
		it->second->Write();
		++it;
	}
	f->Close();
}
}
;

