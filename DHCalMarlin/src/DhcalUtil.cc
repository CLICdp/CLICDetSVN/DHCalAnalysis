/*
 * DhcalUtil.cc
 *
 *  Created on: Feb 27, 2013
 *      Author: Christian Grefe, CERN
 */

# include "DhcalUtil.hh"

// lcio
#include "IMPL/TrackImpl.h"
#include "Exceptions.h"

// root
#include "TPrincipal.h"
#include "TMatrixT.h"
#include "TMath.h"

// c++
#include <algorithm>
#include <cmath>
#include <limits>

using EVENT::CalorimeterHit;
using EVENT::Cluster;
using EVENT::Exception;
using EVENT::LCCollection;
using EVENT::Track;
using EVENT::TrackerHit;

using IMPL::ClusterImpl;
using IMPL::TrackImpl;

using UTIL::CellIDDecoder;
using UTIL::BitField64;

using std::pair;
using std::make_pair;
using std::map;
using std::numeric_limits;
using std::pow;
using std::sqrt;
using std::string;
using std::tan;
using std::vector;

namespace CALICE {

void DhcalUtil::applyEnergyCut(vector<CalorimeterHit*>& hits, float energyCut) {
	vector<CalorimeterHit*>::iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		if ((*itHit)->getEnergy() < energyCut) {
			itHit = hits.erase(itHit);
		} else {
			++itHit;
		}
	}
}

void DhcalUtil::applyEnergyCut(vector<const CalorimeterHit*>& hits, float energyCut) {
	vector<const CalorimeterHit*>::iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		if ((*itHit)->getEnergy() < energyCut) {
			itHit = hits.erase(itHit);
		} else {
			++itHit;
		}
	}
}

void DhcalUtil::selectMinHitsClusters(vector<Cluster*>& clusters, int minHits) {
	vector<Cluster*>::iterator itCluster = clusters.begin();
	while (itCluster != clusters.end()) {
		if ((int) (*itCluster)->getCalorimeterHits().size() < minHits) {
			itCluster = clusters.erase(itCluster);
		} else {
			++itCluster;
		}
	}
}

void DhcalUtil::selectMaxHitsClusters(vector<Cluster*>& clusters, int maxHits) {
	vector<Cluster*>::iterator itCluster = clusters.begin();
	while (itCluster != clusters.end()) {
		if ((int) (*itCluster)->getCalorimeterHits().size() > maxHits) {
			itCluster = clusters.erase(itCluster);
		} else {
			++itCluster;
		}
	}
}

StraightLineFit* DhcalUtil::leastSquaresRegression(const vector<double>& dependantVariable,
		const vector<double>& independantVariable) {
	vector<double> weights(dependantVariable.size(), 1.0);
	return leastSquaresRegression(dependantVariable, independantVariable, weights);
}

StraightLineFit* DhcalUtil::leastSquaresRegression(const vector<double>& dependantVariable,
		const vector<double>& independantVariable, const vector<double>& weights) {
	int nPoints = dependantVariable.size();
	if (nPoints != (int) independantVariable.size() or nPoints != (int) weights.size()) {
		throw Exception("DhcalUtil::leastSquaresRegression: Inconsistent length of input vectors");
	}

	// See http://www.desy.de/~blobel/eBuch.pdf page 162
	double S1(0.), Sx(0.), Sxx(0.), Sxy(0.), Sy(0.), Syy(0.);
	for (int index = 0; index < nPoints; index++) {
		double x = independantVariable[index];
		double y = dependantVariable[index];
		double weight = weights[index];
		S1 += weight;
		Sx += x * weight;
		Sxx += x * x * weight;
		Sxy += x * y * weight;
		Sy += y * weight;
		Syy += y * y * weight;
	}

	StraightLineFit* fitResult = new StraightLineFit;
	double D = S1 * Sxx - Sx * Sx;
	fitResult->intercept = (Sxx * Sy - Sx * Sxy) / D;
	fitResult->interceptVariance = Sxx / D;
	fitResult->slope = (-Sx * Sy + S1 * Sxy) / D;
	fitResult->slopeVariance = S1 / D;
	fitResult->covariance = -Sx / D;
	fitResult->chisq = Syy - fitResult->intercept * Sy - fitResult->slope * Sxy;
	fitResult->dof = nPoints - 1;
	return fitResult;
}

Track* DhcalUtil::straightLineTrackFit(const vector<TVector3>& points,
		const vector<TMatrixTSym<double> >& covariances) {
	int nPoints = points.size();
	if (nPoints != (int) covariances.size()) {
		throw Exception("DhcalUtil::straightLineTrackFit: Inconsistent length of points and covariance matrices");
	}
	vector<double> x;
	vector<double> y;
	vector<double> z;
	vector<double> xWeights;
	vector<double> yWeights;
	for (int index = 0; index < nPoints; index++) {
		const TVector3& point = points[index];
		x.push_back(point.X());
		y.push_back(point.Y());
		z.push_back(point.Z());
		const TMatrixTSym<double>& covariance = covariances[index];
		// here we assume independent measurements of x and y
		// need finite covariances for reasonable weights
		if (covariance[0][0] == 0.)
			throw Exception("DhcalUtil::straightLineTrackFit: Covariance for x is 0");
		if (covariance[1][1] == 0.)
			throw Exception("DhcalUtil::straightLineTrackFit: Covariance for y is 0");
		xWeights.push_back(1. / covariance[0][0]);
		yWeights.push_back(1. / covariance[1][1]);
	}

	StraightLineFit* xzFit = leastSquaresRegression(x, z, xWeights);
	StraightLineFit* yzFit = leastSquaresRegression(y, z, yWeights);

	TrackImpl* trackFit = new TrackImpl();
	float zRef = 0.0;
	pair<double, double> xRefResult = xzFit->calculate(zRef);
	pair<double, double> yRefResult = yzFit->calculate(zRef);
	float xRef = xRefResult.first;
	float yRef = yRefResult.first;
	float referencePoint[] = { xRef, yRef, zRef };
	TVector3 direction(xzFit->slope, yzFit->slope, 1.0);

	trackFit->setOmega(0.);
	trackFit->setD0(0.);
	trackFit->setZ0(0.);
	trackFit->setPhi(direction.Phi());
	trackFit->setTanLambda(tan(TMath::PiOver2() - direction.Theta()));
	trackFit->setReferencePoint(referencePoint);
	trackFit->setChi2(xzFit->chisq + yzFit->chisq);
	trackFit->setNdf(xzFit->dof + yzFit->dof);

	// TODO: calculate track covariance matrix

	// clean up
	delete xzFit;
	delete yzFit;

	return trackFit;
}

Track* DhcalUtil::straightLineTrackFit(const vector<TrackerHit*>& hits) {
	vector<TVector3> positions;
	vector<TMatrixTSym<double> > covariances;
	vector<TrackerHit*>::const_iterator itHit;
	for (itHit = hits.begin(); itHit != hits.end(); itHit++) {
		TrackerHit* hit = *itHit;
		TVector3 position(hit->getPosition());
		positions.push_back(position);
		TMatrixTSym<double> covarianceMatrix(3);
		const vector<float>& covariance = hit->getCovMatrix();
		covarianceMatrix[0][0] = covariance[0];
		covarianceMatrix[1][0] = covariance[1];
		covarianceMatrix[1][1] = covariance[2];
		covarianceMatrix[2][0] = covariance[3];
		covarianceMatrix[2][1] = covariance[4];
		covarianceMatrix[2][2] = covariance[5];
		covariances.push_back(covarianceMatrix);
	}
	TrackImpl* trackFit = dynamic_cast<TrackImpl*>(straightLineTrackFit(positions, covariances));
	// add all contributing hits to the fit
	for (itHit = hits.begin(); itHit != hits.end(); itHit++) {
		trackFit->addHit(*itHit);
	}
	return trackFit;
}

Track* DhcalUtil::straightLineTrackFit(const vector<Cluster*>& clusters) {
	vector<TVector3> positions;
	vector<TMatrixTSym<double> > covariances;
	vector<Cluster*>::const_iterator itCluster;
	for (itCluster = clusters.begin(); itCluster != clusters.end(); itCluster++) {
		Cluster* cluster = *itCluster;
		TVector3 position(cluster->getPosition());
		positions.push_back(position);
		TMatrixTSym<double> covarianceMatrix(3);
		const vector<float>& covariance = cluster->getPositionError();
		covarianceMatrix[0][0] = covariance[0];
		covarianceMatrix[1][0] = covariance[1];
		covarianceMatrix[1][1] = covariance[2];
		covarianceMatrix[2][0] = covariance[3];
		covarianceMatrix[2][1] = covariance[4];
		covarianceMatrix[2][2] = covariance[5];
		covariances.push_back(covarianceMatrix);
	}
	return straightLineTrackFit(positions, covariances);
}

pair<double, double> DhcalUtil::calculateStandardDeviation(const vector<double>& values) {
	vector<double> weights(values.size(), 1.0);
	return calculateStandardDeviation(values, weights);
}

pair<double, double> DhcalUtil::calculateStandardDeviation(const vector<double>& values,
		const vector<double>& weights) {
	int nValues = (int) values.size();
	if (nValues != (int) weights.size()) {
		throw Exception("DhcalUtil::calculateStandardDeviation: Inconsistent length of values and weights");
	}
	double mean = 0.;
	double sumSquared = 0.;
	double totalWeight = 0.;
	for (int index = 0; index < (int) values.size(); index++) {
		double value = values[index];
		double weight = weights[index];
		double tempTotalWeight = weight + totalWeight;
		double delta = value - mean;
		mean += delta * weight / tempTotalWeight;
		sumSquared += weight * delta * (value - mean);
		totalWeight = tempTotalWeight;
	}
	double standardDeviation = 0.;
	if (totalWeight != 0) {
		standardDeviation = sqrt(sumSquared / totalWeight);
	}
	return make_pair(mean, standardDeviation);
}

void DhcalUtil::calculateClusterProperties(ClusterImpl* cluster) {
	vector<CalorimeterHit*> hits = cluster->getCalorimeterHits();
	double xMin = numeric_limits<double>::max();
	double xMax = -numeric_limits<double>::max();
	double yMin = numeric_limits<double>::max();
	double yMax = -numeric_limits<double>::max();
	double zMin = numeric_limits<double>::max();
	double zMax = -numeric_limits<double>::max();
	vector<double> x;
	vector<double> y;
	vector<double> z;
	vector<double> weights;
	double totalWeight = 0.;
	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		TVector3 p(hit->getPosition());
		double weight = hit->getEnergy();
		if (weight == 0.)
			weight = 1.;
		if (p.X() < xMin)
			xMin = p.X();
		if (p.X() > xMax)
			xMax = p.X();
		if (p.Y() < yMin)
			yMin = p.Y();
		if (p.Y() > yMax)
			yMax = p.Y();
		if (p.Z() < zMin)
			zMin = p.Z();
		if (p.Z() > zMax)
			zMax = p.Z();
		x.push_back(p.X());
		y.push_back(p.Y());
		z.push_back(p.Z());
		weights.push_back(weight);
		totalWeight += weight;
		++itHit;
	}
	pair<double, double> xResult = calculateStandardDeviation(x, weights);
	pair<double, double> yResult = calculateStandardDeviation(y, weights);
	pair<double, double> zResult = calculateStandardDeviation(z, weights);
	float position[3] = { (float) xResult.first, (float) yResult.first, (float) zResult.first };
	// FIXME: should not mix U,V and x,y here
	// Uncertainty of single position measurement is cell_size/sqrt(12) and decreases with 1/sqrt(N) for N measurements
	float cov_x = pow(DhcalMapping::instance()->CELL_SIZE_U(), 2) / (12. * x.size());
	float cov_y = pow(DhcalMapping::instance()->CELL_SIZE_V(), 2) / (12. * y.size());
	float cov_z = 0.; // No uncertainty in z measurements
	// lower triangle of covariance matrix: cov(x,x), cov(y,x), cov(y,y), cov(z,x), cov(z,y), cov(z,z)
	float positionError[6] = { cov_x, 0.0, cov_y, 0.0, 0.0, cov_z };
	cluster->setPosition(position);
	cluster->setPositionError(positionError);
	cluster->setEnergy(totalWeight);
	TVector3 principalComponent = calculatePrincipalComponent(hits);
	if (principalComponent.Mag() == principalComponent.Mag()) {
		cluster->setIPhi(principalComponent.Phi());
		cluster->setITheta(principalComponent.Theta());
	}
}

TVector3 DhcalUtil::calculatePrincipalComponent(const vector<CalorimeterHit*>& hits) {
	TPrincipal principalComponents(3);
	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		const double position[3] = { hit->getPosition()[0], hit->getPosition()[1], hit->getPosition()[2] };
		principalComponents.AddRow(position);
		++itHit;
	}
	principalComponents.MakePrincipals();
	const TMatrixD* eigenVectors = principalComponents.GetEigenVectors();
	return TVector3((*eigenVectors)(0, 0), (*eigenVectors)(0, 1), (*eigenVectors)(0, 2));
}

map<int, vector<CalorimeterHit*> > DhcalUtil::buildHitMap(const vector<const LCCollection*>& collections,
		const string& identifier) {
	map<int, vector<CalorimeterHit*> > hitMap;
	vector<const LCCollection*>::const_iterator itCollection = collections.begin();
	while (itCollection != collections.end()) {
		map<int, vector<CalorimeterHit*> > collectionHitMap = buildHitMap(*itCollection, identifier);
		map<int, vector<CalorimeterHit*> >::iterator itMap = collectionHitMap.begin();
		while (itMap != collectionHitMap.end()) {
			int index = itMap->first;
			vector<CalorimeterHit*> collectionIndexHits = itMap->second;
			vector<CalorimeterHit*>& indexHits = hitMap[index];
			indexHits.insert(indexHits.end(), collectionIndexHits.begin(), collectionIndexHits.end());
			++itMap;
		}
		++itCollection;
	}
	return hitMap;
}

map<int, vector<CalorimeterHit*> > DhcalUtil::buildHitMap(const LCCollection* collection, const string& identifier) {
	vector<CalorimeterHit*> caloHits;
	CellIDDecoder<CalorimeterHit> idDecoder(collection);
	for (int iHit = 0; iHit < collection->getNumberOfElements(); iHit++) {
		caloHits.push_back(dynamic_cast<CalorimeterHit*>(collection->getElementAt(iHit)));
	}
	return buildHitMap(caloHits, idDecoder, identifier);
}

map<int, vector<CalorimeterHit*> > DhcalUtil::buildHitMap(const vector<CalorimeterHit*>& hits,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& identifier) {
	map<int, vector<CalorimeterHit*> > hitMap;
	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		int layer = idDecoder(hit)[identifier].value();
		hitMap[layer].push_back(hit);
		++itHit;
	}
	return hitMap;
}

map<int, vector<CalorimeterHit*> > DhcalUtil::buildHitMap(const vector<CalorimeterHit*>& hits, BitField64& idDecoder,
		const string& identifier) {
	map<int, vector<CalorimeterHit*> > hitMap;
	vector<CalorimeterHit*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHit* hit = *itHit;
		idDecoder.setValue(getCellID(hit));
		int layer = idDecoder[identifier].value();
		hitMap[layer].push_back(hit);
		++itHit;
	}
	return hitMap;
}

map<int, vector<Cluster*> > DhcalUtil::buildLayerClusterMap(const LCCollection* collection,
		const string& layerIdentifier) {
	CellIDDecoder<CalorimeterHit> idDecoder(collection);
	vector<Cluster*> clusters;
	for (int iCluster = 0; iCluster < collection->getNumberOfElements(); iCluster++) {
		clusters.push_back(dynamic_cast<Cluster*>(collection->getElementAt(iCluster)));
	}
	return buildLayerClusterMap(clusters, idDecoder, layerIdentifier);
}

map<int, vector<Cluster*> > DhcalUtil::buildLayerClusterMap(const LCCollection* collection,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& layerIdentifier) {
	vector<Cluster*> clusters;
	for (int iCluster = 0; iCluster < collection->getNumberOfElements(); iCluster++) {
		clusters.push_back(dynamic_cast<Cluster*>(collection->getElementAt(iCluster)));
	}
	return buildLayerClusterMap(clusters, idDecoder, layerIdentifier);
}

map<int, vector<Cluster*> > DhcalUtil::buildLayerClusterMap(const vector<Cluster*>& clusters,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& layerIdentifier) {
	map<int, vector<Cluster*> > layerClusterMap;
	vector<Cluster*>::const_iterator itCluster = clusters.begin();
	while (itCluster != clusters.end()) {
		Cluster* cluster = *itCluster;
		map<int, vector<CalorimeterHit*> > layerHitMap = buildLayerHitMap(cluster->getCalorimeterHits(), idDecoder,
				layerIdentifier);
		if (!layerHitMap.empty()) {
			int layer = layerHitMap.begin()->first;
			layerClusterMap[layer].push_back(cluster);
		}
		++itCluster;
	}
	return layerClusterMap;
}

bool DhcalUtil::compareU(const CalorimeterHit* hit, const CalorimeterHit* otherHit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& uIdentifier) {
	int u1 = idDecoder(hit)[uIdentifier].value();
	int u2 = idDecoder(otherHit)[uIdentifier].value();
	return u1 < u2;
}

bool DhcalUtil::compareV(const CalorimeterHit* hit, const CalorimeterHit* otherHit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& vIdentifier) {
	int v1 = idDecoder(hit)[vIdentifier].value();
	int v2 = idDecoder(otherHit)[vIdentifier].value();
	return v1 < v2;
}

bool DhcalUtil::compareLayer(const CalorimeterHit* hit, const CalorimeterHit* otherHit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& layerIdentifier) {
	int layer1 = idDecoder(hit)[layerIdentifier].value();
	int layer2 = idDecoder(otherHit)[layerIdentifier].value();
	return layer1 < layer2;
}

bool DhcalUtil::compareUV(const CalorimeterHit* hit, const CalorimeterHit* otherHit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& uIdentifier, const string& vIdentifier) {
	int u1 = idDecoder(hit)[uIdentifier].value();
	int v1 = idDecoder(hit)[vIdentifier].value();
	int u2 = idDecoder(otherHit)[uIdentifier].value();
	int v2 = idDecoder(otherHit)[vIdentifier].value();
	return sqrt(u1 * u1 + v1 * v1) < sqrt(u2 * u2 + v2 * v2);
}

bool DhcalUtil::compareX(const CalorimeterHit* hit, const CalorimeterHit* otherHit) {
	return hit->getPosition()[0] < otherHit->getPosition()[0];
}

bool DhcalUtil::compareY(const CalorimeterHit* hit, const CalorimeterHit* otherHit) {
	return hit->getPosition()[1] < otherHit->getPosition()[1];
}

bool DhcalUtil::compareZ(const CalorimeterHit* hit, const CalorimeterHit* otherHit) {
	return hit->getPosition()[2] < otherHit->getPosition()[2];
}

bool DhcalUtil::compareXY(const CalorimeterHit* hit, const CalorimeterHit* otherHit) {
	float x1 = hit->getPosition()[0];
	float y1 = hit->getPosition()[1];
	float x2 = otherHit->getPosition()[0];
	float y2 = otherHit->getPosition()[1];
	return sqrt(x1 * x1 + y1 * y1) < sqrt(x2 * x2 + y2 * y2);
}

bool DhcalUtil::compareXYZ(const CalorimeterHit* hit, const CalorimeterHit* otherHit) {
	float x1 = hit->getPosition()[0];
	float y1 = hit->getPosition()[1];
	float z1 = hit->getPosition()[2];
	float x2 = otherHit->getPosition()[0];
	float y2 = otherHit->getPosition()[1];
	float z2 = otherHit->getPosition()[2];
	return sqrt(x1 * x1 + y1 * y1 + z1 * z1) < sqrt(x2 * x2 + y2 * y2 + z2 * z2);
}

bool DhcalUtil::consistentInU(const CalorimeterHit* hit, const CalorimeterHit* otherHit, int limit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& uIdentifier) {
	int u1 = idDecoder(hit)[uIdentifier].value();
	int u2 = idDecoder(otherHit)[uIdentifier].value();
	return std::abs(u1 - u2) <= limit;
}

bool DhcalUtil::consistentInV(const CalorimeterHit* hit, const CalorimeterHit* otherHit, int limit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& vIdentifier) {
	int v1 = idDecoder(hit)[vIdentifier].value();
	int v2 = idDecoder(otherHit)[vIdentifier].value();
	return std::abs(v1 - v2) <= limit;
}

bool DhcalUtil::consistentInUV(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit,
		float maxDistance, UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder, const std::string& uIdentifier,
		const std::string& vIdentifier) {
	int deltaU = idDecoder(hit)[uIdentifier].value() - idDecoder(otherHit)[uIdentifier].value();
	int deltaV = idDecoder(hit)[vIdentifier].value() - idDecoder(otherHit)[vIdentifier].value();
	return sqrt(deltaU * deltaU + deltaV * deltaV) <= maxDistance;
}

bool DhcalUtil::consistentInLayer(const CalorimeterHit* hit, const CalorimeterHit* otherHit, int limit,
		CellIDDecoder<CalorimeterHit>& idDecoder, const string& layerIdentifier) {
	int layer1 = idDecoder(hit)[layerIdentifier].value();
	int layer2 = idDecoder(otherHit)[layerIdentifier].value();
	return std::abs(layer1 - layer2) <= limit;
}

bool DhcalUtil::consistentInX(const CalorimeterHit* hit, const CalorimeterHit* otherHit, float xLimit) {
	float x1 = hit->getPosition()[0];
	float x2 = hit->getPosition()[0];
	return std::abs(x1 - x2) <= xLimit;
}

bool DhcalUtil::consistentInY(const CalorimeterHit* hit, const CalorimeterHit* otherHit, float yLimit) {
	float y1 = hit->getPosition()[1];
	float y2 = hit->getPosition()[1];
	return std::abs(y1 - y2) <= yLimit;
}

bool DhcalUtil::consistentInXY(const CalorimeterHit* hit, const CalorimeterHit* otherHit, float maxDistance) {
	float deltaX = hit->getPosition()[0] - otherHit->getPosition()[0];
	float deltaY = hit->getPosition()[1] - otherHit->getPosition()[1];
	return sqrt(deltaX * deltaX + deltaY * deltaY) <= maxDistance;
}

bool DhcalUtil::consistentInZ(const CalorimeterHit* hit, const CalorimeterHit* otherHit, float zLimit) {
	float z1 = hit->getPosition()[2];
	float z2 = hit->getPosition()[2];
	return std::abs(z1 - z2) <= zLimit;
}

bool DhcalUtil::consistentInXYZ(const CalorimeterHit* hit, const CalorimeterHit* otherHit, float maxDistance) {
	float deltaX = hit->getPosition()[0] - otherHit->getPosition()[0];
	float deltaY = hit->getPosition()[1] - otherHit->getPosition()[1];
	float deltaZ = hit->getPosition()[2] - otherHit->getPosition()[2];
	return sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ) <= maxDistance;
}

} /* namespace CALICE */

