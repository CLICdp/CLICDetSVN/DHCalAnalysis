/*
 * LayerCalibrationProcessor.cc
 *
 *  Created on: Jun 21, 2013
 *      Author: Christian Grefe, CERN
 */

#include "LayerCalibrationProcessor.hh"
#include "DhcalUtil.hh"

#include "Exceptions.h"
#include "EVENT/LCCollection.h"
#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"

#include "streamlog/streamlog.h"

#include <vector>
#include <cmath>

using EVENT::LCCollection;
using EVENT::LCEvent;
using EVENT::Exception;
using EVENT::CalorimeterHit;
using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;

using std::string;
using std::vector;

namespace CALICE {

LayerCalibrationProcessor aLayerCalibrationProcessor;

LayerCalibrationProcessor::LayerCalibrationProcessor() :
		Processor("LayerCalibrationProcessor") {
	_description = "Applies calibration values to hits depending on their layer and module numbers.";

	registerInputCollection(LCIO::CALORIMETERHIT, "InputCollection", "Name of input collections", _inputCollectionName,
			"DhcHits");
	registerProcessorParameter("MeanMultiplicity", "Normalisation for mean multiplicity",
			meanMultiplicity, float(1.0));
	registerProcessorParameter("MeanEfficiency", "Normalisation for mean efficiency",
			meanEfficiency, float(1.0));

	_mapping = DhcalMapping::instance();
}

LayerCalibrationProcessor::~LayerCalibrationProcessor() {

}

void LayerCalibrationProcessor::init() {
	printParameters();
}

void LayerCalibrationProcessor::modifyEvent(LCEvent* event) {
	LCCollection* collection = event->getCollection(_inputCollectionName);
	vector<CalorimeterHitImpl*> hits;
	for (int index = 0; index < collection->getNumberOfElements(); index++) {
		CalorimeterHitImpl* hit = dynamic_cast<CalorimeterHitImpl*>(collection->getElementAt(index));
		hits.push_back(hit);
	}

	vector<CalorimeterHitImpl*>::const_iterator itHit = hits.begin();
	while (itHit != hits.end()) {
		CalorimeterHitImpl* hit = *itHit;
		long64 cellID = DhcalUtil::getCellID(hit);
		float energy = hit->getEnergy();
		if (energy == 0.0) {
			energy = 1.0;
		}
		float multiplicity = 1.0;
		float efficiency = 1.0;
		try {
			multiplicity = _mapping->getMultiplicity(cellID);
		} catch (Exception& e) {

		}
		if (multiplicity != 0.0) {
			energy *= meanMultiplicity / multiplicity;
		}
		try{
			efficiency = _mapping->getEfficiency(cellID);
		} catch (Exception& e) {

		}
		if (efficiency != 0.0) {
			energy *= meanEfficiency / efficiency;
		}
		hit->setEnergy(energy);
		++itHit;
	}
}

} /* namespace CALICE */
