#include "BoxEventFinder.h"
#include "DhcalMapping.hh"
#include <iostream>
#include <utility>
#include <map>
#include <vector>
#include <algorithm>
#include <fstream>
#include "streamlog/streamlog.h"

#include <marlin/Exceptions.h>
#include <EVENT/LCCollection.h>
#include <EVENT/MCParticle.h>
#include <EVENT/ReconstructedParticle.h>
#include <EVENT/CalorimeterHit.h>
#include <UTIL/CellIDDecoder.h>
// ----- include for verbosity dependend logging ---------
#include <marlin/VerbosityLevels.h>


#include "TLorentzVector.h"
#include "TVector3.h"
#include "TRandom3.h"
#include "DhcalUtil.hh"



using namespace lcio;
using namespace marlin;
using namespace std;
using std::endl;
using std::string;
using std::pair; using std::make_pair;
using std::map; using std::vector;
using std::min;

using UTIL::CellIDDecoder;

namespace CALICE {

BoxEventFinder aBoxEventFinder;


BoxEventFinder::BoxEventFinder() 
    : Processor("BoxEventFinder")
    , m_RunNumber(0)
    , m_nBoxEvents(0)
	, m_nRun(0)
	, m_nEvt(0) {
    // modify processor description
    _description = "BoxEventFinder identifies box events produced by to the FEB" ;

    vector<string> defaultCollections;
    defaultCollections.push_back("DhcHits");
    //defaultCollections.push_back("DhcTailCatcherHits");
    // register steering parameters: name, description, class-variable, default value
    registerInputCollections("CalorimeterHit"
        , "CaloHitCollections"
        , "Name of the hit collection"
        , m_hitCollectionNames
        , defaultCollections
    );
    registerProcessorParameter("SkipBoxEvents", "Box events trigger a SkipEventException", m_skipBoxEvents, true);
}


void 
BoxEventFinder::init() { 
    streamlog_out(DEBUG) << "   init called  " << endl;
    // usually a good idea to
    printParameters();
    m_nRun = 0;
    m_nEvt = 0;
}


void
BoxEventFinder::processRunHeader(LCRunHeader* run) {
    ++m_nRun;
    // We print out the information from previous runs here
    if (m_RunNumber > 0) {
        streamlog_out(MESSAGE) << "Run " << m_RunNumber << " has " << m_nBoxEvents << " Box Events" << endl;
    }
    m_RunNumber = run->getRunNumber();
    if (run->getParameters().getStringVal("RunType") != "beamData") {
        m_RunNumber = -1;
    }
    m_nBoxEvents = 0;
}


LCCollection* safelyGetCollection(LCEvent* evt, const string& collectionName) {
    LCCollection* list;
    try {
        list = evt->getCollection( collectionName );
        if (not list) {
            streamlog_out(ERROR) << "could not open collection " << collectionName << endl;
            return 0;
        }
    } catch( lcio::DataNotAvailableException& e ) {
            streamlog_out(WARNING) << collectionName << " collection not available" << endl;
            return 0;
    }
    return list;
}

struct FrontEndBoardHits {
    // number of hits along the four sides of the rectangle
    size_t nSideHits[12];
    // the number of hits in the corner is double-counted, since they belong to each of the adjacent sides
    // they need to be subtracted from the total.
    int nCornerHits;
    // and the total number of hits
    int nTotal;
    // constructor
    FrontEndBoardHits() {
    	nSideHits[0] = 0;
    	nSideHits[1] = 0;
    	nSideHits[2] = 0;
    	nSideHits[3] = 0;
	nSideHits[4] = 0;
    	nSideHits[5] = 0;
    	nSideHits[6] = 0;
    	nSideHits[7] = 0;
	nSideHits[8] = 0;
    	nSideHits[9] = 0;
    	nSideHits[10] = 0;
    	nSideHits[11] = 0;
    	nCornerHits = 0;
    	nTotal = 0;
    }
};


struct StartPoint {
	StartPoint(int x = 0, int y = 0)
	: xStart(x)
	, yStart(y) {};
    int xStart;
    int yStart;
};


bool isBoxEvent(const vector<CalorimeterHit*>& hits, CellIDDecoder<CalorimeterHit>& idDecoder,LCEvent*hevt) {
    // THE STACK:
    // *********
    // *   *   *
    // *********
    // *   *   *
    // *********
    // *   *   *
    // *********
    // Sizes and dimensions:
    // (0, 0) is the bottom left corner of the layer.
    // Layer size is 96x96 pads, consisting of 3 RPC stacked in y
    // One FEB serves a half RPC, i.e., 48x32 pads

    map<FrontEndBoardPosition, StartPoint> startPoints;
    startPoints.insert(make_pair(BottomLeft, StartPoint(0, 0)));
    startPoints.insert(make_pair(BottomRight, StartPoint(48, 0)));
    startPoints.insert(make_pair(MiddleLeft, StartPoint(0, 32)));
    startPoints.insert(make_pair(MiddleRight, StartPoint(48, 32)));
    startPoints.insert(make_pair(TopLeft, StartPoint(0, 64)));
    startPoints.insert(make_pair(TopRight, StartPoint(48, 64)));
    map<FrontEndBoardPosition, FrontEndBoardHits> febHitInfo;

    // We want one hit collection sorted in x
    // and one collection sorted in y
    map<long64, int> xPosHitmap;
    map<long64, int> yPosHitmap;
    vector<CalorimeterHit*>::const_iterator h = hits.begin();
    const BitField64& hbits = idDecoder(*h);
    int boxFoundInLayer=hbits[DhcalMapping::instance()->LAYER_ID()].value();
    for ( ; h != hits.end(); ++h) {
    const BitField64& bits = idDecoder(*h); // This needs to be a reference. Otherwise the bitfield undergoes a double delete.
        long64 u = bits[DhcalMapping::instance()->U_ID()].value();
        xPosHitmap[u] += 1;
        long64 v = bits[DhcalMapping::instance()->V_ID()].value();
        yPosHitmap[v] += 1;
        map<FrontEndBoardPosition, StartPoint>::iterator feb_it = startPoints.begin();
        for ( ; feb_it != startPoints.end(); ++feb_it) {
	  int xStart = feb_it->second.xStart;
	  int yStart = feb_it->second.yStart;
	  if(u==xStart-1&&(v>yStart&&v<yStart+31)){
	    febHitInfo[feb_it->first].nSideHits[4]+=1;
	  }
	  
	  if(u==xStart+48&&(v>yStart&&v<yStart+31)){
	    febHitInfo[feb_it->first].nSideHits[6]+=1;  
	  }
	
	  if (v == yStart-1&&(u>xStart&&u<xStart+47)) {
	    febHitInfo[feb_it->first].nSideHits[8] += 1;	
	  }
 
	  if (v == yStart+32&&(u>xStart&&u<xStart+47)) {
	    febHitInfo[feb_it->first].nSideHits[10] += 1;
	  }
	  
	  
	  
	  if (not (u >= xStart and u <= xStart+47 and v >= yStart and v <= yStart+31)) {
	      continue;
            }
	    
            febHitInfo[feb_it->first].nTotal += 1;
            if (u == xStart) {
            	febHitInfo[feb_it->first].nSideHits[0] += 1;		
	    }
	    else if(u==xStart+1){
	      febHitInfo[feb_it->first].nSideHits[5]+=1;
	    } 
	    else if (u == xStart+47) {
	      febHitInfo[feb_it->first].nSideHits[1] += 1;
            }
	      else if(u==xStart+46){
	      febHitInfo[feb_it->first].nSideHits[7]+=1;
	    }
           
	    if (v == yStart) {
	      febHitInfo[feb_it->first].nSideHits[2] += 1;	
	    }
	    else if (v == yStart+1) {
	      febHitInfo[feb_it->first].nSideHits[9] += 1;	
            }
	    else if (v == yStart+31) {
	      febHitInfo[feb_it->first].nSideHits[3] += 1;
            }
	    
	    else if (v == yStart+30) {
            	febHitInfo[feb_it->first].nSideHits[11] += 1;
            }

            // corner hits belong to two sides, so they are counted twice when computing the number of hits along the border
            if ((u == xStart and (v == yStart or v == yStart+31)) or (u == xStart+47 and (v == yStart or v == yStart+31))) {
                febHitInfo[feb_it->first].nCornerHits += 1;
            }
        }
    }

    bool thisIsABox(false);
    // now loop over all feb to find box events
    map<FrontEndBoardPosition, FrontEndBoardHits>::iterator feb_it = febHitInfo.begin();
    for ( ; feb_it != febHitInfo.end(); ++ feb_it) {
     
        // starting with the outward facing side of the rectangle
        FrontEndBoardHits& febHits = feb_it->second;
        int nBorder = febHits.nSideHits[0]
            + febHits.nSideHits[1]
            + febHits.nSideHits[2]
            + febHits.nSideHits[3]
            - febHits.nCornerHits;
	
        // now the number of hits along the four borders of the rectangle is known.
        // only thing left is the ratio of hits in the middle.
        // the middle is the end of the left or the beginning of the right FEB.
        // we want to know the ratio of hits in this column to the hits in the adjacent column of the other FEB
        // first look in x, then look in y
        switch (feb_it->first) {
            case BottomLeft:
            case MiddleLeft:
            case TopLeft:
	      thisIsABox = nBorder > 17 
		  or febHits.nSideHits[0]>3
		or (febHits.nSideHits[1]>6 and 1.0*febHits.nSideHits[6]/febHits.nSideHits[1]<0.25 and 1.0*febHits.nSideHits[7]/febHits.nSideHits[1]<0.25);
	      
                break;
            case BottomRight:
            case MiddleRight:
            case TopRight:
	      thisIsABox = nBorder > 17 
                    or febHits.nSideHits[1] > 3
		or (febHits.nSideHits[0] > 6 and 1.0 * febHits.nSideHits[4] / febHits.nSideHits[0] < 0.25 and 1.0 * febHits.nSideHits[5] / febHits.nSideHits[0] < 0.25);
		
                break;
            default:
                break;
        }
        // now let's look at the borders parallel to y
        switch (feb_it->first) {
            case BottomLeft:
            case BottomRight:
	      thisIsABox = thisIsABox or (febHits.nSideHits[2] > 3)
                    or (febHits.nSideHits[3] > 6 and 1.0 * febHits.nSideHits[10] / febHits.nSideHits[3] < 0.25)
		
	      or (febHits.nSideHits[3] > 3 and febHits.nSideHits[11]+febHits.nSideHits[11] == 0);
	   
                break;
	case MiddleLeft:
	case MiddleRight:
	                  thisIsABox = thisIsABox or (febHits.nSideHits[2] > 6 and 1.0 * febHits.nSideHits[8] / febHits.nSideHits[2] < 0.25)
		  or (febHits.nSideHits[2] > 7 and 1.0 * febHits.nSideHits[2] / febHits.nTotal > 0.5)
		  or (febHits.nSideHits[2] > 3 and febHits.nSideHits[8]+febHits.nSideHits[9] == 0)
		  or (febHits.nSideHits[3] > 6 and 1.0 * febHits.nSideHits[10] / febHits.nSideHits[3] < 0.25)
		
		  or (febHits.nSideHits[3] > 3 and febHits.nSideHits[11]+febHits.nSideHits[10] == 0);
		
		break;
            case TopLeft:
            case TopRight:
	      thisIsABox = thisIsABox or (febHits.nSideHits[3] > 3)
                    or (febHits.nSideHits[2] > 6 and 1.0 * febHits.nSideHits[8] / febHits.nSideHits[2] < 0.25)
		
                    or (febHits.nSideHits[2] > 3 and febHits.nSideHits[9]+febHits.nSideHits[8] == 0);
	      
		break;
            default:
                break;
        }
        // if we have found a box, we don't need to process the event further


	if (thisIsABox) {
	  //cout<<"box in layer"<<boxFoundInLayer<<endl;
	      	hevt->parameters().setValue("boxFoundInLayer", boxFoundInLayer);
			break;
	    }
	     else {
 	      hevt->parameters().setValue("boxFoundInLayer", 0);
 	   	    }
	 
    }
    return thisIsABox;
}


void
BoxEventFinder::processEvent(LCEvent* evt) { 
    // skip noise events
  if (m_RunNumber < 0) {
    return;
  }
  bool hasBox = false;
   
   for (size_t i=0; i<m_hitCollectionNames.size(); i++) {
        LCCollection* hitList = safelyGetCollection(evt, m_hitCollectionNames.at(i));
        if (not hitList) {
            continue;
        }
        CellIDDecoder<CalorimeterHit> idDecoder(hitList);

        //-- note: this will not be printed if compiled w/o MARLINDEBUG=1 !
        streamlog_out(DEBUG) << "   processing event: " << evt->getEventNumber() 
            << "   in run:  " << evt->getRunNumber() << std::endl;   
        map<int, vector<CalorimeterHit*> > layerHitMap = DhcalUtil::buildLayerHitMap(hitList);
        map<int, vector<CalorimeterHit*> >::iterator it = layerHitMap.begin();
        for ( ; it != layerHitMap.end(); ++it) {
	  if (isBoxEvent((*it).second, idDecoder,evt)) {
            	hasBox = true;
		
            	break;
            }
        }
        if (hasBox) {
        	break;
        }
    }
    if (hasBox) {
    	evt->parameters().setValue("hasBox", 1);
    	++m_nBoxEvents;
    	if (m_skipBoxEvents) {
    		throw SkipEventException(this);
    	}
    } else {
    	evt->parameters().setValue("hasBox", 0);
    }
    m_nEvt++;
}


void
BoxEventFinder::check(LCEvent* evt) { 
    // nothing to check here - could be used to fill checkplots in reconstruction processor
}


void BoxEventFinder::end() {
  streamlog_out(MESSAGE) << "Run " << m_RunNumber << " has " << m_nBoxEvents << " Box Events" << endl;
  streamlog_out(MESSAGE) << "BoxEventFinder::end()  " << name() 
			 << " processed " << m_nEvt << " events in " << m_nRun << " runs "
			 << endl;
}

}
