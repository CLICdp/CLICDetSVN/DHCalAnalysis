/*
 * WireChamberCalibrationProcessor.cc
 *
 *  Created on: Mar 5, 2013
 *      Author: Christian Grefe, CERN
 */

#include "WireChamberCalibrationProcessor.hh"

// lcio
#include "EVENT/LCCollection.h"
#include "EVENT/TrackerHit.h"
#include "IMPL/TrackerHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/CellIDDecoder.h"
#include "UTIL/CellIDEncoder.h"
#include "Exceptions.h"

// marlin
#include "marlin/Exceptions.h"

// root
#include "TVector3.h"

// c++
#include <cmath>

using EVENT::LCEvent;
using EVENT::TrackerData;
using EVENT::TrackerHit;
using IMPL::TrackerHitImpl;
using UTIL::CellIDDecoder;
using UTIL::CellIDEncoder;

using std::map;
using std::make_pair;
using std::pair;
using std::string;
using std::vector;

namespace CALICE {

WireChamberCalibrationProcessor aWireChamberCalibrationProcessor;

WireChamberCalibrationProcessor::WireChamberCalibrationProcessor() :
		Processor("WireChamberCalibrationProcessor") {
	_description = "";

	registerInputCollection(LCIO::TRACKERDATA, "InputCollection", "Input collection of raw tracker hits",
			_inputCollectionName, "WireChamberRawHits");
	registerOutputCollection(LCIO::TRACKERHIT, "OutputCollection", "Output collection of calibrated tracker hits",
			_outputCollectionName, "WireChamberHits");

	registerProcessorParameter("HorizontalSlopes",
			"Calibration values for the horizontal slope of the wire chambers (in mm/ns)", _horizontalSlopes,
			vector<float>());
	registerProcessorParameter("VerticalSlopes",
			"Calibration values for the vertical slope of the wire chambers (in mm/ns)", _verticalSlopes,
			vector<float>());
	registerProcessorParameter("HorizontalResolutions", "Horizontal resolutions of the wire chambers (in mm)",
			_horizontalResolutions, vector<float>());
	registerProcessorParameter("VerticalResolutions", "Vertical resolution of the wire chambers (in mm)",
			_verticalResolutions, vector<float>());
	registerProcessorParameter("XPositions", "X positions of the wire chambers (in mm)", _xPositions, vector<float>());
	registerProcessorParameter("YPositions", "Y positions of the wire chambers (in mm)", _yPositions, vector<float>());
	registerProcessorParameter("ZPositions", "Z positions of the wire chambers (in mm)", _zPositions, vector<float>());
	registerProcessorParameter("NS_per_count", "Re-calibrate the raw tracker data (in ns/count)", _ns_per_count,
			float(0.));
}

WireChamberCalibrationProcessor::~WireChamberCalibrationProcessor() {
	// TODO Auto-generated destructor stub
}

void WireChamberCalibrationProcessor::init() {
	printParameters();
	Size_t nWireChambers = _horizontalSlopes.size();
	if (_verticalSlopes.size() != nWireChambers or _horizontalResolutions.size() != nWireChambers
			or _verticalResolutions.size() != nWireChambers or _xPositions.size() != nWireChambers
			or _yPositions.size() != nWireChambers or _zPositions.size() != nWireChambers) {
		throw EVENT::Exception("Inconsistent length of input parameter lists");
	}
	for (int layer = 0; layer < (int) _horizontalSlopes.size(); layer++) {
		WireChamber wireChamber;
		wireChamber.position = TVector3(_xPositions[layer], _yPositions[layer], _zPositions[layer]);
		wireChamber.slopes[horizontal] = _horizontalSlopes[layer];
		wireChamber.slopes[vertical] = _verticalSlopes[layer];
		wireChamber.resolutions[horizontal] = _horizontalResolutions[layer];
		wireChamber.resolutions[vertical] = _verticalResolutions[layer];
		_wireChambers.insert(make_pair(layer, wireChamber));
	}
}

void WireChamberCalibrationProcessor::processEvent(LCEvent* event) {
	LCCollectionVec* outputCollection = new LCCollectionVec(LCIO::TRACKERHIT);
	CellIDEncoder<TrackerHitImpl> idEncoder("layer:8", outputCollection);
	try {
		LCCollection* inputCollection = event->getCollection(_inputCollectionName);
		vector<TrackerData*> rawHits;
		for (int iHit = 0; iHit < inputCollection->getNumberOfElements(); iHit++) {
			rawHits.push_back(dynamic_cast<TrackerData*>(inputCollection->getElementAt(iHit)));
		}

		CellIDDecoder<TrackerData> idDecoder(inputCollection);
		vector<TrackerData*>::const_iterator itRawHit = rawHits.begin();
		while (itRawHit != rawHits.end()) {
			TrackerData* rawHit = *itRawHit;
			int layer = idDecoder(rawHit)["layer"].value();
			bool isVertical = idDecoder(rawHit)["vertical"].value();
			bool isLeft = idDecoder(rawHit)["left"].value();
			WireChamber& wireChamber = _wireChambers[layer];
			WireChamberSide chamberSide = isVertical ? vertical : horizontal;
			ReadoutSide readoutSide = isLeft ? left : right;
			wireChamber.rawHits[chamberSide][readoutSide].push_back(rawHit);
			++itRawHit;
		}

		// optionally re-calibrate raw data
		float countToNS = 1.0;
		if (_ns_per_count != 0.0) {
			countToNS = _ns_per_count * inputCollection->parameters().getFloatVal("ns/count");
		}
		vector<TrackerData*>::const_iterator itHorizontalRight;
		vector<TrackerData*>::const_iterator itHorizontalLeft;
		vector<TrackerData*>::const_iterator itVerticalRight;
		vector<TrackerData*>::const_iterator itVerticalLeft;
		map<int, WireChamber>::iterator itWireChamber = _wireChambers.begin();
		while (itWireChamber != _wireChambers.end()) {
			int layer = itWireChamber->first;
			WireChamber& wireChamber = itWireChamber->second;

			// create the covariance matrix for this wire chamber
			vector<float> covarianceMatrix;
			covarianceMatrix.push_back(std::pow(wireChamber.resolutions[horizontal], 2)); // cov(x,x)
			covarianceMatrix.push_back(0.0); // cov(y,x)
			covarianceMatrix.push_back(std::pow(wireChamber.resolutions[vertical], 2)); // cov(y,y)
			covarianceMatrix.push_back(0.0); // cov(z,x)
			covarianceMatrix.push_back(0.0); // cov(z,y)
			covarianceMatrix.push_back(0.0); // cov(z,z)
			for (itHorizontalRight = wireChamber.rawHits[horizontal][right].begin();
					itHorizontalRight != wireChamber.rawHits[horizontal][right].end(); ++itHorizontalRight) {
				for (itHorizontalLeft = wireChamber.rawHits[horizontal][left].begin();
						itHorizontalLeft != wireChamber.rawHits[horizontal][left].end(); ++itHorizontalLeft) {
					double xPos = ((*itHorizontalRight)->getTime() - (*itHorizontalLeft)->getTime()) * countToNS
							* wireChamber.slopes[horizontal];
					for (itVerticalRight = wireChamber.rawHits[vertical][right].begin();
							itVerticalRight != wireChamber.rawHits[vertical][right].end(); ++itVerticalRight) {
						for (itVerticalLeft = wireChamber.rawHits[vertical][left].begin();
								itVerticalLeft != wireChamber.rawHits[vertical][left].end(); ++itVerticalLeft) {
							double yPos = ((*itVerticalRight)->getTime() - (*itVerticalLeft)->getTime()) * countToNS
									* wireChamber.slopes[vertical];
							TrackerHitImpl* wireChamberHit = new TrackerHitImpl();
							TVector3 hitPosition(xPos, yPos, 0.0);
							hitPosition += wireChamber.position;
							double position[3] = { hitPosition.X(), hitPosition.Y(), hitPosition.Z() };
							wireChamberHit->setPosition(position);
							wireChamberHit->setCovMatrix(covarianceMatrix);
							wireChamberHit->rawHits().push_back(*itHorizontalRight);
							wireChamberHit->rawHits().push_back(*itHorizontalLeft);
							wireChamberHit->rawHits().push_back(*itVerticalRight);
							wireChamberHit->rawHits().push_back(*itVerticalLeft);
							idEncoder.reset();
							idEncoder["layer"] = layer;
							idEncoder.setCellID(wireChamberHit);
							outputCollection->addElement(wireChamberHit);
						}
					}
				}
			}
			++itWireChamber;
		}
		clearWireChamberHits();
	} catch (lcio::DataNotAvailableException& e) {
		streamlog_out(WARNING)<< e.what() << std::endl;
	}
	event->addCollection(outputCollection, _outputCollectionName);
}

void WireChamberCalibrationProcessor::end() {

}

void WireChamberCalibrationProcessor::clearWireChamberHits() {
	map<int, WireChamber>::iterator itWireChamber = _wireChambers.begin();
	while (itWireChamber != _wireChambers.end()) {
		WireChamber& wireChamber = itWireChamber->second;
		wireChamber.rawHits[horizontal][left].clear();
		wireChamber.rawHits[horizontal][right].clear();
		wireChamber.rawHits[vertical][left].clear();
		wireChamber.rawHits[vertical][right].clear();
		++itWireChamber;
	}
}

} /* namespace CALICE */
