/*
 * DhcalProgressProcessor.cc
 *
 *  Created on: Feb 23, 2013
 *      Author: Christian Grefe, CERN
 */

#include "DhcalProgressProcessor.hh"

// lcio
#include "EVENT/LCParameters.h"

// marlin
#include "marlin/VerbosityLevels.h"

// logging
#include "streamlog/streamlog.h"

// c++
#include <iostream>

using EVENT::LCEvent;
using EVENT::LCParameters;
using EVENT::LCRunHeader;

namespace CALICE {

DhcalProgressProcessor aDhcalProgressProcessor;

DhcalProgressProcessor::DhcalProgressProcessor() :
		Processor("DhcalProgressProcessor"), _processedEvents(0) {
	_description = "DhcalProgressProcessor: print run information and print number of processed events";

	registerProcessorParameter("Interval", "Interval between progress update", _interval, 1000);
}

DhcalProgressProcessor::~DhcalProgressProcessor() {
	;
}

void DhcalProgressProcessor::init() {
	printParameters();
}

void DhcalProgressProcessor::processRunHeader(LCRunHeader* run) {
	streamlog_out( MESSAGE4 ) << "Run " << run->getRunNumber() << " (" << run->getParameters().getStringVal("TimeStamp") << "): "
	<< run->getParameters().getFloatVal("BeamMomentum") << " GeV [" << run->getParameters().getStringVal("RunType") << "]" << std::endl;
}

void DhcalProgressProcessor::processEvent(LCEvent* event) {
	_processedEvents++;
	if (_processedEvents % _interval == 0) {
		streamlog_out( MESSAGE4 )<< "Processed " << _processedEvents << " events" << std::endl;
	}
}

void DhcalProgressProcessor::end() {

}

} /* namespace CALICE */
