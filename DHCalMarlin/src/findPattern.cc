#include <findPattern.hh>

#include "Exceptions.h"

#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/BitField64.h"
#include "UTIL/CellIDDecoder.h"
#include "streamlog/streamlog.h"
#include <fstream>
#include <fstream>
#include <iostream>
#include "TStyle.h"
#include "TPad.h"

using EVENT::LCCollection;
using EVENT::LCEvent;
using EVENT::CalorimeterHit;
using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;
using std::string;

#include "IMPL/ClusterImpl.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include <fstream>
#include <vector>

//root
#include "TH1D.h"
#include "TTree.h"
#include "TFile.h"
#include "TString.h"
#include <algorithm>  
using namespace marlin;
using namespace lcio;
using namespace std;
#include "EventVariablesRootWriter.hh"
#include "DhcalUtil.hh"

#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"

#include <cmath>
#include <utility>
#include <sstream>
#include <stdio.h>
#include "DhcalMapping.hh"

using CALICE::DhcalUtil::calculateStandardDeviation;


using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;
using EVENT::Cluster;

using UTIL::CellIDDecoder;
using namespace CALICE;
using std::pair;
using std::map;
using std::sqrt;
using std::string;
using std::vector;

findPattern afindPattern;

findPattern::findPattern() : Processor("findPattern"){
  patternTFile=0;
  registerProcessorParameter("Collection", "The collection to be used", _inputCollection, string(""));
  registerProcessorParameter("RootFile", "Output Root FIle", _outputRootFile, string(""));
  registerProcessorParameter("CellFile", "Text file with cell idies", _cellFile, string(""));
  registerProcessorParameter("metaFile", "File to store meta data ",_metaFile,string(""));


  clearAll();
}

findPattern::~findPattern(){}
void findPattern::init(){
  totalNumberOfEvents=0;
  patterHisto = new TH2D("patternHisto","patternHitso",96,0,96,96,0,96);
  for(int i=0;i<54;i++){
    ostringstream oss;
    oss<<i;
    TString s = "histo"+oss.str();
    patternMap.insert(pair<int,TH2D*>(i,(TH2D*)patterHisto->Clone(s)));
  }
}
void findPattern::processRunHeader(LCRunHeader* run) {
  
}

void findPattern::processEvent(LCEvent* event){
  
  totalNumberOfEvents++;
  EVENT::LCCollection* collectionDhc =event->getCollection(_inputCollection);
  double numberOfHits=collectionDhc->getNumberOfElements();  
  vector<CalorimeterHit*> hits;
  CellIDDecoder<CalorimeterHit> idDecoder(collectionDhc);
  for (int iHit = 0; iHit < collectionDhc->getNumberOfElements(); iHit++){
    hits.push_back(dynamic_cast<CalorimeterHit*>(collectionDhc->getElementAt(iHit))); 
  }
  
  map<int, vector<CalorimeterHit*> > layerHitMap = CALICE::DhcalUtil::buildLayerHitMap(hits,idDecoder);
  // cout <<"number of hits "<<numberOfHits<<endl;
  for(int i=0; i<numberOfHits;i++){
    calohit=(EVENT::CalorimeterHit*)collectionDhc->getElementAt(i);
    int U = idDecoder(calohit)["i"];
    int V = idDecoder(calohit)["j"];
    int LAYER = idDecoder(calohit)["layer"];
    patternMap[LAYER]->Fill(U,V);           
  }
}
void findPattern::clearAll(){
}

void findPattern::end(){
  DhcalMapping * mapping =DhcalMapping::instance(); 
  //Here the the histograms for each laye is written to a TFile
  patternTFile =new TFile(_outputRootFile.c_str(),"RECREATE");
  ofstream filebadRPC;
  ofstream filebadChips;
  ofstream filedeadChips;
  ofstream filebadCells;
  ofstream filedeadCells;
  

  
  TString cellFile=TString(_cellFile);
  ofstream outfile;
  outfile.open(_metaFile.c_str(),ios_base::app);
  //myfile.open (_outputTextFile.c_str());
  TString badRPC=cellFile+"/badRPC.txt";
  filebadRPC.open (badRPC);
  TString badChips=cellFile+"/badChips.txt";
  filebadChips.open(badChips);
  TString deadChips=cellFile+"/deadChips.txt";
  filedeadChips.open(deadChips);
  TString badCells=cellFile+"/badCells.txt";
  filebadCells.open(badCells);
  TString deadCells=cellFile+"/deadCells.txt";
  filedeadCells.open(deadCells);
  
  map<int,TH2D*>::iterator it;
  int counting = 0;
  int nCellsInBadRPC=0;
  int nCellsInBadChips=0;
  int nCellsInDeadChips=0;
  int nCellsInBadCells=0;
  int nCellsInDeadCells=0;  
  //  This loop writes to file the histograms and the file with all the cellIDs
  for(it=patternMap.begin();it!=patternMap.end();++it){
    it->second->GetXaxis()->SetTitle("x-axis");
    it->second->GetYaxis()->SetTitle("y-axis");
    it->second->Write(); 
    ostringstream oss;
    oss<<counting;
    TString r = "Result"+oss.str();
    cout<<" i detektorlag "<<r<<endl;
    TH2D * results = findHotCells(it->second,r,totalNumberOfEvents);
    //run over all bins in results to find the hot cells
    int binx=results->GetXaxis()->GetNbins();
    int biny=results->GetYaxis()->GetNbins();
    for(int i=1;i<binx+1;i++){
      for(int j=1;j<biny+1;j++){
  	if(results->GetBinContent(i,j)==3||results->GetBinContent(i,j)==4){ 
  	  filebadCells<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl; 
  	  results->SetBinContent(i,j,1);
  	  nCellsInBadCells++;
  	}
  	else if(results->GetBinContent(i,j)>77&&results->GetBinContent(i,j)<100){ 
  	  filebadChips<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl; 
  	  results->SetBinContent(i,j,1);
  	  nCellsInBadChips++;
  	}
  	else if(results->GetBinContent(i,j)==6){
  	  results->SetBinContent(i,j,2);
  	  filedeadCells<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl; 
  	  nCellsInDeadCells++;
  	}
  	else if(results->GetBinContent(i,j)>100&&(results->GetBinContent(i,j)<150)){
  	  results->SetBinContent(i,j,2);
  	  filedeadChips<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl;
  	  nCellsInDeadChips++;
  	}
	
  	else if(results->GetBinContent(i,j)>190){
  	  results->SetBinContent(i,j,2);
  	  filebadRPC<<mapping->getCellIDFromIndices(i-1,j-1,counting)<<endl;
  	  nCellsInBadRPC++;
  	}
	
  	else{
  	  results->SetBinContent(i,j,0);
  	}
      }
    }
    
    results->SetBinContent(1,23,1);
    results->SetBinContent(1,24,1);
    results->SetBinContent(1,53,1);
    results->SetBinContent(1,54,1);
    results->SetBinContent(1,56,1);
    results->SetBinContent(1,57,1);
    results->SetBinContent(2,53,1);
    results->SetBinContent(2,54,1);
    results->SetBinContent(1,85,1);
    results->SetBinContent(1,86,1);
    results->SetBinContent(96,23,1);
    results->SetBinContent(96,24,1);
    results->SetBinContent(96,53,1);
    results->SetBinContent(96,54,1);
    results->SetBinContent(96,56,1);
    results->SetBinContent(96,57,1);
    results->SetBinContent(95,53,1);
    results->SetBinContent(95,54,1);
    results->SetBinContent(96,85,1);
    results->SetBinContent(96,86,1);
        
    
    results->Write();
    delete results;

    filebadCells<<mapping->getCellIDFromIndices(0,22,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,23,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,52,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,53,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,55,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,56,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(1,52,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(1,53,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,84,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(0,85,counting)<<endl;  
    filebadCells<<mapping->getCellIDFromIndices(95,22,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,23,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,52,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,53,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,55,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,56,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(94,52,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(94,53,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,84,counting)<<endl; 
    filebadCells<<mapping->getCellIDFromIndices(95,85,counting)<<endl;  
    nCellsInBadCells+=20;
    counting++;
  }
    outfile<<nCellsInBadRPC<<endl;
    outfile<<nCellsInBadChips<<endl;
    outfile<<nCellsInDeadChips<<endl;
    outfile<<nCellsInBadCells<<endl;
    outfile<<nCellsInDeadCells<<endl;
    outfile<<totalNumberOfEvents<<endl;
    outfile.close();
    filebadRPC.close();
    filebadChips.close();
    filedeadChips.close();
    filebadCells.close();
    filedeadCells.close();
    patternTFile->Write();
    patternTFile->Close();
}

TH2D * findPattern::findHotCells(TH2D * histToBeChecked,TString name, int totalNumberOfEvents){
  
  TH2D * Results = new TH2D(name,name,96,0.0,96.0,96,0.0,96.0);
  int totalNumberOfEntries=histToBeChecked->GetEntries();
  int averageNumberOfEntries=totalNumberOfEntries*1.0/(96.0*96.0);
  int averageNumberOfEntriesD;
  int binx=histToBeChecked->GetXaxis()->GetNbins();
  int biny=histToBeChecked->GetYaxis()->GetNbins();
  



//The ground connectors
  histToBeChecked->SetBinContent(1,23,0);
  histToBeChecked->SetBinContent(1,24,0);
  histToBeChecked->SetBinContent(1,53,0);
  histToBeChecked->SetBinContent(1,54,0);
  histToBeChecked->SetBinContent(1,56,0);
  histToBeChecked->SetBinContent(1,57,0);
  histToBeChecked->SetBinContent(2,53,0);
  histToBeChecked->SetBinContent(2,54,0);
  histToBeChecked->SetBinContent(1,85,0);
  histToBeChecked->SetBinContent(1,86,0);
  histToBeChecked->SetBinContent(96,23,0);
  histToBeChecked->SetBinContent(96,24,0);
  histToBeChecked->SetBinContent(96,53,0);
  histToBeChecked->SetBinContent(96,54,0);
  histToBeChecked->SetBinContent(95,53,0);
  histToBeChecked->SetBinContent(95,54,0);
  histToBeChecked->SetBinContent(95,56,0);
  histToBeChecked->SetBinContent(95,57,0);
  histToBeChecked->SetBinContent(96,85,0);
  histToBeChecked->SetBinContent(96,86,0);

  
  //Look for dead RPC
  int RPC1=0;
  int RPC2=0;
  int RPC3=0;
  
  for(int i=1;i<binx+1;i++){
    for(int j=1;j<33;j++){
      RPC1+=histToBeChecked->GetBinContent(i,j);
    }
    for(int j=33;j<65;j++){
      RPC2+=histToBeChecked->GetBinContent(i,j);
    }for(int j=65;j<97;j++){
      RPC3+=histToBeChecked->GetBinContent(i,j);
    }
  }
  
  if(RPC2<RPC1||RPC2<RPC3){
    for(int i=1;i<binx+1;i++){
      for(int j=33;j<65;j++){
	Results->SetBinContent(i,j,200);
      }
    }
    averageNumberOfEntries=(totalNumberOfEntries-RPC2)*1.0/(64*96.0);
  }
  
  if(RPC1*1.0/RPC3>5){
    for(int i=1;i<binx+1;i++){
      for(int j=65;j<97;j++){
	Results->SetBinContent(i,j,200);
      }
    }
    averageNumberOfEntries=(totalNumberOfEntries-RPC3)*1.0/(64*96.0);
  }

  if(RPC3*1.0/RPC1>5){
    for(int i=1;i<binx+1;i++){
      for(int j=1;j<33;j++){
	Results->SetBinContent(i,j,200);
      }
    }
    averageNumberOfEntries=(totalNumberOfEntries-RPC1)*1.0/(64*96.0);
  }
  
  
  if(averageNumberOfEntries<5){averageNumberOfEntries=5;}  
  
  //Look for dead chips by comparing all empty chips with the chips around it  
  double chipsContent[144]={0};
  double chipsInside[144][4] ={{0}};
  double chipsOutside[144][4]={{0}};
  int startx;
  int starty;
  int numberOfFilledChips=0;
  int numberOfDeadChips=0;



  for(int q=0;q<144;q++){
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    for(int i=startx;i<startx+8;i++){
      for(int j=starty;j<starty+8;j++){
  	chipsContent[q]+=histToBeChecked->GetBinContent(i,j);
      }
    }
  }

  for(int q=0;q<144;q++){
    double contentR=averageNumberOfEntries*8*8*0.07;
    numberOfFilledChips=0;
    if(chipsContent[q]>0){continue;}
    if(q!=143){
      if(chipsContent[q+1]>contentR){
	numberOfFilledChips+=1;
      }
    }
    if(q!=0){
      if(chipsContent[q-1]>contentR){
	numberOfFilledChips+=1;
      }
    }
    if(q<132){
      if(chipsContent[q+12]>contentR){
	numberOfFilledChips+=1;
      }
    }
    
    if(q>11){
      if(chipsContent[q-12]>contentR){
	numberOfFilledChips+=1;
      }
    }
    
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    if(numberOfFilledChips>1){
      numberOfDeadChips++;
      for(int i=startx;i<startx+8;i++){
  	for(int j=starty;j<starty+8;j++){
  	  Results->SetBinContent(i,j,110);
  	}
      }
    }
  }
    



  averageNumberOfEntries=(averageNumberOfEntries*96.0*96.0)/((96.0*96.0)-64.0*numberOfDeadChips);

  
  for(int q=0;q<144;q++){
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    
    for(int i=startx;i<startx+8;i++){
      chipsInside[q][0]+=histToBeChecked->GetBinContent(i,starty);
      chipsOutside[q][0]+=histToBeChecked->GetBinContent(i,starty-1);
    }
    
    for(int j=starty;j<starty+8;j++){
      chipsInside[q][1]+=histToBeChecked->GetBinContent(startx+7,j);
      chipsOutside[q][1]+=histToBeChecked->GetBinContent(startx+8,j);
    }
    
    for(int i=startx;i<startx+8;i++){
      chipsInside[q][2]+=histToBeChecked->GetBinContent(i,starty+7);
      chipsOutside[q][2]+=histToBeChecked->GetBinContent(i,starty+8);
    }
    for(int j=starty;j<starty+8;j++){
      chipsInside[q][3]+=histToBeChecked->GetBinContent(startx,j);
      chipsOutside[q][3]+=histToBeChecked->GetBinContent(startx-1,j);
    }
  }




  //Look for noisy chips by comparing the inside/outside boarders of a chip.
  double inside;
  double outside;
  int numberOfBadChips=0;
  int badEntries=0;
  int numberOfLarger=0;

  for(int q=0;q<144;q++){
    startx=(q%12)*8+1;
    starty=q/12*8+1;
    numberOfLarger=0;

    for(int i=0;i<4;i++){
      if(chipsInside[q][i]*1.0>chipsOutside[q][i]*2.5){
	numberOfLarger++;
      }
    }    
    
    inside =chipsInside[q][0]+chipsInside[q][1]+chipsInside[q][2]+chipsInside[q][3];
    outside =chipsOutside[q][0]+chipsOutside[q][1]+chipsOutside[q][2]+chipsOutside[q][3];
    if(1.0*inside/outside>4&&inside>averageNumberOfEntries*32*0.1&&numberOfLarger>3){
      numberOfBadChips++;
      for(int i=startx;i<startx+8;i++){
	for(int j=starty;j<starty+8;j++){
	  Results->SetBinContent(i,j,80);
	  badEntries=badEntries+histToBeChecked->GetBinContent(i,j);
	} 
      }
    }
  }

  averageNumberOfEntries=(averageNumberOfEntries*96.0*96.0-badEntries)/((96.0*96.0)-64.0*numberOfBadChips);
 
  averageNumberOfEntriesD=averageNumberOfEntries;

  //Finding single dead cells
  if(averageNumberOfEntriesD<15){averageNumberOfEntriesD=15;}
  double currentCell =histToBeChecked->GetBinContent(0,0);
  double nextCell =histToBeChecked->GetBinContent(0,0);
  bool deadArea=false;
  double dFactor=0.8;
  
  for(int i=1;i<binx+1;i++){
    currentCell=0;
    for(int j=1;j<biny+1;j++){
      currentCell=histToBeChecked->GetBinContent(i,j);
      nextCell=histToBeChecked->GetBinContent(i,j+1);
      if(currentCell>averageNumberOfEntriesD*dFactor&&nextCell==0){
	deadArea=true;
      }
      if((deadArea==true)&&(nextCell!=0)){
	deadArea=false;
      }
      if(deadArea&&Results->GetBinContent(i,j+1)<75){
	Results->SetBinContent(i,j+1,5);
      }
    }
  }
  
  for(int j=1;j<biny+1;j++){
    for(int i=1;i<biny+1;i++){
      currentCell=histToBeChecked->GetBinContent(i,j);
      nextCell=histToBeChecked->GetBinContent(i+1,j);
      if(currentCell>averageNumberOfEntriesD*dFactor&&nextCell==0){
	deadArea=true;
      }
      if((deadArea==true)&&(nextCell!=0)){
	deadArea=false;
      }
      if(deadArea){
	Results->Fill(i,j-1);
      }
    }
  }
  
  
  
 //Finding single hot cells
   
 currentCell =histToBeChecked->GetBinContent(0,0);
 nextCell =histToBeChecked->GetBinContent(0,0);
 bool hotArea=false;
 double factor;
 double factor2;
 double smallFactor=2;
 double largeFactor=3;
  //This is made extremly large because we don't want the algorithm to find hot cells in the centeral region until we have a better algorithm
  double multi =1000;
  for(int i=1;i<binx+1;i++){
    hotArea=false;
    for(int j=1;j<biny;j++){
      if(i<30||i>66||j<34||j>63){
	factor=6;
	factor2=smallFactor;
      }
      else{
	factor=multi;
	factor2=largeFactor;
      } 
      currentCell=histToBeChecked->GetBinContent(i,j);
      nextCell=histToBeChecked->GetBinContent(i,j+1); 
      if(nextCell<factor2*averageNumberOfEntries){
	hotArea=false;
	continue;
      }
      if(nextCell>factor*currentCell && hotArea==false&&histToBeChecked->GetBinContent(i,j)!=0){
	
	hotArea=true;
      }
      if(nextCell*factor <currentCell &&hotArea==true){
	hotArea=false;
      }
      if(hotArea&&Results->GetBinContent(i,j+1)<75){
	Results->SetBinContent(i,j+1,2);
      }
    }
  }

  for(int i=1;i<binx+1;i++){
    hotArea=false;
    for(int j=biny;j>1;j--){
      if(i<30||i>66||j<34||j>63){
	factor=6;
	factor2=smallFactor;
      }
      else{
	factor=multi;
	factor2=largeFactor;
      } 
      currentCell=histToBeChecked->GetBinContent(i,j);
      nextCell=histToBeChecked->GetBinContent(i,j-1);
      
      if(nextCell<factor2*averageNumberOfEntries){
	hotArea=false;
	continue;
      }
      if(nextCell>factor*currentCell && hotArea==false&&histToBeChecked->GetBinContent(i,j)!=0){
	hotArea=true;
      }
      if(nextCell*factor <currentCell &&hotArea==true){
	hotArea=false;
      }
      if(hotArea&&Results->GetBinContent(i,j-1)<75){
	Results->SetBinContent(i,j-1,2);
      }
    }
  }

 for(int j=1;j<biny+1;j++){
    hotArea=false;
    for(int i=1;i<binx;i++){
      if(i<30||i>66||j<34||j>63){
	factor=6;
	factor2=smallFactor;
      }
      else{
	factor=multi;
	factor2=largeFactor;
      }     
      currentCell=histToBeChecked->GetBinContent(i,j);
      nextCell=histToBeChecked->GetBinContent(i+1,j);
           
      if(nextCell<factor2*averageNumberOfEntries){
	hotArea=false;
	continue;
      }
      if(nextCell>factor*currentCell && hotArea==false&&histToBeChecked->GetBinContent(i,j)!=0){
	hotArea=true;
      }
      if(nextCell*factor <currentCell &&hotArea==true){
	hotArea=false;
      }
      if(hotArea){
	Results->Fill(i,j-1);
      }
    }
 }
 
 
 for(int j=1;j<biny+1;j++){
   hotArea=false;
   for(int i=binx;i>1;i--){
     if(i<30||i>66||j<34||j>63){
       factor=6;
       factor2=smallFactor;
     }
     else{
       factor=multi;
       factor2=largeFactor;
     }     
     currentCell=histToBeChecked->GetBinContent(i,j);
     nextCell=histToBeChecked->GetBinContent(i-1,j);
     
     
     if(nextCell<factor2*averageNumberOfEntries){
       hotArea=false;
       continue;
     }
     if(nextCell>factor*currentCell && hotArea==false&&histToBeChecked->GetBinContent(i,j)!=0) {
       
       hotArea=true;
      }
      if(nextCell*factor <currentCell &&hotArea==true){
	hotArea=false;
      }
      if(hotArea){
	Results->Fill(i-2,j-1);
      }
    }
  }
 return Results;
}



