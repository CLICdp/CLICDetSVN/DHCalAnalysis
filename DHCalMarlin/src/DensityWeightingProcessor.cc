/*
 * DensityWeightingProcessor.cc
 *
 *  Created on: Sept 21, 2014
 *      Author: Coralie Neubueser, DESY
 */

#include "DensityWeightingProcessor.hh"
#include "DhcalMapping.hh"
#include "DhcalUtil.hh"
#include "LayerColumnRowMap.hh"

// lcio
#include "IO/LCReader.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/SimCalorimeterHit.h"
#include "EVENT/LCEvent.h"
#include "EVENT/LCCollection.h"
#include "EVENT/LCRelation.h"
#include "EVENT/Cluster.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/LCTOOLS.h"
#include "UTIL/BitSet32.h"

// marlin
#include "marlin/VerbosityLevels.h"

// logging
#include "streamlog/streamlog.h"

#include "TROOT.h"
#include "TFile.h"
#include "TTree.h"
#include "TVector3.h"
#include "TH1.h"
#include "TH2.h"

// c++
#include <map>
#include <iostream>
#include <set>
#include "lcio.h"
#include <stdio.h>
#include <sstream>
#include <algorithm>
#include <vector>

using IO::LCReader;
using EVENT::LCEvent;
using EVENT::CalorimeterHit;
using EVENT::SimCalorimeterHit;
using EVENT::LCCollection;
using EVENT::LCRelation;
using EVENT::Cluster;
using IMPL::ClusterImpl;
using IMPL::LCCollectionVec;

using std::map;
using std::set;
using std::string;
using std::stringstream;
using std::vector;
using std::cout;
using namespace lcio ;

namespace CALICE {

  DensityWeightingProcessor aDensityWeightingProcessor;

  DensityWeightingProcessor::DensityWeightingProcessor() :
    Processor("DensityWeightingProcessor") {
    _description =
      "DensityWeightingProcessor: calculates weighting factors";

    _hitMap = 0;
    _compareHitMap = 0;
    _idDecoder = 0;
    _rootFile = 0;
 
    registerInputCollection(LCIO::CALORIMETERHIT, "InputCollections", "Name of input collections",
                             _inputCollectionName, _inputCollectionName);

    registerInputCollection(LCIO::CALORIMETERHIT, "CompareCollections", "Name of collections for comparison",
                             _compareCollectionName, _compareCollectionName);

    registerProcessorParameter("IdentifierU", "Name of the id identifier for U", _uIdentifier, DhcalMapping::instance()->U_ID());
    registerProcessorParameter("IdentifierV", "Name of the id identifier for V", _vIdentifier, DhcalMapping::instance()->V_ID());
    registerProcessorParameter("IdentifierLayer", "Name of the id identifier for layer", _layerIdentifier, DhcalMapping::instance()->LAYER_ID());

    registerProcessorParameter("RootFileName", "Name of ROOT file name for debug plots", _rootFileName, string(""));

  }

  DensityWeightingProcessor::~DensityWeightingProcessor() {
    if (_idDecoder) {
      delete _idDecoder;
    }
    if (_hitMap) {
      delete _hitMap;
    }
   if (_compareHitMap) {
      delete _compareHitMap;
    }
   if (_rootFile) {
      delete _rootFile;
    }
  }
  void DensityWeightingProcessor::init() {
    printParameters();
    _idDecoder = new CellIDDecoder<CalorimeterHit>(DhcalMapping::instance()->CELL_ID_ENCODING());
    _hitMap = new LayerColumnRowMap<CalorimeterHit*>(DhcalMapping::instance()->CELL_ID_ENCODING(), _uIdentifier, _vIdentifier, _layerIdentifier);
    _compareHitMap = new LayerColumnRowMap<CalorimeterHit*>(DhcalMapping::instance()->CELL_ID_ENCODING(), _uIdentifier, _vIdentifier, _layerIdentifier);
    _nNeighbours = new TH1D("_nNeighbours", "hits with n Neighbours",10,0,10);
    _refNNeighbours = new TH1D("_refNNeighbours", "hits with n Neighbours of ref sample",10,0,10);
    _weights = new TH1D("_weights", "weights",10,0,10);
  }

  void DensityWeightingProcessor::processEvent(LCEvent* event) {

    //streamlog_out( DEBUG4 ) << "Event    : " << event->getEventNumber() << std::endl;
           
    _hitMap->clear();
    _compareHitMap->clear(); 
    
    LCCollection* collection = event->getCollection(_inputCollectionName);
    //streamlog_out( DEBUG4 ) << "Collection name                         : " << _inputCollectionName << std::endl;
    //streamlog_out( DEBUG4 ) << "Hits                                    : " << collection->getNumberOfElements() << std::endl;
    
    _hitMap->fill(collection);
    
    LCCollection* refCollection = event->getCollection(_compareCollectionName); 
    //streamlog_out( DEBUG4 ) << "Collection name (ref)                    : " << _compareCollectionName << std::endl;
    //streamlog_out( DEBUG4 ) << "Hits            (ref)                    : " << refCollection->getNumberOfElements() << std::endl;
    
    _compareHitMap->fill(refCollection);
       
    //maps for digi hits and its densitz bin   
    std::map<int, int > myMap;
       
    int nNeighbours = NULL;
    
    
    std::vector <int> DigiHit_cellID;
    DigiHit_cellID.clear();
    
    for (int iHit=0; iHit < collection->getNumberOfElements(); iHit++) {
      CalorimeterHit* hit = (CalorimeterHit*) collection->getElementAt(iHit);
      long long cellID = DhcalMapping::instance()->getCellID(hit);
      //streamlog_out( DEBUG4 ) << "cellID of hit in digi sample : " << cellID << std::endl;
      nNeighbours = (int) _hitMap->getNeighbours(cellID,0,1,1).size();
      myMap[ DhcalMapping::instance()->getCellID(hit) ] = _hitMap->getNeighbours(cellID,0,1,1).size();
      DigiHit_cellID.push_back(cellID);

      //streamlog_out( DEBUG4 ) << "Neighbors of corresponding hit (cellID)      : " << nNeighbours << std::endl;  
      _nNeighbours->Fill(nNeighbours);
      
      for(int i=0; i<10; i++){
      	if(nNeighbours == i)
      	  _N1[i]++;
      } 
    }
    
    for(int iHit=0; iHit < refCollection->getNumberOfElements(); iHit++) {
      CalorimeterHit* hit = (CalorimeterHit*) refCollection->getElementAt(iHit);
      long long cellID = DhcalMapping::instance()->getCellID(hit);

      //streamlog_out( DEBUG4 ) << "cellID of hit in ref sample : " << cellID << std::endl;
      
      int NNeighbours = (int) _compareHitMap->getNeighbours(cellID,0,1,1).size();
      _refNNeighbours->Fill(NNeighbours);
      
      //find refSample cellID in Digi sample
      std::vector<int>::iterator it;
      it = std::find(DigiHit_cellID.begin(),DigiHit_cellID.end(),cellID);

      if(it != DigiHit_cellID.end()){
	//streamlog_out( DEBUG4 ) << "cellID of DigiHit found: " << *it << "get " << myMap[*it] << " neighbours assigned." << std::endl;	  
	  for(int k=0; k<10; k++){
	    if(myMap[*it]==k)
	      _N[k]++;}	
      }
      
      else{
	int NeighboursInDigiSample = _hitMap->getNeighbours(cellID,0,1,1).size();
	//streamlog_out( DEBUG4 ) << "Neighbours in digi sample for hit in ref sample : " << NeighboursInDigiSample << std::endl;
	for(int k=0; k<10; k++){
	  if(NeighboursInDigiSample==k)
	    _N[k]++;}
      }
      
      for(int i=0; i<10; i++){                                                                                                                                                                           
	if(NNeighbours == i)                                                                                                                                                                           
	  _N2[i]++;                                                                                                                                                                                    
      }                                                                                                                                                                                                
    }
    
  }
  
  void DensityWeightingProcessor::end() {
    double j[10]={0.,1.,2.,3.,4.,5.,6.,7.,8.,9.};
    double weight[10]={0};
    
    for(int i=0; i<10; i++){
      weight[i]=_N[i]/_N1[i];
      if( _N1[i]==0 )
	weight[i]=1;
      streamlog_out( DEBUG4 ) << "weights  : " << weight[i] << std::endl;

      _weights->Fill(j[i], weight[i]);
    }
    
    _rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
    _rootFile = gROOT->GetFile(_rootFileName.c_str());
    
    //    _rootFile->mkdir("hcal");
    //    _rootFile->cd("hcal");
    _nNeighbours->Write();
    _refNNeighbours->Write();
    _weights->Write();
    _rootFile->Close();

  }

} /* namespace CALICE */
