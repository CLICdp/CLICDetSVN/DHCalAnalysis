/*
 * SimpleLayerEfficiencyProcessor.cc
 *
 *  Created on: Dec 4, 2013
 *      Author: Christian Grefe, CERN
 */

#include "LayerColumnRowMap.hh"
#include "SimpleLayerEfficiencyProcessor.hh"

// lcio
#include "EVENT/LCCollection.h"
#include "EVENT/Cluster.h"

// root
#include "TFile.h"

// c++
#include <algorithm>
#include <map>
#include <utility>
#include <set>
#include <string>

using EVENT::Cluster;
using EVENT::LCCollection;
using EVENT::LCEvent;

using std::map;
using std::pair;
using std::make_pair;
using std::set;
using std::sort;
using std::string;
using std::stringstream;
using std::vector;

namespace CALICE {

SimpleLayerEfficiencyProcessor aSimpleLayerEfficiencyProcessor;

SimpleLayerEfficiencyProcessor::SimpleLayerEfficiencyProcessor() :
		Processor("SimpleLayerEfficiencyProcessor") {
	_description = "";
	_mapping = DhcalMapping::instance();

	registerInputCollection(LCIO::CALORIMETERHIT, "HitCollection", "", _hitCollectionName, string(""));
	registerInputCollection(LCIO::CLUSTER, "ClusterCollection", "", _clusterCollectionName, string(""));

	registerProcessorParameter("MinLayer", "", _minLayer, 0);
	registerProcessorParameter("MaxLayer", "", _maxLayer, _mapping->N_LAYERS() - 1);
	registerProcessorParameter("MinTowerHits", "", _minTowerHits, 35);
	registerProcessorParameter("HistogramMaxClusterSize", "", _histogramMaxClusterSize, 50);

	registerProcessorParameter("RootFileName", "Name of the ROOT output file", _rootFileName,
			string("layerClusterSize.root"));

}

SimpleLayerEfficiencyProcessor::~SimpleLayerEfficiencyProcessor() {
	;
}

void SimpleLayerEfficiencyProcessor::init() {
	printParameters();
	// sanity check
	if (_minLayer > _maxLayer) {
		std::swap(_minLayer, _maxLayer);
	}
	for (int iLayer = _minLayer; iLayer <= _maxLayer; iLayer++) {
		stringstream histogramBaseName;
		histogramBaseName << "ClusterSize_Layer" << iLayer;
		layerClusterSizeHistogram3DMap.insert(
				make_pair(iLayer,
						new TH3F(histogramBaseName.str().c_str(), histogramBaseName.str().c_str(),
								_mapping->N_CELLS_U(), -0.5, _mapping->N_CELLS_U() - 0.5, _mapping->N_CELLS_V(), -0.5,
								_mapping->N_CELLS_V() -0.5, _histogramMaxClusterSize, -0.5,
								_histogramMaxClusterSize - 0.5)));
	}
}

void SimpleLayerEfficiencyProcessor::processEvent(LCEvent* event) {
	LCCollection* hitCollection = event->getCollection(_hitCollectionName);
	LCCollection* clusterCollection = event->getCollection(_clusterCollectionName);

	map<const CalorimeterHit*, int> hitToClusterSizeMap;
	for (int index = 0; index < clusterCollection->getNumberOfElements(); index++) {
		Cluster* cluster = dynamic_cast<Cluster*>(clusterCollection->getElementAt(index));
		const vector<CalorimeterHit*>& clusterHits = cluster->getCalorimeterHits();
		vector<CalorimeterHit*>::const_iterator itHit = clusterHits.begin();
		while (itHit != clusterHits.end()) {
			hitToClusterSizeMap[*itHit] = clusterHits.size();
			++itHit;
		}
	}

	LayerColumnRowMap<CalorimeterHit*> hitMap;
	hitMap.fill(hitCollection);

	map<int, map<int, map<int, CalorimeterHit*> > >::const_iterator itRow = hitMap.begin();
	while (itRow != hitMap.end()) {
		int v = itRow->first;
		const map<int, map<int, CalorimeterHit*> >& columnMap = itRow->second;
		map<int, map<int, CalorimeterHit*> >::const_iterator itColumn = columnMap.begin();
		while (itColumn != columnMap.end()) {
			int u = itColumn->first;
			const map<int, CalorimeterHit*>& layerMap = itColumn->second;
			if ((int) layerMap.size() >= _minTowerHits) {
				for (int iLayer = _minLayer; iLayer <= _maxLayer; iLayer++) {
					if (layerMap.count(iLayer) > 0) {
						const CalorimeterHit* hit = layerMap.at(iLayer);
						layerClusterSizeHistogram3DMap[iLayer]->Fill(u, v, hitToClusterSizeMap[hit]);
					} else {
						layerClusterSizeHistogram3DMap[iLayer]->Fill(u, v, 0);
					}
				}
			}
			++itColumn;
		}
		++itRow;
	}
}

void SimpleLayerEfficiencyProcessor::end() {
	TFile* rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
	map<int, TH3F*>::iterator itMap3D = layerClusterSizeHistogram3DMap.begin();
	while (itMap3D != layerClusterSizeHistogram3DMap.end()) {
		itMap3D->second->Write();
		++itMap3D;
	}
	rootFile->Close();
	delete rootFile;
}

} /* namespace CALICE */
