/*
 * RPCSimProcessor2.cc
 *
 *  Created on: November 22, 2015
 *      Author: Coralie Neubüser, DESY
 */

#include "RPCSimProcessor2.hh"
#include "LayerColumnRowMap.hh"
#include "DhcalMapping.hh"

#include "EVENT/LCCollection.h"
#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/LCRelationImpl.h"
#include "UTIL/CellIDEncoder.h"
#include "UTIL/BitSet32.h"
#include "Exceptions.h"

#include "marlin/Exceptions.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TMath.h"
#include "TParameter.h"
#include "TF1.h"

#include <algorithm>
#include <list>
#include <vector>

using lcio::long64;

using EVENT::Exception;
using EVENT::DataNotAvailableException;
using EVENT::LCCollection;
using EVENT::LCEvent;
using EVENT::SimCalorimeterHit;
using EVENT::LCIO;

using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;
using IMPL::LCRelationImpl;

using marlin::ParseException;
using marlin::SkipEventException;

using std::list;
using std::map;
using std::set;
using std::sort;
using std::string;
using std::vector;

namespace CALICE {

RPCSimProcessor2 aRPCSimProcessor2;

RPCSimProcessor2::RPCSimProcessor2() :
		Processor("RPCSimProcessor2") {
	_description = "WDHCAL digitization processor wrapping the RPCSim models";
	_mapping = DhcalMapping::instance();
	_random = NULL;
	_rootFile = 0;
	_chargeIntegralMap = 0;
	_nCellNeighboursX = 0;
	_nCellNeighboursY = 0;
	_chargeIntegralMaxX = 0.;
	_chargeIntegralMaxY = 0.;

	registerInputCollection(LCIO::SIMCALORIMETERHIT, "InputCollection", "Name of input collection", _inputCollection,
			string("DhcSimHits"));

	registerOutputCollection(LCIO::CALORIMETERHIT, "OutputCollection",
			"Name of the output collection conatining all hits", _outputCollection, string("DhcHits"));
	registerOutputCollection(LCIO::CALORIMETERHIT, "OutputCollectionMainStack",
			"Name of the output collection containing only the main stack hits", _outputCollectionMainStack,
			string("DhcMainStackHits"));
	registerOutputCollection(LCIO::CALORIMETERHIT, "OutputCollectionTailCatcher",
			"Name of the output collection containing only the tail catcher hits", _outputCollectionTailCatcher,
			string("DhcTailCatcherHits"));

	registerOutputCollection(LCIO::LCRELATION, "RelationCollection",
			"Name of the collection storing the LCRelation between input and output hits.", _relationCollection,
			string("DigiHitRelations"));

	registerProcessorParameter("InputIdentifierU", "Cell ID identifier for U in input collection", _inputIdentifierU,
			string("i"));
	registerProcessorParameter("InputIdentifierV", "Cell ID identifier for V in input collection", _inputIdentifierV,
			string("j"));
	registerProcessorParameter("InputIdentifierLayer", "Cell ID identifier for layer in input collection",
			_inputIdentifierLayer, string("layer"));

	// default parameters for RPCSim3
	vector<float> defaultChargeSpreadParameters;
	defaultChargeSpreadParameters.push_back(0.678);
	defaultChargeSpreadParameters.push_back(6.71);
	defaultChargeSpreadParameters.push_back(0.345);

	registerProcessorParameter("ChargeSpreadModel", "Name of the charge spreading model", _chargeSpreadModelName,
				   string("RPCSim3"));
	registerProcessorParameter("ChargeSpreadParameters", "Parameters for the charge spreading model",
				   _chargeSpreadParameters, defaultChargeSpreadParameters);
	registerProcessorParameter("Threshold", "T used in simulation", _threshold, (double) 0.3645);
	registerProcessorParameter("DistCut", "Distance Cut used in simulation", _distanceCut, (double) 0.092);
        registerProcessorParameter("StepCut", "Step Cut used in simulation", _stepCut, (double) 0.001);
	registerProcessorParameter("Q0", "Signal Charge used in simulation (pC)", _q0, (double) 0.201);
	registerProcessorParameter("MaxRadius", "Maximum radius of the extent of the charge (cm)", _maxRadius,
				   (double) 4.0);
	registerProcessorParameter("IntegrationSteps", "Number of steps used in the charge spread", _integrationSteps,
				   100000);
	registerProcessorParameter("ChargeInterpolationBins",
				   "Number of bins used in the interpolated charge deposit lookup (over MaxRadius + CellSize/2)",
				   _chargeInterpolationBins, 200);
	registerProcessorParameter("ScalingParameter1", "parameter 1 for scaling of the charge", _scalingParameter1,
				   (double) 0.1);
        registerProcessorParameter("ScalingParameter2", "parameter 2 for scaling of the charge", _scalingParameter2,
				   (double) 0.5);
 	registerProcessorParameter("RandomSeed",
				   "Seed for the random number generator. A value of 0 results in a unique seed based on the current time.",
				   _randomSeed, 0);
        registerProcessorParameter("applyDistanceCut", "Use a hard distance cut",
				   _applyDistanceCut, false);
        registerProcessorParameter("ExponentialDistanceCut", "Use exponential distance cut instead of step function",
				   _exponentialDistanceCut, false);
  	registerProcessorParameter("ScalingCharge", "Scale charge according to distance to nearest hit",
				   _scalingCharge, false);
	registerProcessorParameter("ChargeReduction","charge reduction on RPC borders",
				   _chargeReductionAtBorders, true);
	registerProcessorParameter("RootFileName", "Name of ROOT file name for debug plots", _rootFileName, string(""));
	registerProcessorParameter("ChargeIntegralMapName", "Name of charge integral histogram in the ROOT file",
				   _chargeIntegralMapName, string("ChargeIntegralMap"));
	registerProcessorParameter("InputRootFileName", "Name of input ROOT file with a pre-calculated charge integral map",
				   _inputRootFileName, string(""));
}

RPCSimProcessor2::~RPCSimProcessor2() {
	if (_random) {
		delete _random;
	}
	map<string, TH1*>::iterator itMap1D = _histograms1D.begin();
	while (itMap1D != _histograms1D.end()) {
		delete itMap1D->second;
		++itMap1D;
	}
	_histograms1D.clear();
	map<string, TH2*>::iterator itMap2D = _histograms2D.begin();
	while (itMap2D != _histograms2D.end()) {
		delete itMap2D->second;
		++itMap2D;
	}
	if (_rootFile) {
		delete _rootFile;
	}
}

void RPCSimProcessor2::init() {
	printParameters();
	_random = new TRandom3(_randomSeed);

        _chargeGenerationFunction = new TF1("_chargeGenerationFunction", "0.1081*exp(-0.5*pow(x/0.0766,2)) + 0.08996*pow(x,0.5304)*exp(-x*0.9747)",0,50);
	
	// set up debug histograms
	if (not _rootFileName.empty()) {
		_rootFile = TFile::Open(_rootFileName.c_str(), "recreate");

		// 1D histograms
		_histograms1D["SimHits"] = new TH1D("SimHits", "SimHits;Simulated Hits", 1200, -0.5, 1199.5);
		_histograms1D["SimHitsAfterDistanceCut"] = new TH1D("SimHitsAfterDistanceCut",
				"SimHitsAfterDistanceCut;Simulated Hits (after Distance Cut)", 1200, -0.5, 1199.5);
		_histograms1D["DigiHits"] = new TH1D("DigiHits", "DigiHits;Digitized Hits", 1200, -0.5, 1199.5);
		_histograms1D["LayerSimHits"] = new TH1D("LayerSimHits", "LayerSimHits;Simulated Hits/Layer", 30, -0.5, 29.5);
		_histograms1D["LayerSimHitsAfterDistanceCut"] = new TH1D("LayerSimHitsAfterDistanceCut",
				"LayerSimHitsAfterDistanceCut;Simulated Hits/Layer (after Distance Cut)", 30, -0.5, 29.5);
		_histograms1D["LayerDigiHits"] = new TH1D("LayerDigiHits", "LayerDigiHits;Digitized Hits/Layer", 30, -0.5,
				29.5);
		_histograms1D["LayerSimHitsVsLayer"] = new TH1D("LayerSimHitsVsLayer",
				"LayerSimHitsVsLayer;Layer;Simulated Hits", _mapping->N_LAYERS(), -0.5, _mapping->N_LAYERS() - 0.5);
		_histograms1D["LayerSimHitsAfterDistanceCutVsLayer"] = new TH1D("LayerSimHitsAfterDistanceCutVsLayer",
				"LayerSimHitsAfterDistanceCutVsLayer;Layer;Simulated Hits (after Distance Cut)", _mapping->N_LAYERS(),
				-0.5, _mapping->N_LAYERS() - 0.5);
		_histograms1D["LayerDigiHitsVsLayer"] = new TH1D("LayerDigiHitsVsLayer",
				"LayerDigiHitsVsLayer;Layer; Digitized Hits", _mapping->N_LAYERS(), -0.5, _mapping->N_LAYERS() - 0.5);
		_histograms1D["SimHitDistance"] = new TH1D("SimHitDistance", "SimHit Distances;Distance [mm];Entries", 1000, 0.,
				100.);
                _histograms1D["SimHitSteps"] = new TH1D("SimHitSteps", "SimHit Steps;Step [mm];Entries", 1000, 0.,
							   10.);
		_histograms1D["scalingFactorsForCharge"] = new TH1D("scalingFactorsForCharge", "scaling factors for charge ;Entries", 100, 0.,
							   1.1);
 		_histograms1D["PadCharge"] = new TH1D("PadCharge", "Pad Charge;Pad Charge;Entries", 100, 0, 10 * _threshold);
		_histograms1D["GeneratedCharge"] = new TH1D("GeneratedCharge", "Generated Charge;Generated Charge;Entries",
				1000, 0, 100 * _threshold);

		// 2D histograms
		int binsX = 100;
		int binsY = 100;
		double minX = -500;
		double maxX = 500;
		double minY = -500;
		double maxY = 500;
		_histograms2D["SimHitsXY"] = new TH2D("SimHitsXY", "Simulated Hit Positions;x [mm];y [mm];Entries", binsX, minX,
				maxX, binsY, minY, maxY);
		_histograms2D["SimHitsAfterDistanceCutXY"] = new TH2D("SimHitsAfterDistanceCutXY",
				"Simulated Hit Positions (after Distance Cut);x [mm];y [mm];Entries", binsX, minX, maxX, binsY, minY,
				maxY);
		_histograms2D["ChargeDistribution"] = new TH2D("ChargeDistribution", "Charge Distribution;x [mm];y [mm];Charge",
				binsX, -_maxRadius, _maxRadius, binsY, -_maxRadius, _maxRadius);
		_histograms2D["PadChargeIJ"] = new TH2D("PadChargeIJ", "Pad Charge;i;j;Charge", _mapping->N_CELLS_U(), 0,
				_mapping->N_CELLS_U(), _mapping->N_CELLS_V(), 0, _mapping->N_CELLS_V());
		_histograms2D["DigiHitsXY"] = new TH2D("DigiHitsXY", "Digitized Hit Positions;x [mm];y [mm];Entries", binsX,
				minX, maxX, binsY, minY, maxY);
		_histograms2D["DigiHitsIJ"] = new TH2D("DigiHitsIJ", "Digitized Hit Cell IDs;i;j;Entries",
				_mapping->N_CELLS_U(), 0, _mapping->N_CELLS_U(), _mapping->N_CELLS_V(), 0, _mapping->N_CELLS_V());
	}

	double halfCellX = _mapping->CELL_SIZE_U() / 2.;
	double halfCellY = _mapping->CELL_SIZE_V() / 2.;
	if (not _inputRootFileName.empty()) {
		_inputRootFile = TFile::Open(_inputRootFileName.c_str(), "read");
		if (not _inputRootFile or not _inputRootFile->IsOpen()) {
			throw ParseException("Unable to open input file!");
		}
		_chargeIntegralMap = dynamic_cast<TH2D*>(_inputRootFile->Get(_chargeIntegralMapName.c_str()));
		if (not _chargeIntegralMap) {
			throw ParseException(string("Unable to read charge integral map: ") + _chargeIntegralMapName);
		}
		_chargeIntegralMaxX = _chargeIntegralMap->GetXaxis()->GetXmax();
		_chargeIntegralMaxY = _chargeIntegralMap->GetYaxis()->GetXmax();
		_nCellNeighboursX = TMath::Ceil((_chargeIntegralMaxX - halfCellX) / _mapping->CELL_SIZE_U());
		_nCellNeighboursY = TMath::Ceil((_chargeIntegralMaxY - halfCellY) / _mapping->CELL_SIZE_V());
	} else {
		RPCSimChargeSpreadModel* chargeSpreadModel = RPCSimFactory::instance()->createChargeSpreadModel(
				_chargeSpreadModelName);
		chargeSpreadModel->setMaxRadius(_maxRadius);
		chargeSpreadModel->setParameters(_chargeSpreadParameters);
		double chargeFraction = TMath::Pi() * TMath::Power(_maxRadius, 2) / double(_integrationSteps);
		_chargeIntegralMaxX = _maxRadius + halfCellX;
		_chargeIntegralMaxY = _maxRadius + halfCellY;
		double dx = _chargeIntegralMaxX / (double) _chargeInterpolationBins;
		double dy = _chargeIntegralMaxY / (double) _chargeInterpolationBins;
		_nCellNeighboursX = TMath::Ceil(_maxRadius / _mapping->CELL_SIZE_U());
		_nCellNeighboursY = TMath::Ceil(_maxRadius / _mapping->CELL_SIZE_V());
		_histograms2D[_chargeIntegralMapName] = new TH2D(_chargeIntegralMapName.c_str(),
				"ChargeIntegralMap;x [mm];y [mm];Charge", _chargeInterpolationBins, -0.5 * dx,
				_chargeIntegralMaxX + 0.5 * dx, _chargeInterpolationBins, -0.5 * dy, _chargeIntegralMaxY + 0.5 * dy);
		_chargeIntegralMap = _histograms2D[_chargeIntegralMapName];

		TVector3 chargePosition;
		for (int iStep = 0; iStep < _integrationSteps; iStep++) {
			double radius = std::numeric_limits<double>::max();
			// random position within the maximum radius, flat in x and y
			while (radius > _maxRadius) {
				chargePosition.SetX(_random->Uniform(-_maxRadius, _maxRadius));
				chargePosition.SetY(_random->Uniform(-_maxRadius, _maxRadius));
				chargePosition.SetZ(0.);
				radius = chargePosition.Perp();
			}
			double charge = chargeSpreadModel->calculateCharge(radius) * chargeFraction;

			// debug histogram
			if (_rootFile and _rootFile->IsOpen()) {
				_histograms2D["ChargeDistribution"]->Fill(chargePosition.x(), chargePosition.y(), charge);
			}

			// add the charge to the corresponding pad
			for (int ix = 0; ix < _chargeInterpolationBins; ix++) {
				double x = double(ix) * dx;
				if (TMath::Abs(x - chargePosition.x()) > halfCellX) {
					continue;
				}
				for (int iy = 0; iy < _chargeInterpolationBins; iy++) {
					double y = double(iy) * dy;
					if (TMath::Abs((y) - chargePosition.y()) > halfCellY) {
						continue;
					}
					_chargeIntegralMap->Fill(x, y, charge);
				}
			}
		}
	}
}

void RPCSimProcessor2::processEvent(LCEvent* event) {
	int eventNumber = event->getEventNumber();
	LCCollection* inputCollection = 0;

	// safely get the input collection
	try {
		inputCollection = event->getCollection(_inputCollection);
	} catch (DataNotAvailableException& e) {
		m_out(DEBUG) << " Collection " << _inputCollection << " not available in event " << eventNumber << std::endl;
		throw SkipEventException(this);
	}

	// extract the ID encoding used in the simulation
	string inputEncoding(inputCollection->getParameters().getStringVal(LCIO::CellIDEncoding));
	BitField64 inputDecoder(inputEncoding);

	// the main output collection
	LCCollectionVec* digiHits = new LCCollectionVec(LCIO::CALORIMETERHIT);
	digiHits->setFlag(UTIL::set_bit(digiHits->getFlag(), LCIO::RCHBIT_LONG));  // store position with hits
	digiHits->setFlag(UTIL::set_bit(digiHits->getFlag(), LCIO::RCHBIT_TIME));  // store time with hits

	// the output collection for main stack hits
	LCCollectionVec* digiMainStackHits = new LCCollectionVec(LCIO::CALORIMETERHIT);
	digiMainStackHits->setSubset(true);

	// the output collection for tail catcher hits
	LCCollectionVec* digiTailCatcherHits = new LCCollectionVec(LCIO::CALORIMETERHIT);
	digiTailCatcherHits->setSubset(true);

	// call cell ID encoder constructor for each collection to set the encoding string
	CellIDEncoder<CalorimeterHitImpl> idEncoder(_mapping->CELL_ID_ENCODING(), digiHits);
	CellIDEncoder<CalorimeterHitImpl> idEncoderMainStack(_mapping->CELL_ID_ENCODING(), digiMainStackHits);
	CellIDEncoder<CalorimeterHitImpl> idEncoderTailCatcher(_mapping->CELL_ID_ENCODING(), digiTailCatcherHits);

	// store LCRelations between the simulated hits and the corresponding digitized hits
	LCCollectionVec* relations = new LCCollectionVec(LCIO::LCRELATION);

	// store all simulated hits and make them accessible by cell indices
	LayerColumnRowMap<SimCalorimeterHit*> hitMap(inputEncoding, _inputIdentifierU, _inputIdentifierV,
			_inputIdentifierLayer);
	hitMap.fill(inputCollection);

	// counters for debug
	int nSimHits = 0;
	int nSimHitsAfterDistanceCut = 0;
	int nDigiHits = 0;

	// treat all layers individually
	for (int iLayer = 0; iLayer <= _mapping->N_LAYERS(); iLayer++) {
		// counters for debug
		int nDigiLayerHits = 0;

		// get all simulated hits recorded in this layer
		vector<SimCalorimeterHit*> layerHits = hitMap.getAll(iLayer, iLayer, 0, _mapping->N_CELLS_U(), 0,
				_mapping->N_CELLS_V());
		list<const HitContribution*> layerContributions;

		// collect all individual charge deposits of the hit
		vector<SimCalorimeterHit*>::const_iterator itHit = layerHits.begin();
		while (itHit != layerHits.end()) {
			SimCalorimeterHit* hit = *itHit;
			for (int iContribution = 0; iContribution < hit->getNMCContributions(); iContribution++) {
				TVector3 position(hit->getStepPosition(iContribution));
				//float time = hit->getTimeCont(iContribution);
				//std::cout << "time of simHit:  " << time <<"\n";
				//				if(time>200.) continue;
				// debug histogram
				if (_rootFile and _rootFile->IsOpen()) {
				  _histograms2D["SimHitsXY"]->Fill(position.x(), position.y());
				}
				// No charge generated outside of modules
				if (not _mapping->validPosition(position)) {
					m_out(DEBUG) << " Invalid hit position: x=" << position.x() << ", y=" << position.y() << ", z=" << position.z() << std::endl;
					continue;
				}
				layerContributions.push_back(
							     new HitContribution(position, hit->getTimeCont(iContribution),
										 hit->getEnergyCont(iContribution), hit));
			}
			++itHit;
		}

		int nSimLayerHits = layerContributions.size();
		nSimHits += nSimLayerHits;

		// Remove contributions according to distance cut
		if ( _applyDistanceCut )
		  applyDistanceCut(layerContributions);

                // get scaling factors for charges too clode to each other, saved in layerContributions
                if ( _scalingCharge )
                  ScaleCharge(layerContributions);

		int nSimLayerHitsAfterDistanceCut = layerContributions.size();
		nSimHitsAfterDistanceCut += nSimLayerHitsAfterDistanceCut;

		// spread charge for all layer contributions
		list<const HitContribution*>::iterator itContribution = layerContributions.begin();
		for (; itContribution != layerContributions.end(); ++itContribution) {
			const HitContribution* contribution = *itContribution;
			spreadCharge(contribution,iLayer);
			if (_rootFile and _rootFile->IsOpen()) {
				_histograms2D["SimHitsAfterDistanceCutXY"]->Fill(contribution->position.x(),
						contribution->position.y());
			}
		}

		// loop over all pads that have registered charge to create a hit
		const map<long64, PadContributions*>* cellIdPadMap = _layerPads.getCellIdMap();
		map<long64, PadContributions*>::const_iterator itMap = cellIdPadMap->begin();
		while (itMap != cellIdPadMap->end()) {
			const long64& cellID = itMap->first;
			long64 outputCellID = _mapping->getCellIDFromIndices(_mapping->getIndexU(cellID),
					_mapping->getIndexV(cellID), iLayer);
			if(cellID!=outputCellID)
			  std::cout << "outputCellID: " <<outputCellID<< " , cellID :" <<cellID<<"\n";
			PadContributions* pad = itMap->second;
			double padCharge = pad->getTotalCharge();
			if (_rootFile and _rootFile->IsOpen()) {
				_histograms1D["PadCharge"]->Fill(padCharge);
			}
			// check if charge in pad is above threshold
			if (padCharge > _threshold) {
				// create a new hit and add to output collections
				CalorimeterHitImpl* hit = new CalorimeterHitImpl();
				idEncoder.setValue(outputCellID);
				idEncoder.setCellID(hit);
				TVector3 position = _mapping->getPositionFromCellID(outputCellID);
				float positionArray[] = { (float) position.x(), (float) position.y(), (float) position.z() };
				hit->setTime(pad->getTime());
				hit->setPosition(positionArray);
				digiHits->addElement(hit);
				if (_mapping->isTailCatcher(idEncoder.getValue())) {
					digiTailCatcherHits->addElement(hit);
				} else {
					digiMainStackHits->addElement(hit);
				}
				nDigiHits++;
				nDigiLayerHits++;

				// create an LCRelation for all contributing SimCalorimeterHits
				const set<const HitContribution*>& contributions = pad->getContributions();
				set<const HitContribution*>::const_iterator itContribution = contributions.begin();
				while (itContribution != contributions.end()) {
					const HitContribution* contribution = *itContribution;
					relations->addElement(
							new LCRelationImpl(const_cast<SimCalorimeterHit*>(contribution->hit), hit, 1.0));
					itContribution++;
				}

				// debug histograms
				if (_rootFile and _rootFile->IsOpen()) {
					_histograms2D["DigiHitsXY"]->Fill(position.x(), position.y());
					_histograms2D["DigiHitsIJ"]->Fill(_mapping->getIndexU(cellID), _mapping->getIndexV(cellID));
					_histograms2D["PadChargeIJ"]->Fill(_mapping->getIndexU(cellID), _mapping->getIndexV(cellID),
							padCharge);
				}
			}
			++itMap;
		}

		// clean up the layer contributions
		itContribution = layerContributions.begin();
		while (itContribution != layerContributions.end()) {
			delete *itContribution;
			*itContribution = 0;
			++itContribution;
		}

		_layerPads.deleteObjects();

		// debug histograms
		if (_rootFile and _rootFile->IsOpen()) {
			_histograms1D["LayerSimHits"]->Fill(nSimLayerHits);
			_histograms1D["LayerSimHitsAfterDistanceCut"]->Fill(nSimLayerHitsAfterDistanceCut);
			_histograms1D["LayerDigiHits"]->Fill(nDigiLayerHits);
			_histograms1D["LayerSimHitsVsLayer"]->Fill(iLayer - 1, nSimLayerHits);
			_histograms1D["LayerSimHitsAfterDistanceCutVsLayer"]->Fill(iLayer - 1, nSimLayerHitsAfterDistanceCut);
			_histograms1D["LayerDigiHitsVsLayer"]->Fill(iLayer - 1, nDigiLayerHits);
		}
	}
	// debug histograms
	if (_rootFile and _rootFile->IsOpen()) {
		_histograms1D["SimHits"]->Fill(nSimHits);
		_histograms1D["SimHitsAfterDistanceCut"]->Fill(nSimHitsAfterDistanceCut);
		_histograms1D["DigiHits"]->Fill(nDigiHits);
	}

	// store the digitized hits
	event->addCollection(digiHits, _outputCollection);
	event->addCollection(digiMainStackHits, _outputCollectionMainStack);
	event->addCollection(digiTailCatcherHits, _outputCollectionTailCatcher);
	event->addCollection(relations, _relationCollection);
}

void RPCSimProcessor2::end() {
	// write the root file
	if (_rootFile and _rootFile->IsOpen()) {
		_rootFile->Write();
	}
	delete _chargeGenerationFunction;
}
  
  void RPCSimProcessor2::applyDistanceCut(list<const HitContribution*>& contributions) {
    // sort the contributions in time, earlier hits get priority
    contributions.sort(hitContributionComparator);
    list<const HitContribution*>::iterator itContribution = contributions.begin();
    list<const HitContribution*>::iterator itOtherContribution;
    // loop over all possible combinations of charge deposits
    //      std::cout << "contribution size:  " << contributions.size() << "\n";
    std::vector<TVector3> erasedContributionPosition;
    int firstLoop=0;
    int secondLoop=0;
    while (itContribution != contributions.end()) {
      const HitContribution* contribution = *itContribution;
      ModulePosition moduleID = _mapping->getModulePositionFromPosition(contribution->position);
      TVector3 Position = contribution->position;
      if( std::find(erasedContributionPosition.begin(),erasedContributionPosition.end(), Position) != erasedContributionPosition.end() ) std::cout << "looping over erased contribution! \n";
      itOtherContribution = itContribution;
      ++itOtherContribution;
      firstLoop++;

      while (itOtherContribution != contributions.end()) {
	secondLoop++;
	const HitContribution* otherContribution = *itOtherContribution;
	double distanceXY = (contribution->position - otherContribution->position).Perp();
	double distanceZ = fabs (contribution->position.z() - otherContribution->position.z());
	// remove the second charge deposit if too close
	bool removeHit = distanceXY < _distanceCut;
	bool removeStep = distanceZ < _stepCut;
	if (_exponentialDistanceCut) {
	  removeHit = _random->Rndm() < exp(-distanceXY / _distanceCut);
	}
	// remove the later contribution if step is too small
	if (removeStep and distanceXY < 0.5) {
	  itOtherContribution = contributions.erase(itOtherContribution);
	  erasedContributionPosition.push_back(otherContribution->position);
	}
	// only remove the other hit if it is in the same module
	else if (removeHit and _mapping->getModulePositionFromPosition(otherContribution->position) == moduleID) {
	  itOtherContribution = contributions.erase(itOtherContribution);
	  erasedContributionPosition.push_back(otherContribution->position);
	  //std::cout << "moduleID of erased hit:   " << moduleID << " , " << otherContribution->position.x() << "\n";
	}
	else {
	  ++itOtherContribution;
	}
	if (_rootFile and _rootFile->IsOpen()) {
	  _histograms1D["SimHitDistance"]->Fill(distanceXY);
	  if (distanceXY < 0.5)
	    _histograms1D["SimHitSteps"]->Fill(distanceZ);
	}
      }
      ++itContribution;
    }
  }


  void RPCSimProcessor2::ScaleCharge(list<const HitContribution*>& contributions) {
    // sort the contributions in time, earlier hits get priority
    contributions.sort(hitContributionComparator);
    list<const HitContribution*>::iterator itContribution = contributions.begin();
    list<const HitContribution*>::iterator itOtherContribution;

    //set charge = 1!
    while (itContribution != contributions.end()) {
      const HitContribution* contribution = *itContribution;
      contribution->charge = 1.;
      itContribution++;
    }
    
    int firstLoop=0;
    // loop over all possible combinations of charge deposits
    itContribution = contributions.begin();
    while (itContribution != contributions.end()){
      int secondLoop=0;
      const HitContribution* contribution = *itContribution;
      itOtherContribution = itContribution;
      ++itOtherContribution;
      TVector3 Position = contribution->position;
      firstLoop++;
      
      //loop to assign charge scaling factor dependent on distance
      while (itOtherContribution != contributions.end()){
	if (*itOtherContribution == contribution){
	  itOtherContribution++; 
	  continue;
	}
	secondLoop++;
	const HitContribution* otherContribution = *itOtherContribution;
	double distanceXY = (Position - otherContribution->position).Perp();
	
	double scalingFactor = 1.;
	// scale the second charge deposit down by
	double slope = 1/( _scalingParameter2 - _scalingParameter1);
	
	if (distanceXY <= _scalingParameter1)
	  scalingFactor = 0.;
	else if (distanceXY > _scalingParameter1 && distanceXY < _scalingParameter2)
	  scalingFactor = slope * distanceXY;
	else if (distanceXY >= _scalingParameter2)
	  scalingFactor = 1.;
	
	double charge = otherContribution->charge;
	otherContribution->charge = charge*scalingFactor;

	itOtherContribution++;
      }
      itContribution++;
    } 
  }
  
  void RPCSimProcessor2::spreadCharge(const HitContribution* contribution, int layer) {

        //get scaling factors
        double scalingFactor = contribution->charge;

        // randomly generate the total charge deposit
	double Q = _chargeGenerationFunction->GetRandom();
		
	if (_rootFile and _rootFile->IsOpen()) {
	  _histograms1D["GeneratedCharge"]->Fill(Q);
        }

	// include charge reductions at module boundaries
	if (_chargeReductionAtBorders){
	  TVector3 d = _mapping->distanceToModuleBoundary(contribution->position);
	  if (d.x() < 100.) {
	    Q = Q * TMath::Power(d.x() / 100., 0.7);
	  }
	  if (d.y() < 100.) {
	    Q = Q * TMath::Power(d.y() / 100., 0.25);
	  }
	  if (d.y() < 10.) {
	    Q = Q / 6.;
	  }
	}
	long64 cellID = _mapping->getCellIDFromPosition(contribution->position);

	int u = _mapping->getIndexU(cellID);
	int v = _mapping->getIndexV(cellID);
	ModulePosition moduleID;

	moduleID = _mapping->getModulePositionFromPosition(contribution->position);
	
	for (int iu = u - _nCellNeighboursX; iu <= u + _nCellNeighboursX; iu++) {
		for (int iv = v - _nCellNeighboursY; iv <= v + _nCellNeighboursY; iv++) {
			try {
				TVector3 cellPosition = _mapping->getPositionFromIndices(iu, iv, layer);
				TVector3 relativePosition = cellPosition - contribution->position;
				// check if the charge can actually reach this pad
				if (TMath::Abs(relativePosition.x()) >= _chargeIntegralMaxX
						or TMath::Abs(relativePosition.y()) >= _chargeIntegralMaxY) {
					continue;
				}
				long64 icellID = _mapping->getCellIDFromIndices(iu, iv, layer);
				// only allow charge sharing within the same module
				if (_mapping->getModulePositionFromPosition(cellPosition) != moduleID) {
				  continue; std::cout << "Module position: " << moduleID << "compared to " << _mapping->getModulePositionFromPosition(cellPosition) << "\n"; 
				}
				// get the bilinear interpolation of the pre-calculated &  scaled charge fraction in the pad
				double cellCharge = Q
						* _chargeIntegralMap->Interpolate(TMath::Abs(relativePosition.x()),
								TMath::Abs(relativePosition.y()));
				// scale charge
				if (_scalingCharge)
				  if (_rootFile and _rootFile->IsOpen()) {
				    _histograms1D["scalingFactorsForCharge"]->Fill(scalingFactor);
				  }
				  cellCharge = Q * scalingFactor
				    * _chargeIntegralMap->Interpolate(TMath::Abs(relativePosition.x()),
								      TMath::Abs(relativePosition.y()));
				// create a new object to hold the charge if this pad has not been hit yet
				if (not _layerPads.contains(icellID)) {
					_layerPads.fill(icellID, new PadContributions());
				}
				PadContributions* pad = _layerPads.getHit(icellID);
				if (cellCharge > 0) {
					pad->addContribution(contribution, cellCharge);
				}
			} catch (Exception& e) {
				continue;
			}
		}
	}
}

} /* namespace CALICE */
