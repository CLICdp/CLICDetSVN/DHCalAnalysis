/* WireChamberUtil.cc
 *
 * April 21st
 *
 * William Nash
 *
 * Heavily based off of DhcalUtil.cc by Christian Grefe
 */



#include <WireChamberUtil.hh>
#include <DhcalUtil.hh>


//misc
#include <ExtendedTrack.hh>


//root
#include <TMath.h>

using namespace std;

using EVENT::Exception;
using EVENT::TrackerHit;

namespace CALICE {


IMPL::ExtendedTrack* WireChamberUtil::ExtendedLineFit(const std::vector<EVENT::Cluster*>& clusters){
	vector<TVector3> positions;
	vector<TMatrixTSym<double> > covariances;
	vector<EVENT::Cluster*>::const_iterator itCluster;
	for (itCluster = clusters.begin(); itCluster != clusters.end(); itCluster++) {
		EVENT::Cluster* cluster = *itCluster;
		TVector3 position(cluster->getPosition());
		positions.push_back(position);
		TMatrixTSym<double> covarianceMatrix(3);
		const vector<float>& covariance = cluster->getPositionError();
		covarianceMatrix[0][0] = covariance[0];
		covarianceMatrix[1][0] = covariance[1];
		covarianceMatrix[1][1] = covariance[2];
		covarianceMatrix[2][0] = covariance[3];
		covarianceMatrix[2][1] = covariance[4];
		covarianceMatrix[2][2] = covariance[5];
		covariances.push_back(covarianceMatrix);
	}
	return dynamic_cast<IMPL::ExtendedTrack*>(ExtendedLineFit(positions, covariances));
}


IMPL::ExtendedTrack* WireChamberUtil::ExtendedLineFit(const vector<TVector3>& points, const vector<TMatrixTSym<double> >& covariances){
	// add all contributing hits to the fit
	int nPoints = points.size();
	if (nPoints != (int) covariances.size()) {
		throw Exception("DhcalUtil::straightLineTrackFit: Inconsistent length of points and covariance matrices");
	}
	vector<double> x;
	vector<double> y;
	vector<double> z;
	vector<double> xWeights;
	vector<double> yWeights;
	for (int index = 0; index < nPoints; index++) {
		const TVector3& point = points[index];
		x.push_back(point.X());
		y.push_back(point.Y());
		z.push_back(point.Z());
		const TMatrixTSym<double>& covariance = covariances[index];
		// here we assume independent measurements of x and y
		// need finite covariances for reasonable weights
		if (covariance[0][0] == 0.)
			throw Exception("DhcalUtil::straightLineTrackFit: Covariance for x is 0");
		if (covariance[1][1] == 0.)
			throw Exception("DhcalUtil::straightLineTrackFit: Covariance for y is 0");
		xWeights.push_back(1. / covariance[0][0]);
		yWeights.push_back(1. / covariance[1][1]);
	}

	StraightLineFit* xzFit = DhcalUtil::leastSquaresRegression(x, z, xWeights);
	StraightLineFit* yzFit = DhcalUtil::leastSquaresRegression(y, z, yWeights);

	IMPL::ExtendedTrack* trackFit = new IMPL::ExtendedTrack();

	float zRef = 0.0;
	pair<double, double> xRefResult = xzFit->calculate(zRef);
	pair<double, double> yRefResult = yzFit->calculate(zRef);
	float xRef = xRefResult.first;
	float yRef = yRefResult.first;
	float referencePoint[] = { xRef, yRef, zRef };
	TVector3 direction(xzFit->slope, yzFit->slope, 1.0);

	trackFit->setZ0(0.);
	trackFit->setPhi(direction.Phi());
	trackFit->setTanLambda(tan(TMath::PiOver2() - direction.Theta()));
	trackFit->setReferencePoint(referencePoint);

	trackFit->setChi2(xzFit->chisq + yzFit->chisq);
	trackFit->setXZchi2(xzFit->chisq);
	trackFit->setYZchi2(yzFit->chisq);

	trackFit->setNdf(xzFit->dof +yzFit->dof);
	trackFit->setXZndf(xzFit->dof);
	trackFit->setYZndf(yzFit->dof);

	trackFit->setXZintVar(xzFit->interceptVariance);
	trackFit->setYZintVar(yzFit->interceptVariance);
	trackFit->setXZslopeVar(xzFit->slopeVariance);
	trackFit->setYZslopeVar(yzFit->slopeVariance);
	trackFit->setXZslope(xzFit->slope);
	trackFit->setYZslope(yzFit->slope);

	delete xzFit;
	delete yzFit;

	return trackFit;
}

IMPL::ExtendedTrack* WireChamberUtil::ExtendedLineFit(const vector<EVENT::TrackerHit*>& hits){
	vector<TVector3> points;
	vector<TMatrixTSym<double> > covariances;
	vector<TrackerHit*>::const_iterator itHit;
	for (itHit = hits.begin(); itHit != hits.end(); itHit++) {
		TrackerHit* hit = *itHit;
		TVector3 position(hit->getPosition());
		points.push_back(position);
		TMatrixTSym<double> covarianceMatrix(3);
		const vector<float>& covariance = hit->getCovMatrix();
		covarianceMatrix[0][0] = covariance[0];
		covarianceMatrix[1][0] = covariance[1];
		covarianceMatrix[1][1] = covariance[2];
		covarianceMatrix[2][0] = covariance[3];
		covarianceMatrix[2][1] = covariance[4];
		covarianceMatrix[2][2] = covariance[5];
		covariances.push_back(covarianceMatrix);
	}
	IMPL::ExtendedTrack* trackFit = dynamic_cast<IMPL::ExtendedTrack*>(ExtendedLineFit(points, covariances));
	for (itHit = hits.begin(); itHit != hits.end(); itHit++){
		trackFit->addHit(*itHit);
	}
	trackFit->setHitsUsed(hits);
	return trackFit;
}


map<int, vector<TrackerHit*> > WireChamberUtil::buildHitMap(const LCCollection* collection,
		const string& identifier) {
	vector<TrackerHit*> WireChamberHits;
	CellIDDecoder<TrackerHit> idDecoder(collection);
	for (int iHit = 0; iHit < collection->getNumberOfElements(); iHit++) {
		WireChamberHits.push_back(dynamic_cast<TrackerHit*>(collection->getElementAt(iHit)));
	}
	map<int, vector<TrackerHit*> > hitMap;
	vector<TrackerHit*>::const_iterator itHit = WireChamberHits.begin();
	while (itHit != WireChamberHits.end()) {
		TrackerHit* hit = *itHit;
		int layer = idDecoder(hit)[identifier].value();
		hitMap[layer].push_back(hit);
		++itHit;
	}
	return hitMap;
}

vector<float> WireChamberUtil::ResolutionCalculator(TrackerHit* first, TrackerHit* second, TrackerHit* third){
	float x0 = first->getPosition()[0];
    float x1 = second->getPosition()[0];
    float x2 = third->getPosition()[0];
    float y0 = first->getPosition()[1];
    float y1 = second->getPosition()[1];
    float y2 = third->getPosition()[1];
    float z0 = first->getPosition()[2];
    float z1 = second->getPosition()[2];
    float z2 = third->getPosition()[2];

    float x0est = (z0-z1)*(x1-x2)/(z1-z2) + x1;
    float y0est = (z0-z1)*(y1-y2)/(z1-z2) + y1;
    vector<float> differences;
    differences.push_back(x0 - x0est);
    differences.push_back(y0 - y0est);

    return differences;
}


} /*Namespace CALICE*/


