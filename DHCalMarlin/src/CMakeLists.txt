SET( ${PROJECT_NAME}_LIBRARY_NAME  ${PROJECT_NAME}) 


###############################################
# include directories                         #
###############################################

INCLUDE_DIRECTORIES( BEFORE "${${PROJECT_NAME}_SOURCE_DIR}/include")
INCLUDE_DIRECTORIES( ${Marlin_INCLUDE_DIRS}
                     ${LCIO_INCLUDE_DIRS}
                     ${LCCD_INCLUDE_DIRS}
                     ${ROOT_INCLUDE_DIRS}  
                    )


###############################################
# sources                                     #
###############################################
ADD_DEFINITIONS( ${${PROJECT_NAME}_DEFINITIONS}        )
ADD_DEFINITIONS( ${${PROJECT_NAME}_EXPORT_DEFINITIONS} )

SET( ${${PROJECT_NAME}_LIBRARY_NAME}_srcs
	BoxEventFinder.cc
	CalorimeterHitComparator.cc
	DhcalProgressProcessor.cc
	DhcalMapping.cc
	DhcalMappingProcessor.cc
	DhcalUtil.cc
	EventVariablesRootWriter.cc
    HelloWorldProcessor.cc
    HitTimeSelectionProcessor.cc
    LayerCalibrationProcessor.cc
    LayerProfileProcessor.cc
    LayerColumnRowMap.cc
    LayerEfficiencyProcessor.cc
    MuonTracker.cc
    NNClustering.cc
    NNClusteringProcessor.cc
    RemoveHitsProcessor.cc
    SimpleLayerEfficiencyProcessor.cc
    StraightLine3D.cc
    WireChamberCalibrationProcessor.cc
    WireChamberLayerTest.cc
    WireChamberTracker.cc
    WireChamberPlotter.cc
    WireChamberUtil.cc
    RPCSimChargeSpreadModel.cc
    RPCSimProcessor.cc
    RPCSimProcessor2.cc
    findPattern.cc
    PreTest.cc
#    ParticleIdentificationProcessor.cc
#                     Processor2.cc
#                     Processor3.cc
)
#or, alternatively, you can do:
# AUX_SOURCE_DIRECTORY( . ${${PROJECT_NAME}_LIBRARY_NAME}_srcs)

######################################################
# what to build                                      #
######################################################
# require proper c++
SET_SOURCE_FILES_PROPERTIES(  ${${${PROJECT_NAME}_LIBRARY_NAME}_srcs} PROPERTIES
  COMPILE_FLAGS "-Wall -ansi" )

#ADD_LIBRARY( ${lib1name} ${${lib1name}_srcs} )
ADD_LIBRARY( ${${PROJECT_NAME}_LIBRARY_NAME} ${${${PROJECT_NAME}_LIBRARY_NAME}_srcs} )

SET_TARGET_PROPERTIES( ${${PROJECT_NAME}_LIBRARY_NAME} PROPERTIES
  VERSION ${${PROJECT_NAME}_VERSION}
  SOVERSION ${${PROJECT_NAME}_SOVERSION}
  # not needed at the moment (no build of
  # static and dynamic lib at the same time)
  # CLEAN_DIRECT_OUTPUT 1
  )

TARGET_LINK_LIBRARIES(${${PROJECT_NAME}_LIBRARY_NAME} ${CALICE_USERLIB_LIBRARIES})
TARGET_LINK_LIBRARIES(${${PROJECT_NAME}_LIBRARY_NAME} ${Marlin_LIBRARIES} )
TARGET_LINK_LIBRARIES(${${PROJECT_NAME}_LIBRARY_NAME} ${LCIO_LIBRARIES})
TARGET_LINK_LIBRARIES(${${PROJECT_NAME}_LIBRARY_NAME} ${LCCD_LIBRARIES})
TARGET_LINK_LIBRARIES(${${PROJECT_NAME}_LIBRARY_NAME} ${ROOT_LIBRARIES})

######################################################
# install                                            #
######################################################
INSTALL( DIRECTORY "${${PROJECT_NAME}_SOURCE_DIR}/include/" DESTINATION ${INCLUDE_INSTALL_DIR}
  PATTERN "*~" EXCLUDE
  PATTERN "*CVS*" EXCLUDE )


INSTALL( TARGETS ${${PROJECT_NAME}_LIBRARY_NAME}
  DESTINATION ${LIB_INSTALL_DIR}
  PERMISSIONS
  OWNER_READ OWNER_WRITE OWNER_EXECUTE
  GROUP_READ GROUP_EXECUTE
  WORLD_READ WORLD_EXECUTE  )


#####################################################
# prepare for XXXXConfig.cmake                      #
#####################################################

