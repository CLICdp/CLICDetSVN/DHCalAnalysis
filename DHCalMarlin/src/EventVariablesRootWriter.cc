/*
 * EventVariablesRootWriter.cc
 *
 *  Created on: Mar 5, 2013
 *      Author: Christian Grefe, CERN
 */

#include "EventVariablesRootWriter.hh"
#include "DhcalUtil.hh"
#include "LayerColumnRowMap.hh"

#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"

#include <cmath>
#include <utility>

using CALICE::DhcalUtil::calculateStandardDeviation;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;
using EVENT::Cluster;

using UTIL::CellIDDecoder;

using std::pair;
using std::map;
using std::sqrt;
using std::string;
using std::vector;

namespace CALICE {

EventVariablesRootWriter aEventVariablesRootWriter;

EventVariablesRootWriter::EventVariablesRootWriter() :
		Processor("EventVariablesRootWriter") {
	_description = "";
	vector<string> defaultCollections;
	defaultCollections.push_back("DhcalHits");
	registerInputCollections(LCIO::CALORIMETERHIT, "CalorimeterHitCollections", "Collections of all calorimeter hits",
			_hitCollectionNames, defaultCollections);
	registerInputCollection(LCIO::CLUSTER, "LayerClusterCollection", "Collections of clusters for individual layers",
			_layerClusterCollectionName, string(""));

	registerProcessorParameter("RootFileName", "Name of the ROOT file", _rootFileName, string("eventVariables.root"));
	registerProcessorParameter("RootTreeName", "Name of the ROOT tree", _rootTreeName, string("DhcEventVariables"));

	_rootFile = 0;
	_rootTree = 0;
	_interactionLayerEmptyLayerHits = 3;
	clearVariables();
}

EventVariablesRootWriter::~EventVariablesRootWriter() {
	delete _rootFile;
}

void EventVariablesRootWriter::init() {
	printParameters();

	_rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
	_rootTree = new TTree(_rootTreeName.c_str(), _rootTreeName.c_str());

	_rootTree->Branch("runNumber", &runNumber);
	_rootTree->Branch("eventNumber", &eventNumber);
	_rootTree->Branch("nHits", &nHits);
	_rootTree->Branch("interactionLayer", &interactionLayer);
	_rootTree->Branch("cerenkovA", &cerenkovA);
	_rootTree->Branch("cerenkovB", &cerenkovB);
	_rootTree->Branch("hasBox", &hasBox);
	_rootTree->Branch("energy", &totalEnergy);
	_rootTree->Branch("energy_0", &energy_0);
	_rootTree->Branch("energy_1", &energy_1);
	_rootTree->Branch("energy_2", &energy_2);
	_rootTree->Branch("energy_3", &energy_3);
	_rootTree->Branch("energy_4", &energy_4);
	_rootTree->Branch("energy_5", &energy_5);
	_rootTree->Branch("energy_6", &energy_6);
	_rootTree->Branch("energy_7", &energy_7);
	_rootTree->Branch("energy_8", &energy_8);
	_rootTree->Branch("hitDensity", &hitDensity);
	_rootTree->Branch("centerX", &centerX);
	_rootTree->Branch("centerY", &centerY);
	_rootTree->Branch("centerZ", &centerZ);
	_rootTree->Branch("centerU", &centerU);
	_rootTree->Branch("centerV", &centerV);
	_rootTree->Branch("centerLayer", &centerLayer);
	_rootTree->Branch("rmsX", &rmsX);
	_rootTree->Branch("rmsY", &rmsY);
	_rootTree->Branch("rmsXY", &rmsXY);
	_rootTree->Branch("rmsZ", &rmsZ);
	_rootTree->Branch("rmsU", &rmsU);
	_rootTree->Branch("rmsV", &rmsV);
	_rootTree->Branch("rmsUV", &rmsUV);
	_rootTree->Branch("rmsLayer", &rmsLayer);
	_rootTree->Branch("spillTime", &spillTime);

	_rootTree->Branch("layerNumber", &layerNumber);
	_rootTree->Branch("layerHits", &layerNHits);
	_rootTree->Branch("layerClusters", &layerNClusters);
	_rootTree->Branch("layerEnergy", &layerEnergy);
	_rootTree->Branch("layerCenterX", &layerCenterX);
	_rootTree->Branch("layerCenterY", &layerCenterY);
	_rootTree->Branch("layerCenterU", &layerCenterU);
	_rootTree->Branch("layerCenterV", &layerCenterV);
	_rootTree->Branch("layerRmsX", &layerRmsX);
	_rootTree->Branch("layerRmsY", &layerRmsY);
	_rootTree->Branch("layerRmsXY", &layerRmsXY);
	_rootTree->Branch("layerRmsU", &layerRmsU);
	_rootTree->Branch("layerRmsV", &layerRmsV);
	_rootTree->Branch("layerRmsUV", &layerRmsUV);
}

void EventVariablesRootWriter::processEvent(LCEvent* event) {
	vector<CalorimeterHit*> hits;
	CellIDDecoder<CalorimeterHit>* idDecoder = 0;
	LayerColumnRowMap<CalorimeterHit*> hitMap;

	vector<string>::const_iterator itHitCollectionName = _hitCollectionNames.begin();
	while (itHitCollectionName != _hitCollectionNames.end()) {
		const LCCollection* hitCollection = event->getCollection(*itHitCollectionName);
		if (!idDecoder) {
			idDecoder = new CellIDDecoder<CalorimeterHit>(hitCollection);
		}
		for (int iHit = 0; iHit < hitCollection->getNumberOfElements(); iHit++) {
			CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(hitCollection->getElementAt(iHit));
			hits.push_back(hit);
			hitMap.fill(hit);
		}
		++itHitCollectionName;
	}

	const LCCollection* layerClusterCollection = event->getCollection(_layerClusterCollectionName);

	map<int, vector<CalorimeterHit*> > layerHitMap = DhcalUtil::buildLayerHitMap(hits, *idDecoder);
	map<int, vector<Cluster*> > layerClusterMap = DhcalUtil::buildLayerClusterMap(layerClusterCollection);

	nHits = (int) hits.size();
	cerenkovA = (bool) event->getParameters().getIntVal("CerenkovA");
	cerenkovB = (bool) event->getParameters().getIntVal("CerenkovB");
	hasBox = (bool) event->getParameters().getIntVal("hasBox");
	spillTime = event->getParameters().getFloatVal("SecondsInSpill");
	if (nHits != 0) {
		hitDensity = (float) nHits / (float) layerHitMap.size();
	}

	vector<double> x;
	vector<double> y;
	vector<double> xy;
	vector<double> z;
	vector<double> U;
	vector<double> V;
	vector<double> UV;
	vector<double> layer;
	vector<double> weights;
	for (int iHit = 0; iHit < (int) nHits; iHit++) {
		const CalorimeterHit* hit = hits[iHit];
		TVector3 p(hit->getPosition());
		float weight = hit->getEnergy();
		if (weight == 0.) {
			weight = 1.;
		}
		int nNeighbours = (int) hitMap.getNeighbours(hit, 0, 1, 1).size();
		if (nNeighbours == 0) {
			energy_0 += weight;
		} else if (nNeighbours == 1) {
			energy_1 += weight;
		} else if (nNeighbours == 2) {
			energy_2 += weight;
		} else if (nNeighbours == 3) {
			energy_3 += weight;
		} else if (nNeighbours == 4) {
			energy_4 += weight;
		} else if (nNeighbours == 5) {
			energy_5 += weight;
		} else if (nNeighbours == 6) {
			energy_6 += weight;
		} else if (nNeighbours == 7) {
			energy_7 += weight;
		} else if (nNeighbours == 8) {
			energy_8 += weight;
		}
		totalEnergy += weight;
		x.push_back(p.X());
		y.push_back(p.Y());
		xy.push_back(p.Pt());
		z.push_back(p.Z());
		int uIndex = (*idDecoder)(hit)[DhcalMapping::instance()->U_ID()].value();
		int vIndex = (*idDecoder)(hit)[DhcalMapping::instance()->V_ID()].value();
		U.push_back((float) uIndex);
		V.push_back((float) vIndex);
		UV.push_back(sqrt(uIndex * uIndex + vIndex * vIndex));
		layer.push_back((float) (*idDecoder)(hit)[DhcalMapping::instance()->LAYER_ID()].value());
		weights.push_back(weight);
	}

	pair<double, double> xResult = calculateStandardDeviation(x, weights);
	pair<double, double> yResult = calculateStandardDeviation(y, weights);
	pair<double, double> xyResult = calculateStandardDeviation(xy, weights);
	pair<double, double> zResult = calculateStandardDeviation(z, weights);
	pair<double, double> uResult = calculateStandardDeviation(U, weights);
	pair<double, double> vResult = calculateStandardDeviation(V, weights);
	pair<double, double> uvResult = calculateStandardDeviation(UV, weights);
	pair<double, double> layerResult = calculateStandardDeviation(layer, weights);

	centerX = xResult.first;
	centerY = yResult.first;
	centerZ = zResult.first;
	centerU = uResult.first;
	centerV = vResult.first;
	centerLayer = layerResult.first;
	rmsX = xResult.second;
	rmsY = yResult.second;
	rmsXY = xyResult.second;
	rmsZ = zResult.second;
	rmsU = uResult.second;
	rmsV = vResult.second;
	rmsUV = uvResult.second;
	rmsLayer = layerResult.second;

	int previousLayerHits = 2;
	int beforePreviousLayerHits = 2;
	float threeLayerAverageHits = 2;
	float previousThreeLayerAverageHits = 2;
	float beforePreviousThreeLayerAverageHits = 2;

	for (int iLayer = 0; iLayer < DhcalMapping::instance()->N_LAYERS(); iLayer++) {
		vector<CalorimeterHit*>& layerHits = layerHitMap[iLayer];
		vector<Cluster*>& layerClusters = layerClusterMap[iLayer];

		layerNumber.push_back(iLayer);
		layerNHits.push_back((int) layerHits.size());
		layerNClusters.push_back((int) layerClusters.size());

		// calculate the center and spread for the hits in this layer
		x.clear();
		y.clear();
		xy.clear();
		U.clear();
		V.clear();
		UV.clear();
		weights.clear();
		double energy = 0.;
		vector<CalorimeterHit*>::const_iterator itHit = layerHits.begin();
		for (; itHit != layerHits.end(); ++itHit) {
			const CalorimeterHit* hit = *itHit;
			TVector3 p(hit->getPosition());
			float weight = hit->getEnergy();
			if (weight == 0.) {
				weight = 1.;
			}
			energy += weight;
			x.push_back(p.X());
			y.push_back(p.Y());
			xy.push_back(p.Pt());
			int uIndex = (*idDecoder)(hit)[DhcalMapping::instance()->U_ID()].value();
			int vIndex = (*idDecoder)(hit)[DhcalMapping::instance()->V_ID()].value();
			U.push_back((float) uIndex);
			V.push_back((float) vIndex);
			UV.push_back(sqrt(uIndex * uIndex + vIndex * vIndex));
			weights.push_back(weight);
		}
		xResult = calculateStandardDeviation(x, weights);
		yResult = calculateStandardDeviation(y, weights);
		xyResult = calculateStandardDeviation(xy, weights);
		uResult = calculateStandardDeviation(U, weights);
		vResult = calculateStandardDeviation(V, weights);
		uvResult = calculateStandardDeviation(UV, weights);
		layerEnergy.push_back(energy);
		layerCenterX.push_back(xResult.first);
		layerCenterY.push_back(yResult.first);
		layerCenterU.push_back(uResult.first);
		layerCenterV.push_back(vResult.first);
		layerRmsX.push_back(xResult.second);
		layerRmsY.push_back(yResult.second);
		layerRmsXY.push_back(xyResult.second);
		layerRmsU.push_back(uResult.second);
		layerRmsV.push_back(vResult.second);
		layerRmsUV.push_back(uvResult.second);

		// determine the interaction layer
		if (interactionLayer < 0) {
			int thisLayerHits = layerNHits.back();
			if (thisLayerHits == 0) {
				thisLayerHits = _interactionLayerEmptyLayerHits;
			}
			threeLayerAverageHits = (float) (beforePreviousLayerHits + previousLayerHits + thisLayerHits) / 3.;

			if (threeLayerAverageHits > previousThreeLayerAverageHits and threeLayerAverageHits > 3
					and threeLayerAverageHits > 2 * beforePreviousThreeLayerAverageHits) {
				if ((int) layerHitMap[iLayer + 1].size() + (int) layerHitMap[iLayer + 2].size() >= thisLayerHits) {
					interactionLayer = iLayer;
				}
			}
			beforePreviousLayerHits = previousLayerHits;
			previousLayerHits = thisLayerHits;
			beforePreviousThreeLayerAverageHits = previousThreeLayerAverageHits;
			previousThreeLayerAverageHits = threeLayerAverageHits;
		}
	}

	_rootTree->Fill();
	clearVariables();
	delete idDecoder;
}

void EventVariablesRootWriter::end() {
	_rootTree->Write();
	_rootFile->Close();
}

void EventVariablesRootWriter::clearVariables() {
	nHits = 0;
	interactionLayer = -1;
	cerenkovA = false;
	cerenkovB = false;
	hasBox = false;
	totalEnergy = 0.;
	energy_0 = 0.;
	energy_1 = 0.;
	energy_2 = 0.;
	energy_3 = 0.;
	energy_4 = 0.;
	energy_5 = 0.;
	energy_6 = 0.;
	energy_7 = 0.;
	energy_8 = 0.;
	hitDensity = 0;
	centerX = 0.;
	centerY = 0.;
	centerZ = 0.;
	centerU = 0.;
	centerV = 0.;
	centerLayer = 0.;
	rmsX = 0.;
	rmsY = 0.;
	rmsXY = 0.;
	rmsZ = 0.;
	rmsU = 0.;
	rmsV = 0.;
	rmsUV = 0.;
	rmsLayer = 0.;

	layerNumber.clear();
	layerNHits.clear();
	layerNClusters.clear();
	layerEnergy.clear();
	layerCenterX.clear();
	layerCenterY.clear();
	layerCenterU.clear();
	layerCenterV.clear();
	layerRmsX.clear();
	layerRmsY.clear();
	layerRmsXY.clear();
	layerRmsU.clear();
	layerRmsV.clear();
	layerRmsUV.clear();
}

} /* namespace CALICE */
