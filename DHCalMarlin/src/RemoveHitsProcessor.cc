/*
 * RemoveHitsProcessor.cc
 *
 *  Created on: Jun 7, 2013
 *      Author: Christian Grefe, CERN
 */

#include "RemoveHitsProcessor.hh"
#include "DhcalUtil.hh"

#include "Exceptions.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include "IMPL/LCCollectionVec.h"
#include "UTIL/BitField64.h"

#include "streamlog/streamlog.h"

#include <fstream>
#include <iostream>

using EVENT::LCCollection;
using EVENT::LCEvent;
using EVENT::CalorimeterHit;
using IMPL::LCCollectionVec;
using UTIL::BitField64;

using std::string;
using std::set;

namespace CALICE {

using DhcalUtil::getCellID;

RemoveHitsProcessor aRemoveHitsProcessor;

RemoveHitsProcessor::RemoveHitsProcessor() :
		Processor("RemoveHitsProcessor") {
	_description = "Removes all hits from a given collection that match any of the given cell IDs.";

	registerInputCollection(LCIO::CALORIMETERHIT, "InputCollection", "Name of input collections", _inputCollectionName,
			"DhcHits");
	registerOutputCollection(LCIO::CALORIMETERHIT, "OutputCollection", "Name of output collections", _outputCollectionName,
				"FilteredDhcHits");

	_dhcalMapping = DhcalMapping::instance();
}

RemoveHitsProcessor::~RemoveHitsProcessor() {
	// TODO Auto-generated destructor stub
}

void RemoveHitsProcessor::init() {
	printParameters();
}

void RemoveHitsProcessor::processEvent(LCEvent* event) {
	LCCollection* collection = event->getCollection(_inputCollectionName);
	LCCollectionVec* outputCollection = new LCCollectionVec(LCIO::CALORIMETERHIT);
	outputCollection->setFlag(collection->getFlag());
	outputCollection->setSubset(true);
	outputCollection->parameters().setValue(LCIO::CellIDEncoding, collection->getParameters().getStringVal(LCIO::CellIDEncoding));
	for (int index = 0; index < collection->getNumberOfElements(); index++) {
		CalorimeterHit* hit = dynamic_cast<CalorimeterHit*>(collection->getElementAt(index));
		if (not _dhcalMapping->isIgnoredCell(getCellID(hit))) {
			outputCollection->addElement(hit);
		} else {
			streamlog_out(DEBUG0) << "Removing hit with cell ID: " << getCellID(hit) << std::endl;
		}
	}
	event->addCollection(outputCollection, _outputCollectionName);
}

} /* namespace CALICE */
