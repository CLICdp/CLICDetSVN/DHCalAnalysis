/*
 * NewInteractionLayerTestProcessor.cc
 *
 *  Created on: Jul 17, 2013
 *      Author: Christian Reichelt, but based on original "EventVariablesRootWriter.cc" by Christian Grefe, CERN
 */

#include "NewInteractionLayerTestProcessor.hh"
#include "DhcalUtil.hh"

#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"

#include <cmath>
#include <utility>

using CALICE::DhcalUtil::calculateStandardDeviation;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;
using EVENT::Cluster;

using UTIL::CellIDDecoder;

using std::pair;
using std::map;
using std::sqrt;
using std::string;
using std::vector;

namespace CALICE {

NewInteractionLayerTestProcessor aNewInteractionLayerTestProcessor;

NewInteractionLayerTestProcessor::NewInteractionLayerTestProcessor() :
		Processor("NewInteractionLayerTestProcessor") {
	_description = "";
	vector<string> defaultCollections;
	defaultCollections.push_back("DhcalHits");
	registerInputCollections(LCIO::CALORIMETERHIT, "CalorimeterHitCollections", "Collections of all calorimeter hits",
			_hitCollectionNames, defaultCollections);
	registerInputCollection(LCIO::CLUSTER, "LayerClusterCollection", "Collections of clusters for individual layers",
			_layerClusterCollectionName, string(""));

	registerProcessorParameter("RootFileName", "Name of the ROOT file", _rootFileName, string("eventVariables.root"));
	registerProcessorParameter("RootTreeName", "Name of the ROOT tree", _rootTreeName, string("DhcEventVariables"));
	registerProcessorParameter("MinInteractionLayerHits",
			"Minimum number of hits required in two consecutive layers to identify the interaction layer",
			_minInteractionLayerHits, 3);

	_rootFile = 0;
	_rootTree = 0;
	clearVariables();
}

NewInteractionLayerTestProcessor::~NewInteractionLayerTestProcessor() {
	delete _rootFile;
}

void NewInteractionLayerTestProcessor::init() {
	printParameters();

	_rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
	_rootTree = new TTree(_rootTreeName.c_str(), _rootTreeName.c_str());

	_rootTree->Branch("nHits", &nHits);
	_rootTree->Branch("interactionLayer", &interactionLayer);
	_rootTree->Branch("cerenkovA", &cerenkovA);
	_rootTree->Branch("cerenkovB", &cerenkovB);
	_rootTree->Branch("hasBox", &hasBox);
	_rootTree->Branch("energy", &totalEnergy);
	_rootTree->Branch("hitDensity", &hitDensity);
	_rootTree->Branch("centerX", &centerX);
	_rootTree->Branch("centerY", &centerY);
	_rootTree->Branch("centerZ", &centerZ);
	_rootTree->Branch("centerU", &centerU);
	_rootTree->Branch("centerV", &centerV);
	_rootTree->Branch("centerLayer", &centerLayer);
	_rootTree->Branch("rmsX", &rmsX);
	_rootTree->Branch("rmsY", &rmsY);
	_rootTree->Branch("rmsXY", &rmsXY);
	_rootTree->Branch("rmsZ", &rmsZ);
	_rootTree->Branch("rmsU", &rmsU);
	_rootTree->Branch("rmsV", &rmsV);
	_rootTree->Branch("rmsUV", &rmsUV);
	_rootTree->Branch("rmsLayer", &rmsLayer);

	_rootTree->Branch("layerNumber", &layerNumber);
	_rootTree->Branch("layerHits", &layerNHits);
	_rootTree->Branch("layerClusters", &layerNClusters);
	_rootTree->Branch("layerEnergy", &layerEnergy);
	_rootTree->Branch("layerCenterX", &layerCenterX);
	_rootTree->Branch("layerCenterY", &layerCenterY);
	_rootTree->Branch("layerCenterU", &layerCenterU);
	_rootTree->Branch("layerCenterV", &layerCenterV);
	_rootTree->Branch("layerRmsX", &layerRmsX);
	_rootTree->Branch("layerRmsY", &layerRmsY);
	_rootTree->Branch("layerRmsXY", &layerRmsXY);
	_rootTree->Branch("layerRmsU", &layerRmsU);
	_rootTree->Branch("layerRmsV", &layerRmsV);
	_rootTree->Branch("layerRmsUV", &layerRmsUV);
}

void NewInteractionLayerTestProcessor::processEvent(LCEvent* event) {
	vector<CalorimeterHit*> hits;
	CellIDDecoder<CalorimeterHit>* idDecoder = 0;

	vector<string>::const_iterator itHitCollectionName = _hitCollectionNames.begin();
	while (itHitCollectionName != _hitCollectionNames.end()) {
		const LCCollection* hitCollection = event->getCollection(*itHitCollectionName);
		if (!idDecoder) {
			idDecoder = new CellIDDecoder<CalorimeterHit>(hitCollection);
		}
		for (int iHit = 0; iHit < hitCollection->getNumberOfElements(); iHit++) {
			hits.push_back(dynamic_cast<CalorimeterHit*>(hitCollection->getElementAt(iHit)));
		}
		++itHitCollectionName;
	}

	const LCCollection* layerClusterCollection = event->getCollection(_layerClusterCollectionName);

	map<int, vector<CalorimeterHit*> > layerHitMap = DhcalUtil::buildLayerHitMap(hits, *idDecoder);
	map<int, vector<Cluster*> > layerClusterMap = DhcalUtil::buildLayerClusterMap(layerClusterCollection);

	nHits = (int) hits.size();
	cerenkovA = (bool) event->getParameters().getIntVal("CerenkovA");
	cerenkovB = (bool) event->getParameters().getIntVal("CerenkovB");
	hasBox = (bool) event->getParameters().getIntVal("hasBox");
	if (nHits != 0) {
		hitDensity = (float) nHits / (float) layerHitMap.size();
	}

	vector<double> x;
	vector<double> y;
	vector<double> xy;
	vector<double> z;
	vector<double> U;
	vector<double> V;
	vector<double> UV;
	vector<double> layer;
	vector<double> weights;
	for (int iHit = 0; iHit < (int) nHits; iHit++) {
		const CalorimeterHit* hit = hits[iHit];
		TVector3 p(hit->getPosition());
		float weight = hit->getEnergy();
		totalEnergy += weight;
		if (weight == 0.) {
			weight = 1.;
		}
		x.push_back(p.X());
		y.push_back(p.Y());
		xy.push_back(p.Pt());
		z.push_back(p.Z());
		int uIndex = (*idDecoder)(hit)[DhcalUtil::U_ID()].value();
		int vIndex = (*idDecoder)(hit)[DhcalUtil::V_ID()].value();
		U.push_back((float) uIndex);
		V.push_back((float) vIndex);
		UV.push_back(sqrt(uIndex * uIndex + vIndex * vIndex));
		layer.push_back((float) (*idDecoder)(hit)[DhcalUtil::LAYER_ID()].value());
		weights.push_back(weight);
	}

	pair<double, double> xResult = calculateStandardDeviation(x, weights);
	pair<double, double> yResult = calculateStandardDeviation(y, weights);
	pair<double, double> xyResult = calculateStandardDeviation(xy, weights);
	pair<double, double> zResult = calculateStandardDeviation(z, weights);
	pair<double, double> uResult = calculateStandardDeviation(U, weights);
	pair<double, double> vResult = calculateStandardDeviation(V, weights);
	pair<double, double> uvResult = calculateStandardDeviation(UV, weights);
	pair<double, double> layerResult = calculateStandardDeviation(layer, weights);

	centerX = xResult.first;
	centerY = yResult.first;
	centerZ = zResult.first;
	centerU = uResult.first;
	centerV = vResult.first;
	centerLayer = layerResult.first;
	rmsX = xResult.second;
	rmsY = yResult.second;
	rmsXY = xyResult.second;
	rmsZ = zResult.second;
	rmsU = uResult.second;
	rmsV = vResult.second;
	rmsUV = uvResult.second;
	rmsLayer = layerResult.second;

	int prevLayerHits = 3;
	int prevprevLayerHits =3;
	float ThreeLayerAvgHits = 3;
	float prevThreeLayerAvgHits = 3;
	float prevprevThreeLayerAvgHits = 3;
	int NextHits = 0;
	int NextNextHits =0;

	for (int iLayer = 0; iLayer < DhcalUtil::N_LAYERS(); iLayer++) {
		vector<CalorimeterHit*>& layerHits = layerHitMap[iLayer];
		vector<Cluster*>& layerClusters = layerClusterMap[iLayer];

		layerNumber.push_back(iLayer);
		layerNHits.push_back((int) layerHits.size());
		layerNClusters.push_back((int) layerClusters.size());

		// calculate the center and spread for the hits in this layer
		x.clear();
		y.clear();
		xy.clear();
		U.clear();
		V.clear();
		UV.clear();
		weights.clear();
		double energy = 0.;
		vector<CalorimeterHit*>::const_iterator itHit = layerHits.begin();
		for ( ; itHit != layerHits.end(); ++itHit) {
			const CalorimeterHit* hit = *itHit;
			TVector3 p(hit->getPosition());
			float weight = hit->getEnergy();
			energy += weight;
			if (weight == 0.) {
				weight = 1.;
			}
			x.push_back(p.X());
			y.push_back(p.Y());
			xy.push_back(p.Pt());
			int uIndex = (*idDecoder)(hit)[DhcalUtil::U_ID()].value();
			int vIndex = (*idDecoder)(hit)[DhcalUtil::V_ID()].value();
			U.push_back((float) uIndex);
			V.push_back((float) vIndex);
			UV.push_back(sqrt(uIndex * uIndex + vIndex * vIndex));
			weights.push_back(weight);
		}
		xResult = calculateStandardDeviation(x, weights);
		yResult = calculateStandardDeviation(y, weights);
		xyResult = calculateStandardDeviation(xy, weights);
		uResult = calculateStandardDeviation(U, weights);
		vResult = calculateStandardDeviation(V, weights);
		uvResult = calculateStandardDeviation(UV, weights);
		layerEnergy.push_back(energy);
		layerCenterX.push_back(xResult.first);
		layerCenterY.push_back(yResult.first);
		layerCenterU.push_back(uResult.first);
		layerCenterV.push_back(vResult.first);
		layerRmsX.push_back(xResult.second);
		layerRmsY.push_back(yResult.second);
		layerRmsXY.push_back(xyResult.second);
		layerRmsU.push_back(uResult.second);
		layerRmsV.push_back(vResult.second);
		layerRmsUV.push_back(uvResult.second);

		// Determine interaction layer (To many hits in the first layer: Interaction layer -> 55 	No interaction layer found: Interaction layer -> 56)
		if (layerNHits.front() >= 30){		
						interactionLayer = 55;
						}

		if (interactionLayer > 55) {
			
			// Calculate Three-Layer-Average - layers with no hits are assigned a count of 2
			if (layerNHits.back() > 0){
			ThreeLayerAvgHits = (float) (prevprevLayerHits + prevLayerHits+layerNHits.back() ) / 3.;
			}

			if (layerNHits.back() < 1){
			ThreeLayerAvgHits = (float) (prevprevLayerHits + prevLayerHits + 2 ) / 3.;
			}

			// Conditions for an Interaction Layer is checked
			if (	iLayer > 0 and
				ThreeLayerAvgHits >= prevThreeLayerAvgHits and 
				ThreeLayerAvgHits >= 4 and
				ThreeLayerAvgHits > 2 * prevprevThreeLayerAvgHits ) {
				
				// Gets the layerhits for the next two layers
				vector<CalorimeterHit*>& layerHitsNext = layerHitMap[iLayer+1];
				vector<CalorimeterHit*>& layerHitsNextNext = layerHitMap[iLayer+2];
				NextHits = layerHitsNext.size();			
				NextNextHits = layerHitsNextNext.size();

				// If the sum of hits in the two following layers are greater than/equal to current layer the interaction layer is set
				if(NextHits + NextNextHits >= layerNHits.back() ) {
				interactionLayer = iLayer-1;
				}

			}

		

		}
		prevprevThreeLayerAvgHits = prevThreeLayerAvgHits;
		prevThreeLayerAvgHits = ThreeLayerAvgHits;

		prevprevLayerHits = prevLayerHits;
		
		// Sets the previous layer hits - layers with no hits are assigned a count of 3
		if (layerNHits.back() > 0){
			prevLayerHits = layerNHits.back();
		}

		if (layerNHits.back() < 1){
			prevLayerHits = 2;
		}

	}

	_rootTree->Fill();
	clearVariables();
	delete idDecoder;
}

void NewInteractionLayerTestProcessor::end() {
	_rootTree->Write();
	_rootFile->Close();
}

void NewInteractionLayerTestProcessor::clearVariables() {
	nHits = 0;
	interactionLayer = 56;
	cerenkovA = false;
	cerenkovB = false;
	hasBox = false;
	totalEnergy = 0.;
	hitDensity = 0;
	centerX = 0.;
	centerY = 0.;
	centerZ = 0.;
	centerU = 0.;
	centerV = 0.;
	centerLayer = 0.;
	rmsX = 0.;
	rmsY = 0.;
	rmsXY = 0.;
	rmsZ = 0.;
	rmsU = 0.;
	rmsV = 0.;
	rmsUV = 0.;
	rmsLayer = 0.;

	layerNumber.clear();
	layerNHits.clear();
	layerNClusters.clear();
	layerEnergy.clear();
	layerCenterX.clear();
	layerCenterY.clear();
	layerCenterU.clear();
	layerCenterV.clear();
	layerRmsX.clear();
	layerRmsY.clear();
	layerRmsXY.clear();
	layerRmsU.clear();
	layerRmsV.clear();
	layerRmsUV.clear();
}

} /* namespace CALICE */
