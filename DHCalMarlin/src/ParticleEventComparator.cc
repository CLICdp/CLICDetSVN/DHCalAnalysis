/*
 * ParticleEventComparator.cc
 *
 *	Finds a collection of reconstructed particles
 *	and writes out a root file containing information
 *	about it as well as its event
 *
 *  Created on: Aug 21, 2013
 *      Author: William Nash, CERN
 */

#include "ParticleEventComparator.hh"
#include "TrueEventVariablesRootWriter.hh"
#include "DhcalUtil.hh"

//lcio
#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"
#include "marlin/Exceptions.h"
#include "UTIL/BitField64.h"
#include "EVENT/ReconstructedParticle.h"
#include "EVENT/Track.h"

//c++
#include <cmath>
#include <iostream>
#include <fstream>
#include <utility>

//root
#include <TMath.h>


using CALICE::DhcalUtil::calculateStandardDeviation;

using EVENT::LCEvent;
using EVENT::LCCollection;
using EVENT::CalorimeterHit;
using EVENT::Cluster;

using UTIL::CellIDDecoder;

using std::map;
using std::string;
using std::vector;
using std::string;

namespace CALICE {

ParticleEventComparator aParticleEventComparator;

ParticleEventComparator::ParticleEventComparator() :
		Processor("ParticleEventComparator") {
	_description = "";
	vector<string> defaultCollections;
	defaultCollections.push_back("DhcalHits");
	registerInputCollections(LCIO::CALORIMETERHIT, "CalorimeterHitCollections", "Collections of all calorimeter hits",
			_hitCollectionNames, defaultCollections);

	registerProcessorParameter("ParticleName", "Name of Particle", _particleName, string(""));
	registerProcessorParameter("RootFileName", "Name of the ROOT file", _rootFileName, string("eventVariables.root"));
	registerProcessorParameter("RootTreeName", "Name of the ROOT tree", _rootTreeName, string("DhcEventVariables"));
	registerProcessorParameter("MinInteractionLayerHits",
			"Minimum number of hits required in two consecutive layers to identify the interaction layer",
			_minInteractionLayerHits, 3);

	registerProcessorParameter("CalibrationFile", "File name with the calibration values", _efficiencyFileName,
				string(""));
	registerProcessorParameter("BadLayers", "Layers which are excluded from analysis", _badLayers, vector<int>());
	registerProcessorParameter("MinimumEfficiency", "Minimum Efficiency before called a Dead RPC", _minEfficiency, (float) 0.2);

	_rootFile = 0;
	_rootTree = 0;
	clearVariables();
}

ParticleEventComparator::~ParticleEventComparator() {
	delete _rootFile;
}

void ParticleEventComparator::init() {
	printParameters();

	_rootFile = TFile::Open(_rootFileName.c_str(), "recreate");
	_rootTree = new TTree(_rootTreeName.c_str(), _rootTreeName.c_str());

	_rootTree->Branch("dhcalTrackTheta", &dhcalTrackTheta);
	_rootTree->Branch("wireChamberTheta", &wireChamberTheta);
	_rootTree->Branch("nHits", &nHits);
	_rootTree->Branch("energy", &totalEnergy);
	_rootTree->Branch("hitDensity", &hitDensity);
	_rootTree->Branch("energyDensity", &energyDensity);

	string line;
	std::ifstream inputFile;
	inputFile.open(_efficiencyFileName.c_str());
	int layerNumber;
	int moduleNumber;
	float calibrationValue;
	float efficiency;
	float multiplicity;
	if (inputFile.is_open()) {
		while (inputFile.good()) {
			inputFile >> layerNumber;
			inputFile >> moduleNumber;
			inputFile >> calibrationValue;
			inputFile >> efficiency;
			inputFile >> multiplicity;
			ModulePosition position = static_cast<ModulePosition>(moduleNumber);
			_efficiencyMap[layerNumber][position] = efficiency;
		}
	} else {
		throw Exception("Unable to read calibration values from " + _efficiencyFileName);
	}
}

void ParticleEventComparator::processEvent(LCEvent* event) {
	vector<CalorimeterHit*> hits;
	LCCollection* particleCollection = safelyGetCollection(event, _particleName);

	if(!particleCollection){
		streamlog_out(DEBUG) << "Particle collection: " << _particleName << " not found in event " << event->getEventNumber() << std::endl;
		return;
	}

	ReconstructedParticle* particle = dynamic_cast<ReconstructedParticle*>(particleCollection->getElementAt(0));
	Track* particleTrack = dynamic_cast<Track*>(particle->getTracks()[0]);
	dhcalTrackTheta = TMath::PiOver2() - atan(particleTrack->getTanLambda());

	//From here we will only have events with the particle found in it
	LCCollection* hitCollection = 0;
	vector<string>::const_iterator itHitCollectionName = _hitCollectionNames.begin();
	while (itHitCollectionName != _hitCollectionNames.end()) {
		hitCollection = safelyGetCollection(event, *itHitCollectionName);
		if(!hitCollection){
			streamlog_out(DEBUG) << "Particle collection: " << *itHitCollectionName << " not found" << std::endl;
			return;
		}
		for (int iHit = 0; iHit < hitCollection->getNumberOfElements(); iHit++) {
			hits.push_back(dynamic_cast<CalorimeterHit*>(hitCollection->getElementAt(iHit)));
		}
		++itHitCollectionName;
	}

	CellIDDecoder<CalorimeterHit> idDecoder = CellIDDecoder<CalorimeterHit>(hitCollection);

	map<int, vector<CalorimeterHit*> > layerHitMap = DhcalUtil::buildLayerHitMap(hits, idDecoder);
	DhcalMapping* mapping = DhcalMapping::instance();
	map<int, bool> GoodActiveLayers;

	for (int iLayer = 0; iLayer < DhcalUtil::N_LAYERS(); iLayer++) {
		vector<CalorimeterHit*>& layerHits = layerHitMap[iLayer];
		if(layerHits.size() != 0 and layerCheck(iLayer)){  //Bad or Weird Layers, regardless of efficiency
			vector<CalorimeterHit*>::const_iterator itHit = layerHits.begin();
			for ( ; itHit != layerHits.end(); ++itHit) {
				const CalorimeterHit* hit = *itHit;
				const BitField64& bitField = idDecoder(hit);
				long64 cellID = (const_cast<BitField64&>(bitField)).getValue();
				ModulePosition modulePosition = mapping->getModulePositionFromCellID(cellID);
				if(_efficiencyMap[iLayer][modulePosition] > _minEfficiency){ //Checks if the Efficiency of the layer is above threshold
					GoodActiveLayers[iLayer] = true;
					nHits++;
					totalEnergy+=1./_efficiencyMap[iLayer][modulePosition]; //weights the hit according to the RPC's efficiency
				}
			}
		}
	}

	if (nHits != 0 and GoodActiveLayers.size() != 0) {
		hitDensity = (float) nHits / (float) GoodActiveLayers.size();
		energyDensity = (float) totalEnergy/(float) GoodActiveLayers.size();
	}

	string WireChamberCollection = "WireChamberTracks";
	LCCollection* wireChamberTracks = safelyGetCollection(event, WireChamberCollection);
	//Not necessary that we have a wire chamber track
	if(!wireChamberTracks){
		streamlog_out(DEBUG) << "Particle collection: " << WireChamberCollection << " not found" << std::endl;
	}else{
		Track* wireChamberTrack = dynamic_cast<Track*>(wireChamberTracks->getElementAt(0));
		wireChamberTheta = TMath::PiOver2() - atan(wireChamberTrack->getTanLambda());
	}

	_rootTree->Fill();
	clearVariables();
}


void ParticleEventComparator::end() {
	_rootTree->Write();
	std::cout << "Wrote output to file: " << _rootFileName << std::endl;
	_rootFile->Close();
}

bool ParticleEventComparator::layerCheck(int Layer){
	vector<int>::const_iterator itLayer;
	bool goodLayer = true;
	for(itLayer = _badLayers.begin(); itLayer != _badLayers.end(); ++itLayer){
		if(Layer == (*itLayer)){
			goodLayer = false;
		}
	}
	return goodLayer;
}

LCCollection* ParticleEventComparator::safelyGetCollection(LCEvent* event, string collection){
	LCCollection* returnCollection;
	try{
		returnCollection = event->getCollection(collection);
	}catch(lcio::DataNotAvailableException e){
		streamlog_out(DEBUG) << "Particle collection: " << collection << " not found in event "<< event->getEventNumber() << std::endl;
		return 0;
	}
	return returnCollection;
}

void ParticleEventComparator::clearVariables() {
	dhcalTrackTheta=0.;
	wireChamberTheta=-1.;
	nHits = 0;
	totalEnergy = 0.;
	hitDensity = 0;
	energyDensity = 0;

}

} /* namespace CALICE */
