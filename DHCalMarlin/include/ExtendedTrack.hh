#ifndef EXTENDED_TRACK_H
#define EXTENDED_TRACK_H 1

#include "IMPL/TrackImpl.h"
#include "EVENT/TrackerHit.h"

namespace IMPL{


class ExtendedTrack : public IMPL::TrackImpl{
	public:
		ExtendedTrack() {};

		virtual ~ExtendedTrack() {};

		virtual int id() const { return simpleUID() ; }

		virtual float getXZintVar() {return _XZintVar;}
		virtual float getYZintVar() {return _YZintVar;}
		virtual float getXZslopeVar() {return _XZslopeVar;}
		virtual float getYZslopeVar() {return _YZslopeVar;}
		virtual float getYZchi2() {return _XZchi2;}
		virtual float getXZchi2() {return _YZchi2;}
		virtual int getXZndf() {return _XZndf;}
		virtual int getYZndf() {return _YZndf;}
		virtual float getXZslope() {return _XZslope;}
		virtual float getYZslope() {return _YZslope;}
		virtual std::vector<TrackerHit*> getHitsUsed(){return _HitsUsed;}

		virtual void setXZintVar(float XZintVar) {_XZintVar = XZintVar;}
		virtual void setYZintVar(float YZintVar) {_YZintVar = YZintVar;}
		virtual void setXZslopeVar(float XZslopeVar) {_XZslopeVar = XZslopeVar;}
		virtual void setYZslopeVar(float YZslopeVar) {_YZslopeVar = YZslopeVar;}
		virtual void setXZchi2(float XZchi2) {_XZchi2 = XZchi2;}
		virtual void setYZchi2(float YZchi2){_YZchi2 = YZchi2;}
		virtual void setXZndf(int XZndf){_XZndf = XZndf;}
		virtual void setYZndf(int YZndf){_YZndf = YZndf;}
		virtual void setXZslope(float XZslope){_XZslope = XZslope;}
		virtual void setYZslope(float YZslope){_YZslope = YZslope;}
		virtual void setHitsUsed(std::vector<TrackerHit*> HitsUsed){_HitsUsed = HitsUsed;}
		

	protected:
		float _XZintVar;
		float _YZintVar;
		float _XZslopeVar;
		float _YZslopeVar;
		float _XZchi2;
		float _YZchi2;
		float _XZndf;
		float _YZndf;
		float _XZslope;
		float _YZslope;
		std::vector<TrackerHit*> _HitsUsed;

}; //class

} //namespace IMPL
#endif /* ifndef EXTENDED_TRACK_H */
