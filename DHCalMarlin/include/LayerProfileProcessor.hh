/*
 * LayerProfileProcessor.hh
 *
 * A processor to produce 2D histograms of all hits within a layer
 * over the whole input file.
 *
 *  Created on: Mar 19, 2014
 *      Author: cgrefe
 */

#ifndef LAYERPROFILEPROCESSOR_HH_
#define LAYERPROFILEPROCESSOR_HH_

#include "DhcalMapping.hh"

// LCIO
#include "EVENT/LCEvent.h"

// marlin
#include "marlin/Processor.h"

// ROOT
#include "TH2D.h"

// c++
#include <string>
#include <map>

namespace CALICE {

class LayerProfileProcessor: public marlin::Processor {
public:
	LayerProfileProcessor();
	virtual ~LayerProfileProcessor();

	LayerProfileProcessor* newProcessor() {
		return new LayerProfileProcessor();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent*);
	virtual void end();

protected:
	std::string _rootFileName;
	std::string _histogramName;
	std::string _collectionName;

	TH2D* _totalHistogram;
	std::map<int, TH2D*> _layerHistograms;

	DhcalMapping* _mapping;
};

};

#endif /* LAYERPROFILEPROCESSOR_HH_ */
