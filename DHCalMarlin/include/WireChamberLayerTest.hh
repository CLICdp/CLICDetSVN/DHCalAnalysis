/*
 * WireChamberLayerTest.hh
 *
 *  Created on: April 19, 2013
 *      Author: William Nash, BU + CERN,
 */


#ifndef WIRE_CHAMBER_LAYER_TESTER_HH_
#define WIRE_CHAMBER_LAYER_TESTER_HH_ 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include "EVENT/TrackerHit.h"
#include "WireChamberUtil.hh"

//root
#include <TH1F.h>
#include <TH2F.h>

using EVENT::TrackerHit;

namespace CALICE{

class WireChamberLayerTester : public marlin::Processor{
	public:
		WireChamberLayerTester();
		virtual ~WireChamberLayerTester();

		WireChamberLayerTester* newProcessor(){return new WireChamberLayerTester();}

		virtual void init();
		virtual void processRunHeader(EVENT::LCRunHeader*);
		virtual void processEvent(EVENT::LCEvent*);
		virtual void end();
		static Double_t doublegauss(Double_t *x, Double_t *par);

	protected:

		TH1F* Layer0X;
		TH1F* Layer0Y;
		TH1F* Layer1X;
		TH1F* Layer1Y;
		TH1F* Layer2X;
		TH1F* Layer2Y;

		std::map<int, std::vector<TH1F*> > LayerMap;

		TH2F* X0vsX1;
		TH2F* X0vsX2;
		TH2F* X1vsX2;

		TH2F* Y0vsY1;
		TH2F* Y0vsY2;
		TH2F* Y1vsY2;

		TH1F* X0res;
		TH1F* X1res;
		TH1F* X2res;

		TH1F* Y0res;
		TH1F* Y1res;
		TH1F* Y2res;


		std::string RunNumber;
		int _overcount;
		int _FitBounds;
		int _ResFitBounds;
		int _Title; 
		
	private:

};

}/*Namespace CALICE*/

#endif
