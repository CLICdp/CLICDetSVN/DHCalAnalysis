/*
 * WireChamberUtil.hh
 *
 *  Created on: ,April 26 2013
 *      Author: William Nash, BU + CERN
 */

#ifndef WIRECHAMBERUTIL_HH_
#define WIRECHAMBERUTIL_HH_

#include <DhcalUtil.hh>

// lcio
#include "EVENT/Cluster.h"
#include "EVENT/TrackerHit.h"
#include "IMPL/ClusterImpl.h"


// root
#include "TMatrixTSym.h"
#include "TVector3.h"

// c++
#include <vector>
#include <map>

//misc
#include <ExtendedTrack.hh>

using namespace std;

namespace CALICE {

/**
 * Collection of helper methods and convenience methods
 * heavily based off of DhcalUtil.cc by Christian Grefe
 */

namespace WireChamberUtil {


IMPL::ExtendedTrack* ExtendedLineFit(const std::vector<TVector3>& points, const std::vector<TMatrixTSym<double> >& covariances);

IMPL::ExtendedTrack* ExtendedLineFit(const std::vector<EVENT::TrackerHit*>& hits);

IMPL::ExtendedTrack* ExtendedLineFit(const std::vector<EVENT::Cluster*>& hits);

map<int, vector<TrackerHit*> > buildHitMap(const EVENT::LCCollection* collections,
		const string& identifier);
		
vector<float> ResolutionCalculator(EVENT::TrackerHit* first, EVENT::TrackerHit* second, EVENT::TrackerHit* third);

} /* namespace WireChamberUtil */

} /* namespace CALICE */

#endif /* WIRECHAMBERUTIL_HH_ */
