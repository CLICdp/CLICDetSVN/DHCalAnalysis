/*
 * DesityWeightingProcessor.h
 *
 *  Created on: Sept 21, 2013
 *      Author: Coralie Neubueser, DESY
 */

#ifndef DENSITYWEIGHTINGPROCESSOR_HH_
#define DENSITYWEIGHTINGPROCESSOR_HH_

#include "DhcalMapping.hh"
#include "ColumnRowHitMap.hh"
#include "LayerColumnRowMap.hh"
#include "RPCSimChargeSpreadModel.hh"

#include "IO/LCReader.h"
#include "EVENT/LCEvent.h"
#include "EVENT/LCRelation.h"
#include "EVENT/CalorimeterHit.h"
#include "EVENT/SimCalorimeterHit.h"
#include "IMPL/ClusterImpl.h"
#include "UTIL/CellIDDecoder.h"

// marlin
#include "marlin/Processor.h"

// ROOT
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TRandom3.h"
#include "TVector3.h"

#include <limits>
#include <list>
#include <map>
#include <set>
#include <string>
#include <sstream>

namespace CALICE {

class DensityWeightingProcessor: public marlin::Processor {
public:
	DensityWeightingProcessor();
	virtual ~DensityWeightingProcessor();

	DensityWeightingProcessor* newProcessor() {
		return new DensityWeightingProcessor();
	}

	virtual void init();
	virtual void processEvent(lcio::LCEvent*);
	virtual void end();

protected:
        TH1D* _nNeighbours;
        TH1D* _refNNeighbours;
        TH1D* _weights;

        std::string _inputCollectionName;
        std::string _compareCollectionName;
	std::string _rootFileName;
  std::string _uIdentifier;
  std::string _vIdentifier;
  std::string _layerIdentifier;
 
        TFile* _rootFile;
  double _N [10];
  double _N2 [10];
  double _N1 [10];

        LayerColumnRowMap<EVENT::CalorimeterHit*>* _hitMap;
	LayerColumnRowMap<EVENT::CalorimeterHit*>* _compareHitMap;

	std::set<CalorimeterHit*> _usedHits;
	UTIL::CellIDDecoder<EVENT::CalorimeterHit>* _idDecoder;

};

} /* namespace CALICE */
#endif /* DENSITYWEIGHTINGPROCESSOR_HH_ */
