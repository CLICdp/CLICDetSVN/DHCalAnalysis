/*
 * DhcalMapping.hh
 *
 *  Created on: Apr 5, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef DHCALMAPPING_HH_
#define DHCALMAPPING_HH_

#include "LayerColumnRowMap.hh"

// root
#include "TVector3.h"

// lcio
#include "EVENT/CalorimeterHit.h"
#include "EVENT/SimCalorimeterHit.h"
#include "UTIL/BitField64.h"

// c++
#include <map>
#include <set>
#include <string>

namespace CALICE {

typedef long long CellID;

enum FrontEndBoardPosition {
	TopLeft, TopRight, MiddleLeft, MiddleRight, BottomLeft, BottomRight, kFrontEndBoardPositions
};

enum ModulePosition {
	Top, Middle, Bottom, kModulePositions
};

/*
 * Singleton providing access to all geometry information
 */
class DhcalMapping {
	// Allow setting of values via a Marlin processor
	friend class DhcalMappingProcessor;

public:
	// Get the global instance of the DHCAL mapping
	static DhcalMapping* instance();

	// Destructor
	virtual ~DhcalMapping();

	/*************** Helper classes ***************/

	/*
	 * Container to define module dimensions
	 */
	struct Module {
		Module(int uMin = 0, int uMax = 0, int vMin = 0, int vMax = 0, double cellSizeU = 1., double cellSizeV = 1.,
				TVector3 position = TVector3());
		int uMin;
		int uMax;
		int vMin;
		int vMax;
		int nCellsU;
		int nCellsV;
		double xMin;
		double xMax;
		double yMin;
		double yMax;
		double cellSizeU;
		double cellSizeV;
		TVector3 position;
	};

	/*
	 * Container to define front end board dimensions
	 */
	typedef Module FrontEndBoard;

	/*************** Cell ID to Positon Conversions and vice versa ***************/

	// check if position is valid
	bool validPosition(const TVector3& position) const;

	// check if cell ID is valid
	bool validCellID(const CellID& cellID) const;

	// Converts three indices into the corresponding cell ID
	CellID getCellIDFromIndices(int u, int v, int layer) const;

	// Converts a position into the corresponding cell ID
	CellID getCellIDFromPosition(const TVector3& position) const;

	// Convert the cell ID into the corresponding position
	TVector3 getPositionFromCellID(const CellID& cellID) const;

	// Convert the cell indices into the corresponding position
	TVector3 getPositionFromIndices(int u, int v, int layer) const;

	// Extract the layer index from the cell ID
	int getLayerNumberFromCellID(const CellID& cellID) const;

	// Get the layer index corresponding to the given position
	int getLayerNumberFromPosition(const TVector3& position) const;

	// Get the module position corresponding to the given position
	ModulePosition getModulePositionFromPosition(const TVector3& position) const;

	// Get the module position corresponding to the given cell ID
	ModulePosition getModulePositionFromCellID(const CellID& cellID) const;

	// Get the module position corresponding to the given position
	const Module& getModuleFromPosition(const TVector3& position) const;

	// Get the module position corresponding to the given cell ID
	const Module& getModuleFromCellID(const CellID& cellID) const;

	// Get the position of center of the given layer
	const TVector3& getLayerPosition(int layerIndex) const;

	// Get the closest layer index from the given position in z
	int getClosestLayerID(double zPosition) const;

	/*************** Check for boundaries ***************/

	// Checks if the given position is close to a module boundary within the given number of cells
	bool isModuleBoundary(const TVector3& position, int margin = 1) const;

	// Checks if the given cell ID is close to a module boundary within the given number of cells
	bool isModuleBoundary(const CellID& cellID, int margin = 1) const;

	// Returns a vector with the distances to the closest module boundaries
	TVector3 distanceToModuleBoundary(const TVector3& position) const;

	// Returns a vector with the distances to the closest module boundaries
	TVector3 distanceToModuleBoundary(const CellID& cellID) const;

	// Checks if the given position is close to a front end board boundary within the given number of cells
	bool isFrontEndBoardBoundary(const TVector3& position, int margin = 1) const;

	// Checks if the given cell ID is close to a front end board boundary within the given number of cells
	bool isFrontEndBoardBoundary(const CellID& cellID, int margin = 1) const;

	/*************** Check if cell is in tail catcher ***************/

	// Check if the given cell ID is in the tail catcher
	bool isTailCatcher(const CellID& cellID) const;

	// Check if the given position is in the tail catcher
	bool isTailCatcher(const TVector3& position) const;


	/*************** Check if cell is flagged as ignored cell ***************/

    // Access to the list of ignored Cell IDs
	const std::set<CellID>& getIgnoredCells() const {
        return ignoredCellIDs;
    }
    
	// Check if the given cell ID is in the list of ignored cells
	bool isIgnoredCell(const CellID& cellID) const;

	// Check if the given position is in the list of ignored cells
	bool isIgnoredCell(const TVector3& position) const;

	/*************** Access to calibration values ***************/

	// Get the calibration value for the given cell ID
	float getCalibration(const CellID& cellID) const;

	// Get the calibration value for the given position
	float getCalibration(const TVector3& position) const;

	// Get the nominal calibration value for the given layer number and module position
	float getModuleCalibration(int layerID, const ModulePosition& modulePosition) const;

	// Get the efficiency for the given cell ID
	float getEfficiency(const CellID& cellID) const;

	// Get the efficiency for the given cell ID
	float getEfficiency(const TVector3& position) const;

	// Get the nominal efficiency for the given layer number and module position
	float getModuleEfficiency(int layerID, const ModulePosition& modulePosition) const;

	// Get the multiplicity for the given cell ID
	float getMultiplicity(const CellID& cellID) const;

	// Get the multiplicity for the given cell ID
	float getMultiplicity(const TVector3& position) const;

	// Get the nominal multiplicity for the given layer number and module position
	float getModuleMultiplicity(int layerID, const ModulePosition& modulePosition) const;

	/*************** ID decoding ***************/

	// Get the horizontal cell index from the cell ID
	int getIndexU(const CellID& cellID) const;

	// Get the vertical cell index from the cell ID
	int getIndexV(const CellID& cellID) const;

	// Get the layer index from the cell ID
	int getIndexLayer(const CellID& cellID) const;

	// Get the u, v and layer indices simultaneously
	void getIndices(const CellID& cellID, int& u, int& v, int& layer) const;

	// Get the horizontal cell index from the cell ID
	template <class T>
	inline int getIndexU(const T* hit) const {
		return getIndexU(getCellID(hit));
	}

	// Get the vertical cell index from the cell ID
	template <class T>
	inline int getIndexV(const T* hit) const {
		return getIndexV(getCellID(hit));
	}

	// Get the layer index from the cell ID
	template <class T>
	inline int getIndexLayer(const T* hit) const {
		return getIndexLayer(getCellID(hit));
	}

	// Get the u, v and layer indices simultaneously
	template <class T>
	inline void getIndices(const T* hit, int& u, int& v, int& layer) const {
		getIndices(getCellID(hit), u, v, layer);
	}

	// Get the ID decoder
	UTIL::BitField64& getCellIdDecoder() {
		return *_idDecoder;
	}

	/*************** Mapping constants ***************/

	// Get the number of layers
	inline int N_LAYERS() const {
		return nLayers;
	}

	// Get the number of cells in horizontal direction
	inline int N_CELLS_U() const {
		return nCellsU;
	}

	// Get the number of cells in vertical direction
	inline int N_CELLS_V() const {
		return nCellsV;
	}

	// Get the cell size in horizontal direction
	inline double CELL_SIZE_U() const {
		return cellSizeU;
	}

	// Get the cell size in vertical direction
	inline double CELL_SIZE_V() const {
		return cellSizeV;
	}

	// Get the encoding ID for the horizontal cell index
	inline std::string U_ID() const {
		return idU;
	}

	// Get the encoding ID for the vertical cell index
	inline std::string V_ID() const {
		return idV;
	}

	// Get the encoding ID for the layer index
	inline std::string LAYER_ID() const {
		return idLayer;
	}

	// Get the cell encoding definition
	inline std::string CELL_ID_ENCODING() const {
		return _idDecoder->fieldDescription();
	}

	// Extracts the 64 bit cell ID from the two 32 bit cell IDs
	template <class T>
	inline static CellID getCellID(const T* hit) {
		return CellID(hit->getCellID0() & 0xffffffff) | (CellID(hit->getCellID1()) << 32);
	}

protected:
	// Constants
	int nLayers;
	int nCellsU;
	int nCellsV;
	double cellSizeU;
	double cellSizeV;
	std::string idU;
	std::string idV;
	std::string idLayer;
	int tailCatcherStart;

	// The id decoder needs to be mutable because id decoding requires setting of the cell ID
	mutable UTIL::BitField64* _idDecoder;

	std::map<ModulePosition, Module> modulePositionMap;
	std::map<FrontEndBoardPosition, FrontEndBoard> frontEndBoardPositionMap;
	std::map<int, TVector3> layerPositionMap;
	std::vector<double> layerZPositions;
	std::set<CellID> ignoredCellIDs;

	// Maps of calibration values
	std::map<int, std::map<ModulePosition, float> > moduleCalibrationMap;
	std::map<int, std::map<ModulePosition, float> > moduleMultiplicityMap;
	std::map<int, std::map<ModulePosition, float> > moduleEfficiencyMap;
	LayerColumnRowMap<float>* calibrationMap;
	LayerColumnRowMap<float>* multiplicityMap;
	LayerColumnRowMap<float>* efficiencyMap;

	// Sets the ID encoding string by replacing the existing id decoder
	void setIdEncoding(const std::string& encoding);

private:
	// Singleton class: constructor needs to be private
	DhcalMapping();

	// The global instance
	static DhcalMapping* _instance;
};

} /* namespace CALICE */
#endif /* DHCALMAPPING_HH_ */
