/*
 * CalorimeterHitComparator.hh
 *
 *  Created on: Mar 2, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef CALORIMETERHITCOMPARATOR_HH_
#define CALORIMETERHITCOMPARATOR_HH_

#include "DhcalUtil.hh"

// lcio
#include "EVENT/CalorimeterHit.h"
#include "EVENT/LCCollection.h"
#include "UTIL/CellIDDecoder.h"

//c++
#include <string>
#include <vector>

namespace CALICE {

/*
 * Class to define a comparison operation between two hits.
 * Can hold nested CalorimeterHitComparators that will are consecutively checked in compare and consistency methods.
 */
class CalorimeterHitComparator {
public:
	CalorimeterHitComparator();
	virtual ~CalorimeterHitComparator();

	inline virtual void setLimit(float limit) {
		_limit = limit;
	}

	inline virtual void addComparator(CalorimeterHitComparator* comparator) {
		_comparators.push_back(comparator);
	}

	/*
	 * Similar to compare. Required to be used as comparator in std::sort.
	 */
	virtual inline bool operator() (const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return compare(hit, otherHit);
	}

	/*
	 * Returns true if the value to compare is lower for the first hit than the second hit. Returns false otherwise.
	 * Imposes an ordering by this comparator followed by an order following the next nested comparator.
	 */
	virtual bool compare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const;

	/*
	 * Returns true is the value to compare is consistent within the given limits for the two hits in this comparator and all nested comparators.
	 */
	virtual bool consistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const;

protected:
	float _limit;
	std::vector<CalorimeterHitComparator*> _comparators;

	virtual bool myCompare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const = 0;
	virtual bool myConsistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const = 0;
};

class CalorimeterHitComparatorID : public CalorimeterHitComparator {
public:
	CalorimeterHitComparatorID();
	virtual ~CalorimeterHitComparatorID();

	virtual void setIdDecoder(EVENT::LCCollection* collection);
	virtual void setIdDecoder(UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder);

protected:
	mutable UTIL::CellIDDecoder<EVENT::CalorimeterHit>* _idDecoder;
	bool _ownsIdDecoder;
	void cleanIdDecoder();
};

class CalorimeterHitComparatorLayer: public CalorimeterHitComparatorID {
public:
	CalorimeterHitComparatorLayer();
	virtual ~CalorimeterHitComparatorLayer();

	inline virtual void setIdentifierLayer(const std::string& identifier) {
		_layerIdentifier = identifier;
	}

protected:
	inline virtual bool myCompare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::compareLayer(hit, hit, *_idDecoder, _layerIdentifier);
	}

	inline virtual bool myConsistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::consistentInLayer(hit, otherHit, _limit, *_idDecoder, _layerIdentifier);
	}

	std::string _layerIdentifier;
};

class CalorimeterHitComparatorUV: public CalorimeterHitComparatorID {
public:
	CalorimeterHitComparatorUV();
	virtual ~CalorimeterHitComparatorUV();

	inline virtual void setIdentifierU(const std::string& identifier) {
		_uIdentifier = identifier;
	}

	inline virtual void setIdentifierV(const std::string& identifier) {
		_vIdentifier = identifier;
	}

protected:
	inline virtual bool myCompare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::compareUV(hit, otherHit, *_idDecoder, _uIdentifier, _vIdentifier);
	}

	inline virtual bool myConsistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::consistentInUV(hit, otherHit, _limit, *_idDecoder, _uIdentifier, _vIdentifier);
	}

	std::string _uIdentifier;
	std::string _vIdentifier;
};

class CalorimeterHitComparatorXY: public CalorimeterHitComparator {
public:
	CalorimeterHitComparatorXY();
	virtual ~CalorimeterHitComparatorXY();

protected:
	inline virtual bool myCompare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::compareXY(hit, otherHit);
	}

	inline virtual bool myConsistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::consistentInXY(hit, otherHit, _limit);
	}
};

class CalorimeterHitComparatorXYZ: public CalorimeterHitComparator {
public:
	CalorimeterHitComparatorXYZ();
	virtual ~CalorimeterHitComparatorXYZ();

protected:
	inline virtual bool myCompare(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::compareXYZ(hit, otherHit);
	}

	inline virtual bool myConsistent(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit) const {
		return DhcalUtil::consistentInXYZ(hit, otherHit, _limit);
	}
};

} /* namespace CALICE */
#endif /* CALORIMETERHITCOMPARATOR_HH_ */
