/*
 * LayerColumnRowMap.hh
 *
 *  Created on: Jul 15, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef LAYERCOLUMNROWHITMAP_HH_
#define LAYERCOLUMNROWHITMAP_HH_

#include "EVENT/LCCollection.h"
#include "UTIL/BitField64.h"

#include <map>
#include <vector>

namespace CALICE {

typedef long long int CellID;

template<class T> class LayerColumnRowMap: public std::map<int, std::map<int, std::map<int, T> > > {
public:
	/// Default constructor
	LayerColumnRowMap();

	/// Constructor using specific decoding
	LayerColumnRowMap(const std::string& cellEncoding, const std::string& uIdentifier, const std::string& vIdentifier,
			const std::string& layerIdentifier);

/// Destructor
	virtual ~LayerColumnRowMap();

/// Adds an object to the map using the given indices.
	void fill(int layerIndex, int columnIndex, int rowIndex, T obj);

/// Checks if an object exist at the given indices.
	bool contains(int layerIndex, int columnIndex, int rowIndex) const;

/// Checks if an object exists with the given cell ID.
	bool contains(const CellID& cellID) const;

/// Returns the object with the given indices if it exists. Throws a std::out_of_range exception otherwise.
	const T& get(int layerIndex, int columnIndex, int rowIndex) const;

/// Returns the object with the given cell ID if it exists. Throws a std::out_of_range exception otherwise.
	const T& get(const CellID& cellID) const;

/// Returns all objects within the given limits.
	std::vector<T> getAll(int minLayer, int maxLayer, int minColumn, int maxColumn, int minRow, int maxRow) const;

/// Returns a vector containing all neighbouring objects to the given cell indices within the given limits.
	std::vector<T> getNeighbours(int layerIndex, int columnIndex, int rowIndex, int layerDistance = 1,
			int columnDistance = 1, int rowDistance = 1) const;

/// Returns a vector containing all neighbouring objects to the given cell ID within the given limits.
	std::vector<T> getNeighbours(const CellID& cellID, int layerDistance = 1, int columnDistance = 1, int rowDistance =
			1) const;

protected:
	bool _useDefaultMapping;
	mutable UTIL::BitField64 _idDecoder;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::string _layerIdentifier;
};

/// Pointer specialization. This is the one usually used for the LCIO types

template<class T> class LayerColumnRowMap<T*> : public std::map<int, std::map<int, std::map<int, T*> > > {
public:
	/// Default constructor
	LayerColumnRowMap();

	/// Constructor using specific decoding
	LayerColumnRowMap(const std::string& cellEncoding, const std::string& uIdentifier, const std::string& vIdentifier,
			const std::string& layerIdentifier);

/// Destructor
	virtual ~LayerColumnRowMap();

/// Adds an object to the map using the given indices.
	void fill(int layerIndex, int columnIndex, int rowIndex, T* obj);

	/* Adds an object to the map using its cell ID to determine the indices.
	 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
	 */
	void fill(T* obj);

	/* Adds all objects to the map using their cell IDs to determine the indices.
	 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
	 */
	void fill(const std::vector<T*>& objects);

	/* Adds all objects from a collection to the map using their cell ID to determine the indices.
	 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
	 */
	void fill(const EVENT::LCCollection* collection);

/// Checks if an object exist at the given indices.
	bool contains(int layerIndex, int columnIndex, int rowIndex) const;

/// Checks if an object exists with the given cell ID.
	bool contains(const CellID& cellID) const;

	/* Checks if an object exists by its cell ID.
	 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
	 */
	bool contains(const T* obj) const;

/// Returns the object with the given indices if it exists. Returns a null pointer otherwise.
	T* get(int layerIndex, int columnIndex, int rowIndex) const;

/// Returns the object with the given cell ID if it exists. Returns a null pointer otherwise.
	T* get(const CellID& cellID) const;

/// Returns all objects within the given limits.
	std::vector<T*> getAll(int minLayer, int maxLayer, int minColumn, int maxColumn, int minRow, int maxRow) const;

/// Returns a vector containing all neighbouring objects to the given cell indices within the given limits.
	std::vector<T*> getNeighbours(int layerIndex, int columnIndex, int rowIndex, int layerDistance = 1,
			int columnDistance = 1, int rowDistance = 1) const;

/// Returns a vector containing all neighbouring objects to the given cell ID within the given limits.
	std::vector<T*> getNeighbours(const CellID& cellID, int layerDistance = 1, int columnDistance = 1, int rowDistance =
			1) const;

	/* Returns a vector containing all neighbouring objects to the given object within the given limits.
	 * Uses DhcalMapping::getCellID(T obj) to extract the cell ID.
	 */
	std::vector<T*> getNeighbours(const T* obj, int layerDistance = 1, int columnDistance = 1,
			int rowDistance = 1) const;

protected:
	bool _useDefaultMapping;
	mutable UTIL::BitField64 _idDecoder;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::string _layerIdentifier;
};

} /* namespace CALICE */
#endif /* LAYERCOLUMNROWHITMAP_HH_ */
