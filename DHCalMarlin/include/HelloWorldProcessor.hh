#ifndef HELLO_WORLD_PROCESSOR_HH
#define HELLO_WORLD_PROCESSOR_HH 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"

class HelloWorldProcessor : public marlin::Processor{
 public:
  HelloWorldProcessor();
  virtual ~HelloWorldProcessor();

  HelloWorldProcessor* newProcessor(){return new HelloWorldProcessor();}

  virtual void init();
  virtual void processRunHeader(lcio::LCRunHeader*);
  virtual void processEvent(lcio::LCEvent*);
  virtual void end();

 protected:

 private:

};

#endif
