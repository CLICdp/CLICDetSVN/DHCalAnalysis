/*
 * RPCSimChargeSpreadModel.hh
 *
 *  Created on: Nov 1, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef RPCSIMCHARGESPREADMODEL_HH_
#define RPCSIMCHARGESPREADMODEL_HH_

#include <TF1.h>

#include <string>
#include <vector>

namespace CALICE {

class RPCSimChargeSpreadModel;

/// Factory class to create charge spread models by name
class RPCSimFactory {
public:
	/// destructor
	~RPCSimFactory();
	/// access to the global factory instance
	static RPCSimFactory* instance();
	/// create a new charge spread model by its type name
	RPCSimChargeSpreadModel* createChargeSpreadModel(const std::string& name) const;
protected:
	/// constructor
	RPCSimFactory();
	static RPCSimFactory* _instance;  ///< the global factory instance
};

/// Base class for all charge spread models
class RPCSimChargeSpreadModel {
public:
	/// destructor
	virtual ~RPCSimChargeSpreadModel();
	/// sets all parameters by their index depending on their position in the vector
	void setParameters(const std::vector<float>& parameters);
	/// sets a parameter by its name and re-initializes the model
	void setParameter(const std::string& name, float value);
	/// sets a parameter by its index and re-initializes the model
	void setParameter(int index, float value);
	/// sets the maximum radius of the charge spread
	void setMaxRadius(double radius);
	/// access to the underlying function
	const TF1* function() const;
	/// calculate the charge deposited at a given radius
	virtual double calculateCharge(double radius) const;
	/// string representation of the model
	std::string toString() const;
protected:
	/// constructor to be called by inheriting classes
	RPCSimChargeSpreadModel(const std::string& name, const std::vector<std::string>& parameterUnits);
	/// helper method to pre-calculate normalization. Called by ::setParameter()
	virtual void init() = 0;

	std::string name;          ///< the name of the charge spread model
	std::vector<std::string> parameterUnits;  ///< vector to keep track of parameter units
	double maxRadius;          ///< the maximum radius of the charge spread model
	TF1* chargeSpreadFunction; ///< the underlying function of the charge spreading
private:
	/// hide copy constructor
	RPCSimChargeSpreadModel(const RPCSimChargeSpreadModel& m) {}
};

/**
 * Charge spread using double exponential
 */
class RPCSim3Model : public RPCSimChargeSpreadModel {
public:
	/// Default constructor
	RPCSim3Model();
	/// Destructor
	virtual ~RPCSim3Model();
protected:
	/// Override base class method
	virtual void init();
	/// Helper method to access the list of parameter units
	static std::vector<std::string> getParameterUnits();
};

/**
 * Charge spread using single exponential
 */
class RPCSim4Model : public RPCSimChargeSpreadModel {
public:
	/// Default constructor
	RPCSim4Model();
	/// Destructor
	virtual ~RPCSim4Model();
protected:
	/// Override base class method
	virtual void init();
	/// Helper method to access the list of parameter units
	static std::vector<std::string> getParameterUnits();
};

/**
 * Charge spread using double Gaussian
 */
class RPCSim5Model : public RPCSimChargeSpreadModel {
public:
	/// Default constructor
	RPCSim5Model();
	/// Destructor
	virtual ~RPCSim5Model();
protected:
	/// Override base class method
	virtual void init();
	/// Helper method to access the list of parameter units
	static std::vector<std::string> getParameterUnits();
};

/**
 * Charge spread using double Gaussian
 */
class RPCSim6Model : public RPCSimChargeSpreadModel {
public:
	/// Default constructor
	RPCSim6Model();
	/// Destructor
	virtual ~RPCSim6Model();
protected:
	/// Override base class method
	virtual void init();
	/// Helper method to access the list of parameter units
	static std::vector<std::string> getParameterUnits();
};

} /* namespace CALICE */
#endif /* RPCSIMCHARGESPREADMODEL_HH_ */
