#ifndef BOXEVENTFINDER_H
#define BOXEVENTFINDER_H 1

#include "marlin/Processor.h"
#include "lcio.h"
#include <string>

using namespace lcio;
using namespace marlin;


/**  DHCalAnalysis for marlin.
 * 
 * 
 * @param CollectionName Name of the MCParticle collection
 * 
 * @author Jan Strube, CERN
 * @version $Id:$ 
 */

namespace CALICE {
class BoxEventFinder : public Processor {
  
 public:
  
  virtual Processor*  newProcessor() { return new BoxEventFinder; }
  
  
  BoxEventFinder();
  
  /** Called at the begin of the job before anything is read.
   * Use to initialize the processor, e.g. book histograms.
   */
  virtual void init();
  
  /** Called for every run.
   */
  virtual void processRunHeader(LCRunHeader* run);
  
  /** Called for every event - the working horse.
   */
  virtual void processEvent(LCEvent* evt); 
  
  
  virtual void check(LCEvent* evt); 
  
  
  /** Called after data processing for clean up.
   */
  virtual void end();
  
  
 protected:

  int m_RunNumber;
  int m_nBoxEvents;
  int m_nRun;
  int m_nEvt;

  StringVec m_hitCollectionNames;
  std::string m_clusterCollectionName;
  bool m_skipBoxEvents;
};

}

#endif // BOXEVENTFINDER_H
