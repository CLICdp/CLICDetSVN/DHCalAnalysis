/*
 * WireChamberPlotter.hh
 *
 *  Created on: Mar 22, 2013
 *      Author: William Nash, CERN
 */


#ifndef WIRE_CHAMBER_PLOTTER_HH_
#define WIRE_CHAMBER_PLOTTER_HH_ 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include <TH3D.h>
#include "EVENT/TrackerHit.h"
#include <TCanvas.h>
#include <TH2F.h>
#include <TH1F.h>

using std::vector;

namespace CALICE{

class WireChamberPlotter : public marlin::Processor{
	public:
		WireChamberPlotter();
		virtual ~WireChamberPlotter();

		WireChamberPlotter* newProcessor(){return new WireChamberPlotter();}

		virtual void init();
		virtual void processRunHeader(EVENT::LCRunHeader*);
		virtual void processEvent(EVENT::LCEvent*);
		virtual void end();

	protected:
		TH2F* XY_Distribution;
		std::string WCRunNumber;
		float _zLocation;
		int _xBins;
		int _yBins;
		float _xMin;
		float _yMin;
		float _xMax;
		float _yMax;


		TH1F* XZSlope;
		TH1F* YZSlope;
		int _XZ_bins;
		int _YZ_bins;
		float _XZ_Max;
		float _YZ_Max;
		float _XZ_Min;
		float _YZ_Min;
		int _Title;
		int _ResFitBounds;
		
		TH1F* X0res;
		TH1F* X1res;
		TH1F* X2res;

		TH1F* Y0res;
		TH1F* Y1res;
		TH1F* Y2res;


	private:

};


}/*Namespace CALICE*/
#endif
