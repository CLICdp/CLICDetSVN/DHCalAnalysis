/*
 * EventVariablesRootWriter.hh
 *
 *  Created on: Mar 5, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef EVENTVARIABLESROOTWRITER_HH_
#define EVENTVARIABLESROOTWRITER_HH_

#include "marlin/Processor.h"

#include "TFile.h"
#include "TTree.h"

#include <string>
#include <vector>

namespace CALICE {

class EventVariablesRootWriter : public marlin::Processor {
public:
	EventVariablesRootWriter();
	virtual ~EventVariablesRootWriter();

	inline EventVariablesRootWriter* newProcessor() {
		return new EventVariablesRootWriter();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent* event);
	virtual void end();

protected:
	std::vector<std::string> _hitCollectionNames;
	std::string _layerClusterCollectionName;
	std::string _rootTreeName;
	std::string _rootFileName;
	TFile* _rootFile;
	TTree* _rootTree;

	int _interactionLayerEmptyLayerHits;

	// event variables
	int nHits;				// the total number of hits
	int interactionLayer;	// the interaction layer
	bool cerenkovA;			// cerenkov trigger A
	bool cerenkovB;			// cerenkov trigger B
	bool hasBox;			// contains a layer with a box pattern
	float totalEnergy;		// total weighted hits
	float energy_0;			// total weighted hits with 0 neighbors
	float energy_1;			// total weighted hits with 1 neighbors
	float energy_2;			// total weighted hits with 2 neighbors
	float energy_3;			// total weighted hits with 3 neighbors
	float energy_4;			// total weighted hits with 4 neighbors
	float energy_5;			// total weighted hits with 5 neighbors
	float energy_6;			// total weighted hits with 6 neighbors
	float energy_7;			// total weighted hits with 7 neighbors
	float energy_8;			// total weighted hits with 8 neighbors
	float hitDensity;		// density of event: nHits / nActiveLayers
	float centerX;			// arithmetic mean x of all hits
	float centerY;			// arithmetic mean y of all hits
	float centerZ;			// arithmetic mean z of all hits
	float centerU;			// arithmetic mean U of all hits
	float centerV;			// arithmetic mean V of all hits
	float centerLayer;		// arithmetic mean layer of all hits
	float rmsX;				// standard deviation of hit positions in x
	float rmsY;				// standard deviation of hit positions in y
	float rmsXY;			// standard deviation of hit positions in r
	float rmsZ;				// standard deviation of hit positions in z
	float rmsU;				// standard deviation of hit positions in u
	float rmsV;				// standard deviation of hit positions in v
	float rmsUV;			// standard deviation of hit positions in r (using UV indices)
	float rmsLayer;			// standard deviation of hit positions in layer indices
	float spillTime;		// event time within the spill (seconds)

	// layer variables (have length nLayer)
	std::vector<int> layerNumber;
	std::vector<int> layerNHits;
	std::vector<int> layerNClusters;
	std::vector<double> layerEnergy;
	std::vector<double> layerCenterX;
	std::vector<double> layerCenterY;
	std::vector<double> layerCenterV;
	std::vector<double> layerCenterU;
	std::vector<double> layerRmsX;
	std::vector<double> layerRmsY;
	std::vector<double> layerRmsXY;
	std::vector<double> layerRmsU;
	std::vector<double> layerRmsV;
	std::vector<double> layerRmsUV;

	void clearVariables();
};

} /* namespace CALICE */
#endif /* EVENTVARIABLESROOTWRITER_HH_ */
