#ifndef MUON_TRACKER_HH
#define MUON_TRACKER_HH 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include <TH2F.h>
#include "IMPL/ClusterImpl.h"
#include <TH1F.h>
#include <TVector3.h>
#include <TCanvas.h>
#include <TString.h>


namespace CALICE{

class MuonTracker : public marlin::Processor{
 public:
  MuonTracker();
  virtual ~MuonTracker();

  MuonTracker* newProcessor(){return new MuonTracker();}

  virtual void init();
  virtual void processRunHeader(EVENT::LCRunHeader*);
  virtual void processEvent(EVENT::LCEvent*);
  static TCanvas* PlotOverlay(TH1F* Plot1, TH1F* Plot2, TString Title);
  virtual void end();

 protected:

  int _layersConsidered;
  int _xBins;
  int _yBins;
  float _xMin;
  float _yMin;
  float _xMax;
  float _yMax;
  float _chiLimit;
  int _clusterSize;
  float _efficiency;
  int _skipLimit;


  int _MuonxBins;
  int _MuonyBins;
  float _MuonxMin;
  float _MuonyMin;
  float _MuonxMax;
  float _MuonyMax;
  int _Title;


  TH1F* XWCError;
  TH1F* YWCError;
  TH1F* XDHCALError;
  TH1F* YDHCALError;
  TH2F* MuonDeltaDistribution;
  TH1F* XDeltaDistribution;
  TH1F* YDeltaDistribution;
  TH2F* MuonZ0Dist;
  std::string RunNumber;


 private:

};

}/*Namespace CALICE*/

#endif
