#ifndef HCALHISTOS_HH
#define HCALHISTOS_HH 1

#include "DhcalMapping.hh"

// Marlin
#include "marlin/Processor.h"

// LCIO
#include "EVENT/LCEvent.h"
#include "lcio.h"

// ROOT
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TProfile.h"
#include "TProfile2D.h"

namespace CALICE {

class RPCSimProcessor: public marlin::Processor {
public:
	RPCSimProcessor();
	virtual ~RPCSimProcessor();

	RPCSimProcessor* newProcessor() {
		return new RPCSimProcessor();
	}

	virtual void init();
	virtual void processEvent(lcio::LCEvent*);
	virtual void end();

protected:
	/*
	 collections
	 */
	std::string _hcalColName;
	std::string _ecalColName;
	std::string _digitColName;

	std::string _rootFileName;
	std::string _rootFileMode;

	TFile *_rootFile;

	/*
	 HCAL histograms
	 */
	TH1F *_hHitLayer;
	TH2F *_hHitMapXY;
	TH2F *_hHitMapIJ;
	TH2F *_hContribMapXY;
	TProfile * _pHitsPerLayer;
	TH1F *_hNContrib;
	TH1F *_hContribDist;
	TH2F *_hDigiHitsMap;
	TH1F *_hNhits;
	TH1F *_hCloudRadius;
	TH1F *_hContTime;
	TH1F *_hPadCharge;
	TH1F *_hR2;
	TH1F *_hCharge;
	TH1F* _qintegral1;
	TH1F* _qintegral2;
	TH1F* _qintegral;
	TH2F* _XYdep;
	TH1F* _radius;
	TH2F* _nHits2d;
	TH2F* _qPadDeposit;
	TProfile2D* _qPadDepositProfile;
	TH2F* _CellHitLocations;
	TH3F* _CellHitLocations3D;
	TH2F* _FirstLastCellLocation;
	TH2F* _HitContDif;
	TH2F* _DigiHitVsMokkaCell;
	TH2F* _DigiHitVsMokkaPos;

	TH2F* _Layer0Hits;
	TH2F* _Layer1Hits;
	TH2F* _Layer2Hits;
	TH2F* _Layer3Hits;
	TH2F* _Layer4Hits;
	TH2F* _Layer5Hits;
	TH2F* _Layer6Hits;
	TH2F* _Layer7Hits;
	TH2F* _Layer8Hits;
	TH2F* _Layer9Hits;
	TH2F* _Layer10Hits;
	TH2F* _Layer11Hits;
	TH2F* _Layer12Hits;
	TH2F* _Layer13Hits;
	TH2F* _Layer14Hits;
	TH2F* _Layer15Hits;
	TH2F* _Layer16Hits;
	TH2F* _Layer17Hits;
	TH2F* _Layer18Hits;
	TH2F* _Layer19Hits;
	TH2F* _Layer20Hits;
	TH2F* _Layer21Hits;
	TH2F* _Layer22Hits;
	TH2F* _Layer23Hits;
	TH2F* _Layer24Hits;
	TH2F* _Layer25Hits;
	TH2F* _Layer26Hits;
	TH2F* _Layer27Hits;
	TH2F* _Layer28Hits;
	TH2F* _Layer29Hits;
	TH2F* _Layer30Hits;
	TH2F* _Layer31Hits;
	TH2F* _Layer32Hits;
	TH2F* _Layer33Hits;
	TH2F* _Layer34Hits;
	TH2F* _Layer35Hits;
	TH2F* _Layer36Hits;
	TH2F* _Layer37Hits;
	TH2F* _Layer38Hits;
	std::map<int, TH2F*> LayerHitMap;

	float _SLOPE1;
	float _SLOPE2;
	float _RATIO;
	float _T;
	float _DIST_CUT;
	float _Q0;
	float _MAXRADIUS;

	DhcalMapping* _mapping;

private:

};

};

#endif
