/*
 * ParticleEventComparator.hh
 *
 *  Created on: Aug 21, 2013
 *      Author: William Nash, CERN
 */

#ifndef PARTICLEEVENTCOMPARATOR_HH_
#define PARTICLEEVENTCOMPARATOR_HH_

#include "marlin/Processor.h"
#include "DhcalMapping.hh"

//root
#include "TFile.h"
#include "TTree.h"

//lcio
#include "EVENT/LCCollection.h"


#include <string>
#include <vector>

namespace CALICE {

class ParticleEventComparator : public marlin::Processor {
public:
	ParticleEventComparator();
	virtual ~ParticleEventComparator();

	inline ParticleEventComparator* newProcessor() {
		return new ParticleEventComparator();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent* event);
	virtual bool layerCheck(int Layer);
	virtual EVENT::LCCollection* safelyGetCollection(EVENT::LCEvent* event, std::string collection);
	virtual void end();

protected:
	std::vector<std::string> _hitCollectionNames;
	std::string _rootTreeName;
	std::string _rootFileName;
	std::string _particleName;
	std::string _efficiencyFileName;
	float _minEfficiency;
	TFile* _rootFile;
	TTree* _rootTree;
	std::map<int, std::map<ModulePosition, float> > _efficiencyMap;
	std::vector<int> _badLayers;

	int _minInteractionLayerHits;

	// event variables
	int nHits;
	float totalEnergy;
	float hitDensity;
	float energyDensity;
	float dhcalTrackTheta;
	float wireChamberTheta;


	void clearVariables();
};

} /* namespace CALICE */
#endif /* PARTICLEEVENTCOMPARATOR_HH_ */
