/*

 * LayerEfficiencyProcessor.hh
 *
 *  Created on: Feb 28, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef LAYEREFFICIENCYPROCESSOR_HH_
#define LAYEREFFICIENCYPROCESSOR_HH_

#include "StraightLineFit.hh"
#include "DhcalMapping.hh"

// lcio
#include "EVENT/Cluster.h"
#include "EVENT/LCEvent.h"
#include "EVENT/LCRunHeader.h"

// marlin
#include "marlin/Processor.h"

// root
#include "TVector3.h"
#include "TH1D.h"
#include "TH3F.h"
#include "TTree.h"
#include "TFile.h"

// c++
#include <map>
#include <utility>
#include <string>

namespace CALICE {

class LayerEfficiencyProcessor: public marlin::Processor {
public:
	LayerEfficiencyProcessor();
	virtual ~LayerEfficiencyProcessor();

	LayerEfficiencyProcessor* newProcessor() {return new LayerEfficiencyProcessor();}

	virtual void init();
	virtual void processRunHeader(lcio::LCRunHeader*);
	virtual void processEvent(lcio::LCEvent*);
	virtual void end();

protected:
	std::string _layerClusterCollection;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::string _layerIdentifier;
	std::string _rootFileName;
	DhcalMapping* _mapping;

	int _maxTotalHits;
	int _minActiveLayers;

	int _minLayer;
	int _maxLayer;
	int _maxClusterSize;
	int _nLayerNeighbours;
	int _minLayerNeighbourMIPs;
	int _histogramMaxClusterSize;
	double _maxChisq;
	double _maxDistance;

	std::map<int, std::vector<int> > layersToCheckMap;
	std::map<int, TH3F*> layerClusterSizeHistogramMapPositions;
	std::map<int, TH3F*> layerClusterSizeHistogramMapPositionsCluster;
	std::map<int, TH3F*> layerClusterSizeHistogramMapCellIDs;
	std::map<int, TH3F*> layerClusterSizeHistogramMapCellIDsCluster;
	std::map<std::string, TH1*> debugHistograms;

	/**
	 * Helper method to determine the neighboring layers to check
	 */
	std::vector<int> findLayersToCheck(int layer) const;

	/*
	 * Helper method for sorting MIP stub candidates. Longest first.
	 */
	static bool sortMipStubCandidates(const std::vector<EVENT::Cluster*>& c1, const std::vector<EVENT::Cluster*>& c2) {
		return c1.size() > c2.size();
	}

};

} /* namespace CALICE */
#endif /* LAYEREFFICIENCYPROCESSOR_HH_ */
