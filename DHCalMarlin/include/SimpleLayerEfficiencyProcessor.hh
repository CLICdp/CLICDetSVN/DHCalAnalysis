/*
 * SimpleLayerEfficiencyProcessor.hh
 *
 *  Created on: Dec 4, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef SIMPLELAYEREFFICIENCYPROCESSOR_HH_
#define SIMPLELAYEREFFICIENCYPROCESSOR_HH_


#include "DhcalMapping.hh"

// lcio
#include "EVENT/LCEvent.h"

// marlin
#include "marlin/Processor.h"

// root
#include "TH3F.h"

// c++
#include <map>
#include <utility>
#include <string>

namespace CALICE {

class SimpleLayerEfficiencyProcessor: public marlin::Processor {
public:
	SimpleLayerEfficiencyProcessor();
	virtual ~SimpleLayerEfficiencyProcessor();

	SimpleLayerEfficiencyProcessor* newProcessor() {return new SimpleLayerEfficiencyProcessor();}

	virtual void init();
	virtual void processEvent(lcio::LCEvent* event);
	virtual void end();

protected:
	DhcalMapping* _mapping;

	std::string _hitCollectionName;
	std::string _clusterCollectionName;
	std::string _rootFileName;

	int _minLayer;
	int _maxLayer;
	int _minTowerHits;
	int _histogramMaxClusterSize;

	std::map<int, TH3F*> layerClusterSizeHistogram3DMap;
};

} /* namespace CALICE */
#endif /* SIMPLELAYEREFFICIENCYPROCESSOR_HH_ */
