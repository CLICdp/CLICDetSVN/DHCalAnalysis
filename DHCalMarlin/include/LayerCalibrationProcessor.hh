/*
 * LayerCalibrationProcessor.hh
 *
 *  Created on: Jun 21, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef LAYERCALIBRATIONPROCESSOR_HH_
#define LAYERCALIBRATIONPROCESSOR_HH_

#include "DhcalMapping.hh"

#include "EVENT/CalorimeterHit.h"

#include "marlin/Processor.h"
#include "marlin/EventModifier.h"

namespace CALICE {

class LayerCalibrationProcessor: public marlin::Processor, public marlin::EventModifier {
public:
	LayerCalibrationProcessor();
	virtual ~LayerCalibrationProcessor();

	LayerCalibrationProcessor* newProcessor() {return new LayerCalibrationProcessor();}

	void init();
	//virtual void processEvent(lcio::LCEvent* event);
	void modifyEvent(lcio::LCEvent* event);

	const std::string& name() const {
	    return marlin::Processor::name();
	}

protected:
	DhcalMapping* _mapping;
	float meanMultiplicity;
	float meanEfficiency;
	std::string _inputCollectionName;

};

} /* namespace CALICE */
#endif /* LAYERCALIBRATIONPROCESSOR_HH_ */
