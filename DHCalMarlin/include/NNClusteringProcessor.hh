/*
 * NNClusteringProcessor.h
 *
 *  Created on: Feb 21, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef NNCLUSTERINGPROCESSOR_HH_
#define NNCLUSTERINGPROCESSOR_HH_

#include "LayerColumnRowMap.hh"

#include "EVENT/CalorimeterHit.h"
#include "IMPL/ClusterImpl.h"
#include "UTIL/CellIDDecoder.h"

// marlin
#include "marlin/Processor.h"

#include <set>

namespace CALICE {

class NNClusteringProcessor: public marlin::Processor {
public:
	NNClusteringProcessor();
	virtual ~NNClusteringProcessor();

	NNClusteringProcessor* newProcessor() {
		return new NNClusteringProcessor();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent*);
	virtual void end();

protected:
	std::vector<std::string> _inputCollectionNames;
	std::string _outputCollectionName;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::string _layerIdentifier;
	int _uvLimit;
	int _layerLimit;
	float _energyCut;

	LayerColumnRowMap<EVENT::CalorimeterHit*>* _hitMap;
	std::set<CalorimeterHit*> _usedHits;
	UTIL::CellIDDecoder<EVENT::CalorimeterHit>* _idDecoder;

	/*
	 * Helper method that adds a hit to a cluster. It also adds all neighbours (and their neighbours, ...)
	 * recursively to the cluster if they have not been added, yet.
	 */
	void addHitWithNeighboursToCluster(EVENT::CalorimeterHit* hit, IMPL::ClusterImpl* cluster);
};

} /* namespace CALICE */
#endif /* NNCLUSTERINGPROCESSOR_HH_ */
