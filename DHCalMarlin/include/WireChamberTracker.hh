/*
 * WireChamberTracker.hh
 *
 *  Created on: Feb 26, 2013
 *      Author: William Nash, BU + CERN,
 */


#ifndef WIRE_CHAMBER_TRACKER_HH_
#define WIRE_CHAMBER_TRACKER_HH_ 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include "EVENT/TrackerHit.h"
#include "ExtendedTrack.hh"
#include "TVector3.h"
#include "TMatrixTSym.h"

#include <TH1F.h>
#include <TFile.h>

#include <string>

using EVENT::TrackerHit;

namespace CALICE{

class WireChamberTracker : public marlin::Processor{
	public:
		WireChamberTracker();
		virtual ~WireChamberTracker();

		WireChamberTracker* newProcessor(){return new WireChamberTracker();}

		virtual void init();
		virtual void processEvent(EVENT::LCEvent*);
		virtual void end();

	protected:
		float _maxChi2;

		int _Total0Count;
		int _Total1Count;
		int _Total2Count;
		int _Layer0Count;
		int _Layer1Count;
		int _Layer2Count;

		int _TotalCount;
		int _NumFits;

		std::string _inputCollection;
		std::string _outputCollection;

	private:

};

}/*Namespace CALICE*/

#endif
