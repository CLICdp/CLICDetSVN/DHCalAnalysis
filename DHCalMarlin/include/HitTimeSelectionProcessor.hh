/*
 * HitTimeSelectionProcessor.hh
 *
 *  Created on: Mar 17, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef HITTIMESELECTIONPROCESSOR_HH_
#define HITTIMESELECTIONPROCESSOR_HH_

// lcio
#include "EVENT/LCEvent.h"

// marlin
#include <marlin/Processor.h>

namespace CALICE {

class HitTimeSelectionProcessor: public marlin::Processor {
public:
	HitTimeSelectionProcessor();
	virtual ~HitTimeSelectionProcessor();

	inline HitTimeSelectionProcessor* newProcessor() {
		return new HitTimeSelectionProcessor();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent* event);
	virtual void end();

protected:
	std::string _inputCollectionName;
	std::string _outputCollectionName;

	float _minHitTime;
	float _maxHitTime;
};

} /* namespace CALICE */
#endif /* HITTIMESELECTIONPROCESSOR_HH_ */
