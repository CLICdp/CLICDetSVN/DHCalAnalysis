/*
 * DhcalUtil.hh
 *
 *  Created on: Feb 27, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef DHCALUTIL_HH_
#define DHCALUTIL_HH_

#include "DhcalMapping.hh"
#include "StraightLineFit.hh"

// lcio
#include "EVENT/Cluster.h"
#include "EVENT/LCCollection.h"
#include "EVENT/Track.h"
#include "EVENT/TrackerHit.h"
#include "IMPL/ClusterImpl.h"
#include "UTIL/CellIDDecoder.h"
#include "UTIL/BitField64.h"

// root
#include "TMatrixTSym.h"
#include "TVector3.h"

// c++
#include <map>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

namespace CALICE {

/**
 * Collection of helper methods and convenience methods
 */
namespace DhcalUtil {

/*********************************************************************************************************
 *
 * Conversion of numbers to strings. Use std::to_string instead in C++ 11.
 *
 *********************************************************************************************************/

inline std::string to_string(int val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(long val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(long long val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(unsigned val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(unsigned long val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(unsigned long long val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(float val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(double val) {
	std::stringstream s;
	s << val;
	return s.str();
}

inline std::string to_string(long double val) {
	std::stringstream s;
	s << val;
	return s.str();
}


/*********************************************************************************************************
 *
 * Access to cell ID
 *
 *********************************************************************************************************/

inline lcio::long64 getCellID(const EVENT::CalorimeterHit* hit) {
	return lcio::long64(hit->getCellID0() & 0xffffffff) | (lcio::long64(hit->getCellID1()) << 32);
}


/*********************************************************************************************************
 *
 * Selections from vectors of hits and layers
 *
 *********************************************************************************************************/

/*
 * Removes all calorimeter hits below the given energy cut
 */
void applyEnergyCut(std::vector<CalorimeterHit*>& hits, float energyCut);

/*
 * Removes all calorimeter hits below the given energy cut
 */
void applyEnergyCut(std::vector<const CalorimeterHit*>& hits, float energyCut);

/*
 * Removes all clusters with less hits from the vector
 */
void selectMinHitsClusters(std::vector<EVENT::Cluster*>& clusters, int minHits);

/*
 * Removes all clusters with more hits from the vector
 */
void selectMaxHitsClusters(std::vector<EVENT::Cluster*>& clusters, int maxHits);

/*********************************************************************************************************
 *
 * Linear Regression
 *
 *********************************************************************************************************/

StraightLineFit* leastSquaresRegression(const std::vector<double>& dependantVariable,
		const std::vector<double>& independantVariable);

StraightLineFit* leastSquaresRegression(const std::vector<double>& dependantVariable,
		const std::vector<double>& independantVariable, const std::vector<double>& weights);

EVENT::Track* straightLineTrackFit(const std::vector<TVector3>& points,
		const std::vector<TMatrixTSym<double> >& covariances);

EVENT::Track* straightLineTrackFit(const std::vector<EVENT::TrackerHit*>& hits);

EVENT::Track* straightLineTrackFit(const std::vector<EVENT::Cluster*>& hits);

/*********************************************************************************************************
 *
 * Calculation of mean and standard deviation
 *
 *********************************************************************************************************/

/*
 * Calculates the mean and standard deviation of a vector of unweighted values. Returns a pair containing the mean and the standard deviation.
 */
std::pair<double, double> calculateStandardDeviation(const std::vector<double>& values);

/*
 * Calculates the mean and standard deviation of a vector of weighted values. Returns a pair containing the mean and the standard deviation.
 */
std::pair<double, double> calculateStandardDeviation(const std::vector<double>& values,
		const std::vector<double>& weights);

/*********************************************************************************************************
 *
 * Cluster properties
 *
 *********************************************************************************************************/

/*
 * Calculates the cluster position as the energy weighted center of the cluster
 * Calculates the cluster covariance as the cluster extend in x, y, z
 * Calculates the cluster direction theta and phi as the direction of the first principal component
 */
void calculateClusterProperties(IMPL::ClusterImpl* cluster);

/*
 * Calculates the principal components for a vector of calorimeter hits
 */
TVector3 calculatePrincipalComponent(const std::vector<EVENT::CalorimeterHit*>& hits);

/*********************************************************************************************************
 *
 * Mapping column, row and layer to hits
 *
 *********************************************************************************************************/

/*
 * Builds a map of indices to the vector of corresponding hits from a vector of calorimeter hit collections
 */
std::map<int, std::vector<EVENT::CalorimeterHit*> > buildHitMap(
		const std::vector<const EVENT::LCCollection*>& collections, const std::string& identifier);

/*
 * Builds a map of indices to the vector of corresponding hits from a calorimeter hit collection
 */
std::map<int, std::vector<EVENT::CalorimeterHit*> > buildHitMap(const EVENT::LCCollection* collection,
		const std::string& identifier);

/*
 * Builds a map of indices to the vector of corresponding hits from a vector of hits
 */
std::map<int, std::vector<EVENT::CalorimeterHit*> > buildHitMap(const std::vector<EVENT::CalorimeterHit*>& hits,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder, const std::string& identifier);

/*
 * Builds a map of indices to the vector of corresponding hits from a vector of hits
 */
std::map<int, std::vector<EVENT::CalorimeterHit*> > buildHitMap(const std::vector<EVENT::CalorimeterHit*>& hits,
		UTIL::BitField64& idDecoder, const std::string& identifier);

/*
 * Builds a map of column numbers to the vector of corresponding hits from a vector of calorimeter hit collections
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildColumnHitMap(
		const std::vector<const EVENT::LCCollection*>& collections, const std::string& uIdentifier =
				DhcalMapping::instance()->U_ID()) {
	return buildHitMap(collections, uIdentifier);
}
;

/*
 * Builds a map of column numbers to the vector of corresponding hits from a calorimeter hit collection
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildColumnHitMap(const EVENT::LCCollection* collection,
		const std::string& uIdentifier = DhcalMapping::instance()->U_ID()) {
	return buildHitMap(collection, uIdentifier);
}

/*
 * Builds a map of column numbers to the vector of corresponding hits from a vector of hits
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildColumnHitMap(
		const std::vector<EVENT::CalorimeterHit*>& hits, UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& uIdentifier = DhcalMapping::instance()->U_ID()) {
	return buildHitMap(hits, idDecoder, uIdentifier);
}

/*
 * Builds a map of row numbers to the vector of corresponding hits from a vector of calorimeter hit collections
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildRowHitMap(
		const std::vector<const EVENT::LCCollection*>& collections, const std::string& vIdentifier =
				DhcalMapping::instance()->V_ID()) {
	return buildHitMap(collections, vIdentifier);
}

/*
 * Builds a map of row numbers to the vector of corresponding hits from a calorimeter hit collection
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildRowHitMap(const EVENT::LCCollection* collection,
		const std::string& vIdentifier = DhcalMapping::instance()->V_ID()) {
	return buildHitMap(collection, vIdentifier);
}

/*
 * Builds a map of row numbers to the vector of corresponding hits from a vector of hits
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildRowHitMap(
		const std::vector<EVENT::CalorimeterHit*>& hits, UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& vIdentifier = DhcalMapping::instance()->V_ID()) {
	return buildHitMap(hits, idDecoder, vIdentifier);
}

/*
 * Builds a map of layer numbers to the vector of corresponding hits from a vector of calorimeter hit collections
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildLayerHitMap(
		const std::vector<const EVENT::LCCollection*>& collections, const std::string& layerIdentifier =
				DhcalMapping::instance()->LAYER_ID()) {
	return buildHitMap(collections, layerIdentifier);
}

/*
 * Builds a map of layer numbers to the vector of corresponding hits from a calorimeter hit collection
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildLayerHitMap(const EVENT::LCCollection* collection,
		const std::string& layerIdentifier = DhcalMapping::instance()->LAYER_ID()) {
	return buildHitMap(collection, layerIdentifier);
}

/*
 * Builds a map of layer numbers to the vector of corresponding hits from a vector of hits
 */
inline std::map<int, std::vector<EVENT::CalorimeterHit*> > buildLayerHitMap(
		const std::vector<EVENT::CalorimeterHit*>& hits, UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& layerIdentifier = DhcalMapping::instance()->LAYER_ID()) {
	return buildHitMap(hits, idDecoder, layerIdentifier);
}

/*********************************************************************************************************
 *
 * Mapping layer number to clusters
 *
 *********************************************************************************************************/

/*
 * Builds a map of layer numbers to the vector of corresponding clusters from a cluster hit collection.
 * The layer number of a cluster is determined as the lowest layer number of the hits in the cluster.
 * Requires that the CellIDEncoding of the calorimeter hits is stored as a parameter of the cluster collection.
 */
std::map<int, std::vector<EVENT::Cluster*> > buildLayerClusterMap(const EVENT::LCCollection* collection,
		const std::string& layerIdentifier = DhcalMapping::instance()->LAYER_ID());

/*
 * Builds a map of layer numbers to the vector of corresponding clusters from a cluster hit collection.
 * The layer number of a cluster is determined as the lowest layer number of the hits in the cluster.
 */
std::map<int, std::vector<EVENT::Cluster*> > buildLayerClusterMap(const EVENT::LCCollection* collection,
		UTIL::CellIDDecoder<CalorimeterHit>& idDecoder,
		const std::string& layerIdentifier = DhcalMapping::instance()->LAYER_ID());

/*
 * Builds a map of layer numbers to the vector of corresponding clusters from a vector of clusters.
 * The layer number of a cluster is determined as the lowest layer number of the hits in the cluster.
 */
std::map<int, std::vector<EVENT::Cluster*> > buildLayerClusterMap(const std::vector<EVENT::Cluster*>& clusters,
		UTIL::CellIDDecoder<CalorimeterHit>& idDecoder,
		const std::string& layerIdentifier = DhcalMapping::instance()->LAYER_ID());

/*********************************************************************************************************
 *
 * Hit comparisons
 *
 *********************************************************************************************************/

/*
 * Helper method to compare U of two hits. Returns true if U of first hit lower than U of second hit, false otherwise.
 */
bool compareU(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& uIdentifier = DhcalMapping::instance()->U_ID());

/*
 * Helper method to compare V of two hits. Returns true if V of first hit lower than V of second hit, false otherwise.
 */
bool compareV(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& vIdentifier = DhcalMapping::instance()->V_ID());

/*
 * Helper method to compare layer of two hits. Returns true if layer of first hit lower than layer of second hit, false otherwise.
 */
bool compareLayer(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& vIdentifier = DhcalMapping::instance()->LAYER_ID());

/*
 * Helper method to compare UV of two hits. Returns true if UV of first hit lower than UV of second hit, false otherwise.
 */
bool compareUV(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder, const std::string& uIdentifier,
		const std::string& vIdentifier = DhcalMapping::instance()->V_ID());

/*
 * Helper method to compare x position of two hits. Returns true if x of first hit lower than x of second hit, false otherwise.
 */
bool compareX(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit);

/*
 * Helper method to compare y position of two hits. Returns true if y of first hit lower than y of second hit, false otherwise.
 */
bool compareY(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit);

/*
 * Helper method to compare z position of two hits. Returns true if z of first hit lower than z of second hit, false otherwise.
 */
bool compareZ(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit);

/*
 * Helper method to compare xy radius of two hits. Returns true if xy of first hit lower than xy of second hit, false otherwise.
 */
bool compareXY(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit);

/*
 * Helper method to compare xyz radius of two hits. Returns true if xyz of first hit lower than xyz of second hit, false otherwise.
 */
bool compareXYZ(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit);

/*********************************************************************************************************
 *
 * Consistency checks between two hits
 *
 *********************************************************************************************************/

/*
 * Helper method to check if two hits are within the given distance in cells to each other in U
 */
bool consistentInU(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, int uLimit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& uIdentifier = DhcalMapping::instance()->U_ID());

/*
 * Helper method to check if two hits are within the given distance in cells to each other in V
 */
bool consistentInV(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, int vLimit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& vIdentifier = DhcalMapping::instance()->V_ID());

/*
 * Helper method to check if two hits are within the given distance in cells to each other in the UV plane
 */
bool consistentInUV(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float maxDistance,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder,
		const std::string& uIdentifier = DhcalMapping::instance()->U_ID(), const std::string& vIdentifier =
				DhcalMapping::instance()->V_ID());

/*
 * Helper method to check if two hits are within the given distance to each other in layer
 */
bool consistentInLayer(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, int layerLimit,
		UTIL::CellIDDecoder<EVENT::CalorimeterHit>& idDecoder, const std::string& layerIdentifier =
				DhcalMapping::instance()->LAYER_ID());

/*
 * Helper method to check if two hits are within the given distance to each other in x
 */
bool consistentInX(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float xLimit);

/*
 * Helper method to check if two hits are within the given distance to each other in y
 */
bool consistentInY(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float yLimit);

/*
 * Helper method to check if two hits are within the given distance to each other within the xy plane
 */
bool consistentInXY(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float maxDistance);

/*
 * Helper method to check if two hits are within the given distance to each other in z
 */
bool consistentInZ(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float zLimit);

/*
 * Helper method to check if two hits are within the given distance to each
 */
bool consistentInXYZ(const EVENT::CalorimeterHit* hit, const EVENT::CalorimeterHit* otherHit, float maxDistance);

} /* namespace DhcalUtil */

} /* namespace CALICE */

#endif /* DHCALUTIL_HH_ */
