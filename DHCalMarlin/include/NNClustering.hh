/*
 * NNClustering.hh
 *
 *  Created on: Feb 21, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef NNCLUSTERING_HH_
#define NNCLUSTERING_HH_

#include "CalorimeterHitComparator.hh"

// lcio
#include "EVENT/CalorimeterHit.h"
#include "EVENT/Cluster.h"
#include "EVENT/LCCollection.h"
#include "IMPL/ClusterImpl.h"
#include "UTIL/CellIDDecoder.h"

// c++
#include <map>
#include <string>
#include <vector>

namespace CALICE {

/*
 * Simple nearest neighbour clustering algorithm. Using cell IDs to identify neighbours.
 */
class NNClustering {
public:
	NNClustering();
	virtual ~NNClustering();

	/*
	 * Sets the cell ID string for U
	 */
	void setUIdentifier(const std::string& identifier) {
		_uIdentifier = identifier;
	}

	/*
	 * Sets the cell ID string for V
	 */
	void setVIdentifier(const std::string& identifier) {
		_vIdentifier = identifier;
	}

	/*
	 * Sets the cell ID string for layer
	 */
	void setLayerIdentifier(const std::string& identifier) {
		_layerIdentifier = identifier;
	}

	/*
	 * Sets the minimum energy for hits to be used in the clustering
	 */
	void setEnergyCut(float cut) {
		_energyCut = cut;
	}

	/*
	 * Adds an input collection that should be clustered
	 */
	void addCollection(const EVENT::LCCollection* collection);

	/*
	 * Clears the input collections
	 */
	void clear() {
		_collections.clear();
	}

	/*
	 * Returns the cell ID encoding string used for the clustering
	 */
	std::string getCellIdEncoding();

	/*
	 * Runs the nearest neighbour clustering on the input collections.
	 * Clusters all hits within the given distance in cells in UV and the given layer distance.
	 */
	std::vector<EVENT::Cluster*> findClusters(int uvLimit, int layerLimit = 0) const;

	/*
	 * Runs the nearest neighbour clustering on the input collections.
	 * Clusters all hits within the given Cartesian distance XY and the given layer distance.
	 */
	std::vector<EVENT::Cluster*> findClusters(float xyLimit, int layerLimit = 0) const;

	/*
	 * Runs the nearest neighbour clustering on the input collections.
	 * Clusters all hits within the given Cartesian distance in XYZ.
	 */
	std::vector<EVENT::Cluster*> findClusters(float xyzLimit) const;

	/*
	 * Runs the nearest neighbour clustering on the input collections.
	 * Clusters all hits within the consistent check of the given CalorimeterHitComparator.
	 */
	std::vector<EVENT::Cluster*> findClusters(const CalorimeterHitComparator& comparator) const;

	/*
	 * Runs the nearest neighbour clustering on the given list of hits.
	 * Clusters all hits within the consistent check of the given CalorimeterHitComparator.
	 */
	std::vector<EVENT::Cluster*> findClusters(const std::vector<EVENT::CalorimeterHit*>& hits,
			const CalorimeterHitComparator& comparator) const;

	/*
	 * Runs the nearest neighbour clustering on the given list of clusters.
	 * Clusters all hits within the consistent check of the given CalorimeterHitComparator.
	 */
	std::vector<EVENT::Cluster*> findClusters(const std::vector<EVENT::Cluster*>& clusters,
			const CalorimeterHitComparator& comparator) const;

	/*
	 * Runs the nearest neighbour clustering on the given list of clusters.
	 * Clusters all hits within the consistent check of the given CalorimeterHitComparator.
	 */
	std::vector<EVENT::Cluster*> findClusters(const std::vector<IMPL::ClusterImpl*>& clusters,
			const CalorimeterHitComparator& comparator) const;

protected:
	std::vector<const EVENT::LCCollection*> _collections;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::string _layerIdentifier;
	float _energyCut;
	mutable UTIL::CellIDDecoder<CalorimeterHit>* _idDecoder;
};

} /* namespace CALICE */
#endif /* NNCLUSTERING_HH_ */
