/*
 * DhcalProgressProcessor.hh
 *
 *  Created on: Feb 23, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef DHCALPROGRESSPROCESSOR_HH_
#define DHCALPROGRESSPROCESSOR_HH_

// marlin
#include "marlin/Processor.h"

namespace CALICE {

class DhcalProgressProcessor: public marlin::Processor {
public:
	DhcalProgressProcessor();
	virtual ~DhcalProgressProcessor();

	DhcalProgressProcessor* newProcessor() {return new DhcalProgressProcessor();}

	virtual void init();
	virtual void processRunHeader(lcio::LCRunHeader*);
	virtual void processEvent(lcio::LCEvent*);
	virtual void end();

protected:
	int _interval;
	int _processedEvents;
};

} /* namespace CALICE */
#endif /* DHCALPROGRESSPROCESSOR_HH_ */
