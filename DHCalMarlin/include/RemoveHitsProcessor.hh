/*
 * RemoveHitsProcesser.hh
 *
 *  Created on: Jun 7, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef REMOVEHITSPROCESSOR_HH_
#define REMOVEHITSPROCESSOR_HH_

#include "DhcalMapping.hh"

#include "EVENT/CalorimeterHit.h"
#include "UTIL/CellIDDecoder.h"

// marlin
#include "marlin/Processor.h"

// c++
#include <string>
#include <set>

namespace CALICE {

class RemoveHitsProcessor : public marlin::Processor {
public:
	RemoveHitsProcessor();
	virtual ~RemoveHitsProcessor();

	RemoveHitsProcessor* newProcessor() {return new RemoveHitsProcessor();}

	virtual void init();
	virtual void processEvent(lcio::LCEvent* event);

protected:
	std::string _inputCollectionName;
	std::string _outputCollectionName;
	DhcalMapping* _dhcalMapping;
};

} /* namespace CALICE */
#endif /* REMOVEHITSPROCESSOR_HH_ */
