#ifndef PATTERN_PROCESSOR_HH
#define PATTERN_PROCESSOR_HH 
#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include "EVENT/CalorimeterHit.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TCanvas.h"
#include "TVectorD.h"
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include <map>
#include <iostream>
#include "DhcalMapping.hh"
using namespace std;
class findPattern : public marlin::Processor{
public:
  findPattern();
  virtual ~findPattern();
  //TFile * hfile;  
  findPattern* newProcessor(){return new findPattern();}
  virtual void init();
  virtual void processRunHeader(lcio::LCRunHeader*);
  virtual void processEvent(lcio::LCEvent*);
  virtual void end();
  TH2D * findHotCells(TH2D* HistToBeChecked, TString name, int totalNumberOfEvents);
  void clearAll();
  TFile * patternTFile;
  TFile * resultsTFile;
  
protected:
  map <int,TH2D*> patternMap;
  TH2D * patterHisto;
  string _outputRootFile;
  string _inputCollection;
  string _metaFile;
  
  string _cellFile;
private:
  
  int totalNumberOfEvents;
  EVENT::CalorimeterHit* calohit;
  
};

#endif
