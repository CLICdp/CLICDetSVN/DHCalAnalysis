/*
 * RPCSimProcessor2.hh
 *
 *  Created on: Oct 17, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef RPCSIMPROCESSOR2_HH_
#define RPCSIMPROCESSOR2_HH_

#include "DhcalMapping.hh"
#include "ColumnRowHitMap.hh"
#include "RPCSimChargeSpreadModel.hh"

// LCIO
#include "EVENT/LCEvent.h"

// Marlin
#include "marlin/Processor.h"

// ROOT
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TRandom3.h"
#include "TVector3.h"

// C++
#include <limits>
#include <list>
#include <map>
#include <set>
#include <string>


namespace CALICE {

class RPCSimProcessor2: public marlin::Processor {
public:
	RPCSimProcessor2();
	virtual ~RPCSimProcessor2();

	RPCSimProcessor2* newProcessor() {
		return new RPCSimProcessor2();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent* event);
	virtual void end();

protected:

	/**
	 * Container class for all relevant information of a contribution to a SimCalorimeterHit
	 */
	struct HitContribution {
		// constructor
		HitContribution(const TVector3& position = TVector3(), double time = 0., double charge = 0.,
				const SimCalorimeterHit* hit = 0) :
				position(position), time(time), charge(charge) {
			this->hit = hit;
		}
		// destructor
		~HitContribution() {
		}

		// allow sorting in time
		bool operator<(const HitContribution& other) const {
			return this->time > other.time;
		}

		TVector3 position;
		double time;
		mutable double charge;
		const SimCalorimeterHit* hit;
	};

	struct HitContributionComparator {
		bool operator() (const HitContribution* first, const HitContribution* second) const {
			return first->time < second->time;
		}
	} hitContributionComparator;

	/**
	 * Container class to store the total charge within a pad
	 */
	class PadContributions {
	public:
		PadContributions() :
				totalCharge(0.), time(std::numeric_limits<double>::max()), contributions() {
		}

		// adds a new contribution to the pad with the corresponding charge
		void addContribution(const HitContribution* contribution, double collectedCharge) {
			contributions.insert(contribution);
			totalCharge += collectedCharge;
			if (contribution->time < time) {
				time = contribution->time;
			}
		}

		// gets the earliest time of any contribution
		double getTime() const {
			return time;
		}

		// gets the total charge collected in this pad
		double getTotalCharge() const {
			return totalCharge;
		}

		// get all contributions to this pad
		const std::set<const HitContribution*>& getContributions() const {
			return contributions;
		}

	protected:
		double totalCharge;
		double time;
		std::set<const HitContribution*> contributions;
	};

	// checks distance of all contributions with each other. Removes the second in case of too close distance
	void applyDistanceCut(std::list<const HitContribution*>& contributions);

        // Get Scaling Factors for each contribution, saves them in HitContribution.charge to do the scaling in spreadCharge
  void ScaleCharge(std::list<const HitContribution*>& contributions);

	// spreads the charge into the pad around the contribution
	void spreadCharge(const HitContribution* contribution, int layer = 0);

	// add charge deposit to the corresponding pad based on its position
	//void addCharge(const TVector3& position, double charge, const HitContribution* contribution);

	// parameters of the charge spreading model
	double _threshold;
	double _distanceCut;
        double _stepCut;
	double _q0;
	double _maxRadius;
	int _integrationSteps;

	// scaling parameters
	double _scalingParameter1;
	double _scalingParameter2;

	// the random seed used
	int _randomSeed;

	// collection names
	std::string _inputCollection;
	std::string _outputCollection;
	std::string _outputCollectionMainStack;
	std::string _outputCollectionTailCatcher;
	std::string _relationCollection;

	// cell ID encoding
	std::string _inputIdentifierU;
	std::string _inputIdentifierV;
	std::string _inputIdentifierLayer;

        // use a hard distance cut
        bool _applyDistanceCut;

	// use exponential distance cut
	bool _exponentialDistanceCut;
      
        // use scaling
        bool _scalingCharge;

        // reduce charge on RPC borders
        bool _chargeReductionAtBorders;

	// the random number generator
	TRandom3* _random;

	// the random number generator
	TF1* _chargeGenerationFunction;

	// the DHCAL geometry
	DhcalMapping* _mapping;

	// map to store all pad contributions in the current layer
	ColumnRowHitMap<PadContributions*> _layerPads;

	std::string _chargeSpreadModelName;
	std::vector<float> _chargeSpreadParameters;

	// map to store charge integral values
	TH2* _chargeIntegralMap;
	int _chargeInterpolationBins;
	int _nCellNeighboursX;
	int _nCellNeighboursY;
	double _chargeIntegralMaxX;
	double _chargeIntegralMaxY;

	// histograms and ROOT file for debugging
	std::map<std::string, TH1*> _histograms1D;
	std::map<std::string, TH2*> _histograms2D;
	std::string _rootFileName;
	TFile* _rootFile;

	// input ROOT file containing pre-calculated charge distribution map
	std::string _inputRootFileName;
	std::string _chargeIntegralMapName;
	TFile* _inputRootFile;
};

} /* namespace CALICE */
#endif /* RPCSIMPROCESSOR2_HH_ */
