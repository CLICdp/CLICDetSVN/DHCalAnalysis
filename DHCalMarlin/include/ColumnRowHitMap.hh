/*
 * ColumnRowHitMap.hh
 *
 *  Created on: Oct 22, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef COLUMNROWHITMAP_HH_
#define COLUMNROWHITMAP_HH_

#include "DhcalMapping.hh"

#include "EVENT/LCCollection.h"
#include "UTIL/BitField64.h"

#include <map>
#include <vector>

namespace CALICE {

template<class T> class ColumnRowHitMap: public std::map<int, std::map<int, T> > {
public:
	ColumnRowHitMap(const std::string& cellEncoding = DhcalMapping::instance()->CELL_ID_ENCODING(),
			const std::string& uIdentifier = DhcalMapping::instance()->U_ID(), const std::string& vIdentifier =
					DhcalMapping::instance()->V_ID()) :
			_idDecoder(cellEncoding) {
		_uIdentifier = uIdentifier;
		_vIdentifier = vIdentifier;
	}

	virtual ~ColumnRowHitMap() {
	}

	/*
	 * Adds a hit to the map using the given indices (ignores the actual cell ID).
	 */
	void fill(int columnIndex, int rowIndex, T hit) {
		(*this)[columnIndex][rowIndex] = hit;
	}

	/*
	 * Adds a hit to the map using its cell ID to determine the indices.
	 */
	void fill(T hit) {
		_idDecoder.setValue(DhcalMapping::getCellID(hit));
		int column = _idDecoder[_uIdentifier];
		int row = _idDecoder[_vIdentifier];
		(*this)[column][row] = hit;
	}

	/*
	 * Adds all hits to the map using their cell IDs to determine the indices.
	 */
	void fill(const std::vector<T>& hits) {
		typename std::vector<T>::const_iterator itHit = hits.begin();
		while (itHit != hits.end()) {
			fill(*itHit);
			++itHit;
		}
	}

	/*
	 * Adds all hits from a collection to the map using their cell ID to determine the indices.
	 */
	void fill(const EVENT::LCCollection* collection) {
		for (int index = 0; index < collection->getNumberOfElements(); index++) {
			fill(dynamic_cast<T>(collection->getElementAt(index)));
		}
	}

	/*
	 * Checks if a hit exist at the given indices.
	 */
	bool contains(int columnIndex, int rowIndex) const {
		return getHit(columnIndex, rowIndex) != NULL;
	}

	/*
	 * Returns the hit with the given indices if it exists. Returns a null pointer otherwise.
	 */
	T& getHit(int columnIndex, int rowIndex) const {
		typename std::map<int, std::map<int, T> >::const_iterator columnMapIt = this->find(columnIndex);
		if (columnMapIt == this->end()) {
			return NULL;
		}
		typename std::map<int, T>::const_iterator rowMapIt = columnMapIt->second.find(rowIndex);
		if (rowMapIt == columnMapIt->second.end()) {
			return NULL;
		}
		return rowMapIt->second;
	}

	/*
	 * Returns all hits within the given limits if it exists.
	 */
	std::vector<T> getHits(int minColumn, int maxColumn, int minRow, int maxRow) const {
		std::vector<T> hits;
		for (int iColumn = minColumn; iColumn <= maxColumn; iColumn++) {
			for (int iRow = minRow; iRow <= maxRow; iRow++) {
				T hit = this->getHit(iColumn, iRow);
				if (hit) {
					hits.push_back(hit);
				}
			}
		}
		return hits;
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given cell indices within the given limits.
	 */
	std::vector<T> getNeighbours(int columnIndex, int rowIndex, int columnDistance = 1, int rowDistance = 1) const {
		std::vector<T> neighbours;
		for (int iColumn = columnIndex - columnDistance; iColumn <= columnIndex + columnDistance; iColumn++) {
			for (int iRow = rowIndex - rowDistance; iRow <= rowIndex + rowDistance; iRow++) {
				T neighbour = this->getHit(iColumn, iRow);
				if (neighbour and not (iColumn == columnIndex and iRow == rowIndex)) {
					neighbours.push_back(neighbour);
				}
			}
		}
		return neighbours;
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given cell ID within the given limits.
	 */
	std::vector<T> getNeighbours(const lcio::long64& cellID, int columnDistance = 1, int rowDistance = 1) const {
		_idDecoder.setValue(cellID);
		return this->getNeighbours(_idDecoder[_uIdentifier], _idDecoder[_vIdentifier], columnDistance, rowDistance);
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given hit within the given limits.
	 */
	std::vector<T> getNeighbours(const T& hit, int columnDistance = 1, int rowDistance = 1) const {
		return this->getNeighbours(DhcalMapping::getCellID(hit), columnDistance, rowDistance);
	}

protected:
	mutable UTIL::BitField64 _idDecoder;
	std::string _uIdentifier;
	std::string _vIdentifier;
};

/*
 * Pointer specialization. This is the one usually used.
 */

template<class T> class ColumnRowHitMap<T*> : public std::map<int, std::map<int, T*> > {
public:
	ColumnRowHitMap(const std::string& cellEncoding = DhcalMapping::instance()->CELL_ID_ENCODING(),
			const std::string& uIdentifier = DhcalMapping::instance()->U_ID(), const std::string& vIdentifier =
					DhcalMapping::instance()->V_ID()) :
			_idDecoder(cellEncoding) {
		_uIdentifier = uIdentifier;
		_vIdentifier = vIdentifier;
		_cellIdMap = new std::map<lcio::long64, T*>();
	}

	virtual ~ColumnRowHitMap() {
		if (_cellIdMap) {
			delete _cellIdMap;
		}
	}

	void clear() {
		_cellIdMap->clear();
		std::map<int, std::map<int, T*> >::clear();
	}

	void deleteObjects() {
		typename std::map<lcio::long64, T*>::iterator itMap = _cellIdMap->begin();
		while (itMap != _cellIdMap->end()) {
			delete itMap->second;
			++itMap;
		}
		this->clear();
	}

	/*
	 * Adds a hit to the map using the given indices (ignores the actual cell ID).
	 */
	void fill(int columnIndex, int rowIndex, T* hit) {
		_idDecoder.reset();
		_idDecoder[_uIdentifier] = columnIndex;
		_idDecoder[_vIdentifier] = rowIndex;
		(*_cellIdMap)[_idDecoder.getValue()] = hit;
		(*this)[columnIndex][rowIndex] = hit;
	}

	/*
	 * Adds a hit to the map using the given cell ID (ignores the actual cell ID).
	 */
	void fill(const lcio::long64& cellID, T* hit) {
		_idDecoder.setValue(cellID);
		int columnIndex = _idDecoder[_uIdentifier];
		int rowIndex = _idDecoder[_vIdentifier];
		(*_cellIdMap)[cellID] = hit;
		(*this)[columnIndex][rowIndex] = hit;
	}

	/*
	 * Adds a hit to the map using its cell ID to determine the indices.
	 */
	void fill(T* hit) {
		_idDecoder.setValue(DhcalMapping::getCellID(hit));
		int column = _idDecoder[_uIdentifier];
		int row = _idDecoder[_vIdentifier];
		(*_cellIdMap)[_idDecoder.getValue()] = hit;
		(*this)[column][row] = hit;
	}

	/*
	 * Adds all hits to the map using their cell IDs to determine the indices.
	 */
	void fill(const std::vector<T*>& hits) {
		typename std::vector<T*>::const_iterator itHit = hits.begin();
		while (itHit != hits.end()) {
			fill(*itHit);
			++itHit;
		}
	}

	/*
	 * Adds all hits from a collection to the map using their cell ID to determine the indices.
	 */
	void fill(const EVENT::LCCollection* collection) {
		for (int index = 0; index < collection->getNumberOfElements(); index++) {
			fill(dynamic_cast<T*>(collection->getElementAt(index)));
		}
	}

	/*
	 * Returns a 1D map of all cell IDs to the corresponding hits
	 */
	const std::map<lcio::long64, T*>* getCellIdMap() {
		return _cellIdMap;
	}

	/*
	 * Checks if a hit exist at the given indices.
	 */
	bool contains(int columnIndex, int rowIndex) const {
		return getHit(columnIndex, rowIndex) != NULL;
	}

	/*
	 * Checks if a hit exist with the given cell ID.
	 */
	bool contains(const lcio::long64& cellID) const {
		_idDecoder.setValue(cellID);
		int columnIndex = _idDecoder[_uIdentifier];
		int rowIndex = _idDecoder[_vIdentifier];
		return getHit(columnIndex, rowIndex) != NULL;
	}

	/*
	 * Returns the hit with the given indices if it exists. Returns a null pointer otherwise.
	 */
	T* getHit(int columnIndex, int rowIndex) const {
		typename std::map<int, std::map<int, T*> >::const_iterator columnMapIt = this->find(columnIndex);
		if (columnMapIt == this->end()) {
			return NULL;
		}
		typename std::map<int, T*>::const_iterator rowMapIt = columnMapIt->second.find(rowIndex);
		if (rowMapIt == columnMapIt->second.end()) {
			return NULL;
		}
		return rowMapIt->second;
	}

	/*
	 * Returns the hit with the given cell ID if it exists. Returns a null pointer otherwise.
	 */
	T* getHit(const lcio::long64& cellID) const {
		_idDecoder.setValue(cellID);
		int columnIndex = _idDecoder[_uIdentifier];
		int rowIndex = _idDecoder[_vIdentifier];
		return getHit(columnIndex, rowIndex);
	}

	/*
	 * Returns all hits within the given limits if it exists.
	 */
	std::vector<T*> getHits(int minColumn, int maxColumn, int minRow, int maxRow) const {
		std::vector<T*> hits;
		for (int iColumn = minColumn; iColumn <= maxColumn; iColumn++) {
			for (int iRow = minRow; iRow <= maxRow; iRow++) {
				T* hit = this->getHit(iColumn, iRow);
				if (hit) {
					hits.push_back(hit);
				}
			}
		}
		return hits;
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given cell indices within the given limits.
	 */
	std::vector<T*> getNeighbours(int columnIndex, int rowIndex, int columnDistance = 1, int rowDistance = 1) const {
		std::vector<T*> neighbours;
		for (int iColumn = columnIndex - columnDistance; iColumn <= columnIndex + columnDistance; iColumn++) {
			for (int iRow = rowIndex - rowDistance; iRow <= rowIndex + rowDistance; iRow++) {
				T* neighbour = this->getHit(iColumn, iRow);
				if (neighbour and not (iColumn == columnIndex and iRow == rowIndex)) {
					neighbours.push_back(neighbour);
				}
			}
		}
		return neighbours;
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given cell ID within the given limits.
	 */
	std::vector<T*> getNeighbours(const lcio::long64& cellID, int columnDistance = 1, int rowDistance = 1) const {
		_idDecoder.setValue(cellID);
		return this->getNeighbours(_idDecoder[_uIdentifier], _idDecoder[_vIdentifier], columnDistance, rowDistance);
	}

	/*
	 * Returns a vector containing all neighbouring hits to the given hit within the given limits.
	 */
	std::vector<T*> getNeighbours(const T* hit, int columnDistance = 1, int rowDistance = 1) const {
		return this->getNeighbours(DhcalMapping::getCellID(hit), columnDistance, rowDistance);
	}

protected:
	mutable UTIL::BitField64 _idDecoder;
	std::string _uIdentifier;
	std::string _vIdentifier;
	std::map<lcio::long64, T*>* _cellIdMap;
};

} /* namespace CALICE */
#endif /* COLUMNROWHITMAP_HH_ */
