/*
 * StraightLineFit.hh
 *
 *  Created on: Feb 28, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef STRAIGHTLINEFIT_HH_
#define STRAIGHTLINEFIT_HH_

#include <cmath>
#include <utility>

namespace CALICE {

/*
 * Container for a straight line fit:
 * f(x) = a + b*x
 */
class StraightLineFit {
public:
	StraightLineFit() :
			intercept(0.), interceptVariance(0.), slope(0.), slopeVariance(0.), covariance(0.), chisq(0.), dof(0.) {
	}

	virtual ~StraightLineFit() {
	}

	// calculates y and its uncertainty for a given x
	std::pair<double, double> calculate(double x) {
		double y = intercept + slope * x;
		double ySigma = std::sqrt(std::abs(interceptVariance + 2 * x * covariance + x * x * slopeVariance));
		return std::make_pair(y, ySigma);
	}

	double intercept;			// a
	double interceptVariance;	// var(a) = sigma(a)^2
	double slope;				// b
	double slopeVariance;		// var(b) = sigma(b)^2
	double covariance;			// cov(a,b)
	double chisq;
	double dof;
};

} /* namespace CALICE */
#endif /* STRAIGHTLINEFIT_HH_ */
