/*
 * DhcalMappingProcessor.hh
 *
 *  Created on: Aug 23, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef DHCALMAPPINGPROCESSOR_HH_
#define DHCALMAPPINGPROCESSOR_HH_

#include "DhcalMapping.hh"

#include <marlin/Processor.h>

#include <string>
#include <vector>

namespace CALICE {

class DhcalMappingProcessor: public marlin::Processor {
public:
	DhcalMappingProcessor();
	virtual ~DhcalMappingProcessor();

	DhcalMappingProcessor* newProcessor() {
		return new DhcalMappingProcessor();
	}

	void init();

protected:
	DhcalMapping* _dhcalMapping;
	std::string _cellIdEncoding;
	std::vector<std::string> _moduleCalibrationFileNames;
	std::vector<std::string> _localCalibrationFileNames;
	std::vector<std::string> _ignoreCellIdFileNames;
	std::vector<std::string> _ignoreColumnRowLayerFileNames;
};

} /* namespace CALICE */
#endif /* DHCALMAPPINGPROCESSOR_HH_ */
