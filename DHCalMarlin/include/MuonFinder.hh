#ifndef MUON_FINDER_HH
#define MUON_FINDER_HH 1

#include "marlin/Processor.h"
#include "EVENT/LCEvent.h"
#include "IMPL/ClusterImpl.h"


namespace CALICE{

class MuonFinder : public marlin::Processor{
 public:
  MuonFinder();
  virtual ~MuonFinder();

  MuonFinder* newProcessor(){return new MuonFinder();}

  virtual void init();
  virtual void processEvent(EVENT::LCEvent*);
  virtual void end();

 protected:

  int _layersConsidered;
  float _chiLimit;
  int _clusterSize;
  float _averageClusters;
  std::string _inputClusterCollection;
  std::string _MuonCollection;
  int _muonCount;
  int _eventCount;
  int _skipLimit;
  int _consecutiveSkipLimit;

 private:

};

}/*Namespace CALICE*/

#endif
