/*
 * StraightLine3D.hh
 *
 *  Created on: Mar 17, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef STRAIGHTLINE3D_HH_
#define STRAIGHTLINE3D_HH_

#include "EVENT/Track.h"

#include "TVector3.h"

namespace CALICE {

/*
 * Container for a straight line in a Cartesian 3D coordinate system.
 * Provides methods for propagating a track.
 */
class StraightLine3D {
public:
	StraightLine3D(const TVector3& origin = TVector3(), double phi = 0., double theta = 0.);
	StraightLine3D(const TVector3& origin, const TVector3& direction);
	StraightLine3D(const EVENT::Track* track);
	virtual ~StraightLine3D();

	/*
	 * Calculates the distance of closest approach to the given point
	 */
	double getDistanceOfClosestApproach(const TVector3& point);

	/*
	 * Calculates the length along the line from its origin to the point of closest approach to the give point
	 */
	double getLengthToPointOfClosestApproach(const TVector3& point);

	/*
	 * Calculates the point of closest approach on the line to the given point
	 */
	TVector3 getPointOfClosestApproach(const TVector3& point);

	/*
	 * Calculates the position on the line at the given distance from its origin
	 */
	TVector3 getPointAtLength(double length);

	/*
	 * Calculates the length along the line from its origin to the intersection with the given plane
	 */
	double getLengthToIntersectionWithPlane(const TVector3& planeOrigin, const TVector3& planeNormal);

	/*
	 * Calculates the intersection of the line with the plane
	 */
	TVector3 getIntersectionWithPlane(const TVector3& planeOrigin, const TVector3& planeNormal);

	/*
	 * Calculates the position on the line for the given z
	 */
	TVector3 getPositionAtZ(double z = 0.);

	/*
	 * Access to the origin
	 */
	const TVector3& origin() const {
		return _origin;
	}

	/*
	 * Access to the direction
	 */
	const TVector3& direction() const {
		return _direction;
	}

protected:
	TVector3 _origin;
	TVector3 _direction;
};

} /* namespace CALICE */
#endif /* STRAIGHTLINE3D_HH_ */
