/*
 * WireChamberCalibrationProcessor.hh
 *
 *  Created on: Mar 5, 2013
 *      Author: Christian Grefe, CERN
 */

#ifndef WIRECHAMBERCALIBRATIONPROCESSOR_HH_
#define WIRECHAMBERCALIBRATIONPROCESSOR_HH_

// lcio
#include "EVENT/LCEvent.h"
#include "EVENT/TrackerData.h"

// marlin
#include "marlin/Processor.h"

// root
#include "TVector3.h"

// c++
#include <map>
#include <string>
#include <vector>

namespace CALICE {

enum WireChamberSide {
	horizontal, vertical
};

enum ReadoutSide {
	left, right
};

/*
 * Container to hold wire chamber information and hits.
 */
struct WireChamber {
public:
	TVector3 position;
	std::map<WireChamberSide, float> resolutions;
	std::map<WireChamberSide, float> slopes;
	std::map<WireChamberSide, std::map<ReadoutSide, std::vector<EVENT::TrackerData*> > > rawHits;
};

/*
 * Processor to build calibrated wire chamber hits from TrackerData
 */
class WireChamberCalibrationProcessor: public marlin::Processor {
public:
	WireChamberCalibrationProcessor();
	virtual ~WireChamberCalibrationProcessor();

	inline WireChamberCalibrationProcessor* newProcessor() {
		return new WireChamberCalibrationProcessor();
	}

	virtual void init();
	virtual void processEvent(EVENT::LCEvent* event);
	virtual void end();

protected:
	std::string _inputCollectionName;
	std::string _outputCollectionName;

	std::vector<float> _horizontalSlopes;
	std::vector<float> _verticalSlopes;
	std::vector<float> _horizontalResolutions;
	std::vector<float> _verticalResolutions;
	std::vector<float> _xPositions;
	std::vector<float> _yPositions;
	std::vector<float> _zPositions;
	float _ns_per_count;

	std::map<int, WireChamber> _wireChambers;

	void clearWireChamberHits();
};

} /* namespace CALICE */
#endif /* WIRECHAMBERCALIBRATIONPROCESSOR_HH_ */
