#!/bin/bash



InputFile=$'dhcLcio-Run660435.slcio'
OutputFile=$'cleanedMC300GeV.slcio'



echo $InputFile




Marlin --global.LCIOInputFiles="$InputFile" --PreTest.Collection=DhcHits PreTest.xml 
Marlin --global.LCIOInputFiles="$InputFile" --HitSelection.InputCollection=DhcHits RemoveNoiseAndBoxEvents.xml
rm preTest.txt
Marlin --global.LCIOInputFiles="RemoveNoisyCellsTest.slcio" --findPattern.Collection=DhcHits --findPattern.CellFile="/tmp/badCells660442.txt" --findPattern.RootFile="/tmp/noeAnnet.root" FindPattern.xml
Marlin --global.LCIOInputFiles="RemoveNoisyCellsTest.slcio" --Output.LCIOOutputFile=$OutputFile --HitSelection.CellIdFile="bad300GeVCells.txt" --HitSelection.InputCollection=DhcHits --HitSelection.OutputCollection="cleanedDhcHits" RemoveNoisyCells.xml
