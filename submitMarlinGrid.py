# THIS SCRIPT USES THE NEW API
# author: jan.strube@cern.ch
from DIRAC.Core.Base import Script
Script.initialize()
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import SLIC, LCSIM, SLICPandora, Marlin, OverlayInput
from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient
import sys
import os.path as path
import re


if __name__ == '__main__':
    Script.parseCommandLine()
    dirac = DiracILC(True, "repository.cfg")
    sandboxList = []
    job = UserJob()
    job.setName("DHCalMarlinTest")
    job.setJobGroup("DHCAL")
    job.setInputSandbox(sandboxList)

    app = Marlin()
    app.setVersion('011301')
    app.setSteeringFile('DHCalMarlin/steer/BoxFinding.xml')
    # app.setOutputFile("out.slcio")
    app.setInputFile('LFN:/ilc/user/c/cgrefe/calice/dhcal/lcio/Conversion-2013_02_18/dhcLcio-Run660052.slcio')
    result = job.append(app)
    if not result['OK']:
        print 'Could not append Vertexing'
        sys.exit(-1)

    job.setDestination('LCG.CERN.ch')
    job.setBannedSites(['LCG.IN2P3-CC.fr', 'LCG.RAL-LCG2.uk', 'LCG.DESY-HH.de', 'LCG.DESYZN.de', 'LCG.KEK.jp'])
    job.setCPUTime(50000)
    job.setSystemConfig('x86_64-slc5-gcc43-opt')
    # job.setOutputData(outputName, args.outputPath, args.SE)
    #job.setOutputData("", args.outputPath, args.SE)
    job.setOutputSandbox(['*.log', '*.xml'])
    job.setLogLevel("VERBOSE")
    job.submit(dirac, mode='agent')
    # job.submit()
