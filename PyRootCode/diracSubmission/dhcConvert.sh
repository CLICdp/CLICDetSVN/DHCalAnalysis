#!/bin/bash

for fileName in `ls Run*.bin`; do
    runNumber=${fileName:3:6}
done

bin/dhcLcio -f . -w . -r $runNumber
