from DIRAC.Core.Base import Script
import sys

runNumber = None
title = None
processAll = False

# Output data
outputSE = 'CERN-SRM'

Script.registerSwitch( 'r:', 'run=', 'Run number to be processed' )
Script.registerSwitch( 't:', 'title=', 'Job title' )
Script.registerSwitch( 'a', 'all', 'Process all runs' )
Script.setUsageMessage( sys.argv[0]+'-r <runNumber> -t <jobTitle>' )

Script.parseCommandLine()

from ILCDIRAC.Interfaces.API.NewInterface.Applications import GenericApplication, GetSRMFile
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from DhcalFiles import dhcalFiles

def submitConversion( runNumber = None, title = None ):
    
    dirac = DiracILC()
    
    job = UserJob()
    job.setName( 'DHCAL-%s-%s'%(title, runNumber) )
    job.setJobGroup( 'DHCAL-%s'%(title) )

    outputPath = 'calice/dhcal/lcio/%s'%(title)

    inputData = []
    for dhcalFile in dhcalFiles:
        if '%s'%(runNumber) in dhcalFile:
            #dhcalFile = dhcalFile.lstrip( 'LFN:' )
            inputData.append( dhcalFile )

    if len(inputData) == 0:
        print 'No DHCAL files found for Run%s'%(runNumber)
        return

    #print inputData
    inputSandbox = []
    inputSandbox.append( 'libDhcal.tgz' )
    inputSandbox.append( 'DHCalRunList.txt' )
    inputSandbox.extend( inputData )    
    #job.setInputSandbox( ['LFN:/ilc/user/c/cgrefe/calice/dhcal/libDhcal.tar.gz'] )
    job.setInputSandbox( inputSandbox )
    job.setOutputSandbox( ['*.log'] )

    job.setOutputData( ['dhcLcio-Run%s.slcio'%(runNumber)], outputPath, outputSE )

    dhcConverter = GenericApplication( {'Name':'DhcConverter'} )
    dhcConverter.setScript( 'dhcConvert.sh' )
    dhcConverter.setArguments( '-h' )
    #dhcConverter.setDependency( {'lcio':'v02-01-02'} )

    result = job.append( dhcConverter )
    if not result['OK']:
        print result['Message']
        sys.exit(2)

    job.dontPromptMe()
    job.submit( dirac )

if __name__ == "__main__":
    switches = Script.getUnprocessedSwitches()

    for switch in switches:
        opt = switch[0]
        arg = switch[1]
        if opt in ('r','run'):
            runNumber = int(arg)
        if opt in ('t','title'):
            title = arg
        if opt in ('a','all'):
            processAll = True

    if (not runNumber and not processAll) or not title:
        Script.showHelp()
        sys.exit(2)

    if processAll:
        for runNumber in xrange(660000, 660650, 1):
            submitConversion( runNumber, title )
    else:
        if runNumber < 100000 or runNumber > 999999:
            print 'Run number has to be a six digit integer'
            sys.exit(2)
        
        submitConversion( runNumber, title )

