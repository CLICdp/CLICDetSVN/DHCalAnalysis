#!/bin bash

DHCalMarlinLib=../../DHCalMarlin/lib        # lib path of the DHCalMarlin build
TarballName=caliceDhcal.tgz                 # name of the tar ball used by dirac
MarlinLibs="libCaliceDhcal.so"              # list of marlin libraries needed
DllLibs=""                                  # list of other dependencies that need to be shipped

# create the directory structure expected by ILCDIRAC
mkdir -p ./lib/marlin_dll       # place all dll containing marlin processors here (added to MARLIN_DLL)
mkdir -p ./lib/lddlib           # place additional dependencies here (added to LD_LIBRARY_PATH)

# copy the dhcal library and its dependencies
if [ -d $DHCalMarlinLib ]; then
    for MarlinLib in $MarlinLibs; do
        if [ -f $DHCalMarlinLib/$MarlinLib ]; then
            cp $DHCalMarlinLib/$MarlinLib ./lib/marlin_dll
        else
            echo "ERROR: Missing $DHCalMarlinLib/$MarlinLib"
        fi
    done
    for DllLib in $DllLibs; do
        if [ -f $DllLib ]; then
            cp $DllLib ./lib/lddlib
        else
            echo "ERROR: Missing $DHCalMarlinLib/$MarlinLib"
        fi
    done
    tar -czf $TarballName lib
    rm -r ./lib                 # clean up
    echo "Created $TarballName"
    tar -tzf $TarballName
else
    echo "ERROR: Need to build DHCalMarlin package first!"
fi
