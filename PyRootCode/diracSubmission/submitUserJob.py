##########################################################################
# Script to automatically submit W-DHCAL analysis Marlin jobs to ILCDIRAC
# Contact: christian.grefe@cern.ch
##########################################################################

from DIRAC.Core.Base import Script
import os, sys

# register all switches with the DIRAC command line parser
Script.registerSwitch( 'b', 'beamdata', 'Process beam data runs' )
Script.registerSwitch( 'i:', 'inputpath=', 'Grid path for the input SLCIO files' )
Script.registerSwitch( 'n', 'noise', 'Process noise runs' )
Script.registerSwitch( 'm:', 'momentum=', 'Process runs with given beam momentum' )
Script.registerSwitch( 'M:', 'marlin=', 'Sets the marlin version' )
Script.registerSwitch( 'p:', 'outputpath=', 'Storage path (relative to grid user directory)' )
Script.registerSwitch( 't:', 'title=', 'Job title' )
Script.registerSwitch( 'S', 'silent', 'No check before submission' )
Script.registerSwitch( 'x:', 'xml=', 'Marlin steering file template' )

# define a help message
Script.setUsageMessage( sys.argv[0]+' --inputpath=<gridPath> --outputpath=<gridPath> --title=<jobTitle> --xml=<marlinSteeringTemplate> (--all OR --beamdata OR --noise)\n\nThe marlin steering template has to contain at least one of the following keywords to automatically set the corresponding output file name for each job:\n\t__outputAida__\n\t__outputRoot__\n\t__outputSlcio__\n' )

# parse the command line. Has to happen before other DIRAC imports!
Script.parseCommandLine()

from ILCDIRAC.Interfaces.API.NewInterface.Applications import Marlin
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient
from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import *

fileCatalog = FileCatalogClient()
myDirac = DiracILC()

# Helper method for string replacement in a text file
def prepareFile( fileNameTemplate, fileNameOut, replacements ):
    f = open(fileNameTemplate)
    txt = f.read()
    counter = []
    for repkey,reprep in replacements:
        counter.append( ( repkey, reprep, txt.count(repkey) ) )
        txt = txt.replace( repkey, reprep )
    fOut = open( fileNameOut, "w" )
    fOut.write( txt )
    return counter

# class to hold all information necessary for the job submission
class dhcalUserJob():

    def __init__( self ):
        self.userLib = 'caliceDhcal.tgz'
        self.marlinSteeringTemplate = ''
        self.marlinSteeringFile = ''
        self.marlinVersion = '0116'
        self.cpuLimit = 100000
        self.title = ''
        self.systemConfig = 'x86_64-slc5-gcc43-opt'
        self.storagePath = ''
        self.storageElement = 'CERN-SRM'
        self.tempDirectory = 'temp'
        self.inputPath = ''
        self.foundFiles = []
        self.inputData = []
        self.outputData = []
        self.silent = False
    
    # helper method for the string replacement in the steering file
    def prepareSteeringFile( self, outputFileBase ):
        replacements = [
            ( '__outputSlcio__', outputFileBase+'.slcio' ),
            ( '__outputRoot__', outputFileBase+'.root' ),
            ( '__outputAida__', outputFileBase+'.aida' ),
        ]
        if not os.path.exists( self.tempDirectory ):
            os.makedirs( self.tempDirectory )
        self.marlinSteeringFile = '%s/%s.xml'%(self.tempDirectory, outputFileBase)
        result = prepareFile( self.marlinSteeringTemplate, self.marlinSteeringFile, replacements )
        for key, value, counter in result:
            if key.count('output') and counter:
                self.outputData.append( value )
    
    # the main submission method, all member values need to be set before
    def submit( self, runNumber ):
        # check sanity
        if not self.marlinSteeringTemplate:
            print 'ERROR: No marlin steering file defined!'
            sys.exit( 2 )
        if not self.inputPath:
            print 'ERROR: No input data defined!'
            sys.exit( 2 )
        
        path = self.inputPath.rstrip('/')
        
        if not self.foundFiles:
            result = fileCatalog.listDirectory( path )
            if not result['OK']:
                print result['Message']
                sys.exit( 2 )
            self.foundFiles = result['Value']['Successful'][path]['Files'].keys()
        self.inputData = []
        for lfn in self.foundFiles:
            if str(runNumber) in lfn:
                self.inputData.append( '%s'%(lfn) )
        
        if len( self.inputData ) == 0:
            print 'Found no input files for run %d' % ( runNumber )
            return
            
        jobTitle = '%s_%s'%( self.title, runNumber )
        
        self.outputData = []
        self.prepareSteeringFile( jobTitle )
        if not self.outputData:
            print 'ERROR: No output data found, check marlin steering template.'
            sys.exit( 2 )
        
        job = UserJob()
        
        inputSandbox = []
        inputSandbox.append( self.userLib )
        job.setInputData( self.inputData )
        job.setInputSandbox( inputSandbox )
        job.setOutputSandbox( ['*.log', '*.xml'] )
        job.setOutputData( self.outputData, self.storagePath, self.storageElement )
        
        job.setCPUTime( self.cpuLimit )
        job.setSystemConfig( self.systemConfig )
        job.setName( jobTitle )
        job.setJobGroup( self.title )
        
        marlin = Marlin()
        marlin.setVersion( self.marlinVersion )
        marlin.setSteeringFile( self.marlinSteeringFile )
        #marlin.setInputFile( self.inputData )
        result = job.append( marlin )
        if not result['OK']:
            print result['Message']
            sys.exit(2)
        
        if self.silent:
            job.dontPromptMe()
        else:
            self.silent = True
            print 'Submitting following job to ILCDIRAC:'
            print '\tMarlin steering template: %s'%(self.marlinSteeringTemplate)
            print '\tMarlin version: %s'%(self.marlinVersion)
            print '\tInput data directory: %s'%(path)
            print '\tOutput data directory: %s'%(self.storagePath)
            print '\tOutput storage element: %s'%(self.storageElement)
            result = raw_input( 'Continue? [y/n] ' )
            if result not in [ 'y', 'Y' ]:
                sys.exit( 2 )

        print 'Submitting %s'%(jobTitle)
        job.submit( myDirac )
        
if __name__ == "__main__":
    switches = Script.getUnprocessedSwitches()
    job = dhcalUserJob()
    
    processAll = False
    processBeamData = False;
    processNoise = False
    momentum = None
    
    for switch in switches:
        opt = switch[0]
        arg = switch[1]
        if opt in ('i','inputpath'):
            job.inputPath = arg
        if opt in ('p','outputpath'):
            job.storagePath = arg
        if opt in ('t','title'):
            job.title = arg
        if opt in ('m','momentum'):
            momentum = int(arg)
        if opt in ('M','marlin'):
            job.marlinVersion = arg
        if opt in ('a','all'):
            processAll = True
        if opt in ('n','noise'):
            processNoise = True
        if opt in ('b','beamdata'):
            processBeamData = True
        if opt in ('S','silent'):
            job.silent = True
        if opt in ('x','xml'):
            job.marlinSteeringTemplate = arg
    
    if not job.marlinSteeringTemplate:
        print 'ERROR: No marlin steering template provided\n'
        Script.showHelp()
        sys.exit( 2 )
    if not job.inputPath:
        print 'ERROR: No input LFN path provided\n'
        Script.showHelp()
        sys.exit( 2 )
    if not job.storagePath:
        print 'ERROR: No output path provided\n'
        Script.showHelp()
        sys.exit( 2 )
    if not job.title:
        print 'ERROR: No job title provided\n'
        Script.showHelp()
        sys.exit( 2 )
    
    myRunList = RunList()
    #runList = RunList.readTextFile( '/home/cgrefe/clicdet/DHCalAnalysis/DHCalRunList.txt' )
    runList = RunList.readTextFile()
    if processAll:
        myRunList.extend( runList )
    else:
        if processBeamData:
            myRunList.extend( runList.getRunsByRunType( RunType.DATA ) )
        if processNoise:
            myRunList.extend( runList.getRunsByRunType( RunType.NOISE ) )
    if momentum:
        myRunList = myRunList.getRunsByMomentum( momentum )
    
    if not myRunList:
        print 'WARNING: No runs found to process'
        sys.exit( 2 )
    print 'Found %d runs to process'%(len(myRunList))
    for run in myRunList:
        job.submit( run.runNumber )
    
