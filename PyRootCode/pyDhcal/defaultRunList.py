from runList import *

runList = RunList()

def create():
  runList.clear()
  runList.addRun( Run( 660167, 10, Polarity.negative, False, 30.4, 963.6  ) )
  runList.addRun( Run( 660166, 10, Polarity.negative, False, 30.4, 964.6  ) )
  runList.addRun( Run( 660165,  9, Polarity.negative, False, 30.5, 966.4  ) )
  runList.addRun( Run( 660164,  9, Polarity.negative, False, 30.5, 966.45 ) )
  runList.addRun( Run( 660163,  9, Polarity.negative, False, 30.5, 966.4  ) )
  runList.addRun( Run( 660162,  9, Polarity.negative, False, 30.5, 966.4  ) )

  return runList
