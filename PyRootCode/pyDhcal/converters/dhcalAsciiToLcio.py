#!/usr/bin/env python

'''
Created on July 21, 2014

Example for converting ASCII event format into LCIO

@author: <a href="mailto:christian.grefe@cern.ch">Christian Grefe</a>
'''
from argparse import ArgumentParser
from pyDhcal import dhcalMapping
from pyDhcal.dhcalDefinitions import * 
from array import array
import os, sys

MASS_ELECTRON = 0.000510998928 # GeV
MASS_MUON = 0.1056583715 # GeV
MASS_PION_CHARGED = 0.13957018 # GeV
MASS_PROTON = 0.938272046 # GeV
MASS_NEUTRON = 0.939565379 # GeV

def createMCParticle( pdgid, momentum, origin ):
    from pyLCIO import IMPL
    mcParticle = IMPL.MCParticleImpl()
    mcParticle.setVertexVec( origin )
    mcParticle.setTime( 0. )
    mcParticle.setMomentumVec( momentum )
    mcParticle.setPDG( pdgid )
    mcParticle.setGeneratorStatus( 1 )
    abspdgid = abs(pdgid)
    if abspdgid in [13]:
        # muon
        mcParticle.setCharge( -float(pdgid / abspdgid) )
        mcParticle.setMass( MASS_MUON )
    elif abspdgid in [22]:
        # photon
        mcParticle.setCharge( 0. )
        mcParticle.setMass( 0. )
    elif abspdgid in [22]:
        # electron
        mcParticle.setCharge( -float(pdgid / abspdgid) )
        mcParticle.setMass( MASS_ELECTRON )
    elif abspdgid in [211]:
        # charged pion
        mcParticle.setCharge( float(pdgid / abspdgid) )
        mcParticle.setMass( MASS_PION_CHARGED )
    elif abspdgid in [2212]:
        # proton
        mcParticle.setCharge( float(pdgid / abspdgid) )
        mcParticle.setMass( MASS_PROTON )
    elif abspdgid in [2112]:
        # neutron
        mcParticle.setCharge( 0. )
        mcParticle.setMass( MASS_NEUTRON )
    return mcParticle

class AsciiToLcioConverter():
    """ Converts DHCAL ASCII files to LCIO format
    """
    
    inputFileName = None
    inputFile = None
    outputFileName = None
    runNumber = 0
    detectorName = 'DHCAL'
    lcioWriter = None
    description = ''
    eventNumber = -1
    cellIdEncoding = 'i:8,j:8,layer:8'
    tailCatcherStart = 39
    maxEvents = -1
    eventsProcessed = 0
    mc_p = 10.
    mc_pdg = 13
    
    def __init__( self ):
        """ Constructor
        """
        
        # nothing to do
        pass
    
    def convertSim( self ):
        """ Main method to run the conversion
        """
        
        from pyLCIO import EVENT, IMPL, IOIMPL, UTIL
        self.initialize()
        event = None
        simHits = None
        mcParticles = None
        mcParticle = None
        idEncoder = None
        cellIdToHitMap = None
        
        for line in self.inputFile:
            items = line.split()
            
            # what are the input variables?
            self.eventNumber = self.eventsProcessed
            layerID = int( items[0] )
            localX = float( items[1] )
            localY = float( items[2] )
            dEdx = float( items[3] )
            time = float( items[4] )
            localZ = float( items[5] )
            moduleID = int( items[6] )           
            
            # if all variables are -1 begin a new event
            if all(v == -1 for v in (layerID, localX, localY, dEdx, time, localZ, moduleID)):
                from ROOT import TVector3
                # write the previous event if we already have one
                if event:
                    self.lcioWriter.writeEvent( event )
                    self.eventsProcessed += 1
                    if self.eventsProcessed == self.maxEvents:
                        break
                    if self.eventsProcessed % 100 == 0:
                        print 'Processed %d events' % self.eventsProcessed
                event = self.newEvent()
                
                # create the MC particle collection
                mcParticles = IMPL.LCCollectionVec(EVENT.LCIO.MCPARTICLE);
                
                # create a very simple MC particle
                mcParticle = createMCParticle( self.mc_pdg, TVector3(0., 0., self.mc_p) , TVector3(0., 0., 0.) )
                mcParticles.addElement( mcParticle )
                
                # create main hit collection
                simHits = IMPL.LCCollectionVec(EVENT.LCIO.SIMCALORIMETERHIT);
                # store cell position with hits
                simHits.setFlag(UTIL.set_bit(simHits.getFlag(), EVENT.LCIO.CHBIT_LONG));
                # store step positions with hits
                simHits.setFlag(UTIL.set_bit(simHits.getFlag(), EVENT.LCIO.CHBIT_STEP));
                # store time with hits
                #simHits.setFlag(UTIL.set_bit(simHits.getFlag(), EVENT.LCIO.RCHBIT_TIME));
                # create an ID encoder
                idEncoder = UTIL.CellIDEncoder( IMPL.SimCalorimeterHitImpl )( self.cellIdEncoding, simHits )
                
                # create a map to store cell ID to hit relations
                cellIdToHitMap = {}
                
                # add all collections
                event.addCollection( mcParticles, 'MCParticles' )
                event.addCollection( simHits, 'DhcSimHits' )
            
            # we do have a proper hit, add it to the event
            else:
                from ROOT import TVector3
                layer = dhcalMapping.layerMap[layerID]
                if moduleID == 0:
                    module = layer.modules[ModulePosition.BOTTOM]
                elif moduleID == 1:
                    module = layer.modules[ModulePosition.MIDDLE]
                elif moduleID == 2:
                    module = layer.modules[ModulePosition.TOP]
                else:
                    raise Exception( 'Unknown module index: "%s"' % moduleID )
                
                # x, y and z are local coordinates in the module => correct to global position
                stepPosition = layer.position + module.position + TVector3( localX, localY, localZ )
                i, j, layerID = dhcalMapping.getIndicesFromPosition( stepPosition )
                
                idEncoder.reset()
                idEncoder['layer'] = layerID
                idEncoder['i'] = i
                idEncoder['j'] = j                
                cellID = idEncoder.getValue()
                
                hit = None
                if cellIdToHitMap.has_key( cellID ):
                    hit = cellIdToHitMap[cellID]
                else:
                    hit = IMPL.SimCalorimeterHitImpl()
                    hit.setPositionVec( dhcalMapping.getPositionFromIndices( i, j, layerID ) )
                    idEncoder.setCellID( hit )
                    simHits.addElement( hit )
                    cellIdToHitMap[cellID] = hit
                
                hit.addMCParticleContribution( mcParticle, dEdx, time, self.mc_pdg, array( 'f', [stepPosition.x(), stepPosition.y(), stepPosition.z()] ) )
                
        self.finalize()
    
    def convertDigi( self ):
        """ Main method to run the conversion
        """
        
        from pyLCIO import EVENT, IMPL, IOIMPL, UTIL
        self.initialize()
        event = None
        allHits = None
        mainStackHits = None
        tailCatcherHits = None
        idEncoder = None
        
        for line in self.inputFile:
            items = line.split()
            
            # what are the input variables?
            self.eventNumber = int( items[0] )
            i = int( items[1] )
            j = int( items[2] )
            layer = int( items[3] )
            
            # if all variables are -1 begin a new event
            if all(v == -1 for v in (i, j, layer)):
                # write the previous event if we already have one
                if event:
                    self.lcioWriter.writeEvent( event )
                    self.eventsProcessed += 1
                    if self.eventsProcessed == self.maxEvents:
                        break
                    if self.eventsProcessed % 100 == 0:
                        print 'Processed %d events' % self.eventsProcessed
                event = self.newEvent()
                
                # create main hit collection
                allHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
                # store position with hits
                allHits.setFlag(UTIL.set_bit(allHits.getFlag(), EVENT.LCIO.RCHBIT_LONG));
                # store time with hits
                allHits.setFlag(UTIL.set_bit(allHits.getFlag(), EVENT.LCIO.RCHBIT_TIME));
                # create an ID encoder
                idEncoder = UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, allHits )
                
                # create sub collection for main stack hits
                mainStackHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
                mainStackHits.setSubset( True )
                # use the constructor to set the encoding string
                UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, mainStackHits )
                
                # create sub collection for tail catcher hits
                tailCatcherHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
                tailCatcherHits.setSubset( True )
                # use the constructor to set the encoding string
                UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, tailCatcherHits )
                
                # add all collections
                event.addCollection( allHits, 'DhcHits' )
                event.addCollection( mainStackHits, 'DhcMainStackHits' )
                event.addCollection( tailCatcherHits, 'DhcTailCatcherHits' )
            
            # we do have a proper hit, add it to the event
            else:
                hit = IMPL.CalorimeterHitImpl()
                hitPosition = dhcalMapping.getPositionFromIndices( i, j, layer )
                hit.setPositionVec( hitPosition )
                hit.setTime( 0. )
                hit.setEnergy( 1. )
                
                idEncoder.reset()
                idEncoder['layer'] = layer
                idEncoder['i'] = i
                idEncoder['j'] = j
                idEncoder.setCellID( hit )
                
                allHits.addElement( hit )
                if layer < self.tailCatcherStart:
                    mainStackHits.addElement( hit )
                else:
                    tailCatcherHits.addElement( hit )
        
        self.finalize()
        
    def newEvent( self, timeStamp = 0 ):
        """ Helper method to create a new event
        """
        
        from pyLCIO import IMPL
        # create a new event object
        event = IMPL.LCEventImpl()
        event.setEventNumber( self.eventNumber )
        event.setRunNumber( self.runNumber )
        event.setDetectorName( self.detectorName )
        event.setTimeStamp( timeStamp )
        return event
    
    def initialize( self ):
        """ Helper method to open the input and output files
        """
        
        from pyLCIO import EVENT, IMPL, IOIMPL
        # check input and output file names
        if not os.path.isfile( self.inputFileName ):
            raise IOError( 'Input file "%s" does not exist!' % self.inputFileName )
        if os.path.exists( self.outputFileName ):
            raise IOError( 'Output file "%s" already exists!' % self.outputFileName )
        
        # open the input file
        self.inputFile = open( self.inputFileName, 'r' )
        
        print 'Reading events from %s' % self.inputFileName
        
        # create a writer and open the output file
        self.lcioWriter = IOIMPL.LCFactory.getInstance().createLCWriter()
        self.lcioWriter.open( self.outputFileName, EVENT.LCIO.WRITE_NEW )
        
        # create and write the run header
        runHeader = IMPL.LCRunHeaderImpl()
        runHeader.setRunNumber( self.runNumber )
        runHeader.setDetectorName( self.detectorName )
        runHeader.setDescription( self.description )
        self.lcioWriter.writeRunHeader( runHeader )
    
    def finalize( self ):
        """ Helper method to close the input and output files
        """
        
        # close all files
        self.inputFile.close()
        self.lcioWriter.flush()
        self.lcioWriter.close()
        
        # Summary printout
        print 'Written %d events to %s' % ( self.eventsProcessed, self.outputFileName )

if __name__ == "__main__":
    """ Main program
    """
    
    # set up and parse command line options
    parser = ArgumentParser()
    parser.add_argument( '-i', '--input', help='Name of the input ASCII file', required=True )
    parser.add_argument( '-o', '--output', help='Name of the output LCIO file', required=True )
    parser.add_argument( '-r', '--run', type=int, default=0, help='Run number set in LCIO output' )
    parser.add_argument( '-n', '--detectorName', default='DHCAL', help='Name of the detector set in LCIO output' )
    parser.add_argument( '-d', '--description', default='', help='Description added to the LCIO output' )
    parser.add_argument( '-c', '--tailCatcherStart', default=39, help='First layer number of the tail catcher layer' )
    parser.add_argument( '-m', '--maxEvents', type=int, default=-1, help='Maximum number of events to process' )
    parser.add_argument( '-t', '--type', default='sim', choices=['sim','digi'], help='Choose if input ASCII format is "sim" (7 columns) or "digi" (4 columns)' )
    parser.add_argument( '--mc_p', type=float, default=10., help='Momentum of the Monte Carlo particles that are written (only used in sim mode)' )
    parser.add_argument( '--mc_pdg', type=int, default=13, help='PDG ID of the Monte Carlo particles that are written (only used in sim mode)' )
    results = parser.parse_args()
    
    # set the parameters of the converter
    converter = AsciiToLcioConverter()
    converter.inputFileName = results.input
    converter.outputFileName = results.output
    converter.runNumber = results.run
    converter.detectorName = results.detectorName
    converter.description = results.description
    converter.tailCatcherStart = results.tailCatcherStart
    converter.maxEvents = results.maxEvents
    converter.mc_pdg = results.mc_pdg
    converter.mc_p = results.mc_p
    
    # convert the file
    try:
        if results.type.lower() in ['sim']:
            converter.convertSim()
        elif results.type.lower() in ['digi']:
            converter.convertDigi()
        else:
            raise Exception( 'Unknown input type %s' % results.type )
    except Exception as e:
        print 'Error:', e
        sys.exit( 2 )
    