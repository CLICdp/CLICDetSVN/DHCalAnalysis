#!/usr/bin/env python

'''
Created on October 27, 2014

Example for converting ROOT event format into LCIO

@author: <a href="mailto:christian.grefe@cern.ch">Christian Grefe</a>
'''

from argparse import ArgumentParser
from pyDhcal import dhcalMapping
import os, sys

class RootToLcioConverter():
    """ Converts DHCAL ROOT files to LCIO format
    """
    
    rootLibrary = ''
    inputFileName = None
    inputFile = None
    outputFileName = None
    runNumber = 0
    treeName = 'DHCAL'
    tree = None
    detectorName = 'DHCAL'
    lcioWriter = None
    description = ''
    eventNumber = 0
    cellIdEncoding = 'i:8,j:8,layer:8'
    tailCatcherStart = 39
    maxEvents = -1
    eventsProcessed = 0
    ignoreTCMT = False
    
    def __init__( self ):
        """ Constructor
        """
        
        # nothing to do
        pass
    
    def convert( self ):
        """ Main method to run the conversion
        """
        
        from pyLCIO import EVENT, IMPL, IOIMPL, UTIL
        self.initialize()
        event = None
        allHits = None
        mainStackHits = None
        tailCatcherHits = None
        idEncoder = None
        
        for entry in self.tree:
            dhcalEvent = self.tree.DHCALEvent
            if not dhcalEvent:
                continue
            if self.runNumber != dhcalEvent.GetRunNo():
                runHeader = self.newRunHeader( dhcalEvent )
                self.lcioWriter.writeRunHeader( runHeader )
            event = self.newEvent( dhcalEvent )
            self.lcioWriter.writeEvent( event )
            self.eventNumber = self.eventsProcessed
            self.eventsProcessed += 1
            if self.eventsProcessed == self.maxEvents:
                break
            if self.eventsProcessed % 100 == 0:
                print 'Processed %d events' % self.eventsProcessed
        
        self.finalize()
    
    def newRpcHit( self, rpcHit, idEncoder ):
        """ Helper method to create a new hit object
        """
        
        from pyLCIO import IMPL
        from ROOT import TVector3
        
        layer = rpcHit.z
        # need to convert cm into mm
        position = dhcalMapping.layerMap[layer].position + TVector3( 10.*rpcHit.x, 10.*rpcHit.y, 0. )
        i = int(position.x() / 10.)
        j = int(position.y() / 10.)
        # accept hits in cell 95 that are exactly on boundary
        if (position.x() == 960.):
            i -= 1
        if (position.y() == 960.):
            j -= 1
        
        hit = IMPL.CalorimeterHitImpl()
        hit.setPositionVec( position )
        hit.setTime( rpcHit.t )
        hit.setEnergy( 1. )
        
        idEncoder.reset()
        idEncoder['layer'] = layer
        idEncoder['i'] = i
        idEncoder['j'] = j
        idEncoder.setCellID( hit )
        
        return hit
    
    def newTcmtHit( self, tcmtHit, idEncoder ):
        """ Helper method to create a new hit object
        """
        
        from pyLCIO import IMPL
        from ROOT import TVector3
        
        # tail catcher consists of 20 x 50mm strips per layer and has only one coordinate
        # tail catcher layer index starts at 0 in ROOT data, correcting to get somewhat reasonable positions
        layer = tcmtHit.z
        # start one layer behind main stack
        z = dhcalMapping.layerMap[39].position.z()
        # first 8 layers are 20mm + 5mm, second 8 are 100mm + 5mm
        if layer < 8:
            z += layer * 25.
        else:
            z += 8 * 25. + layer * 105.
        if tcmtHit.x == -1:
            x = 0.
            i = 255
        else:
            i = tcmtHit.x
            x = i * 5 - 475.
        if tcmtHit.y == -1:
            y = 0.
            j = 255
        else:
            j = tcmtHit.y
            y = j * 5 - 475.
        # need to convert cm into mm
        position = TVector3( x, y, z )
        
        hit = IMPL.CalorimeterHitImpl()
        hit.setPositionVec( position )
        hit.setTime( 0. )
        hit.setEnergy( tcmtHit.E )
        
        idEncoder.reset()
        idEncoder['layer'] = layer
        idEncoder['i'] = i
        idEncoder['j'] = j
        idEncoder.setCellID( hit )
        
        return hit
    
    def newEvent( self, dhcalEvent ):
        """ Helper method to create a new event
        """
        
        from pyLCIO import EVENT, IMPL, UTIL
        # create a new event object
        event = IMPL.LCEventImpl()
        event.setEventNumber( self.eventNumber )
        event.setRunNumber( dhcalEvent.GetRunNo() )
        event.setDetectorName( self.detectorName )
        event.setTimeStamp( dhcalEvent.GetEventTime() )
        
        cerenkovBitA = 0
        cerenkovBitB = 0
        muonTag = 0
        if dhcalEvent.GetCerenkov() in [1, 3, 5, 7]:
            cerenkovBitA = 1
        if dhcalEvent.GetCerenkov() in [2, 3, 6, 7]:
            cerenkovBitB = 1
        if dhcalEvent.GetCerenkov() in [4, 5, 6, 7]:
            muonTag = 1
        event.parameters().setValue( "CerenkovA", cerenkovBitA );
        event.parameters().setValue( "CerenkovB", cerenkovBitB );
        event.parameters().setValue( "MuonTag", muonTag );
        
        # create main hit collection
        allHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
        # store position with hits
        allHits.setFlag(UTIL.set_bit(allHits.getFlag(), EVENT.LCIO.RCHBIT_LONG));
        # store time with hits
        allHits.setFlag(UTIL.set_bit(allHits.getFlag(), EVENT.LCIO.RCHBIT_TIME));
        # create an ID encoder
        idEncoder = UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, allHits )
                
        # create sub collection for main stack hits
        mainStackHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
        mainStackHits.setSubset( True )
        # use the constructor to set the encoding string
        UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, mainStackHits )
                
        # create sub collection for tail catcher hits
        tailCatcherHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
        tailCatcherHits.setSubset( True )
        # use the constructor to set the encoding string
        UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, tailCatcherHits )
        
        # create TCMT hit collection
        tcmtHits = IMPL.LCCollectionVec(EVENT.LCIO.CALORIMETERHIT);
        # store position with hits
        tcmtHits.setFlag(UTIL.set_bit(tcmtHits.getFlag(), EVENT.LCIO.RCHBIT_LONG));
        # store time with hits
        tcmtHits.setFlag(UTIL.set_bit(tcmtHits.getFlag(), EVENT.LCIO.RCHBIT_TIME));
        # use the constructor to set the encoding string
        UTIL.CellIDEncoder( IMPL.CalorimeterHitImpl )( self.cellIdEncoding, tcmtHits )
                
        # add all collections
        event.addCollection( allHits, 'DhcHits' )
        event.addCollection( mainStackHits, 'DhcMainStackHits' )
        event.addCollection( tailCatcherHits, 'DhcTailCatcherHits' )
        
        for rpcHit in dhcalEvent.GetRPCHits():
            hit = self.newRpcHit( rpcHit, idEncoder )
            allHits.addElement( hit )
            if rpcHit.z < self.tailCatcherStart:
                mainStackHits.addElement( hit )
            else:
                tailCatcherHits.addElement( hit )
        
        if not self.ignoreTCMT:
            event.addCollection( tcmtHits, 'TCMTHits' )
            for tcmtHit in dhcalEvent.GetTCMTHits():
                hit = self.newTcmtHit( tcmtHit, idEncoder )
                tcmtHits.addElement( hit )    
        
        return event
    
    def newRunHeader( self, dhcalEvent ):
        """ Helper method to create a new event
        """
        
        from pyLCIO import IMPL
        runHeader = IMPL.LCRunHeaderImpl()
        runHeader.setRunNumber( dhcalEvent.GetRunNo() )
        runHeader.setDetectorName( self.detectorName )
        runHeader.setDescription( self.description )
        
        runHeader.parameters().setValue( "TimeStamp", 0. );
        runHeader.parameters().setValue( "RunType", '' );
        runHeader.parameters().setValue( "BeamMomentum", 0. );
        runHeader.parameters().setValue( "Temperature", 0. );
        runHeader.parameters().setValue( "Pressure", 0. );
        runHeader.parameters().setValue( "EventsPerSpill", 0. );
        runHeader.parameters().setValue( "CerenkovA", 0. );
        runHeader.parameters().setValue( "CerenkovB", 0. );
        runHeader.parameters().setValue( "Trigger", '' );
        runHeader.parameters().setValue( "IsBad", False );
        runHeader.parameters().setValue( "Comment", '' );
        runHeader.parameters().setValue( "Absorber", '' );
        runHeader.parameters().setValue( "Target", '' );
        runHeader.parameters().setValue( "BeamFile", '' );
        
        self.runNumber = dhcalEvent.GetRunNo()
        
        return runHeader
        
    def initialize( self ):
        """ Helper method to open the input and output files
        """
        
        from ROOT import gSystem, TFile
        
        # load the ROOT library
        gSystem.Load( self.rootLibrary )
        
        from pyLCIO import EVENT, IOIMPL
        # check input and output file names
        if not os.path.isfile( self.inputFileName ):
            raise IOError( 'Input file "%s" does not exist!' % self.inputFileName )
        if os.path.exists( self.outputFileName ):
            raise IOError( 'Output file "%s" already exists!' % self.outputFileName )
        
        # open the input file and get the tree
        self.inputFile = TFile.Open( self.inputFileName, 'read' )
        self.tree = self.inputFile.Get( self.treeName )
        
        print 'Reading events from %s' % self.inputFileName
                
        # create a writer and open the output file
        self.lcioWriter = IOIMPL.LCFactory.getInstance().createLCWriter()
        self.lcioWriter.open( self.outputFileName, EVENT.LCIO.WRITE_NEW )
    
    def finalize( self ):
        """ Helper method to close the input and output files
        """
        
        # close all files
        self.inputFile.Close()
        self.lcioWriter.flush()
        self.lcioWriter.close()
        
        # Summary printout
        print 'Written %d events to %s' % ( self.eventsProcessed, self.outputFileName )

if __name__ == "__main__":
    """ Main program
    """
    
    # set up and parse command line options
    parser = ArgumentParser()
    parser.add_argument( '-i', '--input', help='Name of the input ROOT file', required=True )
    parser.add_argument( '-o', '--output', help='Name of the output LCIO file', required=True )
    parser.add_argument( '-l', '--library', help='The path to the ROOT library', required=True )
    parser.add_argument( '-n', '--detectorName', default='DHCAL', help='Name of the detector set in LCIO output' )
    parser.add_argument( '-d', '--description', default='', help='Description added to the LCIO output' )
    parser.add_argument( '-m', '--maxEvents', type=int, default=-1, help='Maximum number of events to process' )
    parser.add_argument( '-t', '--tailCatcherStart', default=39, help='First layer number of the tail catcher layer' )
    parser.add_argument( '--ignoreTCMT', action='store_true', help='Decide if TCMT information schould be ignored' )
    results = parser.parse_args()
    
    # set the parameters of the converter
    converter = RootToLcioConverter()
    converter.rootLibrary = results.library
    converter.inputFileName = results.input
    converter.outputFileName = results.output
    converter.detectorName = results.detectorName
    converter.description = results.description
    converter.maxEvents = results.maxEvents
    converter.ignoreTCMT = results.ignoreTCMT
    
    # convert the file
    try:
        converter.convert()
    except Exception as e:
        print 'Error:', e
        sys.exit( 2 )
 
