from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('../dhcCode/DhcTree_cxx.so')
ROOT.gStyle.SetOptFit(1111)
storagePath = '/work0/jfstrube/DhcalMay2012_rootTrees'
runNumber = 660268
fileName = '%s/dhcTree-Run%s.root' % (storagePath, runNumber)
if len(sys.argv) > 1:
    fileName = sys.argv[1]
# tree = ROOT.TChain('DhcTree')
# tree.Add(fileName)
# tree.Draw('EventBranch.nHits : EventBranch.nHitLayers >> myhist(70,0,70,300,0,300)', 'EventBranch.fTriggerBits.fAllBits == 0', 'colz')

infile = ROOT.TFile.Open(fileName)
tree = infile.Get('DhcTree')

timeHist = ROOT.TH1F('timeHist', 'time between events', 100, 0, 50)
eventHist = ROOT.TH1F('eventHist', 'nEvents per spill', 100, 300, 500)
nHitsInFirstHundred = ROOT.TH1F('nHitsInFirstHundred', 'nHits', 100, 0, 300)
nHitsInSecondHundred = ROOT.TH1F('nHitsInSecondHundred', 'nHits in last hundred events / spill', 100, 0, 300)
nHitsInThirdHundred = ROOT.TH1F('nHitsInThirdHundred', 'nHits per event in third hundred events / spill', 100, 0, 300)

nHitsInLastHundred = ROOT.TH1F('nHitsInLastHundred', 'nHits in first hundred events / spill', 100, 0, 300)
nEvts = tree.GetEntries()
lastTime = -1
nEvents = 0
firstHundred_nHits = []
lastHundred_nHits = []
nHits = []
for iEv in xrange(nEvts):
	tree.GetEvent(iEv)
	nEvents += 1
	time = tree.EventBranch.GetHeader().GetEvtTime().AsDouble()
	nHits.append(tree.EventBranch.GetNhits())
	nHitsInFirstHundred.Fill(tree.EventBranch.GetNhits())
	if lastTime < 0:
		lastTime = time
		continue
	diff = abs(time - lastTime)
	if diff > 1.:
		# nHitsInFirstHundred.Fill(sum(nHits[:10])/10)
		# print sum(nHits[:100])/100, 
		# print sum(nHits[-100:])/100
		for h in nHits[:100]:
			nHitsInLastHundred.Fill(h)
		for h in nHits[-100:]:
			nHitsInSecondHundred.Fill(h)
		eventHist.Fill(nEvents)
		timeHist.Fill(diff)
		nHits = []
		nEvents = 0
	lastTime = time

c1 = ROOT.TCanvas()
timeHist.Draw()
c2 = ROOT.TCanvas()
eventHist.Draw()
c3 = ROOT.TCanvas()
f1 = ROOT.TF1('f1','gaus', 80, 130)
nHitsInFirstHundred.SetLineColor(2)
nHitsInFirstHundred.SetLineWidth(2)
nHitsInFirstHundred.Draw()
nHitsInFirstHundred.Fit(f1, 'R')
nHitsInLastHundred.SetLineColor(3)
nHitsInLastHundred.SetLineWidth(2)
c4 = ROOT.TCanvas()
nHitsInLastHundred.Draw()
nHitsInLastHundred.Fit(f1, 'R')
c5 = ROOT.TCanvas()
nHitsInSecondHundred.SetLineWidth(2)
nHitsInSecondHundred.Draw()
nHitsInSecondHundred.Fit(f1, 'R')
