from runList import RunList
import ROOT

# path for the root trees
rootTreePath = "/afs/cern.ch/work/j/jfstrube/public/DHCAL/DhcalMay2012_rootTrees/"

# load the root library
ROOT.gSystem.Load("../dhcCode/libDhcTree.so")

# open root file from a run
rootFile = ROOT.TFile().Open(rootTreePath + "dhcTree-Run660408.root")

# get the tree from the rootFile
tree = rootFile.Get("DhcTree")

# go to one of the events
tree.GetEntry(25)

# get the current event
event = tree.EventBranch

# run the clustering
event.findClusters()

# get the cluster map of layer position to list of clusters in that layer
clusterMap = event.get2DClusters()

# loop over the map and do something with the entries
print "Printing clusters in event %d"%(event.GetHeader().GetEvtNum())
for entry in clusterMap:
    zPosition = entry.first
    clusterList = entry.second
    print "\tFound %d clusters at z = %2.1fmm"%(len(clusterList), zPosition)
    for cluster in clusterList:
        print "\t\tCluster of %d hits at position (%2.1fmm, %2.1fmm)"%(cluster.getSize(), cluster.getX(), cluster.getY())
