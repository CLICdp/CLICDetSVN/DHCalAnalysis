from runList import RunList
from dhcalDefinitions import *

runList = RunList.readTextFile()

runList = runList.getRunsByMomentum( [5, 7, 10] )
runList = runList.getRunsByPolarity( Polarity.NEGATIVE )
runList = runList.getRunsByTrigger( Trigger.TEN_TEN )

print 'Some low energy runs:'
for run in runList:
    print '\t', run, 'using trigger:', run.trigger

runList = RunList.readTextFile()
runList = runList.getRunsByMomentum( 180 )
runList = runList.getRunsByTrigger( Trigger.THIRTY_THIRTY )
print 'Some high energy muon runs:'
for run in runList:
    print '\t', run, 'using trigger:', run.trigger
