from ROOT import TVector3
from dhcalDefinitions import ModulePosition, FrontEndBoardPosition
from exceptions import AttributeError

class Element:
    def __init__( self, uMin=0, uMax=0, vMin=0, vMax=0, cellSizeU=10., cellSizeV=10., position=TVector3(), mother=None ):
        self.uMin = uMin
        self.uMax = uMax
        self.vMin = vMin
        self.vMax = vMax
        self.cellSizeU = cellSizeU
        self.cellSizeV = cellSizeV
        self.nCellsU = uMax - uMin + 1
        self.nCellsV = vMax - vMin + 1
        self.position = position
        self.xMin = self.position.x() - ( self.nCellsU * self.cellSizeU ) / 2.
        self.xMax = self.position.x() + ( self.nCellsU * self.cellSizeU ) / 2.
        self.yMin = self.position.y() - ( self.nCellsV * self.cellSizeV ) / 2.
        self.yMax = self.position.y() + ( self.nCellsV * self.cellSizeV ) / 2.
        self.mother = mother
        
class Module( Element ):
    def __init__( self, uMin=0, uMax=0, vMin=0, vMax=0, cellSizeU=10., cellSizeV=10., position=TVector3(), ID=ModulePosition.UNKNOWN, mother=None ):
        Element.__init__( self, uMin, uMax, vMin, vMax, cellSizeU, cellSizeV, position, mother )
        self.frontEndBoards = {}
        self.id = ID
        
class FrontEndBoard( Element ):
    def __init__( self, uMin=0, uMax=0, vMin=0, vMax=0, cellSizeU=10., cellSizeV=10., position=TVector3(), ID=FrontEndBoardPosition.UNKNOWN, mother=None ):
        Element.__init__( self, uMin, uMax, vMin, vMax, cellSizeU, cellSizeV, position, mother )
        self.id = ID

class Layer():
    def __init__( self, position=TVector3(), ID=0 ):
        self.modules = {}
        self.position = position
        self.id = ID

nLayers = 54
nCellsU = 96
nCellsV = 96
cellSizeU = 10.
cellSizeV = 10.
UID = 'i'
VID = 'j'
LayerID = 'layer'
encoding = 'i:8,j:8,layer:8'

layerMap = {}
for layerID in xrange( nLayers ):
    layer = Layer( ID=layerID )
    layer.modules[ModulePosition.TOP] = Module( 0, 95, 64, 95, cellSizeU, cellSizeV, TVector3( 0., 325.3, 0. ), ModulePosition.TOP, layer )
    layer.modules[ModulePosition.MIDDLE] = Module( 0, 95, 32, 63, cellSizeU, cellSizeV, TVector3( 0., 0., 0. ), ModulePosition.MIDDLE, layer )
    layer.modules[ModulePosition.BOTTOM] = Module( 0, 95, 0, 31, cellSizeU, cellSizeV, TVector3( 0., -325.3, 0. ), ModulePosition.BOTTOM, layer )
    for moduleID, module in layer.modules.items():
        module.frontEndBoards[FrontEndBoardPosition.LEFT] = FrontEndBoard( 0, 47, module.vMin, module.vMax, cellSizeU, cellSizeV, TVector3( -240., 0., 0. ), FrontEndBoardPosition.LEFT, module );
        module.frontEndBoards[FrontEndBoardPosition.RIGHT] = FrontEndBoard( 48, 95, module.vMin, module.vMax, cellSizeU, cellSizeV, TVector3( 240., 0., 0. ), FrontEndBoardPosition.RIGHT, module );
    layerMap[layerID] = layer
    
layerMap[0].position = TVector3( 0., 0., 0. )
layerMap[1].position = TVector3( 0., 0., 27. )
layerMap[2].position = TVector3( 0., 0., 54. )
layerMap[3].position = TVector3( 0., 0., 81. )
layerMap[4].position = TVector3( 0., 0., 108. )
layerMap[5].position = TVector3( 0., 0., 135. )
layerMap[6].position = TVector3( 0., 0., 162. )
layerMap[7].position = TVector3( 0., 0., 189. )
layerMap[8].position = TVector3( 0., 0., 216. )
layerMap[9].position = TVector3( 0., 0., 243. )
layerMap[10].position = TVector3( 0., 0., 270. )
layerMap[11].position = TVector3( 0., 0., 297. )
layerMap[12].position = TVector3( 0., 0., 324. )
layerMap[13].position = TVector3( 0., 0., 351. )
layerMap[14].position = TVector3( 0., 0., 378. )
layerMap[15].position = TVector3( 0., 0., 405. )
layerMap[16].position = TVector3( 0., 0., 432. )
layerMap[17].position = TVector3( 0., 0., 459. )
layerMap[18].position = TVector3( 0., 0., 486. )
layerMap[19].position = TVector3( 0., 0., 513. )
layerMap[20].position = TVector3( 0., 0., 540. )
layerMap[21].position = TVector3( 0., 0., 567. )
layerMap[22].position = TVector3( 0., 0., 594. )
layerMap[23].position = TVector3( 0., 0., 621. )
layerMap[24].position = TVector3( 0., 0., 648. )
layerMap[25].position = TVector3( 0., 0., 675. )
layerMap[26].position = TVector3( 0., 0., 702. )
layerMap[27].position = TVector3( 0., 0., 729. )
layerMap[28].position = TVector3( 0., 0., 756. )
layerMap[29].position = TVector3( 0., 0., 774.3 )
layerMap[30].position = TVector3( 0., 0., 810. )
layerMap[31].position = TVector3( 0., 0., 837. )
layerMap[32].position = TVector3( 0., 0., 864. )
layerMap[33].position = TVector3( 0., 0., 891. )
layerMap[34].position = TVector3( 0., 0., 918. )
layerMap[35].position = TVector3( 0., 0., 945. )
layerMap[36].position = TVector3( 0., 0., 972. )
layerMap[37].position = TVector3( 0., 0., 999. )
layerMap[38].position = TVector3( 0., 0., 1026. )
layerMap[39].position = TVector3( 0., 0., 1261. )
layerMap[40].position = TVector3( 0., 0., 1314. )
layerMap[41].position = TVector3( 0., 0., 1367. )
layerMap[42].position = TVector3( 0., 0., 1420. )
layerMap[43].position = TVector3( 0., 0., 1473. )
layerMap[44].position = TVector3( 0., 0., 1526. )
layerMap[45].position = TVector3( 0., 0., 1579. )
layerMap[46].position = TVector3( 0., 0., 1632. )
layerMap[47].position = TVector3( 0., 0., 1762. )
layerMap[48].position = TVector3( 0., 0., 1892. )
layerMap[49].position = TVector3( 0., 0., 2022. )
layerMap[50].position = TVector3( 0., 0., 2152. )
layerMap[51].position = TVector3( 0., 0., 2282. )
layerMap[52].position = TVector3( 0., 0., 2412. )
layerMap[53].position = TVector3( 0., 0., 2542. )

def getClosestLayer( zPosition ):
    myLayer = None
    for layerID, layer in layerMap.items():
        if layer.position.z() > zPosition:
            break
        myLayer = layer
    if not myLayer:
        return layerMap[0]
    return myLayer

def getModuleFromPosition( position ):
    layer = getClosestLayer( position.z() )
    myModule = None
    for moduleID, module in layer.modules.items():
        if module.yMin <= position.y() and module.yMax >= position.y() and module.xMin <= position.x() and module.xMax >= position.x():
            myModule = module
            break
    if not myModule:
        raise AttributeError('Invalid position in getModuleFromPosition: %g, %g, %g' % (position.x(), position.y(), position.z()))
    return myModule

def getModuleFromIndices( uIndex, vIndex, layerIndex ):
    layer = layerMap[layerIndex]
    myModule = None
    for moduleID, module in layer.modules.items():
        if module.uMin <= uIndex and module.uMax >= uIndex and module.vMin <= vIndex and module.vMax >= vIndex:
            myModule = module
            break
    if not myModule:
        raise AttributeError('Invalid position in getModuleFromIndices: %d, %d, %d' % (uIndex, vIndex, layerIndex))
    return myModule

def getFrontEndBoardFromPosition( position ):
    try:
        module = getModuleFromPosition( position )
    except AttributeError:
        raise AttributeError('Invalid position in getFrontEndBoardFromPosition: %g, %g, %g' % (position.x(), position.y(), position.z()) )
    myFrontEndBoard = None
    for frontEndBoardID, frontEndBoard in module.frontEndBoards.items():
        if frontEndBoard.yMin <= position.y() and frontEndBoard.yMax >= position.y() and frontEndBoard.xMin <= position.x() and frontEndBoard.xMax >= position.x():
            myFrontEndBoard = frontEndBoard
            break
    if not myFrontEndBoard:
        raise AttributeError('Invalid position in getFrontEndBoardFromPosition: %g, %g, %g' % (position.x(), position.y(), position.z()))
    return myFrontEndBoard

def getFrontEndBoardFromIndices( uIndex, vIndex, layerIndex ):
    try:
        module = getModuleFromIndices( uIndex, vIndex, layerIndex )
    except AttributeError:
        raise AttributeError('Invalid position in getFrontEndBoardFromIndices: %d, %d, %d' % (uIndex, vIndex, layerIndex))
    myFrontEndBoard = None
    for frontEndBoardID, frontEndBoard in module.frontEndBoards.items():
        if frontEndBoard.uMin <= uIndex and frontEndBoard.uMax >= uIndex and frontEndBoard.vMin <= vIndex and frontEndBoard.vMax >= vIndex:
            myFrontEndBoard = frontEndBoard
            break
    if not myFrontEndBoard:
        raise AttributeError('Invalid position in getFrontEndBoardFromIndices: %d, %d, %d' % (uIndex, vIndex, layerIndex))
    return myFrontEndBoard

def getPositionFromIndices( uIndex, vIndex, layerIndex ):
    myLayer = layerMap[layerIndex]
    myModule = None
    myFrontEndBoard = None
    for moduleID, module in layer.modules.items():
        if module.vMin <= vIndex and module.vMax >= vIndex:
            myModule = module
            for frontEndBoardID, frontEndBoard in module.frontEndBoards.items():
                if frontEndBoard.uMin <= uIndex and frontEndBoard.uMax >= uIndex:
                    myFrontEndBoard = frontEndBoard
                    break
            break
    if not myFrontEndBoard:
        raise AttributeError('Invalid indices in getPositionFromIndices: %d, %d, %d' % (uIndex, vIndex, layerIndex))
    localPosition = TVector3( ( uIndex - myFrontEndBoard.uMin - ( myFrontEndBoard.nCellsU - 1. ) / 2. ) * myFrontEndBoard.cellSizeU,
                ( vIndex - myFrontEndBoard.vMin - ( myFrontEndBoard.nCellsV - 1. ) / 2. ) * myFrontEndBoard.cellSizeV, 0. )
    return localPosition + myFrontEndBoard.position + myModule.position + myLayer.position
    
def getIndicesFromPosition( position ):
    try:
        module = getModuleFromPosition( position )
    except AttributeError:
        raise AttributeError('Invalid position in getIndicesFromPosition: %g, %g, %g' % (position.x(), position.y(), position.z()))
    layer = module.mother
    localPosition = position - layer.position
    u = int((localPosition.x() - module.xMin) / module.cellSizeU) + module.uMin;
    v = int((localPosition.y() - module.yMin) / module.cellSizeV) + module.vMin;
    if localPosition.x() == module.xMax:
        u -= 1
    if localPosition.y() == module.yMax:
        v -= 1
    return u, v, layer.id

def isModuleBoundary( uIndex, vIndex, layerIndex, margin ):
    try:
        module = getModuleFromIndices( uIndex, vIndex, layerIndex )
    except AttributeError:
        return False
    if uIndex >= (module.uMax - margin) or uIndex <= (module.uMin + margin) or vIndex >= (module.vMax - margin) or vIndex <= (module.vMin + margin):
        return True;
    return False

def isFrontEndBoardBoundary( uIndex, vIndex, layerIndex, margin ):
    try:
        frontEndBoard = getFrontEndBoardFromIndices( uIndex, vIndex, layerIndex )
    except AttributeError:
        return False
    if uIndex >= (frontEndBoard.uMax - margin) or uIndex <= (frontEndBoard.uMin + margin) or vIndex >= (frontEndBoard.vMax - margin) or vIndex <= (frontEndBoard.vMin + margin):
        return True;
    return False
