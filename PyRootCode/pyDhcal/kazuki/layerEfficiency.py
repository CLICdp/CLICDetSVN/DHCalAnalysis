from __future__ import division
import ROOT
import sys
from runList import RunList
import muon

runList = RunList.readTextFile()

runNumberList = ['081','109','082','119','087','127','105','140','070','164','166','177','178','181','192','194','210','211','213','214','215','217','220','223','224','226','227','228','229','232','233','234','235','236','237','238','239','240','241','242','243','251','253','254','257','258','259','260','265','268','279','309','321','343','354','386','393','381','378','358','356','361','368','365','363']

canvasDict = {}
canIndex = 0
for runNumber in runNumberList:
#runNumber = 660348 #180GeV

	tree = muon.getTree660(runNumber)
	nEvts = tree.GetEntries()
	
	graphDictZX = {}
	graphDict = {}
	nhitsList = []
	
	passhist = ROOT.TH1I('run660{0}'.format(runNumber),'DHCAL Run 660{0}'.format(runNumber),54,0,54)
	totalhist = ROOT.TH1I('th','th',54,0,54)
	
	for iEv in xrange(nEvts):
		if iEv % 10000 == 0:
			print 'processing...', repr(iEv).rjust(7),
			print 'of', repr(nEvts).rjust(7), 'runNumber', '660{0}'.format(runNumber)
		graphDict[iEv] = ROOT.TGraph()
		graphDict[iEv].SetName('graphforfit{0}'.format(iEv))
		graphDictZX[iEv] = ROOT.TGraph()
		graphDictZX[iEv].SetName('graphforfitZX{0}'.format(iEv))
		clusters = muon.prepareCluster(tree, iEv)
		muon.cutClusters(clusters, graphDict[iEv], iEv, 100)
		muon.cutClustersZX(clusters, graphDictZX[iEv], iEv, 100)
		if graphDict[iEv].GetN() > 9 and graphDictZX[iEv].GetN() > 9:
			if muon.pol1fit(graphDict[iEv], tree) < 100 and muon.pol1fit(graphDictZX[iEv], tree) < 100:
				x0 = muon.getX0(clusters)
				y0 = muon.getY0(clusters)
				fitZY = graphDict[iEv].GetFunction("pol1")
				p0ZY = fitZY.GetParameter(0)
				p1ZY = fitZY.GetParameter(1)
				fitZX = graphDictZX[iEv].GetFunction("pol1")
				p0ZX = fitZX.GetParameter(0)
				p1ZX = fitZX.GetParameter(1)
				muon.fillpasshist(clusters,p0ZY,p0ZX,p1ZY,p1ZX,passhist)
				muon.filltotalhist(totalhist)
	
	canvasDict[canIndex] = ROOT.TCanvas('c660{0}'.format(runNumber),'DHCAL Run 660{0}'.format(runNumber))
	effgraph = ROOT.TGraphAsymmErrors(passhist,totalhist)
	effgraph.Draw("AP")
	outfile = ROOT.TFile("middleLayerEff.root","update")
	effgraph.Write()
	outfile.Close()
	canIndex += 1
				
	
