from __future__ import division
import ROOT
import sys
from runList import RunList
import muon

fillList = [660113, 660123, 660129, 660099, 660146, 660155, 660169, 660267, 660236, 660263, 660386, 660348]
#660113:2GeV , 660123:4GeV , 660129:6GeV , 660099:7GeV , 660146:8GeV , 660155:9GeV , 
#660169:10GeV , 660267:20GeV , 660236:60GeV , 660263:100GeV , 660368:150GeV , 660348:180GeV
runList = RunList.readTextFile()

gr = ROOT.TGraph()
gr.SetName("muonSelection")
for runN in fillList:
	np = gr.GetN()
	mom = runList.getRun(runN).beamMomentum
	hits = muon.muonSelection(runN)
	gr.SetPoint(np,mom,hits)
gr.GetXaxis().SetTitle("beamMomentum [GeV]")
gr.GetYaxis().SetTitle("number of hits")
gr.Draw("AP")
