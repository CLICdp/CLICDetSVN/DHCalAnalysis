from runList import RunList
import ROOT

ROOT.gSystem.Load('/afs/cern.ch/user/k/kmotohas/public/DHCalAnalysis/dhcCode/libDhcTree.so')
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'
runList = RunList.readTextFile()

for run in runList:
	print 'runNumber =',
	print '%7s' % run.runNumber,
	print '[spreadsheet]:',
	print '%7s' % run.nEvents,
	##
	print '[Root file]:',
	fileName = '%s/dhcTree-Run%s.root' % (storagePath, run.runNumber)
	infile = ROOT.TFile.Open(fileName)
	if not infile:
		print 'False'
	else:
		tree = infile.Get('DhcTree')
		nEvts = tree.GetEntries()
		print '%7s' % nEvts,
		diff = abs(run.nEvents - nEvts)
		if not run.runType == run.runType.DATA :
			print run.runType
		elif diff < 3000:
			print 'well matched'
		else:
			print 'different!!!!!!!!!!!!!!'
	if infile:
		infile.Close()
