from __future__ import division
from runList import RunList
import ROOT
import sys

ROOT.gSystem.Load('/afs/cern.ch/user/k/kmotohas/public/DHCalAnalysis/dhcCode/libDhcTree.so')
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'
runList = RunList.readTextFile()
#runNumber = 660268
for run in runList:
	print 'runNumber =',
	print '%7s' % run.runNumber,
	print '[spreadsheet]:',
	print '%6s' % run.eventsPerSpill,

	print '[Root file]:',
	if run.eventsPerSpill == 0.0:
		print 0
		continue

	fileName = '%s/dhcTree-Run%s.root' % (storagePath, run.runNumber)
#if len(sys.argv) > 1:
#	fileName = sys.argv[1]
	infile = ROOT.TFile.Open(fileName)
	if not infile:
		print 'False'
		continue
	else:
		tree = infile.Get('DhcTree')

	time = 0
	nEvents = 0
	nEventsInSpill = []
	nEvts = tree.GetEntries()

	for iEv in xrange(nEvts):
		tree.GetEvent(iEv)
		prevtime = time
		time = tree.EventBranch.GetHeader().GetEvtTime().AsDouble()
		if time - prevtime < 1: #
			nEvents += 1
		elif nEvents < 10: continue
		else:
			nEventsInSpill += [nEvents]
			nEvents = 0	

	if len(nEventsInSpill) == 0:
		print 0
	else:
		average = sum(nEventsInSpill) / len(nEventsInSpill)	
	#print nEventsInSpill
		print '%.1f' % average
	if infile: infile.Close()
