from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('../../dhcCode/libDhcTree.so')
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'

runNumber = 660109 # 2 GeV
if len(sys.argv) > 1:
	runNumber = sys.argv[1]
fileName = '{0}/dhcTree-Run{1}.root'.format(storagePath,runNumber)

ofile = open('output.dat','w')

infile = ROOT.TFile.Open(fileName)
tree = infile.Get('DhcTree')
nEvts = tree.GetEntries()

for iEv in xrange(nEvts):
	tree.GetEntry(iEv)
	eb = tree.EventBranch
	#print ev.GetNHits()
	eb.findClusters()
	clusters = eb.get2DClusters()
	sizeList = []
	for layerClusters in clusters:
		sizeList.append(int(layerClusters.second.at(0).getSize()))
	if len(sizeList) == 0: continue
	average2DCS = sum(sizeList) / len(sizeList)
	if iEv % 100 == 0: print iEv
	striEv = str(iEv)
	straverage2DCS = str(average2DCS)
	written = striEv + ' ' + straverage2DCS + '\n'
	ofile.write(written)
	#print iEv, average2DCS
