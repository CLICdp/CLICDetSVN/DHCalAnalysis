#!/bin/bash
#BSUB -J singleParticles[1-106]
#BSUB -q 8nh
#BSUB -o /afs/cern.ch/user/j/jfstrube/public/produceSingleEvents/stdout.%J_%I
#BSUB -e /afs/cern.ch/user/j/jfstrube/public/produceSingleEvents/stderr.%J_%I
#BSUB -R "type==SLC5_64"

### LCG Libraries
export PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.6.2/x86_64-slc5/bin:${PATH}
export LD_LIBRARY_PATH=/afs/cern.ch/sw/lcg/contrib/gcc/4.6.2/x86_64-slc5/lib64:${LD_LIBRARY_PATH}
export PATH=/afs/cern.ch/user/j/jfstrube/public/Ocaml-3.12.1/bin:${PATH}
export LD_LIBRARY_PATH=/afs/cern.ch/user/j/jfstrube/public/Ocaml-3.12.1/lib:${LD_LIBRARY_PATH}
source /afs/cern.ch/eng/clic/software/setup.sh

mkdir LSB_${LSB_JOBID}_${LSB_JOBINDEX}
cd LSB_${LSB_JOBID}_${LSB_JOBINDEX}
contents=($(cat /afs/cern.ch/user/k/kmotohas/filelist.txt))
filename=${contents[${LSB_JOBINDEX} - 1]}
cp ${filename} .
base=$(basename ${filename})
python <your analysis program> ${base}
cp output.root /afs/cern.ch/work/k/kmotohas/${base/.root/_analysis.root}

