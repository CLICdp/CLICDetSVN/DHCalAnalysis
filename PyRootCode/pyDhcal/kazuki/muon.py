from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('../../dhcCode/libDhcTree.so')
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'

PzLayer = {'0':0, '27':1, '54':2, '81':3, '108':4, '135':5, '162':6, '188':7, '216':8, '242':9, '270':10, '297':11, '324':12, '350':13, '377':14, '405':15, '432':16, '459':17, '485':18, '512':19, '540':20, '567':21, '594':22, '620':23, '648':24, '675':25, '701':26, '729':27, '747':28, '755':28, '774':29, '783':29, '810':30, '836':31, '864':32, '890':33, '918':34, '945':35, '971':36, '999':37, '1025':38, '1260':39, '1313':40, '1366':41, '1420':42, '1473':43, '1526':44, '1578':45, '1631':46, '1761':47, '1891':48, '2021':49, '2151':50, '2281':51, '2411':52, '2541':53}

graphDict = {}
histDict = {}
nhitsList = []

def getTree(runNumber):
	infileName = '{0}/dhcTree-Run{1}.root'.format(storagePath,runNumber)
	infile = ROOT.TFile.Open(infileName)
	tree = infile.Get('DhcTree')
	return tree
	
def getTree660(runNumber):
	infileName = '{0}/dhcTree-Run660{1}.root'.format(storagePath,runNumber)
	infile = ROOT.TFile.Open(infileName)
	tree = infile.Get('DhcTree')
	return tree
	
def prepareCluster(tree, iEv):
	tree.GetEntry(iEv)
	eb = tree.EventBranch
	eb.findClusters()
	clusters = eb.get2DClusters()
	return clusters
	
def getX0(clusters):
	layer0 = []
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = lcs.at(0).getZ()
		if z == 0:
			for index in xrange(len(lcs)):
				x = lcs.at(index).getX()
				y = lcs.at(index).getY()
				rr = x*x + y*y
				layer0.append([rr,x,y])
		else:
			break
	layer0.sort()
	try:
		x0 = layer0[0][1]
	except IndexError:
		x0 = 0
	return x0

def getY0(clusters):
	layer0 = []
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = lcs.at(0).getZ()
		if z == 0:
			for index in xrange(len(lcs)):
				x = lcs.at(index).getX()
				y = lcs.at(index).getY()
				rr = x*x + y*y
				layer0.append([rr,x,y])
		else:
			break
	layer0.sort()
	try:
		y0 = layer0[0][2]
	except IndexError:
		y0 = 0
	return y0
		
	
def cutClustersZX(clusters, graph, iEv, radius):
	x0 = getX0(clusters)
	for layerClusters in clusters:
		lcs = layerClusters.second # for omitting
		for index in xrange(len(lcs)):
			np = graph.GetN()
			x = lcs.at(index).getX()
			z = lcs.at(index).getZ()
			if abs(x - x0) < radius:
				graph.SetPoint(np,z,x)

	
def cutClusters(clusters, graph, iEv, radius):
	y0 = getY0(clusters)
	for layerClusters in clusters:
		lcs = layerClusters.second # for omitting
		for index in xrange(len(lcs)):
			np = graph.GetN()
			y = lcs.at(index).getY()
			z = lcs.at(index).getZ()
			if abs(y - y0) < radius:
				graph.SetPoint(np,z,y)

def pol1fit(graph, tree):
	graph.Fit("pol1","Q")
	chi2 = graph.GetFunction("pol1").GetChisquare()
	ndf = graph.GetFunction("pol1").GetNDF()
	return chi2 / ndf

def pol2fit(graph, tree):
	graph.Fit("pol2","Q")
	chi2 = graph.GetFunction("pol2").GetChisquare()
	ndf = graph.GetFunction("pol2").GetNDF()
	return chi2 / ndf
	
def muonSelection( runNumber ):
	tree = getTree(runNumber)
	nEvts = tree.GetEntries()
	for iEv in xrange(nEvts):
		if iEv % 10000 == 0:
			print 'processing...', repr(iEv).rjust(7) ,
			print 'of', repr(nEvts).rjust(7), 'runNumber', runNumber
		graphDict[iEv] = ROOT.TGraph()
		graphDict[iEv].SetName('graphforfit{0}'.format(iEv))
		clusters = prepareCluster(tree, iEv)
		cutClusters(clusters, graphDict[iEv], iEv, 160)
		if graphDict[iEv].GetN() > 9:
			if pol2fit(graphDict[iEv], tree) < 100: # it's muon!!
				nhitsList.append(tree.EventBranch.GetNHits()) 	
	return sum(nhitsList) / len(nhitsList)

def fillpasshist(clusters,p0ZY,p0ZX,p1ZY,p1ZX,passhist):
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = int(lcs.at(0).getZ())
		xi = p0ZX + z * p1ZX
		yi = p0ZY + z * p1ZY
		for index in xrange(len(lcs)):
			x = lcs.at(index).getX()
			y = lcs.at(index).getY()
			rr = (x - xi)**2 + (y - yi)**2
			if rr < 2500:
				layer = PzLayer[str(z)]
				passhist.Fill(layer)	
				break

def filltotalhist(totalhist):
	for index in xrange(len(PzLayer)):
		totalhist.Fill(index)
