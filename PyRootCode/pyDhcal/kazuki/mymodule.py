from __future__ import division
from dhcalDefinitions import *
import ROOT
import math
import sys
import operator
import array

ROOT.gSystem.Load('../../dhcCode/libDhcTree.so')
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'

PzLayer = {'0':0, '27':1, '54':2, '81':3, '108':4, '135':5, '162':6, '188':7, '216':8, '242':9, '270':10, '297':11, '324':12, '350':13, '377':14, '405':15, '432':16, '459':17, '485':18, '512':19, '540':20, '567':21, '594':22, '620':23, '648':24, '675':25, '701':26, '729':27, '747':28, '755':28, '774':29, '783':29, '810':30, '836':31, '864':32, '890':33, '918':34, '945':35, '971':36, '999':37, '1025':38, '1260':39, '1313':40, '1366':41, '1420':42, '1473':43, '1526':44, '1578':45, '1631':46, '1761':47, '1891':48, '2021':49, '2151':50, '2281':51, '2411':52, '2541':53}
# {'z position':layer number} 

graphDict = {}
histDict = {}
nhitsList = []

def getTree(runNumber):
	infileName = '{0}/dhcTree-Run{1}.root'.format(storagePath,runNumber)
	infile = ROOT.TFile.Open(infileName)
	tree = infile.Get('DhcTree')
	return tree
	
# omit 660
def getTree660(runNumber):
	infileName = '{0}/dhcTree-Run660{1}.root'.format(storagePath,runNumber)
	infile = ROOT.TFile.Open(infileName)
	tree = infile.Get('DhcTree')
	return tree
	
def prepareCluster(tree, iEv):
	tree.GetEntry(iEv)
	eb = tree.EventBranch
	eb.findClusters()
	clusters = eb.get2DClusters()
	return clusters
	
def getX0(clusters):
# get x of center cluster on layer 0
	layer0 = []
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = lcs.at(0).getZ()
		if z == 0:
			for index in xrange(len(lcs)):
				x = lcs.at(index).getX()
				y = lcs.at(index).getY()
				rr = x*x + y*y
				layer0.append([rr,x,y])
		else:
			break
	layer0.sort()
	try:
		x0 = layer0[0][1]
	except IndexError:
		x0 = 0
	return x0

def getY0(clusters):
# get y of center cluster on layer 0
	layer0 = []
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = lcs.at(0).getZ()
		if z == 0:
			for index in xrange(len(lcs)):
				x = lcs.at(index).getX()
				y = lcs.at(index).getY()
				rr = x*x + y*y
				layer0.append([rr,x,y])
		else:
			break
	layer0.sort()
	try:
		y0 = layer0[0][2]
	except IndexError:
		y0 = 0
	return y0
		
	
# cylinder cut
def cutClustersZX(clusters, graph, iEv, radius): # z-x plane
	x0 = getX0(clusters)
	for layerClusters in clusters:
		lcs = layerClusters.second # for omitting
		for index in xrange(len(lcs)):
			np = graph.GetN()
			x = lcs.at(index).getX()
			z = lcs.at(index).getZ()
			if abs(x - x0) < radius:
				graph.SetPoint(np,z,x)

	
def cutClusters(clusters, graph, iEv, radius): # z-y plane
	y0 = getY0(clusters)
	for layerClusters in clusters:
		lcs = layerClusters.second # for omitting
		for index in xrange(len(lcs)):
			np = graph.GetN()
			y = lcs.at(index).getY()
			z = lcs.at(index).getZ()
			if abs(y - y0) < radius:
				graph.SetPoint(np,z,y)

# get chi2/ndf in line fit
def pol1fit(graph, tree):
	graph.Fit("pol1","Q")
	chi2 = graph.GetFunction("pol1").GetChisquare()
	ndf = graph.GetFunction("pol1").GetNDF()
	return chi2 / ndf

def pol2fit(graph, tree):
	graph.Fit("pol2","Q")
	chi2 = graph.GetFunction("pol2").GetChisquare()
	ndf = graph.GetFunction("pol2").GetNDF()
	return chi2 / ndf
	
def muonSelection( runNumber ):
	tree = getTree(runNumber)
	nEvts = tree.GetEntries()
	for iEv in xrange(nEvts):
		if iEv % 10000 == 0:
			print 'processing...', repr(iEv).rjust(7) ,
			print 'of', repr(nEvts).rjust(7), 'runNumber', runNumber
		graphDict[iEv] = ROOT.TGraph()
		graphDict[iEv].SetName('graphforfit{0}'.format(iEv))
		clusters = prepareCluster(tree, iEv)
		cutClusters(clusters, graphDict[iEv], iEv, 160)
		if graphDict[iEv].GetN() > 9:
			if pol2fit(graphDict[iEv], tree) < 100: # it's muon!!
				nhitsList.append(tree.EventBranch.GetNHits()) 	
	return sum(nhitsList) / len(nhitsList)

# to compute layer efficiency, filling 'On' events
def fillpasshist(clusters,p0ZY,p0ZX,p1ZY,p1ZX,passhist):
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = int(lcs.at(0).getZ())
		xi = p0ZX + z * p1ZX
		yi = p0ZY + z * p1ZY
		for index in xrange(len(lcs)):
			x = lcs.at(index).getX()
			y = lcs.at(index).getY()
			rr = (x - xi)**2 + (y - yi)**2
			if rr < 2500:
				layer = PzLayer[str(z)]
				passhist.Fill(layer)	
				break

# to compute layer efficiency, filling the total number of events
def filltotalhist(totalhist):
	for index in xrange(len(PzLayer)):
		totalhist.Fill(index)

# hist of the x y distance between line fit and cluster center
def radiushist(clusters,p0ZY,p0ZX,p1ZY,p1ZX,hx,hy):
	for layerClusters in clusters:
		lcs = layerClusters.second
		z = int(lcs.at(0).getZ())
		xi = p0ZX + z * p1ZX
		yi = p0ZY + z * p1ZY
		for index in xrange(len(lcs)):
			x = lcs.at(index).getX()
			y = lcs.at(index).getY()
			X = (x - xi)
			Y = (y - yi)
			hx.Fill(X)	
			hy.Fill(Y)	

# find line noise(hot FEB), box noise(hot ASIC)
layerNoiseLineDict = {}
def findNoise(tree,iEv,runNumber):  
	for index in xrange(len(PzLayer)):
		layerNoiseLineDict[index] = 0
	LineNoiseRegion = (-10 < x < 10 or y == 475 or y == 165 or y == 155 or y == -155 or y == -165 or y == -475)
	layerNoiseBoxDict = {'22':0,'24':0,'39':0,'40':0}
	nextlayerNoiseBoxDict = {'21':0,'23':0,'25':0,'38':0,'41':0}
	BoxLayer = (layerNum == 22 or layerNum == 24 or layerNum == 39 or layerNum == 40)
	BoxNextLayer = (layerNum == 21 or layerNum == 23 or layerNum == 25 or layerNum == 38 or layerNum == 41)
	BoxRegion = (-80 < x < 0 and 240 < y < 320) or (0 < x < 80 and -160 < y < -80) or (80 < x < 160 and -240 < y < -160)
	if runNumber <= 660205:
		BoxLayer = (layerNum == 22)
		BoxNextLayer = (layerNum == 21 or layerNum == 23)
		BoxRegion = (0 < x < 80 and -160 < y < -80)
	tree.GetEvent(iEv)
	hitlist = tree.EventBranch.GetHits()
	nEntries = hitlist.GetEntries()
	for hitnum in xrange(nEntries):
		hit = hitlist[hitnum]
		x = hit.GetPx()
		y = hit.GetPy()
		z = hit.GetPz()
		layerNum = PzLayer[str(z)]
		if LineNoiseRegion:
			layerNoiseLineDict[layerNum] += 1
		if (BoxLayer and BoxRegion):
			layerNoiseBoxDict[str(layerNum)] += 1
		if (BoxNextLayer and BoxRegion):
			nextlayerNoiseBoxDict[str(layerNum)] += 1
	if max(layerNoiseLineDict.iteritems(), key=operator.itemgetter(1))[1] > 50:
		return True # line noise
	if max(layerNoiseBoxDict.iteritems(), key=operator.itemgetter(1))[1] > 50:
		if max(nextlayerNoiseBoxDict.iteritems(), key=operator.itemgetter(1))[1] < 50: # not shower
			print max(nextlayerNoiseBoxDict.iteritems(), key=operator.itemgetter(1))[1],
			return True # box noise

# helper method to read the trigger bit from an event by Christian
def getTriggerBit( event ):
    bit = array.array( 'i', [0] )
    event.GetTriggerBits().Get( bit )
    return bit[0]
