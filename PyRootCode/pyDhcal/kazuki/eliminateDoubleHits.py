from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('../../dhcCode/libDhcTree.so')
ROOT.gStyle.SetOptFit(1111)
storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'
runNumber = 660268
fileName = '%s/dhcTree-Run%s.root' % (storagePath, runNumber)
if len(sys.argv) > 1:
	fileName = sys.argv[1]

infile = ROOT.TFile.Open(fileName)
tree = infile.Get('DhcTree')

nEvts = tree.GetEntries()
nEvents = 0
buffer = set()
#bufferTS = set()

hist = ROOT.TH1I("hist","double hits",20000,0,20000)

for iEv in xrange(nEvts):
	tree.GetEvent(iEv)
	hitlist = tree.EventBranch.GetHits()
	buffer.clear()
	#clearbufferTS.clear()
	for hitnum in xrange(hitlist.GetEntries()):
		hit = hitlist[hitnum]
		if ( hit.GetPx(),hit.GetPy(),hit.GetPz() ) in buffer:
			hist.Fill(iEv)
			#print iEv, hitnum, [hit.GetPx(),hit.GetPy(),hit.GetPz()],
			#print "already exists"
		else:
			buffer.add( ( hit.GetPx(),hit.GetPy(),hit.GetPz() ) )
			#bufferTS.add( ( hit.GetPx(),hit.GetPy(),hit.GetPz(),hit.GetTS() ) )
	
	#nEvents += 1
	
hist.Draw()
