from __future__ import division
import ROOT
import sys
from runList import RunList
import muon

fillList = [660113, 660123, 660129, 660099, 660146, 660155, 660169, 660267, 660236, 660263, 660386, 660348]
#660113:2GeV , 660123:4GeV , 660129:6GeV , 660099:7GeV , 660146:8GeV , 660155:9GeV , 
#660169:10GeV , 660267:20GeV , 660236:60GeV , 660263:100GeV , 660368:150GeV , 660348:180GeV
runList = RunList.readTextFile()

gr = ROOT.TGraph()
gr.SetName("muonratio")
for runNumber in fillList:
	numMuon = 0
	tree = muon.getTree(runNumber)
	nEvts = tree.GetEntries()
	graphDict = {}
	nhitsList = []
	for iEv in xrange(nEvts):
		if iEv % 10000 == 0:
			print 'processing...', repr(iEv).rjust(7) ,
			print 'of', repr(nEvts).rjust(7), 'runNumber', runNumber, numMuon
		graphDict[iEv] = ROOT.TGraph()
		graphDict[iEv].SetName('graphforfit{0}'.format(iEv))
		clusters = muon.prepareCluster(tree, iEv)
		muon.cutClusters(clusters, graphDict[iEv], iEv, 160)
		if graphDict[iEv].GetN() > 9:
			if muon.pol1fit(graphDict[iEv], tree) < 100: # it's muon!!
				numMuon += 1 	
	np = gr.GetN()
	mom = runList.getRun(runNumber).beamMomentum
	eff = numMuon / nEvts
	gr.SetPoint(np,mom,eff)
gr.GetXaxis().SetTitle("beamMomentum [GeV]")
gr.GetYaxis().SetTitle("muon ratio")
gr.Draw("AP")
