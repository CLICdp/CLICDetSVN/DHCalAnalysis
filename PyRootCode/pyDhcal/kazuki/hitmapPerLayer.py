from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('/afs/cern.ch/user/k/kmotohas/public/DHCalAnalysis/dhcCode/libDhcTree.so')
ROOT.gStyle.SetOptStat(0)

storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'
runNumber = 660390
if len(sys.argv) > 1:
	runNumber = sys.argv[1]
fileName = '%s/dhcTree-Run%s.root' % (storagePath, runNumber)

infile = ROOT.TFile.Open(fileName)
tree = infile.Get('DhcTree')

PzLayer = [0, 27, 54, 81, 108, 135, 162, 188, 216, 242, 270, 297, 324, 350, 377, 405, 432, 459, 485, 512, 540, 567, 594, 620, 648, 675, 701, 729, 755, 783, 810, 836, 864, 890, 918, 945, 971, 999, 1025, 1260, 1313, 1366, 1420, 1473, 1526, 1578, 1631, 1761, 1891, 2021, 2151, 2281, 2411]
#PzLayer = [0, 27, 54, 81, 108, 135, 162, 188, 216, 242, 270, 297, 324, 350, 377, 405, 432, 459, 485, 512, 540, 567, 594, 620, 648, 675, 701, 729, 747, 774, 810, 836, 864, 890, 918, 945, 971, 999, 1025, 1260, 1313, 1366, 1420, 1473, 1526, 1578, 1631, 1761, 1891, 2021, 2151, 2281, 2411]
#PzLayer = {'0':0, '27':1, '54':2, '81':3, '108':4, '135':5, '162':6, '188':7, '216':8, '242':9, '270':10, '297':11, '324':12, '350':13, '377':14, '405':15, '432':16, '459':17, '485':18, '512':19, '540':20, '567':21, '594':22, '620':23, '648':24, '675':25, '701':26, '729':27, '747':28, '755':28, '774':29, '783':29, '810':30, '836':31, '864':32, '890':33, '918':34, '945':35, '971':36, '999':37, '1025':38, '1260':39, '1313':40, '1366':41, '1420':42, '1473':43, '1526':44, '1578':45, '1631':46, '1761':47, '1891':48, '2021':49, '2151':50, '2281':51, '2411':52, '2541':53}

nPzLayer = len(PzLayer)
nEvts = tree.GetEntries()
nEvents = 0

hitmapDict = {}
Nmake3D = 500

for index in xrange(len(PzLayer)):
	hitmapDict[index] =  ROOT.TH2F('HitmapLayer{0}'.format(index),'HitmapLayer{0} #{1};x[mm];y[mm]'.format(index,runNumber),100,-500,500,100,-500,500)

Hitmap3Dtmp = ROOT.TH3F('Hitmap3Dtmp','Hitmap3Dtmp #%s;x[mm];y[mm];z[mm]' % (runNumber),100,-500,500,100,-500,500,250,0,2500)
ctmp = ROOT.TCanvas()
for iEv in xrange(nEvts):
	if iEv % 500 == 0: print "processing...",iEv,"in",nEvts 
	tree.GetEvent(iEv)
	hitlist = tree.EventBranch.GetHits()
	nEntries = hitlist.GetEntries()
	if iEv % Nmake3D == 0: Hitmap3Dtmp.Reset()
	for hitnum in xrange(nEntries):
		hit = hitlist[hitnum]
		for index in xrange(nPzLayer):
			if hit.GetPz() == PzLayer[index]: hitmapDict[index].Fill(hit.GetPx(),hit.GetPy())
		if iEv % Nmake3D == 0: Hitmap3Dtmp.Fill(hit.GetPx(),hit.GetPy(),hit.GetPz())
	if iEv % Nmake3D == 0:
		Hitmap3Dtmp.SetMarkerColor(ROOT.kBlue)
		Hitmap3Dtmp.Draw("BOX")
		ctmp.Update()

canvasDict = {}
for index in xrange(nPzLayer):
	if index % 9 == 0:
		canvasDict[int(index / 9)] = ROOT.TCanvas('Layer{0}-{1}'.format(index,index+8),'Layer{0}-{1}'.format(index,index+8))
		canvasDict[int(index / 9)].Divide(3,3)
	canvasDict[int(index / 9)].cd((index % 9) + 1)
	hitmapDict[index].SetMaximum(150)
	hitmapDict[index].Draw("COLZ")
