from __future__ import division
from runList import RunList
import ROOT
import sys

ROOT.gSystem.Load('/afs/cern.ch/user/k/kmotohas/public/DHCalAnalysys/dhcCode/libDhcTree.so')
ROOT.gStyle.SetOptStat(0)

storagePath = '/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DhcalMay2012_rootTrees'
runList = RunList.readTextFile()

nEvents = 0

#RunList = open('/afs/cern.ch/user/k/kmotohas/public/DHCalAnalysis/DHCalRunList.txt','r')
#runNumberList = []
#energyList = []
#for line in RunList.readlines():
#	if line = 0: continue # the beginning column is just string
#	array = line.split()
#	runNumberList.append(array[2])
#	energyList.append(array[7])
#	if array[2] = 660422: break # after #660422, array[2] isn't runNumber

energyList = []
pressureList = [] 

graph = ROOT.TGraph()
for run in runList:
	np = graph.GetN()
	fileName = '{0}/dhcTree-Run{1}.root'.format(storagePath,run.runNumber)
	infile = ROOT.TFile.Open(fileName)
	tree = infile.Get('DhcTree')
	nEvts = tree.GetEntries()
	nHits = 0
	for iEv in xrange(nEvts):
		tree.GetEvents()
		nHits += tree.EventBranch.GetNhits()
	graph.SetPoint(np,run.beamMomentum,nHits / nEvts)
	

#nHitsList = []
#energyVector = []
#for runNumber in runNumberList:
#	try: energyVector.append(int(energyList[runNumber]))
#	except ValueError: continue 
#	
#	fileName = '{0}/dhcTree-Run{1}.root'.format(storagePath,runNumber)
#	infile = ROOT.TFile.Open(fileName)
#	tree = infile.Get('DhcTree')
#	nEvts = tree.GetEntries()
#	
#	nHits = 0
#	for iEv in xrange(nEvts):
#		tree.GetEvents()
#		nHits += tree.EventBranch.GetNhits()
#	nHitsVector.append(int(nHits / nEvts))
	
#nHitsVsEnergy = ROOT.TGraph(len(runList),energyList,pressureList)
graph.Draw("AP")
