import PySelectorBase
import ROOT

class DefaultAnalysis(PySelectorBase.PySelectorBase):

	def ProcessEntry(self, tree, entry):
		tree.EventBranch.FindClusters()
		print tree.EventBranch.GetNClusters()
		self.TestHist.Fill(tree.EventBranch.GetNClusters())
		
	def Setup(self):
		ROOT.gSystem.Load('/afs/cern.ch/user/j/jfstrube/Workspace/DHCalAnalysis/dhcCode/libDhcTree.so')
		self.TestHist = ROOT.TH1F('h1', 'h1', 100, 0, 100)

	def BookHistograms(self):
		self.RegisterHistograms([self.TestHist])
		pass

