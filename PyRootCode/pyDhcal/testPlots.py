from ROOT import *

from pyDhcal.dhcalDefinitions import *

# located on pclcd05
#storagePath = '/Storage/data00/DhcalMay2012_rootTrees'
#userLibPath = '/afs/cern.ch/user/j/jstrube/public/online/lib'
#userLibs = [ userLibPath+'/libDhcTree.so' ]

#runNumber = 660134

#fileName = '%s/dhcTree-Run%s.root'%(storagePath, runNumber)

#for lib in userLibs:
#    print 'Loading %s ...'%(lib)
#    gSystem.Load( lib )

#inputFile = TFile( fileName, 'READ' )
#tree = TChain( 'DhcTree' )
#tree.Add( fileName )
#tree.Draw( 'EventBranch.fTriggerBits.fAllBits' )
#tree.Draw( 'EventBranch.nHits : EventBranch.nHitLayers >> myhist(70,0,70,300,0,300)', 'EventBranch.fTriggerBits.fAllBits == 3', 'colz' )
