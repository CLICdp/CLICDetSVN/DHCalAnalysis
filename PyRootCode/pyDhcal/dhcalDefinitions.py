from collections import defaultdict

class Enum:
    def __init__( self, name ):
        self.name = name
        self.title = 'Enum'
    
    def __eq__( self, other ):
        return self.name == other.name and self.title == other.title

    def __hash__( self ):
        return hash( self.name + self.title )

    def __str__( self ):
        return self.name
        
    def __repr__( self ):
        return '%s: %s'%(self.title, self.name)

# Module Positions
modulePositions = {}

class ModulePosition( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Module Position'
        modulePositions[name] = self
        
ModulePosition.TOP = ModulePosition( 'top' )
ModulePosition.MIDDLE = ModulePosition( 'middle' )
ModulePosition.BOTTOM = ModulePosition( 'bottom' )
ModulePosition.UNKNOWN = ModulePosition( 'unknown' )

# Front End Board Positions
frontEndBoardPositions = {}

class FrontEndBoardPosition( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Front End Board Position'
        frontEndBoardPositions[name] = self
        
FrontEndBoardPosition.LEFT = FrontEndBoardPosition( 'left' )
FrontEndBoardPosition.RIGHT = FrontEndBoardPosition( 'right' )
FrontEndBoardPosition.UNKNOWN = FrontEndBoardPosition( 'unknown' )

# Beam Polarities
polarities = {}

class Polarity( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Beam Polarity'
        polarities[name] = self
        
Polarity.POSITIVE = Polarity('+')
Polarity.NEGATIVE = Polarity('-')
Polarity.UNKNOWN = Polarity('unknown')
Polarity.ALL = Polarity('all')


# particle types
particleTypes = defaultdict( lambda : ParticleType.UNKNOWN )

class ParticleType( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Particle ID'
        particleTypes[name] = self
        
ParticleType.PION = ParticleType( '#pi' )
ParticleType.ELECTRON = ParticleType( 'e' )
ParticleType.MUON = ParticleType( '#mu' )
ParticleType.PROTON = ParticleType( 'p' )
ParticleType.UNKNOWN = ParticleType( 'unknown' )


# Run types
runTypes = defaultdict( lambda : RunType.UNKNOWN )      
        
class RunType( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Run Type'
        runTypes[name] = self
        
RunType.DATA = RunType( 'beamData' )
RunType.NOISE = RunType( 'DhcNoise' )
RunType.UNKNOWN = RunType( 'unknown' )


# Triggers
triggers = defaultdict( lambda : Trigger.UNKNOWN )

class Trigger( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Trigger'
        triggers[name] = self
        
Trigger.TEN_TEN = Trigger( '10x10' )
Trigger.THIRTY_THIRTY = Trigger( '30x30' )
Trigger.UNKNOWN = Trigger( 'unknown' )


# Beam types
beamTypes = defaultdict( lambda : BeamType.UNKNOWN )  
      
class BeamType( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Beam Type'
        beamTypes[name] = self
        
BeamType.PRIMARY = BeamType( 'primary' )
BeamType.SECONDARY = BeamType( 'secondary' )
BeamType.TERTIARY = BeamType( 'tertiary' )
BeamType.UNKNOWN = BeamType( 'unknown' )


# Absorbers
absorbers = defaultdict( lambda : Absorber.UNKNOWN )

class Absorber( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Absorber'
        absorbers[name] = self
        
Absorber.AIR = Absorber( 'air' )
Absorber.AIR_DUMP = Absorber( 'air+dump' )
Absorber.Pb6 = Absorber( '6 mm Pb' )
Absorber.Pb18 = Absorber( '18 mm Pb' )
Absorber.UNKNONW = Absorber( 'unknown' )


# Targets
targets = defaultdict( lambda : Target.UNKNOWN )

class Target( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Target'
        targets[name] = self
        
Target.AIR = Target( 'air' )
Target.Pb6 = Target( '6 mm Pb' )
Target.Pb18 = Target( '18 mm Pb' )
Target.Cu40 = Target( '40 cm Cu' )
Target.UNKNOWN = Target( 'unknown' )


# Beam stopper
beamStoppers = defaultdict( lambda : BeamStopper.UNKNOWN )

class BeamStopper( Enum ):
    def __init__( self, name ):
        Enum.__init__( self, name )
        self.title = 'Beam Stopper'
        beamStoppers[name] = self

BeamStopper.OPEN = BeamStopper( 'Open' )
BeamStopper.CLOSED = BeamStopper( 'Closed' )
BeamStopper.UNKNOWN = BeamStopper( 'unknown' )
