from run import Run

# Class to hold run list
class RunList( list ):

    def __init__( self, runList=[] ):
        list.__init__( self )
        self.addRuns( runList )
    
    @classmethod    
    def readTextFile( cls, fileName='../../DHCalRunList.txt', includeBadRuns=False ):
        runList = cls()
        print 'Reading run list from %s ...'%(fileName)
        inFile = open( fileName, 'r' )
        lines = inFile.readlines()
        fields = lines[0].rstrip().split('\t')
        nFields = len(fields)
        for line in lines[1:]:
            line = line.split( '\t' )
            if len(line) != nFields:
                continue
            line[-1] = line[-1].rstrip()
            dictionary = dict( zip(fields, line) )
            run = Run.fromDictionary( dictionary )
            if run != None:
                if includeBadRuns or not run.isBad:
                    runList.addRun( run )
        return runList

    # Convenience method for filling the dictionary
    def addRun( self, run ):
        runNumber = run.runNumber
        for myRun in self:
            if runNumber == myRun.runNumber: 
                #print 'Unable to add run %s, already in run list.'%(runNumber)
                return
        self.append(run)
        
    def addRuns( self, runList ):
        for run in runList:
            self.addRun( run )
    
    # Search the list for a run with a given run number
    def getRun( self, runNumber ):
        for run in self:
            if run.runNumber == runNumber:
                return run
        return 
                
    def getMomentumDict( self ):
        runs = {}
        for run in self:
            momentum = run.beamMomentum
            if runs.has_key( momentum ):
                runs[momentum].addRun( run )
            else:
                runs[momentum] = RunList( [run] )
        return runs
        
    
    # Some filter methods
    def getRunsByMomentum( self, momentum ):
        runList = RunList()
        for run in self:
            if isinstance(momentum, list):
                if run.beamMomentum in momentum:
                    runList.addRun( run )
            elif run.beamMomentum == momentum:
                runList.addRun( run )
        return runList
    
    def getRunsByPolarity( self, polarity ):
        runList = RunList()
        for run in self:
            if run.polarity == polarity:
                runList.addRun( run )
        return runList
    
    def getRunsByRunType( self, runType ):
        runList = RunList()
        for run in self:
            if run.runType == runType:
                runList.addRun( run )
        return runList
    
    def getRunsByTrigger( self, trigger ):
        runList = RunList()
        for run in self:
            if run.trigger == trigger:
                runList.addRun( run )
        return runList
    
    def getRunsByAbsorber( self, absorber ):
        runList = RunList()
        for run in self:
            if run.absorber == absorber:
                runList.addRun( run )
        return runList
    
    def getRunsByTarget( self, target ):
        runList = RunList()
        for run in self:
            if run.target == target:
                runList.addRun( run )
        return runList

    def getRunsByBeamType( self, beamType ):
        runList = RunList()
        for run in self:
            if run.beamType == beamType:
                runList.addRun( run )
        return runList
  
    def getGoodRuns( self ):
        runList = RunList()
        for run in self:
            if not run.isBad:
                runList.addRun( run )
        return runList
    
    # Custom print message
    def __str__( self ):
        string = '{'
        for run in self:
            string += '%s, '%(run.__str__())
        string += '}'
        return string
