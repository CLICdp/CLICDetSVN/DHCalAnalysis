import ROOT

from runList import RunList
from dhcalDefinitions import *

setupRootLibs()
ROOT.gROOT.ProcessLine( '.x ~/Root/rootstyle/CLICStyle.C' )

runList = RunList.readTextFile()

beamEnergies = [10, 20, 40, 50]
#beamEnergies = [5]

histograms = []

c = ROOT.TCanvas( 'nHits', 'nHits' )
legend = ROOT.TLegend( 0.6, 0.6, 0.9, 0.9 )

xLabel = 'nHits'
yLabel = 'Normalized entries'

polarity = Polarity.NEGATIVE

for energy in beamEnergies:
    runs = runList.getRunsByMomentum( [energy] )
    runs = runs.getRunsByPolarity( polarity )
    runs = runs.getGoodRuns()
    runs.findRootTrees( '/afs/cern.ch/user/c/cgrefe/work/public/DHCalAnalysis/root' )
    #print runs
    tree = runs.getRootTree()
    
    histName = 'hNHits_%sGeV'%(energy)
    h = ROOT.TH1D( histName, '%s:%s:%s'%(histName, xLabel, yLabel), 700, 0, 700 )
    histograms.append( h )
    legend.AddEntry( h, '%s%s GeV'%(polarity, energy), 'L' )
    command = 'nHits >> %s'%(histName)
    cut = 'fTriggerBits.fAllBits == 0 && nMaxHitsPerLayer > 10 && nHitLayers > 15'
    opt = 'goff'
    tree.Draw( command, cut , opt )

stack = ROOT.THStack()
for h in histograms:
    index = histograms.index( h )
    h.Scale( 1. / h.GetEntries() )
    h.SetLineColor( index + 1 )
    stack.Add( h )
stack.Draw( 'nostack' )
legend.Draw()
stack.GetXaxis().SetTitle( xLabel )
stack.GetYaxis().SetTitle( yLabel )
ROOT.gPad.Update()
