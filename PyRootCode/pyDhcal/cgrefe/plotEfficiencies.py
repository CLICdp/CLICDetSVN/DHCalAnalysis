#from pyDhcal import dhcalMapping
#from ROOT import TFile, TTree, TH1D, TH2D, TEfficiency, TGraphErrors
from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import RunType, ModulePosition
from array import array
import sys, os, argparse

class EfficiencyPlotter:
    def __init__( self ):
        self.plots = {}
        self.runNumber = 0
        self.outputFileName = ''
        self.outputRootFile = None
        self.inputFileName = ''
        self.inputRootFile = None
        self.basePlot = 'CellID'
        self.basePlots = [ 'CellID', 'CellID_Cluster', 'Position' ]
        self.minEfficiency = 0.5
        self.fishingLineBins = [ 7, 8, 13, 14, 19, 20, 25, 26, 38, 39, 44, 45, 51, 52, 57, 58, 70, 71, 76, 77, 82, 83, 88, 89 ]
        self.centralBins = [ 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54 ]
        self.moduleBoundaryMargin = 6
        self.efficiencyAxis = ( 'Efficiency', 0., 1. )
        self.multiplicityAxis = ( 'Multiplicity', 1., 3. )
        self.entriesAxis = ('Entries', None, None)
        self.moduleIdDict = {ModulePosition.BOTTOM : 0, ModulePosition.MIDDLE: 1, ModulePosition.TOP : 2}
    
    def openFiles( self ):
        from ROOT import TFile
        if not self.inputRootFile or not self.inputRootFile.IsOpen():
            if not os.path.exists( self.inputFileName ):
                print 'Input file does not exist: %s' % ( self.inputFileName )
                return False
            self.inputRootFile = TFile.Open( self.inputFileName )
            if not self.inputRootFile.IsOpen():
                print 'Unable to open input file: %s' % self.inputFileName
                return False
        if not self.outputRootFile or not self.outputRootFile.IsOpen():
            self.outputRootFile = TFile.Open( self.outputFileName, 'recreate' )
            if not self.outputRootFile.IsOpen():
                print 'Unable to open output file: %s' % self.outputFileName
                sys.exit( 2 )
        return True
            
    def addBranches( self, tree, newBranches=True ):
        for t, variableNames in self.variableNames.items():
            for variableName in variableNames:
                varArray = array( t, [0] )
                self.variables[variableName] = varArray[0]
                if newBranches:
                    tree.Branch( variableName, varArray, '%s/%s' % (variableName, t.upper()) )
                else:
                    tree.SetBranchAddress( variableName, varArray )
            
    def closeFiles( self ):
        if self.inputRootFile and self.inputRootFile.IsOpen():
            self.inputRootFile.Close()
        if self.outputRootFile and self.outputRootFile.IsOpen():
            plotPaths = self.plots.keys();
            plotPaths.sort()
            for plotPath in plotPaths:
                plot = self.plots[plotPath]
                name = os.path.basename(plotPath)
                directory = os.path.dirname(plotPath)
                if not self.outputRootFile.GetDirectory( directory ):
                    self.outputRootFile.mkdir( directory )
                self.outputRootFile.cd( directory )
                plot.Write( name )
            self.outputRootFile.Close()

    def createEfficiencyGraph( self, name, xAxis, yAxis ):
        from ROOT import TEfficiency
        xTitle, xBins, xMin, xMax = xAxis
        yTitle, yMin, yMax = yAxis
        h = TEfficiency( 'e' + name, '%s;%s;%s' % (name, xTitle, yTitle), xBins, xMin, xMax )
        return h

    def createHistogram1D( self, name, xAxis, yAxis ):
        from ROOT import TH1D
        xTitle, xBins, xMin, xMax = xAxis
        yTitle, yMin, yMax = yAxis
        h = TH1D( 'h' + name, '%s;%s;%s' % (name, xTitle, yTitle), xBins, xMin, xMax )
        if yMin:
            h.SetMinimum( yMin )
        if yMax:
            h.SetMaximum( yMax )
        return h

    def createHistogram2D( self, name, xAxis, yAxis, zAxis ):
        from ROOT import TH2D
        xTitle, xBins, xMin, xMax = xAxis
        yTitle, yBins, yMin, yMax = yAxis
        zTitle, zMin, zMax = zAxis
        h = TH2D( 'h' + name, '%s;%s;%s;%s' % (name, xTitle, yTitle, zTitle), xBins, xMin, xMax, yBins, yMin, yMax )
        if zMin:
            h.SetMinimum( zMin )
        if zMax:
            h.SetMaximum( zMax )
        return h
    
    def extractProjection( self, clusterDict, index, gEfficiency, gMultiplicity):
        if clusterDict.has_key(index):
            h = clusterDict[index]
            gEfficiency.SetTotalEvents( index, int(h.GetEntries()) )
            gEfficiency.SetPassedEvents( index, int(h.GetEntries() - h.GetBinContent(1)) )
            h.GetXaxis().SetRange( 2, h.GetXaxis().GetLast() )
            n = gMultiplicity.GetN() + 1
            gMultiplicity.Set( n )
            gMultiplicity.SetPoint( n, index, h.GetMean() )
            gMultiplicity.SetPointError( n, 0., h.GetMeanError() )
            
    def addToHistogramDict( self, dictionary, key, object, cloneName ):
        if not dictionary.has_key( key ):
            dictionary[key] = object.Clone( cloneName )
        else:
            dictionary[key].Add( object )
        
    def processLayer( self, layerID ):
        from ROOT import TGraphErrors
        from pyDhcal import dhcalMapping
        
        layerName = 'Layer%d' % layerID
        layerHistName = 'ClusterSize_vs_%s_%s' % ( self.basePlot, layerName)
        layerHist = self.inputRootFile.Get( layerHistName )
        lastBinZ = layerHist.GetZaxis().GetLast()
        
        layer = dhcalMapping.layerMap[layerID]
        
        xAxis = ( layerHist.GetXaxis().GetTitle(), layerHist.GetXaxis().GetNbins(), layerHist.GetXaxis().GetXmin(), layerHist.GetXaxis().GetXmax() )
        yAxis = ( layerHist.GetYaxis().GetTitle(), layerHist.GetYaxis().GetNbins(), layerHist.GetYaxis().GetXmin(), layerHist.GetYaxis().GetXmax() )
        zAxis = ( layerHist.GetZaxis().GetTitle(), layerHist.GetZaxis().GetNbins(), layerHist.GetZaxis().GetXmin(), layerHist.GetZaxis().GetXmax() )
        
        name = layerName + '_Entries'
        hEntries = self.createHistogram2D( name, xAxis, yAxis, self.entriesAxis )
        self.plots['%s/%s' % (layerName, name)] = hEntries
        
        name = layerName + '_Efficiency'
        hEfficiency = self.createHistogram2D( name, xAxis, yAxis, self.efficiencyAxis )
        gEfficiencyX = self.createEfficiencyGraph( name + 'X', xAxis, self.efficiencyAxis )
        gEfficiencyY = self.createEfficiencyGraph( name + 'Y', yAxis, self.efficiencyAxis )
        self.plots['%s/%s' % (layerName, name)] = hEfficiency
        self.plots['%s/%sX' % (layerName, name)] = gEfficiencyX
        self.plots['%s/%sY' % (layerName, name)] = gEfficiencyY
        
        name = layerName + '_Cleaned_Efficiency'
        hEfficiencyCleaned = self.createHistogram2D( name, xAxis, yAxis, self.efficiencyAxis )
        gEfficiencyCleanedX = self.createEfficiencyGraph( name + 'X', xAxis, self.efficiencyAxis )
        gEfficiencyCleanedY = self.createEfficiencyGraph( name + 'Y', yAxis, self.efficiencyAxis )
        self.plots['%s/%s' % (layerName, name)] = hEfficiencyCleaned
        self.plots['%s/%sX' % (layerName, name)] = gEfficiencyCleanedX
        self.plots['%s/%sY' % (layerName, name)] = gEfficiencyCleanedY
        
        name = layerName + '_Final_Efficiency'
        hEfficiencyFinal = self.createHistogram2D( name, xAxis, yAxis, self.efficiencyAxis )
        gEfficiencyFinalX = self.createEfficiencyGraph( name + 'X', xAxis, self.efficiencyAxis )
        gEfficiencyFinalY = self.createEfficiencyGraph( name + 'Y', yAxis, self.efficiencyAxis )
        self.plots['%s/%s' % (layerName, name)] = hEfficiencyFinal
        self.plots['%s/%sX' % (layerName, name)] = gEfficiencyFinalX
        self.plots['%s/%sY' % (layerName, name)] = gEfficiencyFinalY
        
        name = layerName + '_Multiplicity'
        hMultiplicity = self.createHistogram2D( name, xAxis, yAxis, self.multiplicityAxis )
        self.plots['%s/%s' % (layerName, name)] = hMultiplicity
        gMultiplicityX = TGraphErrors()
        gMultiplicityX.SetNameTitle( 'g' + name + 'X', '%s;%s;%s' % (name + 'X', xAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sX' % (layerName, name)] = gMultiplicityX
        gMultiplicityY = TGraphErrors()
        gMultiplicityY.SetNameTitle( 'g' + name + 'Y', '%s;%s;%s' % (name + 'Y', yAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sY' % (layerName, name)] = gMultiplicityY
        
        name = layerName + '_Cleaned_Multiplicity'
        hMultiplicityCleaned = self.createHistogram2D( name, xAxis, yAxis, self.multiplicityAxis )
        self.plots['%s/%s' % (layerName, name)] = hMultiplicityCleaned
        gMultiplicityCleanedX = TGraphErrors()
        gMultiplicityCleanedX.SetNameTitle( 'g' + name + 'X', '%s;%s;%s' % (name + 'X', xAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sX' % (layerName, name)] = gMultiplicityCleanedX
        gMultiplicityCleanedY = TGraphErrors()
        gMultiplicityCleanedY.SetNameTitle( 'g' + name + 'Y', '%s;%s;%s' % (name + 'Y', yAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sY' % (layerName, name)] = gMultiplicityCleanedY
        
        name = layerName + '_Final_Multiplicity'
        hMultiplicityFinal = self.createHistogram2D( name, xAxis, yAxis, self.multiplicityAxis )
        self.plots['%s/%s' % (layerName, name)] = hMultiplicityFinal
        gMultiplicityFinalX = TGraphErrors()
        gMultiplicityFinalX.SetNameTitle( 'g' + name + 'X', '%s;%s;%s' % (name + 'X', xAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sX' % (layerName, name)] = gMultiplicityFinalX
        gMultiplicityFinalY = TGraphErrors()
        gMultiplicityFinalY.SetNameTitle( 'g' + name + 'Y', '%s;%s;%s' % (name + 'Y', yAxis[0], self.multiplicityAxis[0]) )
        self.plots['%s/%sY' % (layerName, name)] = gMultiplicityFinalY
        
        hClusterX = {}
        hClusterY = {}
        
        hClusterCleanedX = {}
        hClusterCleanedY = {}
        
        hClusterFinalX = {}
        hClusterFinalY = {}
        
        for moduleID, module in layer.modules.items():
            moduleName = str(moduleID).title()
            
            name = '%s_%s_ClusterSize' % (layerName, moduleName)
            hClusterSize = self.createHistogram1D( name, zAxis, ('Entries', None, None) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = hClusterSize
            
            name = '%s_%s_Cleaned_ClusterSize' % (layerName, moduleName)
            hClusterSizeCleaned = self.createHistogram1D( name, zAxis, ('Entries', None, None) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = hClusterSizeCleaned
            
            name = '%s_%s_Final_ClusterSize' % (layerName, moduleName)
            hClusterSizeFinal = self.createHistogram1D( name, zAxis, ('Entries', None, None) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = hClusterSizeFinal            
            
            name = '%s_%s_EfficiencyX' % (layerName, moduleName)
            gEfficiencyModuleX = self.createEfficiencyGraph( name, xAxis, self.efficiencyAxis )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gEfficiencyModuleX
            
            name = '%s_%s_EfficiencyCleanedX' % (layerName, moduleName)
            gEfficiencyModuleCleanedX = self.createEfficiencyGraph( name, xAxis, self.efficiencyAxis )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gEfficiencyModuleCleanedX
            
            name = '%s_%s_EfficiencyFinalX' % (layerName, moduleName)
            gEfficiencyModuleFinalX = self.createEfficiencyGraph( name, xAxis, self.efficiencyAxis )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gEfficiencyModuleFinalX
            
            name = '%s_%s_MultiplicityX' % (layerName, moduleName)
            gMultiplicityModuleX = TGraphErrors()
            gMultiplicityModuleX.SetNameTitle( 'g' + name, '%s;%s;%s' % (name, xAxis[0], self.multiplicityAxis[0]) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gMultiplicityModuleX
            
            name = '%s_%s_MultiplicityCleanedX' % (layerName, moduleName)
            gMultiplicityModuleCleanedX = TGraphErrors()
            gMultiplicityModuleCleanedX.SetNameTitle( 'g' + name, '%s;%s;%s' % (name, xAxis[0], self.multiplicityAxis[0]) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gMultiplicityModuleCleanedX
            
            name = '%s_%s_MultiplicityFinalX' % (layerName, moduleName)
            gMultiplicityModuleFinalX = TGraphErrors()
            gMultiplicityModuleFinalX.SetNameTitle( 'g' + name, '%s;%s;%s' % (name, xAxis[0], self.multiplicityAxis[0]) )
            self.plots['%s/%s/%s' % (layerName, moduleName, name)] = gMultiplicityModuleFinalX
            
            deadCells = []
            hClusterModuleX = {}
            hClusterModuleCleanedX = {}
            hClusterModuleFinalX = {}
            for j in xrange(module.vMin + 1, module.vMax + 1):
                for i in xrange(module.uMin + 1, module.uMax + 1):
                    hCluster = layerHist.ProjectionZ( '_pz', i, i+1, j, j+1 )
                    nTotal = hCluster.GetEntries()
                    if not nTotal > 0:
                        continue
                    self.addToHistogramDict( hClusterModuleX, i, hCluster, 'hClusterModuleX_%d' % i )
                    self.addToHistogramDict( hClusterX, i, hCluster, 'hClusterX_%d' % i )
                    self.addToHistogramDict( hClusterY, j, hCluster, 'hClusterY_%d' % j )
                    nNotReo = hCluster.GetBinContent( 1 )
                    nReco = nTotal - nNotReo
                    hEntries.SetBinContent( i, j, nTotal )
                    hClusterSize.Add( hCluster )
                    efficiency = nReco / nTotal
                    hCluster.GetXaxis().SetRange( 2, lastBinZ )
                    multiplicity = hCluster.GetMean()
                    hEfficiency.SetBinContent( i, j, efficiency )
                    hMultiplicity.SetBinContent( i, j, multiplicity )
                    if efficiency < self.minEfficiency:
                        for ii in xrange(i-1, i+2):
                            for jj in xrange(j-1, j+2):
                                deadCells.append( (ii, jj) )
            for j in xrange(module.vMin + self.moduleBoundaryMargin + 1, module.vMax - self.moduleBoundaryMargin + 1):
                if j in self.fishingLineBins:
                    continue
                for i in xrange(module.uMin + self.moduleBoundaryMargin + 1, module.uMax - self.moduleBoundaryMargin + 1):
                    if (i, j) in deadCells:
                        continue
                    hCluster = layerHist.ProjectionZ( '_pz', i, i+1, j, j+1 )
                    nTotal = hCluster.GetEntries()
                    if not nTotal > 0:
                        continue
                    self.addToHistogramDict( hClusterModuleCleanedX, i, hCluster, 'hClusterModuleCleanedX_%d' % i )
                    self.addToHistogramDict( hClusterCleanedX, i, hCluster, 'hClusterCleanedX_%d' % i )
                    self.addToHistogramDict( hClusterCleanedY, j, hCluster, 'hClusterCleanedY_%d' % j )
                    nNotReo = hCluster.GetBinContent( 1 )
                    nReco = nTotal - nNotReo
                    hClusterSizeCleaned.Add( hCluster )
                    efficiency = nReco / nTotal
                    hCluster.GetXaxis().SetRange( 2, lastBinZ )
                    multiplicity = hCluster.GetMean()
                    hEfficiencyCleaned.SetBinContent( i, j, efficiency )
                    hMultiplicityCleaned.SetBinContent( i, j, multiplicity )
                    if i in self.centralBins:
                        continue
                    self.addToHistogramDict( hClusterModuleFinalX, i, hCluster, 'hClusterModuleFinalX_%d' % i )
                    self.addToHistogramDict( hClusterFinalX, i, hCluster, 'hClusterFinalX_%d' % i )
                    self.addToHistogramDict( hClusterFinalY, j, hCluster, 'hClusterFinalY_%d' % j )
                    hClusterSizeFinal.Add( hCluster )
                    hEfficiencyFinal.SetBinContent( i, j, efficiency )
                    hMultiplicityFinal.SetBinContent( i, j, multiplicity )
            for i in xrange(module.uMin + 1, module.uMax + 1):
                self.extractProjection(hClusterModuleX, i, gEfficiencyModuleX, gMultiplicityModuleX)
                self.extractProjection(hClusterModuleCleanedX, i, gEfficiencyModuleCleanedX, gMultiplicityModuleCleanedX)
                self.extractProjection(hClusterModuleFinalX, i, gEfficiencyModuleFinalX, gMultiplicityModuleFinalX)
            for j in xrange(module.vMin + 1, module.vMax + 1):
                self.extractProjection(hClusterY, j, gEfficiencyY, gMultiplicityY)
                self.extractProjection(hClusterCleanedY, j, gEfficiencyCleanedY, gMultiplicityCleanedY)
                self.extractProjection(hClusterFinalY, j, gEfficiencyFinalY, gMultiplicityFinalY)
        for i in xrange(module.uMin + 1, module.uMax + 1):
            self.extractProjection(hClusterX, i, gEfficiencyX, gMultiplicityX)
            self.extractProjection(hClusterCleanedX, i, gEfficiencyCleanedX, gMultiplicityCleanedX)
            self.extractProjection(hClusterFinalX, i, gEfficiencyFinalX, gMultiplicityFinalX)
        return
    
    def process( self, layers ):
        if not self.openFiles():
            return
        print 'Processing %s' % self.inputFileName
        for layerID in layers:
            sys.stdout.write( '\r\tProcessing layer: %d' % layerID )
            sys.stdout.flush()
            self.processLayer( layerID )
        sys.stdout.write( '\n' )
        self.closeFiles()
        print 'Created %s' % self.outputFileName

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extracts efficiency and multiplicity histograms from base histograms created by LayerEfficiencyProcessor.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-a', '--all', action='store_true', help='Processes all runs in the run list file')
    group.add_argument('-m', '--muons', action='store_true', help='Use the combined muon runs file')
    group.add_argument('-r', '--runNumber', type=int, help='Run number (builds the input and output file names automatically)')
    group.add_argument('-i', '--inputFile', default='', help='Name of the input file')
    parser.add_argument('-R', '--runList', default='/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt', help='Run list file')
    parser.add_argument('-I', '--inputPath', default='/afs/cern.ch/work/cgrefe/public/DHCAL/root/LayerEfficiency_2014-02-27', help='Base path for input files')
    parser.add_argument('-O', '--outputPath', default='', help='Base path for output files')
    parser.add_argument('-o', '--outputFile', default='output.root', help='Name of the output file')
    args = parser.parse_args()
    
    from pyDhcal import dhcalMapping
    layers = xrange(dhcalMapping.nLayers)
    
    plotter = EfficiencyPlotter()
    if args.muons:
        plotter.inputFileName = os.path.join( args.inputPath, 'LayerEfficiency_muons.root' )
        plotter.outputFileName = os.path.join( args.outputPath, 'CalibrationHistograms_muons.root' )
        plotter.process( layers )
    elif args.runNumber:
        runList = RunList.readTextFile( args.runList ).getRunsByRunType( RunType.DATA )
        run = runList.getRun( args.runNumber )
        if not run:
            print 'Run %d is not a valid run number' % run.runNumber
            sys.exit( 2 )
        plotter.inputFileName = os.path.join( args.inputPath, 'LayerEfficiency_%d.root' % run.runNumber )
        plotter.outputFileName = os.path.join( args.outputPath, 'CalibrationHistograms_%d.root' % run.runNumber )
        plotter.process( layers )
    elif args.all:
        runList = RunList.readTextFile( args.runList ).getRunsByRunType( RunType.DATA )
        for run in runList:
            plotter.inputFileName = os.path.join( args.inputPath, 'LayerEfficiency_%d.root' % run.runNumber )
            plotter.outputFileName = os.path.join( args.outputPath, 'CalibrationHistograms_%d.root' % run.runNumber )
            plotter.process( layers )
    elif args.inputFile:
        plotter.inputFileName = args.inputFile
        plotter.outputFileName = args.outputFile
        plotter.process( layers )
        