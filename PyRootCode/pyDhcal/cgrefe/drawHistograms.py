from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import *
from cgrefe.tools.Plot import Plot, Plot2D
from ROOT import TFile, TTree, TH1D, TObject
import os, sys, ROOT

basePath = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/'
rootBasePath = os.path.join( basePath, 'root' )
rootPath = os.path.join( rootBasePath, 'EventVariables' )
rootTreeName = 'DhcEventVariables'

pidRootPath = os.path.join( rootBasePath, 'ParticleID' )
pidRootTreeName = 'DhcParticleID'

linearizedRootPath = os.path.join( rootBasePath, 'Linearized' )
linearizedRootTreeName = 'DhcLinearized'

plotPath = os.path.join ( basePath, 'plots', 'variables' )
histogramPath = os.path.join ( basePath, 'histograms' )

def setupPlot( run, variable, plotTitle, cut='' ):
    runNumber = run.runNumber
    plot = Plot( variable, '', plotTitle )
    eventVariablesFile = os.path.join(rootPath, 'EventVariables_%d.root'%(runNumber))
    particleIdFile = os.path.join(pidRootPath, 'ParticleID_%d.root'%(runNumber))
    linearizedFile = os.path.join(linearizedRootPath, 'Linearized_%d.root'%(runNumber))
    if not os.path.exists( eventVariablesFile ) or not os.path.exists(particleIdFile):
        return False, False
    if not os.path.exists( linearizedFile ) and linearizedRootTreeName in variable:
        print 'Error missing linearized tree'
    plot.addFile( eventVariablesFile, rootTreeName, '', [( particleIdFile, pidRootTreeName )] )
    plot.setLegendTitle( 'Run %s, %s%d GeV'%(runNumber, run.polarity, run.beamMomentum) )
    plot.drawStats = False
    plot.setCutStyle( color = True )
    myCut = 'hasBox == 0'
    if cut:
        myCut += ' && %s'%(cut)
    plot.style.colors = []
    if not ':' in variable:
        plot.addCut( myCut, 'All' )
        plot.style.colors.append( ROOT.kBlack )
        plot.addCut( '%s && %s.electron'%(myCut, pidRootTreeName), 'e' )
        plot.style.colors.append( ROOT.kRed )
    plot.addCut( '%s && %s.muon'%(myCut, pidRootTreeName), '#mu' )
    plot.style.colors.append( ROOT.kBlue )
    plot.addCut( '%s && %s.pion'%(myCut, pidRootTreeName), '#pi' )
    plot.style.colors.append( ROOT.kGreen + 2 )
    if run.polarity == Polarity.POSITIVE and not ':' in variable:
        plot.addCut( '%s && %s.proton'%(myCut, pidRootTreeName), 'p' )
        plot.style.colors.append( ROOT.kViolet+2 )
    return plotTitle, plot

def drawNHits( run ):
    plotTitle, plot = setupPlot( run, 'nHits', 'nHits' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Hits', 'Events' )
    plot.setBinning( 1000, 0, 1000, False )
    plot.create()
    return plotTitle, plot

def drawNHitsLinearized( run ):
    plotTitle, plot = setupPlot( run, '%s.nHits'%(linearizedRootTreeName), 'nHitsLinearized' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Hits', 'Events' )
    plot.setBinning( 1000, 0, 1000, False )
    plot.create()
    return plotTitle, plot

def drawEnergy( run ):
    plotTitle, plot = setupPlot( run, 'energy', 'energy' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Calibrated Hits Linearized', 'Events' )
    plot.setBinning( 1000, 0, 1000, False )
    plot.create()
    return plotTitle, plot

def drawEnergyLinearized( run ):
    plotTitle, plot = setupPlot( run, '%s.energy'%(linearizedRootTreeName), 'energyLinearized' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Calibrated Hits Linearized', 'Events' )
    plot.setBinning( 1000, 0, 1000, False )
    plot.create()
    return plotTitle, plot

def drawNHitsEarly( run ):
    plotTitle, plot = setupPlot( run, 'nHits', 'nHitsEarly', 'interactionLayer < 10' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Hits', 'Events' )
    plot.setBinning( 6000, 0, 6000, False )
    plot.create()
    return plotTitle, plot

def drawEnergyEarly( run ):
    plotTitle, plot = setupPlot( run, 'energy', 'energyEarly' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Calibrated Hits', 'Events' )
    plot.setBinning( 1000, 0, 1000, False )
    plot.create()
    return plotTitle, plot

def drawInteractionLayer( run ):
    plotTitle, plot = setupPlot( run, 'interactionLayer', 'interactionLayer' )
    if not plot:
        return False
    plot.setBinning( 54, 0, 54, False )
    plot.setTitle( 1, plotTitle, 'Interaction Layer', 'Events' )
    plot.create()
    return plotTitle, plot

def drawNHitsProfile( run ):
    plotTitle, plot = setupPlot( run, 'layerHits : layerNumber', 'layerHitsProfile' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer', 'Mean Hits' )
    plot.setBinning( 54, 0, 54, False )
    plot.create()
    return plotTitle, plot

def drawNHitsProfileCorrected( run ):
    plotTitle, plot = setupPlot( run, 'layerHits : layerNumber - interactionLayer', 'layerHitsProfileCorrected' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer - InteractionLayer', 'Mean Hits' )
    plot.setBinning( 54, -10, 44, False )
    plot.create()
    return plotTitle, plot

def drawEnergyProfile( run ):
    plotTitle, plot = setupPlot( run, 'layerEnergy : layerNumber', 'layerEnergyProfile' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer', 'Mean Calibrated Hits' )
    plot.setBinning( 54, 0, 54, False )
    plot.create()
    return plotTitle, plot

def drawEnergyProfileCorrected( run ):
    plotTitle, plot = setupPlot( run, 'layerEnergy : layerNumber - interactionLayer', 'layerEnergyProfileCorrected' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer - InteractionLayer', 'Mean Calibrated Hits' )
    plot.setBinning( 54, -10, 44, False )
    plot.create()
    return plotTitle, plot

def drawShowerProfile( run ):
    plotTitle, plot = setupPlot( run, 'layerRmsXY : layerNumber', 'layerShowerProfile' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer', 'Mean Shower Radius' )
    plot.setBinning( 54, 0, 54, False )
    plot.create()
    return plotTitle, plot

def drawShowerProfileCorrected( run ):
    plotTitle, plot = setupPlot( run, 'layerRmsXY : layerNumber - interactionLayer', 'layerShowerProfileCorrected' )
    if not plot:
        return False
    plot.setTitle( 1, plotTitle, 'Layer - InteractionLayer', 'Mean Shower Radius' )
    plot.setBinning( 54, -10, 44, False )
    plot.create()
    return plotTitle, plot

def drawBaryCenter( run ):
    plotTitle, plot = setupPlot( run, 'centerLayer', 'centerLayer' )
    if not plot:
        return False
    plot.setBinning( 54, 0, 54, False )
    plot.setTitle( 1, plotTitle, 'Central Layer', 'Events' )
    plot.create()
    return plotTitle, plot

def drawShowerRadius( run ):
    plotTitle, plot = setupPlot( run, 'rmsXY', 'showerRadius' )
    if not plot:
        return False
    plot.setBinning( 100, 0, 300, False )
    plot.setTitle( 1, plotTitle, 'Shower RMS [mm]', 'Events' )
    plot.create()
    return plotTitle, plot

def drawDensity( run ):
    plotTitle, plot = setupPlot( run, 'hitDensity', 'hitDensity' )
    if not plot:
        return False
    plot.setBinning( 100, 0, 50, False )
    plot.setTitle( 1, plotTitle, 'Density', 'Events' )
    plot.create()
    return plotTitle, plot

def drawCerenkovCounter( run ):
    plotTitle, plot = setupPlot( run, 'cerenkovB*cerenkovA', 'cerenkovProduct' )
    if not plot:
        return False
    plot.setBinning( 2, 0, 2, False )
    plot.setTitle( 1, plotTitle, 'CerenkovA * CerenkovB', 'Events' )
    plot.create()
    return plotTitle, plot

def drawCerenkovCounterSum( run ):
    plotTitle, plot = setupPlot( run, 'cerenkovA+cerenkovB', 'cerenkovSum' )
    if not plot:
        return False
    plot.setBinning( 3, 0, 3, False )
    plot.setTitle( 1, plotTitle, 'CerenkovA + CerenkovB', 'Events' )
    plot.create()
    return plotTitle, plot

def drawAll( run ):
    plots = []
    #plots.append( drawNHitsProfile( run ) )
    plots.append( drawNHitsProfileCorrected( run ) )
    #plots.append( drawEnergyProfile( run ) )
    plots.append( drawEnergyProfileCorrected( run ) )
    #plots.append( drawShowerProfile( run ) )
    #plots.append( drawShowerProfileCorrected( run ) )
    plots.append( drawNHits( run ) )
    plots.append( drawNHitsEarly( run ) )
    plots.append( drawEnergy( run ) )
    plots.append( drawEnergyEarly( run ) )
    plots.append( drawInteractionLayer( run ) )
    plots.append( drawBaryCenter( run ) )
    plots.append( drawShowerRadius( run ) )
    plots.append( drawDensity( run ) )
    #plots.append( drawCerenkovCounter( run ) )
    #plots.append( drawCerenkovCounterSum( run ) )
    
    if not plots[0]:
        return
    
    runNumber = run.runNumber
    rootFile = TFile.Open( os.path.join( histogramPath, 'histograms_%d.root'%( runNumber ) ), 'UPDATE' )
    for plotName, plot in plots:
        plot.saveAs( os.path.join(plotPath, '%s_%d.pdf'%(plotName, runNumber)) )
        if not rootFile.cd( plotName ):
            rootFile.mkdir( plotName ).cd()
        for legend, histogram in plot.getHistograms().items():
            histogram.Write( legend, TObject.kOverwrite )
        plot.close()
        del plot
    rootFile.Close()
    
if __name__ == '__main__':
    runListFile = '/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt'
    runList = RunList.readTextFile( runListFile )
    runList = runList.getRunsByRunType( RunType.DATA )
    
    if len(sys.argv) < 2:
        for run in runList:
            if run.runNumber < 660229:
                continue
            drawAll( run )
    else:
        runNumber = int(sys.argv[1])
        run = runList.getRun( runNumber )
        if run:
            drawAll( run )
        else:
            print 'Run number %d not in %s'%(runNumber, runListFile)
    