from ROOT import TFile, TTree, TEfficiency
from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import RunType, ModulePosition
from pyDhcal.dhcalMapping import layerMap
from array import array
import os, sys, argparse

class CalibrationTreeBuilder():
    
    def __init__( self ):
        self.inputBasePath = ''
        self.inputFile = None
        self.outputFileName = ''
        self.outputFile = None
        self.runList = None
        self.rootTree = None
        self.rootTreeName = ''
        self.moduleDict = { ModulePosition.TOP : 0,
                            ModulePosition.MIDDLE : 1,
                            ModulePosition.BOTTOM : 2 }
        self.variables = {}
        self.variableNames = { 'i' : ['runNumber', 'layer', 'module', 'entries', 'cleaned_entries', 'raw_entries'],
                               'f' : ['temperature','pressure','momentum',
                                      'efficiency', 'efficiencyErrLo', 'efficiencyErrHi',
                                      'multiplicity', 'multiplicityErrLo', 'multiplicityErrHi',
                                      'cleaned_efficiency', 'cleaned_efficiencyErrLo', 'cleaned_efficiencyErrHi',
                                      'cleaned_multiplicity', 'cleaned_multiplicityErrLo', 'cleaned_multiplicityErrHi',
                                      'raw_efficiency', 'raw_efficiencyErrLo', 'raw_efficiencyErrHi',
                                      'raw_multiplicity', 'raw_multiplicityErrLo', 'raw_multiplicityErrHi']
                             }
    
    def openInputFile( self, runNumber ):
        fileName = os.path.join( self.inputBasePath, 'CalibrationHistograms_%d.root' % runNumber )
        if not os.path.isfile( fileName ):
            return False
        self.inputFile = TFile.Open( fileName )
        if self.inputFile.IsOpen():
            return True
        return False
    
    def openTreeFile( self ):
        self.outputFile = TFile.Open( self.outputFileName, 'recreate' )
        if not self.outputFile.IsOpen():
            print 'Unable to open output file: %s' % self.outputFileName
            sys.exit( 2 )
        self.rootTree = TTree( self.rootTreeName, self.rootTreeName )
        for t, variableNames in self.variableNames.items():
            for variableName in variableNames:
                varArray = array( t, [0] )
                self.variables[variableName] = varArray
                self.rootTree.Branch( variableName, varArray, '%s/%s' % (variableName, t.upper()) )
    
    def closeInputFile( self ):
        if self.inputFile and self.inputFile.IsOpen():
            self.inputFile.Close()
    
    def closeTreeFile( self ):
        self.outputFile.cd()
        self.rootTree.Write()
        self.outputFile.Close()
    
    def buildTree( self ):
        self.openTreeFile()
        for run in self.runList:
            if not self.openInputFile( run.runNumber ):
                continue
            self.variables['runNumber'][0] = run.runNumber
            self.variables['temperature'][0] = run.temperature
            self.variables['pressure'][0] = run.pressure
            self.variables['momentum'][0] = run.beamMomentum
            for layerID, layer in layerMap.items():
                self.variables['layer'][0] = layerID
                self.treatLayer( layer )
            self.closeInputFile()
        self.closeTreeFile()
        
    def treatLayer( self, layer ):
        layerName = 'Layer%d' % layer.id
        for moduleID in layer.modules:
            moduleName = str(moduleID).title()
            self.variables['module'][0] = self.moduleDict[moduleID]
            
            hist = self.inputFile.Get( '%s/%s/%s_%s_Final_ClusterSize' % ( layerName, moduleName, layerName, moduleName ) )
            nTotal = hist.GetEntries()
            self.variables['entries'][0] = int(nTotal)
            nReconstructed = nTotal - hist.GetBinContent( 1 )
            efficiency = 0.
            if nTotal > 0:
                efficiency = nReconstructed / nTotal            
            self.variables['efficiency'][0] = efficiency
            self.variables['efficiencyErrHi'][0] = TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, True ) - efficiency
            self.variables['efficiencyErrLo'][0] = efficiency - TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, False )
            hist.GetXaxis().SetRange( 2, hist.GetXaxis().GetLast() )
            self.variables['multiplicity'][0] = hist.GetMean()
            self.variables['multiplicityErrHi'][0] = hist.GetMeanError()
            self.variables['multiplicityErrLo'][0] = hist.GetMeanError()
            
            hist = self.inputFile.Get( '%s/%s/%s_%s_Cleaned_ClusterSize' % ( layerName, moduleName, layerName, moduleName ) )
            nTotal = hist.GetEntries()
            self.variables['cleaned_entries'][0] = int(nTotal)
            nReconstructed = nTotal - hist.GetBinContent( 1 )
            efficiency = 0.
            if nTotal > 0:
                efficiency = nReconstructed / nTotal            
            self.variables['cleaned_efficiency'][0] = efficiency
            self.variables['cleaned_efficiencyErrHi'][0] = TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, True ) - efficiency
            self.variables['cleaned_efficiencyErrLo'][0] = efficiency - TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, False )
            hist.GetXaxis().SetRange( 2, hist.GetXaxis().GetLast() )
            self.variables['cleaned_multiplicity'][0] = hist.GetMean()
            self.variables['cleaned_multiplicityErrHi'][0] = hist.GetMeanError()
            self.variables['cleaned_multiplicityErrLo'][0] = hist.GetMeanError()
            
            hist = self.inputFile.Get( '%s/%s/%s_%s_ClusterSize' % ( layerName, moduleName, layerName, moduleName ) )
            nTotal = hist.GetEntries()
            self.variables['raw_entries'][0] = int(nTotal)
            nReconstructed = nTotal - hist.GetBinContent( 1 )
            efficiency = 0.
            if nTotal > 0:
                efficiency = nReconstructed / nTotal            
            self.variables['raw_efficiency'][0] = efficiency
            self.variables['raw_efficiencyErrHi'][0] = TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, True ) - efficiency
            self.variables['raw_efficiencyErrLo'][0] = efficiency - TEfficiency.ClopperPearson( int(nTotal), int(nReconstructed), 0.95, False )
            hist.GetXaxis().SetRange( 2, hist.GetXaxis().GetLast() )
            self.variables['raw_multiplicity'][0] = hist.GetMean()
            self.variables['raw_multiplicityErrHi'][0] = hist.GetMeanError()
            self.variables['raw_multiplicityErrLo'][0] = hist.GetMeanError()
            
            self.rootTree.Fill()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creates a calibration ROOT tree from calibration histograms') 
    parser.add_argument('-i', '--inputPath', default='/Users/cgrefe/Work/DHCAL/CalibrationHistograms/', help='Path for input files' )
    parser.add_argument('-o', '--outputFile', default='Calibration.root', help='Name of the output file')
    parser.add_argument('-t', '--treeName', default='DhcalCalibration', help='Name of the ROOT tree')
    args = parser.parse_args()
    
    builder = CalibrationTreeBuilder()
    builder.runList = RunList.readTextFile('/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt').getRunsByRunType( RunType.DATA )
    builder.inputBasePath = args.inputPath
    builder.outputFileName = args.outputFile
    builder.rootTreeName = args.treeName
    
    builder.buildTree()
    