#from ROOT import TFile, SetOwnership, TH1D
from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import RunType, Trigger, Polarity
import os, sys, argparse


class AverageEfficiencyCalculator():
    
    def __init__( self ):
        from ROOT import TH1D
        self.inputFileName = ''
        self.inputRootFile = None
        self.type = ''
        self.debug = False
        self.clusterHistogram = TH1D('ClusterSize', 'ClusterSize', 50, -0.5, 49.5 )
        
    def openFiles( self ):
        from ROOT import TFile
        if not self.inputRootFile or not self.inputRootFile.IsOpen():
            if not os.path.exists( self.inputFileName ):
                print 'Input file does not exist: %s' % ( self.inputFileName )
                return False
            self.inputRootFile = TFile.Open( self.inputFileName )
            if not self.inputRootFile.IsOpen():
                print 'Unable to open input file: %s' % self.inputFileName
                return False
        return True
    
    def closeFiles( self ):
        if self.inputRootFile and self.inputRootFile.IsOpen():
            self.inputRootFile.Close()
    
    def process( self ):
        if not self.openFiles():
            return
        line = 'Extracting efficiencies from %s using ' % self.inputFileName
        if self.type:
            line += self.type
        else:
            line += 'Default'
        print line + ' pre selection'
        from pyDhcal import dhcalMapping
        for layerID, layer in dhcalMapping.layerMap.items():
            layerName = 'Layer%d' % layerID
            for moduleID in layer.modules:
                moduleName = str(moduleID).title()
                histogramName = '%s/%s/%s_%s' % ( layerName, moduleName, layerName, moduleName )
                if self.type:
                    histogramName += '_%s' % self.type
                histogramName += '_ClusterSize'
                h = self.inputRootFile.Get( histogramName )
                self.clusterHistogram.Add( h )
                nTotal = h.GetEntries()
                if nTotal == 0:
                    continue
                nReconstructed = nTotal - h.GetBinContent( 1 )
                efficiency = nReconstructed / nTotal
                h.GetXaxis().SetRange( 2, h.GetXaxis().GetLast() )
                multiplicity = h.GetMean()
                if self.debug:
                    print '\tLayer: %d, Module: %s, Entries: %d, Efficiency: %.3g, Multiplicity: %.3g' % (layerID, moduleID, int(h.GetEntries()), efficiency, multiplicity)
        self.closeFiles()
        
    def calculate( self ):
        if not self.clusterHistogram:
            return
        nTotal = self.clusterHistogram.GetEntries()
        nReconstructed = nTotal - self.clusterHistogram.GetBinContent( 1 )
        efficiency = nReconstructed / nTotal

        self.clusterHistogram.GetXaxis().SetRange( 2, self.clusterHistogram.GetXaxis().GetLast() )
        multiplicity = self.clusterHistogram.GetMean()

        print 'Average Efficiency:', efficiency
        print 'Average Multiplicity:', multiplicity

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Extracts average efficiency and multiplicity from calibration histograms.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-m', '--muons', action='store_true', help='Use all muon runs')
    group.add_argument('-r', '--runNumber', type=int, help='Run number (builds the input and output file names automatically)')
    group.add_argument('-i', '--inputFile', default='', help='Name of the input file')
    parser.add_argument('-R', '--runList', default='/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt', help='Run list file')
    parser.add_argument('-I', '--inputPath', default='/afs/cern.ch/work/cgrefe/public/DHCAL/root/CalibrationHistograms', help='Base path for input files')
    parser.add_argument('-t', '--type', default='Final', choices=['','Cleaned','Final'], help='Type of channel preselection'  )
    parser.add_argument('-d', '--debug', action='store_true', help='Adds debug outpu')
    args = parser.parse_args()
    
    from pyDhcal import dhcalMapping
    layers = xrange(dhcalMapping.nLayers)
    
    calculator = AverageEfficiencyCalculator()
    calculator.debug = args.debug
    calculator.type = args.type
    if args.muons:
        runList = RunList.readTextFile( args.runList ).getRunsByRunType( RunType.DATA )
        runList = runList.getRunsByMomentum( [180.] )
        runList = runList.getRunsByTrigger( Trigger.THIRTY_THIRTY )
        runList = runList.getRunsByPolarity( Polarity.POSITIVE )
        for run in runList:
            calculator.inputFileName = os.path.join( args.inputPath, 'CalibrationHistograms_%d.root' % run.runNumber )
            calculator.process()
    elif args.runNumber:
        runList = RunList.readTextFile( args.runList ).getRunsByRunType( RunType.DATA )
        run = runList.getRun( args.runNumber )
        if not run:
            print 'Run %d is not a valid run number' % run.runNumber
            sys.exit( 2 )
        calculator.inputFileName = os.path.join( args.inputPath, 'CalibrationHistograms_%d.root' % run.runNumber )
        calculator.process()
    elif args.inputFile:
        calculator.inputFileName = args.inputFile
        calculator.process()
        
    calculator.calculate()