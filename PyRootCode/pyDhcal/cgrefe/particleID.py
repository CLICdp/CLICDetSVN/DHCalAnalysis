from pyDhcal.dhcalDefinitions import *

def identifyParticle( event, momentum, polarity ):
    particleType = ParticleType.UNKNOWN
    
    nHits = event.getNHits()
    triggerBits = event.getTriggerBits()
    density = event.getDensity()
    interactionLayer = event.getInteractionLayer()
    baryCenter = event.getBaryCenterLayer()
    hitsInFirstLayer = event.getLayerHits( 1 ).size()
    hitsInLastFourLayers = 0
    cerenkovA = triggerBits.TestBitNumber( 0 )
    cerenkovB = triggerBits.TestBitNumber( 1 )
    for layer in [ 51, 52, 53, 54 ]:
        hitsInLastFourLayers += event.getLayerHits( layer ).size()
    
    # muon Identification
    if baryCenter > 20. and density < 3. and nHits > 10 and hitsInLastFourLayers > 0:
        return ParticleType.MUON
    
    # electron identification
    electronDensityCut = 4
    electronHitsInFirstLayerCut = 4
    if not momentum > 12.:
        electronDensityCut = -1
        electronHitsInFirstLayerCut = -1
    
    if cerenkovA and cerenkovB and baryCenter < 8. and density > electronDensityCut and hitsInFirstLayer > electronHitsInFirstLayerCut:
        return ParticleType.ELECTRON
    
    # pion identification
    pionInteractionLayerCut = 2
    cerenkovTest = (not cerenkovA) and (not cerenkovB)
    if polarity == Polarity.POSITIVE:
        if momentum > 10.:
            cerenkovTest = cerenkovA and cerenkovB
        else:
            cerenkovTest = cerenkovA and not cerenkovB        
    if not momentum > 3.:
        pionInteractionLayerCut = -1
    
    #print cerenkovTest, cerenkovA, cerenkovB
    
    if cerenkovTest and density > 2. and interactionLayer > pionInteractionLayerCut:
        return ParticleType.PION
    
    # proton identification
    if not cerenkovA and not cerenkovB and density > 2. and polarity == Polarity.POSITIVE:
        return ParticleType.PROTON
    
    return particleType

# helper method to read the trigger bit from an event
#def getTriggerBit( event ):
#    bit = array.array( 'i', [0] )
#    event.getTriggerBits().Get( bit )
#   return bit[0]