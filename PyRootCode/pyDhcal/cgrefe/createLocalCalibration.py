from ROOT import TCanvas, TFile, TTree, TF1, Double, gROOT, gDirectory, std
from ROOT import kRed, kDashed

from pyDhcal.dhcalMapping import layerMap, moduleIdMap
from os import path
from argparse import ArgumentParser
from array import array

import math

gROOT.ProcessLine( '.x /Users/cgrefe/CLICStyle.C' )

types = ['Cleaned', 'Raw', 'Final']
plots = ['Efficiency', 'Multiplicity']

moduleNames = {
    0 : 'Top',
    1 : 'Middle',
    2 : 'Bottom',       
    }

def splitGaussianPlusFlat( x, par ):
    """
    Split Gaussian + flat distribution:
    amplitude: par[0]
    mean:      par[1]
    sigma1:    par[2]
    sigma2:    par[3]
    offset:    par[4]
    """
    if x[0] < par[1]:
        if par[2] == 0.:
            return 0.
        return par[4] + par[0] * math.exp(-0.5*((x[0] - par[1])/par[2])**2)
    else:
        if par[3] == 0.:
            return 0.
        return par[4] + par[0] * math.exp(-0.5*((x[0] - par[1])/par[3])**2)

class LocalCalibrationCreator:
    
    inputPath = ''
    outputRootPath = ''
    outputCalibrationPath = ''
    runNumber = 0
    inputFile = None
    outputFile = None
    tree = None
    treeName = 'DhcalLocalCalibration'
    calibrationFile = None
    efficiencyCombinedFit = None
    efficiencyLineFit = None
    multiplicityFit = None
    plotType = 'Cleaned'
    plotDir = None
    efficiencyDir = None
    multiplicityDir = None
    xMin = -0.5
    xMax = 95.5
    xMean = 47.5
    fitOptions = 'EMNRQEX0'
    maxErrorEfficiency = 0.05
    maxErrorMultiplicity = 0.2
    x = Double(0.)
    y = Double(0.)
    variableTypes = { 'runNumber' : 'i', 'layer' : 'i', 'module' : 'i', 'efficiency' : 'f', 'multiplicity' : 'f',
                      'calibration' : 'f', 'sigmaL' : 'f', 'sigmaR' : 'f', 'amplitude' : 'f', 'integralL' : 'f', 'integralR' : 'f' }
    variables = {}
    
    def openFiles( self ):
        self.inputFile = TFile.Open( path.join( self.inputPath, 'CalibrationHistograms_%d.root' % self.runNumber ) )
        self.outputFile = TFile.Open( path.join( self.outputRootPath, 'CalibrationHistograms_%d.root' % self.runNumber ), 'recreate' )
        self.tree = TTree( self.treeName, self.treeName )
        for variableName, variableType in self.variableTypes.items():
            self.variables[variableName] = array( variableType, [0] )
            self.tree.Branch( variableName, self.variables[variableName], '%s/%s' % (variableName, variableType.upper()) )
        self.variables['xIndex'] = std.vector( 'float' )()
        self.tree.Branch( 'xIndex', self.variables['xIndex'] )
        self.variables['xEfficiency'] = std.vector( 'float' )()
        self.tree.Branch( 'xEfficiency', self.variables['xEfficiency'] )
        self.plotDir = self.outputFile.mkdir( 'plots' )
        self.efficiencyDir = self.plotDir.mkdir( 'efficiency' )
        self.multiplicityDir = self.plotDir.mkdir( 'multiplicity' )
        self.calibrationFile = open( path.join( self.outputCalibrationPath, '%d.txt' % self.runNumber ), 'w' )
    
    def closeFiles( self ):
        self.inputFile.Close()
        self.outputFile.cd()
        self.tree.Write()
        self.outputFile.Close()
        self.calibrationFile.close()
    
    def prepareFits( self ):
        self.efficiencyLineFit = TF1( 'EfficiencyLineFit', '[0]', self.xMin, self.xMax )
        self.efficiencyLineFit.SetLineColor( kRed )
        self.efficiencyLineFit.SetLineStyle( kDashed )
        self.efficiencyLineFit.SetLineWidth( 2 )
        self.efficiencyLineFit.SetParLimits( 0, 0.0, 1.0 )
        self.efficiencyLineFit.SetParameter( 0, 0.5 )
        
        self.efficiencyCombinedFit = TF1( 'EfficiencyCombinedFit', splitGaussianPlusFlat, self.xMin, self.xMax, 5 )
        self.efficiencyCombinedFit.SetLineColor( kRed )
        self.efficiencyCombinedFit.SetLineWidth( 2 )
        self.efficiencyCombinedFit.SetParLimits( 0, -999.9, 0.0 )
        self.efficiencyCombinedFit.SetParLimits( 1, self.xMean, self.xMean )
        self.efficiencyCombinedFit.SetParameter( 1, self.xMean )
        self.efficiencyCombinedFit.SetParLimits( 2, 0.5, 48.0 )
        self.efficiencyCombinedFit.SetParameter( 2, 0.5 )
        self.efficiencyCombinedFit.SetParLimits( 3, 0.5, 48.0 )
        self.efficiencyCombinedFit.SetParameter( 3, 0.5 )
        self.efficiencyCombinedFit.SetParLimits( 4, 0.0, 1.0 )
        
        self.multiplicityFit = TF1( 'MuliplicityFit', '[0]', self.xMin, self.xMax )
        self.multiplicityFit.SetLineColor( kRed )
        self.multiplicityFit.SetLineWidth( 2 )
        
    def fitEfficiency( self, graph ):
        graph.Fit( self.efficiencyLineFit, self.fitOptions )
        self.efficiencyCombinedFit.SetParameter( 4, self.efficiencyLineFit.GetParameter( 0 ) )
        graph.Fit( self.efficiencyCombinedFit, self.fitOptions )
        self.efficiencyLineFit.SetParameter( 0, self.efficiencyCombinedFit(4) )
    
    def fitMultiplicity( self, graph ):
        graph.Fit( self.multiplicityFit, self.fitOptions )
        
    def writeLayerCalibration( self ):
        layer = self.variables['layer'][0]
        module = self.variables['module'][0]
        multiplicity = self.variables['multiplicity'][0]
        for x, efficiency in zip(self.variables['xIndex'], self.variables['xEfficiency']):
            self.calibrationFile.write( '%d %d %d %s %s %s\n' % (layer, module, int(x), efficiency*multiplicity, efficiency, multiplicity) )
    
    def processAll( self ):
        self.openFiles()
        if not self.inputFile or not self.inputFile.IsOpen():
            return
        self.prepareFits()
        self.variables['runNumber'] = self.runNumber
        for layerID, layer in layerMap.items():
            for moduleID in layer.modules:
                self.processLayer( layerID, moduleID )
        self.closeFiles()
    
    def processLayer( self, layerID, moduleID ):
        self.variables['xIndex'].clear()
        self.variables['xEfficiency'].clear()
        layerName = 'Layer%d' % layerID
        moduleName = str(moduleID).title()
        self.variables['layer'][0] = int(layerID)
        self.variables['module'][0] = int(moduleIdMap[moduleID])
        effCanvas = TCanvas( 'Efficiency_%s_%s' % ( layerName, moduleName ), 'Efficiency %s %s %d' % ( layerName, moduleName, self.runNumber ), 800, 700 )
        plotName = '%s_%s_Efficiency%sX' % ( layerName, moduleName, self.plotType )
        e = self.inputFile.Get( '%s/%s/%s' % (layerName, moduleName, plotName ) )
        totalHist = e.GetTotalHistogram()
        nTotal = totalHist.GetEffectiveEntries()
        g = e.CreateGraph()
        g.SetLineWidth( 2 )
        i = 0
        while i < g.GetN():
            # combine asymmetric errors. Those are 95% confidence level boundaries.
            if g.GetErrorYhigh( i ) + g.GetErrorYlow( i ) > 2 * self.maxErrorEfficiency:
                g.RemovePoint( i )
                i -= 1
            else:
                g.GetPoint( i, self.x, self.y )
                if self.y < 1.0:
                    self.variables['xIndex'].push_back( self.x )
                    self.variables['xEfficiency'].push_back( self.y )
            i += 1
        if g.GetN() < 10:
            return
        self.fitEfficiency( g )
        self.variables['efficiency'][0] = self.efficiencyCombinedFit.GetParameter( 4 )
        self.variables['amplitude'][0] = self.efficiencyCombinedFit.GetParameter( 0 )
        self.variables['sigmaL'][0] = self.efficiencyCombinedFit.GetParameter( 2 )
        self.variables['sigmaR'][0] = self.efficiencyCombinedFit.GetParameter( 3 )

        g.SetTitle( 'Efficiency %s %s %d' % ( layerName, moduleName, self.runNumber) )
        g.Draw( 'AP' )
        self.efficiencyLineFit.Draw( 'same' )
        self.efficiencyCombinedFit.Draw( 'same' )
        self.efficiencyCombinedFit.GetXaxis().SetRangeUser( self.xMin, self.xMax )
        self.efficiencyCombinedFit.GetYaxis().SetRangeUser( 0.0, 1.05 )
        self.variables['integralL'][0] = self.efficiencyLineFit.Integral( self.xMin, self.xMean ) - self.efficiencyCombinedFit.Integral( self.xMin, self.xMean )
        self.variables['integralR'][0] = self.efficiencyLineFit.Integral( self.xMean, self.xMax ) - self.efficiencyCombinedFit.Integral( self.xMean, self.xMax )
        effCanvas.Update()
        self.efficiencyDir.cd()
        effCanvas.Write()
        
        multCanvas = TCanvas( 'Multiplicity_%s_%s' % ( layerName, moduleName ), 'Multiplicity %s %s %d' % ( layerName, moduleName, self.runNumber ), 800, 700 )
        plotName = '%s_%s_Multiplicity%sX' % (layerName, moduleName, self.plotType )
        g = self.inputFile.Get( '%s/%s/%s' % (layerName, moduleName, plotName ) )
        g.SetTitle( 'Multiplicity %s %s %d' % ( layerName, moduleName, self.runNumber) )
        i = 0
        while i < g.GetN():
            yError = g.GetErrorY( i )
            # remove all points without any error bars, i.e. which have just one entry
            if yError == 0. or yError > self.maxErrorMultiplicity:
                g.RemovePoint( i )
                i -= 1
            else:
                g.SetPointError( i, 0.5, g.GetErrorY( i ) )
            i += 1
        self.fitMultiplicity( g )
        self.variables['multiplicity'][0] = self.multiplicityFit.GetParameter( 0 )
        g.Draw( 'AP' )
        self.multiplicityFit.Draw( 'same' )
        self.multiplicityFit.GetXaxis().SetRangeUser( self.xMin, self.xMax )
        self.multiplicityFit.GetYaxis().SetRangeUser( 1.0, 4.0 )
        multCanvas.Update()
        self.multiplicityDir.cd()
        multCanvas.Write()
        self.variables['calibration'][0] = self.variables['efficiency'][0] * self.variables['multiplicity'][0]
        self.tree.Fill()
        self.writeLayerCalibration()

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument( '-b', action='store_false', help='batch mode' )
    parser.add_argument( '-r', '--runNumber', type=int, help='Run number to process', required=True )
    parser.add_argument( '-i', '--inputPath', default='/Users/cgrefe/Work/DHCAL/CalibrationHistograms' )
    parser.add_argument( '-o', '--outputPath', default='/Users/cgrefe/Work/DHCAL/plots/calibrationBasic' )
    parser.add_argument( '-c', '--calibrationPath', default='/Users/cgrefe/Work/DHCAL/calibration/' )
    parser.add_argument( '-t', '--type', choices=types, default='Cleaned' )
    result = parser.parse_args()
    
    cal = LocalCalibrationCreator()
    cal.runNumber = result.runNumber
    cal.inputPath = result.inputPath
    cal.outputRootPath = result.outputPath
    cal.outputCalibrationPath = result.calibrationPath
    cal.plotType = result.type
    
    cal.processAll()