from argparse import ArgumentParser
from ROOT import TFile

nLayers = 54
nModules = 3

calibrationMap = {}

def parseTree( tree ):
    for entry in tree:
        runNumber = tree.runNumber
        layer = tree.layer
        module = tree.module
        efficiency = tree.efficiency
        multiplicity = tree.multiplicity
        #entries = tree.entries
        #efficiency = tree.cleaned_efficiency
        #multiplicity = tree.cleaned_multiplicity
        #entries = tree.cleaned_entries
        entries = 0
        if efficiency == 0.:
            efficiency == 1.0
        if multiplicity == 0.:
            multiplicity == 1.0
        calibration = multiplicity * efficiency
        if not calibrationMap.has_key( runNumber ):
            calibrationMap[runNumber] = {}
        if not calibrationMap[runNumber].has_key( layer ):
            calibrationMap[runNumber][layer] = {}
        calibrationMap[runNumber][layer][module] = ( calibration, efficiency, multiplicity, entries )

def createCalibrationFile( runNumber ):
    if not calibrationMap.has_key( runNumber ):
        return
    outputFileName = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/calibration/cleaned_%d.txt' % ( runNumber )
    outputFile = open( outputFileName, 'w' )
    runCalibration = calibrationMap[ runNumber ]
    for layer in xrange(nLayers):
        for module in xrange(nModules):
            calibration, efficiency, multiplicity, entries = runCalibration[layer][module]
            if entries < 100:
                mostEntries = 0
                otherID = 0
                for otherModule in xrange(nModules):
                    if runCalibration[layer][otherModule][3] > mostEntries:
                        calibration, efficiency, multiplicity, entries = runCalibration[layer][otherModule] # use central module instead
                        mostEntries = entries
                        otherID = otherModule
            outputFile.write( '%d %d %s %s %s\n' % ( layer, module, calibration, efficiency, multiplicity ) )
    outputFile.close()

if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument( '--run', action='store', default=0 )
    results = parser.parse_args()
    
    #inputFileName = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/root/LayerEfficiency_2013-09-05/layerCalibration.root'
    inputFileName = '/Users/cgrefe/Documents/workspace/DHCAL_PyROOT/pyDhcal/cgrefe/Calibration.root'
    inputFile = TFile.Open( inputFileName )
    tree = inputFile.Get( 'DhcalCalibration' )
    
    parseTree( tree )
    
    if results.run > 0:
        createCalibrationFile( int(results.run) )
    else:
        for runNumber in xrange( 660000, 660700 ):
            createCalibrationFile( runNumber )
