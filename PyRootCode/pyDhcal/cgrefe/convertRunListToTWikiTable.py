import sys
from pyDhcal.runList import RunList

def convertRunListToTWikiTable( runList, tableFileName ):
    print 'Writing run list as TWiki table to %s' % ( tableFileName )
    tableFile = open( tableFileName, 'w' )
    # write the table header
    line = '|  *Time*  |  *Run Number*  |  *Type*  |  *Momentum [GeV]*  |  *Polarity*  |  *Trigger*  |  *nEvents*  |  *Comment*  |  *Bad Run*  |'
    tableFile.write( line + '\n' )
    for run in runList:
        line = '|  %s |  %d |  %s |  %d |  %s |  %s |  %d | %s |  %s |' % ( run.timeStamp, run.runNumber, run.runType, run.beamMomentum, run.polarity, run.trigger, run.nEvents, run.comment, run.isBad )
        tableFile.write( line + '\n' )
    tableFile.close()

def usage():
    print 'python %s <table.txt>' % (sys.argv[0])

if __name__ == '__main__':
    if len(sys.argv) < 2 or sys.argv[1] in [ '-h', '--help' ]:
        usage()
        sys.exit( 1 )
    tableFileName = sys.argv[1]
    runList = RunList.readTextFile( '/afs/cern.ch/user/c/cgrefe/work/DHCalAnalysis/DHCalRunList.txt', includeBadRuns=True )
    convertRunListToTWikiTable( runList, tableFileName )
