from ROOT import TFile, TTree
from pyDhcal.dhcalDefinitions import ParticleType, Polarity, RunType
from pyDhcal.runList import RunList
import os, sys
from numpy import array
from pylisis import rootFileTester

class ParticleIdentifier():
    
    def __init__( self, momentum, polarity, tree ):
        self.momentum = momentum
        self.polarity = polarity
        self.tree = tree
        
        self.electronDensityCut = 4
        self.electronHitsInFirstLayerCut = 4
        if not self.momentum > 12.:
            self.electronDensityCut = -1
            self.electronHitsInFirstLayerCut = -1
            
        self.pionInteractionLayerCut = 2
        if not self.momentum > 3.:
            self.pionInteractionLayerCut = -1
             

    def identifyParticle( self ):
        noParticleID = ParticleType.UNKNOWN
        
        nHits = self.tree.nHits
        if nHits > 6000 or nHits == 0:
            return noParticleID
        
        clustersInFirstLayer = self.tree.layerClusters[0]
        if clustersInFirstLayer != 1:
            #pass
            return noParticleID
        
        hitsInFirstLayer = self.tree.layerHits[0]
        if hitsInFirstLayer > 11:
            return noParticleID
        
        cerenkovA = self.tree.cerenkovA
        cerenkovB = self.tree.cerenkovB
        density = self.tree.hitDensity
        interactionLayer = self.tree.interactionLayer
        baryCenter = self.tree.centerLayer
        hitsInLastFiveLayers = 0
        for layer in [ 49, 50, 51, 52, 53 ]:
            hitsInLastFiveLayers += self.tree.layerHits[ layer ]
            
        if baryCenter > 20. and density < 3. and nHits > 10 and hitsInLastFiveLayers > 0 and interactionLayer < 0:
            return ParticleType.MUON
        
        if cerenkovA and cerenkovB and baryCenter < 8. and density > self.electronDensityCut and hitsInFirstLayer > self.electronHitsInFirstLayerCut:
            return ParticleType.ELECTRON
        
        pionCerenkovTest = (not cerenkovA) and (not cerenkovB)
        if self.polarity == Polarity.POSITIVE:
            if self.momentum > 10.:
                pionCerenkovTest = cerenkovA and cerenkovB
            else:
                pionCerenkovTest = cerenkovA and not cerenkovB
        
        if pionCerenkovTest and density > 2. and interactionLayer >= self.pionInteractionLayerCut:
            return ParticleType.PION
        
        if not cerenkovA and not cerenkovB and density > 2. and self.polarity == Polarity.POSITIVE:
            return ParticleType.PROTON
        
        return noParticleID

def createParticleIdTree( run, rootTree, outputFileName, outputTreeName='DhcParticleID' ):
    particleID = ParticleIdentifier( run.beamMomentum, run.polarity, rootTree )
    
    entries = rootTree.GetEntries()
    
    outputRootFile = TFile.Open( outputFileName, 'recreate' )
    outputRootTree = TTree( outputTreeName, outputTreeName )
    
    isElectron = array( [False], dtype=bool )
    isMuon = array( [False], dtype=bool )
    isPion = array( [False], dtype=bool )
    isProton = array( [False], dtype=bool )
    
    outputRootTree.Branch( 'electron', isElectron, 'electron/O' )
    outputRootTree.Branch( 'muon', isMuon, 'muon/O' )
    outputRootTree.Branch( 'pion', isPion, 'pion/O' )
    outputRootTree.Branch( 'proton', isProton, 'proton/O' )
    
    print 'Processing %s with %d events'%(run, entries)
    
    for entry in xrange(entries):
        ientry = rootTree.LoadTree( entry )
        if ientry < 0:
            break
        nb = rootTree.GetEntry( entry )
        if nb <= 0:
            print 'nb:', nb
            break
        
        if (entry % 10000 == 0):
            print '%d / %d'%(entry, entries)
        
        isElectron[0] = False
        isMuon[0] = False
        isPion[0] = False
        isProton[0] = False
        
        PID = particleID.identifyParticle()
        if PID == ParticleType.ELECTRON:
            isElectron[0] = True
        if PID == ParticleType.MUON:
            isMuon[0] = True
        if PID == ParticleType.PION:
            isPion[0] = True
        if PID == ParticleType.PROTON:
            isProton[0] = True
        
        outputRootTree.Fill()
    
    outputRootFile.Write()
    outputRootFile.Close()

def processRun( run ):
    rootTreeName = "DhcEventVariables"
    basePath = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/root'
    runNumber = run.runNumber
    rootFileName = os.path.join( basePath, 'EventVariables', 'EventVariables_%d.root'%(runNumber) )
    if not os.path.exists( rootFileName ):
        print '%s does not exist'%(rootFileName)
        return
    outRootFileName = os.path.join( basePath, 'ParticleID', 'ParticleID_%d.root'%(runNumber) )
    if os.path.exists( outRootFileName ):
        print '%s already exists'%(outRootFileName)
        return
    rootFile = TFile.Open( rootFileName )
    rootTree = rootFile.Get( rootTreeName )
    
    createParticleIdTree( run, rootTree, outRootFileName, 'DhcParticleID' )
    rootFile.Close()

if __name__ == "__main__":
    runListFile = '/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt'
    runList = RunList.readTextFile( runListFile )
    runList = runList.getRunsByRunType( RunType.DATA )
    
    if len(sys.argv) < 2:
        for run in runList:
            processRun( run )
    else:
        runNumber = int(sys.argv[1])
        run = runList.getRun( runNumber )
        if run:
            processRun( run )
        else:
            print 'Run number %d not in %s'%(runNumber, runListFile)
        