from ROOT import TFile
import os

def parseTree( tree ):
    calibrationMap = {}
    for entry in tree:
        layer = tree.layer
        module = tree.module
        efficiency = tree.efficiency
        multiplicity = tree.multiplicity
        calibration = tree.calibration
        if not calibrationMap.has_key(layer):
            calibrationMap[layer] = {}
        calibrationMap[layer][module] = ( calibration, efficiency, multiplicity )
    return calibrationMap
    
def createCalibrationFile( runNumber ):
    inputBasePath = '/Users/cgrefe/Work/DHCAL/plots/calibrationBasic/'
    outputBasePath = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/calibration/fit/'
    inputFileName = inputBasePath + 'CalibrationHistograms_%d.root' % runNumber
    outputFileName = outputBasePath + '%d.txt' % runNumber
    outputFile = open( outputFileName, 'w' )
    
    nLayers = 54
    nModules = 3
    
    if not os.path.isfile( inputFileName ):
        outputFile.close()
        return
    rootFile = TFile.Open( inputFileName )
    if not rootFile or not rootFile.IsOpen():
        outputFile.close()
        return
    tree = rootFile.Get( 'DhcalLocalCalibration' )
    if not tree:
        outputFile.close()
        rootFile.Close()
        return
    calibrationMap = parseTree( tree )
    rootFile.Close()
    
    for layer in xrange(nLayers):
        if not calibrationMap.has_key( layer ):
            continue
        for module in xrange(nModules):
            if not calibrationMap[layer].has_key( module ):
                continue
            calibration, efficiency, multiplicity = calibrationMap[layer][module]
            outputFile.write( '%d %d %s %s %s\n' % ( layer, module, calibration, efficiency, multiplicity ) )
    outputFile.close()
    
if __name__ == '__main__':
    for runNumber in xrange( 660000, 660700 ):
       createCalibrationFile( runNumber )
