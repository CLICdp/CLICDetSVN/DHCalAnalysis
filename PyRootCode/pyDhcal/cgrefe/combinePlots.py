from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import RunType, Polarity, ParticleType
from cgrefe.tools.Canvas import Canvas
from ROOT import TFile, TTree, TH1D, TGraph, TGraphErrors, TF1, kDashed
import os
from cgrefe.tools.RootStyle import RootStyle
from cgrefe.tools.DistributionTools import findSmallestHistInterval

basePath = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/histograms'
plotPath = '/afs/cern.ch/work/c/cgrefe/public/DHCAL/plots/combined'

polarities = [ Polarity.NEGATIVE, Polarity.POSITIVE ]
polarities = [ Polarity.NEGATIVE ]
particleTypes = [ ParticleType.ELECTRON, ParticleType.PION, ParticleType.PROTON, ParticleType.MUON ]
particleTypes = [ ParticleType.ELECTRON, ParticleType.PION ]

legendConversion = {
    'nHits' : '',
    'nHitsEarly' : '',
    'energy' : 'cal.',
    'energyEarly' : 'cal.',
    }

histogramTypes = {
    #'nHits' : ( 'Hits', 'Events', 0, 6000 ),
    'nHitsEarly' : ( 'Hits', 'Events', 0, 2000 ),
    #'energy' : ( 'Calibrated Hits', 'Events', 0, 2000 ),
    'energyEarly' : ( 'Calibrated Hits', 'Events', 0, 2000 ),
    #'hitDensity' : ( 'Density', 'Events', 0, 500 ),
    #'showerRadius' : ( 'Shower Radius', 'Events', 0, 300 ),
    #'interactionLayer' : ( 'InteractionLayer', 'Events', 0, 52 ),
    #'layerHitsProfile' : ( 'Layer', 'Mean Hits', 0, 52 ),
    #'layerHitsProfileCorrected' : ( 'Layer - InteractionLayer', 'Mean Hits', -10, 44 ),
    #'layerEnergyProfileCorrected' : ( 'Layer - InteractionLayer', 'Mean Hits', -10, 44 ),
    #'layerShowerProfile' : ( 'Layer', 'Mean RMS [mm]', 0, 52 ),
    #'layerShowerProfileCorrected' : ( 'Layer - InteractionLayer', 'Mean RMS [mm]', -10, 40 ),
    }

style = RootStyle()

TH1D.SetDefaultSumw2( True )

ignoredRuns = [ 660305, 660307, 660309, 660311, 660313, 660318, 660319, 660320 ] # 40 GeV
ignoredRuns.extend( [ 660265, 660267, 660268, 660269, 660271, 660273, 660275, 660277, 660279, 660281, 660283 ] ) # 20 GeV
ignoredRuns.extend( [ 660252 ] ) # 80 GeV
ignoredRuns.extend( [ 660111, 660113 ] ) # 2 GeV

def combinePlots( runList ):
    histograms = {}
    for histogramType in histogramTypes:
        histograms[histogramType] = {}
        for polarity in polarities:
            histograms[histogramType][polarity] = {}
            for particleType in particleTypes:
                histograms[histogramType][polarity][particleType] = {}
    
    for run in runList:
        runNumber = run.runNumber
        if runNumber in ignoredRuns:
            continue
        polarity = run.polarity
        if polarity not in polarities:
            continue
        momentum = run.beamMomentum
        rootFileName = os.path.join( basePath, 'histograms_%d.root'%(runNumber) )
        if not os.path.exists( rootFileName ):
            continue
        rootFile = TFile.Open( rootFileName )
        for particleType in particleTypes:
            for histogramType in histogramTypes:
                histogram = rootFile.Get( '%s/%s'%(histogramType, particleType) )
                if histogram:
                    histogram.Sumw2( True )
                    #histogram.Rebin(2)
                    if histogram.GetEntries() == 0:
                        continue
                    if not momentum in histograms[histogramType][polarity][particleType]:
                        histograms[histogramType][polarity][particleType][momentum] = []
                    histograms[histogramType][polarity][particleType][momentum].append( ( runNumber, histogram ) )
    
    
    plots = {}
    
    linPlot = Canvas( 1, 1, 'linearity', 'linearity', 800, 700 )
    resPlot = Canvas( 1, 1, 'resolution', 'resolution', 800, 700 )
    chi2Plot = Canvas( 1, 1, 'chi2test', 'chi2test', 800, 700 )
    
    plots['linearity'] = linPlot
    plots['resolution'] = resPlot
    plots['chi2'] = chi2Plot
    
    linPlot.addLegend( 1, 0.2, 0.6, 0.5, 0.9, '', 0.03 )
    linPlot.getLegend( 1 ).SetNColumns( 2 )
    resPlot.addLegend( 1, 0.3, 0.6, 0.6, 0.9, '', 0.03 )
    resPlot.getLegend( 1 ).SetNColumns( 2 )
    
    histogramIndex = 0
    for histogramType, histogramDefinition in histogramTypes.items():
        xTitle, yTitle, xMin, xMax = histogramDefinition
        isProfile = False
        if 'Profile' in histogramType:
            isProfile = True
        particleIndex = 0
        for particleType in particleTypes:
            for polarity in polarities:
                if particleType == ParticleType.PROTON and polarity == Polarity.NEGATIVE:
                    continue
                plotTitle = '%s_%s%s'%(histogramType, polarity, particleType)
                plotTitle = plotTitle.replace( '-', 'Minus' )
                plotTitle = plotTitle.replace( '+', 'Plus' )
                combinedPlot = Canvas( 1, 1, plotTitle, plotTitle, 800, 700 )
                momenta = histograms[histogramType][polarity][particleType].keys()
                momenta.sort()
                momentumIndex = 0
                if histogramType in ['nHits', 'nHitsEarly', 'energy', 'energyEarly']:
                    linGraph = TGraphErrors( 0 )
                    resGraph = TGraphErrors( 0 )
                    chi2Graph = TGraph( 0 )
                    linPlot.addGraph( 1, linGraph, 'AP' )
                    legendEntry = '%s^{%s}' % ( particleType, polarity )
                    if legendConversion[histogramType]:
                        legendEntry += ' %s' % ( legendConversion[histogramType] )
                    print '%s%s %s'%(polarity, particleType, xTitle)
                    linPlot.addLegendEntry( 1, linGraph, legendEntry, 'LP')
                    resPlot.addGraph( 1, resGraph, 'ALP' )
                    resPlot.addLegendEntry( 1, resGraph, legendEntry, 'LP')
                    chi2Plot.addGraph(1, chi2Graph, 'ALP' )
                    chi2Plot.addLegendEntry( 1, chi2Graph, legendEntry, 'LP')
                    style.setMarker( linGraph, particleIndex )
                    style.setColor( linGraph, particleIndex )
                    style.setLine( linGraph, histogramIndex )
                    style.setMarker( resGraph, particleIndex )
                    style.setColor( resGraph, particleIndex )
                    style.setLine( resGraph, histogramIndex )
                    style.setMarker( chi2Graph, particleIndex )
                    style.setColor( chi2Graph, particleIndex )
                    style.setLine( chi2Graph, histogramIndex )
                    if polarity == Polarity.POSITIVE:
                        style.setFullMarker( linGraph, particleIndex )
                        style.setFullMarker( resGraph, particleIndex )
                        style.setFullMarker( chi2Graph, particleIndex )
                particleLegend = '%s^{%s}'%(particleType, polarity)
                if particleType == ParticleType.PROTON:
                    particleLegend = '%s'%(particleType)
                combinedPlot.addLegend( 1, 0.6, 0.6, 0.91, 0.90, '%s'%(particleLegend), 0.03 )
                combinedPlotLimit = 0.
                for momentum in momenta:
                    momentumHistograms = histograms[histogramType][polarity][particleType][momentum]
                    if len(momentumHistograms) == 0:
                        continue
                    if momentum in [9, 12, 30, 50, 100, 300]:
                        continue
                        pass
                    if momentum > 120:
                        continue
                        pass
                    if momentum != 2:
                        #continue
                        pass
                    momentumPlotTitle = '%s_%dGeV'%(plotTitle, momentum)
                    momentumPlot = Canvas( 1, 1, momentumPlotTitle, momentumPlotTitle, 800, 700 )
                    momentumPlot.addLegend( 1, 0.25, 0.83, 0.4, 0.90, '%d GeV %s'%(momentum, particleLegend), 0.04 )
                    momentumPlot.setTitle( 1, plotTitle, xTitle, yTitle )
                    plots[momentumPlotTitle] = momentumPlot
                    
                    momentumByRunPlotTitle = '%s_%dGeV_runs'%(plotTitle, momentum)
                    momentumByRunPlot = Canvas( 1, 1, momentumByRunPlotTitle, momentumByRunPlotTitle, 800, 700 )
                    momentumByRunPlot.addLegend( 1, 0.25, 0.83 -0.03*len(momentumHistograms), 0.4, 0.90, '%d GeV %s'%(momentum, particleLegend), 0.03 )
                    if isProfile:
                        momentumByRunPlot.setTitle( 1, plotTitle, xTitle, yTitle )
                    else:
                        momentumByRunPlot.setTitle( 1, plotTitle, xTitle, 'Normalized ' + yTitle )
                    plots[momentumByRunPlotTitle] = momentumByRunPlot
                    
                    if 'Profile' in histogramType and momentum not in [ 3, 6, 10, 60, 100, 180 ]:
                        continue
                    totalHistogram = None
                    runIndex = 0
                    runPlotLimit = 0.
                    referenceHistogram = None
                    totalChi2 = 0.
                    for runNumber, histogram in momentumHistograms:
                        entries = histogram.GetEntries()
                        if entries == 0 or entries < 500:
                            continue
                        histogram.SetStats( False )
                        if not totalHistogram:
                            totalHistogram = histogram.Clone( 'totalHistogram_%s'%(momentumPlotTitle) )
                        else:
                            totalHistogram.Add( histogram )
                        if not isProfile:
                            histogram.Scale( 1. / histogram.GetEntries() )
                        mean = histogram.GetMean()
                        rms = histogram.GetRMS()
                        if not referenceHistogram:
                            referenceHistogram = histogram
                        else:
                            totalChi2 += referenceHistogram.Chi2Test(histogram, 'WW CHI2/NDF')
                        if (mean + 3*rms) > runPlotLimit:
                            runPlotLimit = mean + 3*rms
                        style.setColor( histogram, runIndex )
                        momentumByRunPlot.addHistogram( 1, histogram )
                        momentumByRunPlot.addLegendEntry( 1, histogram, '%d'%(runNumber), 'L')
                        runIndex += 1
                    if not isProfile:
                        momentumPlot.setRange( 1, 'x', 0., runPlotLimit )
                        momentumByRunPlot.setRange( 1, 'x', 0., runPlotLimit )
                    if runPlotLimit > combinedPlotLimit:
                        combinedPlotLimit = runPlotLimit
                    if not totalHistogram:
                        continue
                    entries = totalHistogram.GetEntries()
                    if entries == 0:
                        continue
                    individualTotalHistogram = totalHistogram.Clone( 'individual%s'%(totalHistogram.GetName()) )
                    momentumPlot.addHistogram( 1, individualTotalHistogram )
                    if histogramType in ['nHits', 'nHitsEarly', 'energy', 'energyEarly']:
                        mean = individualTotalHistogram.GetMean()
                        rms = individualTotalHistogram.GetRMS()
                        fit = TF1( 'fitHistIndividual%s'%(totalHistogram.GetName()), 'gaus', mean - rms, mean + rms )
                        #rms90hist = findSmallestHistInterval( individualTotalHistogram.Clone( 'fitHistIndividual%s'%(totalHistogram.GetName()) ), 0.9 )
                        individualTotalHistogram.Fit( fit, 'QMR' )
                        fit.SetLineColor( 2 )
                        #momentumPlot.addFunction( 1, fit, 'L' )
                        fitMean = fit.GetParameter(1)
                        fitMeanErr = fit.GetParError(1)
                        fitRms = fit.GetParameter(2)
                        fitRmsErr = fit.GetParError(2)
                        nPoints = linGraph.GetN()
                        linGraph.Set(nPoints + 1)
                        linGraph.SetPoint( nPoints, momentum, fitMean )
                        linGraph.SetPointError( nPoints, 0.0, fitMeanErr )
                        resGraph.SetPoint( nPoints, momentum, fitRms / fitMean )
                        resGraph.SetPointError( nPoints, 0.0, fitRmsErr / fitMean )
                        if runIndex > 1:
                            chi2Graph.SetPoint( nPoints, momentum, totalChi2 / (runIndex - 1) )
                    if entries < 200:
                        continue    
                    style.setColor( totalHistogram, momentumIndex )
                    if not isProfile:
                        totalHistogram.Scale( 1. / entries )
                    combinedPlot.addHistogram( 1, totalHistogram )
                    combinedPlot.addLegendEntry( 1, totalHistogram, '%s GeV'%(momentum), 'L')
                    momentumIndex += 1
                if histogramType in ['nHits', 'nHitsEarly', 'energy', 'energyEarly']:
                    linGraphFit = TF1( 'linFit%s'%particleType, '[0]*x^[1]', 0.5, 70 )
                    linGraphFit.SetParLimits(0, 0, 100)
                    linGraphFit.SetParLimits(1, 0, 2)
                    linGraphFit.SetLineStyle( linGraph.GetLineStyle() )
                    linGraphFit.SetLineColor( linGraph.GetLineColor() )
                    linGraph.Fit( linGraphFit, 'R' )
                    xPos = 0.65
                    yPos = 0.6
                    for parameterIndex, parameterName in {0 : '#alpha',1 : '#beta'}.items() :
                        print parameterIndex, parameterName
                        value = linGraphFit.GetParameter( parameterIndex )
                        print value
                        if value:
                            linPlot.addText( 1, '%s = %.4G' % ( parameterName, value ), linGraphFit.GetLineColor(), xPos, yPos )
                            yPos -= 0.06
                if combinedPlot.getLegend( 1 ).GetNRows() > 5:
                    combinedPlot.getLegend( 1 ).SetNColumns( 2 )
                combinedPlot.setTitle( 1, plotTitle, xTitle, yTitle )
                combinedPlot.setRange( 1, 'x', xMin, xMax)
                
                if not isProfile:
                    combinedPlot.setRange( 1, 'x', 0, combinedPlotLimit)
                if 'nHits' in histogramType:
                    combinedPlot.setRange( 1, 'y', 0.0001, 0.1 )
                plots[plotTitle] = combinedPlot
            particleIndex += 1
        histogramIndex += 1
    
#    for particleType in particleTypes:
#        particleIndex += 1
#        for polarity in polarities:
#            if particleType == ParticleType.PROTON and polarity == Polarity.NEGATIVE:
#                continue
#            momenta = meanHits[polarity][particleType]
#            linGraph = TGraph( len(momenta) )
#            resGraph = TGraph( len(momenta) )
#            index = 0
#            style = linPlot.style
#            style.setMarker( linGraph, particleIndex )
#            style.setColor( linGraph, particleIndex )
#            style.setMarker( resGraph, particleIndex )
#            style.setColor( resGraph, particleIndex )
#            if polarity == Polarity.POSITIVE:
#                style.setFullMarker( linGraph, particleIndex )
#                style.setFullMarker( resGraph, particleIndex )
#            for momentum in momenta:
#                mean = meanHits[polarity][particleType][momentum]
#                if mean == 0.:
#                    continue
#                rms = rmsHits[polarity][particleType][momentum]
#                linGraph.SetPoint(index, momentum, mean)
#                resGraph.SetPoint(index, momentum, rms/mean)
#                index += 1
#            linPlot.addGraph( 1, linGraph, 'AP' )
#            linPlot.addLegendEntry( 1, linGraph, '%s%s'%(polarity, particleType), 'P')
#            resPlot.addGraph( 1, resGraph, 'AP' )
#            resPlot.addLegendEntry( 1, resGraph, '%s%s'%(polarity, particleType), 'P')
    linPlot.setTitle( 1, 'linearity', 'Beam Momentum [GeV]', 'mean(Hits)' )
    resPlot.setTitle( 1, 'resolution', 'Beam Momentum [GeV]', '#sigma(Hits) / mean(Hits)' )
    chi2Plot.setTitle( 1, 'resolution', 'Beam Momentum [GeV]', 'Average #Chi^{2}/NDF' )

    for plotName, plot in plots.items():
        plot.draw()
        plotFileName = plotName.replace('#','') + '.pdf'
        if 'nHits' in plotFileName and 'GeV' not in plotFileName:
            plot.setLog( 1, yAxis=True )
        plot.saveAs( os.path.join( plotPath, plotFileName ) )
        #continue
        if plotName in ['linearity', 'resolution', 'chi2']:
            plot.setRange( 1, 'x', 0, 15 )
            plot.draw()
            plotFileName = plotName.replace('#','') + '_zoomed.pdf'
            plot.saveAs( os.path.join( plotPath, plotFileName ) )
    return plots

if __name__ == '__main__':
    runList = RunList.readTextFile( '/afs/cern.ch/work/c/cgrefe/DHCalAnalysis/DHCalRunList.txt' )
    runList = runList.getRunsByRunType( RunType.DATA )
    
    plots = combinePlots( runList )
    x = raw_input( 'Continue?' )
    
        