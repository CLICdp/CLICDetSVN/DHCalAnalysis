import sys
from glob import glob
import re
import matplotlib.pyplot as pl
from pyDhcal.runList import RunList
from pyDhcal.dhcalDefinitions import *
from collections import defaultdict
import numpy as np

rl = RunList.readTextFile()

def parse_outputFiles(runNumber):
	runMap = {}
	pattern = re.compile(r'\[ MESSAGE "BoxFinder"\] Run (\d+) has (\d+) Box Events')
	for filename in glob('../../../stdout.%s_*' % runNumber):
		for line in open(filename):
			m = pattern.match(line)
			if m:
				runMap[int(m.group(1))] = int(m.group(2))
	return runMap


def groupByEnergy(runMap):
	energies = (defaultdict(list))
	for r in runMap:
		run = rl.getRun(r)
		# remove muon runs
		if run.trigger == Trigger.THIRTY_THIRTY:
		#if run.trigger == Trigger.TEN_TEN:
			energies[run.beamMomentum].append(runMap[r])
	xVals = []
	yVals = []
	yErr = []
	for e in energies:
		xVals.append(e)
		yVals.append(np.mean(energies[e]))
		yErr.append(np.std(energies[e]))
	pl.errorbar(xVals, yVals, fmt='.', yerr=yErr)
	pl.xlabel('Beam Momentum (GeV)')
	pl.ylabel('Number of Box Events')
	pl.xlim(1, 400)
	#pl.ylim(1, 2200)
	# pl.yscale('log')
	pl.savefig('x.pdf')

if __name__ == '__main__':
	runMap = parse_outputFiles('374236394')
	groupByEnergy(runMap)
