from __future__ import division
import ROOT
import sys

ROOT.gSystem.Load('../dhcCode/libDhcTree.so')
ROOT.gStyle.SetOptFit(1111)
storagePath = '/work0/jfstrube/DhcalMay2012_rootTrees'
runNumber = 660268
fileName = '%s/dhcTree-Run%s.root' % (storagePath, runNumber)
if len(sys.argv) > 1:
    fileName = sys.argv[1]
tree = ROOT.TChain('DhcTree')
tree.Add(fileName)
nEvts = tree.GetEntries()
for iEv in xrange(2):
	print 'Event', iEv
	tree.GetEvent(iEv)
	tree.EventBranch.FindClusters()
	print tree.EventBranch.fClusters
	front = tree.EventBranch.fClusters.front()
	print front
