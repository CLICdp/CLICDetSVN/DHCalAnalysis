from runList import RunList
import ROOT

runList = RunList.readTextFile( includeBadRuns = True )

testRunList = RunList()
testRunList.addRun(runList.getRun(660362))

testRunList.findRootTrees("/afs/cern.ch/work/j/jfstrube/public/DHCAL/DhcalMay2012_rootTrees")

tree = testRunList.getRootTree()

for entry in xrange(100):
    tree.GetEntry(entry)
    event = tree.EventBranch
    hits = event.GetHits()
    
    header = event.GetHeader()
    evtTime = header.GetEvtTime()
    
    print evtTime.GetNanoSec()
    eventTime = 0
    for hit in hits:
        hitTime = hit.getTs()
        if hitTime != eventTime:
            eventTime = hitTime
            print 'Found a new hit time:', hitTime, 'in event', entry
