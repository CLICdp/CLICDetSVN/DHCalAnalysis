from runList import RunList
import ROOT

runList = RunList.readTextFile()
myList = RunList()
myList.addRun(runList.getRun(660372))

myList.findRootTrees("/afs/cern.ch/work/j/jfstrube/public/DHCAL/DhcalMay2012_rootTrees")

tree = myList.getRootTree()

entries = tree.GetEntries()

tree.GetEntry(25)

event = tree.EventBranch

event.findClusters(50, 50, 300)

#clusterMap = event.get2DClusters()

#size = clusterMap.size()

event.printClusters()
