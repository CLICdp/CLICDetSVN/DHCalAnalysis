from dhcalDefinitions import *
from datetime import datetime

# class to hold meta data connected to a run    
class Run:

    def __init__( self, runNumber=0, beamMomentum=0, polarity=Polarity.NEGATIVE, runType=RunType.DATA , temperature=0 , pressure=0, timeStamp=datetime.min, comment = '' ):
        self.runNumber = runNumber
        self.beamMomentum = beamMomentum
        self.polarity = polarity
        self.runType = runType
        self.temperature = temperature
        self.pressure = pressure
        self.timeStamp = timeStamp
        self.comment = comment
        self.nEvents = 0
        self.eventsPerSpill = 0
        self.trigger = None
        self.cerenkovA = 0.
        self.cerenkovB = 0.
        self.beamFile = None
        self.beamType = None
        self.isBad = False
        self.absorber = None
        self.target = None
        self.beamStopper = None
    
    @classmethod
    def fromDictionary( cls, dictionary ):
        run = cls()
        try:
            run.runNumber = int(dictionary['Run No.'])
        except ValueError:
            return
        try:
            run.temperature = float(dictionary['Temperature'])
        except ValueError:
            run.temperature = 0.
        try:
            run.pressure = float(dictionary['Pressure'])
        except ValueError:
            run.pressure = 0.
        try:
            run.beamMomentum = float(dictionary['Beam Momentum [GeV]'])
        except ValueError:
            run.beamMomentum = 0.
        try:
            run.timeStamp = datetime.strptime( dictionary['Timestamp'], '%m/%d/%Y %H:%M:%S' )
        except ValueError:
            run.timeStamp = datetime.min
        try:
            run.nEvents = int(dictionary['Events'])
        except ValueError:
            run.nEvents = 0
        try:
            run.eventsPerSpill = float(dictionary['Events / spill'])
        except ValueError:
            run.eventsPerSpill = 0.
        try:
            run.cerenkovA = float(dictionary['Cerenkov BCA'])
        except ValueError:
            run.cerenkovA = 0.
        try:
            run.cerenkovB = float(dictionary['Cerenkov BCB'])
        except ValueError:
            run.cerenkovB = 0.
            
        if dictionary['Bad run'] == 'bad run':
            run.isBad = True
            
        #run.polarity = polarities[dictionary['Beam Polarity']]
        run.polarity = Polarity(dictionary['Beam Polarity'])
        run.runType = runTypes[dictionary['Run Type']]
        run.trigger = triggers[dictionary['Trigger']]
        run.beamType = beamTypes[dictionary['Beam type']]
        run.beamStopper = beamStoppers[dictionary['Beam Stopper']]
        run.beamFile = dictionary['Beam File']
        run.absorber = absorbers[dictionary['Absorber']]
        run.target = targets[dictionary['Target']]
        run.comment = dictionary['Comment']
        
        # override values in case of noise runs
        if run.runType == RunType.NOISE:
            run.beamMomentum = 0.0
            run.polarity = Polarity.UNKNOWN
        return run
    
    def __str__( self ):
        if self.runType == RunType.NOISE:
            string = '%s : %s (%s)'%( self.runNumber, self.runType, self.timeStamp )
        else:
            string = '%s : %s %s%s GeV (%s)'%( self.runNumber, self.runType, self.polarity, self.beamMomentum, self.timeStamp )
        return string
    
    def isPositive( self ):
        return self.polarity == Polarity.POSITIVE
        
    def isNegative( self ):
        return self.polarity == Polarity.NEGATIVE
