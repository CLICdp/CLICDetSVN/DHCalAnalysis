#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "SubAccessor.hh"

#include "DhcEB.hh"

#include "IMPL/CalorimeterHitImpl.h"
#include "IMPL/LCCollectionVec.h"
#include "IMPL/LCEventImpl.h"
#include "IMPL/LCRunHeaderImpl.h"
#include "IMPL/TrackerDataImpl.h"
#include "IMPL/TrackerHitImpl.h"
#include "IO/LCWriter.h"
#include "IOIMPL/LCFactory.h"
#include "UTIL/BitSet32.h"
#include "UTIL/CellIDEncoder.h"

#include <deque>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
#include <vector>

using std::cerr;
using std::cout;
using std::deque;
using std::endl;
using std::ifstream;
using std::map;
using std::pair;
using std::string;
using std::stringstream;
using std::vector;

using IMPL::CalorimeterHitImpl;
using IMPL::LCCollectionVec;
using IMPL::LCEventImpl;
using IMPL::LCRunHeaderImpl;
using IMPL::TrackerDataImpl;
using IMPL::TrackerHitImpl;

using EVENT::DataNotAvailableException;

class DhcLcioWriter {
    
private:
    class Run {
    public:
        int runNumber;
        string beamPolarity;
        float beamMomentum;
        float temperature;
        float pressure;
        int eventsPerSpill;
        float cerenkovA;
        float cerenkovB;
        string trigger;
        bool isBad;
        string comment;
        string absorber;
        string target;
        string beamFile;
        string timeStamp;
        string runType;
        string beamType;
    };
    
    IO::LCWriter* _writer;
	
	RcdArena* _arena;
	RunReader _reader;

	DhcEB* _dhceb;

	int _runNumber;
	string _runPath;
	string _lcioPath;
	int _minLayers;
	bool _intTrigger;
	int _maxEvents;
	int _tailCatcherStart;
	
	static const int WIRECHAMBERS = 3;
	static const float NS_PER_COUNT = 0.025;
	
	string _detectorName;
	string _runDescription;
	
	map<int, Run*> _runList;
	
	void readTrigger(const RcdRecord& record, LCEventImpl* event);
	void readEvent(const RcdRecord& record, LCEventImpl* event);

public:
	DhcLcioWriter();
	virtual ~DhcLcioWriter();

	bool initialize();
	void record();

	void runNumber(int runNumber);
	void runPath(const string& path);
	void lcioPath(const string& path);
	void minLayers(int nLayers);
	void maxEvents(int nEvents);
	void tailCatcherStart(int tailCatcherStart);
	
	bool parseRunList(const string& path);
};

DhcLcioWriter::DhcLcioWriter() :
        _dhceb(0), _writer(IOIMPL::LCFactory::getInstance()->createLCWriter()), _detectorName("CALICE_DHCAL"), _runDescription("") {
    ;
}

DhcLcioWriter::~DhcLcioWriter() {
    map<int, DhcLcioWriter::Run*>::iterator it;
    for (it = _runList.begin(); it != _runList.end(); ++it) {
        delete it->second;
        it->second = 0;
    }
    _runList.clear();
}

/* 
 * Read the run meta data from a run log spread sheet
 */
bool DhcLcioWriter::parseRunList(const string& path) {
    ifstream infile;
    infile.open(path.c_str());
    if (!infile.is_open()) {
        cerr << "Unable to open run list: " << path << endl;
        return false;
    }
    string line;
    vector<string> entries;
    vector<string> headers;
    vector<string>::iterator itHeader = headers.begin();
    std::getline(infile, line);
    stringstream headerStream(line);
    const char seperator = '\t';
    while(!headerStream.eof()) {
        string entry;
        std::getline(headerStream, entry, seperator);
        headers.push_back(entry);
    }
    while(!infile.eof()) {
        entries.clear();
        itHeader = headers.begin();
        std::getline(infile, line);
        stringstream lineStream(line);
        while(!lineStream.eof()) {
            string entry;
            std::getline(lineStream, entry, seperator);
            entries.push_back(entry);
        }
        //    std::getline(lineStream, run->runNumber, seperator);
        DhcLcioWriter::Run* run  = new Run();
        stringstream polarityMomentum("");
        for (vector<string>::iterator itEntry = entries.begin(); itEntry != entries.end(); ++itEntry, ++itHeader) {
            stringstream entryStream(*itEntry);
            string header = *itHeader;
            if (header.compare("Run No.") == 0) {
                entryStream >> run->runNumber;
            }
            if (header.compare("Timestamp") == 0) {
                run->timeStamp = entryStream.str();
            }
            if (header.compare("Run Type") == 0) {
                run->runType = entryStream.str();
            }
            if (header.compare("Beam Polarity") == 0) {
                polarityMomentum << *itEntry;
            }
            if (header.compare("Beam Momentum [GeV]") == 0) {
                polarityMomentum << *itEntry;
                polarityMomentum >> run->beamMomentum;
            }
            if (header.compare("Trigger") == 0) {
                run->trigger = entryStream.str();
            }
            if (header.compare("Cerenkov BCA") == 0) {
                entryStream >> run->cerenkovA;
            }
            if (header.compare("Cerenkov BCB") == 0) {
                entryStream >> run->cerenkovB;
            }
            if (header.compare("Events / spill") == 0) {
                entryStream >> run->eventsPerSpill;
            }
            if (header.compare("Temperature") == 0) {
                entryStream >> run->temperature;
            }
            if (header.compare("Pressure") == 0) {
                entryStream >> run->pressure;
            }
            if (header.compare("Comment") == 0) {
                run->comment = entryStream.str();
            }
            if (header.compare("Bad run") == 0) {
                if (entryStream.str().compare("bad run") == 0) {
                    run->isBad = true;
                }
            }
            if (header.compare("Beam File") == 0) {
                run->beamFile = entryStream.str();
            }
            if (header.compare("Beam type") == 0) {
                run->beamType = entryStream.str();
            }
            if (header.compare("Absorber") == 0) {
                run->absorber = entryStream.str();
            }
            if (header.compare("Target") == 0) {
                run->target = entryStream.str();
            }
        }
        if (run->runNumber != 0) {
            _runList.insert(pair<int, Run*>(run->runNumber, run));
        } else {
            delete run;
        }
    }
    return true;
}

/*
 * Open the RcdReader, the LCIO writer and write the run header to the LCIO file
 */
bool DhcLcioWriter::initialize() {
	_arena = new RcdArena;
	_reader.directory(_runPath);
	assert(_reader.open(_runNumber));
	
	// determine run type
	while (!_dhceb) {

		_reader.read(*_arena);
		switch (_arena->recordType()) {

		    case RcdHeader::runStart: {
			    // Access the DaqRunStart
			    SubAccessor accessor(*_arena);
			    vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
			    assert(v.size()==1);

			    DaqRunType runType = v[0]->runType();

			    switch (runType.type()) {

			        case DaqRunType::dhcNoise: {
				        _dhceb = new DhcEBIntTrg(_runNumber);
				        _intTrigger = true;
				        break;
			        }
			        case DaqRunType::beamData:
			        case DaqRunType::dhcBeam:
			        case DaqRunType::dhcCosmics: {
				        _dhceb = new DhcEBExtTrg(_runNumber);
				        _intTrigger = false;
				        break;
			        }
			        default: {
				        cerr << "Run type " << runType.typeName() << " not supported" << endl;
				        return false;
				        break;
			        }
			    }
		    }
		    default: {
			    break;
			}
        }
	}

	stringstream sout;
	sout << _lcioPath << "/dhcLcio-Run" << _runNumber << ".slcio";

    _writer->open(sout.str(), EVENT::LCIO::WRITE_NEW);
    
    // write the run header at the beginning of the file
	LCRunHeaderImpl* runHeader = new LCRunHeaderImpl();
	runHeader->setRunNumber(_runNumber);
	runHeader->setDetectorName(_detectorName);
	runHeader->setDescription(_runDescription);
	
	if (_runList.count(_runNumber) == 0) {
    	_runList[_runNumber] = new DhcLcioWriter::Run();
    }
    
    DhcLcioWriter::Run* run = _runList[_runNumber];
	
	runHeader->parameters().setValue("TimeStamp", run->timeStamp);
	runHeader->parameters().setValue("RunType", run->runType);
	runHeader->parameters().setValue("BeamMomentum", run->beamMomentum);
	runHeader->parameters().setValue("Temperature", run->temperature);
	runHeader->parameters().setValue("Pressure", run->pressure);
	runHeader->parameters().setValue("EventsPerSpill", run->eventsPerSpill);
	runHeader->parameters().setValue("CerenkovA", run->cerenkovA);
	runHeader->parameters().setValue("CerenkovB", run->cerenkovB);
	runHeader->parameters().setValue("Trigger", run->trigger);
	runHeader->parameters().setValue("IsBad", run->isBad);
	runHeader->parameters().setValue("Comment", run->comment);
	runHeader->parameters().setValue("Absorber", run->absorber);
	runHeader->parameters().setValue("Target", run->target);
	runHeader->parameters().setValue("BeamFile", run->beamFile);
	
	_writer->writeRunHeader( runHeader );
    
	return true;
}

/*
 * Read a trigger record and add the wire chamber hits to the LCIO event
 */
void DhcLcioWriter::readTrigger(const RcdRecord& record, LCEventImpl* event) {
    //cout << "Reading trigger " << event.getEventNumber() << endl;
    
    SubAccessor accessor(record);
    vector<const BmlLocationData<BmlCaen1290EventData>*> v(accessor.access<BmlLocationData<BmlCaen1290EventData> >());
    
    if (v.empty()) {
	    //cerr << "Found no wire chamber data for event " << event->getEventNumber() << "!" << endl;
	    return;
    }
    
    if (v.size() > 1) {
	    //cerr << "Found more than one set of wire chamber data for event " << event->getEventNumber() << "!" << endl;
	}
    
    const BmlLocationData<BmlCaen1290EventData>* wireChamberData = v.front();
    
    LCCollectionVec* wireChamberRawHits = new LCCollectionVec(EVENT::LCIO::TRACKERDATA);
    wireChamberRawHits->parameters().setValue("ns/count", NS_PER_COUNT);
    UTIL::CellIDEncoder<TrackerDataImpl> idEncoderRaw("vertical:1,left:1,layer:8", wireChamberRawHits);
    
    for (int wireChamber = 0; wireChamber < WIRECHAMBERS; ++wireChamber) {
        // create the raw tracker data
        vector<const BmlCaen1290EventDatum*>::const_iterator itData;
        // each wire chamber reads 4 channels: 0 x-right, 1 x-left, 2 y-right, 3 y-right
        for (int isVertical = 0; isVertical < 2; isVertical++) {
            for (int isLeft = 0; isLeft < 2; isLeft++) {
                int channelIndex = 2*isVertical + isLeft;
                vector<const BmlCaen1290EventDatum*> tdcData = wireChamberData->data()->channelData(wireChamber * 4 + channelIndex);
                for (itData = tdcData.begin(); itData != tdcData.end(); ++itData) {
                    TrackerDataImpl* trackerData = new TrackerDataImpl();
                    trackerData->setTime((double)(*itData)->time() * NS_PER_COUNT);
                    idEncoderRaw.reset();
                    idEncoderRaw["vertical"] = isVertical;
                    idEncoderRaw["left"] = isLeft;
                    idEncoderRaw["layer"] = wireChamber;
                    idEncoderRaw.setCellID(trackerData);
                    wireChamberRawHits->addElement(trackerData);
                }
            }
        }
    }
    event->addCollection(wireChamberRawHits, "WireChamberRawHits");
}

/*
 * Read an event record and add the DHCal hits to the LCIO event
 */
void DhcLcioWriter::readEvent(const RcdRecord& record, LCEventImpl* event) {
    //cout << "Reading event " << event->getEventNumber() << endl;

   	vector<vector<DhcPadPts*>*> pts;
	vector<vector<DhcPadPts*>*>::const_iterator ipts;
	
    unsigned triggerTimeStamp = _dhceb->ttm_tts();
    
    if (!_dhceb->raw_hitdata().empty()) {
		// get copy of events
		pts = _dhceb->built_points();
	}
	
	if (pts.empty()) {
	    //cerr << "Found no hit data for event " << event->getEventNumber() << "!" << endl;
	    _dhceb->clear();
	    return;
	}
	
	if (pts.size() > 1) {
	    //cerr << "Found more than one set of hit data for event " << event->getEventNumber() << "!" << endl;
	}
	
	vector<DhcPadPts*>* dhcPads = pts.front();
	vector<DhcPadPts*>::const_iterator itPad = dhcPads->begin();

    if (_minLayers > _dhceb->layers(dhcPads)) {
        _dhceb->clear();
	    return;
	}

    string cellIdEncoding = "i:8,j:8,layer:8";

    LCCollectionVec* dhcHits = new LCCollectionVec(EVENT::LCIO::CALORIMETERHIT);
    // store position with hits
    dhcHits->setFlag(UTIL::set_bit(dhcHits->getFlag(), EVENT::LCIO::RCHBIT_LONG));
    // store time with hits
    dhcHits->setFlag(UTIL::set_bit(dhcHits->getFlag(), EVENT::LCIO::RCHBIT_TIME));
    // create the id encoder
    UTIL::CellIDEncoder<CalorimeterHitImpl> idEncoder(cellIdEncoding, dhcHits);
    
    // a sub set to store only the hits from the main stack
    LCCollectionVec* dhcMainStackHits = new LCCollectionVec(EVENT::LCIO::CALORIMETERHIT);
    UTIL::CellIDEncoder<CalorimeterHitImpl> idEncoderMainStack(cellIdEncoding, dhcMainStackHits);
    dhcMainStackHits->setSubset();
    
    // a sub set to store only tail catcher hits
    LCCollectionVec* dhcTailCatcherHits = new LCCollectionVec(EVENT::LCIO::CALORIMETERHIT);
    UTIL::CellIDEncoder<CalorimeterHitImpl> idEncoderTailCatcher(cellIdEncoding, dhcTailCatcherHits);
    dhcTailCatcherHits->setSubset();

    for (itPad = dhcPads->begin(); itPad != dhcPads->end(); ++itPad ) {
        DhcPadPts* dhcPad = *itPad;
        int timeBin = dhcPad->Ts - triggerTimeStamp;
        CalorimeterHitImpl* dhcHit = new CalorimeterHitImpl();
	    float position[3] = {(10. * dhcPad->Px), (10. * dhcPad->Py), (10. * dhcPad->Pz)};
	    int layer = dhcPad->Layer - 1; // start layers at 0 instead of 1
	    dhcHit->setPosition(position);
	    dhcHit->setTime(timeBin*100.); // Each time bin is 100 ns
		
		idEncoder.reset();
		idEncoder["layer"] = layer;
    	// 96 pads of 10 mm width
		// 0 0 is left bottom corner
		idEncoder["i"] = (int) ((position[0] + 475.)/10.);
		idEncoder["j"] = (int) ((position[1] + 475.)/10.) ;
		idEncoder.setCellID(dhcHit);
		
		dhcHits->addElement(dhcHit);
	    if (layer < _tailCatcherStart) {
		    dhcMainStackHits->addElement(dhcHit);
		} else {
		    dhcTailCatcherHits->addElement(dhcHit);
		}
    }
    
	event->addCollection(dhcHits, "DhcHits");
	event->addCollection(dhcMainStackHits, "DhcMainStackHits");
	event->addCollection(dhcTailCatcherHits, "DhcTailCatcherHits");
	
	_dhceb->clear();
}

void DhcLcioWriter::record() {
    int eventNumber = 0;
    int eventsWritten = 0;
    UtlTime spillStartTime;
    deque<LCEventImpl*> eventsInSpill;
    LCEventImpl* event;
    while (_reader.read(*_arena)) {
        _dhceb->record(*_arena);
        switch (_arena->recordType()) {
            case RcdHeader::trigger: {
                UtlTime recordTime = _arena->recordTime();
                if (eventsInSpill.empty()) {
                    spillStartTime = recordTime;
                }
                UtlTimeDifference timeInSpill = recordTime - spillStartTime;
                long long eventTime = recordTime.seconds() * 1000000000LL + recordTime.microseconds() * 1000LL;
                event = new LCEventImpl();
                event->setEventNumber(eventNumber++);
			    event->setRunNumber(_runNumber);
    		    event->setTimeStamp(eventTime);
			    event->setDetectorName(_detectorName);
			    event->parameters().setValue("SecondsInSpill", (float) timeInSpill.deltaTime());
			    eventsInSpill.push_back(event);
                readTrigger(*_arena, event);
                break;
            }
            case RcdHeader::event: {
                event = eventsInSpill.front();
                eventsInSpill.pop_front();
                
                UTIL::BitSet32 cerenkovBits = (int) _dhceb->ttm_flag();
                event->parameters().setValue("CerenkovA", cerenkovBits.test(0));
                event->parameters().setValue("CerenkovB", cerenkovBits.test(1));
                
                readEvent(*_arena, event);
                // only write events with dhc hits
                try {
                    event->getCollection("DhcHits");
                    _writer->writeEvent(event);
                    eventsWritten++;
                } catch (DataNotAvailableException& e) {
                    ;
                }
                delete event;
                break;
            }
            default: {
                break;
            }
        }
        if (eventsWritten % 1000 == 0 && eventsWritten > 0) {
	        cout << "Processed " << eventsWritten << " events" << endl;
	    }
	    if (eventsWritten == _maxEvents) {
	        break;
	    }
    }
  	_reader.close();
	_writer->close();
	delete _dhceb;
	delete _arena;
}

void DhcLcioWriter::runNumber(int runNumber) {
    _runNumber = runNumber;
}

void DhcLcioWriter::runPath(const string& path) {
    _runPath = path;
}

void DhcLcioWriter::lcioPath(const string& path) {
    _lcioPath = path;
}

void DhcLcioWriter::minLayers(int nLayers) {
	_minLayers = nLayers;
}

void DhcLcioWriter::maxEvents(int nEvents) {
    _maxEvents = nEvents;
}

void DhcLcioWriter::tailCatcherStart(int tailCatcherStart) {
    _tailCatcherStart = tailCatcherStart;
}


int main(int argc, const char** argv) {
	UtlArguments argh(argc, argv);

	const unsigned runNumber(argh.optionArgument('r', 999999, "Run number"));
	const string runPath(
			argh.optionArgument('f', "data/run",
					"Path where run data is located"));
	const string lcioPath(
			argh.optionArgument('w', "lcio",
					"Path where lcio file is written"));
	const string runListFile(
			argh.optionArgument('L', "DHCalRunList.txt",
					"File name of the run list table"));
	const unsigned minLayers(
			argh.optionArgument('l', 1, "Min hit layers for recorded events"));
	const int maxEvents(argh.optionArgument('n', -1, "Max events processed"));
	const int tailCatcherStart(argh.optionArgument('t', 39, "First tail catcher layer"));

	if (argh.help()) {
		return 0;
	}
	
	DhcLcioWriter* writer = new DhcLcioWriter;
	// set parameters
	writer->runNumber(runNumber);
	writer->runPath(runPath);
	writer->lcioPath(lcioPath);
	writer->minLayers(minLayers);
	writer->maxEvents(maxEvents);
    writer->tailCatcherStart(tailCatcherStart);
	
    if (!writer->parseRunList(runListFile)) {
        return 2;
    }
    
	if (writer->initialize()) {
	    writer->record();
	}
}
