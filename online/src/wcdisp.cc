#include <iostream>
#include <map>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include <TGClient.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGTab.h>

#include "TH1F.h"
#include "TH2F.h"

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

const float MM_PER_NS    = 0.2;
const float NS_PER_COUNT = 0.025;
const float NO_HIT_DATA  = 1e6;

const char *wcnames[] = { "Upstream",
			  "Middle",
			  "Downstream" };

const char *tabnames[] = { "Hit Times",
			   "Hit Counts",
			   "X-Y Profiles",
			   "X-Projections",
			   "Y-Projections" };


class ChkTdcHits : public RcdUserRO {

public:

  virtual ~ChkTdcHits() {}

  std::map<unsigned, std::vector<int>*> rawTimes;
  std::map<unsigned, std::vector<int>*> hitCount;
  std::map<unsigned, std::vector<float>*> hitPos;;


  ChkTdcHits()
    : RcdUserRO(9)
  {}

  bool record(const RcdRecord &r) {

    SubAccessor accessor(r);
  
    switch(r.recordType()) {
    case RcdHeader::runStart: {

      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::dhcBeam:
      case DaqRunType::beamData: {
	break;
      }

      default: {
	std::cerr << "No Wire Chamber data for Run type " << runType.typeName() << std::endl;
	exit(0);
      }
      }
      break;
    }
     
    case RcdHeader::trigger:
    case RcdHeader::event: {

      std::vector<const BmlLocationData<BmlCaen1290EventData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290EventData> >());
 
      std::vector<const BmlCaen1290EventDatum*> cdata[16];

      if (v.size()>0) {
	for (unsigned s(0); s<v.size(); s++) {
	  for (unsigned c(0); c<16; c++) {
	    cdata[c] = v[s]->data()->channelData(c);
	    if (cdata[c].size() > 0) {

	      if (rawTimes.find(c) == rawTimes.end())
		rawTimes[c] = new std::vector<int>;
	      if (hitCount.find(c) == hitCount.end())
		hitCount[c] = new std::vector<int>;

	      for (unsigned h(0); h<cdata[c].size(); h++) {
		rawTimes[c]->push_back((cdata[c])[h]->time());
	      }
	      hitCount[c]->push_back(cdata[c].size());
	    }
	  } 
	}
	for (unsigned w(0); w<12; w+=2) {

	  if (hitPos.find(w/2) == hitPos.end())
	    hitPos[w/2] = new std::vector<float>;

	  // Allow only 1 hit/chan
	  if ((cdata[w].size()) && (cdata[w+1].size())) {
	    float coord(((float)((cdata[w])[0]->time()) -
			 (float)((cdata[w+1])[0]->time())) * NS_PER_COUNT * MM_PER_NS);
	    hitPos[w/2]->push_back(coord);
	  }
	  else {
	    hitPos[w/2]->push_back(NO_HIT_DATA);
	  }
	}
      }
      break;
    }
    default: {  
      break;
    }
    };

    return true;
  }

};


class CMainFrame : public TGMainFrame {

public:
  CMainFrame(const TGWindow *p,UInt_t w,UInt_t h)
    : TGMainFrame(p,w,h)
{
  SetCleanup(kDeepCleanup);
}

private:
void CloseWindow()
{
  gApplication->Terminate();
}

};


int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path of run data"));
  const unsigned width(argh.optionArgument('w',1200,"Width"));
  const unsigned height(argh.optionArgument('v',400,"Height"));
  if(argh.help()) return 0;

  ChkTdcHits cdh;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    cdh.record(*arena);

  reader.close();
  delete arena;

  TApplication _application("Wire Chamber Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("e");

  TGTab               *fTab;
  TRootEmbeddedCanvas *fEc[5];
  TGCompositeFrame    *fF[5];
  TGLayoutHints       *fL[5];

  CMainFrame* mf = new CMainFrame(gClient->GetRoot(), width, height);
  fTab = new TGTab(mf, 300, 300);

  TGCompositeFrame *tf;
  for (unsigned t(0); t<5; t++) {

    tf = fTab->AddTab(tabnames[t]);
    fL[t] = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
			      kLHintsExpandY, 5, 5, 5, 5);
    fF[t] = new TGCompositeFrame(tf, 60, 60, kHorizontalFrame);

    fEc[t] = new TRootEmbeddedCanvas (tabnames[t],fF[t],width,height);
    if (t<2)
      fEc[t]->GetCanvas()->Divide(6,2);
    else
      fEc[t]->GetCanvas()->Divide(3,1);

    fF[t]->AddFrame(fEc[t], fL[t]);
    tf->AddFrame(fF[t], fL[t]);
  }
  fTab->SetTab("X-Y Profiles");
  mf->AddFrame(fTab);

  mf->SetWindowName("Wire Chambers");

  mf->MapSubwindows();
  mf->Resize(mf->GetDefaultSize());
  mf->MapWindow();

  std::map<unsigned, std::vector<int>*>::iterator mit;
  std::map<unsigned, std::vector<float>*>::iterator wit;
  std::map<unsigned, TH1F*> times;
  std::map<unsigned, TH1F*>::iterator iti;
  std::map<unsigned, TH1F*> hits;
  std::map<unsigned, TH1F*>::iterator ihi;
  std::map<unsigned, TH2D*> profile;
  std::map<unsigned, TH2D*>::iterator ipi;
  std::map<unsigned, TH1D*> xProfile;
  std::map<unsigned, TH1D*>::iterator ixi;
  std::map<unsigned, TH1D*> yProfile;
  std::map<unsigned, TH1D*>::iterator iyi;

  for (mit=cdh.rawTimes.begin(); mit!=cdh.rawTimes.end(); mit++) {
    unsigned chan((*mit).first);

    if (times.find(chan) == times.end()) {
      std::ostringstream sout;
      sout << "TDC Chan " << chan;
      times[chan] = new TH1F((sout.str()+" times").c_str(), sout.str().c_str(), 1000, 0, 4000);
    }
    if (hits.find(chan) == hits.end()) {
      std::ostringstream sout;
      sout << "TDC Chan " << chan;
      hits[chan] = new TH1F((sout.str()+" hits").c_str(), sout.str().c_str(), 10, 0, 10);
    }    
  }

  for (mit=cdh.rawTimes.begin(); mit!=cdh.rawTimes.end(); mit++) {
    unsigned chan((*mit).first);

    std::vector<int>::iterator it(cdh.rawTimes[chan]->begin());
    for (; it!=cdh.rawTimes[chan]->end(); it++)
      times[chan]->Fill((*it)*0.025);

    std::vector<int>::iterator ih(cdh.hitCount[chan]->begin());
    for (; ih!=cdh.hitCount[chan]->end(); ih++)
      hits[chan]->Fill((*ih));
  }

  for (wit=cdh.hitPos.begin(); wit!=cdh.hitPos.end(); wit++) {
    unsigned cham((*wit).first);

    if (profile.find(cham) == profile.end()) {
      std::ostringstream sout;
      sout << "Wire Chamber " << wcnames[cham/2];
      profile[cham] = new TH2D((sout.str()+"profile").c_str(), sout.str().c_str(),
			       120, -60, 60, 120, -60, 60);
    }

    std::vector<float>::iterator ipx(cdh.hitPos[cham]->begin());
    std::vector<float>::iterator ipy(cdh.hitPos[cham+1]->begin());
    for (; ipx!=cdh.hitPos[cham]->end() && ipy!=cdh.hitPos[cham+1]->end(); ipx++, ipy++)
      profile[cham]->Fill((*ipx), (*ipy));

    xProfile[cham] = profile[cham]->ProjectionX();
    yProfile[cham] = profile[cham]->ProjectionY();

    wit++;
  }

  Int_t c(0);
  for (iti=times.begin(), ihi=hits.begin(); iti!=times.end(); iti++, ihi++) {

    Int_t cc(2*(c/4) + 6*((c%4)/2) + c%2);

    fEc[0]->GetCanvas()->cd(cc + 1)->SetLogy(1);
    (*iti).second->GetXaxis()->SetTitle("Event tDist (nsec)");
    (*iti).second->Draw();

    fEc[1]->GetCanvas()->cd(cc + 1)->SetLogy(1);
    (*ihi).second->GetXaxis()->SetTitle("Event hitCount");
    (*ihi).second->Draw();

    c++;
  }

  c = 1;
  for (ipi=profile.begin(); ipi!=profile.end(); ipi++) {

    fEc[2]->GetCanvas()->cd(c);
    (*ipi).second->GetXaxis()->SetTitle("X-Pos (mm)");
    (*ipi).second->GetYaxis()->SetTitle("Y-Pos (mm)");
    (*ipi).second->Draw("colz");

    c++;
  }

  c = 1;
  for (ixi=xProfile.begin(), iyi=yProfile.begin(); ixi!=xProfile.end(); ixi++, iyi++) {

    fEc[3]->GetCanvas()->cd(c);
    (*ixi).second->GetXaxis()->SetTitle("X-Pos (mm)");
    (*ixi).second->Draw();

    fEc[4]->GetCanvas()->cd(c);
    (*iyi).second->GetXaxis()->SetTitle("Y-Pos (mm)");
    (*iyi).second->Draw();

    c++;
  }

  _application.Run();

}
