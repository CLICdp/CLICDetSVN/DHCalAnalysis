#include <iostream>
#include <deque>
#include <map>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdWriterBin.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEventData.hh"
#include "DhcFeHitData.hh"

const uint32_t MAXIMUM_SUBRECORD_SIZE = 0XFF00;
const uint32_t DEVICE_BUFFER_SIZE = 1024*1024;
const uint32_t MINIMUM_TRIGGER_DELTA_TIME = 100; // 10 usec
const uint32_t MAXIMUM_TRIGGER_DELTA_TIME = 100000; // 10 msec


class DhcTSOrder : public RcdUserRO {

public:
  DhcTSOrder();
  virtual ~DhcTSOrder();

  bool record(const RcdRecord &r);
  bool check(const RcdRecord &r);
  bool reset(const unsigned &key);
  bool parseEventData(const unsigned &key, DhcLocationData<DhcEventData> *p);

  const std::map<unsigned, unsigned*>& buffers() const;
  unsigned wordCount(const unsigned&);
  unsigned _nEvent;

private:
  std::map<unsigned, unsigned*> _buffers;
  std::map<unsigned, unsigned*> _endata;
  std::map<unsigned, unsigned*> _hitdata;
  std::map<unsigned, std::deque<unsigned*> > _ttsdata;
};

DhcTSOrder::DhcTSOrder()
  : RcdUserRO(9)
{}

DhcTSOrder::~DhcTSOrder()
{
    std::map<unsigned, unsigned*>::const_iterator its(_buffers.begin());
    for (; its!=_buffers.end(); its++) 
      delete [] its->second;;
}

const std::map<unsigned, unsigned*>& DhcTSOrder::buffers() const
{
  return _buffers;
}

unsigned DhcTSOrder::wordCount(const unsigned& key) {

  return (_endata[key] - _buffers[key]);

}

bool DhcTSOrder::check(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {
      for (unsigned s(0); s<v.size(); s++)
	v[s]->print(std::cout) << std::endl;
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

bool DhcTSOrder::record(const RcdRecord &r) {

#ifdef DHC_DEBUG    
  r.RcdHeader::print(std::cout);
#endif // DHC_DEBUG

  bool status(true);
  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::transferStart: {

    std::map<unsigned, unsigned*>::iterator its(_buffers.begin());
    std::map<unsigned, unsigned*>::iterator ite(_endata.begin());

    for (; its!=_buffers.end(); its++, ite++)
      ite->second = its->second;

    _nEvent = 0;
    status = true;
    break;
  }

  case RcdHeader::transferEnd: {

    std::map<unsigned, unsigned*>::const_iterator its(_buffers.begin());

    for (; its!=_buffers.end(); its++) {

      if (!reset(its->first))
	std::cout << " reset failed" << std::endl;
    }

    status = false;
    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {
      for (unsigned s(0); s<v.size(); s++)
	if (v[s]->data()->numberOfWords() > 0) {
	  UtlPack loc(0);
	  loc.byte(3, v[s]->location().crateNumber());
	  loc.byte(2, v[s]->location().slotNumber());

	  if (_buffers.find(loc.word()) == _buffers.end()) {
	    _buffers[loc.word()] = new unsigned[DEVICE_BUFFER_SIZE];
	    _endata[loc.word()] = _buffers[loc.word()];
	  }
	  _endata[loc.word()] = (unsigned*)mempcpy(_endata[loc.word()],
						   v[s]->data()->data(),
						   4*v[s]->data()->numberOfWords());
	}
    }

    _nEvent++;
    status = true;
    break;
  }

  default: {  
    break;
  }
  };

  return status;
}

bool DhcTSOrder::reset(const unsigned& key) {

  if (_buffers.find(key) == _buffers.end())
    return false;

  _hitdata[key] = _buffers[key];
  _ttsdata[key].clear();

  // find first trigger timestamp
  DhcFeHitData* hits = (DhcFeHitData*)(_buffers[key]);
  DhcFeHitData* endata = (DhcFeHitData*)(_endata[key]);

  while (hits < endata) {
    if (hits->trg()) {
      _ttsdata[key].push_back((unsigned*)hits);
      break;
    }
    hits++;
  }

  if (_ttsdata[key].empty())
    return true;

  hits = (DhcFeHitData*)(_ttsdata[key].front());
  unsigned thisevent = hits->timestamp();

  while (hits < endata) {
    if (hits->trg())
      if (abs(hits->timestamp() - thisevent) > MINIMUM_TRIGGER_DELTA_TIME) {
	thisevent = hits->timestamp();
	_ttsdata[key].push_back((unsigned*)hits);
      }
    hits++;
  }

  return true;
}

bool DhcTSOrder::parseEventData(const unsigned& key, DhcLocationData<DhcEventData>* p) {

  if (_ttsdata.find(key) == _ttsdata.end())
    return false;

  if (_ttsdata[key].empty())
    return false;

  UtlPack loc(key);
  DhcLocation _location(loc.byte(3), loc.byte(2), 0);
  p->location(_location);

  DhcFeHitData* substart((DhcFeHitData*)(p->data()->data()));
  DhcFeHitData* subdata(substart);

  DhcFeHitData* begindata((DhcFeHitData*)(_hitdata[key]));
  DhcFeHitData* endata((DhcFeHitData*)(_endata[key]));
  DhcFeHitData* hits((DhcFeHitData*)(_ttsdata[key].front()));

  unsigned thisevent = hits->timestamp();
  _ttsdata[key].pop_front();

  while (hits < endata) {
    unsigned timestamp(hits->timestamp());
    if (hits->trg()) {
      if (abs(thisevent - timestamp) < MINIMUM_TRIGGER_DELTA_TIME)
	memcpy(subdata++, hits, sizeof(DhcFeHitData));
      else
	if (abs(thisevent - timestamp) > MAXIMUM_TRIGGER_DELTA_TIME)
	  break;
    }
    else {
      // don't re-scan out of order data
      if (begindata <= hits) {
	if (abs(thisevent - timestamp) < MINIMUM_TRIGGER_DELTA_TIME)
	  memcpy(subdata++, hits, sizeof(DhcFeHitData));
	else {
	  _hitdata[key] = (unsigned*)hits;
	  break;
	}
      }
    }
    hits++;
  }

  // byte count of hit data,
  uint32_t byteCount((subdata - substart)*sizeof(DhcFeHitData));

  // set number of words in record
  p->data()->numberOfWords(byteCount/4);

  return true;
}

int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const std::string outPath(argh.optionArgument('w',"data/test","Path where test data is written"));

  if(argh.help()) return 0;

  DhcTSOrder dts;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::ostringstream sout;
  sout << outPath << "/Run" << runNumber << ".000";
  RcdWriterBin writer;
  writer.open(sout.str());
  
  while(reader.read(*arena)) {

    if (arena->recordType() != RcdHeader::event)
      writer.write(*arena);

    if (!dts.record(*arena)) {

      // transferEnd, parse event buffers
      std::map<unsigned, unsigned*> buffers(dts.buffers());
      std::map<unsigned, unsigned*>::const_iterator its(buffers.begin());

      UtlTime timer;
      timer.update();
      double start(timer.time());

      for (unsigned i(0); i<dts._nEvent; i++) {

	arena->initialise(RcdHeader::event);
	for (its=buffers.begin(); its!=buffers.end(); its++) {

	  SubInserter inserter(*arena);
	  DhcLocationData<DhcEventData>
	    *p(inserter.insert< DhcLocationData<DhcEventData> >());

	  if (dts.parseEventData(its->first, p))
	    // extend the record
	    inserter.extend(p->data()->numberOfWords()*4);
	  else
	    std::cout << " parseEventData failed" << std::endl;
	}

#ifdef DHC_DEBUG    
	dts.check(*arena);
#endif // DHC_DEBUG

	writer.write(*arena);
      }
      timer.update();
      std::cout << "Elapsed time " << timer.time() - start << " sec"
		<< "  -  " << dts._nEvent << " events"
		<< std::endl;
    }
  }
  reader.close();
  writer.close();
  delete arena;
}

