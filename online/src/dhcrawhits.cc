#include <iostream>
#include <sstream>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "DhcWriterBin.hh"
#include "DhcEB.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  DhcEB* dhceb(0);

  RcdArena* arena(new RcdArena);
  DhcWriterBin writer;

  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::ostringstream sout;
  sout << runNumber;

  assert (writer.open(std::string(runPath+"/Dhc" )+sout.str()));

  // determine run type
  while (!dhceb) {

    reader.read(*arena);

    switch (arena->recordType()) {

    case RcdHeader::runStart: {
      // Access the DaqRunStart
      SubAccessor accessor(*arena);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::dhcNoise: {
	dhceb = new DhcEBIntTrg(runNumber);
	break;
      }

      case DaqRunType::beamData:
      case DaqRunType::dhcBeam:
      case DaqRunType::dhcQinj:
      case DaqRunType::dhcCosmics: {
	dhceb = new DhcEBExtTrg(runNumber);
	break;
      }

      default: {
	std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
	break;
	}
      }
    }
    default:
      break;
    }
  }

  while(reader.read(*arena)) {

    dhceb->record(*arena);

    if (!dhceb->raw_hitdata().empty())
      writer.convert(dhceb->raw_hitdata());

    // clear events
    dhceb->clear();
  }

  reader.close();
  writer.close();

  delete arena;
}
