#include <iostream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <bitset>

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TCanvas.h"

#include <TGClient.h>
#include <TGButton.h>
#include <TGButtonGroup.h>
#include <TGLabel.h>
#include <TGNumberEntry.h>
#include <TGFrame.h>
#include <TGTab.h>

#include "UtlArguments.hh"

#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdWriterAsc.hh"
#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "SubModifier.hh"
#include "SubInserter.hh"

#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcFeConfigurationData.hh"
#include "DhcReadoutConfigurationData.hh"
#include "DhcTriggerConfigurationData.hh"


class CDispFrame : public RcdUserRW, public TGMainFrame {

  enum RecordIdentifiers {
    R_READOUT = 1,
    R_TRIGGER,
    R_DCONS,
    R_DCONAD,
    R_DCALS,
    R_DCALAD,
  };

  enum TriggerIdentifiers {
    ID_TR_CRATE = (R_TRIGGER<<28),
    ID_TR_POLL,
    ID_TR_MODE_WAIT,
    ID_TR_MODE_INT,
    ID_TR_MODE_EXT,
  };

  enum DConIdentifiers {
    ID_DC_TTS_ENA = (R_DCONS<<28),
    ID_DC_ZS_ENA,
    ID_DC_TWIDTH,
    ID_DC_TDELAY,
    ID_DC_CHIP_ENA,
    ID_DC_CRATE = (R_DCONAD<<28),
    ID_DC_SLOT,
    ID_DC_FESLOT,
    ID_DC_ADD,
    ID_DC_DEL,
  };

  enum DCalIdentifiers {
    ID_FE_REGS = (R_DCALS<<28),
    ID_FE_KILL = ID_FE_REGS+8,
    ID_FE_INJ = ID_FE_KILL+8,
    ID_FE_CAPSEL = ID_FE_INJ+8,
    ID_FE_GAIN = ID_FE_CAPSEL+8,
    ID_FE_BASELINE = ID_FE_GAIN+8,
    ID_FE_ACCEPT = ID_FE_BASELINE+8,
    ID_FE_PIPELINE = ID_FE_ACCEPT+8,
    ID_FE_TRIGGER = ID_FE_PIPELINE+8,
    ID_FE_CRATE = (R_DCALAD<<28),
    ID_FE_SLOT,
    ID_FE_FESLOT,
    ID_FE_FECHIP,
    ID_FE_ADD,
    ID_FE_DEL,
  };

private:
  TGTab               *fTab;

  std::vector<DhcReadoutConfigurationData*> _vRo;
  std::vector<DhcTriggerConfigurationData*> _vTr;
  std::vector<TtmLocationData<TtmConfigurationData>*> _vTt;
  std::vector<DhcLocationData<DhcBeConfigurationData>*> _vBe;
  std::vector<DhcLocationData<DhcDcConfigurationData>*> _vDc;
  std::vector<DhcLocationData<DhcFeConfigurationData>*> _vFe;

  virtual void CloseWindow();

  TGCompositeFrame* readout(TGCompositeFrame* tf);
  TGCompositeFrame* trigger(TGCompositeFrame* tf);
  TGCompositeFrame* dcons(TGCompositeFrame* tf);
  TGCompositeFrame* dcals(TGCompositeFrame* tf);
  TGGroupFrame* dcon(TGCompositeFrame* tf, const DhcLocationData<DhcDcConfigurationData>* dc, int i);
  TGGroupFrame* dcal(TGCompositeFrame* tf, const DhcLocationData<DhcFeConfigurationData>* fe, int i);
  TGGroupFrame* dconad(TGCompositeFrame* tf);
  TGGroupFrame* dcalad(TGCompositeFrame* tf);

  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
  std::map<unsigned, TGNumberEntry*> _neMap;
  std::map<unsigned, TGCheckButton*> _cbMap;
  std::map<unsigned, TGRadioButton*> _rbMap;
  std::map<unsigned, UtlPack> _locMap;

  void readoutEdit(Long_t msg, Long_t parm);
  void triggerEdit(Long_t msg, Long_t parm);
  void dconsEdit(Long_t msg, Long_t parm);
  void dcalsEdit(Long_t msg, Long_t parm);
  void dconsAD(Long_t msg, Long_t parm);
  void dcalsAD(Long_t msg, Long_t parm);

  std::string _file;
  int _refresh;

public:
  CDispFrame(const TGWindow *p,UInt_t w,UInt_t h,
	     const std::string& name);
  virtual ~CDispFrame();

  bool record(RcdRecord &r);
  void display(int& index);
  bool update(RcdRecord &r);
  bool beUpdate();
  int refresh();

};

CDispFrame::CDispFrame(const TGWindow *p,UInt_t w,UInt_t h,
		       const std::string& name)
  : TGMainFrame(p,w,h),_file(name),_refresh(0)
{
  SetCleanup(kDeepCleanup);
}

CDispFrame::~CDispFrame()
{}

void CDispFrame::CloseWindow()
{
  gApplication->Terminate(1);
}

TGCompositeFrame*
CDispFrame::readout(TGCompositeFrame* tf)
{
  TGGroupFrame* crate[2];
  TGCheckButton* dcon[12];
  TGCompositeFrame* slot[20];
  TGLabel* sl[20];

  TGCompositeFrame* f = new TGCompositeFrame(tf, 1, 1, kVerticalFrame);

  for (unsigned c(0); c<_vRo.size(); c++) {

    std::ostringstream sout;
    sout << "Crate " << static_cast<unsigned>(_vRo[c]->crateNumber());

    crate[c] = new TGGroupFrame(f, sout.str().c_str(), kHorizontalFrame);

    unsigned baseId(R_READOUT<<28 | c<<8);
    for (unsigned s(4); s<20; s++) {

      slot[s] = new TGCompositeFrame(crate[c], 1, 1, kVerticalFrame);

      std::ostringstream sout;
      sout << "slot " << s;
      sl[s] = new TGLabel(slot[s], sout.str().c_str());

      slot[s]->AddFrame(sl[s]);

      for (unsigned i(0); i<12; i++) {
	UtlPack loc(0);
	loc.byte(3, _vRo[c]->crateNumber());
	loc.byte(2, s);
	loc.byte(1, i);
	_locMap[baseId] = loc;

	std::ostringstream sout;
	sout << "dc" << i;

	dcon[i] = new TGCheckButton(slot[s], sout.str().c_str(), baseId);
	_cbMap[baseId++] = dcon[i];
	dcon[i]->Associate(this);
	if (_vRo[c]->slotFeEnable(s, i))
	  dcon[i]->SetOn();
	slot[s]->AddFrame(dcon[i]);
      }
      crate[c]->AddFrame(slot[s]);
    }
    f->AddFrame(crate[c]);
  }
  return f;
}

TGCompositeFrame*
CDispFrame::trigger(TGCompositeFrame* tf)
{
  TGCompositeFrame* general;

  TGCompositeFrame* f = new TGCompositeFrame(tf, 1, 1, kVerticalFrame);
  general = new TGCompositeFrame(f);

  TGCompositeFrame* chf = new TGCompositeFrame(general, 1, 1, kHorizontalFrame);

  TGLabel* crateLabel = new TGLabel(chf, "Crate");
  TGNumberEntry* crateEntry = new TGNumberEntry(chf, _vTr[0]->crateNumber(), 8, ID_TR_CRATE,
						TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEAPositive,
						TGNumberFormat::kNELLimitMinMax,
						0,255);
  _neMap[ID_TR_CRATE] = crateEntry;
  crateEntry->Associate(this);
  chf->AddFrame(crateLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  chf->AddFrame(crateEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* phf = new TGCompositeFrame(general, 1, 1, kHorizontalFrame);

  TGLabel* pollLabel = new TGLabel(phf, "Poll Interval");
  double pollInterval(_vTr[0]->pollInterval().deltaTime());
  TGNumberEntry* pollEntry = new TGNumberEntry(phf, pollInterval, 8, ID_TR_POLL,
					      TGNumberFormat::kNESReal,
					      TGNumberFormat::kNEAPositive,
					      TGNumberFormat::kNELLimitMinMax,
					      0.0001,10.00);
  _neMap[ID_TR_POLL] = pollEntry;
  pollEntry->Associate(this);
  phf->AddFrame(pollLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  phf->AddFrame(pollEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  general->AddFrame(chf);
  general->AddFrame(phf);
  f->AddFrame(general);

  TGVButtonGroup* triggerMode = new TGVButtonGroup(f, "Trigger Mode");
  TGRadioButton* mode[3];
  unsigned modeid[3];
  for (unsigned i(0); i<3; i++)
    modeid[i] = ID_TR_MODE_WAIT+i;

  mode[0] = new TGRadioButton(triggerMode, "By Waiting", modeid[0]);
  if (_vTr[0]->triggerByWaiting())
    mode[0]->SetOn();
  mode[1] = new TGRadioButton(triggerMode, "Internally", modeid[1]);
  if (_vTr[0]->triggerInternally())
    mode[1]->SetOn();
  mode[2] = new TGRadioButton(triggerMode, "Externally", modeid[2]);
  if (_vTr[0]->triggerExternally())
    mode[2]->SetOn();

  for (unsigned i(0); i<3; i++) {
    _rbMap[modeid[i]] = mode[i];
    mode[i]->Associate(this);
  }
  f->AddFrame(triggerMode, new TGLayoutHints(kLHintsLeft, 5,5,5,5));
  return f;
}

TGCompositeFrame*
CDispFrame::dcons(TGCompositeFrame* tf)
{
  TGCompositeFrame* f = new TGCompositeFrame(tf, 1, 1, kHorizontalFrame);

  int index(0);
  std::vector<DhcLocationData<DhcDcConfigurationData>*>::const_iterator it;
  for (it=_vDc.begin(); it!=_vDc.end(); it++) {
    
    TGGroupFrame* dcF = dcon(f, *it, index++);
    f->AddFrame(dcF);
  }
  TGGroupFrame* dcF = dconad(f);
  f->AddFrame(dcF);
  return f;
}

TGCompositeFrame*
CDispFrame::dcals(TGCompositeFrame* tf)
{
  TGCompositeFrame* f = new TGCompositeFrame(tf, 1, 1, kHorizontalFrame);

  int index(0);
  std::vector<DhcLocationData<DhcFeConfigurationData>*>::const_iterator it;
  for (it=_vFe.begin(); it!=_vFe.end(); it++) {
    
    TGGroupFrame* feF = dcal(f, *it, index++);
    f->AddFrame(feF);
  }
  TGGroupFrame* feF = dcalad(f);
  f->AddFrame(feF);
  return f;
}

TGGroupFrame*
CDispFrame::dconad(TGCompositeFrame* tf)
{
  TGGroupFrame* dataCon = new TGGroupFrame(tf, "Add/Delete Data Concentrator");
  TGGroupFrame* location = new TGGroupFrame(dataCon);

  TGCompositeFrame* cf = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* crateLabel = new TGLabel(cf, "Crate");

  unsigned cid(ID_DC_CRATE);
  TGNumberEntry* crateEntry = new TGNumberEntry(cf, 220, 4, cid,
						TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEAPositive,
						TGNumberFormat::kNELLimitMinMax,
						220,221);
  _neMap[cid] = crateEntry;
  crateEntry->Associate(this);

  cf->AddFrame(crateLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  cf->AddFrame(crateEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* sf = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* slotLabel = new TGLabel(sf, "Slot");

  unsigned sid(ID_DC_SLOT);
  TGNumberEntry* slotEntry = new TGNumberEntry(sf, 4, 4, sid,
					       TGNumberFormat::kNESInteger,
					       TGNumberFormat::kNEAPositive,
					       TGNumberFormat::kNELLimitMinMax,
					       4,19);
  _neMap[sid] = slotEntry;
  slotEntry->Associate(this);

  sf->AddFrame(slotLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  sf->AddFrame(slotEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* fef = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* feslotLabel = new TGLabel(fef, "feSlot");

  unsigned fid(ID_DC_FESLOT);
  TGNumberEntry* feslotEntry = new TGNumberEntry(fef, 0, 4, fid,
						 TGNumberFormat::kNESInteger,
						 TGNumberFormat::kNEANonNegative,
						 TGNumberFormat::kNELLimitMinMax,
						 0,11);
  _neMap[fid] = feslotEntry;
  feslotEntry->Associate(this);

  fef->AddFrame(feslotLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  fef->AddFrame(feslotEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  location->AddFrame(cf);
  location->AddFrame(sf);
  location->AddFrame(fef);

  TGCompositeFrame* action = new TGCompositeFrame(dataCon, 1, 1, kHorizontalFrame);

  TGTextButton* add = new TGTextButton(action, "&Add", ID_DC_ADD);
  add->Associate(this);
  action->AddFrame(add, new TGLayoutHints(kLHintsLeft, 4,4,4,4));

  TGTextButton* del = new TGTextButton(action, "&Delete", ID_DC_DEL);
  del->Associate(this);
  action->AddFrame(del, new TGLayoutHints(kLHintsLeft, 4,4,4,4));

  dataCon->AddFrame(location);
  dataCon->AddFrame(action);

  return dataCon;
}

TGGroupFrame*
CDispFrame::dcon(TGCompositeFrame* tf, const DhcLocationData<DhcDcConfigurationData>* dc, int index)
{
  TGCompositeFrame* row[4];
  TGCheckButton* chip[24];

  std::ostringstream sout;
  sout << " Crate " << static_cast<unsigned>(dc->location().crateNumber());
  if (dc->location().slotBroadcast())
    sout << " Slot broadcast";
  else
    sout << " Slot " << static_cast<unsigned>(dc->location().slotNumber());
  sout << " " << dc->location().dhcComponentName();

  TGGroupFrame* dataCon = new TGGroupFrame(tf, sout.str().c_str());

  UtlPack loc(0);
  loc.byte(3, dc->location().crateNumber());
  loc.byte(2, dc->location().slotNumber());
  loc.byte(1, dc->location().dhcComponent());

  TGGroupFrame* control = new TGGroupFrame(dataCon);

  unsigned tid(ID_DC_TTS_ENA | index<<8);
  TGCheckButton* ttsEnable = new TGCheckButton(control, "Trigger Timestamp enable", tid);
  _cbMap[tid] = ttsEnable;
  _locMap[tid] = loc;
  ttsEnable->Associate(this);
  if (dc->data()->controlTriggerTimestampEnable())
    ttsEnable->SetOn();
  control->AddFrame(ttsEnable);

  unsigned zid(ID_DC_ZS_ENA | index<<8);
  TGCheckButton* zsEnable = new TGCheckButton(control, "Zero Suppression enable", zid);
  _cbMap[zid] = zsEnable;
  _locMap[zid] = loc;
  zsEnable->Associate(this);
  if (dc->data()->controlZeroSuppressionEnable())
    zsEnable->SetOn();
  control->AddFrame(zsEnable);
  
  TGCompositeFrame* whf = new TGCompositeFrame(control, 1, 1, kHorizontalFrame);

  TGLabel* widthLabel = new TGLabel(whf, "Trigger Width");
  unsigned triggerWidth(*dc->data()->triggerWidth());
  unsigned wid(ID_DC_TWIDTH | index<<8);
  TGNumberEntry* widthEntry = new TGNumberEntry(whf, triggerWidth, 2, wid,
						TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEAPositive,
						TGNumberFormat::kNELLimitMinMax,
						0,7);
  _neMap[wid] = widthEntry;
  _locMap[wid] = loc;
  widthEntry->Associate(this);

  whf->AddFrame(widthLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  whf->AddFrame(widthEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* dhf = new TGCompositeFrame(control, 1, 1, kHorizontalFrame);

  TGLabel* delayLabel = new TGLabel(dhf, "Trigger Delay");
  unsigned triggerDelay(*dc->data()->triggerDelay());
  unsigned did(ID_DC_TDELAY | index<<8);
  TGNumberEntry* delayEntry = new TGNumberEntry(dhf, triggerDelay, 2, did,
						TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEANonNegative,
						TGNumberFormat::kNELLimitMinMax,
						0,19);
  _neMap[did] = delayEntry;
  _locMap[did] = loc;
  delayEntry->Associate(this);

  dhf->AddFrame(delayLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  dhf->AddFrame(delayEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  control->AddFrame(whf);
  control->AddFrame(dhf);

  TGGroupFrame* enable = new TGGroupFrame(dataCon, "Chip enable", kHorizontalFrame);
  unsigned cid(0);
  const unsigned char* cer(dc->data()->enable());
  std::bitset<24> cen((cer[2]<<16) | (cer[1]<<8) | (cer[0]));

  for (unsigned s(0); s<4; s++) {

    row[s] = new TGCompositeFrame(enable, 1, 1, kVerticalFrame);

    for (unsigned i(0); i<6; i++) {

      std::ostringstream sout;
      sout << cid;

      unsigned id(ID_DC_CHIP_ENA+cid | index<<8);
      chip[cid] = new TGCheckButton(row[s], sout.str().c_str(), id);
      _cbMap[id] = chip[cid];
      _locMap[id] = loc;
      chip[cid]->Associate(this);
      if (cen.test(cid))
	chip[cid]->SetOn();
      row[s]->AddFrame(chip[cid]);
      cid++;
    }
    enable->AddFrame(row[s]);
  }

  dataCon->AddFrame(control);
  dataCon->AddFrame(enable);

  return dataCon;
}

TGGroupFrame*
CDispFrame::dcalad(TGCompositeFrame* tf)
{
  TGGroupFrame* dCal = new TGGroupFrame(tf, "Add/Delete DCal ASCI");
  TGGroupFrame* location = new TGGroupFrame(dCal);

  TGCompositeFrame* cf = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* crateLabel = new TGLabel(cf, "Crate");

  unsigned cid(ID_FE_CRATE);
  TGNumberEntry* crateEntry = new TGNumberEntry(cf, 220, 4, cid,
						TGNumberFormat::kNESInteger,
						TGNumberFormat::kNEAPositive,
						TGNumberFormat::kNELLimitMinMax,
						220,221);
  _neMap[cid] = crateEntry;
  crateEntry->Associate(this);

  cf->AddFrame(crateLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  cf->AddFrame(crateEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* sf = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* slotLabel = new TGLabel(sf, "Slot");

  unsigned sid(ID_FE_SLOT);
  TGNumberEntry* slotEntry = new TGNumberEntry(sf, 4, 4, sid,
					       TGNumberFormat::kNESInteger,
					       TGNumberFormat::kNEAPositive,
					       TGNumberFormat::kNELLimitMinMax,
					       4,19);
  _neMap[sid] = slotEntry;
  slotEntry->Associate(this);

  sf->AddFrame(slotLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  sf->AddFrame(slotEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* fef = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* feslotLabel = new TGLabel(fef, "feSlot");

  unsigned fid(ID_FE_FESLOT);
  TGNumberEntry* feslotEntry = new TGNumberEntry(fef, 0, 4, fid,
						 TGNumberFormat::kNESInteger,
						 TGNumberFormat::kNEANonNegative,
						 TGNumberFormat::kNELLimitMinMax,
						 0,11);
  _neMap[fid] = feslotEntry;
  feslotEntry->Associate(this);

  fef->AddFrame(feslotLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  fef->AddFrame(feslotEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  TGCompositeFrame* af = new TGCompositeFrame(location, 1, 1, kHorizontalFrame);
  TGLabel* asicLabel = new TGLabel(af, "chipID");

  unsigned aid(ID_FE_FECHIP);
  TGNumberEntry* asicEntry = new TGNumberEntry(af, 0, 4, aid,
					       TGNumberFormat::kNESInteger,
					       TGNumberFormat::kNEANonNegative,
					       TGNumberFormat::kNELLimitMinMax,
					       0,26);
  _neMap[aid] = asicEntry;
  asicEntry->Associate(this);

  af->AddFrame(asicLabel, new TGLayoutHints(kLHintsRight, 5,5,5));
  af->AddFrame(asicEntry, new TGLayoutHints(kLHintsLeft, 5,5,0,5));

  location->AddFrame(cf);
  location->AddFrame(sf);
  location->AddFrame(fef);
  location->AddFrame(af);

  TGCompositeFrame* action = new TGCompositeFrame(dCal, 1, 1, kHorizontalFrame);

  TGTextButton* add = new TGTextButton(action, "&Add", ID_FE_ADD);
  add->Associate(this);
  action->AddFrame(add, new TGLayoutHints(kLHintsLeft, 4,4,4,4));

  TGTextButton* del = new TGTextButton(action, "&Delete", ID_FE_DEL);
  del->Associate(this);
  action->AddFrame(del, new TGLayoutHints(kLHintsLeft, 4,4,4,4));

  dCal->AddFrame(location);
  dCal->AddFrame(action);

  return dCal;
}

TGGroupFrame*
CDispFrame::dcal(TGCompositeFrame* tf, const DhcLocationData<DhcFeConfigurationData>* fe, int index)
{
  TGCompositeFrame* col[3];

  TGCompositeFrame* reg[8];
  TGLabel* regLabel[8];
  TGNumberEntry* regEntry[8];

  TGCompositeFrame* regKill[8];
  TGNumberEntry* regKillEntry[8];
  TGCompositeFrame* regInject[8];
  TGNumberEntry* regInjectEntry[8];

  std::vector<std::string> label(8);
  label[0] = "ChipID";
  label[1] = "PLSR";
  label[2] = "IntD";
  label[3] = "Shp2D";
  label[4] = "Shp1D";
  label[5] = "BlrD";
  label[6] = "VtnD";
  label[7] = "VtpD";
;
  std::vector<unsigned> value(8);
  value[0] = *fe->data()->chipid();
  value[1] = *fe->data()->plsr();
  value[2] = *fe->data()->intd();
  value[3] = *fe->data()->shp2();
  value[4] = *fe->data()->shp1();
  value[5] = *fe->data()->blrd();
  value[6] = *fe->data()->vtnd();
  value[7] = *fe->data()->vtpd();

  std::ostringstream sout;
  sout << " Crate " << static_cast<unsigned>(fe->location().crateNumber());
  if (fe->location().slotBroadcast())
    sout << " Slot broadcast";
  else
    sout << " Slot " << static_cast<unsigned>(fe->location().slotNumber());
  sout << " " << fe->location().dhcComponentName();

  TGGroupFrame* dCal = new TGGroupFrame(tf, sout.str().c_str());

  UtlPack loc(0);
  loc.byte(3, fe->location().crateNumber());
  loc.byte(2, fe->location().slotNumber());
  loc.byte(1, fe->location().dhcComponent());
  loc.byte(0, *fe->data()->chipid());
  
  TGCompositeFrame* hf = new TGCompositeFrame(dCal, 1, 1, kHorizontalFrame);

  col[0] = new TGCompositeFrame(hf, 1, 1, kVerticalFrame);
  for (unsigned i(0); i<8; i++) {

    reg[i] = new TGCompositeFrame(col[0], 1, 1, kHorizontalFrame);

    regLabel[i] = new TGLabel(reg[i], label[i].c_str());
    unsigned id(ID_FE_REGS+i | index<<8);
    regEntry[i] = new TGNumberEntry(reg[i], value[i], 3, id,
				    TGNumberFormat::kNESInteger,
				    TGNumberFormat::kNEANonNegative,
				    TGNumberFormat::kNELLimitMinMax,
				    0,255);
    _neMap[id] = regEntry[i];
    _locMap[id] = loc;
    regEntry[i]->Associate(this);

    reg[i]->AddFrame(regLabel[i], new TGLayoutHints(kLHintsRight, 2,2,2,2));
    reg[i]->AddFrame(regEntry[i], new TGLayoutHints(kLHintsLeft, 2,2,2,2));

    col[0]->AddFrame(reg[i]);
  }

  col[1] = new TGCompositeFrame(hf, 1, 1, kVerticalFrame);
  for (unsigned i(0); i<8; i++) {

    regKill[i] = new TGCompositeFrame(col[1], 1, 1, kHorizontalFrame);

    std::ostringstream sout;
    sout << "Kill" << i;
    TGLabel* killLabel = new TGLabel(regKill[i], sout.str().c_str());
    unsigned kill(*(fe->data()->kill()+i));
    unsigned id(ID_FE_KILL+i | index<<8);
    regKillEntry[i] = new TGNumberEntry(regKill[i], kill, 3, id,
					TGNumberFormat::kNESHex,
					TGNumberFormat::kNEANonNegative,
					TGNumberFormat::kNELLimitMinMax,
					0,255);
    _neMap[id] = regKillEntry[i];
    _locMap[id] = loc;
    regKillEntry[i]->Associate(this);

    regKill[i]->AddFrame(killLabel, new TGLayoutHints(kLHintsRight, 2,2,2,2));
    regKill[i]->AddFrame(regKillEntry[i], new TGLayoutHints(kLHintsLeft, 2,2,2,2));

    col[1]->AddFrame(regKill[i]);
  }

  col[2] = new TGCompositeFrame(hf, 1, 1, kVerticalFrame);
  for (unsigned i(0); i<8; i++) {

    regInject[i] = new TGCompositeFrame(col[2], 1, 1, kHorizontalFrame);

    std::ostringstream sout;
    sout << "Inject" << i;
    TGLabel* injectLabel = new TGLabel(regInject[i], sout.str().c_str());
    unsigned inj(*(fe->data()->inj()+i));
    unsigned id(ID_FE_INJ+i | index<<8);
    regInjectEntry[i] = new TGNumberEntry(regInject[i], inj, 3, id,
					  TGNumberFormat::kNESHex,
					  TGNumberFormat::kNEANonNegative,
					  TGNumberFormat::kNELLimitMinMax,
					  0,255);
    _neMap[id] = regInjectEntry[i];
    _locMap[id] = loc;
    regInjectEntry[i]->Associate(this);

    regInject[i]->AddFrame(injectLabel, new TGLayoutHints(kLHintsRight, 2,2,2,2));
    regInject[i]->AddFrame(regInjectEntry[i], new TGLayoutHints(kLHintsLeft, 2,2,2,2));

    col[2]->AddFrame(regInject[i]);
  }

  hf->AddFrame(col[0]);
  hf->AddFrame(col[1]);
  hf->AddFrame(col[2]);

  dCal->AddFrame(hf);

  TGGroupFrame* dcr = new TGGroupFrame(dCal, "DCR");

  TGHButtonGroup* capSelect = new TGHButtonGroup(dcr, "Shaping Time (ns)");
  TGRadioButton* fCS[4];
  unsigned csid(ID_FE_CAPSEL | index<<8);
  fCS[0] = new TGRadioButton(capSelect, new TGHotString("65 "), csid);
  fCS[1] = new TGRadioButton(capSelect, new TGHotString("85 "), csid+1);
  fCS[2] = new TGRadioButton(capSelect, new TGHotString("100 "), csid+2);
  fCS[3] = new TGRadioButton(capSelect, new TGHotString("125 "), csid+3);
  for (unsigned i(0);i<4; i++) {
    _rbMap[csid+i] = fCS[i];
    _locMap[csid+i] = loc;
    fCS[i]->Associate(this);
  }
  fCS[fe->data()->dcrCapSelect()]->SetOn();
  dcr->AddFrame(capSelect);

  TGHButtonGroup* gainSelect = new TGHButtonGroup(dcr, "Gain");
  TGRadioButton* fG[2];
  unsigned gid(ID_FE_GAIN | index<<8);
  fG[0] = new TGRadioButton(gainSelect, new TGHotString("Low-RPC "), gid);
  fG[1] = new TGRadioButton(gainSelect, new TGHotString("High-GEM "), gid+1);
  for (unsigned i(0);i<2; i++) {
    _rbMap[gid+i] = fG[i];
    _locMap[gid+i] = loc;
    fG[i]->Associate(this);
  }
  if (fe->data()->dcrLowGain())
    fG[0]->SetOn();
  else
    fG[1]->SetOn();
  dcr->AddFrame(gainSelect);

  TGHButtonGroup* baseSelect = new TGHButtonGroup(dcr, "Baseline Restore");
  TGRadioButton* fB[2];
  unsigned bid(ID_FE_BASELINE | index<<8);
  fB[0] = new TGRadioButton(baseSelect, new TGHotString("Disabled "), bid);
  fB[1] = new TGRadioButton(baseSelect, new TGHotString("Enabled "), bid+1);
  for (unsigned i(0);i<2; i++) {
    _rbMap[bid+i] = fB[i];
    _locMap[bid+i] = loc;
    fB[i]->Associate(this);
  }
  if (fe->data()->dcrDisableBaselineRestore())
    fB[0]->SetOn();
  else
    fB[1]->SetOn();
  dcr->AddFrame(baseSelect);

  TGHButtonGroup* maskSelect = new TGHButtonGroup(dcr, "Mask");
  TGRadioButton* fM[2];
  unsigned mid(ID_FE_ACCEPT | index<<8);
  fM[0] = new TGRadioButton(maskSelect, new TGHotString("Accept "), mid);
  fM[1] = new TGRadioButton(maskSelect, new TGHotString("Mask "), mid+1);
  for (unsigned i(0);i<2; i++) {
    _rbMap[mid+i] = fM[i];
    _locMap[mid+i] = loc;
    fM[i]->Associate(this);
  }
  if (fe->data()->dcrAccept())
    fM[0]->SetOn();
  else
    fM[1]->SetOn();
  dcr->AddFrame(maskSelect);

  TGHButtonGroup* pipeSelect = new TGHButtonGroup(dcr, "Pipeline");
  TGRadioButton* fP[2];
  unsigned pid(ID_FE_PIPELINE | index<<8);
  fP[0] = new TGRadioButton(pipeSelect, new TGHotString("Enabled "), pid);
  fP[1] = new TGRadioButton(pipeSelect, new TGHotString("Disabled "), pid+1);
  for (unsigned i(0);i<2; i++) {
    _rbMap[pid+i] = fP[i];
    _locMap[pid+i] = loc;
    fP[i]->Associate(this);
  }
  if (fe->data()->dcrActivatePipeline())
    fP[0]->SetOn();
  else
    fP[1]->SetOn();
  dcr->AddFrame(pipeSelect);

  TGHButtonGroup* trigSelect = new TGHButtonGroup(dcr, "Trigger");
  TGRadioButton* fT[2];
  unsigned tid(ID_FE_TRIGGER | index<<8);
  fT[0] = new TGRadioButton(trigSelect, new TGHotString("External "), tid);
  fT[1] = new TGRadioButton(trigSelect, new TGHotString("Internal "), tid+1);
  for (unsigned i(0);i<2; i++) {
    _rbMap[tid+i] = fT[i];
    _locMap[tid+i] = loc;
    fT[i]->Associate(this);
  }
  if (fe->data()->dcrExternalTrigger())
    fT[0]->SetOn();
  else
    fT[1]->SetOn();
  dcr->AddFrame(trigSelect);

  dCal->AddFrame(dcr);

  return dCal;
}

Bool_t
CDispFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{
  UtlPack id(parm1);
  unsigned recordType(id.bits(28,31));

  switch (recordType) {

  case R_READOUT:
    readoutEdit(msg, parm1);
    break;

  case R_TRIGGER:
    triggerEdit(msg, parm1);
    break;

  case R_DCONS:
    dconsEdit(msg, parm1);
    break;

  case R_DCONAD:
    dconsAD(msg, parm1);
    break;

  case R_DCALS:
    dcalsEdit(msg, parm1);
    break;

  case R_DCALAD:
    dcalsAD(msg, parm1);
    break;

  default:
    break;
  };

  return kTRUE;
}

void
CDispFrame::readoutEdit(Long_t msg, Long_t parm)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_CHECKBUTTON: {

      UtlPack id(parm);
      TGCheckButton* cb = _cbMap[parm];

      for (unsigned c(0); c<_vRo.size(); c++)
	if (_vRo[c]->crateNumber() == _locMap[parm].byte(3)) {
	  _vRo[c]->slotFeEnable(_locMap[parm].byte(2), _locMap[parm].byte(1), cb->IsOn());
	  _vRo[c]->slotEnable(_locMap[parm].byte(2),
			      _vRo[c]->slotFeEnables(_locMap[parm].byte(2)) > 0 ? true : false);
	}
      break;
    }

    default:
      break;
    }

  default:
    break;
  }
}

void
CDispFrame::triggerEdit(Long_t msg, Long_t parm)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_RADIOBUTTON: {
      TGRadioButton* rb = _rbMap[parm];
      if (parm == ID_TR_MODE_WAIT)
	_vTr[0]->triggerByWaiting(rb->IsOn());
      else if (parm == ID_TR_MODE_INT)
	_vTr[0]->triggerInternally(rb->IsOn());
      else if (parm == ID_TR_MODE_EXT)
	_vTr[0]->triggerExternally(rb->IsOn());

      break;	
    }

    default:
      break;
    }

  case kC_TEXTENTRY:
    switch (GET_SUBMSG(msg)) {

    case kTE_TEXTCHANGED: {
      TGNumberEntry* ne = _neMap[parm];
      if (parm == ID_TR_CRATE)
	_vTr[0]->crateNumber(static_cast<unsigned char>(ne->GetNumber()));
      else if (parm == ID_TR_POLL)
	_vTr[0]->pollInterval(UtlTimeDifference(ne->GetNumber()));

      break;
    }

    default:
      break;
    }

  default:
    break;
  }
}

void
CDispFrame::dconsAD(Long_t msg, Long_t parm)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_BUTTON:
      switch(parm) {
      case ID_DC_ADD: {

	DhcLocationData<DhcDcConfigurationData>* dc = new DhcLocationData<DhcDcConfigurationData>();
	dc->location(DhcLocation(static_cast<unsigned char>(_neMap[ID_DC_CRATE]->GetNumber()),
				 static_cast<unsigned char>(_neMap[ID_DC_SLOT]->GetNumber()),
				 static_cast<unsigned char>(_neMap[ID_DC_FESLOT]->GetNumber()),
				 1));
	_vDc.push_back(dc);

	_refresh = fTab->GetCurrent();
	CloseWindow();
	break;
      }

      case ID_DC_DEL: {

	std::vector<DhcLocationData<DhcDcConfigurationData>*>:: iterator dit( _vDc.begin());
	for(; dit!=_vDc.end(); dit++) {

	  if (static_cast<unsigned char>(_neMap[ID_DC_CRATE]->GetNumber())
	      != (*dit)->location().crateNumber())
	    continue;
	  if (static_cast<unsigned char>(_neMap[ID_DC_SLOT]->GetNumber())
	      != (*dit)->location().slotNumber())
	    continue;
	  if (static_cast<unsigned char>(_neMap[ID_DC_FESLOT]->GetNumber())
	      != (*dit)->location().dhcComponent())
	    continue;
	
	  _vDc.erase(dit);
	  break;
	}

	_refresh = fTab->GetCurrent();
	CloseWindow();
	break;
      }

      default:
	break;
      }

    default:
      break;
    }

  case kC_TEXTENTRY:
    switch (GET_SUBMSG(msg)) {

    case kTE_TEXTCHANGED: {
      
      break;
    }

    default:
      break;
    }

  default:
    break;
  }
}

void
CDispFrame::dcalsAD(Long_t msg, Long_t parm)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_BUTTON:
      switch(parm) {
      case ID_FE_ADD: {

	DhcLocationData<DhcFeConfigurationData>* fe = new DhcLocationData<DhcFeConfigurationData>();
	fe->data()->chipid(static_cast<unsigned char>(_neMap[ID_FE_FECHIP]->GetNumber()));
	fe->location(DhcLocation(static_cast<unsigned char>(_neMap[ID_FE_CRATE]->GetNumber()),
				 static_cast<unsigned char>(_neMap[ID_FE_SLOT]->GetNumber()),
				 static_cast<unsigned char>(_neMap[ID_FE_FESLOT]->GetNumber()),
				 1));
	_vFe.push_back(fe);

	_refresh = fTab->GetCurrent();
	CloseWindow();
	break;
      }

      case ID_FE_DEL: {

	std::vector<DhcLocationData<DhcFeConfigurationData>*>:: iterator fit( _vFe.begin());
	for(; fit!=_vFe.end(); fit++) {

	  if (static_cast<unsigned char>(_neMap[ID_FE_CRATE]->GetNumber())
	      != (*fit)->location().crateNumber())
	    continue;
	  if (static_cast<unsigned char>(_neMap[ID_FE_SLOT]->GetNumber())
	      != (*fit)->location().slotNumber())
	    continue;
	  if (static_cast<unsigned char>(_neMap[ID_FE_FESLOT]->GetNumber())
	      != (*fit)->location().dhcComponent())
	    continue;
	
	  _vFe.erase(fit);
	  break;
	}

	_refresh = fTab->GetCurrent();
	CloseWindow();
	break;
      }

      default:
	break;
      }

    default:
      break;
    }

  case kC_TEXTENTRY:
    switch (GET_SUBMSG(msg)) {

    case kTE_TEXTCHANGED: {
      
      break;
    }

    default:
      break;
    }

  default:
    break;
  }
}

void
CDispFrame::dconsEdit(Long_t msg, Long_t parm)
{
  UtlPack id(parm);
  id.byte(1,0);
  for (unsigned i(0); i<_vDc.size(); i++) {

    if (_locMap[parm].byte(3) != _vDc[i]->location().crateNumber())
      continue;
    if (_locMap[parm].byte(2) != _vDc[i]->location().slotNumber())
      continue;
    if (_locMap[parm].byte(1) != _vDc[i]->location().dhcComponent())
      continue;

    switch (GET_MSG(msg)) {

    case kC_COMMAND:
      switch (GET_SUBMSG(msg)) {

      case kCM_CHECKBUTTON: {

	TGCheckButton* cb = _cbMap[parm];
	if (id.word() >= ID_DC_CHIP_ENA) {

	  const unsigned char* cer(_vDc[i]->data()->enable());
	  std::bitset<24> cen((cer[2]<<16) | (cer[1]<<8) | (cer[0]));
	  unsigned cid(id.word() - ID_DC_CHIP_ENA);

	  if (cb->IsOn())
	    cen.set(cid);
	  else
	    cen.reset(cid);

	  UtlPack ces(cen.to_ulong());
	  _vDc[i]->data()->enable(0, ces.byte(0));
	  _vDc[i]->data()->enable(1, ces.byte(1));
	  _vDc[i]->data()->enable(2, ces.byte(2));
	}
	else if (id.word() == ID_DC_TTS_ENA)
	  _vDc[i]->data()->controlTriggerTimestampEnable(cb->IsOn());
	else if (id.word() == ID_DC_ZS_ENA)
	  _vDc[i]->data()->controlZeroSuppressionEnable(cb->IsOn());
	break;
      }

      default:
	break;
      }

    case kC_TEXTENTRY:
      switch (GET_SUBMSG(msg)) {

      case kTE_TEXTCHANGED: {
	TGNumberEntry* ne = _neMap[parm];
	if (id.word() == ID_DC_TWIDTH)
	  _vDc[i]->data()->triggerWidth(static_cast<unsigned char>(ne->GetNumber()));
	else if (id.word() == ID_DC_TDELAY)
	  _vDc[i]->data()->triggerDelay(static_cast<unsigned char>(ne->GetNumber()));
	break;
      }

      default:
	break;
      }

    default:
      break;
    }
  }
}

void
CDispFrame::dcalsEdit(Long_t msg, Long_t parm)
{
  UtlPack id(parm);
  id.byte(1,0);
  id.bits(0,2,0);

  for (unsigned i(0); i<_vFe.size(); i++) {

    if (_locMap[parm].byte(3) != _vFe[i]->location().crateNumber())
      continue;
    if (_locMap[parm].byte(2) != _vFe[i]->location().slotNumber())
      continue;
    if (_locMap[parm].byte(1) != _vFe[i]->location().dhcComponent())
      continue;
    if (_locMap[parm].byte(0) != *_vFe[i]->data()->chipid())
      continue;

    switch (GET_MSG(msg)) {

    case kC_COMMAND:
      switch (GET_SUBMSG(msg)) {

      case kCM_RADIOBUTTON: {
	unsigned char cval(parm&0x7);

	switch (id.word()) {

	case ID_FE_CAPSEL: {
	  _vFe[i]->data()->dcrCapSelect(cval);
	  break;
	}
	case ID_FE_GAIN: {
	  _vFe[i]->data()->dcrLowGain(cval==0);
	  break;
	}
	case ID_FE_BASELINE: {
	  _vFe[i]->data()->dcrDisableBaselineRestore(cval==0);
	  break;
	}
	case ID_FE_ACCEPT: {
	  _vFe[i]->data()->dcrAccept(cval==0);
	  break;
	}
	case ID_FE_PIPELINE: {
	  _vFe[i]->data()->dcrActivatePipeline(cval==0);
	  break;
	}
	case ID_FE_TRIGGER: {
	    _vFe[i]->data()->dcrExternalTrigger(cval==0);
	  break;
	}
	default:
	  break;
	}
	break;	
      }

      default:
	break;
      }

    case kC_TEXTENTRY:
      switch (GET_SUBMSG(msg)) {

      case kTE_TEXTCHANGED: {
	TGNumberEntry* ne = _neMap[parm];
	unsigned char cval(parm&0x07);

	switch (id.word()) {

	case ID_FE_REGS: {

	  switch (cval) {
	  case 1:
	    _vFe[i]->data()->plsr(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 2:
	    _vFe[i]->data()->intd(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 3:
	    _vFe[i]->data()->shp2(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 4:
	    _vFe[i]->data()->shp1(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 5:
	    _vFe[i]->data()->blrd(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 6:
	    _vFe[i]->data()->vtnd(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  case 7:
	    _vFe[i]->data()->vtpd(static_cast<unsigned char>(ne->GetNumber()));
	    break;
	  default:
	    break;
	  }
	  break;
	}
	case ID_FE_KILL: {
	  _vFe[i]->data()->kill(cval, static_cast<unsigned char>(ne->GetNumber()));
	  break;
	}
	case ID_FE_INJ: {
	  _vFe[i]->data()->inj(cval, static_cast<unsigned char>(ne->GetNumber()));
	  break;
	}
	default:
	  break;
	}
	break;
      }

      default:
	break;
      }

    default:
      break;
    }
  }
}

void CDispFrame::display(int& index)
{
  fTab = new TGTab(this, 20, 20);
  TGCompositeFrame *tf;

  tf = fTab->AddTab("Readout");
  TGCompositeFrame *rf = readout(tf);
  tf->AddFrame(rf, new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
				     kLHintsExpandY));
  tf = fTab->AddTab("Trigger");
  tf->AddFrame(trigger(tf), new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
					      kLHintsExpandY));
  tf = fTab->AddTab("Data Concentrators");
  tf->AddFrame(dcons(tf), new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
					    kLHintsExpandY));
  tf = fTab->AddTab("DCAL ASICs");
  tf->AddFrame(dcals(tf), new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
					    kLHintsExpandY));
  fTab->SetTab(index);
  AddFrame(fTab);

  std::string windowName("DHCal Configuration Editor - "+_file);
  SetWindowName(windowName.c_str());

  MapSubwindows();
  Resize();
  MapWindow();

}
					    
int CDispFrame::refresh()
{
  return _refresh;
}

bool CDispFrame::record(RcdRecord& r)
{
  SubModifier modifier(r);
  {
    std::vector<DhcReadoutConfigurationData*>
      vRo(modifier.access<DhcReadoutConfigurationData>());
    std::vector<DhcReadoutConfigurationData*>::iterator rt;

    _vRo.clear();
    for (rt=vRo.begin(); rt!=vRo.end(); rt++)
      _vRo.push_back(new DhcReadoutConfigurationData (*(*rt)));
  }
  {
    std::vector<DhcTriggerConfigurationData*>
      vTr(modifier.access<DhcTriggerConfigurationData>());
    std::vector<DhcTriggerConfigurationData*>::iterator tt;

    _vTr.clear();
    for (tt=vTr.begin(); tt!=vTr.end(); tt++)
      _vTr.push_back(new DhcTriggerConfigurationData (*(*tt)));
  }
  {
    std::vector<TtmLocationData<TtmConfigurationData>*>
      vTt(modifier.access< TtmLocationData<TtmConfigurationData> >());
    std::vector<TtmLocationData<TtmConfigurationData>*>::iterator tt;

    _vTt.clear();
    for (tt=vTt.begin(); tt!=vTt.end(); tt++)
      _vTt.push_back(new TtmLocationData<TtmConfigurationData> (*(*tt)));
  }
  {
    std::vector<DhcLocationData<DhcBeConfigurationData>*>
      vBe(modifier.access< DhcLocationData<DhcBeConfigurationData> >());
    std::vector<DhcLocationData<DhcBeConfigurationData>*>::iterator bt;

    _vBe.clear();
    for (bt=vBe.begin(); bt!=vBe.end(); bt++)
      _vBe.push_back(new DhcLocationData<DhcBeConfigurationData> (*(*bt)));
  }
  {
    std::vector<DhcLocationData<DhcDcConfigurationData>*>
      vDc(modifier.access< DhcLocationData<DhcDcConfigurationData> >());
    std::vector<DhcLocationData<DhcDcConfigurationData>*>::iterator dt;

    _vDc.clear();
    for (dt=vDc.begin(); dt!=vDc.end(); dt++)
      _vDc.push_back(new DhcLocationData<DhcDcConfigurationData> (*(*dt)));
  }
  {
    std::vector<DhcLocationData<DhcFeConfigurationData>*>
      vFe(modifier.access< DhcLocationData<DhcFeConfigurationData> >());
    std::vector<DhcLocationData<DhcFeConfigurationData>*>::iterator ft;

    _vFe.clear();
    for (ft=vFe.begin(); ft!=vFe.end(); ft++)
      _vFe.push_back(new DhcLocationData<DhcFeConfigurationData> (*(*ft)));
  }
  return true;
}

bool CDispFrame::beUpdate()
{
  std::vector<DhcReadoutConfigurationData*>::iterator rit;
  std::vector<DhcLocationData<DhcBeConfigurationData>*>::iterator bit;

  for(bit=_vBe.begin(); bit!=_vBe.end(); bit++) 
    for(rit=_vRo.begin(); rit!=_vRo.end(); rit++) 
      if ((*rit)->crateNumber() == (*bit)->location().crateNumber())
	for (unsigned s(4); s<20; s++)
	  if (s == (*bit)->location().slotNumber())
	    (*bit)->data()->dconEnable((*rit)->slotFeEnables(s));

  return true;
}

bool CDispFrame::update(RcdRecord& r)
{
  SubInserter inserter(r);

  std::vector<DhcTriggerConfigurationData*>::iterator trit(_vTr.begin());
  for(; trit!=_vTr.end(); trit++)
    inserter.insert< DhcTriggerConfigurationData >(*trit);

  std::vector<TtmLocationData<TtmConfigurationData>*>::iterator ttit( _vTt.begin());
  for(; ttit!=_vTt.end(); ttit++)
    inserter.insert< TtmLocationData<TtmConfigurationData> >(*ttit);

  std::vector<DhcReadoutConfigurationData*>::iterator rit(_vRo.begin());
  for(; rit!=_vRo.end(); rit++)
    inserter.insert< DhcReadoutConfigurationData >(*rit);

  std::vector<DhcLocationData<DhcBeConfigurationData>*>::iterator bit( _vBe.begin());
  for(; bit!=_vBe.end(); bit++)
    inserter.insert< DhcLocationData<DhcBeConfigurationData> >(*bit);

  std::vector<DhcLocationData<DhcDcConfigurationData>*>:: iterator dit( _vDc.begin());
  for(; dit!=_vDc.end(); dit++)
    inserter.insert< DhcLocationData<DhcDcConfigurationData> >(*dit);

  std::vector<DhcLocationData<DhcFeConfigurationData>*>::iterator fit( _vFe.begin());
  for(; fit!=_vFe.end(); fit++)
    inserter.insert< DhcLocationData<DhcFeConfigurationData> >(*fit);

  return true;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned width(argh.optionArgument('x',800,"Width"));
  const unsigned height(argh.optionArgument('y',400,"Height"));
  const std::string cfgPath(argh.optionArgument('f',"cfg/dhcal/beam/dhcConfig","Config data file"));

  if(argh.help()) return 0;

  TApplication _application("DHCal Config Application",0,0);
  gROOT->SetStyle();

  RcdReaderAsc *reader=new RcdReaderAsc();
  reader->open(cfgPath);

  RcdArena* arena(new RcdArena);
  reader->read(*arena);
  reader->close();

  int index(-1);
  while (index!=0) {

    CDispFrame* frame = new CDispFrame(gClient->GetRoot(), width, height, cfgPath);
    frame->record(*arena);
    frame->display(index);

    _application.Run(1);
    index = frame->refresh();

    arena->initialise(RcdHeader::startUp);

    frame->beUpdate();
    frame->update(*arena);

    delete frame;
   }

  RcdWriterAsc *writer=new RcdWriterAsc();
  writer->open(cfgPath);
  writer->write(*arena);
  writer->close();

  delete reader;
  delete writer;
  delete arena;
}
