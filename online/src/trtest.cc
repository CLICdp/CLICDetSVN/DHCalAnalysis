#include <iostream>

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcTree.hh"
#include "DhcTrack.hh"

#include "TTree.h"
#include "TFile.h"

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include <TGClient.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGTab.h>

#include "TH1D.h"
#include "TH2D.h"


class SliceStats {

public:
  SliceStats() :
    nExpected(0), nObserved(0), nHits(0)
  {};

  float nExpected;
  float nObserved;
  float nHits;

};

const char *tabnames[] = { "Zenith Angle",
			   "ChiSq",
			   "Expected Rate",
			   "Efficiency",
			   "Multiplicity",
			   "Effic Dist",
			   "Multip Dist"};

class CMainFrame : public TGMainFrame {

public:
  CMainFrame(const TGWindow *p,UInt_t w,UInt_t h)
    : TGMainFrame(p,w,h)
{
  SetCleanup(kDeepCleanup);
}

private:
void CloseWindow()
{
  gApplication->Terminate();
}

};

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned maxX(argh.optionArgument('x',480,"Maximum abs(x) (mm)"));
  const unsigned maxY(argh.optionArgument('y',160,"Maximum abs(y) (mm)"));
  const unsigned width(argh.optionArgument('w',1200,"Width"));
  const unsigned height(argh.optionArgument('v',600,"Height"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const unsigned minLayers(argh.optionArgument('l',3,"Minimum layer cut"));
  const unsigned maxLayers(argh.optionArgument('m',9,"Maximum layer cut"));
  const std::string rootPath(argh.optionArgument('f',"root","Path of root tree data"));
  const bool printCanvas(argh.option('p',"Print canvas"));

  const unsigned maxPoints(10);
  const unsigned maxDev(20);
  const float xmax(maxX);
  const float ymax(maxY);
  const float xybin(80);
  const float zbin(100);
  const float zmax(maxLayers*zbin);
  const float chimax(2);

  if(argh.help()) return 0;

  std::map<unsigned, SliceStats> stats;
  std::map<unsigned, SliceStats >::const_iterator ie;

  std::vector<std::pair<double, double> > slopes;
  std::vector<std::pair<double, double> > chisqs;
  std::vector<std::pair<double, double> >::const_iterator ivp;

  std::ostringstream sout;
  sout << rootPath << "/dhcTree-Run" << runNumber << ".root";

  TFile* tfile = new TFile(sout.str().c_str());
  TTree* tree = (TTree*)tfile->Get("DhcTree");

  Event* event = new Event();
  TBranch* branch = tree->GetBranch("EventBranch");
  branch->SetAddress(&event);

  Int_t nevt = tree->GetEntries();
  for (Int_t i(0); i<nevt; i++) {

    branch->GetEntry(i);
    TClonesArray* pts = event->GetHits();
    
    Int_t nhits(event->GetNhits());

    if (minLayers <= event->GetNhitLayers() &&
	maxPoints >= event->GetMaxHitsPerLayer()) {

      DhcTrack track;
      std::set<Int_t> layer;

      for (Int_t j(0); j<nhits; j++) {
	DhcHit *hit((DhcHit*)((*pts)[j]));

	if (abs(hit->GetPx()) < xmax &&
	    abs(hit->GetPy()) < ymax &&
	    abs(hit->GetPz()) < zmax) {

	  track.addHit(hit);
	  layer.insert(hit->GetPz());
	}
      }

      if (minLayers <= layer.size()) {

	LineFitter lxz, lyz;
	std::set<ClusterPt*> xz_points, yz_points;

	std::set<Int_t>::const_iterator il(layer.begin());
	for (; il!=layer.end(); il++) {

	  std::set<DhcCluster*>* clusters(track.getLayerClusters(*il));

	  std::set<DhcCluster*>::const_iterator ic(clusters->begin());
	  if (clusters->size() == 1) {
	    lxz.addPoint(new ClusterPt((*ic)->getX(), (*ic)->getZ(), (*ic)->getSize()));
	    lyz.addPoint(new ClusterPt((*ic)->getY(), (*ic)->getZ(), (*ic)->getSize()));
	  }

	  for (; ic!=clusters->end(); ic++) {
	    xz_points.insert(new ClusterPt((*ic)->getX(), (*ic)->getZ(), (*ic)->getSize()));
	    yz_points.insert(new ClusterPt((*ic)->getY(), (*ic)->getZ(), (*ic)->getSize()));
	  }

	  clusters->clear();
	  delete clusters;
	}

	if (lxz.fitPoints() && lyz.fitPoints())
	  if (lxz.chi2 < chimax && lyz.chi2 < chimax) {

	    slopes.push_back(std::make_pair(lxz.slope, lyz.slope));
	    chisqs.push_back(std::make_pair(lxz.chi2, lyz.chi2));
	 
	    for (float z(0); z<zmax; z+=zbin) {

	      float x((z-lxz.intercept)/lxz.slope);
	      float y((z-lyz.intercept)/lyz.slope);

	      if (fabs(x) < xmax && fabs(y) < ymax) {

		unsigned char ix((x + xmax) / xybin);
		unsigned char iy((y + ymax) / xybin);
		unsigned char iz(z / zbin);
		UtlPack loc(0);
		loc.byte(0, ix);
		loc.byte(1, iy);
		loc.byte(2, iz);

		stats[loc.word()].nExpected++;

		std::set<ClusterPt*>::const_iterator ixz(xz_points.begin());
		std::set<ClusterPt*>::const_iterator iyz(yz_points.begin());
		bool hxz(false);
		bool hyz(false);
	      
		for (; ixz!= xz_points.end(); ixz++)
		  if (fabs((*ixz)->second - z) < maxDev)
		    if (fabs((*ixz)->first - x) < maxDev) {
		      hxz = true;
		      break;
		    }

		for (; iyz!= yz_points.end(); iyz++)
		  if (fabs((*iyz)->second - z) < maxDev)
		    if (fabs((*iyz)->first - y) < maxDev) {
		      hyz = true;
		      break;
		    }

		if (hxz && hyz) {
		  stats[loc.word()].nObserved++;
		  stats[loc.word()].nHits += (*ixz)->nHits;
		}
	      }
	    }
	  }
	std::set<ClusterPt*>::const_iterator ixz(xz_points.begin());
	std::set<ClusterPt*>::const_iterator iyz(yz_points.begin());
	for (; ixz!=xz_points.end(); ixz++)
	  delete *ixz;
	for (; iyz!=yz_points.end(); iyz++)
	  delete *iyz;
      }
    }
    event->Clear();
  }

  TApplication application("Cosmic Ray Scan Application",0,0);
  gROOT->SetStyle("Bold");
  gStyle->SetOptStat("emr");
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetLabelSize(0.04, "xyz");
  gStyle->SetPalette(1,0);

  TGTab               *fTab;
  TRootEmbeddedCanvas *fEc[5];
  TGCompositeFrame    *fF[5];
  TGLayoutHints       *fL[5];

  CMainFrame* mf = new CMainFrame(gClient->GetRoot(), width, height);
  fTab = new TGTab(mf, 300, 300);

  TGCompositeFrame *tf;
  for (unsigned t(0); t<7; t++) {

    tf = fTab->AddTab(tabnames[t]);
    fL[t] = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
			      kLHintsExpandY, 5, 5, 5, 5);
    fF[t] = new TGCompositeFrame(tf, 60, 60, kHorizontalFrame);

    fEc[t] = new TRootEmbeddedCanvas (tabnames[t],fF[t],width,height);
    if (t<2)
      fEc[t]->GetCanvas()->Divide(3,1);
    else
      fEc[t]->GetCanvas()->Divide(2, (maxLayers+1)/2);

    fF[t]->AddFrame(fEc[t], fL[t]);
    tf->AddFrame(fF[t], fL[t]);
  }

  mf->AddFrame(fTab);

  mf->SetWindowName("Cosmic Ray Analysis");

  mf->MapSubwindows();
  mf->Resize(mf->GetDefaultSize());
  mf->MapWindow();

  double PI2(3.14159/2);
  TH1D xangle("xzAngle", "Zenith angle xz-plane", 100, -PI2, PI2);
  TH1D yangle("yzAngle", "Zenith angle yz-plane", 100, -PI2, PI2);
  TH1D angle("zAngle", "Zenith angle", 100, 0, PI2);

  for (ivp=slopes.begin(); ivp!=slopes.end(); ivp++) {
    double xang((*ivp).first < 0 ?
		-(atan((*ivp).first) + PI2) : PI2 - atan((*ivp).first));
    double yang((*ivp).second < 0 ?
		-(atan((*ivp).second) + PI2) : PI2 - atan((*ivp).second));
    double ang(PI2 - atan(pow((pow((*ivp).first,-2) + pow((*ivp).second,-2)), -0.5)));

    xangle.Fill(xang);
    yangle.Fill(yang);
    angle.Fill(ang);
  }

  fEc[0]->GetCanvas()->cd(1);
  xangle.Draw();

  fEc[0]->GetCanvas()->cd(2);
  yangle.Draw();

  fEc[0]->GetCanvas()->cd(3);
  angle.Draw();

  TH1D xchisq("xzChisq", "ChiSq/NDF xz-plane", 100, 0, chimax);
  TH1D ychisq("yzChisq", "ChiSq/NDF yz-plane", 100, 0, chimax);

  for (ivp=chisqs.begin(); ivp!=chisqs.end(); ivp++) {
    xchisq.Fill((*ivp).first);
    ychisq.Fill((*ivp).second);
  }

  fEc[1]->GetCanvas()->cd(1);
  xchisq.Draw();

  fEc[1]->GetCanvas()->cd(2);
  ychisq.Draw();

  for (unsigned t(0); t<2; t++) {
    fEc[t]->GetCanvas()->Update();
    if (printCanvas) {
      std::ostringstream sout;
      sout << "root/" + std::string(fEc[t]->GetCanvas()->GetName()) + ".png";
      fEc[t]->GetCanvas()->Print(sout.str().c_str(), "png");
    }
  }

  gStyle->SetTitleFontSize(0.1);
  gStyle->SetLabelSize(0.1, "xyz");

  std::map<unsigned char, TH2D*> rate;
  std::map<unsigned char, TH2D*>::const_iterator ira;
  std::map<unsigned char, TH2D*> effic;
  std::map<unsigned char, TH2D*>::const_iterator ief;
  std::map<unsigned char, TH2D*> multi;
  std::map<unsigned char, TH2D*>::const_iterator imu;
  std::map<unsigned char, TH1D*> h_effic;
  std::map<unsigned char, TH1D*>::const_iterator hef;
  std::map<unsigned char, TH1D*> h_multi;
  std::map<unsigned char, TH1D*>::const_iterator hmu;

  int np(0);
  for (ie=stats.begin(); ie!=stats.end(); ie++) {
    UtlPack loc((*ie).first);
    unsigned char ix(loc.byte(0));
    unsigned char iy(loc.byte(1));
    unsigned char iz(loc.byte(2));

    if (rate.find(iz) == rate.end()) {
      std::ostringstream sout;
      sout << "Layer " << floor(iz);
      rate[iz] = new TH2D((sout.str()+" rate").c_str(), sout.str().c_str(),
			  (Int_t)(2*xmax/xybin), -xmax/10, xmax/10,
			  (Int_t)(2*ymax/xybin), -ymax/10, ymax/10);
    }

    if (effic.find(iz) == effic.end()) {
      std::ostringstream sout;
      sout << "Layer " << floor(iz);
      effic[iz] = new TH2D((sout.str()+" efficiency").c_str(), sout.str().c_str(),
			   (Int_t)(2*xmax/xybin), -xmax/10, xmax/10,
			   (Int_t)(2*ymax/xybin), -ymax/10, ymax/10);
    }

    if (multi.find(iz) == multi.end()) {
      std::ostringstream sout;
      sout << "Layer " << floor(iz);
      multi[iz] = new TH2D((sout.str()+" multipplicity").c_str(), sout.str().c_str(),
			   (Int_t)(2*xmax/xybin), -xmax/10, xmax/10,
			   (Int_t)(2*ymax/xybin), -ymax/10, ymax/10);
    }

    if (h_effic.find(iz) == h_effic.end()) {
      std::ostringstream sout;
      sout << "Layer " << floor(iz);
      h_effic[iz] = new TH1D((sout.str()+" efficiency dist").c_str(), sout.str().c_str(),
			     100, 0., 1.2);
    }

    if (h_multi.find(iz) == h_multi.end()) {
      std::ostringstream sout;
      sout << "Layer " << floor(iz);
      h_multi[iz] = new TH1D((sout.str()+" multiplicity dist").c_str(), sout.str().c_str(),
			     100, 0., 4.0);
    }
    
    rate[iz]->SetBinContent(ix+1, iy+1,
			    (((*ie).second).nExpected));
    effic[iz]->SetBinContent(ix+1, iy+1,
			(((*ie).second).nObserved)/(((*ie).second).nExpected));
    multi[iz]->SetBinContent(ix+1, iy+1,
			(((*ie).second).nHits)/(((*ie).second).nObserved));

    h_effic[iz]->Fill((((*ie).second).nObserved)/(((*ie).second).nExpected));
    h_multi[iz]->Fill((((*ie).second).nHits)/(((*ie).second).nObserved));
  }

  Int_t c(1);
  for (ira=rate.begin(); ira!=rate.end(); ira++) {
    fEc[2]->GetCanvas()->cd(c++)->SetRightMargin(0.15);
    (*ira).second->GetZaxis()->SetNdivisions(7);
    (*ira).second->SetStats(0);
    (*ira).second->SetMinimum(0);
    (*ira).second->Draw("colz");
  }

  c=1;
  for (ief=effic.begin(); ief!=effic.end(); ief++) {
    fEc[3]->GetCanvas()->cd(c++)->SetRightMargin(0.15);
    (*ief).second->GetZaxis()->SetNdivisions(7);
    (*ief).second->SetStats(0);
    (*ief).second->SetMinimum(0);
    (*ief).second->Draw("colz");
  }

  c=1;
  for (imu=multi.begin(); imu!=multi.end(); imu++) {
    fEc[4]->GetCanvas()->cd(c++)->SetRightMargin(0.15);
    (*imu).second->GetZaxis()->SetNdivisions(7);
    (*imu).second->SetStats(0);
    (*imu).second->SetMinimum(0);
    (*imu).second->Draw("colz");
  }

  gStyle->SetStatFontSize(0.12);

  c=1;
  for (hef=h_effic.begin(); hef!=h_effic.end(); hef++) {
    fEc[5]->GetCanvas()->cd(c++)->SetRightMargin(0.15);
    (*hef).second->Draw();
  }

  c=1;
  for (hmu=h_multi.begin(); hmu!=h_multi.end(); hmu++) {
    fEc[6]->GetCanvas()->cd(c++)->SetRightMargin(0.15);
    (*hmu).second->Draw();
  }

  for (unsigned t(2); t<=6; t++) {
    fEc[t]->GetCanvas()->Update();
    if (printCanvas) {
      std::ostringstream sout;
      sout << "root/" + std::string(fEc[t]->GetCanvas()->GetName()) + ".png";
      fEc[t]->GetCanvas()->Print(sout.str().c_str(), "png");
    }
  }

  application.Run();
}
