#include <iostream>
#include <map>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"


class DhcBlkErr : public RcdUserRO {

public:
  DhcBlkErr();
  virtual ~DhcBlkErr() {}

  bool record(const RcdRecord &r);

  const std::map<unsigned, unsigned>& err_count() const;

private:
  std::map<unsigned, unsigned> _errCount;

};

DhcBlkErr::DhcBlkErr()
  : RcdUserRO(9)
{}

const std::map<unsigned, unsigned>& DhcBlkErr::err_count() const
{
  return _errCount;
}

bool DhcBlkErr::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {
      for (unsigned s(0); s<v.size(); s++)
	if (v[s]->data()->numberOfWords() == 0) {
	  UtlPack loc(0);
	  loc.byte(3, v[s]->location().crateNumber());
	  loc.byte(2, v[s]->location().slotNumber());
	  _errCount[loc.word()]++;
	}
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  DhcBlkErr dbe;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    dbe.record(*arena);

  reader.close();
  delete arena;

  std::map<unsigned, unsigned> errCount(dbe.err_count());
  std::map<unsigned, unsigned>::const_iterator ibe(errCount.begin());

  for (; ibe!=errCount.end(); ibe++) {
    UtlPack loc((*ibe).first);
    DhcLocation dloc(loc.byte(3), loc.byte(2), 0);
    dloc.print(std::cout);
    std::cout << " Block transfer error count: " << (*ibe).second << std::endl;
  }

}
