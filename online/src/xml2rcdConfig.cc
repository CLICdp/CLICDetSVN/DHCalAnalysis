//
// $Id: xml2rcdConfig.cc,v 1.2 2008/02/29 19:22:17 jls Exp $
//

#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RcdWriterAsc.hh"

#include "UtlArguments.hh"

#include "DhcConfigReader.hh"


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  if(argh.argument(0,"")!="-h")
    if (argh.numberOfArguments() < 2) {

      std::cout << std::endl
		<< "    This program only has to be run after one of the .xml files" << std::endl
		<< "    has been modified, not every time a series of runs different" << std::endl
		<< "    from the previous runType is started." << std::endl << std::endl
		<< "    Type 'xml2rcdConfig -h' for help" << std::endl
		<< std::endl;
      return 0;
    }

  const std::string type(argh.optionArgument('t',"beam","Config type (beam|cosmic|noise|qinj)"));
  const std::string file(argh.optionArgument('f',"cfg/dhcal/beam/cubic.xml", "XML config file"));

  if(argh.help()) return 0;

  DhcConfigReader configReader(file.c_str());

  RcdArena* arena(new RcdArena);
  RcdWriterAsc writer;

  // Load file
  if(!configReader.load(*arena)) return 2;

  // write records
  writer.printLevel(0);
  assert(writer.open("cfg/dhcal/"+type+"/dhcConfig"));
  assert(writer.write(*arena));
  writer.close();

  delete arena;

  return 0;
}
