#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>

#include "RcdHeader.hh"
#include "RcdUserRO.hh"
#include "SubAccessor.hh"

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "DhcCableMap.hh"


class ChkDhcConfig : public RcdUserRO {

private:

  DhcCableMap* _cmap;
  std::map<unsigned, bool> _lockMap;
  bool _verbose;

public:
  ChkDhcConfig(const unsigned& runNumber,
	       const bool& verbose)
  : RcdUserRO(9),_verbose(verbose) 
  {
    _cmap = new DhcCableMap(runNumber);
  }

  virtual ~ChkDhcConfig()
  {
    delete _cmap;
  }

  bool record(const RcdRecord &r) {

    SubAccessor accessor(r);

    switch(r.recordType()) {

    case RcdHeader::configurationStart: {

      std::vector<const DaqConfigurationStart*>
	v(accessor.extract<DaqConfigurationStart>());
      assert(v.size()==1);

      std::cout << " Dhc FE configuration number " << v[0]->configurationNumberInRun()
		<< " in run." << std::endl;

      std::vector<const DhcReadoutConfigurationData*>
	ro(accessor.access<DhcReadoutConfigurationData>());

      assert(ro.size()>0);

      std::vector<const DhcLocationData<DhcBeConfigurationData>*>
	be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

      assert(be.size()>0);

      std::map<unsigned, bool> _lockMap;
      std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
      for (bit=be.begin(); bit!=be.end(); bit++) {
	if (!(*bit)->write()) {

	  UtlPack loc(0);
	  loc.byte(3, ((*bit)->crateNumber()));
	  loc.byte(2, ((*bit)->slotNumber()));

	  UtlPack dconEnabled(0);
	  dconEnabled.word(((*bit)->data()->dconEnable()));

	  for (unsigned i(0); i<12; i++) {
	    loc.byte(1, i);
	    for (unsigned j(0); j<24; j++) {
	      loc.byte(0,j);
	      _lockMap[loc.word()] = (dconEnabled.bit(i) == dconEnabled.bit(i+16));
	    }
	  }
	}
      }

      std::vector<const DhcLocationData<DhcFeConfigurationData>*>
	fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

      assert(fe.size()>0);

      std::map<unsigned, const DhcFeConfigurationData*> _feData;
      const DhcFeConfigurationData* feData;

      unsigned notConfig(0);
      std::vector<const DhcLocationData<DhcFeConfigurationData>*>::const_iterator it;
      for (it=fe.begin(); it!=fe.end(); it++) {

	UtlPack loc(0);
	loc.byte(3, (*it)->location().crateNumber());
	loc.byte(2, (*it)->location().slotNumber());
	loc.byte(1, (*it)->dhcComponent());
	loc.byte(0, *(*it)->data()->chipid());

	if ((*it)->location().write())
	  _feData[loc.word()] = (*it)->data();

	else {

	  // test for DCOL port not locked
	  if (!_lockMap[loc.word()])
	    continue;

	  if (_feData.find(loc.word()) != _feData.end())
	    feData = _feData[loc.word()];

	  else
	    feData = _feData.rbegin()->second;
	
	  unsigned v(loc.byte(3)-220);
	  unsigned s(loc.byte(2));

	  unsigned maxChip = (ro[v]->slotFeRevision(s) == 2) ? 4 : 24;

	  const unsigned char* _inj = feData->inj();
	  const unsigned char* _kill = feData->kill();
		      
	  bool cfgd(true);

	  if (*feData->plsr() != *(*it)->data()->plsr()) cfgd=false;
	  if (*feData->intd() != *(*it)->data()->intd()) cfgd=false;
	  if (*feData->shp2() != *(*it)->data()->shp2()) cfgd=false;
	  if (*feData->shp1() != *(*it)->data()->shp1()) cfgd=false;
	  if (*feData->blrd() != *(*it)->data()->blrd()) cfgd=false;
	  if (*feData->vtnd() != *(*it)->data()->vtnd()) cfgd=false;
	  if (*feData->vtpd() != *(*it)->data()->vtpd()) cfgd=false;
	  if (*feData->dcr() != *(*it)->data()->dcr()) cfgd=false;

	  if (maxChip>4)
	    for (unsigned i(0); i<8; i++) {
	      if (_inj[i] != ((*it)->data()->inj())[i]) cfgd=false;
	      if (_kill[i] != ((*it)->data()->kill())[i]) cfgd=false;
	    }
		
	  if (!cfgd) {
	    notConfig++;
	    if (_verbose)
	      std::cout << " Side "
			<< _cmap->label((*it)->location().crateNumber(),
					(*it)->location().slotNumber(),
					(*it)->location().dhcComponent())
			<< " (" << static_cast<unsigned int>((*it)->location().crateNumber())
			<< ", " << static_cast<unsigned int>((*it)->location().slotNumber())
			<< ", " << static_cast<unsigned int>((*it)->location().dhcComponent())
			<< ") Chip "
			<< static_cast<unsigned int>(*(*it)->data()->chipid())
			<< " did not configure."
			<< std::endl;
	  }
	}
      }
      if (notConfig)
	std::cout << " " << notConfig << " chips did not configure" << std::endl;
      else
	std::cout << " All chips configured" << std::endl;
      break;
    }

    default: {  
      break;
    }
    };

    return true;
  }

};


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const bool verbose(argh.option('v',"Verbose print"));

  if(argh.help()) return 0;

  ChkDhcConfig* cdc = new ChkDhcConfig(runNumber, verbose);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena)) {
    if(cdc!=0) cdc->record(*arena);
  }
  reader.close();

  delete cdc;
  delete arena;
}
