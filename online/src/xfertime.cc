#include <iostream>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TH2F.h"


class DhcXferTime : public RcdUserRO {

public:
  DhcXferTime();
  virtual ~DhcXferTime() {}

  bool record(const RcdRecord &r);

  const std::vector<double>& xfer_time() const;
  const std::vector<double>& event_count() const;
  const std::vector<double>& event_size() const;

private:
  std::vector<double> _xferTime;
  std::vector<double> _eventCount;
  std::vector<double> _eventSize;

  UtlTime _startTime;

  unsigned _nEvents;
  unsigned _nWords;
};

DhcXferTime::DhcXferTime()
  : RcdUserRO(9)
{}

const std::vector<double>& DhcXferTime::xfer_time() const
{
  return _xferTime;
}

const std::vector<double>& DhcXferTime::event_count() const
{
  return _eventCount;
}

const std::vector<double>& DhcXferTime::event_size() const
{
  return _eventSize;
}

bool DhcXferTime::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::transferStart: {

    _startTime = r.recordTime();

    _nEvents = 0;
    _nWords = 0;
    break;
  }

  case RcdHeader::transferEnd: {

    if (_nEvents) {
      UtlTimeDifference eventTime = r.recordTime() - _startTime;
      _xferTime.push_back(eventTime.deltaTime());
      _eventCount.push_back(_nEvents);
      _eventSize.push_back(_nWords/_nEvents);
    }
    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {

      for (unsigned s(0); s<v.size(); s++)
	_nWords += v[s]->data()->numberOfWords();

      _nEvents++;
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path of run data"));

  if(argh.help()) return 0;

  DhcXferTime dxt;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    dxt.record(*arena);

  reader.close();
  delete arena;

  std::vector<double> times = dxt.xfer_time();
  std::vector<double> counts = dxt.event_count();
  std::vector<double> sizes = dxt.event_size();

  double tmax(2*(*(std::max_element(times.begin(), times.end()))));
  double cmax(2*(*(std::max_element(counts.begin(), counts.end()))));
  double smax(2*(*(std::max_element(sizes.begin(), sizes.end()))));

  TApplication application("Trigger Time Check Application",0,0);
  gROOT->SetStyle("Bold");
  gStyle->SetOptStat("emr");

  TCanvas canvas("DhcReadout Transfer Time", "Transfer Times", 600,600);
  canvas.Divide(1,2);

  TH2F xferTime0("xferTime0","xferTime vs eventCount", 100, 0, cmax, 100, 0, tmax);
  TH2F xferTime1("xferTime1","xferTime vs eventSize", 100, 0, smax, 100, 0, tmax);
  for (unsigned i(0); i<times.size(); i++) {
    xferTime0.Fill(counts[i], times[i]);
    xferTime1.Fill(sizes[i], times[i]);
  }

  canvas.cd(1);
  xferTime0.SetTitle("XferTime vs eventCount");
  xferTime0.GetXaxis()->SetTitle("events/spill");
  xferTime0.GetYaxis()->SetTitle("readout time (sec)");
  xferTime0.Draw("box");

  canvas.cd(2);
  xferTime1.SetTitle("XferTime vs eventSize");
  xferTime1.GetXaxis()->SetTitle("words/event");
  xferTime1.GetYaxis()->SetTitle("readout time (sec)");
  xferTime1.Draw("box");

  canvas.Update();

  application.Run();
}

