#include <iostream>
#include <map>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TH1F.h"
#include "TH2F.h"

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

const char *wcnames[] = { "Upstream",
			  "Middle",
			  "Downstream" };


class ChkTdcHits : public RcdUserRO {

public:
  ChkTdcHits();
  virtual ~ChkTdcHits() {}

  bool record(const RcdRecord &r);

  std::map<unsigned, std::vector<int>*> rawTimes;
  std::map<unsigned, std::vector<int>*> hitCount;
  std::map<unsigned, std::vector<float>*> hitPos;;

};

ChkTdcHits::ChkTdcHits()
  : RcdUserRO(9)
{}


bool ChkTdcHits::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::event: {

    std::vector<const BmlLocationData<BmlCaen1290EventData>*>
      v(accessor.access<BmlLocationData<BmlCaen1290EventData> >());
 
    std::vector<const BmlCaen1290EventDatum*> cdata[16];

    if (v.size()>0) {
      for (unsigned s(0); s<v.size(); s++) {
	for (unsigned c(0); c<16; c++) {
	  cdata[c] = v[s]->data()->channelData(c);
	  if (cdata[c].size() > 0) {

	    if (rawTimes.find(c) == rawTimes.end())
	      rawTimes[c] = new std::vector<int>;
	    if (hitCount.find(c) == hitCount.end())
	      hitCount[c] = new std::vector<int>;

	    for (unsigned h(0); h<cdata[c].size(); h++) {
	      rawTimes[c]->push_back((cdata[c])[h]->time());
	    }
	    hitCount[c]->push_back(cdata[c].size());
	  }
	} 
      }
      for (unsigned w(0); w<12; w+=2) {

	if (hitPos.find(w/2) == hitPos.end())
	  hitPos[w/2] = new std::vector<float>;

	// Allow only 1 hit/chan
	if ((cdata[w].size()) && (cdata[w+1].size())) {
	  float coord(((float)((cdata[w])[0]->time()) -
		       (float)((cdata[w+1])[0]->time())) * 0.025 * 0.2);
	  hitPos[w/2]->push_back(coord);
	  //std::cout << w/2 << "  " << coord << std::endl;
	}
	else {
	  hitPos[w/2]->push_back(0xFFFF);
	  //std::cout << w/2 << std::endl;
	}
      }
      //std::cout << std::endl;
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path of run data"));
  if(argh.help()) return 0;

  ChkTdcHits cdh;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    cdh.record(*arena);

  reader.close();
  delete arena;

  TApplication _application("Wire Chamber Time Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eou");

  TCanvas canvas("TDC Basic", "TDC Basic", 1200, 600);
  canvas.Divide(8,3);
  TCanvas canvas2("Wire Chamber Profiles", "Wire Profiles", 1200, 400);
  canvas2.Divide(3,1);

  std::map<unsigned, std::vector<int>*>::iterator mit;
  std::map<unsigned, std::vector<float>*>::iterator wit;
  std::map<unsigned, TH1F*> times;
  std::map<unsigned, TH1F*>::iterator iti;
  std::map<unsigned, TH1F*> hits;
  std::map<unsigned, TH1F*>::iterator ihi;
  std::map<unsigned, TH2F*> profile;
  std::map<unsigned, TH2F*>::iterator ipi;

  Int_t c(1);
  for (mit=cdh.rawTimes.begin(); mit!=cdh.rawTimes.end(); mit++) {
    unsigned chan((*mit).first);

    if (times.find(chan) == times.end()) {
      std::ostringstream sout;
      sout << "TDC Chan " << chan;
      times[chan] = new TH1F((sout.str()+" times").c_str(), sout.str().c_str(), 1000, 0, 4000);
    }
    if (hits.find(chan) == hits.end()) {
      std::ostringstream sout;
      sout << "TDC Chan " << chan;
      hits[chan] = new TH1F((sout.str()+" hits").c_str(), sout.str().c_str(), 10, 0, 10);
    }    
  }

  for (mit=cdh.rawTimes.begin(); mit!=cdh.rawTimes.end(); mit++) {
    unsigned chan((*mit).first);

    std::vector<int>::iterator it(cdh.rawTimes[chan]->begin());
    for (; it!=cdh.rawTimes[chan]->end(); it++)
      times[chan]->Fill((*it)*0.025);

    std::vector<int>::iterator ih(cdh.hitCount[chan]->begin());
    for (; ih!=cdh.hitCount[chan]->end(); ih++)
      hits[chan]->Fill((*ih));
  }

  for (wit=cdh.hitPos.begin(); wit!=cdh.hitPos.end(); wit++) {
    unsigned cham((*wit).first);

    if (profile.find(cham) == profile.end()) {
      std::ostringstream sout;
      sout << "Wire Chamber " << wcnames[cham/2];
      profile[cham] = new TH2F((sout.str()+"profile").c_str(), sout.str().c_str(),
			       120, -60, 60, 120, -60, 60);
    }

    std::vector<float>::iterator ipx(cdh.hitPos[cham]->begin());
    std::vector<float>::iterator ipy(cdh.hitPos[cham+1]->begin());
    for (; ipx!=cdh.hitPos[cham]->end() && ipy!=cdh.hitPos[cham+1]->end(); ipx++, ipy++)
      profile[cham]->Fill((*ipx), (*ipy));

    wit++;
  }


  for (iti=times.begin(), ihi=hits.begin(); iti!=times.end(); iti++, ihi++) {

    canvas.cd(c++)->SetLogy(1);
    (*iti).second->GetXaxis()->SetTitle("Event tDist (nsec)");
    (*iti).second->Draw();

    canvas.cd(c++)->SetLogy(1);
    (*ihi).second->GetXaxis()->SetTitle("Event hitCount");

    (*ihi).second->Draw();
  }
  canvas.Update();

  c = 1;
  for (ipi=profile.begin(); ipi!=profile.end(); ipi++) {

    canvas2.cd(c++);
    (*ipi).second->GetXaxis()->SetTitle("X-Pos (mm)");
    (*ipi).second->GetYaxis()->SetTitle("Y-Pos (mm)");
    (*ipi).second->Draw("colz");
  }
  canvas2.Update();

  _application.Run();

}
