#include <iostream>
#include <vector>
#include <deque>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcFeHitData.hh"
#include "TtmFifoData.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TRootEmbeddedCanvas.h"
#include "TCanvas.h"
#include "TGFrame.h"
#include "TGraphAsymmErrors.h"
#include "THStack.h"
#include "TH1F.h"
#include "TF1.h"
#include "TPaveText.h"
#include "TLegend.h"

#include "TSystem.h"
#include "TTimer.h"


class ChkDhcTts : public RcdUserRO {

public:
  ChkDhcTts();
  virtual ~ChkDhcTts() {}

  bool record(const RcdRecord &r);
  bool report();

  const std::vector<double>& hit_time() const;
  const std::vector<std::pair<double,bool> >& trg_time() const;

  UtlTime _recordTime;
  unsigned _outOfTime;
  unsigned _missingData;
  unsigned _noData;
  unsigned _earlyData;
  unsigned _lateData;
  unsigned _nTts;
  unsigned _nAts;
  unsigned _nBts;

private:
  std::deque<TtmFifoData*> _ttmtts;
  std::map<unsigned, unsigned> _enableMap;

  unsigned _triggerDelay;
  unsigned _triggerStart;
  unsigned _configurationNumber;
  unsigned _acquisitionNumber;
  bool _standalone;

  std::vector<double> _hitTime;
  std::vector<std::pair<double,bool> > _trgTime;

};


ChkDhcTts::ChkDhcTts()
  : RcdUserRO(9), _outOfTime(0), _missingData(0), _noData(0), _earlyData(0), _lateData(0),
    _nTts(0), _nAts(0), _nBts(0) {}

const std::vector<double>& ChkDhcTts::hit_time() const
{
  return _hitTime;
}

const std::vector<std::pair<double,bool> >& ChkDhcTts::trg_time() const
{
  return _trgTime;
}

bool ChkDhcTts::record(const RcdRecord &r) {

  _recordTime = r.recordTime();
  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::runStart: {

    std::vector<const DaqRunStart*>
      v(accessor.extract<DaqRunStart>());
    assert(v.size()==1);

    DaqRunType runType=v[0]->runType();
    unsigned runNumber=v[0]->runNumber();

    switch(runType.type()) {

    case DaqRunType::beamData: {
      _standalone = false;
      break;
    }

    case DaqRunType::dhcBeam: {
      _standalone = true;
      break;
    }

    default: {
      std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
      exit(0);
    }
    }
    break;
  }

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);
    _configurationNumber = v[0]->configurationNumberInRun();

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    assert(be.size()>0);

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
    for (bit=be.begin(); bit!=be.end(); bit++) {
      if (!(*bit)->write()) {

	UtlPack loc(0);
	loc.byte(2, ((*bit)->crateNumber()));
	loc.byte(1, ((*bit)->slotNumber()));

	UtlPack dconEnabled(0);
	dconEnabled.word(((*bit)->data()->dconEnable()));

	_enableMap[loc.word()] = dconEnabled.bits(0,11);
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    assert(dc.size()>0);
    _triggerDelay = static_cast<unsigned int>(*dc[0]->data()->triggerDelay()) + 1;
    _triggerStart = 21 - _triggerDelay;

    break;
  }

  case RcdHeader::acquisitionStart: {

    std::vector<const DaqAcquisitionStart*>
      v(accessor.extract<DaqAcquisitionStart>());
    assert(v.size()==1);
    _acquisitionNumber = v[0]->acquisitionNumberInRun();

    break;
  }

  case RcdHeader::trigger: {

    std::vector<const TtmLocationData<TtmTriggerData>*>
      v(accessor.access<TtmLocationData<TtmTriggerData> >());

    assert(v.size()==1);

    const unsigned n(v[0]->data()->numberOfWords());
    const unsigned* data(v[0]->data()->data());

    _ttmtts.push_back(new TtmFifoData(n, data));

    break;
  }

  case RcdHeader::event: {

    TtmFifoData* ttmtts = _ttmtts.front();
    unsigned tts = *ttmtts->triggerTS();

    std::vector<const CrcLocationData<CrcVlinkEventData>* > 
      cv(accessor.access< CrcLocationData<CrcVlinkEventData> >());

    bool oscTrg(false);
    for(unsigned i(0); i < cv.size();i++) {
      const CrcVlinkTrgData *td(cv[i]->data()->trgData());
      if (td)
	if (td->lineHistory(19).size() == 1)
	  if (td->lineHistory(24).size() == 1)
	    oscTrg = true;
    }

    if (_standalone || _configurationNumber%3 == 2) {
      _trgTime.push_back(std::make_pair(_acquisitionNumber*1e7 + tts, oscTrg));

      _nTts += ttmtts->numberOfTriggerTS();
      if (ttmtts->numberOfFifoATS())
	_nAts++;
      if (ttmtts->numberOfFifoBTS())
	_nBts++;
    }
    _ttmtts.pop_front();

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    bool inTime(true);
    bool missingData(false);
    bool noData(true);
    bool dataEarly(false);
    bool dataLate(false);

    for(unsigned s(0);s<v.size();s++) {

      UtlPack loc(0);
      loc.byte(2, (v[s]->crateNumber()));
      loc.byte(1, (v[s]->slotNumber()));

      UtlPack dconTrg(0);

      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {

	noData = false;
	const DhcFeHitData* hits(v[s]->data()->feData(i));

	if (!hits->trg()) {
	  double tDelta((double)tts - (double)hits->timestamp());
	  _hitTime.push_back(tDelta);
	  if (tDelta > _triggerStart)
	    dataEarly=true;
	  if (tDelta < 0)
	    dataLate=true;
	  continue;
	}

	dconTrg.bit(hits->dconad(), 1);

	unsigned tDelta = hits->timestamp() >= _triggerDelay ?
	  hits->timestamp() - tts : (hits->timestamp()+10000000) - tts;
	if (tDelta != _triggerDelay)
	  inTime=false;
      }

#ifdef DHC_DEBUG    
      if (dataEarly || dataLate)
	v[s]->print(std::cout, " ");
#endif // DHC_DEBUG

      if (dconTrg.word() != _enableMap[loc.word()]) {
	missingData = true;
#ifdef DHC_DEBUG    
	v[s]->print(std::cerr, " ");
#endif // DHC_DEBUG
      }
    }

    if (!inTime)
      _outOfTime++;

    if (dataEarly)
      _earlyData++;
    if (dataLate)
      _lateData++;
    
    if (noData) {
      _noData++;

      std::vector<const DaqEvent*> de(accessor.extract<DaqEvent>());
      assert(de.size()==1);

      std::cerr << "No event data." << std::endl;
      de[0]->print(std::cerr, " ");
      break;
    }

    if (missingData)
      _missingData++;

    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

bool ChkDhcTts::report()
{
  std::cout << " Last updated: " << _recordTime << std::endl;
  std::cout << "        " << _outOfTime << " Out of Time Events" << std::endl;
  std::cout << "        " << _missingData << " events with missing data" << std::endl;
  std::cout << "        " << _noData << " events with no data" << std::endl;
  std::cout << "        " << _earlyData << " events with early data (timestamp too small)" << std::endl;
  std::cout << "        " << _lateData << " events with late data (timestamp too large)" << std::endl;
  std::cout << "Cerenkov:"
	    << " Outer Fraction: " << (double)_nAts/(double)_nTts
	    << " Inner fraction: " << (double)_nBts/(double)_nTts << std::endl;
  return true;
}


class DhcTimeLine : public RcdUserRO {

public:
  DhcTimeLine();
  virtual ~DhcTimeLine() {}

  bool record(const RcdRecord &r);

  const std::vector<double>& event_time() const;
  const std::vector<double>& max_record() const;

private:
  std::vector<double> _eventTime;
  std::vector<double> _maxRecord;

  UtlTime _startTime;
  unsigned _configurationNumber;
  bool _combined;
};

DhcTimeLine::DhcTimeLine()
  : RcdUserRO(9)
{}

const std::vector<double>& DhcTimeLine::event_time() const
{
  return _eventTime;
}

const std::vector<double>& DhcTimeLine::max_record() const
{
  return _maxRecord;
}

bool DhcTimeLine::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::runStart: {

    std::vector<const DaqRunStart*>
      v(accessor.extract<DaqRunStart>());
    assert(v.size()==1);

    DaqRunType runType=v[0]->runType();

    switch(runType.type()) {

    case DaqRunType::beamData: {
      _combined = true;
      break;
    }

    default: {
      _combined = false;
      break;
    }
    }
    _startTime = r.recordTime();
    break;
  }

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);
    _configurationNumber = v[0]->configurationNumberInRun();

    break;
  }

  case RcdHeader::trigger: {

    if (_combined && _configurationNumber%3 <2)
      break;

    UtlTimeDifference eventTime = r.recordTime() - _startTime;
    _eventTime.push_back(eventTime.deltaTime());

    break;
  }

  case RcdHeader::event: {

    if (_combined && _configurationNumber%3 <2)
      break;

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {

      unsigned maxRecord(0);
      for (unsigned s(0); s<v.size(); s++) {
	maxRecord = std::max(maxRecord, v[s]->data()->numberOfWords()*4);
      }
      _maxRecord.push_back(maxRecord);
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}


class TtcMonitor : public TGMainFrame , public TTimer {

public:
  TtcMonitor(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h,
	     std::string f, unsigned r, int s=60)
    : TGMainFrame(p,w,h),
      _runPath(f), _runNumber(r), _sleepSecs(s),
      _canvas(0), _timeLine(0), _timeDelta(0), _tsDelta(0),
      _tfit(0), _paves(0)
  {
    _canvas = new TRootEmbeddedCanvas ("DHCal Trigger Timing Check",this,w,h);
    AddFrame(_canvas);

    _pad[0] = new TPad("TLine","TLine", 0, 0.6, 1, 1);
    _pad[0]->SetLeftMargin(0.05);
    _pad[0]->SetRightMargin(0.05);
    _pad[0]->Draw();
    _pad[1] = new TPad("TRate","TRate",0, 0.15, 0.5, 0.6);
    _pad[1]->Draw();
    _pad[2] = new TPad("TStamp","TStamp",0.5, 0.15, 1, 0.6);
    _pad[2]->Draw();
    _pad[3] = new TPad("Report","Report",0, 0, 1, 0.15);
    _pad[3]->Draw();

    _dtl = new DhcTimeLine;
    _dhctts = new ChkDhcTts;

    _arena = new RcdArena;
    _reader = new RunReader;

    _reader->directory(_runPath);
    assert(_reader->open(_runNumber));

    SetWindowName("DHCal Trigger Timing Check");

    MapSubwindows();
    MapWindow();
    MoveResize(x, y);

    SetTime(_sleepSecs*1000);
    gSystem->AddTimer(this);
  }

  virtual ~TtcMonitor()
  {
    _reader->close();
    delete _arena;
  }

  void CloseWindow()
  {
    gApplication->Terminate();
  }

  void reset() {

    if (_timeLine)
      delete _timeLine;

    if (_timeDelta)
      delete _timeDelta;

    if (_timeDeltaNO)
      delete _timeDeltaNO;

    if (_tsDelta)
      delete _tsDelta;

    if (_paves)
      delete (_paves);

    if (_stack)
      delete(_stack);

    if (_legend)
      delete _legend;
  }

  Bool_t Notify() {

    // Actions after timer time-out
    while(_reader->read(*_arena)) {

      if (_arena->recordType() == RcdHeader::configurationStart) {
	SubAccessor accessor(*_arena);
	std::vector<const DaqConfigurationStart*>
	  v(accessor.extract<DaqConfigurationStart>());

	assert(v.size()==1);
	_maximumTimeOfSpill = v[0]->maximumTimeOfSpill();
      }

      // last spill may not have complete event records
      if (_arena->recordType() == RcdHeader::spillStart &&
	  (UtlTime(true)-_arena->recordTime()) < _maximumTimeOfSpill)
	break;

      _dtl->record(*_arena);
      _dhctts->record(*_arena);
    }

    reset();

    std::vector<double> times = _dtl->event_time();
    std::vector<double> sizes = _dtl->max_record();
    std::vector<double> tss = _dhctts->hit_time();
    std::vector<std::pair<double,bool> > tts = _dhctts->trg_time();

    _timeLine = new TGraphAsymmErrors(times.size(), &times.front(), &sizes.front());
    _timeDelta = new TH1F("timeDelta","timeDelta", 110, 0, 0.11);
    _timeDeltaNO = new TH1F("timeDeltaNO","timeDelta", 110, 0, 0.11);
    _tsDelta = new TH1F("tsDelta","tsDelta", 25, 0, 25);
    _paves = new TPaveText(0.05, 0.1, 0.65, 0.9);
    _stack = new THStack("hs", "Delta Trigger Timestamps");
    _legend = new TLegend(0.5,0.87,0.99,0.92);
    
    for (unsigned i(1); i<_timeLine->GetN(); i++) {
      _timeLine->SetPointEYhigh(i, 0);
      _timeLine->SetPointEYlow(i, sizes[i]);
    }

    // timestamp delta for all triggers
    for (unsigned i(1); i<tts.size(); i++) {
      double tDelta(tts[i].first - tts[i-1].first);
      if (tDelta<0)
	tDelta += 1e7;
      _timeDelta->Fill(tDelta/1e7);
    }

    //  with oscillator triggers removed
    double tlast(tts[0].first);
    for (unsigned i(1); i<tts.size(); i++) {
      if (!tts[i].second) {
	double tDelta(tts[i].first - tlast);
	tlast = tts[i].first;
	if (tDelta<0)
	  tDelta += 1e7;
	_timeDeltaNO->Fill(tDelta/1e7);
      }
    }

    std::vector<double>::const_iterator it(tss.begin());
    for (; it!=tss.end(); it++)
      _tsDelta->Fill(*it);

    _canvas->GetCanvas()->cd();
    _pad[0]->cd()->SetLogy(1);
    _timeLine->SetTitle("Max Record Size Time Line");
    _timeLine->GetHistogram()->GetXaxis()->SetTimeDisplay(1);
    _timeLine->Draw("APZ");

    _pad[1]->cd()->SetLogy(1);
    _tsDelta->SetTitle("Trigger - HitData Timestamp");
    _tsDelta->Draw();

    _pad[2]->cd()->SetLogy(1);
    if (_tfit)
      delete _tfit;
    _tfit = new TF1("tdk", "expo(0)+expo(2)", 0, 0.1);

    _tfit->SetRange(0.001, 0.05);
    _timeDelta->Fit(_tfit, "QR");

    double tmax = std::min(0.1 - 0.001,
			   (log(0.1) -
			    std::min(_tfit->GetParameter(0),_tfit->GetParameter(2)))
			   /std::max(_tfit->GetParameter(1),_tfit->GetParameter(3)));
    
    _tfit->SetRange(0.001, tmax);
    _timeDelta->Fit(_tfit, "QR");

    int nRate1 = int(-_tfit->GetParameter(1));
    int nRate3 = int(-_tfit->GetParameter(3));
    std::ostringstream sout;
    sout << "Time Between Triggers, Fitted Rates "
	 << std::min(nRate1,nRate3) << " ( + " << std::max(nRate1,nRate3) << " ) Hz";

    _timeDelta->SetFillStyle(3001);
    _timeDelta->SetFillColor(kRed);
    _timeDeltaNO->SetFillStyle(3001);
    _timeDeltaNO->SetFillColor(kGreen);

    _stack->Add(_timeDelta);
    //_stack->Add(_timeDeltaNO);
    _stack->SetNameTitle("Time Between Triggers", sout.str().c_str());
    _stack->Draw("nostack");

    _legend->SetTextSize(0.035);
    sout.str("");
    sout << "All Triggers (" << _timeDelta->GetEntries() << ")";
    _legend->AddEntry(_timeDelta, sout.str().c_str(), "f");
    //sout.str("");
    //sout << "No Oscillator Triggers (" << _timeDeltaNO->GetEntries() << ")";
    //_legend->AddEntry(_timeDeltaNO, sout.str().c_str(), "f");
    _legend->Draw();

    _pad[3]->cd();
    _paves->SetTextAlign(12);

    sout.str("");
    sout << " Last updated: " << _dhctts->_recordTime << std::endl;;
    _paves->AddText(sout.str().c_str());

    sout.str("");
    sout << _dhctts->_outOfTime << " Out of Time Events" << std::endl;
    _paves->AddText(sout.str().c_str());

    sout.str("");
    sout << _dhctts->_missingData << " events with missing data" << "                  ";
    sout << _dhctts->_earlyData << " events with early data (timestamps too small)" << std::endl;
    _paves->AddText(sout.str().c_str());

    sout.str("");
    sout << _dhctts->_noData << " events with no data" << "                           ";
    sout << _dhctts->_lateData << " events with late data (timestamps too large)" << std::endl;
    _paves->AddText(sout.str().c_str());

    sout.str("");
    sout << "Cerenkov:"
	 << " Outer fraction: " << (double)_dhctts->_nAts/(double)_timeDeltaNO->GetEntries() << " / "
	 << " inner fraction: " << (double)_dhctts->_nBts/(double)_timeDeltaNO->GetEntries()
	 << std::endl;
    _paves->AddText(sout.str().c_str());

    _paves->SetFillColor(43);
    _paves->Draw();

    _canvas->GetCanvas()->Update();

    TTimer::Reset();
    return kFALSE;
  }

private:
  const std::string _runPath;
  const unsigned _runNumber;
  const int _sleepSecs;

  DhcTimeLine* _dtl;
  ChkDhcTts* _dhctts;

  RcdArena* _arena;
  RunReader* _reader;

  UtlTimeDifference _maximumTimeOfSpill;

  TRootEmbeddedCanvas* _canvas;
  TPad* _pad[4];
  TGraphAsymmErrors* _timeLine;
  TH1F* _timeDelta;
  TH1F* _timeDeltaNO;
  TH1F* _tsDelta;
  TF1* _tfit;
  TPaveText* _paves;
  THStack* _stack;
  TLegend* _legend;
};


int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const int xpos(argh.optionArgument('x',750,"X-pos"));
  const int ypos(argh.optionArgument('y',400,"Y-pos"));
  const unsigned width(argh.optionArgument('w',800,"Width"));
  const unsigned height(argh.optionArgument('v',600,"Height"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path of run data"));
  const int sleepSecs(argh.optionArgument('s',60,"Sleep period (secs)"));

  if(argh.help()) return 0;

  gROOT->SetStyle();
  gStyle->SetOptStat("e");

  TDatime da(1995,01,01,00,00,00);
  gStyle->SetTimeOffset(da.Convert());

  TApplication application("Trigger Time Check Application",0,0);
  TtcMonitor* tm = new TtcMonitor(gClient->GetRoot(), xpos, ypos, width, height,
				  runPath, runNumber, sleepSecs);
  application.Run();
}
