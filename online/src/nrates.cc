#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include "TGClient.h"

#include "TCanvas.h"
#include "THStack.h"
#include "TLegend.h"
#include "TH2I.h"
#include "TF1.h"

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runSecs(argh.optionArgument('s',60,"Use only first s secondss of run"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  DhcEB* dhceb(new DhcEBIntTrg(runNumber));

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::map<std::pair<double,double>, std::vector<unsigned> > tdeltaTs;
  std::map<std::pair<double,double>, std::vector<DhcPadPts*>*> xyzt;
  std::map<unsigned, std::pair<double,double> > rpcmap;

  UtlTime _start(0,0);
  UtlTimeDifference _elapse(0,0);
  bool acquisitionStarted(false);

  while(reader.read(*arena)) {

    switch(arena->recordType()) {

    case RcdHeader::runStart: {

      DhcCableMap cmap(runNumber);
      DhcPadPts* pt;
      std::map<double, double>  _layerz;

      for (unsigned s(4); s<20; s++)
	for (unsigned d(0); d<12; d++)
	  if ((pt = cmap.origin(220, s, d)) != 0)
	    _layerz[pt->Pz] = 1;

      double layer(1);
      std::map<double, double>::iterator mit;
      for (mit=_layerz.begin(); mit!=_layerz.end(); mit++)
	(*mit).second = layer++;

      for (unsigned c(0); c<=1; c++)
	for (unsigned s(0); s<16; s++)
	  for (unsigned d(0); d<12; d++)
	    if ((pt = cmap.origin(c+220, s+4, d)) != 0) {
	      unsigned id(c<<16 | s<<8 | d);
	      double layer(_layerz[pt->Pz]);
	      double tmb(pt->Px > 0 ? (-pt->Py + 16)/32 : (-pt->Py + 48)/32);
	      rpcmap[id] = std::make_pair(layer,tmb);
	    }
      break;
    }

    case RcdHeader::acquisitionStart: {
      _start = arena->RcdHeader::recordTime();
      acquisitionStarted = true;
      break;
    }

    default:
      if (acquisitionStarted)
	_elapse = arena->RcdHeader::recordTime() -_start;
      break;
    }

    dhceb->record(*arena);

    if (!dhceb->raw_hitdata().empty()) {
      std::vector<DhcFeHitData*>::const_iterator ih(dhceb->raw_hitdata().begin());
      std::vector<DhcFeHitData*>::const_iterator ihe(dhceb->raw_hitdata().end());

      for (; ih<ihe; ih++) {

	unsigned id((*ih)->vmead()<<16 | (*ih)->dcolad()<<8 | (*ih)->dconad());

	if (rpcmap.find(id) != rpcmap.end()) {
	  std::pair<double,double> pid(rpcmap[id]);

	  if (xyzt.find(pid) == xyzt.end())
	    xyzt[pid] = new std::vector<DhcPadPts*>;
	  
	  dhceb->hits2pts(*ih, xyzt[pid]);
	}
      }
      std::map<std::pair<double,double>, std::vector<DhcPadPts*>*>::iterator mit(xyzt.begin());
      for (; mit!=xyzt.end(); mit++) {

	std::vector<DhcPadPts*>::const_iterator it((*mit).second->begin());
	std::vector<DhcPadPts*>::const_iterator ite((*mit).second->end());

	for (++it; it<ite; it++) {
	  unsigned tDelta(((*it)->Ts - (*(it-1))->Ts));

	  // record spans counter reset
	  if (tDelta > 5000000)
	    break;

	  if (tDelta > 0)
	    tdeltaTs[(*mit).first].push_back(tDelta);
	  delete *(it-1);
	}
	delete *(it-1);
	(*mit).second->clear();
      }

      // clear events
      dhceb->clear();
    }

    if (_elapse.deltaTime() > runSecs)
      break;
  }
  reader.close();

  delete dhceb;
  delete arena;

  TApplication _application("Noise Rate Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eo");
  gStyle->SetOptFit(1100);
  gStyle->SetStatFontSize(0.024);

  std::map<double, TH2I*> txy;

  std::map<std::pair<double,double>, std::vector<unsigned> >::iterator mit;

  unsigned tmaxi(0);
  for (mit=tdeltaTs.begin(); mit!=tdeltaTs.end(); mit++)
    tmaxi = std::max(tmaxi, *(std::max_element((*mit).second.begin(), (*mit).second.end()-1)));

  double tmaxd(std::min(tmaxi/2e7, 0.05));
  for (mit=tdeltaTs.begin(); mit!=tdeltaTs.end(); mit++) {

    double layer(((*mit).first).first);
    double tmb(((*mit).first).second);
    if (txy.find(tmb) == txy.end()) {
      txy[tmb] = new TH2I;
      txy[tmb]->SetBins(200, 0, tmaxd, 55, 0, 55);
      txy[tmb]->GetXaxis()->SetTitle("Hit tDelta (sec)");
      txy[tmb]->GetYaxis()->SetTitle("Layer No.");
      txy[tmb]->GetXaxis()->SetLabelSize(0.03);
      txy[tmb]->GetXaxis()->SetTitleSize(0.03);
      txy[tmb]->GetXaxis()->SetTitleOffset(2);
      txy[tmb]->GetYaxis()->SetLabelSize(0.03);
      txy[tmb]->GetYaxis()->SetTitleSize(0.03);
      txy[tmb]->GetYaxis()->SetTitleOffset(2);
    }
    std::vector<unsigned>::iterator it((*mit).second.begin());
    for (; it!=(*mit).second.end(); it++) {
      txy[tmb]->Fill((*it)/1e7, layer);
    }      
  }

  std::map<unsigned, std::string> rpcy;
  std::map<unsigned, unsigned> rpcm;
  rpcy[0] = "Top    ";
  rpcy[1] = "Middle ";
  rpcy[2] = "Bottom ";
  rpcm[0] = 22;
  rpcm[1] = 21;
  rpcm[2] = 23;

  TCanvas* canvas[8];
  unsigned c(0);
  std::map<double, TH2I*>::iterator it;

  for (it=txy.begin(); it!=txy.end(); it++) {

    std::ostringstream sout;
    sout << "tDelta vs LayerZ (" << rpcy[c] << ")";
    canvas[c] = new TCanvas(sout.str().c_str(), sout.str().c_str(),
			    gClient->GetDisplayWidth()/2 - 640*((3-c)/2),
			    gClient->GetDisplayHeight()/2 - 480*((c+1)%2),
			    640, 480);

    (*it).second->SetTitle(rpcy[c].c_str());
    (*it).second->Draw("lego");
    gPad->SetLogz(1);
    gPad->SetPhi(gPad->GetPhi() - 60);

    canvas[c++]->Update();
  }

  canvas[c] = new TCanvas("Fitted Rates", "Fitted Noise Rates",
			  gClient->GetDisplayWidth()/2 - 640*((3-c)/2),
			  gClient->GetDisplayHeight()/2 - 480*((c+1)%2),
			  640, 480);
  canvas[c]->Divide(1,2);

  unsigned cc(0);
  THStack stack("hs", "Noise Rates (Hz)");
  THStack stackd("hsd", "Noise Rate Distribution");
  THStack stacks("hs", "Sorted Noise Rates (Hz)");
  TLegend * legend = new TLegend(0.65,0.7,0.9,0.9);
  legend->SetTextSize(0.035);
  legend->SetHeader("Mean Noise Rates (Hz)");

  std::map<double, std::map<double, double> > stxy;

  std::vector<TObjArray*> aSlices;
  for (it=txy.begin(); it!=txy.end(); it++) {

    TF1* tfits = new TF1("tdks", "expo(0)", 0, tmaxd/2);
    tfits->SetRange(tmaxd/200, tmaxd/2);
    aSlices.push_back(new TObjArray);
    (*it).second->FitSlicesX(tfits,0,-1,0,"QNR",aSlices.back());

    TObjArray oa = *(aSlices.back());
    TH1F* txy_1 = (TH1F*)oa[1];
    txy_1->Scale(-1);
    txy_1->SetNameTitle(rpcy[cc].c_str(), rpcy[cc].c_str());
    txy_1->SetMarkerStyle(rpcm[cc]);
    txy_1->SetMarkerColor(2 + cc);

    stack.Add(txy_1);

    TH1F* nrate = new TH1F;
    nrate->SetBins(1000, 100, 100000);
    nrate->SetLineColor(2+cc);
    nrate->SetLineWidth(2);
    for (int i(1); i<txy_1->GetNbinsX(); i++)
      if (txy_1->GetBinContent(i)) {
	nrate->Fill(txy_1->GetBinContent(i));
	stxy[(*it).first][txy_1->GetBinContent(i)] = i;
      }

    stackd.Add(nrate);

    std::ostringstream sout;
    sout << rpcy[cc] << " - " << floor(nrate->GetMean());;
    legend->AddEntry(nrate, sout.str().c_str(), "l");

    cc++;
  }

  canvas[c]->cd(1)->SetLogy(1);
  stack.Draw("nostack");
  canvas[c]->cd(2)->SetLogx(1);
  stackd.Draw();
  legend->Draw();
  canvas[c]->Update();

  TObjArray* oa = stack.GetStack();
  std::map<double, TH2I*> otxy;
  std::map<double, TH1F*> qtxy;
  std::map<double, std::map<double, double> >::iterator sit(stxy.begin());
  for (; sit!=stxy.end(); sit++) {
    double tmb((*sit).first);
    if (otxy.find(tmb) == otxy.end()) {

      if (oa->At((int)tmb)) {
	qtxy[tmb] = new TH1F;
	((TH1F*)oa->At((int)tmb))->Copy(*qtxy[tmb]);
	qtxy[tmb]->Reset();

	otxy[tmb] = new TH2I;
	txy[tmb]->Copy(*otxy[tmb]);
	otxy[tmb]->Reset();
      }
    }

    if (qtxy.find(tmb) != qtxy.end()) {
      unsigned j(1);
      std::map<double, double>::iterator mit((*sit).second.begin());
      for (; mit!=(*sit).second.end(); mit++, j++) {
	qtxy[tmb]->SetBinContent(j, (*mit).first);

	for (int i(1); i<txy[tmb]->GetNbinsX(); i++)
	  otxy[tmb]->SetBinContent(i, j, txy[tmb]->GetBinContent(i,(int)((*mit).second)));
      }
      qtxy[tmb]->SetMarkerStyle(rpcm[(int)tmb]);
      qtxy[tmb]->SetMarkerColor(2 + (int)tmb);
      stacks.Add(qtxy[tmb]);
    }
  }

  c++;
  for (it=otxy.begin(); it!=otxy.end(); it++) {

    std::ostringstream sout;
    sout << "Sorted by Rate (" << rpcy[c-4] << ")";
    canvas[c] = new TCanvas(sout.str().c_str(), sout.str().c_str(),
			    gClient->GetDisplayWidth()/2 - 640*((7-c)/2),
			    gClient->GetDisplayHeight()/2 - 480*((c+1)%2),
			    640, 480);

    (*it).second->SetTitle(rpcy[c].c_str());
    (*it).second->Draw("lego");
    gPad->SetLogz(1);
    gPad->SetPhi(gPad->GetPhi() - 60);

    canvas[c++]->Update();
  }

  canvas[c] = new TCanvas("Sorted Rates", "Sortted Noise Rates",
			  gClient->GetDisplayWidth()/2 - 640*((7-c)/2),
			  gClient->GetDisplayHeight()/2 - 480*((c+1)%2),
			  640, 480);

  canvas[c]->cd(1)->SetLogy(1);
  stacks.Draw("Pnostack");
  legend->Draw();
  canvas[c]->Update();

  _application.Run();

}
