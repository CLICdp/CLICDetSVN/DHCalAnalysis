#include <string>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const unsigned minLayers(argh.optionArgument('l',8,"Minimum layer cut"));
  const std::string runType(argh.optionArgument('t',"IntTrg","EB run type"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path of run data"));

  if(argh.help()) return 0;

  DhcEB* dhceb;

  if (runType == "IntTrg")
    dhceb = new DhcEBIntTrg(runNumber);
  else if (runType == "ExtTrg")
    dhceb = new DhcEBExtTrg(runNumber);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::vector<std::vector<DhcPadPts*>*> pts;
  std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;

  while (1) {

    if (pts.empty()) {
      // read more records until some events are built
      while(reader.read(*arena)) {

	dhceb->record(*arena);

	if (!dhceb->raw_hitdata().empty()) {
	  // get copy of events
	  pts = dhceb->built_points();
	  break;
	}
      }
      // if still no events, at end of data
      if (pts.empty())
	break;

      for (ipts=pts.begin(); ipts!=pts.end(); ipts++) {
	if (minLayers <= dhceb->layers(*ipts))
	  dhceb->show_points(*ipts);
      }
      if(ipts==pts.end()) {
	// clear events
	dhceb->clear();
	pts.clear();
      }
    }
  }
  reader.close();

  delete dhceb;
  delete arena;
}
