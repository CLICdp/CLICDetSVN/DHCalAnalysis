#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>
#include <cassert>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdWriterAsc.hh"

#include "DaqRunStart.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

#include "ChkCount.hh"
#include "ChkPrint.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const bool useReadAsc(argh.option('a',"Ascii input file"));
  const bool slwRead(argh.option('s',"Slow data input file"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const unsigned numberOfRecords(argh.optionArgument('r',0x7fffffff,"Number of records"));
  const unsigned vnum(argh.lastArgument(999999));

  if(argh.help()) return 0;

#ifdef RCD_READER
  ostringstream sout;
  sout << vnum;

  RcdReader *reader(0);
  if(useReadAsc) reader=new RcdReaderAsc();
  else           reader=new RcdReaderBin();

  if(slwRead) assert(reader->open(std::string("data/slw/Slw")+sout.str()));
  else        assert(reader->open(std::string("data/run/Run")+sout.str()));
#else
  RunReader *reader(new RunReader);
  reader->directory(runPath);
  reader->open(vnum);
#endif

  RcdArena &arena(*(new RcdArena));
  ChkCount ct;
  ChkPrint hn;

  // This enables printing of all subrecord headers, but not their data
  //hn.enableSubrecords(true);

  // This enables printing of all record headers
  hn.enableType(true);

  // These enable/disable printing of particular types of record headers
  //hn.enableType(RcdHeader::startUp,true);
  //hn.enableType(RcdHeader::slowControl,true);
  //hn.enableType(RcdHeader::slowReadout,true);
  //hn.enableType(RcdHeader::configurationStart,true);
  //hn.enableType(RcdHeader::configurationStop,true);
  //hn.enableType(RcdHeader::configurationEnd,true);
  //hn.enableType(RcdHeader::trigger,false);
  //hn.enableType(RcdHeader::event,false);

  // This enables printing of all subrecord data
  //hn.enable(true);

  // These enable printing of groups of subrecord data
  //hn.enable(SubHeader::daq,true);
  //hn.enable(SubHeader::crc,true);
  //hn.enable(SubHeader::emc,true);
  //hn.enable(SubHeader::ahc,true);
  //hn.enable(SubHeader::trg,true);

  // These enable/disable printing of particular types of subrecord data
  //hn.enable(subRecordType<DaqRunEndV0>(),true);
  //hn.enable(subRecordType<DaqRunEnd>(),true);
  hn.enable(subRecordType<DaqEvent>(),true);
  hn.enable(subRecordType<DaqTrigger>(),true);

  //hn.enable(subRecordType< CrcLocationData<CrcAdm1025StartupData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcAdm1025SlowControlsData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcAdm1025SlowReadoutData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcLm82StartupData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcLm82SlowControlsData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcLm82SlowReadoutData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcBeRunData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcBeEventDataV0> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcBeEventData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcBeTrgRunData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcFeConfigurationData> >(),true);
  //hn.enable(subRecordType< CrcLocationData<CrcVlinkEventData> >(),true);

  //hn.enable(subRecordType< CrcLocationData<EmcFeConfigurationData> >(),true);
  
  //hn.enable(subRecordType< MpsLocationData<MpsUsbDaqBunchTrainData> >(),true);
  //hn.enable(subRecordType< MpsLocationData<MpsSensor1BunchTrainData> >(),true);

  //hn.enable(subRecordType<BmlLc1176RunData>(),true);
  //hn.enable(subRecordType<BmlLc1176ConfigurationData>(),true);
  //hn.enable(subRecordType<BmlLc1176EventData>(),true);

  //hn.enable(subRecordType< DhcReadoutConfigurationData >(),true);
  //hn.enable(subRecordType< DhcTriggerConfigurationData >(),true);
  //hn.enable(subRecordType< DhcLocationData<DhcBeConfigurationData> >(),true);
  //hn.enable(subRecordType< DhcLocationData<DhcDcConfigurationData> >(),true);
  //hn.enable(subRecordType< DhcLocationData<DhcFeConfigurationData> >(),true);
  //hn.enable(subRecordType< TtmLocationData<TtmConfigurationData> >(),true);
  //hn.enable(subRecordType< TtmLocationData<TtmTriggerData> >(),true);
  //hn.enable(subRecordType< DhcLocationData<DhcTriggerData> >(),true);
  hn.enable(subRecordType< DhcLocationData<DhcEventData> >(),true);
  //hn.enable(subRecordType< DhcLocationData<DhcDcRunData> >(),true);

  //hn.enable(subRecordType< BmlCaen1290ReadoutConfigurationData >(),true);
  //hn.enable(subRecordType< BmlLocationData<BmlCaen1290OpcodeData> >(),true);
  //hn.enable(subRecordType< BmlLocationData<BmlCaen1290ConfigurationData> >(),true);
  //hn.enable(subRecordType< BmlLocationData<BmlCaen1290TriggerData> >(),true);
  //hn.enable(subRecordType< BmlLocationData<BmlCaen1290EventData> >(),true);

  unsigned i(0);
  while(reader->read(arena) && i<numberOfRecords) {
    ct.record(arena);
    hn.record(arena);
    i++;
  }

  ct.print(cout);

  reader->close();
  delete reader;
}
