#include <iostream>
#include <sstream>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "DhcWriterBin.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  RcdArena* arena(new RcdArena);
  DhcWriterBin writer;

  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::ostringstream sout;
  sout << runNumber;

  assert (writer.open(std::string(runPath+"/Dhc" )+sout.str()));

  while(reader.read(*arena)) {
    writer.convert(*arena);
  }

  reader.close();
  writer.close();

  delete arena;
}
