#include <iostream>
#include <sstream>

#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"

#include "EbDhcRecords.hh"

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  //unsigned slot(argh.optionArgument('s',3,"Slot"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  bool runType(argh.optionArgument('e',0,"Run Type, 0-noise, 1-cosmic"));
    // 0 for noise, 1 for cosmic

  if(argh.help()) return 0;

  //TApplication _application("Plots App",0,0);
  //gROOT->Reset();

  EbDhcRecords* ebmain = new EbDhcRecords();
  ebmain->openWriter(runNumber,runType);
//  HstDhcPlots* hp = new HstDhcPlots();

  std::ostringstream sout;
  sout << runNumber;

// initialize eb parameters
  RcdArena* arenaIP(new RcdArena);
  RunReader readerIP;
  //RcdReaderBin readerIP;

  //readerIP.printLevel(0);
  assert(readerIP.open(runNumber));
  //assert(readerIP.open(std::string("data/run/Run" )+sout.str()+
  //                   std::string(".000")));
  while(readerIP.read(*arenaIP)) {
    if(ebmain!=0) {
       ebmain->eventIP(*arenaIP);
    }
  }

  readerIP.close();
  delete arenaIP;

// run event builder on data
  RcdArena* arena(new RcdArena);
  RunReader reader;
  //RcdReaderBin reader;

  //reader.printLevel(0);
  assert(reader.open(runNumber));
  //assert(reader.open(std::string("data/run/Run" )+sout.str()+
//		     std::string(".000")));
    while(reader.read(*arena)) {
      if(ebmain!=0) {
         ebmain->event(*arena, runType); // builds events
      }
    }

    reader.close();

  delete arena;
  //ebmain->update();
  ebmain->closeWriter();
 
  //_application.Run(1);

  delete ebmain;
  //delete hp;

}
