#include <sstream>
#include <vector>
#include <map>
#include <cassert>

#include "RcdArena.hh"
#include "RcdReaderBin.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DaqConfigurationStart.hh"

#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcFeConfigurationData.hh"


class ChkDhcSlow : public RcdUserRO {

public:
  ChkDhcSlow(const bool& verbose);
  virtual ~ChkDhcSlow() {}

  bool record(const RcdRecord &r);

private:
  std::map<unsigned, const DhcBeConfigurationData*> _beData;
  std::map<unsigned, const DhcDcConfigurationData*> _dcData;
  std::map<unsigned, const DhcFeConfigurationData*> _feData;
  std::map<unsigned, bool> _lockMap;

  bool _verbose;

};

ChkDhcSlow::ChkDhcSlow(const bool& verbose)
  : RcdUserRO(9), _verbose(verbose) {}

bool ChkDhcSlow::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);

    std::cout << std::endl << r.recordTime()
	      << " Dhc configuration number " << v[0]->configurationNumberInRun()
	      << " in run." << std::endl << std::endl;;
    
    std::map<unsigned, const DhcBeConfigurationData*>::const_iterator bet;
    std::map<unsigned, const DhcDcConfigurationData*>::const_iterator dct;
    std::map<unsigned, const DhcFeConfigurationData*>::const_iterator fet;

    for (bet=_beData.begin(); bet!=_beData.end(); bet++)
      delete bet->second;

    for (dct=_dcData.begin(); dct!=_dcData.end(); dct++)
      delete dct->second;

    for (fet=_feData.begin(); fet!=_feData.end(); fet++)
      delete fet->second;

    _beData.clear();
    _dcData.clear();
    _feData.clear();
    _lockMap.clear();

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    if (be.size()>0) {

      std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
      for (bit=be.begin(); bit!=be.end(); bit++) {

	UtlPack loc(0);
	loc.byte(3, (*bit)->crateNumber());
	loc.byte(2, (*bit)->slotNumber());

	if ((*bit)->write()) {

	  if (_beData.find(loc.word()) != _beData.end())
	    delete _beData[loc.word()];

	  _beData[loc.word()] = new DhcBeConfigurationData(*(*bit)->data());
	}
	else {

	  UtlPack dconEnabled(0);
	  dconEnabled.word(((*bit)->data()->dconEnable()));

	  for (unsigned i(0); i<12; i++) {
	    loc.byte(1, i);
	    for (unsigned j(0); j<24; j++) {
	      loc.byte(0,j);
	      _lockMap[loc.word()] = (dconEnabled.bit(i) == dconEnabled.bit(i+16));
	    }

	    if (dconEnabled.bit(i) != dconEnabled.bit(i+16))
	      std::cout << " Crate " << static_cast<unsigned int>(loc.byte(3))
			<< " Slot " << static_cast<unsigned int>(loc.byte(2))
			<< " Data Collector input " << i << " not locked"
			<< std::endl;
	  }
	}
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    if (dc.size()>0) {

      std::vector<const DhcLocationData<DhcDcConfigurationData>*>::iterator dit;
      for (dit=dc.begin(); dit!=dc.end(); dit++) {

	UtlPack loc(0);
	loc.byte(3, (*dit)->crateNumber());
	loc.byte(2, (*dit)->slotNumber());
	loc.byte(1, (*dit)->dhcComponent());

	if ((*dit)->write()) {

	  if (_dcData.find(loc.word()) != _dcData.end())
	    delete _dcData[loc.word()];

	  _dcData[loc.word()] = new DhcDcConfigurationData(*(*dit)->data());
	}
	else {

	  // test for DCOL port not locked
	  if (!_lockMap[loc.word()])
	    continue;

	  const DhcDcConfigurationData* dcData;
	  if (_dcData.find(loc.word()) != _dcData.end())
	    dcData = _dcData[loc.word()];
	  else
	    dcData = _dcData.rbegin()->second;

	  // compare written/read configuration data
	  bool cfgd(true);

	  if (*dcData->control() != *(*dit)->data()->control()) cfgd=false;
	  if (*dcData->triggerWidth() != *(*dit)->data()->triggerWidth()) cfgd=false;
	  if (*dcData->triggerDelay() != *(*dit)->data()->triggerDelay()) cfgd=false;

	  if (!cfgd) {
	    std::cout << " "
		      << " (" << static_cast<unsigned int>((*dit)->location().crateNumber())
		      << ", " << static_cast<unsigned int>((*dit)->location().slotNumber())
		      << ", " << static_cast<unsigned int>((*dit)->location().dhcComponent())
		      << ") Data Concentrator Chip is not configured correctly."
		      << std::endl;
	    if (_verbose)
	      (*dit)->print(std::cout,"");
	  }
	}
      }
    }

    std::vector<const DhcLocationData<DhcFeConfigurationData>*>
      fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

    if (fe.size()>0) {

      std::vector<const DhcLocationData<DhcFeConfigurationData>*>::iterator fit;
      for (fit=fe.begin(); fit!=fe.end(); fit++) {

	UtlPack loc(0);
	loc.byte(3, (*fit)->crateNumber());
	loc.byte(2, (*fit)->slotNumber());
	loc.byte(1, (*fit)->dhcComponent());
	loc.byte(0, *(*fit)->data()->chipid());

	if ((*fit)->write()) {

	  if (_feData.find(loc.word()) != _feData.end())
	    delete _feData[loc.word()];

	  _feData[loc.word()] = new DhcFeConfigurationData(*(*fit)->data());
	}
	else {

	  // test for DCOL port not locked
	  if (!_lockMap[loc.word()])
	    continue;

	  const DhcFeConfigurationData* feData;
	  if (_feData.find(loc.word()) != _feData.end())
	    feData = _feData[loc.word()];
	  else
	    feData = _feData.rbegin()->second;
	
	  unsigned maxChip(24);

	  const unsigned char* _inj = feData->inj();
	  const unsigned char* _kill = feData->kill();

	  bool cfgd(true);

	  if (*feData->plsr() != *(*fit)->data()->plsr()) cfgd=false;
	  if (*feData->intd() != *(*fit)->data()->intd()) cfgd=false;
	  if (*feData->shp2() != *(*fit)->data()->shp2()) cfgd=false;
	  if (*feData->shp1() != *(*fit)->data()->shp1()) cfgd=false;
	  if (*feData->blrd() != *(*fit)->data()->blrd()) cfgd=false;
	  if (*feData->vtnd() != *(*fit)->data()->vtnd()) cfgd=false;
	  if (*feData->vtpd() != *(*fit)->data()->vtpd()) cfgd=false;
	  if (*feData->dcr() != *(*fit)->data()->dcr()) cfgd=false;

	  if (maxChip>4)
	    for (unsigned i(0); i<8; i++) {
	      if (_inj[i] != ((*fit)->data()->inj())[i]) cfgd=false;
	      if (_kill[i] != ((*fit)->data()->kill())[i]) cfgd=false;
	    }

	  if (!cfgd) {
	    std::cout << " "
		      << " (" << static_cast<unsigned int>((*fit)->location().crateNumber())
		      << ", " << static_cast<unsigned int>((*fit)->location().slotNumber())
		      << ", " << static_cast<unsigned int>((*fit)->location().dhcComponent())
		      << ") DCAL Chip "
		      << static_cast<unsigned int>(*(*fit)->data()->chipid())
		      << " is not configured correctly."
		      << std::endl;
	    if (_verbose)
	      (*fit)->print(std::cout,"");
	  }
	}
      }
    }

    break;
  }

  default:
    break;
  }

  return true;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const std::string runPath(argh.optionArgument('f',"data/slw","Path where slw data is located"));
  const bool verbose(argh.option('v',"Verbose print"));
  const unsigned vnum(argh.lastArgument(999999));

  if(argh.help()) return 0;

  std::ostringstream sout;
  sout << runPath << "/Slw" << vnum;

  RcdReader *reader(0);
  reader=new RcdReaderBin();
  assert(reader->open(sout.str()));

  RcdArena* arena(new RcdArena);
  
  ChkDhcSlow* cds = new ChkDhcSlow(verbose);

  while(reader->read(*arena)) {
    if(cds!=0) cds->record(*arena);
  }
  reader->close();

  delete cds;
  delete arena;
  delete reader;
}
