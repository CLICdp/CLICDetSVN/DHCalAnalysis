// prototype event display

#include <sstream>
#include <map>
#include <utility>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RcdRecord.hh"
#include "RunReader.hh"

#include "DaqEvent.hh"
#include "SubAccessor.hh"

#include "UtlArguments.hh"

#include "DhcEventData.hh"
#include "DhcFeHitData.hh"

#include <TROOT.h>
#include <TError.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TApplication.h>
#include <TGClient.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGSlider.h>
#include <TGStatusBar.h>
#include <TGFrame.h>

#include "TView3D.h"
#include "TPolyMarker3D.h"
#include "TTimer.h"
#include "TText.h"

#include "DhcEB.hh"
#include "DhcGeom.hh"

class EDispEvent {

public:
  EDispEvent(unsigned& e, UtlTime& t, std::vector<DhcPadPts>* p, unsigned f)
    : eventNumber(e), recordTime(t), pts(p), ttmFlag(f)
  {}
  ~EDispEvent()
  { delete pts; }

public:
  unsigned eventNumber;
  UtlTime recordTime;
  std::vector<DhcPadPts>* pts;
  unsigned ttmFlag;
};

class EDispFrame : public TGMainFrame , public TTimer {

  enum ECommandIdentifiers {
    B__PAUSE,
    B__NEXT,
    B__PREVIOUS,
    B__PRINT,
    B__EXIT,
    S_DELAY,
  };

private:
  TRootEmbeddedCanvas *fCanvas;
  TGStatusBar* fStatus;;
  TGTextButton* fPause;
  TGTextButton* fDraw;
  TGTextButton* fPrevious;
  TGLabel* fFaster;
  TGLabel* fSlower;
  TGHSlider* fDelay;
  TPad *pad[5];
  TView *view[5];
  TText *ttmFlag;

  TPolyMarker3D* _pm3d;
  std::vector<std::vector<DhcPadPts*>*> pts;
  std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;
  std::map<unsigned, EDispEvent*> _events;
  std::map<unsigned, Color_t> _pm3c;

  std::string _file;
  UtlTime _recordTime;
  unsigned _runNumber;
  unsigned _eventNumber;
  unsigned _eventNumberInRun;
  unsigned _eventIndex;
  unsigned _eventsKept;
  unsigned _sleepSecs;
  unsigned _minLayers;
  unsigned _maxPoints;
  unsigned _minPoints;
  unsigned _maxTDelta;
  unsigned _timeSince;
  std::string _rpcGeom;
  std::string _runPath;
  double _bbox;
  double _wangle;
  double _zpivot;
  unsigned _nviews;
  unsigned _ttmFlag;
  bool _intTrigger;

  RcdArena* _arena;
  RunReader _reader;

  DhcEB* _dhceb;
  DhcGeom* _dhcgeom;

  void setup();
  void cleanup();

  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
  Bool_t Notify();
  bool _running;

  bool acquire();
  void release(unsigned nKeep=0);

  bool DoDraw();
  void DoPrint();

public:
  EDispFrame(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h);
  virtual ~EDispFrame() {}

  void initialize();
  void runNumber(const unsigned& r);
  void sleepSecs(const unsigned& s);
  void minLayers(const unsigned& l);
  void maxPoints(const unsigned& p);
  void minPoints(const unsigned& p);
  void maxTDelta(const unsigned& t);
  void timeSince(const unsigned& t);
  void rpcGeom(const std::string& g);
  void runPath(const std::string& f);
  void bBox(const double& b);
  void wAngle(const double& a);
};

EDispFrame::EDispFrame(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h),
    _pm3d(0),
    _eventNumber(0xffffffff),
    _eventNumberInRun(0xffffffff),
    _eventIndex(0xffffffff),
    _eventsKept(1000),
    _nviews(0),
    _dhceb(0)
{
  SetCleanup(kDeepCleanup);

  fCanvas = new TRootEmbeddedCanvas ("Event Display",this,w,h);
  AddFrame(fCanvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGHorizontalFrame* hFrame = new TGHorizontalFrame(this, w, 40);
  fStatus = new TGStatusBar(hFrame, w/2, 40);
  fStatus->SetText("Processing data. Please wait..." ,0);
  hFrame->AddFrame(fStatus, new TGLayoutHints(kLHintsLeft, 4,4,3,4));
  
  TGTextButton* exit = new TGTextButton(hFrame, "&Exit", B__EXIT);
  exit->Associate(this);
  hFrame->AddFrame(exit, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  TGTextButton* print = new TGTextButton(hFrame, "Pr&int", B__PRINT);
  print->Associate(this);
  hFrame->AddFrame(print, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fDraw = new TGTextButton(hFrame, "&Next", B__NEXT);
  fDraw->Associate(this);
  hFrame->AddFrame(fDraw, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fPrevious = new TGTextButton(hFrame, "Pre&vious", B__PREVIOUS);
  fPrevious->Associate(this);
  hFrame->AddFrame(fPrevious, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fPause = new TGTextButton(hFrame, "&Pause", B__PAUSE);
  fPause->Associate(this);
  hFrame->AddFrame(fPause, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fSlower = new TGLabel(hFrame, "slower");
  hFrame->AddFrame(fSlower, new TGLayoutHints(kLHintsRight, 0,4,10,0));

  fDelay = new TGHSlider(hFrame, 80, kSlider1|kScaleBoth, S_DELAY);
  fDelay->Associate(this);
  fDelay->SetRange(0,8);
  hFrame->AddFrame(fDelay, new TGLayoutHints(kLHintsRight, 0,0,3,4));

  fFaster = new TGLabel(hFrame, "faster");
  hFrame->AddFrame(fFaster, new TGLayoutHints(kLHintsRight, 4,0,10,0));

  AddFrame(hFrame, new TGLayoutHints(kLHintsCenterX, 2,2,2,2));

  SetWindowName("DHCal RPC Event Display");

  MapSubwindows();
  MapWindow();
  MoveResize(x, y);
}

void EDispFrame::initialize()
{
  _arena = new RcdArena;
  _reader.directory(_runPath);
  assert(_reader.open(_runNumber));

  // determine run type
  while (!_dhceb) {

    _reader.read(*_arena);
    switch (_arena->recordType()) {

    case RcdHeader::runStart: {
      // Access the DaqRunStart
      SubAccessor accessor(*_arena);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::dhcNoise: {
	_dhceb = new DhcEBIntTrg(_runNumber);
	_intTrigger = true;
	break;
      }

      case DaqRunType::beamData:
      case DaqRunType::dhcBeam:
      case DaqRunType::dhcQinj:
      case DaqRunType::dhcCosmics: {
	_dhceb = new DhcEBExtTrg(_runNumber);
	_intTrigger = false;
	break;
      }

      default: {
	std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
	CloseWindow();
	break;
	}
      }
    }
    default:
      break;
    }
  }

  if (_rpcGeom == "Slice") {
    _dhcgeom = new DhcSliceGeom();
  }
  else if (_rpcGeom == "Cubic") {
    _dhcgeom = new DhcCubicGeom();
  }
  else if (_rpcGeom == "CubicTC") {
    _dhcgeom = new DhcCubicTCGeom();
  }
  else if (_rpcGeom == "CubicWTC") {
    _dhcgeom = new DhcCubicWTCGeom();
  }
  else if (_rpcGeom == "Cuboid") {
    _dhcgeom = new DhcCuboidGeom();
  }
  else {
    std::cerr << "Geometry " << _rpcGeom << " not implemented" << std::endl;
    CloseWindow();
  }

  if (_minPoints)
    _maxPoints = 0xffffffff;

  setup();

  fDelay->SetPosition(_sleepSecs);
  SetTime(_sleepSecs*1000+20);
  gSystem->AddTimer(this);
  _running=true;
}

void EDispFrame::CloseWindow()
{
  _reader.close();
  cleanup();

  gApplication->Terminate();
}

Bool_t EDispFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_BUTTON:

      switch(parm1) {
      case B__PAUSE:
	if (_running) {
	  _running=false;
	  TTimer::TurnOff();
	  fPause->SetText("&Run");
	} else {
	  _running=true;
	  TTimer::Reset();
	  TTimer::TurnOn();
	  fPause->SetText(" &Pause  ");
	}	  
	break;

      case B__NEXT:
	_eventNumber++;
	fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

	if (!DoDraw())
	  _eventNumber--;;
	break;

      case B__PREVIOUS:
	_eventNumber--;
	fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

	DoDraw();
	break;

      case B__PRINT:
	DoPrint();
	break;

      case B__EXIT:
	CloseWindow();   // this also terminates theApp
	break;

      default:
	break;
      }
      
    default:
      break;
    }

  case kC_HSLIDER:
    switch (GET_SUBMSG(msg)) {
    
    case kSL_RELEASE:

      switch(parm1) {
      case S_DELAY:
	_sleepSecs = fDelay->GetPosition();
	SetTime(_sleepSecs*1000+20);
	TTimer::Reset();
	break;

      default:
	break;
      }

    default:
      break;
    }

  default:
    break;
  }

  return kTRUE;
}

Bool_t EDispFrame::Notify() {

  _eventNumber++;
  fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

  if (!DoDraw())
    _eventNumber--;;

  TTimer::Reset();
  return kFALSE;
}

void EDispFrame::cleanup()
{
  // clear saved events
  release();

  // clear points
  if (_pm3d)
    delete _pm3d;;

  for (int i=0; i<_nviews; i++) {
    if (view[i])
      delete view[i];
    if (pad[i])
      delete pad[i];
  }

  if (_dhceb)
    delete _dhceb;

  delete _arena;
}

void EDispFrame::setup()
{
  int iret;
  double rmin[3] = {-_bbox, -_bbox, 0};
  double rmax[3] = {_bbox, _bbox, 2*_bbox};

  // distort bbox based on geometry
  if (_rpcGeom == "CubicTC" || _rpcGeom == "CubicWTC") {
    rmax[2]*=2.4;
  }
  else if (_rpcGeom == "Cuboid") {
    rmax[2]*=1.5;
  }
  double elong = (rmax[2]-rmin[2]) / (rmax[1]-rmin[1]);
  double vSplit = elong/(elong+1);

  TGeoManager::SetVerboseLevel(0);
  _dhcgeom->transform(new TGeoCombiTrans(_zpivot*sin(_wangle/57.2958), 0, 0,
					 new TGeoRotation("y-axis",-90,_wangle,90)));
  TGeoVolume* dhcLayout(_dhcgeom->layout(_bbox));
  const std::vector<DhcView*> views(_dhcgeom->views());
  _nviews = views.size();

  for (int i=0; i<_nviews; i++) {

    fCanvas->GetCanvas()->cd();
    switch (i) {
    case 0:
      pad[i] = (_nviews == 5) ?
	new TPad("Top", "Top", 0, 0, 0.33, 0.33) :
	new TPad("Top", "Top", 0, 0, 1-vSplit, vSplit);
      break;

    case 1:
      pad[i] = (_nviews == 5) ?
	new TPad("Front", "Front", 0, 0.33, 0.33, 0.67) :
	new TPad("Front", "Front", 0, vSplit, 1-vSplit, 1);
      break;

    case 2:
      pad[i] = (_nviews == 5) ?
	new TPad("Side", "Side", 0, 0.67, 0.33, 1) :
	new TPad("Side", "Side", 1-vSplit, vSplit, 1, 1);
      break;

    case 3:
      pad[i] = (_nviews == 5) ?
	new TPad("Pers1", "Pers1", 0.33, 0, 1, 0.5) :
	new TPad("Pers1", "Pers1", 1-vSplit, 0, 1, vSplit);
      break;

    case 4:
      pad[i] = new TPad("Pers2", "Pers2", 0.33, 0.5, 1, 1);
      break;

    default:
      break;
    }

    pad[i]->Draw();
    pad[i]->cd();

    dhcLayout->Draw();

    view[i] = new TView3D(1,rmin,rmax);

    DhcView* v(views[i]);
    view[i]->SetView(v->phi, v->theta, v->psi, iret);
    view[i]->ZoomView(pad[i],v->zoom);

    pad[i]->SetTheta(90 - v->theta);
    pad[i]->SetPhi(-90 - v->phi);

    switch (i) {
    case 0: // top
    case 1: // front
    case 2: // side
      view[i]->SetParallel();
      break;

    case 3:
    case 4:
      view[i]->SetPerspective();
      view[i]->SetDview(2*view[i]->GetDview());
      view[i]->SetDproj(2*view[i]->GetDproj());

      if (elong > 2) {
	view[i]->MoveWindow('l');
	view[i]->MoveWindow('i');
      }
      break;

    default:
      break;
    }
  }

  // marker colors
  _pm3c[0] = kRed;
  _pm3c[1] = kBlue;
  _pm3c[2] = kBlack;
  _pm3c[3] = kMagenta;

  ttmFlag = 0;
}

void EDispFrame::release(unsigned nKeep)
{
  // relese some memory
  std::map<unsigned, EDispEvent*>::iterator eit;
  std::map<unsigned, EDispEvent*>::iterator eib(_events.begin());
  std::map<unsigned, EDispEvent*>::iterator eie(_events.find(_eventIndex - nKeep));
  
  for  (eit=eib; eit!=eie; eit++)
    delete eit->second;

  _events.erase(eib, eie);
}

bool EDispFrame::acquire()
{
 if (_events.size() > 2*_eventsKept)
   release(_eventsKept);

  // if no more sets of pts to plot, read more events
  if (pts.empty()) {
    // read more records until some events are built
    while(_reader.read(*_arena)) {
      if (_arena->recordType() == RcdHeader::event) {
	_eventNumberInRun++;
	// savelast record time
	_recordTime = _arena->recordTime();
      }
      _dhceb->record(*_arena);
      if (!_dhceb->built_hitdata().empty()) {
	// get copy of events
	pts = _dhceb->built_points();
	ipts = pts.begin();
	break;
      }
    }
  }

  // if still no points to draw, must be at end of data
  if (pts.empty()) {
    //CloseWindow();
    return false;
  }

  // cut on minimum no. of layers
  while ( ipts!=pts.end() &&
	  (_dhceb->layers(*ipts) < _minLayers ||
	   _dhceb->maxPtsPerLayer(*ipts) < _minPoints ||
	   _dhceb->maxPtsPerLayer(*ipts) > _maxPoints ||
    	   (UtlTime(true)-_recordTime).seconds() > 60*_timeSince ))
    ipts++;

  if(ipts!=pts.end()) {
    // next vector  of points
    std::vector<DhcPadPts*>* _pts = *ipts++;
    std::vector<DhcPadPts*>::const_iterator epts(_pts->begin());

    _eventIndex++;
    unsigned eventNumber(_intTrigger ? _eventIndex :_eventNumberInRun);
    _events[_eventIndex] = new EDispEvent(eventNumber, _recordTime,
					  new std::vector<DhcPadPts>, _dhceb->ttm_flag());
    for (; epts!=_pts->end(); epts++)
      _events[_eventIndex]->pts->push_back(*(*epts));
  }

  // check for end of events
  if(ipts==pts.end()) {
    // clear events
    _dhceb->clear();
    pts.clear();
  }
  return true;
}

bool EDispFrame::DoDraw()
{
  while (_events.find(_eventNumber) == _events.end())
    if (!acquire())
      return false;;

  std::ostringstream sout;
  sout << "Run " << _runNumber << ", "
       << "Event " << _events[_eventNumber]->eventNumber << ", "
       << "Time " << _events[_eventNumber]->recordTime;

  fStatus->SetText(sout.str().c_str(),0);

  fCanvas->GetCanvas()->cd();
  
  // clear TPolyMarker3D points
  if (_pm3d)
    delete _pm3d;;
  
  if (ttmFlag)
    delete ttmFlag;

  unsigned npts(_events[_eventNumber]->pts->size());
  _pm3d = new TPolyMarker3D(npts, 8);
  _pm3d->SetMarkerSize(0.5);
  _pm3d->SetMarkerColor(kRed);
  
  std::vector<DhcPadPts>::const_iterator pt(_events[_eventNumber]->pts->begin());
  unsigned ts((*(pt+npts/2)).Ts);

  for (unsigned j=0; j<npts; j++, pt++) {
    if (abs((*pt).Ts - ts) < _maxTDelta) {
      double w = (*pt).Pz < 120 ? (_wangle/57.2958) : 0;
      double x = (*pt).Px*cos(w) + (_zpivot - (*pt).Pz)*sin(w);
      double y = (*pt).Py;
      double z = (*pt).Px*sin(w) + (*pt).Pz*cos(w);
      _pm3d->SetPoint(j, x, y, z);
    }
  }

  // draw points in all views
  for (int i=0; i<_nviews; i++) {
    pad[i]->cd();
    _pm3d->Draw();
    if (i == _nviews-1) {
      std::ostringstream sout;
      switch (_events[_eventNumber]->ttmFlag) {
      case 1:
	sout << "A ";
	break;
      case 2:
	sout << "B ";
	break;
      case 3:
	sout << "A&B ";
	break;
      default:
	break;
      }
      ttmFlag = new TText(0.9, 0.9, sout.str().c_str());
      ttmFlag->SetTextAlign(33);
      ttmFlag->SetTextSize(0.05);
      ttmFlag->Draw();
    }
    pad[i]->Draw();
  }

  fCanvas->GetCanvas()->Update();
  return true;
}

void EDispFrame::DoPrint()
{
  std::ostringstream sout;
  sout << "root/"
       << "Run_" << _runNumber
       << "_Event_" << _events[_eventNumber]->eventNumber
       << ".png";

  fCanvas->GetCanvas()->Print(sout.str().c_str(), "png");
}

void EDispFrame::runNumber(const unsigned& r) {
  _runNumber = r;
}
void EDispFrame::sleepSecs(const unsigned& s) {
  _sleepSecs = s;
}
void EDispFrame::minLayers(const unsigned& l) {
  _minLayers = l;
}
void EDispFrame::maxPoints(const unsigned& p) {
  _maxPoints = p;
}
void EDispFrame::minPoints(const unsigned& p) {
  _minPoints = p;
}
void EDispFrame::maxTDelta(const unsigned& t) {
  _maxTDelta = t;
}
void EDispFrame::timeSince(const unsigned& t) {
  _timeSince = t;
}
void EDispFrame::rpcGeom(const std::string& g) {
  _rpcGeom = g;
}
void EDispFrame::runPath(const std::string& f) {
  _runPath = f;
}
void EDispFrame::bBox(const double& b) {
  _bbox = b;
}
void EDispFrame::wAngle(const double& a) {
  _wangle = a;
  _zpivot = 20;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const int xpos(argh.optionArgument('x',0,"X-pos"));
  const int ypos(argh.optionArgument('y',0,"Y-pos"));
  const unsigned width(argh.optionArgument('w',680,"Width"));
  const unsigned height(argh.optionArgument('v',680,"Height"));
  const unsigned skipMins(argh.optionArgument('m',0xffffffff,"Display only last m minutes"));
  const unsigned sleepSecs(argh.optionArgument('s',2,"Hold display for s seconds"));
  const unsigned minLayers(argh.optionArgument('l',2,"Min hit layers for displayed events"));
  const unsigned maxPoints(argh.optionArgument('p',300,"Max points/layer for displayed events"));
  const unsigned minPoints(argh.optionArgument('q',0,"Min points/layer for displayed events"));
  const unsigned maxTDelta(argh.optionArgument('t',2,"Max timestamp diff for displayed events"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const std::string rpcGeom(argh.optionArgument('g',"CubicWTC","Geometry (Slice|Cubic|CubicTC|Cuboid)"));
  const int angle(argh.optionArgument('a',0,"Rotation angle (degrees) of DHCal"));

  if(argh.help()) return 0;

  gROOT->SetStyle();
  gErrorIgnoreLevel=kWarning;
  gStyle->SetCanvasPreferGL(false);

  TApplication _application("Event Display Application",0,0);

  EDispFrame fEd(gClient->GetRoot(), xpos, ypos, width, height);

  // set parameters
  fEd.runNumber(runNumber);
  fEd.runPath(runPath);
  fEd.sleepSecs(sleepSecs);
  fEd.minLayers(minLayers);
  fEd.maxPoints(maxPoints);
  fEd.minPoints(minPoints);
  fEd.maxTDelta(maxTDelta);
  fEd.timeSince(skipMins);
  fEd.rpcGeom(rpcGeom);
  fEd.bBox(55);
  fEd.wAngle((double)angle);
  fEd.initialize();

  _application.Run();

}
