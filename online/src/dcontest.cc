#include <sstream>
#include <vector>
#include <map>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcCableMap.hh"


class ChkDhcConfig : public RcdUserRO {

public:
  ChkDhcConfig(const unsigned& runNumber,
	       const bool& verbose);
  virtual ~ChkDhcConfig() {}

  bool record(const RcdRecord &r);

private:
  std::map<unsigned, std::string> _sernoMap;
  std::map<unsigned, unsigned> _enableMap;
  std::map<unsigned, bool> _lockMap;
  std::map<unsigned, const DhcDcConfigurationData*> _dcData;

  std::vector<const DhcReadoutConfigurationData*>* _roConfig;

  DhcCableMap* _cmap;
  bool _verbose;
  bool _status;
};

ChkDhcConfig::ChkDhcConfig(const unsigned& runNumber,
			   const bool& verbose)
  : RcdUserRO(9),_verbose(verbose),_status(true) {

  _roConfig = new std::vector<const DhcReadoutConfigurationData*>;

  // connect to SQL db.
  _cmap = new DhcCableMap(runNumber);
}

bool ChkDhcConfig::record(const RcdRecord &r) {

  SubAccessor accessor(r);

  switch(r.recordType()) {

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);

    std::cout << " Dhc DC configuration number " << v[0]->configurationNumberInRun()
	      << " in run." << std::endl << std::endl;

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    assert(be.size()>0);

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
    for (bit=be.begin(); bit!=be.end(); bit++) {
      if (!(*bit)->write()) {

	UtlPack loc(0);
	loc.byte(2, ((*bit)->crateNumber()));
	loc.byte(1, ((*bit)->slotNumber()));

	UtlPack dconEnabled(0);
	dconEnabled.word(((*bit)->data()->dconEnable()));

	for (unsigned i(0); i<12; i++) {
	  loc.byte(0, i);
	  _lockMap[loc.word()] = (dconEnabled.bit(i) == dconEnabled.bit(i+16));
	}
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    assert(dc.size()>0);

    const DhcDcConfigurationData* dcData;
    std::vector<const DhcLocationData<DhcDcConfigurationData>*>::const_iterator it;
    for (it=dc.begin(); it!=dc.end(); it++) {

      UtlPack loc(0);
      loc.byte(2, (*it)->location().crateNumber());
      loc.byte(1, (*it)->location().slotNumber());
      loc.byte(0, (*it)->dhcComponent());

      if ((*it)->location().write())
	_dcData[loc.word()] = (*it)->data();

      else {

	// test for DCOL port not locked
	if (!_lockMap[loc.word()])
	  continue;

	if (_dcData.find(loc.word()) != _dcData.end())
	  dcData = _dcData[loc.word()];
	else
	  dcData = _dcData.rbegin()->second;

	const unsigned char* enable((*it)->data()->enable());
	UtlPack ena(0);
	ena.byte(2, enable[2]);
	ena.byte(1, enable[1]);
	ena.byte(0, enable[0]);

	_enableMap[loc.word()] = ena.word();

	// compare written/read configuration data
	const unsigned char* _enable = dcData->enable();

	bool cfgd(true);

	if (*dcData->control() != *(*it)->data()->control()) cfgd=false;
	if (*dcData->triggerWidth() != *(*it)->data()->triggerWidth()) cfgd=false;
	if (*dcData->triggerDelay() != *(*it)->data()->triggerDelay()) cfgd=false;
	for (unsigned i(0); i<3; i++) 
	  if (_enable[i] != ((*it)->data()->enable())[i]) cfgd=false;

	if (!cfgd) {
	  std::cout << " Side "
		    << _cmap->label((*it)->location().crateNumber(),
				    (*it)->location().slotNumber(),
				    (*it)->location().dhcComponent())
		    << " (" << static_cast<unsigned int>((*it)->location().crateNumber())
		    << ", " << static_cast<unsigned int>((*it)->location().slotNumber())
		    << ", " << static_cast<unsigned int>((*it)->location().dhcComponent())
		    << ") Data Concentrator Chip did not configure."
		    << std::endl;

	  _status=false;
	}
      }
    }

    std::vector<const DhcReadoutConfigurationData*>
      ro(accessor.access<DhcReadoutConfigurationData>());
    assert(ro.size()>0);

    std::vector<const DhcReadoutConfigurationData*>::const_iterator rt;
    for (rt=ro.begin(); rt!=ro.end(); rt++)
      _roConfig->push_back(new DhcReadoutConfigurationData(*(*rt)));

    break;
  }

  case RcdHeader::runEnd: {

    std::cout << std::endl;

    std::vector<const DhcLocationData<DhcDcRunData>*>
      dc(accessor.access< DhcLocationData<DhcDcRunData> >());

    assert(dc.size()>0);

    std::vector<const DhcLocationData<DhcDcRunData>*>::const_iterator dt;
    for (dt=dc.begin(); dt!=dc.end(); dt++) {

      UtlPack loc(0);
      loc.byte(2, (*dt)->location().crateNumber());
      loc.byte(1, (*dt)->location().slotNumber());
      loc.byte(0, (*dt)->location().dhcComponent());

      // test for DCOL port not locked
      if (!_lockMap[loc.word()])
	continue;

      if ((*(*dt)->data()->pllCount()>0) || (*(*dt)->data()->syncCount()>0)) {

	std::cout << " Side "
		  << _cmap->label((*dt)->location().crateNumber(),
				  (*dt)->location().slotNumber(),
				  (*dt)->location().dhcComponent())
		  << " (" << static_cast<unsigned int>((*dt)->location().crateNumber())
		  << ", " << static_cast<unsigned int>((*dt)->location().slotNumber())
		  << ", " << static_cast<unsigned int>((*dt)->location().dhcComponent())
		  << ") Data Concentrator"
		  << "  PLL Count " << static_cast<unsigned int>(*(*dt)->data()->pllCount())
		  << ", SYNC Count " << static_cast<unsigned int>(*(*dt)->data()->syncCount())
		  << std::endl;

	_status=false;
      }

      const unsigned char* _serial = (*dt)->data()->serial();

      std::ostringstream sout;
      for(unsigned i(0); i<2; i++)
	sout << std::setfill('0') << std::setw(2)
	     << std::hex << static_cast<unsigned int>(_serial[i]) << std::dec
	     << std::setfill(' ');

      _sernoMap[loc.word()] = sout.str();
    }

    std::cout << std::endl;

    std::vector<const DhcReadoutConfigurationData*>::const_iterator rt;
    for (rt=_roConfig->begin(); rt!=_roConfig->end(); rt++) {
      unsigned c(static_cast<unsigned int>((*rt)->crateNumber()));
      std::cout << "Crate "
		<< c
		<< std::endl;

      // loop over DCOLS
      for (unsigned s(4); s<20; s++) {

	if ((*rt)->slotEnable(s)) {
	  // loop over DCONS
	  for(unsigned f(0);f<12;f++) {

	    if((*rt)->slotFeEnable(s,f)) {

	      UtlPack loc(0);
	      loc.byte(2, (*rt)->crateNumber());
	      loc.byte(1, s);
	      loc.byte(0, f);

	      if (_lockMap.find(loc.word()) == _lockMap.end())
		continue;

	      if (_enableMap.find(loc.word()) == _enableMap.end()) {
		_status = false;
		std::cout << " Side " << _cmap->label(c, s, f)
			  << " Slot " << s << ", Data Concentrator " << f 
			  << " enabled, but not configured" << std::endl;
	      }
	      if (_sernoMap.find(loc.word()) == _sernoMap.end()) {
		_status = false;
		std::cout << " Side " << _cmap->label(c, s, f)
			  << " Slot " << s << ", Data Concentrator " << f 
			  << " enaabled, but not readable" << std::endl;
	      }
	    }
	    else
	      std::cout << " Slot " << s << ", DCON " << f << " disabled" << std::endl;
	  }
	}
	else {
	  std::cout << " Slot " << s << " disabled" << std::endl;
	}
      }
    }
    if (_status)
      std::cout << " All enabled Data Concentrators OK" << std::endl;

    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const bool verbose(argh.option('v',"Verbose print"));

  if(argh.help()) return 0;

  ChkDhcConfig ddc(runNumber, verbose);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    ddc.record(*arena);

  reader.close();
  delete arena;
}
