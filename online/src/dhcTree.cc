#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"
#include "DhcEvent.hh"

#include <TSystem.h>
#include "TTree.h"
#include "TFile.h"

#include <vector>
#include <iostream>
using namespace std;

class DhcTreeWriter {

private:

	TFile* _tfile;
	TTree* _tree;

	RcdArena* _arena;
	RunReader _reader;

	DhcEB* _dhceb;

	unsigned _runNumber;
	std::string _runPath;
	std::string _rootPath;
	unsigned _minLayers;
	bool _intTrigger;
	int _minTimeBin;
	int _maxTimeBin;
	int _maxEvents;

public:
	DhcTreeWriter();
	virtual ~DhcTreeWriter();

	bool initialize();
	void record();

	void runNumber(const unsigned& r);
	void runPath(const std::string& f);
	void rootPath(const std::string& w);
	void minLayers(const unsigned& l);
	void minTimeBin(const int& bin);
	void maxTimeBin(const int& bin);
	void maxEvents(const int& nEvents);

};

DhcTreeWriter::DhcTreeWriter() :
		_dhceb(0) {
}

DhcTreeWriter::~DhcTreeWriter() {
}

bool DhcTreeWriter::initialize() {
	_arena = new RcdArena;
	_reader.directory(_runPath);
	assert(_reader.open(_runNumber));

	// determine run type
	while (!_dhceb) {

		_reader.read(*_arena);
		switch (_arena->recordType()) {

		case RcdHeader::runStart: {
			// Access the DaqRunStart
			SubAccessor accessor(*_arena);
			std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
			assert(v.size()==1);

			DaqRunType runType = v[0]->runType();

			switch (runType.type()) {

			case DaqRunType::dhcNoise: {
				_dhceb = new DhcEBIntTrg(_runNumber);
				_intTrigger = true;
				break;
			}
			case DaqRunType::beamData:
			case DaqRunType::dhcBeam:
			case DaqRunType::dhcCosmics: {
				_dhceb = new DhcEBExtTrg(_runNumber);
				_intTrigger = false;
				break;
			}

			default: {
				std::cerr << "Run type " << runType.typeName()
						<< " not supported" << std::endl;
				return false;
				break;
			}
			}
		}
		default:
			break;
		}
	}

	std::ostringstream sout;
	sout << _rootPath << "/dhcTree-Run" << _runNumber << ".root";

	_tfile = new TFile(sout.str().c_str(), "recreate");
	_tree = new TTree("DhcTree", "DhcTree");

	return true;
}

void DhcTreeWriter::record() {
	DhcEvent* event = new DhcEvent();
	_tree->Branch("EventBranch", "DhcEvent", &event);

	std::vector<std::vector<DhcPadPts*>*> pts;
	std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;

	Int_t eventNumber(0);
	Int_t eventNumberInRun(-1);
	UtlTime recordTime;

	while (1) {
		if (pts.empty()) {
			// read more records until some events are built
			const vector<DhcFeHitData*>* hitdata;
			while (_reader.read(*_arena)) {
				if (_arena->totalNumberOfBytes() > sizeof(*_arena) / 2)
					continue;
				_dhceb->record(*_arena);
				if (_arena->recordType() == RcdHeader::event) {
					eventNumberInRun++;
					// savelast record time
					recordTime = _arena->recordTime();
					event->setTriggerTimeStamp(_dhceb->triggerTS());
				}
				if (!_dhceb->raw_hitdata().empty()) {
					// get copy of events
					pts = _dhceb->built_points();
					hitdata = &_dhceb->raw_hitdata();
					break;
				}
			}
			// if still no events, at end of data
			if (pts.empty())
				break;
			vector<DhcFeHitData*>::const_iterator hitdata_it = hitdata->begin();

			TTimeStamp eventTime(recordTime.timeStamp(),
					1000 * recordTime.microseconds());
			for (ipts = pts.begin(); ipts != pts.end(); ipts++) {
				unsigned nHitLayers(_dhceb->layers(*ipts));
				if (_minLayers <= nHitLayers) {
					if (_intTrigger)
						event->setHeader(eventNumber++, eventTime);
					else
						event->setHeader(eventNumberInRun, eventTime);

					//event->SetNhitLayers(nHitLayers);
					//event->SetMaxHitsPerLayer(_dhceb->maxPtsPerLayer(*ipts));

					const unsigned ttmflag(_dhceb->ttm_flag());
					event->setTriggerBits((const Int_t*) &ttmflag);

					std::vector<DhcPadPts*>::const_iterator ib(
							(*ipts)->begin());
					std::vector<DhcPadPts*>::const_iterator ie((*ipts)->end());

					for (; ib != ie; ib++) {
					    int timeBin = (*ib)->Ts - event->getTriggerTimeStamp();
					    if (timeBin >= _minTimeBin && timeBin <= _maxTimeBin) {
						    event->addHit((10. * (*ib)->Px), (10. * (*ib)->Py), (10. * (*ib)->Pz), (*ib)->Layer, (*ib)->Ts);
						}
					}
					event->findClusters();
					_tree->Fill();
					event->Clear();
				}				
			}

			if (ipts == pts.end()) {
				// clear events
				_dhceb->clear();
				pts.clear();
			}
		}
	    if (eventNumberInRun % 100 == 0) {
	        cout << "Processed " << eventNumberInRun << " events" << endl;
	    }
	    if (eventNumberInRun == _maxEvents) {
	        break;
	    }
	}
	_reader.close();
	_tfile->Write();
	delete _dhceb;
	delete _arena;
}

void DhcTreeWriter::runNumber(const unsigned& r) {
	_runNumber = r;
}
void DhcTreeWriter::runPath(const std::string& f) {
	_runPath = f;
}
void DhcTreeWriter::rootPath(const std::string& w) {
	_rootPath = w;
}
void DhcTreeWriter::minLayers(const unsigned& l) {
	_minLayers = l;
}
void DhcTreeWriter::minTimeBin(const int& bin) {
    _minTimeBin = bin;
}
void DhcTreeWriter::maxTimeBin(const int& bin) {
    _maxTimeBin = bin;
}
void DhcTreeWriter::maxEvents(const int& nEvents) {
    _maxEvents = nEvents;
}

int main(int argc, const char **argv) {
	UtlArguments argh(argc, argv);

	const unsigned runNumber(argh.optionArgument('r', 999999, "Run number"));
	const std::string runPath(
			argh.optionArgument('f', "data/run",
					"Path where run data is located"));
	const std::string rootPath(
			argh.optionArgument('w', "root",
					"Path where root file is written"));
	const unsigned minLayers(
			argh.optionArgument('l', 4, "Min hit layers for recorded events"));
	const int minTimeBin(argh.optionArgument('m', -19, "Min time bin for hits"));
	const int maxTimeBin(argh.optionArgument('M', -17, "Min time bin for hits"));
	const int maxEvents(argh.optionArgument('n', -1, "Max events processed"));

	if (argh.help())
		return 0;

	DhcTreeWriter* tw = new DhcTreeWriter;
	// set parameters
	tw->runNumber(runNumber);
	tw->runPath(runPath);
	tw->rootPath(rootPath);
	tw->minLayers(minLayers);
	tw->minTimeBin(minTimeBin);
	tw->maxTimeBin(maxTimeBin);
	tw->maxEvents(maxEvents);

	if (tw->initialize()) {
		tw->record();
	}
}
