#include <iostream>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TGraphAsymmErrors.h"
#include "TH1F.h"
#include "TF1.h"


class DhcTimeLine : public RcdUserRO {

public:
  DhcTimeLine();
  virtual ~DhcTimeLine() {}

  bool record(const RcdRecord &r);

  const std::vector<double>& event_time() const;
  const std::vector<double>& max_record() const;

private:
  std::vector<double> _eventTime;
  std::vector<double> _maxRecord;

  UtlTime _startTime;
  unsigned _configurationNumber;
};

DhcTimeLine::DhcTimeLine()
  : RcdUserRO(9)
{}

const std::vector<double>& DhcTimeLine::event_time() const
{
  return _eventTime;
}

const std::vector<double>& DhcTimeLine::max_record() const
{
  return _maxRecord;
}

bool DhcTimeLine::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::runStart: {

    _startTime = r.recordTime();
    break;
  }

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);
    _configurationNumber = v[0]->configurationNumberInRun();

    break;
  }

  case RcdHeader::trigger: {

    UtlTimeDifference eventTime = r.recordTime() - _startTime;
    _eventTime.push_back(eventTime.deltaTime());

    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    if (v.size()>0) {

      unsigned maxRecord(0);
      for (unsigned s(0); s<v.size(); s++) {
	maxRecord = std::max(maxRecord, v[s]->data()->numberOfWords()*4);
      }
      _maxRecord.push_back(maxRecord);
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  DhcTimeLine dtl;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    dtl.record(*arena);

  reader.close();
  delete arena;

  std::vector<double> times = dtl.event_time();
  std::vector<double> sizes = dtl.max_record();

  TApplication application("Trigger Time Check Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eo");

  TDatime da(1995,01,01,00,00,00);
  gStyle->SetTimeOffset(da.Convert());

  TCanvas canvas("DhcReadout Trigger Time Check", "Trigger Timing Check", 600, 800);
  canvas.Divide(1,2);

  TGraphAsymmErrors timeLine(times.size(), &times.front(), &sizes.front());
  TH1F timeDelta("timeDelta","timeDelta", 110, 0, 0.11);

  for (unsigned i(1); i<timeLine.GetN(); i++) { 
    timeLine.SetPointEYhigh(i, 0);
    timeLine.SetPointEYlow(i, sizes[i]);

    timeDelta.Fill(times[i] - times[i-1]);
  }

  canvas.cd(1);
  timeLine.SetTitle("Max Record size Time Line");
  timeLine.GetHistogram()->GetXaxis()->SetTimeDisplay(1);

  timeLine.Draw("APZ");

  canvas.cd(2)->SetLogy(1);
  TF1* tfit = new TF1("tdk", "expo(0)+expo(2)", 0, 0.1);
  tfit->SetRange(0.001, 0.05);
  timeDelta.Fit(tfit, "QR");

  double tmax = std::min(0.1 - 0.001,
			 (log(0.1) -
			  std::min(tfit->GetParameter(0),tfit->GetParameter(2)))
			 /std::max(tfit->GetParameter(1),tfit->GetParameter(3)));
  tfit->SetRange(0.001, tmax);
  timeDelta.Fit(tfit, "QR");

  int nRate1 = int(-tfit->GetParameter(1));
  int nRate3 = int(-tfit->GetParameter(3));
  std::ostringstream sout;
  sout << "Time Between Triggers, Fitted Rates "
       << std::min(nRate1,nRate3) << " ( + " << std::max(nRate1,nRate3) << " ) Hz";

  timeDelta.SetNameTitle("Time Between Triggers", sout.str().c_str());
  canvas.cd(2)->SetLogy(1);
  timeDelta.Draw();
  canvas.Update();

  application.Run();
}
