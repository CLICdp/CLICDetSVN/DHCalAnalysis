#include <iostream>

#include <signal.h>
#include <unistd.h>

#include "UtlArguments.hh"

#include "BmlCaenV2718Device.hh"

bool continueLoop;

void signalHandler(int signal) {
  continueLoop=false;
}


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned spillDuration(argh.optionArgument('d',4,"Time duration of spill (sec)"));
  const unsigned spillInterval(argh.optionArgument('i',60,"Time interval between spills (sec)"));

  if(argh.help()) return 0;

  // Catch signals
  signal(SIGTERM,signalHandler);
  signal(SIGINT ,signalHandler);

  BmlCaenV2718Device* device(new BmlCaenV2718Device(0, 0));

  if (!device->alive())
    {
      std::cerr << " Error opening the device:" << std::endl;
      exit(1);
    }

  continueLoop=true;;
  while (continueLoop)
    {
      if (spillDuration > 0) {
	device->setOutput((unsigned short)cvOut0Bit);
	sleep(spillDuration);
      }
      if (spillInterval > 0) {
	device->clearOutput((unsigned short)cvOut0Bit);
	sleep(spillInterval);
      }
    }

  delete device;
}
