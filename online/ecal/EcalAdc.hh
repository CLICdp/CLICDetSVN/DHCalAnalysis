//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalAdc_HH
#define EcalAdc_HH

#include <cstdlib>

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "EcalPhysicalPad.hh"

//////////////////////////////////////////////////////////////////////////////

/// Class to hold the adc content of ECAL.
/** Navigation is through EcalPhysicalPad.
*/
class EcalAdc 
{

   public:

//............................................................................  
   /// Constructor.
   /**
   */
   EcalAdc() 
   {
   }
//............................................................................  
   /// Destructor.
   /**
   */
   virtual ~EcalAdc() 
   {
   }
//............................................................................
   /// Reset the adc content.
   /** It sets adc value of all channels to a DEFAULT value.
   */
   void reset() 
   {
      for(EcalPhysicalPad pp(0); pp.isValid(); pp++)
      {
 	 _adc[pp.value()]=DEFAULT;
      }
   }
//............................................................................
   /// Return adc value of given channel.
   /**
   */
   short int adc(EcalPhysicalPad p) const 
   {
      assert(p.isValid());
      return _adc[p.value()];
   }
//............................................................................  
   /// Set adc value of given channel.
   /**
   */
   void adc(EcalPhysicalPad p, short int a) 
   {
      assert(p.isValid());
      _adc[p.value()]=a;
   }
//............................................................................  
   /// Print out adc content of ECAL.
   /** Only channels with non-default value are shown.
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalAdc::print()" << std::endl;

      for(EcalPhysicalPad pp(0); pp.isValid(); pp++)
      {
	 if(_adc[pp.value()]!=DEFAULT) 
	 {
	    out << " X,Y,Z " << std::setw(3) << pp.horizontal() 
		                << std::setw(3) << pp.vertical()
		                << std::setw(3) << pp.layer()
	                        << std::setw(8) << _adc[pp.value()] << std::endl;
	 }
      }

      return out;
   }
//............................................................................

   private:
   
   /// Array to hold the adc content of ECAL.
   /** Navigation is through EcalPhysicalPad.
   */
   short int _adc[18*18*30];
   
   /// Default initial adc value of each channel.
   /**
   */ 
   const static short int DEFAULT = -32768;

};//end class EcalAdc

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
