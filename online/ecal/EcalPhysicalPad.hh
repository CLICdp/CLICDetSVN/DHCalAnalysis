//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalPhysicalPad_HH
#define EcalPhysicalPad_HH

#include <iostream>
#include <fstream>
#include <string>

//////////////////////////////////////////////////////////////////////////////

/// Class to hold physical position channel id.
/** ECAL has in total 30 layers of 18x18 pads each.
    Each channel is identified by 3 integer indices for
    horizontal, vertical, layer position respectively.    
*/
class EcalPhysicalPad 
{

   public:
   
//............................................................................
   /// Constructor.
   /**
   */
   EcalPhysicalPad() : _value(0xffff) 
   {
   }
//............................................................................
   /// Constructor, see operator++.
   /**
   */
   EcalPhysicalPad(unsigned short v) : _value(v) 
   {  
      assert(isValid());
   }
//............................................................................
   /// Constructor with attributes.
   /**
   */
   EcalPhysicalPad(unsigned horizontal, unsigned vertical, unsigned layer) 
   : _value(18*(18*layer+vertical)+horizontal) 
   {
      assert(horizontal<18 && vertical<18 && layer<30);
   }
//............................................................................
   /// Destructor.
   /**
   */
   ~EcalPhysicalPad()
   {
   }   
//............................................................................
   /// Return packed id.
   /**
   */
   unsigned short value() const 
   {
      return _value;
   }
//............................................................................
/*
   void value(unsigned short v) 
   {
      _value=v;
      assert(this->isValid());
   }
*/
//............................................................................
   /// Return horizontal index.
   /**
   */
   unsigned horizontal() const 
   {
      return _value%18;
   }
//............................................................................
   /// Return vertical index.
   /**
   */
   unsigned vertical() const 
   {
      return (_value/18)%18;
   }
//............................................................................
   /// Return layer index.
   /**
   */
   unsigned layer() const 
   {
      return _value/(18*18);
   }
//............................................................................
   /// Print out channel id.
   /**
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalPhysicalPad::print()" << std::endl;
      out << " Value        = " << _value << std::endl;
      out << " Horizontal(x)= " << std::setw(2) << horizontal() << std::endl;
      out << " Vertical(y)  = " << std::setw(2) << vertical() << std::endl;
      out << " Layer(z)     = " << std::setw(2) << layer() << std::endl;
      return out;
   }
//............................................................................
   /// Check if channel has id within valid range.
   /**
   */
   bool isValid()
   {
      bool flag = false;
      //if(_value >= _begin && _value < _end) flag = true;
      if(_value < _end) flag = true;
      return flag; 
   }
//............................................................................
   /// Convenient increment operator. 
   /** To be used along with isValid() in for-loops to access all channels.
       I.e. "for(EcalPhysicalPad pp(0);pp.isValid();pp++)". 
   */
   void operator ++ (int)
   {
      _value++;
   } 
//............................................................................

   private:
	
   //static const unsigned short _begin=0;
   
   /// Total ECAL channels.
   /**
   */   
   static const unsigned short _end=18*18*30;
   
   /// Packed channel id.
   /**
   */
   unsigned short _value;

};//end class EcalPhysicalPad

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
