#ifndef BmlLocationData_HH
#define BmlLocationData_HH

#include "BmlLocation.hh"

template <class Data> class BmlLocationData : public BmlLocation {

public:
  BmlLocationData();
  BmlLocationData(BmlLocation l);
  BmlLocationData(BmlLocation l, const Data &d);
  
  BmlLocation location() const;
  void location(BmlLocation l);

  const Data* data() const;
  Data*       data();
  void        data(Data &p);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  Data _data;
};


template <class Data> 
BmlLocationData<Data>::BmlLocationData() : BmlLocation(), _data() {
}
  
template <class Data> 
BmlLocationData<Data>::BmlLocationData(BmlLocation l) :
  BmlLocation(l), _data() {
}
  
template <class Data> 
BmlLocationData<Data>::BmlLocationData(BmlLocation l, const Data &d) :
  BmlLocation(l), _data(d) {
}
  
template <class Data> 
BmlLocation BmlLocationData<Data>::location() const {
  return *((BmlLocation*)this);
}

template <class Data> 
void BmlLocationData<Data>::location(BmlLocation l) {
  *((BmlLocation*)this)=l;
}

template <class Data> 
const Data* BmlLocationData<Data>::data() const {
  return &_data;
}

template <class Data> 
Data* BmlLocationData<Data>::data() {
  return &_data;
}

template <class Data> 
void BmlLocationData<Data>::data(Data &p) {
  _data=p;
}

template <class Data> 
std::ostream& BmlLocationData<Data>::print(std::ostream &o, std::string s) const {
  o << s << "BmlLocationData::print()" << std::endl;
  BmlLocation::print(o,s+" ");
  _data.print(o,s+" ");
  return o;
}

#ifdef CALICE_DAQ_ICC
#endif
#endif
