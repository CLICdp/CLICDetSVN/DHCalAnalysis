#ifndef BmlLc1176ConfigurationData_HH
#define BmlLc1176ConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlLc1176ConfigurationData {

public:
  enum {
    versionNumber=0
  };

  BmlLc1176ConfigurationData();

  unsigned label() const;
  void     label(unsigned l);

  unsigned csr() const;
  void     csr(unsigned c);

  bool executeTestCycle() const;
  void executeTestCycle(bool b);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _csr;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLc1176ConfigurationData::BmlLc1176ConfigurationData() {
  memset(this,0,sizeof(BmlLc1176ConfigurationData));
}

unsigned BmlLc1176ConfigurationData::label() const {
  if(_csr.bit(31)) return 1;
  else             return 0;
}

void BmlLc1176ConfigurationData::label(unsigned l) {
  if(l==0) _csr.bit(31,false);
  else     _csr.bit(31,true);
}

bool BmlLc1176ConfigurationData::executeTestCycle() const {
  return _csr.bit(0);
}

void BmlLc1176ConfigurationData::executeTestCycle(bool b) {
  _csr.bit(0,b);
}

unsigned BmlLc1176ConfigurationData::csr() const {
  return _csr.bits(0,30);
}

void BmlLc1176ConfigurationData::csr(unsigned c) {
  _csr.bits(0,30,c&0x7fffffff);
}

std::ostream& BmlLc1176ConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "BmlLc1176ConfigurationData::print()" << std::endl;
  o << s << " Label = " << label();
  if(label()==0) o << " = read";
  if(label()==1) o << " = write";
  o << std::endl;
  o << s << " CSR = " << printHex(csr()) << std::endl;
  return o;
}

#endif
#endif
