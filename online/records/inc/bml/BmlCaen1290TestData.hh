#ifndef BmlCaen1290TestData_HH
#define BmlCaen1290TestData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"
#include "BmlCaen1290EventDatum.hh"


class BmlCaen1290TestData {

public:
  enum {
    versionNumber=0
  };

  enum Pattern {
    count,
    empty,
    event
  };

  BmlCaen1290TestData();

  unsigned numberOfRepeats() const;
  void     numberOfRepeats(unsigned n);

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);

  void setPattern(Pattern p=count, unsigned n=0);
  
  const BmlCaen1290EventDatum* data() const;
        BmlCaen1290EventDatum* data();
 
  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _numberOfRepeats;
  unsigned _numberOfWords;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290TestData::BmlCaen1290TestData() {
  memset(this,0,sizeof(BmlCaen1290TestData));
  _numberOfRepeats=1;
}

unsigned BmlCaen1290TestData::numberOfRepeats() const {
  return _numberOfRepeats;
}

void BmlCaen1290TestData::numberOfRepeats(unsigned n) {
  _numberOfRepeats=n;
}

unsigned BmlCaen1290TestData::numberOfWords() const {
  return _numberOfWords;
}

void BmlCaen1290TestData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

void BmlCaen1290TestData::setPattern(Pattern p, unsigned n) {
  BmlCaen1290EventDatum *a(data());
  //UtlPack *b((UtlPack*)a);

  if(p==count) {
    a[0].setHeader(0x15,0);
    for(unsigned i(1);i<1024-1;i++) a[i].setDatum(0,false,true,i);
    a[1024-1].setEob(0x15,7,1024-1);
    _numberOfWords=1024;
    _numberOfRepeats=32;
  }

  if(p==empty) {
    a[0].setHeader(0x15,0);
    a[1].setEob(0x15,7,0);
    _numberOfWords=2;
    _numberOfRepeats=16*1024;
  }

  if(p==event) {
    a[0].setHeader(0x05,0);

    unsigned channel(0),time(0);
    bool lead;
    for(unsigned i(1);i<=n;i++) {
      if((i%2)==1) {
	channel=rand()%8;
	time=(rand()%32)+1000;
	lead=true;
      } else {
	time+=50;
	lead=false;
      }
      a[i].setDatum(channel,false,lead,time);
    }

    a[n+1].setEob(0x05,7,n);

    _numberOfWords=n+2;
    _numberOfRepeats=32*1024/(n+2);
  }
}

const BmlCaen1290EventDatum* BmlCaen1290TestData::data() const {
  return (const BmlCaen1290EventDatum*)(this+1);
}

BmlCaen1290EventDatum* BmlCaen1290TestData::data() {
  return (BmlCaen1290EventDatum*)(this+1);
}

std::ostream& BmlCaen1290TestData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290TestData::print()" << std::endl;

  o << s << " Number of repeats = " << _numberOfRepeats << std::endl;

  o << s << " Number of words = " << _numberOfWords << std::endl;
  const BmlCaen1290EventDatum *p(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    if(p[i].label()==BmlCaen1290EventDatum::tdcHeader && i!=0) o << std::endl;
    o << s << " Datum " << std::setw(4) << i;
    p[i].print(o,s+"  ");
  }

  return o;
}

#endif
#endif
