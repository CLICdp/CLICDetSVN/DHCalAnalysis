#ifndef BmlCaen1290TriggerData_HH
#define BmlCaen1290TriggerData_HH

#include <iostream>
#include <fstream>

#include "BmlCaen1290StatusRegister.hh"


class BmlCaen1290TriggerData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen1290TriggerData();

  BmlCaen1290StatusRegister statusRegister() const;
  void statusRegister(unsigned h, unsigned short r);

  unsigned eventCounter() const;
  void     eventCounter(unsigned n);
 
  unsigned eventStored() const;
  void     eventStored(unsigned n);
 
  unsigned eventFifoStored() const;
  void     eventFifoStored(unsigned n);
 
  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  BmlCaen1290StatusRegister _statusRegister;
  unsigned _eventCounter;
  UtlPack _stores;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290TriggerData::BmlCaen1290TriggerData() {
  memset(this,0,sizeof(BmlCaen1290TriggerData));
}

BmlCaen1290StatusRegister BmlCaen1290TriggerData::statusRegister() const {
  return _statusRegister;
}

void BmlCaen1290TriggerData::statusRegister(unsigned h, unsigned short r) {
  _statusRegister.statusRegister(h,r);
}

unsigned BmlCaen1290TriggerData::eventCounter() const {
  return _eventCounter;
}

void BmlCaen1290TriggerData::eventCounter(unsigned n) {
  _eventCounter=n;
}

unsigned BmlCaen1290TriggerData::eventStored() const {
  return _stores.halfWord(0);
}

void BmlCaen1290TriggerData::eventStored(unsigned n) {
  _stores.halfWord(0,n);
}

unsigned BmlCaen1290TriggerData::eventFifoStored() const {
  return _stores.halfWord(1);
}

void BmlCaen1290TriggerData::eventFifoStored(unsigned n) {
  _stores.halfWord(1,n);
}

std::ostream& BmlCaen1290TriggerData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290TriggerData::print()" << std::endl;

  _statusRegister.print(o,s+" ");

  o << s << " Event counter = " << _eventCounter << std::endl;
  o << s << " Event stored = " << eventStored() << std::endl;
  o << s << " Event FIFO stored = " << eventFifoStored() << std::endl;

  return o;
}

#endif
#endif
