#ifndef BmlCaen1290ConfigurationData_HH
#define BmlCaen1290ConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen1290ConfigurationData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen1290ConfigurationData();

  UtlPack controlRegister() const;
  void    controlRegister(unsigned h, unsigned short r);

  bool busErrorEnable() const;
  void busErrorEnable(bool b);

  bool softwareTermination() const;
  void softwareTermination(bool b);

  bool softwareTerminationEnable() const;
  void softwareTerminationEnable(bool b);

  bool globalEventEnable() const;
  void globalEventEnable(bool b);

  bool align64Enable() const;
  void align64Enable(bool b);

  bool inlCompensationEnable() const;
  void inlCompensationEnable(bool b);

  bool testFifoEnable() const;
  void testFifoEnable(bool b);

  bool readCSramEnable() const;
  void readCSramEnable(bool b);

  bool eventFifoEnable() const;
  void eventFifoEnable(bool b);

  bool timeTagEnable() const;
  void timeTagEnable(bool b);

  bool memoryTest() const;
  void memoryTest(bool b);

  UtlPack interruptRegister() const;
  void    interruptLevel(unsigned short r);
  void    interruptVector(unsigned short r);

  UtlPack countRegister() const;

  unsigned short almostFullLevel() const;
  void           almostFullLevel(unsigned short n);

  unsigned char bltEventNumber() const;
  void          bltEventNumber(unsigned char n);

  /** CRP 31/10/10 added for completeness */ 
  UtlPack spare() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _controlRegister;
  UtlPack _interruptRegister;
  UtlPack _countRegister;
  UtlPack _spare;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290ConfigurationData::BmlCaen1290ConfigurationData() {
  memset(this,0,sizeof(BmlCaen1290ConfigurationData));

  softwareTermination(true);
  globalEventEnable(true);

  almostFullLevel(30000);
  bltEventNumber(1);
}

UtlPack BmlCaen1290ConfigurationData::controlRegister() const {
  return _controlRegister;
}

void BmlCaen1290ConfigurationData::controlRegister(unsigned h, unsigned short r) {
  assert(h<2);
  _controlRegister.halfWord(h,r);
}

bool BmlCaen1290ConfigurationData::busErrorEnable() const {
  return _controlRegister.bit(0);
}

void BmlCaen1290ConfigurationData::busErrorEnable(bool b) {
  _controlRegister.bit(0,b);
}

bool BmlCaen1290ConfigurationData::softwareTermination() const {
  return _controlRegister.bit(1);
}

void BmlCaen1290ConfigurationData::softwareTermination(bool b) {
  _controlRegister.bit(1,b);
}

bool BmlCaen1290ConfigurationData::softwareTerminationEnable() const {
  return _controlRegister.bit(2);
}

void BmlCaen1290ConfigurationData::softwareTerminationEnable(bool b) {
  _controlRegister.bit(2,b);
}

bool BmlCaen1290ConfigurationData::globalEventEnable() const {
  return _controlRegister.bit(3);
}

void BmlCaen1290ConfigurationData::globalEventEnable(bool b) {
  _controlRegister.bit(3,b);
}

bool BmlCaen1290ConfigurationData::align64Enable() const {
  return _controlRegister.bit(4);
}

void BmlCaen1290ConfigurationData::align64Enable(bool b) {
  _controlRegister.bit(4,b);
}

bool BmlCaen1290ConfigurationData::inlCompensationEnable() const {
  return _controlRegister.bit(5);
}

void BmlCaen1290ConfigurationData::inlCompensationEnable(bool b) {
  _controlRegister.bit(5,b);
}

bool BmlCaen1290ConfigurationData::testFifoEnable() const {
  return _controlRegister.bit(6);
}

void BmlCaen1290ConfigurationData::testFifoEnable(bool b) {
  _controlRegister.bit(6,b);
}

bool BmlCaen1290ConfigurationData::readCSramEnable() const {
  return _controlRegister.bit(7);
}

void BmlCaen1290ConfigurationData::readCSramEnable(bool b) {
  _controlRegister.bit(7,b);
}

bool BmlCaen1290ConfigurationData::eventFifoEnable() const {
  return _controlRegister.bit(8);
}

void BmlCaen1290ConfigurationData::eventFifoEnable(bool b) {
  _controlRegister.bit(8,b);
}

bool BmlCaen1290ConfigurationData::timeTagEnable() const {
  return _controlRegister.bit(9);
}

void BmlCaen1290ConfigurationData::timeTagEnable(bool b) {
  _controlRegister.bit(9,b);
}

bool BmlCaen1290ConfigurationData::memoryTest() const {
  return _controlRegister.bit(16);
}

void BmlCaen1290ConfigurationData::memoryTest(bool b) {
  _controlRegister.bit(16,b);
}

UtlPack BmlCaen1290ConfigurationData::interruptRegister() const {
  return _interruptRegister;
}

void BmlCaen1290ConfigurationData::interruptLevel(unsigned short r) {
  _interruptRegister.halfWord(0,r);
}

void BmlCaen1290ConfigurationData::interruptVector(unsigned short r) {
  _interruptRegister.halfWord(1,r);
}

UtlPack BmlCaen1290ConfigurationData::countRegister() const {
  return _countRegister;
}

unsigned short BmlCaen1290ConfigurationData::almostFullLevel() const {
  return _countRegister.halfWord(0);
}

void BmlCaen1290ConfigurationData::almostFullLevel(unsigned short n) {
  _countRegister.halfWord(0,n);
}

unsigned char BmlCaen1290ConfigurationData::bltEventNumber() const {
  return _countRegister.byte(2);
}

void BmlCaen1290ConfigurationData::bltEventNumber(unsigned char n) {
  _countRegister.byte(2,n);
}

/** CRP 31/10/10 added for completeness */
UtlPack BmlCaen1290ConfigurationData::spare() const {
  return _spare;
}


std::ostream& BmlCaen1290ConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290ConfigurationData::print()" << std::endl;

  o << s << " Control register   = " << printHex(_controlRegister) << std::endl;
  if(busErrorEnable()) o << s << "  VME bus error enabled" << std::endl;
  else                        o << s << "  VME bus error disabled" << std::endl;
  if(softwareTermination()) o << s << "  Software termination on" << std::endl;
  else                        o << s << "  Software termination off" << std::endl;
  if(softwareTerminationEnable()) o << s << "  Software termination enabled" << std::endl;
  else                        o << s << "  Software termination disabled" << std::endl;
  if(globalEventEnable()) o << s << "  Global header and trailer for empty events enabled" << std::endl;
  else                        o << s << "  Global header and trailer for empty events disabled" << std::endl;
  if(align64Enable()) o << s << "  64-bit alignment enabled" << std::endl;
  else                        o << s << "  64-bit alignment disabled" << std::endl;
  if(inlCompensationEnable()) o << s << "  INL compensation enabled" << std::endl;
  else                        o << s << "  INL compensation disabled" << std::endl;
  if(testFifoEnable()) o << s << "  Test FIFO enabled" << std::endl;
  else                        o << s << "  Test FIFO disabled" << std::endl;
  if(readCSramEnable()) o << s << "  Read compensation SRAM enabled" << std::endl;
  else                        o << s << "  Read compensation SRAM disabled" << std::endl;
  if(eventFifoEnable()) o << s << "  Event FIFO enabled" << std::endl;
  else                        o << s << "  Event FIFO disabled" << std::endl;
  if(timeTagEnable()) o << s << "  Trigger time tag enabled" << std::endl;
  else                        o << s << "  Trigger time tag disabled" << std::endl;

  if(memoryTest()) o << s << "  Memory test enabled" << std::endl;
  else             o << s << "  Memory test disabled" << std::endl;

  o << s << " Interrupt register = " << printHex(_interruptRegister) << std::endl;
  o << s << "  Interrupt level     = " << _interruptRegister.bits(0,2) << std::endl;
  o << s << "  Interrupt status/id = " << (unsigned)_interruptRegister.byte(2) << std::endl;

  o << s << " Count register = " << printHex(_interruptRegister) << std::endl;
  o << s << "  Almost full level = " << almostFullLevel() << std::endl;
  o << s << "  BLT event number  = " << (unsigned)bltEventNumber() << std::endl;
  //CRP 31/10/10
  o << s << " Spare = " << printHex(_spare) << std::endl;

  return o;
}

#endif
#endif
