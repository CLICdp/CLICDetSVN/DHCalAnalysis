#ifndef BmlLocation_HH
#define BmlLocation_HH

#include <string>
#include <iostream>

#include "UtlLocation.hh"

class BmlLocation : public UtlLocation {

public:
  BmlLocation();
  BmlLocation(unsigned char c, unsigned short a, unsigned char l=0);
  
  unsigned baseAddress() const;
  void     baseAddress(unsigned short a);

  bool addressBroadcast() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"


BmlLocation::BmlLocation() : UtlLocation() {
}

BmlLocation::BmlLocation(unsigned char c, unsigned short a, unsigned char l) : 
  UtlLocation(c,0,0,l) {
  baseAddress(a);
}

unsigned BmlLocation::baseAddress() const {
  UtlPack a(0);
  a.byte(1,slotNumber());
  a.byte(0,componentNumber());
  return a.halfWord(0);
}
  
void BmlLocation::baseAddress(unsigned short a) {
  UtlPack ap(a);
  slotNumber(ap.byte(1));
  componentNumber(ap.byte(0));
}

bool BmlLocation::addressBroadcast() const {
  return baseAddress()==0xffff;
}
  
std::ostream& BmlLocation::print(std::ostream &o, std::string s) const {
  o << s << "BmlLocation::print()" << std::endl;

  o << s << " Crate number     = "
    << printHex(crateNumber(),false) << std::endl;

  o << s << " Base address   = " << std::setw(4)
    << printHex((unsigned short)baseAddress(),false);
  if(addressBroadcast()) o << ", broadcast";
  o << std::endl;

  o << s << " Label            = " << printHex(label());
  if(write()) o << " = write";
  else        o << " = read";
  if(ignore()) o << ", ignore";
  o << std::endl;

  return o;
}

#endif
#endif
