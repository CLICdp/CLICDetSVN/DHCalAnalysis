#ifndef BmlHodRunData_HH
#define BmlHodRunData_HH

#include <iostream>
#include <string>

class BmlHodRunData {

public:
  enum {
    versionNumber=0
  };

  BmlHodRunData();

  unsigned numberOfBytes() const;
  void numberOfBytes(unsigned n);

  const unsigned char* data() const;
  unsigned char* data();

  std::string label() const;
  
  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned _numberOfBytes;
  unsigned _data[3];
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"

BmlHodRunData::BmlHodRunData() {
  _numberOfBytes=0;
}

unsigned BmlHodRunData::numberOfBytes() const {
  return _numberOfBytes;
}

void BmlHodRunData::numberOfBytes(unsigned n) {
  assert(n<=12);
  _numberOfBytes=n;
}

const unsigned char* BmlHodRunData::data() const {
  return (const unsigned char*)_data;
}

unsigned char* BmlHodRunData::data() {
  return (unsigned char*)_data;
}

std::string BmlHodRunData::label() const {
  std::string s;
  const unsigned char *c(data());
  for(unsigned i(0);i<_numberOfBytes && i<12;i++) s+=c[i];
  return s;
}

std::ostream& BmlHodRunData::print(std::ostream &o, std::string s) const {
  o << s << "BmlHodRunData::print()" << std::endl;
  o << s << " Number of bytes = " << _numberOfBytes << std::endl;
  o << s << " Label = " << label() << std::endl;
  return o;
}

#endif
#endif
