#ifndef BmlCaen1290EventDatum_HH
#define BmlCaen1290EventDatum_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen1290EventDatum {

public:
  BmlCaen1290EventDatum();

  enum Label {
    tdcDatum,
    datum=tdcDatum,
    tdcHeader,
    tdcTrailer=3,
    tdcError,
    globalHeader=8,
    globalTrailer=16,
    globalTriggerTimeTag,
    filler=24
  };

  Label label() const;
  
  unsigned char geoAddress() const;

  unsigned short eventNumber() const;

  unsigned bunchId() const;

  unsigned short eventId() const;

  unsigned short tdc() const;

  unsigned time() const;

  unsigned char channelNumber() const;

  bool  risingEdge() const;
  bool leadingEdge() const;
  bool fallingEdge() const;

  unsigned short tdcWordCount() const;

  unsigned triggerTimeTag() const;

  unsigned short globalWordCount() const;

  unsigned short tdcErrorFlags() const;

  bool tdcErrors() const;

  bool bufferOverflow() const;

  bool triggerLost() const;

  void setHeader(unsigned g, unsigned e);

  void setDatum(unsigned c, bool s, bool l, unsigned t);

  void setEob(unsigned g, unsigned s, unsigned e);

  void setInvalid();

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _datum;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290EventDatum::BmlCaen1290EventDatum() {
  memset(this,0,sizeof(BmlCaen1290EventDatum));
}


BmlCaen1290EventDatum::Label BmlCaen1290EventDatum::label() const {
  return (Label)_datum.bits(27,31);
}

unsigned char BmlCaen1290EventDatum::geoAddress() const {
  if(label()!=globalHeader && label()!=globalTrailer) return 0;
  return _datum.bits(0,4);
}

unsigned short BmlCaen1290EventDatum::eventNumber() const {
  if(label()!=globalHeader) return 0;
  return _datum.bits(5,26);
}

unsigned BmlCaen1290EventDatum::bunchId() const {
  if(label()!=tdcHeader) return 0;
  return _datum.bits(0,11);
}

unsigned short BmlCaen1290EventDatum::eventId() const {
  if(label()!=tdcHeader && label()!=tdcTrailer) return 0;
  return _datum.bits(12,23);
}

unsigned short BmlCaen1290EventDatum::tdc() const {
  if(label()!=tdcHeader && label()!=tdcTrailer && label()!=tdcError) return 4;
  return _datum.bits(24,25);
}

unsigned BmlCaen1290EventDatum::time() const {
  if(label()!=tdcDatum) return 0;
  return _datum.bits(0,20);
}

unsigned char BmlCaen1290EventDatum::channelNumber() const {
  if(label()!=tdcDatum) return 0;
  return _datum.bits(21,25);
}

bool BmlCaen1290EventDatum::risingEdge() const {
  if(label()!=tdcDatum) return false;
  return !_datum.bit(26);
}

bool BmlCaen1290EventDatum::leadingEdge() const {
  return risingEdge();
}

bool BmlCaen1290EventDatum::fallingEdge() const {
  if(label()!=tdcDatum) return false;
  return _datum.bit(26);
}

unsigned short BmlCaen1290EventDatum::tdcWordCount() const {
  if(label()!=tdcTrailer) return 0;
  return _datum.bits(0,11);
}

unsigned BmlCaen1290EventDatum::triggerTimeTag() const {
  if(label()!=globalTriggerTimeTag) return 0;
  return _datum.bits(0,26);
}

unsigned short BmlCaen1290EventDatum::globalWordCount() const {
  if(label()!=globalTrailer) return 0;
  return _datum.bits(5,20);
}

unsigned short BmlCaen1290EventDatum::tdcErrorFlags() const {
  if(label()!=tdcError) return 0;
  return _datum.bits(0,14);
}

bool BmlCaen1290EventDatum::tdcErrors() const {
  if(label()!=globalTrailer) return false;
  return _datum.bit(24);
}

bool BmlCaen1290EventDatum::bufferOverflow() const {
  if(label()!=globalTrailer) return false;
  return _datum.bit(25);
}

bool BmlCaen1290EventDatum::triggerLost() const {
  if(label()!=globalTrailer) return false;
  return _datum.bit(26);
}

void BmlCaen1290EventDatum::setHeader(unsigned g, unsigned e) {
  _datum.word(0);
  _datum.bits(0,11,e);
  _datum.bits(27,31,globalHeader);
  _datum.bits(27,31,g);
}

void BmlCaen1290EventDatum::setDatum(unsigned c, bool s, bool l, unsigned t) {
  _datum.word(0);
  _datum.bits(0,19,t);
  _datum.bit(20,l);
  _datum.bits(27,31,tdcDatum);  
  _datum.bit(23,s);
  _datum.bits(24,30,c);
}

void BmlCaen1290EventDatum::setEob(unsigned g, unsigned s, unsigned e) {
  _datum.word(0);
  _datum.bits(0,15,e);
  _datum.bits(27,31,globalTrailer);
  _datum.bits(24,26,s);
  _datum.bits(27,31,g);
}

void BmlCaen1290EventDatum::setInvalid() {
  _datum.word(0);
  _datum.bits(27,31,filler);
}

std::ostream& BmlCaen1290EventDatum::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290EventDatum::print()" << std::endl;
  o << s << " Data word = " << printHex(_datum) << std::endl;

  bool known(false);  

  // Global header 
  if(label()==globalHeader) {
    known=true;
    o << s << "  Global header:  Geo address = " << (unsigned)geoAddress()
      << ", event number = " << eventNumber() << std::endl;
  }
  
  // Global trailer 
  if(label()==globalTrailer) {
    known=true;
    o << s << "  Global trailer: Geo address = " << (unsigned)geoAddress()
      << ", word count = " << globalWordCount();
    if(tdcErrors()) o << ", TDC errors";
    else           o << ", no TDC errors";
    if(bufferOverflow()) o << ", buffer overflow";
    else           o << ", no buffer overflow";
    if(triggerLost()) o << ", trigger lost";
    else           o << ", no trigger lost";
    o << std::endl;
  }
  
  // TDC header 
  if(label()==tdcHeader) {
    known=true;
    o << s << "  TDC header:  TDC = " << tdc()
      << ", bunch id   = " << std::setw(6) << bunchId()
      << ", event id = " << std::setw(6) << eventId() << std::endl;
  }
  
  // TDC datum
  if(label()==tdcDatum) {
    known=true;
    o << s << "  TDC datum:   Channel number = " << std::setw(2)
      << (unsigned)channelNumber() << ", time = "
      << std::setw(6) << time();
    if(risingEdge()) o << ",  rising";
    else             o << ", falling";
    o << std::endl;
  }

  // TDC header 
  if(label()==tdcTrailer) {
    known=true;
    o << s << "  TDC trailer: TDC = " << tdc()
	<< ", word count = "  << std::setw(6) << tdcWordCount()
	<< ", event id = "  << std::setw(6) << eventId() << std::endl;
  }
  
  // TDC error
  if(label()==tdcError) {
    known=true;
    o << s << "  TDC error:  Error flags = " << printHex(tdcErrorFlags())
      << ", TDC = " << tdc() << std::endl;
  }
  
  // Filler
  if(label()==filler) {
    known=true;
    o << s << "  Filler:  No data" << std::endl;
  }

  // Label not valid
  if(!known) {
    o << s << "  Unknown data" << std::endl;
  }

  return o;
}

#endif
#endif
