#ifndef BmlCaen1290StatusRegister_HH
#define BmlCaen1290StatusRegister_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen1290StatusRegister {

public:
  BmlCaen1290StatusRegister();

  UtlPack statusRegister() const;
  void    statusRegister(unsigned h, unsigned short r);

  bool       dataReady() const;

  bool bufferAlmostFull() const;
  bool bufferFull() const;


  bool triggerMatching() const;

  bool headerAndTrailer() const;
  bool termination() const;

  bool tdcError(unsigned t) const;
  bool anyTdcError() const;

  bool busError() const;

  bool purged() const;

  unsigned lsbResolution() const;

  bool pairMode() const;

  bool triggerLost() const;

  bool eventFifoDataReady() const;

  bool eventFifoFull() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _statusRegister;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290StatusRegister::BmlCaen1290StatusRegister() {
  memset(this,0,sizeof(BmlCaen1290StatusRegister));
}

UtlPack BmlCaen1290StatusRegister::statusRegister() const {
  return _statusRegister;
}

void BmlCaen1290StatusRegister::statusRegister(unsigned h, unsigned short r) {
  assert(h<2);
  _statusRegister.halfWord(h,r);
}

bool BmlCaen1290StatusRegister::dataReady() const {
  return _statusRegister.bit(0);
}

bool BmlCaen1290StatusRegister::bufferAlmostFull() const {
  return _statusRegister.bit(1);
}

bool BmlCaen1290StatusRegister::bufferFull() const {
  return _statusRegister.bit(2);
}

bool BmlCaen1290StatusRegister::triggerMatching() const {
  return _statusRegister.bit(3);
}

bool BmlCaen1290StatusRegister::headerAndTrailer() const {
  return _statusRegister.bit(4);
}

bool BmlCaen1290StatusRegister::termination() const {
  return _statusRegister.bit(5);
}

bool BmlCaen1290StatusRegister::tdcError(unsigned t) const {
  assert(t<4);
  return _statusRegister.bit(6+t);
}

bool BmlCaen1290StatusRegister::anyTdcError() const {
  return _statusRegister.bits(6,9)!=0;
}

bool BmlCaen1290StatusRegister::busError() const {
  return _statusRegister.bit(10);
}

bool BmlCaen1290StatusRegister::purged() const {
  return _statusRegister.bit(11);
}

unsigned BmlCaen1290StatusRegister::lsbResolution() const {
  return _statusRegister.bits(12,13);
}

bool BmlCaen1290StatusRegister::pairMode() const {
  return _statusRegister.bit(14);
}

bool BmlCaen1290StatusRegister::triggerLost() const {
  return _statusRegister.bit(15);
}

bool BmlCaen1290StatusRegister::eventFifoDataReady() const {
  return _statusRegister.bit(16);
}

bool BmlCaen1290StatusRegister::eventFifoFull() const {
  return _statusRegister.bit(17);
}

std::ostream& BmlCaen1290StatusRegister::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290StatusRegister::print()" << std::endl;

  o << s << " Status register = " << printHex(_statusRegister) << std::endl;

  if(dataReady())       o << s << "  Module has data ready" << std::endl;
  else                  o << s << "  Module has no data ready" << std::endl;
  if(bufferAlmostFull())      o << s << "  Output buffer almost full" << std::endl;
  else                  o << s << "  Output buffer not almost full" << std::endl;
  if(bufferFull())      o << s << "  Output buffer full" << std::endl;
  else                  o << s << "  Output buffer not full" << std::endl;
  if(triggerMatching())       o << s << "  Trigger matching mode" << std::endl;
  else             o << s << "  Continuous storage mode" << std::endl;
  if(headerAndTrailer()) o << s << "  Header and trailer enabled" << std::endl;
  else             o << s << "  Header and trailer disabled" << std::endl;
  if(termination())      o << s << "  Control bus termination on" << std::endl;
  else  o << s << "  Control bus termination off" << std::endl;

  for(unsigned i(0);i<4;i++) {
    if(tdcError(i)) o << s << "  TDC " << i << " has an error" << std::endl;
    else            o << s << "  TDC " << i << " has no error" << std::endl;
  }

  if(busError())      o << s << "  A bus error has occurred" << std::endl;
  else  o << s << "  No bus error has occurred" << std::endl;
  if(purged())      o << s << "  Module is purged" << std::endl;
  else  o << s << "  Module is not purged" << std::endl;

  o << s << "  LSB resolution = " << lsbResolution() << std::endl;

  if(pairMode())      o << s << "  Pair mode enabled" << std::endl;
  else  o << s << "  Pair mode disabled" << std::endl;
  if(triggerLost())      o << s << "  Triggers have been lost" << std::endl;
  else  o << s << "  Triggers have not been lost" << std::endl;

  if(eventFifoDataReady())      o << s << "  Event FIFO has data ready" << std::endl;
  else  o << s << "  Event FIFO has no data ready" << std::endl;
  if(eventFifoFull())      o << s << "  Event FIFO is full" << std::endl;
  else  o << s << "  Event FIFO is not full" << std::endl;

  return o;
}

#endif
#endif
