#ifndef BmlCaen767ReadoutConfigurationData_HH
#define BmlCaen767ReadoutConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class BmlCaen767ReadoutConfigurationData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen767ReadoutConfigurationData();

  unsigned char crateNumber() const;
  void          crateNumber(unsigned char c);

  bool enable() const;
  void enable(bool b);

  unsigned readPeriod() const;
  void     readPeriod(unsigned n);

  bool softTrigger() const;
  void softTrigger(bool e);

  bool bltReadout() const;
  void bltReadout(bool b);

  bool arrayReadout() const;
  void arrayReadout(bool b);

  UtlPack mode() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _mode;
  unsigned _readPeriod;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

BmlCaen767ReadoutConfigurationData::BmlCaen767ReadoutConfigurationData() {
  memset(this,0,sizeof(BmlCaen767ReadoutConfigurationData));
  enable(true);
  bltReadout(true);
  _readPeriod=1;
}

unsigned char BmlCaen767ReadoutConfigurationData::crateNumber() const {
  return _mode.byte(0);
}

void BmlCaen767ReadoutConfigurationData::crateNumber(unsigned char c) {
  _mode.byte(0,c);
}

unsigned BmlCaen767ReadoutConfigurationData::readPeriod() const {
  return _readPeriod;
}

void BmlCaen767ReadoutConfigurationData::readPeriod(unsigned n) {
  _readPeriod=n;
}

bool BmlCaen767ReadoutConfigurationData::enable() const {
  return _mode.bit(8);
}

void BmlCaen767ReadoutConfigurationData::enable(bool e) {
  _mode.bit(8,e);
}

bool BmlCaen767ReadoutConfigurationData::softTrigger() const {
  return _mode.bit(9);
}

void BmlCaen767ReadoutConfigurationData::softTrigger(bool e) {
  _mode.bit(9,e);
}

bool BmlCaen767ReadoutConfigurationData::bltReadout() const {
  return _mode.bit(10);
}

void BmlCaen767ReadoutConfigurationData::bltReadout(bool b) {
  _mode.bit(10,b);
}

bool BmlCaen767ReadoutConfigurationData::arrayReadout() const {
  return _mode.bit(11);
}

void BmlCaen767ReadoutConfigurationData::arrayReadout(bool b) {
  _mode.bit(11,b);
}


/**CRP added for convenience
  *function was declared but not defined
  */
UtlPack BmlCaen767ReadoutConfigurationData::mode() const {
  return _mode;
}


std::ostream& BmlCaen767ReadoutConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen767ReadoutConfigurationData::print()" << std::endl;

  o << s << " Crate number  = " << printHex(crateNumber()) << std::endl;
  o << s << " Mode          = " << printHex(_mode.byte(1)) << std::endl;
  if(enable())       o << s << "  Readout enabled" << std::endl;
  else               o << s << "  Readout disabled" << std::endl;
  if(softTrigger())  o << s << "  Soft trigger enabled" << std::endl;
  else               o << s << "  Soft trigger disabled" << std::endl;
  if(bltReadout())   o << s << "  Readout using block transfer" << std::endl;
  else               o << s << "  Readout using standard transfer" << std::endl;
  if(arrayReadout()) o << s << "  Readout using buffer array" << std::endl;
  else               o << s << "  Readout using buffer FIFO" << std::endl;

  o << s << " Readout period = " << _readPeriod << std::endl;
  
  return o;
}

#endif
#endif
