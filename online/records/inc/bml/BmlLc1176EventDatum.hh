#ifndef BmlLc1176EventDatum_HH
#define BmlLc1176EventDatum_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class BmlLc1176EventDatum {

public:
  BmlLc1176EventDatum();

  bool lastWord() const;
  bool invalidData() const;
  unsigned channelNumber() const;
  bool leadingEdge() const;
  unsigned short time() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _data;
};

#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLc1176EventDatum::BmlLc1176EventDatum() {
  memset(this,0,sizeof(BmlLc1176EventDatum));
}

bool BmlLc1176EventDatum::lastWord() const {
  return !_data.bit(23);
}

bool BmlLc1176EventDatum::invalidData() const {
  return _data.bit(21);
}

unsigned BmlLc1176EventDatum::channelNumber() const {
  return _data.bits(17,20);
}

bool BmlLc1176EventDatum::leadingEdge() const {
  return _data.bit(16);
}

unsigned short BmlLc1176EventDatum::time() const {
  return _data.halfWord(0);
}

std::ostream& BmlLc1176EventDatum::print(std::ostream &o, std::string s) const {
  o << s << "BmlLc1176EventDatum::print()" << std::endl;

  o << s << " Datum = " << printHex(_data) << std::endl;
  o << s << "  Channel " << std::setw(2) << channelNumber();
  if(leadingEdge()) o << ", leading";
  else              o << ", falling";
  o << ", time = " << std::setw(5) << time();
  if(lastWord()) o << ", last word";
  if(invalidData()) o << ", INVALID!!!";
  o << std::endl;
  
  return o;
}

#endif
#endif
