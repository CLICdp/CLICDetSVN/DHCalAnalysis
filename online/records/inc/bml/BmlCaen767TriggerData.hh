#ifndef BmlCaen767TriggerData_HH
#define BmlCaen767TriggerData_HH

#include <iostream>
#include <fstream>

#include "BmlCaen767StatusRegister.hh"


class BmlCaen767TriggerData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen767TriggerData();

  BmlCaen767StatusRegister statusRegister() const;
  void statusRegister(unsigned h, unsigned short r);

  unsigned eventCounter() const;
  void     eventCounter(unsigned n);
 
  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  BmlCaen767StatusRegister _statusRegister;
  unsigned _eventCounter;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen767TriggerData::BmlCaen767TriggerData() {
  memset(this,0,sizeof(BmlCaen767TriggerData));
}

BmlCaen767StatusRegister BmlCaen767TriggerData::statusRegister() const {
  return _statusRegister;
}

void BmlCaen767TriggerData::statusRegister(unsigned h, unsigned short r) {
  _statusRegister.statusRegister(h,r);
}

unsigned BmlCaen767TriggerData::eventCounter() const {
  return _eventCounter;
}

void BmlCaen767TriggerData::eventCounter(unsigned n) {
  _eventCounter=n;
}

std::ostream& BmlCaen767TriggerData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen767TriggerData::print()" << std::endl;

  _statusRegister.print(o,s+" ");

  o << s << " Event counter = " << _eventCounter << std::endl;

  return o;
}

#endif
#endif
