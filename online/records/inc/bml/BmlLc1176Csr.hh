#ifndef BmlLc1176Csr_HH
#define BmlLc1176Csr_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class BmlLc1176Csr {

public:
  BmlLc1176Csr();
  BmlLc1176Csr(UtlPack c);
  BmlLc1176Csr(unsigned c);

  UtlPack data() const;
  void    data(UtlPack d);

  bool bufferFull() const;
  unsigned numberOfEvents() const;

  bool clear() const;
  void clear(bool c);

  bool reserved31() const;
  void reserved31(bool r);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _data;
};

#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLc1176Csr::BmlLc1176Csr() {
  memset(this,0,sizeof(BmlLc1176Csr));
}

BmlLc1176Csr::BmlLc1176Csr(UtlPack c) : _data(c) {
}

BmlLc1176Csr::BmlLc1176Csr(unsigned c) : _data(c) {
}

UtlPack BmlLc1176Csr::data() const {
  return _data;
}

void BmlLc1176Csr::data(UtlPack d) {
  _data=d;
}

bool BmlLc1176Csr::bufferFull() const {
  return _data.bit(29);
}

unsigned BmlLc1176Csr::numberOfEvents() const {
  if(_data.bit(29)) return 32;
  return _data.bits(24,28);
}

bool BmlLc1176Csr::clear() const {
  return _data.bit(23);
}

void BmlLc1176Csr::clear(bool c) {
  return _data.bit(23,c);
}

bool BmlLc1176Csr::reserved31() const {
  return _data.bit(31);
}

void BmlLc1176Csr::reserved31(bool r) {
  return _data.bit(31,r);
}

std::ostream& BmlLc1176Csr::print(std::ostream &o, std::string s) const {
  o << s << "BmlLc1176Csr::print()" << std::endl;
  o << s << " Data = " << printHex(_data) << std::endl;

  o << s << "  Number of events = " << numberOfEvents();
  if(bufferFull()) o << ", buffer full" << std::endl;
  else o << std::endl;
  
  return o;
}

#endif
#endif
