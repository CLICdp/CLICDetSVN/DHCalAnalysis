#ifndef BmlCaen1290RunData_HH
#define BmlCaen1290RunData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen1290RunData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen1290RunData();

  unsigned char crRom(unsigned c) const;
  void          crRom(unsigned c, unsigned char r);

  unsigned char checksum() const;
  unsigned checksumLength() const;
  unsigned constant() const;
  unsigned char cCode() const;
  unsigned char rCode() const;
  UtlPack manufacturerId() const;
  unsigned char boardVersion() const;
  UtlPack boardId() const;
  UtlPack revisionId() const;

  UtlPack serialNumber() const;
  void    serialNumber(unsigned h, unsigned char s);

  unsigned short addressDecoder() const;
  void           addressDecoder(unsigned h, unsigned char a);

  unsigned short mcstAddress() const;
  void           mcstAddress(unsigned short a);

  unsigned char vmeFirmwareRevision() const;
  void          vmeFirmwareRevision(unsigned char r);

  unsigned char outputControl() const;
  void          outputControl(unsigned char r);


  unsigned tdcId(unsigned t) const;
  void     tdcId(unsigned t, unsigned i);

  unsigned char firmwareRevision() const;
  void          firmwareRevision(unsigned char r);

  unsigned short microRevision() const;
  void           microRevision(unsigned short m);
  unsigned short microDay() const;
  void           microDay(unsigned short m);
  unsigned short microMonth() const;
  void           microMonth(unsigned short m);
  unsigned short microYear() const;
  void           microYear(unsigned short m);


  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _crRom[5];
  UtlPack _serialNumber;
  UtlPack _addresses;

  UtlPack _tdcId[4];
  UtlPack _firmwareRevision;
  UtlPack _microInfo[2];
  UtlPack _spare;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290RunData::BmlCaen1290RunData() {
  memset(this,0,sizeof(BmlCaen1290RunData));
}

unsigned char BmlCaen1290RunData::crRom(unsigned c) const {
  assert(c<20);
  return _crRom[c/4].byte(c%4);
}

void BmlCaen1290RunData::crRom(unsigned c, unsigned char r) {
  assert(c<20);
  _crRom[c/4].byte(c%4,r);
}

unsigned char BmlCaen1290RunData::checksum() const {
  return _crRom[0].byte(0);
}

unsigned BmlCaen1290RunData::checksumLength() const {
  UtlPack reply(0);
  reply.byte(0,_crRom[0].byte(3));
  reply.byte(1,_crRom[0].byte(2));
  reply.byte(2,_crRom[0].byte(1));
  return reply.word();
}

unsigned BmlCaen1290RunData::constant() const {
  UtlPack reply(0);
  reply.byte(0,_crRom[1].byte(2));
  reply.byte(1,_crRom[1].byte(1));
  reply.byte(2,_crRom[1].byte(0));
  return reply.word();
}

unsigned char BmlCaen1290RunData::cCode() const {
  return _crRom[1].byte(3);
}

unsigned char BmlCaen1290RunData::rCode() const {
  return _crRom[2].byte(0);
}

UtlPack BmlCaen1290RunData::manufacturerId() const {
  UtlPack reply(0);
  reply.byte(0,_crRom[2].byte(3));
  reply.byte(1,_crRom[2].byte(2));
  reply.byte(2,_crRom[2].byte(1));
  return reply;
}

unsigned char BmlCaen1290RunData::boardVersion() const {
  return _crRom[3].byte(0);
}

UtlPack BmlCaen1290RunData::boardId() const {
  UtlPack reply(0);
  reply.byte(0,_crRom[3].byte(3));
  reply.byte(1,_crRom[3].byte(2));
  reply.byte(2,_crRom[3].byte(1));
  return reply;
}

UtlPack BmlCaen1290RunData::revisionId() const {
  UtlPack reply;
  reply.byte(0,_crRom[4].byte(3));
  reply.byte(1,_crRom[4].byte(2));
  reply.byte(2,_crRom[4].byte(1));
  reply.byte(3,_crRom[4].byte(0));
  return reply;
}

UtlPack BmlCaen1290RunData::serialNumber() const {
  return _serialNumber.halfWord(0);
}

void BmlCaen1290RunData::serialNumber(unsigned h, unsigned char c) {
  assert(h<2);
  _serialNumber.byte(h,c);
}

unsigned short BmlCaen1290RunData::addressDecoder() const {
  return _addresses.halfWord(0);
}

void BmlCaen1290RunData::addressDecoder(unsigned h, unsigned char a) {
  assert(h<2);
  _addresses.byte(h,a);
}

unsigned short BmlCaen1290RunData::mcstAddress() const {
  return _addresses.halfWord(1);
}

void BmlCaen1290RunData::mcstAddress(unsigned short a) {
  _addresses.halfWord(1,a);
}

unsigned char BmlCaen1290RunData::vmeFirmwareRevision() const {
  return _serialNumber.byte(2);
}

void BmlCaen1290RunData::vmeFirmwareRevision(unsigned char r) {
  _serialNumber.byte(2,r);
}

unsigned char BmlCaen1290RunData::outputControl() const {
  return _serialNumber.byte(3);
}

void BmlCaen1290RunData::outputControl(unsigned char r) {
  _serialNumber.byte(3,r);
}

unsigned BmlCaen1290RunData::tdcId(unsigned c) const {
  assert(c<4);
  return _tdcId[c].word();
}

void BmlCaen1290RunData::tdcId(unsigned c, unsigned b) {
  assert(c<4);
  _tdcId[c].word(b);
}

unsigned char BmlCaen1290RunData::firmwareRevision() const {
  return _firmwareRevision.byte(0);
}

void BmlCaen1290RunData::firmwareRevision(unsigned char r) {
  _firmwareRevision.byte(0,r);
}

unsigned short BmlCaen1290RunData::microRevision() const {
  return _microInfo[0].halfWord(0);
}

void BmlCaen1290RunData::microRevision(unsigned short m) {
  _microInfo[0].halfWord(0,m);
}

unsigned short BmlCaen1290RunData::microDay() const {
  return _microInfo[0].halfWord(1);
}

void BmlCaen1290RunData::microDay(unsigned short m) {
  _microInfo[0].halfWord(1,m);
}

unsigned short BmlCaen1290RunData::microMonth() const {
  return _microInfo[1].halfWord(0);
}

void BmlCaen1290RunData::microMonth(unsigned short m) {
  _microInfo[1].halfWord(0,m);
}

unsigned short BmlCaen1290RunData::microYear() const {
  return _microInfo[1].halfWord(1);
}

void BmlCaen1290RunData::microYear(unsigned short m) {
  _microInfo[1].halfWord(1,m);
}

std::ostream& BmlCaen1290RunData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290RunData::print()" << std::endl;

  for(unsigned i(0);i<5;i++) {
    o << s << " CR ROM word " << std::setw(4) << i << " = " << printHex(_crRom[i]) << std::endl;
    if(i==0) {
      o << s << "  Checksum  = " << printHex(checksum()) << std::endl;
      o << s << "  Checksum length = " << checksumLength() << std::endl;
    }
    if(i==1) {
      o << s << "  Constant = " << constant() << std::endl;
      o << s << "  C-code  = " << printHex(cCode()) << std::endl;
    }
    if(i==2) {
      o << s << "  R-code  = " << printHex(rCode()) << std::endl;
      o << s << "  Manufacturer Id = " << printHex(manufacturerId()) << std::endl;
    }
    if(i==3) {
      o << s << "  Board version  = " << printHex(boardVersion()) << " = ";
      if(boardVersion()==0x00) o << "A";
      else if(boardVersion()==0x02) o << "N";
      else o << "unknown";
      o << std::endl;
      o << s << "  Board Id        = " << printHex(boardId()) << std::endl;
    }
    if(i==4) {
      o << s << "  Revision Id     = " << printHex(revisionId()) << std::endl;
    }
  }

  o << s << " Serial Number          = " << printHex(serialNumber()) << std::endl;
  o << s << " VME firmware revision  = " << (unsigned)vmeFirmwareRevision() << std::endl;
  o << s << " Output programmable control = " << (unsigned)outputControl() << std::endl;

  o << s << " Addresses = " << printHex(_addresses) << std::endl;
  o << s << "  Address decoder = " << printHex(addressDecoder()) << std::endl;
  o << s << "  MCST address    = " << printHex(mcstAddress()) << std::endl;

  for(unsigned i(0);i<4;i++) {
    o << s << " TDC " << std::setw(2) << i
      << " Id = " << printHex(tdcId(i)) << std::endl;
  }

  o << s << " Firmware revision = " << (unsigned)firmwareRevision() << std::endl;
  o << s << " Micro revision = " << microRevision() << ", date " 
    << std::setw(2) << std::setfill('0') << microDay() << "/" 
    << std::setw(2) << std::setfill('0') << microMonth() << "/200" << microYear()
    << std::setfill(' ') << std::endl;

  return o;
}

#endif
#endif
