#ifndef BmlCernSlowRunData_HH
#define BmlCernSlowRunData_HH

#include <string>
#include <iostream>

//The sizes of the arrays which contain the beam data information
#define NSEXTCUR 4
#define NBENDCUR 18
#define NCOLPOS 44
#define NH6AEXPCOUNT 4
#define NH6BEXPCOUNT 4
#define NH6CEXPCOUNT 4
#define NQUADCUR 32
#define NRPEXPCOUNT 8
#define NSCINTCOUNT 9
#define NTRIMCUR 12

#include "UtlPack.hh"
#include "UtlMapDefinitions.hh"

#pragma pack (4)

class BmlCernSlowRunData {

public:
  enum {
    versionNumber=0
  };

  BmlCernSlowRunData();

  bool parse(std::string r);
  bool parse(std::string r, unsigned p);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  //CRP access functions defined by RP
  double getAbsorberPosition() const;
  double getT4Position() const;
  double getTargetPosition() const;

  /**Access functions to retrieve the maps containing the different values */
  DaqTypesDblMap_t getDblMap() { 
    //Initialize the map between types and vectors containing the values - doubles
    //std::cout << "In get double maps: " << &_theDblTypes << std::endl;
    DaqTypesDblMap_t _theDblTypes;
    _theDblTypes.clear();
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("SEXTAPOLECURRENTS", _sextapoleCurrents())).second) std::cout << "Problems with inserting SEXTAPOLECURRENTS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("BENDCURRENTS", _bendCurrents())).second) std::cout << "Problems with inserting BENDCURRENTS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("COLLIMATORPOSITIONS", _collimatorPositions())).second) std::cout << "Problems with inserting COLLIMATORPOSITIONS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("QUADRUPOLECURRENTS", _quadrupoleCurrents())).second) std::cout << "Problems with inserting QUADRUPOLECURRENTS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("TRIMCURRENTS", _trimCurrents())).second) std::cout << "Problems with inserting TRIMCURRENTS into map" << std::endl;  
    return _theDblTypes;
  }; 


 DaqTypesUIntMap_t getUIntMap() { 
    DaqTypesUIntMap_t _theUIntTypes;
    _theUIntTypes.clear();
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("H6AEXPERIMENTCOUNT", _h6aExperimentCounts())).second) std::cout << "Problems with inserting H6AEXPERIMENTCOUNT into map" << std::endl;  
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("H6BEXPERIMENTCOUNT", _h6bExperimentCounts())).second) std::cout << "Problems with inserting H6BEXPERIMENTCOUNT into map" << std::endl;  
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("H6CEXPERIMENTCOUNT", _h6cExperimentCounts())).second) std::cout << "Problems with inserting H6CEXPERIMENTCOUNT into map" << std::endl;  
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("RPEXPERIMENTCOUNT", _rpExperimentCounts())).second) std::cout << "Problems with inserting RPEXPERIMENTCOUNT into map" << std::endl;  
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("SCINTILLATORCOUNT", _scintillatorCounts())).second) std::cout << "Problems with inserting SCINTILLATORCOUNTS into map" << std::endl;  
    return _theUIntTypes;
  };


DaqTypesFloatMap_t getFloatMap() { 
    DaqTypesFloatMap_t _theFloatTypes;
    _theFloatTypes.clear();
    return _theFloatTypes;
  };

 //Implemented here for convenience and to be in synch with similar classes for e.g. FNAL
  std::map<std::string,time_t> getTimeStamps() {
    std::map<std::string,time_t> _theTimeStamps;  
    _theTimeStamps.clear(); 
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("CERNTIMESTAMP", timeStamp())).second) std::cout << "Problems with inserting CERNTIMESTAMP into map" << std::endl;  
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("DAQTIMESTAMP", 0)).second) std::cout << "Problems with inserting DAQTIMESTAMP into map" << std::endl;    return _theTimeStamps;
   }



  std::ostream& print(std::ostream &o, std::string s="") const;

std::vector<double> _sextapoleCurrents() const;
  std::vector<double> _absorberPositions() const;
  std::vector<double> _bendCurrents() const;
  std::vector<double> _collimatorPositions() const;
  std::vector<double> _quadrupoleCurrents() const;
  std::vector<double> _trimCurrents() const;

  std::vector<unsigned int> _h6aExperimentCounts() const; 
  std::vector<unsigned int> _h6bExperimentCounts() const; 
  std::vector<unsigned int> _h6cExperimentCounts() const; 
  std::vector<unsigned int> _rpExperimentCounts() const;
  std::vector<unsigned int> _scintillatorCounts() const;




private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;

  double _sextapoleCurrent[2][2];
  double _absorberPosition;
  double _bendCurrent[9][2];
  double _collimatorPosition[11][2][2];
  unsigned _h6aExperimentCount[4];
  unsigned _h6bExperimentCount[4];
  unsigned _h6cExperimentCount[4];
  double _quadrupoleCurrent[16][2];
  unsigned _rpExperimentCount[8];
  unsigned _scintillatorCount[9];
  double _t4Position;
  double _targetPosition;
  double _trimCurrent[6][2];
};

#pragma pack ()

#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


BmlCernSlowRunData::BmlCernSlowRunData() {
  memset(this,0,sizeof(BmlCernSlowRunData));
}

bool BmlCernSlowRunData::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp; // SPS database time
  if(!sin) return false;

  unsigned otherTimeStamp; // Slow controls PC time; not saved
  sin >> otherTimeStamp;
  if(!sin) return false;

  for(unsigned i(0);i<2 && sin;i++) {
    for(unsigned j(0);j<2 && sin;j++) {
      sin >> _sextapoleCurrent[i][j];
      if(!sin) return false;
    }
  }

  sin >> _absorberPosition;
  if(!sin) return false;

  for(unsigned i(0);i<9 && sin;i++) {
    if(i!=7) { // BEND08 missing?
      for(unsigned j(0);j<2 && sin;j++) {
	sin >> _bendCurrent[i][j];
	if(!sin) return false;
      }
    }
  }

  for(unsigned i(0);i<11 && sin;i++) {
    if(i!=3 && i!=6) { // COLL04 and COLL07 missing?
      for(unsigned j(0);j<2 && sin;j++) {
	for(unsigned k(0);k<2 && sin;k++) {
	  sin >> _collimatorPosition[i][j][k];
	  if(!sin) return false;
	}
      }
    }
  }
  
  for(unsigned i(0);i<4 && sin;i++) {
    sin >> _h6aExperimentCount[i];
    if(!sin) return false;
  }
  
  for(unsigned i(0);i<4 && sin;i++) {
    sin >> _h6bExperimentCount[i];
    if(!sin) return false;
  }
  
  for(unsigned i(0);i<4 && sin;i++) {
    sin >> _h6cExperimentCount[i];
    if(!sin) return false;
  }
  
  for(unsigned i(0);i<16 && sin;i++) {
    for(unsigned j(0);j<2 && sin;j++) {
      sin >> _quadrupoleCurrent[i][j];
      if(!sin) return false;
    }
  }

  for(unsigned i(0);i<8 && sin;i++) {
    sin >> _rpExperimentCount[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<9 && sin;i++) {
    if(i!=1 && i!=2) { // SCINT02 and SCINT03 missing?
      sin >> _scintillatorCount[i];
      if(!sin) return false;
    }
  }

  sin >> _t4Position;
  if(!sin) return false;

  sin >> _targetPosition;
  if(!sin) return false;
  
  for(unsigned i(0);i<6 && sin;i++) {
    for(unsigned j(0);j<2 && sin;j++) {
      sin >> _trimCurrent[i][j];
      if(!sin) return false;
    }
  }
  
  return sin;
}

bool BmlCernSlowRunData::parse(std::string r, unsigned p) {
  assert(p<=10);

  std::istringstream sin(r);

  // Choose to stroe timestamp here only  
  if(p==0) {
    sin >> _timeStamp; // SPS database timestamp
    if(!sin) return false;

    unsigned ts; // Slow control PC timestamp
    sin >> ts;
    if(!sin) return false;

    for(unsigned i(0);i<2 && sin;i++) {
      for(unsigned j(0);j<2 && sin;j++) {
	sin >> _sextapoleCurrent[i][j];
	if(!sin) return false;
      }
    }
    return sin;
  }

  if(p==1) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;

    sin >> _absorberPosition;
    if(!sin) return false;
    return sin;
  }

  if(p==2) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    for(unsigned i(0);i<9 && sin;i++) {
      if(i!=7) { // BEND08 missing?
	for(unsigned j(0);j<2 && sin;j++) {
	  sin >> _bendCurrent[i][j];
	  if(!sin) return false;
	}
      }
    }
    return sin;
  }
  
  if(p==3) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    for(unsigned i(0);i<11 && sin;i++) {
      if(i!=3 && i!=6) { // COLL04 and COLL07 missing?
	for(unsigned j(0);j<2 && sin;j++) {
	  for(unsigned k(0);k<2 && sin;k++) {
	    sin >> _collimatorPosition[i][j][k];
	    if(!sin) return false;
	  }
	}
      }
    }
    return sin;
  }  
  
  if(p==4) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;

    double ec;    
    for(unsigned i(0);i<4 && sin;i++) {
      sin >> ec;
      _h6aExperimentCount[i]=(unsigned)(ec+0.5);
      if(!sin) return false;
    }
    
    for(unsigned i(0);i<4 && sin;i++) {
      sin >> ec;
      _h6bExperimentCount[i]=(unsigned)(ec+0.5);
      if(!sin) return false;
    }
    
    for(unsigned i(0);i<4 && sin;i++) {
      sin >> ec;
      _h6cExperimentCount[i]=(unsigned)(ec+0.5);
      if(!sin) return false;
    }
    return sin;
  }
  
  if(p==5) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    for(unsigned i(0);i<16 && sin;i++) {
      for(unsigned j(0);j<2 && sin;j++) {
	sin >> _quadrupoleCurrent[i][j];
	if(!sin) return false;
      }
    }
    return sin;
  }
  
  if(p==6) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;

    double ec;
    for(unsigned i(0);i<8 && sin;i++) {
      sin >> ec;
      _rpExperimentCount[i]=(unsigned)(ec+0.5);
      if(!sin) return false;
    }
    return sin;
  }
  
  if(p==7) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    double sc;
    for(unsigned i(0);i<9 && sin;i++) {
      if(i!=1 && i!=2) { // SCINT02 and SCINT03 missing?
	sin >> sc;
	_scintillatorCount[i]=(unsigned)(sc+0.5);
	if(!sin) return false;
      }
    }
    return sin;
  }
  
  if(p==8) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    sin >> _t4Position;
    if(!sin) return false;
    return sin;
  }
  
  if(p==9) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    sin >> _targetPosition;
    if(!sin) return false;
    return sin;
  }
    
  if(p==10) {
    unsigned ts;
    sin >> ts;
    sin >> ts;
    if(!sin) return false;
    
    for(unsigned i(0);i<6 && sin;i++) {
      for(unsigned j(0);j<2 && sin;j++) {
	sin >> _trimCurrent[i][j];
	if(!sin) return false;
      }
    }
    return sin;
  }
  
  return sin;
}

time_t BmlCernSlowRunData::timeStamp() const {
  return _timeStamp;
}

void BmlCernSlowRunData::timeStamp(time_t t) {
  _timeStamp=t;
}

//CRP Implementation of access functions 
//Note that we have to convert the unsigned ints appearing in this function to ints as the LCGenericObjects
//used further down the line cannot deal with unsigned ints and we want to reduce the number of methods
//in the part of the converter which treats these functions
//We can do/use the reinterpret cast operators since we switch between two very similar objects
//i.e. unsigned and int. It's very likely that this remains safe forever
std::vector<double> BmlCernSlowRunData::_sextapoleCurrents() const {
  std::vector<double> v;
  const double* sextapoleCurrentPtr(&_sextapoleCurrent[0][0]);
  //for(unsigned int i=0; i<NSEXTCUR; i++) v.push_back((_sextapoleCurrent[0])[i]);
  for(unsigned int i=0; i<NSEXTCUR; i++) v.push_back(sextapoleCurrentPtr[i]);
  return v;
}
std::vector<double> BmlCernSlowRunData::_bendCurrents() const {
  std::vector<double> v;
  const double* bendCurrentPtr(&_bendCurrent[0][0]); 
  for(unsigned int i=0; i<NBENDCUR; i++) v.push_back(bendCurrentPtr[i]);
  return v;
}
std::vector<double> BmlCernSlowRunData::_collimatorPositions() const {
  std::vector<double> v;
  const double* collimatorPositionPtr(&_collimatorPosition[0][0][0]);
  for(unsigned int i=0; i<NCOLPOS; i++) v.push_back(collimatorPositionPtr[i]);
  return v;
}
std::vector<double> BmlCernSlowRunData::_quadrupoleCurrents() const {
  std::vector<double> v;
  const double* quadrupoleCurrentPtr(&_quadrupoleCurrent[0][0]);
  for(unsigned int i=0; i<NQUADCUR; i++) v.push_back(quadrupoleCurrentPtr[i]);
  return v;
}
std::vector<double> BmlCernSlowRunData::_trimCurrents() const {
  std::vector<double> v;
  const double* trimCurrentPtr(&_trimCurrent[0][0]);
  for(unsigned int i=0; i<NTRIMCUR; i++) v.push_back(trimCurrentPtr[i]);
  return v;
}
std::vector<unsigned int> BmlCernSlowRunData::_h6aExperimentCounts() const {
  std::vector<unsigned int> v;
  for(unsigned int i=0; i<NH6AEXPCOUNT; i++) v.push_back(_h6aExperimentCount[i]);
  return v;
}

std::vector<unsigned int> BmlCernSlowRunData::_h6bExperimentCounts() const {
  std::vector<unsigned int> v;
  for(unsigned int i=0; i<NH6BEXPCOUNT; i++) v.push_back(_h6bExperimentCount[i]);
  return v;
}

std::vector<unsigned int> BmlCernSlowRunData::_h6cExperimentCounts() const {
  std::vector<unsigned int> v;
  for(unsigned int i=0; i<NH6CEXPCOUNT; i++) v.push_back(_h6cExperimentCount[i]);
  return v;
}
std::vector<unsigned int> BmlCernSlowRunData::_rpExperimentCounts() const {
  std::vector<unsigned int> v;
  for(unsigned int i=0; i<NRPEXPCOUNT; i++) v.push_back(_rpExperimentCount[i]);
  return v;
}
std::vector<unsigned int> BmlCernSlowRunData::_scintillatorCounts() const {
  std::vector<unsigned int> v;
  for(unsigned int i=0; i<NSCINTCOUNT; i++) v.push_back(_scintillatorCount[i]);
  return v;
}



double BmlCernSlowRunData::getAbsorberPosition() const { return _absorberPosition;}
double BmlCernSlowRunData::getT4Position() const { return _t4Position;}
double BmlCernSlowRunData::getTargetPosition() const { return _targetPosition;}


std::ostream& BmlCernSlowRunData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCernSlowRunData::print()" << std::endl;

  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);

  for(unsigned i(0);i<2;i++) {
    for(unsigned j(0);j<2;j++) {
      o << s << " Sextapole current " << i;
      if(j==0) o << " measured  = ";
      else     o << " reference = ";
      o << _sextapoleCurrent[i][j] << " A" << std::endl;
    }
  }
  
  o << s << " Absorber position " << _absorberPosition << std::endl;

  for(unsigned i(0);i<9;i++) {
    for(unsigned j(0);j<2;j++) {
      o << s << " Bend current " << i;
      if(j==0) o << " measured  = ";
      else     o << " reference = ";
      o << _bendCurrent[i][j] << " A" << std::endl;
    }
  }
  
  for(unsigned i(0);i<11;i++) {
    for(unsigned j(0);j<2;j++) {
      for(unsigned k(0);k<2;k++) {
	o << s << " Collimator position " << _collimatorPosition[i][j][k] << std::endl;
      }
    }
  }
  
  for(unsigned i(0);i<4;i++) {
    o << s << " H6A experiment " << i << " count = "
      << _h6aExperimentCount[i] << std::endl;
  }
  
  for(unsigned i(0);i<4;i++) {
    o << s << " H6B experiment " << i << " count = "
      << _h6bExperimentCount[i] << std::endl;
  }
  
  for(unsigned i(0);i<4;i++) {
    o << s << " H6C experiment " << i << " count = "
      << _h6cExperimentCount[i] << std::endl;
  }
  
  for(unsigned i(0);i<16;i++) {
    for(unsigned j(0);j<2;j++) {
      o << s << " Quadrupole current " << std::setw(2) << i;
      if(j==0) o << " measured  = ";
      else     o << " reference = ";
      o << _quadrupoleCurrent[i][j] << " A" << std::endl;
    }
  }

  for(unsigned i(0);i<8;i++) {
    o << s << " RP experiment " << i << " count = "
      << _rpExperimentCount[i] << std::endl;
  }

  for(unsigned i(0);i<9;i++) {
    o << s << " Scintillator " << i << " count = "
      << _scintillatorCount[i] << std::endl;
  }

  o << s << " T4 position " << _t4Position << std::endl;

  o << s << " Target position " << _targetPosition << std::endl;
  
  for(unsigned i(0);i<6;i++) {
    for(unsigned j(0);j<2;j++) {
      o << s << " Trim current " << i;
      if(j==0) o << " measured  = ";
      else     o << " reference = ";
      o << _trimCurrent[i][j] << " A" << std::endl;
    }
  }

  return o;
}

#endif
#endif
