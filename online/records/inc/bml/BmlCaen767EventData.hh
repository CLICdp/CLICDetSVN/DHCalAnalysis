#ifndef BmlCaen767EventData_HH
#define BmlCaen767EventData_HH

#include <iostream>
#include <fstream>
#include <vector>

#include "BmlCaen767StatusRegister.hh"
#include "BmlCaen767EventDatum.hh"


class BmlCaen767EventData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen767EventData();

  BmlCaen767StatusRegister statusRegister() const;
  void statusRegister(unsigned h, unsigned short r);

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);
  
  const BmlCaen767EventDatum* data() const;
        BmlCaen767EventDatum* data();
 
  std::vector<const BmlCaen767EventDatum*> channelData(unsigned c, bool l=false) const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  BmlCaen767StatusRegister _statusRegister;
  unsigned _numberOfWords;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen767EventData::BmlCaen767EventData() {
  memset(this,0,sizeof(BmlCaen767EventData));
}

BmlCaen767StatusRegister BmlCaen767EventData::statusRegister() const {
  return _statusRegister;
}

void BmlCaen767EventData::statusRegister(unsigned h, unsigned short r) {
  _statusRegister.statusRegister(h,r);
}

unsigned BmlCaen767EventData::numberOfWords() const {
  return _numberOfWords;
}

void BmlCaen767EventData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const BmlCaen767EventDatum* BmlCaen767EventData::data() const {
  return (const BmlCaen767EventDatum*)(this+1);
}

BmlCaen767EventDatum* BmlCaen767EventData::data() {
  return (BmlCaen767EventDatum*)(this+1);
}

std::vector<const BmlCaen767EventDatum*> BmlCaen767EventData::channelData(unsigned c, bool l) const {
  std::vector<const BmlCaen767EventDatum*> v;

  const BmlCaen767EventDatum *p(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    if(p[i].label()==BmlCaen767EventDatum::datum) {
      if(p[i].channelNumber()==c) {
	if(!l || p[i].leadingEdge()) v.push_back(p+i);
      }
    }
  }

  return v;
}

std::ostream& BmlCaen767EventData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen767EventData::print()" << std::endl;

  _statusRegister.print(o,s+" ");

  o << s << " Number of words = " << _numberOfWords << std::endl;

  const BmlCaen767EventDatum *p(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    o << s << " Datum " << std::setw(4) << i;
    p[i].print(o,s+"  ");
  }

  return o;
}

#endif
#endif
