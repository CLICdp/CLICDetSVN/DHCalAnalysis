#ifndef BmlLalHodoscopeRunData_HH
#define BmlLalHodoscopeRunData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlLalHodoscopeRunData {

public:
  enum {
    versionNumber=0
  };

  BmlLalHodoscopeRunData();

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned d);

  unsigned char softwareVersion() const;
  void          softwareVersion(unsigned char v);

  unsigned char orientation() const;
  void          orientation(unsigned char o);

  unsigned short mode() const;
  void           mode(unsigned short m);

  unsigned misc() const;
  void     misc(unsigned m);

  unsigned short hv(unsigned c) const;
  void           hv(unsigned c, unsigned short v);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _numberOfWords;
  UtlPack _constants;
  unsigned _misc;
  UtlPack _hv[2];
  
  /*
  UtlPack _modeACQ;
  UtlPack _thrOPERA[2];
  UtlPack _delay;
  UtlPack _gain[64];
  */
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLalHodoscopeRunData::BmlLalHodoscopeRunData() {
  memset(this,0,sizeof(BmlLalHodoscopeRunData));
}

unsigned BmlLalHodoscopeRunData::numberOfWords() const {
  return _numberOfWords;
}

void BmlLalHodoscopeRunData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

unsigned char BmlLalHodoscopeRunData::softwareVersion() const {
  return _constants.byte(0);
}

void BmlLalHodoscopeRunData::softwareVersion(unsigned char c) {
  _constants.byte(0,c);
}

unsigned char BmlLalHodoscopeRunData::orientation() const {
  return _constants.byte(1);
}

void BmlLalHodoscopeRunData::orientation(unsigned char o) {
  _constants.byte(1,o);
}

unsigned short BmlLalHodoscopeRunData::mode() const {
  return _constants.halfWord(1);
}

void BmlLalHodoscopeRunData::mode(unsigned short m) {
  _constants.halfWord(1,m);
}

unsigned BmlLalHodoscopeRunData::misc() const {
  return _misc;
}

void BmlLalHodoscopeRunData::misc(unsigned m) {
  _misc=m;
}

unsigned short BmlLalHodoscopeRunData::hv(unsigned c) const {
  assert(c<4);
  return _hv[c/2].halfWord(c%2);
}

void BmlLalHodoscopeRunData::hv(unsigned c, unsigned short v) {
  assert(c<4);
  _hv[c/2].halfWord(c%2,v);
}

std::ostream& BmlLalHodoscopeRunData::print(std::ostream &o, std::string s) const {
  o << s << "BmlLalHodoscopeRunData::print()" << std::endl;
  o << s << " Number of words = " << _numberOfWords << std::endl;

  o << s << " Constants = " << printHex(_constants) << std::endl;
  o << s << "  Software version = " << (unsigned)softwareVersion() << std::endl;
  o << s << "  Orientation = " << (unsigned)orientation() << std::endl;
  o << s << "  Mode = " << mode() << std::endl;

  o << s << " Misc = " << _misc << std::endl;

  o << s << " HV word 0 = " << printHex(_hv[0]) << std::endl;
  o << s << "  HV channel 0 = " << (unsigned)hv(0) << std::endl;
  o << s << "  HV channel 1 = " << (unsigned)hv(1) << std::endl;

  o << s << " HV word 1 = " << printHex(_hv[1]) << std::endl;
  o << s << "  HV channel 2 = " << (unsigned)hv(2) << std::endl;
  o << s << "  HV channel 3 = " << (unsigned)hv(3) << std::endl;

  return o;
}

#endif
#endif
