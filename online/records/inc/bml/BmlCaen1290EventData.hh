#ifndef BmlCaen1290EventData_HH
#define BmlCaen1290EventData_HH

#include <iostream>
#include <fstream>
#include <vector>

#include "BmlCaen1290StatusRegister.hh"
#include "BmlCaen1290EventDatum.hh"


class BmlCaen1290EventData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen1290EventData();

  BmlCaen1290StatusRegister statusRegister() const;
  void statusRegister(unsigned h, unsigned short r);

  UtlPack eventFifo() const;
  void    eventFifo(unsigned e);

  unsigned short eventCount() const;
  unsigned short wordCount() const;

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);
  
  const BmlCaen1290EventDatum* data() const;
        BmlCaen1290EventDatum* data();
 
  std::vector<const BmlCaen1290EventDatum*> channelData(unsigned c, bool l=false) const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  BmlCaen1290StatusRegister _statusRegister;
  UtlPack _eventFifo;
  unsigned _numberOfWords;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290EventData::BmlCaen1290EventData() {
  memset(this,0,sizeof(BmlCaen1290EventData));
}

BmlCaen1290StatusRegister BmlCaen1290EventData::statusRegister() const {
  return _statusRegister;
}

void BmlCaen1290EventData::statusRegister(unsigned h, unsigned short r) {
  _statusRegister.statusRegister(h,r);
}

unsigned short BmlCaen1290EventData::eventCount() const {
  return _eventFifo.halfWord(1);
}

unsigned short BmlCaen1290EventData::wordCount() const {
  return _eventFifo.halfWord(0);
}

UtlPack BmlCaen1290EventData::eventFifo() const {
  return _eventFifo;
}

void BmlCaen1290EventData::eventFifo(unsigned e) {
  _eventFifo.word(e);
}

unsigned BmlCaen1290EventData::numberOfWords() const {
  return _numberOfWords;
}

void BmlCaen1290EventData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const BmlCaen1290EventDatum* BmlCaen1290EventData::data() const {
  return (const BmlCaen1290EventDatum*)(this+1);
}

BmlCaen1290EventDatum* BmlCaen1290EventData::data() {
  return (BmlCaen1290EventDatum*)(this+1);
}

std::vector<const BmlCaen1290EventDatum*> BmlCaen1290EventData::channelData(unsigned c, bool l) const {
  std::vector<const BmlCaen1290EventDatum*> v;

  const BmlCaen1290EventDatum *p(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    if(p[i].label()==BmlCaen1290EventDatum::tdcDatum) {
      if(p[i].channelNumber()==c) {
	if(!l || p[i].risingEdge()) v.push_back(p+i);
      }
    }
  }

  return v;
}

std::ostream& BmlCaen1290EventData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290EventData::print()" << std::endl;

  _statusRegister.print(o,s+" ");

  o << s << " Event FIFO = " << printHex(_eventFifo) << std::endl;
  o << s << "  Event count = " << eventCount() << std::endl;
  o << s << "  Word count = " << wordCount() << std::endl;

  o << s << " Number of words = " << _numberOfWords << std::endl;

  const BmlCaen1290EventDatum *p(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    o << s << " Datum " << std::setw(4) << i;
    p[i].print(o,s+"  ");
  }

  return o;
}

#endif
#endif
