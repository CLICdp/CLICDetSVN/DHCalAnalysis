#ifndef BmlFnalSlowReadoutDataV1_HH
#define BmlFnalSlowReadoutDataV1_HH

#include <vector>
#include <string>
#include <iostream>

#include "UtlMapDefinitions.hh"

class BmlFnalSlowReadoutDataV1 {

public:
  enum {
    versionNumber=1
  };

  BmlFnalSlowReadoutDataV1();

  bool parse(std::string r);
  
  time_t slowTimeStamp() const;
  void   slowTimeStamp(time_t t);

  time_t fnalTimeStamp() const;
  void   fnalTimeStamp(time_t t);

  std::vector<double> magnetCurrents() const;
  std::vector<double> targetAndCollimators() const;
  std::vector<double> pinholeCollimators() const;
  std::vector<double> cherenkovPressures() const;
  std::vector<double> hallProbes() const;

   /**Access functions to retrieve the maps containing the different values */
  /**Definition of a pointers to functiona to access the readings*/
  DaqTypesDblMap_t getDblMap() { 
    //Initialize the map between types and access functions in the DAQ class - doubles
    DaqTypesDblMap_t _theDblTypes;
    _theDblTypes.clear();
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("MAGNETCURRENTS", magnetCurrents())).second) std::cout << "Problems with inserting MAGNETCURRENTS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("TARGETANDCOLLIMATORS", targetAndCollimators())).second) std::cout << "Problems with inserting TARGETANDCOLLIMATORS into map" << std::endl; 
     if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("PINHOLECOLLIMATORS", pinholeCollimators())).second) std::cout << "Problems with inserting PINHOLECOLLIMATORS into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CHERENKOVPRESSURES", cherenkovPressures())).second) std::cout << "Problems with inserting CHERENKOVPRESSURES into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("HALLPROBES", hallProbes())).second) std::cout << "Problems with inserting HALLPROBES into map" << std::endl;  
    return _theDblTypes;
  }; 

  //For Completeness 
  DaqTypesUIntMap_t getUIntMap() { 
    DaqTypesUIntMap_t _theUIntTypes;
    _theUIntTypes.clear();
    return _theUIntTypes;
  };

 DaqTypesFloatMap_t getFloatMap() { 
    DaqTypesFloatMap_t _theFloatTypes;
    _theFloatTypes.clear();
    return _theFloatTypes;
  };

  //Return a map of time stamps
  std::map<std::string,time_t> getTimeStamps() {
    std::map<std::string,time_t> _theTimeStamps;  
    _theTimeStamps.clear(); 
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("FNALTIMESTAMP", fnalTimeStamp())).second) std::cout << "Problems with inserting FNALTIMESTAMP into map" << std::endl;  
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("DAQTIMESTAMP", slowTimeStamp())).second) std::cout << "Problems with inserting DAQTIMESTAMP into map" << std::endl;    return _theTimeStamps;
   }



  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  //time_t _timeStamp[2]; // 8 bytes on 64-bit
  unsigned _timeStamp[2];

  double _magnetCurrent[12];
  double _targetAndCollimator[9];
  double _pinholeCollimator[4];
  double _cherenkovPressure[2];
  double _hallProbe[3];
  double _spares[4];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


BmlFnalSlowReadoutDataV1::BmlFnalSlowReadoutDataV1() {
  memset(this,0,sizeof(BmlFnalSlowReadoutDataV1));
}

bool BmlFnalSlowReadoutDataV1::parse(std::string r) {
  std::istringstream sin(r);

  // Set the spare locations
  for(unsigned i(0);i<4;i++) {
    _spares[i]=999999.0;
  }

  // Store timestamps
  sin >> _timeStamp[0] >> _timeStamp[1];
  if(!sin) return false;
  
  // Store data
  for(unsigned i(0);i<(12+9+4+2+3) && sin;i++) {
    sin >> _magnetCurrent[i];
    if(!sin) return false;
  }

  return sin;
}

time_t BmlFnalSlowReadoutDataV1::slowTimeStamp() const {
  return _timeStamp[0];
}

void BmlFnalSlowReadoutDataV1::slowTimeStamp(time_t t) {
  _timeStamp[0]=t;
}

time_t BmlFnalSlowReadoutDataV1::fnalTimeStamp() const {
  return _timeStamp[1];
}

void BmlFnalSlowReadoutDataV1::fnalTimeStamp(time_t t) {
  _timeStamp[1]=t;
}

std::vector<double> BmlFnalSlowReadoutDataV1::magnetCurrents() const {
  std::vector<double> v(12);
  for(unsigned i(0);i<12;i++) v[i]=_magnetCurrent[i];
  return v;
}

std::vector<double> BmlFnalSlowReadoutDataV1::targetAndCollimators() const {
  std::vector<double> v(9);
  for(unsigned i(0);i<9;i++) v[i]=_targetAndCollimator[i];
  return v;
}

std::vector<double> BmlFnalSlowReadoutDataV1::pinholeCollimators() const {
  std::vector<double> v(4);
  for(unsigned i(0);i<4;i++) v[i]=_pinholeCollimator[i];
  return v;
}

std::vector<double> BmlFnalSlowReadoutDataV1::cherenkovPressures() const {
  std::vector<double> v(2);
  for(unsigned i(0);i<2;i++) v[i]=_cherenkovPressure[i];
  return v;
}

std::vector<double> BmlFnalSlowReadoutDataV1::hallProbes() const {
  std::vector<double> v(3);
  for(unsigned i(0);i<3;i++) v[i]=_hallProbe[i];
  return v;
}

std::ostream& BmlFnalSlowReadoutDataV1::print(std::ostream &o, std::string s) const {
  o << s << "BmlFnalSlowReadoutDataV1::print()" << std::endl;

  time_t sts(slowTimeStamp());
  o << s << " Slow controls timestamp = " << _timeStamp[0] << " = " << ctime(&sts);
  time_t fts(fnalTimeStamp());
  o << s << " FNAL database timestamp = " << _timeStamp[1] << " = " << ctime(&fts);

  o << s << " Magnet main west bend         = " << _magnetCurrent[ 0] << " A" << std::endl;
  o << s << " Magnet target bend            = " << _magnetCurrent[ 1] << " A" << std::endl;
  o << s << " Magnet target low limit       = " << _magnetCurrent[ 2] << " A" << std::endl;
  o << s << " Magnet last east bend         = " << _magnetCurrent[ 3] << " A" << std::endl;
  o << s << " Magnet last east low limit    = " << _magnetCurrent[ 4] << " A" << std::endl;
  o << s << " Magnet quadrupole 1           = " << _magnetCurrent[ 5] << " A" << std::endl;
  o << s << " Magnet quadrupole 1 low limit = " << _magnetCurrent[ 6] << " A" << std::endl;
  o << s << " Magnet quadrupole 2           = " << _magnetCurrent[ 7] << " A" << std::endl;
  o << s << " Magnet quadrupole 2 low limit = " << _magnetCurrent[ 8] << " A" << std::endl;
  o << s << " Magnet vertical trim 1        = " << _magnetCurrent[ 9] << " A" << std::endl;
  o << s << " Magnet vertical trim 2        = " << _magnetCurrent[10] << " A" << std::endl;
  o << s << " Magnet horizontal trim        = " << _magnetCurrent[11] << " A" << std::endl;

  o << s << " Target upstream              = " << _targetAndCollimator[0] << " mm" << std::endl;
  o << s << " Target downstream            = " << _targetAndCollimator[1] << " mm" << std::endl;
  o << s << " Collimator first vertical    = " << _targetAndCollimator[2] << " mm" << std::endl;
  o << s << " Collimator first horizontal  = " << _targetAndCollimator[3] << " mm" << std::endl;
  o << s << " Lead sheet                   = " << _targetAndCollimator[4] << " mm" << std::endl;
  o << s << " Collimator second horizontal = " << _targetAndCollimator[5] << " mm" << std::endl;
  o << s << " Collimator second vertical   = " << _targetAndCollimator[6] << " mm" << std::endl;
  o << s << " Muon absorber half 1         = " << _targetAndCollimator[7] << " mm" << std::endl;
  o << s << " Muon absorber half 2         = " << _targetAndCollimator[8] << " mm" << std::endl;

  o << s << " Pinhole collimator upstream horizontal   = " << _pinholeCollimator[0] << " mm" << std::endl;
  o << s << " Pinhole collimator downstream horizontal = " << _pinholeCollimator[1] << " mm" << std::endl;
  o << s << " Pinhole collimator upstream vertical     = " << _pinholeCollimator[2] << " mm" << std::endl;
  o << s << " Pinhole collimator downstream vertical   = " << _pinholeCollimator[3] << " mm" << std::endl;

  o << s << " Cherenkov pressure inner? = " << _cherenkovPressure[0] << std::endl;
  o << s << " Cherenkov pressure outer? = " << _cherenkovPressure[1] << std::endl;

  o << s << " Hall probe 0 = " << _hallProbe[0] << std::endl;
  o << s << " Hall probe 1 = " << _hallProbe[1] << std::endl;
  o << s << " Hall probe 2 = " << _hallProbe[2] << std::endl;

  return o;
}

#endif
#endif
