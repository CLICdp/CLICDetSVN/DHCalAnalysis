#ifndef BmlLc1176RunData_HH
#define BmlLc1176RunData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlLc1176RunData {

public:
  enum {
    versionNumber=0
  };

  BmlLc1176RunData();

  unsigned char baseAddress() const;
  void baseAddress(unsigned char b);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _data;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLc1176RunData::BmlLc1176RunData() {
  memset(this,0,sizeof(BmlLc1176RunData));
}

unsigned char BmlLc1176RunData::baseAddress() const {
  return _data.byte(0);
}

void BmlLc1176RunData::baseAddress(unsigned char b) {
  _data.word(0);
  _data.byte(0,b);
}

std::ostream& BmlLc1176RunData::print(std::ostream &o, std::string s) const {
  o << s << "BmlLc1176RunData::print()" << std::endl;
  o << s << " Data = " << printHex(_data.word()) << std::endl;
  return o;
}

#endif
#endif
