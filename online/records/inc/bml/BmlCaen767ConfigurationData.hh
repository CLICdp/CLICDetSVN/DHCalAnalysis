#ifndef BmlCaen767ConfigurationData_HH
#define BmlCaen767ConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen767ConfigurationData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen767ConfigurationData();

  UtlPack controlRegister() const;
  void    controlRegister(unsigned h, unsigned short r);

  bool memoryTest() const;
  void memoryTest(bool b);

  UtlPack interruptRegister() const;
  void    interruptLevel(unsigned short r);
  void    interruptVector(unsigned short r);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _controlRegister;
  UtlPack _interruptRegister;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen767ConfigurationData::BmlCaen767ConfigurationData() {
  memset(this,0,sizeof(BmlCaen767ConfigurationData));
}

UtlPack BmlCaen767ConfigurationData::controlRegister() const {
  return _controlRegister;
}

void BmlCaen767ConfigurationData::controlRegister(unsigned h, unsigned short r) {
  assert(h<2);
  _controlRegister.halfWord(h,r);
}

bool BmlCaen767ConfigurationData::memoryTest() const {
  return _controlRegister.bit(20);
}

void BmlCaen767ConfigurationData::memoryTest(bool b) {
  _controlRegister.bit(20,b);
}

UtlPack BmlCaen767ConfigurationData::interruptRegister() const {
  return _interruptRegister;
}

void BmlCaen767ConfigurationData::interruptLevel(unsigned short r) {
  _interruptRegister.halfWord(0,r);
}

void BmlCaen767ConfigurationData::interruptVector(unsigned short r) {
  _interruptRegister.halfWord(1,r);
}

std::ostream& BmlCaen767ConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen767ConfigurationData::print()" << std::endl;

  o << s << " Control register   = " << printHex(_controlRegister) << std::endl;
  if(_controlRegister.bit(2)) o << s << "  Send data until end-of-block" << std::endl;
  else                        o << s << "  Ignor end-of-block" << std::endl;
  if(_controlRegister.bit(4)) o << s << "  Front panel reset of whole module enabled" << std::endl;
  else                        o << s << "  Front panel reset of data-only enabled" << std::endl;
  if(_controlRegister.bit(5)) o << s << "  VME bus error enabled" << std::endl;
  else                        o << s << "  VME bus error disabled" << std::endl;

  if(_controlRegister.bits(16,17)==0) o << s << "  Stop trigger matching mode" << std::endl;
  if(_controlRegister.bits(16,17)==1) o << s << "  Start trigger matching mode" << std::endl;
  if(_controlRegister.bits(16,17)==2) o << s << "  Start gating mode" << std::endl;
  if(_controlRegister.bits(16,17)==3) o << s << "  Continuous storage mode" << std::endl;
  if(_controlRegister.bits(18,19)==0) o << s << "  Data ready = event ready" << std::endl;
  if(_controlRegister.bits(18,19)==1) o << s << "  Data ready = buffer almost full" << std::endl;
  if(_controlRegister.bits(18,19)==2) o << s << "  Data ready = buffer not empty" << std::endl;
  if(_controlRegister.bits(18,19)==3) o << s << "  Data ready = unknown" << std::endl;
  if(_controlRegister.bit(20)) o << s << "  Memory test enabled" << std::endl;
  else                         o << s << "  Memory test disabled" << std::endl;

  o << s << " Interrupt register = " << printHex(_interruptRegister) << std::endl;
  o << s << "  Interrupt level     = " << _interruptRegister.bits(0,2) << std::endl;
  o << s << "  Interrupt status/id = " << (unsigned)_interruptRegister.byte(2) << std::endl;

  return o;
}

#endif
#endif
