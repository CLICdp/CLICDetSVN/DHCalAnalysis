#ifndef BmlCaen1290OpcodeData_HH
#define BmlCaen1290OpcodeData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlCaen1290OpcodeData {

public:
  enum {
    versionNumber=0
  };

  BmlCaen1290OpcodeData();

  bool triggerMatching() const;
  bool continuousStorage() const;
  void acquisitionMode(bool b);

  unsigned short windowWidth() const;
  unsigned       windowWidthNs() const;
  void           windowWidth(unsigned short w);

  short windowOffset() const;
  int   windowOffsetNs() const;
  void  windowOffset(short w);

  int   windowStartNs() const;
  int   windowEndNs() const;

  unsigned short extraMargin() const;
  unsigned       extraMarginNs() const;
  void           extraMargin(unsigned short w);

  unsigned short rejectMargin() const;
  unsigned       rejectMarginNs() const;
  void           rejectMargin(unsigned short w);

  bool subtractTriggerTime() const;
  void subtractTriggerTime(bool b);

  bool pairMode() const;
  bool risingEdgesOnly() const;
  bool fallingEdgesOnly() const;
  bool bothEdges() const;
  void edgeDetection(unsigned d);

  unsigned char lsbResolution() const;
  unsigned char pairResolution() const;
  void          resolution(unsigned short r);

  unsigned char deadTime() const;
  void          deadTime(unsigned char d);

  bool headerAndTrailer() const;
  void headerAndTrailer(bool b);

  unsigned maximumHits() const;
  void     maximumHits(unsigned char c);

  unsigned short tdcInternalErrorEnable() const;
  void           tdcInternalErrorEnable(unsigned short e);

  unsigned char readoutFifoSize() const;
  void          readoutFifoSize(unsigned char s);
  
  bool enable(unsigned c) const;
  void enable(unsigned c, bool b);
  unsigned enablePattern() const;
  void     enablePattern(unsigned n);
  unsigned enablePattern32(unsigned t) const;

  unsigned short globalOffset() const;
  void           globalOffset(unsigned short g);

  unsigned char channelAdjust(unsigned c) const;
  void          channelAdjust(unsigned c, unsigned char a);

  unsigned short tdcRcAdjust(unsigned t) const;
  void           tdcRcAdjust(unsigned t, unsigned short a);

  bool operator!=(const BmlCaen1290OpcodeData &c) const;
  bool operator==(const BmlCaen1290OpcodeData &c) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _bits;
  UtlPack _window;
  UtlPack _margin;
  UtlPack _tdcErrorGlobalOffset;
  UtlPack _enablePattern;
  UtlPack _channelAdjust[8];
  UtlPack _tdcRcAdjust[2];
  UtlPack _spare[2];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlCaen1290OpcodeData::BmlCaen1290OpcodeData() {
  memset(this,0,sizeof(BmlCaen1290OpcodeData));

  acquisitionMode(true);
  subtractTriggerTime(true);

  _enablePattern.word(0xffffffff);

  // Window ends at max of ~1.5us after trigger
  windowWidth(80); // 2us
  //windowWidth(500); // 12.5us: TEMP TO SEE FNAL BEAM STRUCTURE
  windowOffset(-20); // 500ns
  //windowOffset(-40); // 1us

  extraMargin(16);
  rejectMargin(8);

  edgeDetection(2);
  headerAndTrailer(true);

  resolution(3);
  maximumHits(9);

  tdcInternalErrorEnable(0x7ff);
  readoutFifoSize(7);

  tdcRcAdjust(0,1023);
  tdcRcAdjust(1,1023);
  //tdcRcAdjust(2,1023);
  //tdcRcAdjust(3,1023);

  /*
  //memoryTest(true);
  //acquisitionMode(1);
  _enablePattern[0].halfWord(0,0x3ff); // Channels 0-9

  _enablePattern[0].word(0xffffffff);
  _enablePattern[1].word(0xffffffff);
  _enablePattern[2].word(0xffffffff);
  enable(49,false);


  triggerConfiguration(1);
  detection(0,3); // Leading and fallling edges
  detection(1,3);
  detection(2,3);
  //detection(0,1); // Leading edge only
  //detection(1,1);
  //detection(2,1);


  //dataReadyMode(1);
  //almostFullLevel(2); // Minimum allowed
  //almostFullLevel(32767); // Maximum allowed
  //almostFullLevel(32000); // CERN
  almostFullLevel(29000);// FNAL 16 words x 200 triggers below 32k
  */
}
/*
bool BmlCaen1290OpcodeData::memoryTest() const {
  return _bits.bit(0);
}

void BmlCaen1290OpcodeData::memoryTest(bool b) {
  _bits.bit(0,b);
}
*/
bool BmlCaen1290OpcodeData::triggerMatching() const {
  return _bits.bit(0);
}

bool BmlCaen1290OpcodeData::continuousStorage() const {
  return !_bits.bit(0);
}

void BmlCaen1290OpcodeData::acquisitionMode(bool b) {
  _bits.bit(0,b);
}

unsigned short BmlCaen1290OpcodeData::windowWidth() const  {
  return _window.halfWord(0);
}

unsigned BmlCaen1290OpcodeData::windowWidthNs() const  {
  return 25*windowWidth();
}

void BmlCaen1290OpcodeData::windowWidth(unsigned short w)  {
  _window.halfWord(0,w);
}

short BmlCaen1290OpcodeData::windowOffset() const  {
  return (short)_window.halfWord(1);
}

int BmlCaen1290OpcodeData::windowOffsetNs() const  {
  return 25*windowOffset();
}

void BmlCaen1290OpcodeData::windowOffset(short w)  {
  _window.halfWord(1,(unsigned short)w);
}

int BmlCaen1290OpcodeData::windowStartNs() const  {
  return windowOffsetNs();
}

int BmlCaen1290OpcodeData::windowEndNs() const  {
  return windowOffsetNs()+windowWidthNs();
}

unsigned short BmlCaen1290OpcodeData::extraMargin() const  {
  return _margin.halfWord(0);
}

unsigned BmlCaen1290OpcodeData::extraMarginNs() const  {
  return 25*extraMargin();
}

void BmlCaen1290OpcodeData::extraMargin(unsigned short w)  {
  _margin.halfWord(0,w);
}

unsigned short BmlCaen1290OpcodeData::rejectMargin() const  {
  return _margin.halfWord(1);
}

unsigned BmlCaen1290OpcodeData::rejectMarginNs() const  {
  return 25*rejectMargin();
}

void BmlCaen1290OpcodeData::rejectMargin(unsigned short w)  {
  _margin.halfWord(1,w);
}

bool BmlCaen1290OpcodeData::subtractTriggerTime() const {
  return _bits.bit(1);
}

void BmlCaen1290OpcodeData::subtractTriggerTime(bool b) {
  _bits.bit(1,b);
}

bool BmlCaen1290OpcodeData::pairMode() const {
  return _bits.bits(4,5)==0;
}

bool BmlCaen1290OpcodeData::risingEdgesOnly() const {
  return _bits.bits(4,5)==2;
}

bool BmlCaen1290OpcodeData::fallingEdgesOnly() const {
  return _bits.bits(4,5)==1;
}

bool BmlCaen1290OpcodeData::bothEdges() const {
  return _bits.bits(4,5)==3;
}

void BmlCaen1290OpcodeData::edgeDetection(unsigned d) {
  assert(d<4);
  _bits.bits(4,5,d);
}

unsigned char BmlCaen1290OpcodeData::lsbResolution() const {
  return _bits.halfByte(4);
}

unsigned char BmlCaen1290OpcodeData::pairResolution() const {
  return _bits.halfByte(5);
}

void BmlCaen1290OpcodeData::resolution(unsigned short d) {
  _bits.halfByte(4,d&0xf);
  _bits.halfByte(5,(d>>8)&0xf);
}

unsigned char BmlCaen1290OpcodeData::deadTime() const {
  return _bits.bits(6,7);
}

void BmlCaen1290OpcodeData::deadTime(unsigned char d) {
  return _bits.bits(6,7,d);
}

bool BmlCaen1290OpcodeData::headerAndTrailer() const {
  return _bits.bit(2);
}

void BmlCaen1290OpcodeData::headerAndTrailer(bool b) {
  _bits.bit(2,b);
}

unsigned BmlCaen1290OpcodeData::maximumHits() const  {
  if(_bits.halfByte(2)==0) return 0;
  if(_bits.halfByte(2)>=9) return 0xffffffff;
  return 1<<(_bits.halfByte(2)-1);
}

void BmlCaen1290OpcodeData::maximumHits(unsigned char w)  {
  assert(w<16);
  _bits.halfByte(2,w);
}

unsigned short BmlCaen1290OpcodeData::tdcInternalErrorEnable() const  {
  return _tdcErrorGlobalOffset.halfWord(0);
}

void BmlCaen1290OpcodeData::tdcInternalErrorEnable(unsigned short w)  {
  _tdcErrorGlobalOffset.halfWord(0,w);
}

unsigned char BmlCaen1290OpcodeData::readoutFifoSize() const  {
  return _bits.halfByte(3);
}

void BmlCaen1290OpcodeData::readoutFifoSize(unsigned char w)  {
  assert(w<8);
  _bits.halfByte(3,w);
}

bool BmlCaen1290OpcodeData::enable(unsigned c) const {
  assert(c<32);
  return _enablePattern.bit(c);
}

void BmlCaen1290OpcodeData::enable(unsigned c, bool b) {
  assert(c<32);
  _enablePattern.bit(c,b);
}

unsigned BmlCaen1290OpcodeData::enablePattern() const {
  return _enablePattern.word();
}

void BmlCaen1290OpcodeData::enablePattern(unsigned n) {
  _enablePattern.word(n);
}

unsigned BmlCaen1290OpcodeData::enablePattern32(unsigned t) const {
  assert(t<4);
  UtlPack reply(0);
  for(unsigned i(0);i<8;i++) reply.bit(4*i,_enablePattern.bit(8*t+i));
  return reply.word();
}

unsigned short BmlCaen1290OpcodeData::globalOffset() const  {
  return _tdcErrorGlobalOffset.halfWord(1);
}

void BmlCaen1290OpcodeData::globalOffset(unsigned short w)  {
  _tdcErrorGlobalOffset.halfWord(1,w);
}

unsigned char BmlCaen1290OpcodeData::channelAdjust(unsigned c) const {
  assert(c<32);
  return _channelAdjust[c/4].byte(c%4);
}

void BmlCaen1290OpcodeData::channelAdjust(unsigned c, unsigned char b) {
  assert(c<32);
  _channelAdjust[c/4].byte(c%4,b);
}

unsigned short BmlCaen1290OpcodeData::tdcRcAdjust(unsigned c) const {
  assert(c<4);
  return _tdcRcAdjust[c/2].halfWord(c%2);
}

void BmlCaen1290OpcodeData::tdcRcAdjust(unsigned c, unsigned short b) {
  assert(c<4);
  _tdcRcAdjust[c/2].halfWord(c%2,b);
}

bool BmlCaen1290OpcodeData::operator!=(const BmlCaen1290OpcodeData &c) const {
  /*
  if(_bits!=c._bits) return true;
  if(_enablePattern[0]!=c._enablePattern[0]) return true;
  if(_enablePattern[1]!=c._enablePattern[1]) return true;
  if(_enablePattern[2]!=c._enablePattern[2]) return true;
  if(_enablePattern[3]!=c._enablePattern[3]) return true;
  if(_window!=c._window) return true;
  if(_latencyOffset!=c._latencyOffset) return true;
  if(_almostFull!=c._almostFull) return true;
  */
  return false;
}
 
bool BmlCaen1290OpcodeData::operator==(const BmlCaen1290OpcodeData &c) const {
  return !operator!=(c);
}

std::ostream& BmlCaen1290OpcodeData::print(std::ostream &o, std::string s) const {
  o << s << "BmlCaen1290OpcodeData::print()" << std::endl;

  o << s << " Bits         = " << printHex(_bits) << std::endl;
  if(triggerMatching())   o << s << "  Trigger matching enabled" << std::endl;
  if(continuousStorage()) o << s << "  Continuous storage enabled" << std::endl;

  if(subtractTriggerTime()) o << s << "  Subtraction of trigger time enabled" << std::endl;
  else                      o << s << "  Subtraction of trigger time disabled" << std::endl;

  if(pairMode()) o << s << "  Pair mode enabled" << std::endl;
  if(risingEdgesOnly()) o << s << "  Rising edges only enabled" << std::endl;
  if(fallingEdgesOnly()) o << s << "  Falling edges only enabled" << std::endl;
  if(bothEdges())        o << s << "  Both edges enabled" << std::endl;

  if(headerAndTrailer()) o << s << "  Header and trailer enabled" << std::endl;
  else                   o << s << "  Header and trailer disabled" << std::endl;

  o << s << " Window width = " << windowWidth() << " = " << windowWidthNs() << "ns"
    << ", offset = " << windowOffset() << " = " << windowOffsetNs() << "ns" << std::endl;
  o << s << "  Window start = " << windowStartNs() << "ns, end = " << windowEndNs()  << "ns";
  if(windowEndNs()>1000) o << ", hits cut off at 1000ns";
  o << std::endl;
  o << s  << " Window extra margin = " << extraMargin() << " = " << extraMarginNs() << "ns"
    << ", reject margin = " << rejectMargin() << " = " << rejectMarginNs() << "ns" << std::endl;

  o << s << " LSB resolution " << (unsigned)lsbResolution() << " = ";
  if(!pairMode()) {
    if(lsbResolution()==0) o << "800ps";
    if(lsbResolution()==1) o << "200ps";
    if(lsbResolution()==2) o << "100ps";
    if(lsbResolution()==3) o << "25ps";
    if(lsbResolution()>=4) o << "unknown";
  } else {
    if(lsbResolution()==0) o << "100ps";
    if(lsbResolution()==1) o << "200ps";
    if(lsbResolution()==2) o << "400ps";
    if(lsbResolution()==3) o << "800ps";
    if(lsbResolution()==4) o << "1.6ns";
    if(lsbResolution()==5) o << "3.2ns";
    if(lsbResolution()==6) o << "6.25ns";
    if(lsbResolution()==7) o << "12.5ns";
    if(lsbResolution()>=8) o << "unknown";
  }
  o << ", pair resolution " << (unsigned)pairResolution() << " = ";
  if(!pairMode()) {
    if(pairResolution()== 0) o << "n/a";
    if(pairResolution()>= 1) o << "unknown";
  } else {
    if(pairResolution()== 0) o << "100ps";
    if(pairResolution()== 1) o << "200ps";
    if(pairResolution()== 2) o << "400ps";
    if(pairResolution()== 3) o << "800ps";
    if(pairResolution()== 4) o << "1.6ns";
    if(pairResolution()== 5) o << "3.2ns";
    if(pairResolution()== 6) o << "6.25ns";
    if(pairResolution()== 7) o << "12.5ns";
    if(pairResolution()== 8) o << "25ns";
    if(pairResolution()== 9) o << "50ns";
    if(pairResolution()==10) o << "100ns";
    if(pairResolution()==11) o << "200ns";
    if(pairResolution()==12) o << "400ns";
    if(pairResolution()==13) o << "800ns";
    if(pairResolution()>=14) o << "unknown";
  }
  o << std::endl;

  o << s << " Double hit deadtime " << (unsigned)deadTime() << " = ";
  if(deadTime()==0) o << "5ns" << std::endl;
  if(deadTime()==1) o << "10ns" << std::endl;
  if(deadTime()==2) o << "30ns" << std::endl;
  if(deadTime()==3) o << "100ns" << std::endl;
    
  o << s << " Maximum hits per event " << (unsigned)maximumHits() << std::endl;
  o << s << " TDC internal error enables = " << printHex(tdcInternalErrorEnable()) << std::endl;
  o << s << " Global offset = " << globalOffset() << std::endl;

  o << s << " Readout FIFO size " << (unsigned)readoutFifoSize() << " = " << (1<<(readoutFifoSize())) << " words" << std::endl;

  o << s << " Enable pattern = " << printHex(_enablePattern) << std::endl;
  for(unsigned i(0);i<4;i++) {
    o << s << "  32-bit enable pattern " << i << " = " << printHex(enablePattern32(i)) << std::endl;
  }


  o << s << " Channel adjusts" << std::endl;
  for(unsigned i(0);i<4;i++) {
    o << s << "  Channels " << std::setw(2) << 8*i << "-"  << std::setw(2) << 8*i+7 << " = ";
    for(unsigned j(8*i);j<8*(i+1);j++) {
      o << std::setw(4) << (unsigned)channelAdjust(j);
    }
    o << std::endl;
  }


  o << s << " TDC RC adjusts = ";
  for(unsigned i(0);i<4;i++) {
    o << std::setw(5) << tdcRcAdjust(i);
  }
  o << std::endl;

  return o;
}

#endif
#endif
