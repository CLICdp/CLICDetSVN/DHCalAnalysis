#ifndef BmlFnalAcquisitionData_HH
#define BmlFnalAcquisitionData_HH

#include <vector>
#include <string>
#include <iostream>

#include "UtlMapDefinitions.hh"

#pragma pack (4)

class BmlFnalAcquisitionData {

public:
  enum {
    versionNumber=0
  };

  BmlFnalAcquisitionData();

  bool parse(std::string r);
  
  time_t slowTimeStamp() const;
  void   slowTimeStamp(time_t t);

  time_t fnalTimeStamp() const;
  void   fnalTimeStamp(time_t t);

  std::vector<double> beamFluxes() const;
  std::vector<unsigned> scalers() const;


    /**Access functions to retrieve the maps containing the different values */
  /**Definition of a pointers to functiona to access the readings*/
  DaqTypesDblMap_t getDblMap() { 
    //Initialize the map between types and access functions in the DAQ class - doubles
    DaqTypesDblMap_t _theDblTypes;
    _theDblTypes.clear();
    //std::vector<double> v(_sextapoleCurrents());
    std::vector<double> v;
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("BEAMFLUXES", beamFluxes())).second) std::cout << "Problems with inserting BEAMFLUXES into map" << std::endl;  
    return _theDblTypes;
  }; 


 DaqTypesUIntMap_t getUIntMap() { 
    DaqTypesUIntMap_t _theUIntTypes;
    _theUIntTypes.clear();
    if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("SCALERS", scalers()   )).second) std::cout
 << "Problems with inserting FNAL SCALERS into map" << std::endl;  
    return _theUIntTypes;
  };


  //Return a map of time stamps
  std::map<std::string,time_t> getTimeStamps() {
    std::map<std::string,time_t> _theTimeStamps;  
    _theTimeStamps.clear(); 
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("FNALTIMESTAMP", fnalTimeStamp())).second) std::cout << "Problems with inserting FNALTIMESTAMP into map" << std::endl;  
    if(!_theTimeStamps.insert(std::pair<std::string, time_t >("DAQTIMESTAMP", slowTimeStamp())).second) std::cout << "Problems with inserting DAQTIMESTAMP into map" << std::endl;    return _theTimeStamps;
   }



  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  //time_t _timeStamp[2]; // 8 bytes on 64-bit
  unsigned _timeStamp[2];

  double _beamFlux[4];
  unsigned _scaler[11];
};

#pragma pack ()

#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


BmlFnalAcquisitionData::BmlFnalAcquisitionData() {
  memset(this,0,sizeof(BmlFnalAcquisitionData));
}

bool BmlFnalAcquisitionData::parse(std::string r) {
  std::istringstream sin(r);

  // Store timestamps
  sin >> _timeStamp[0] >> _timeStamp[1];
  if(!sin) return false;
  
  // Store data; first the doubles
  for(unsigned i(0);i<4 && sin;i++) {
    sin >> _beamFlux[i];
    if(!sin) return false;
  }

  // Store data; now the scalers
  double n;
  for(unsigned i(0);i<11 && sin;i++) {
    sin >> n;
    if(n<0.0) _scaler[i]=(unsigned)(n-0.5);
    else      _scaler[i]=(unsigned)(n+0.5);
    if(!sin) return false;
  }

  return sin;
}

time_t BmlFnalAcquisitionData::slowTimeStamp() const {
  return _timeStamp[0];
}

void BmlFnalAcquisitionData::slowTimeStamp(time_t t) {
  _timeStamp[0]=t;
}

time_t BmlFnalAcquisitionData::fnalTimeStamp() const {
  return _timeStamp[1];
}

void BmlFnalAcquisitionData::fnalTimeStamp(time_t t) {
  _timeStamp[1]=t;
}

std::vector<double> BmlFnalAcquisitionData::beamFluxes() const {
  std::vector<double> v(4);
  for(unsigned i(0);i<4;i++) v[i]=_beamFlux[i];
  return v;
}

std::vector<unsigned> BmlFnalAcquisitionData::scalers() const {
  std::vector<unsigned> v(11);
  for(unsigned i(0);i<11;i++) v[i]=_scaler[i];
  return v;
}

std::ostream& BmlFnalAcquisitionData::print(std::ostream &o, std::string s) const {
  o << s << "BmlFnalAcquisitionData::print()" << std::endl;

  time_t sts(slowTimeStamp());
  o << s << " Slow controls timestamp = " << _timeStamp[0] << " = " << ctime(&sts);
  time_t fts(fnalTimeStamp());
  o << s << " FNAL database timestamp = " << _timeStamp[1] << " = " << ctime(&fts);

  o << s << " Main injector beam flux     = " << _beamFlux[0] << " p" << std::endl;
  o << s << " Switchyard beam flux        = " << _beamFlux[1] << " p" << std::endl;
  o << s << " West split beam flux        = " << _beamFlux[2] << " p" << std::endl;
  o << s << " Downstream target beam flux = " << _beamFlux[3] << " p" << std::endl;

  o << s << " First TOF scaler  = " << _scaler[0] << std::endl;
  o << s << " Second TOF scaler = " << _scaler[1] << std::endl;
  for(unsigned i(0);i<8;i++) {
    o << s << " User scaler " << i << "     = " << _scaler[2+i] << std::endl;
  }
  o << s << " Cherenkov scaler  = " << _scaler[10] << std::endl;

  return o;
}

#endif
#endif
