#ifndef BmlHodEventData_HH
#define BmlHodEventData_HH

#include <iostream>
#include <string>


class BmlHodEventData {

public:
  enum {
    versionNumber=0
  };

  enum Plane {
    x1,y1,x2,y2
  };

  BmlHodEventData();
  BmlHodEventData(const unsigned short *d);

  unsigned short data(Plane p) const;

  bool hit(Plane p, unsigned j) const;
  unsigned hit(Plane p) const;

  unsigned numberOfBits(Plane p) const;
  
  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned short _data[4];
};

#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"

BmlHodEventData::BmlHodEventData() {
}

BmlHodEventData::BmlHodEventData(const unsigned short *d) {
  _data[0]=d[0];
  _data[1]=d[1];
  _data[2]=d[2];
  _data[3]=d[3];
}

unsigned short BmlHodEventData::data(Plane p) const {
  return _data[p];
}

bool BmlHodEventData::hit(Plane p, unsigned j) const {
  return (_data[p]&(1<<j))!=0;
}

unsigned BmlHodEventData::hit(Plane p) const {
  for(unsigned j(0);j<16;j++) {
    if(_data[p]==(1<<j)) return 2*j;
  }
  
  for(unsigned j(0);j<15;j++) {
    if(_data[p]==(3<<j)) return 2*j+1;
  }
  return 0xffffffff;
}

unsigned BmlHodEventData::numberOfBits(Plane p) const {
  unsigned n(0);
  for(unsigned i(0);i<16;i++) {
    if((_data[p]&(1<<i))!=0) n++;
  }
  return n;
}

std::ostream& BmlHodEventData::print(std::ostream &o, std::string s) const {
  o << s << "BmlHodEventData::print()" << std::endl;
  o << s << " Plane X1 = " << printHex(_data[0]) << std::endl;
  o << s << " Plane Y1 = " << printHex(_data[1]) << std::endl;
  o << s << " Plane X2 = " << printHex(_data[2]) << std::endl;
  o << s << " Plane Y2 = " << printHex(_data[3]) << std::endl;
  return o;
}

#endif
#endif
