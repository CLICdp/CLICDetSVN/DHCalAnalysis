#ifndef BmlLalHodoscopeTriggerData_HH
#define BmlLalHodoscopeTriggerData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class BmlLalHodoscopeTriggerData {

public:
  enum {
    versionNumber=0
  };

  BmlLalHodoscopeTriggerData();

  unsigned counter() const;
  void     counter(unsigned c);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _counter;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


BmlLalHodoscopeTriggerData::BmlLalHodoscopeTriggerData() {
  memset(this,0,sizeof(BmlLalHodoscopeTriggerData));
}

unsigned BmlLalHodoscopeTriggerData::counter() const {
  return _counter;
}

void BmlLalHodoscopeTriggerData::counter(unsigned c) {
  _counter=c;
}

std::ostream& BmlLalHodoscopeTriggerData::print(std::ostream &o, std::string s) const {
  o << s << "BmlLalHodoscopeTriggerData::print()" << std::endl;
  o << s << " Counter = " << _counter << std::endl;
  return o;
}

#endif
#endif
