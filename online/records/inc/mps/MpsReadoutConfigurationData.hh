#ifndef MpsReadoutConfigurationData_HH
#define MpsReadoutConfigurationData_HH

#include <iostream>
#include <fstream>


class MpsReadoutConfigurationData {

public:
  enum {
    versionNumber=0
  };

  MpsReadoutConfigurationData();

  bool usbDaqEnable(unsigned u) const;
  void usbDaqEnable(unsigned u, bool b);
  void usbDaqEnableAll(bool b);

  bool usbDaqIoEnable(unsigned u) const;
  void usbDaqIoEnable(unsigned u, bool b);
  void usbDaqIoEnableAll(bool b);

  bool pcbEnable(unsigned u) const;
  void pcbEnable(unsigned u, bool b);
  void pcbEnableAll(bool b);

  bool sensorEnable(unsigned u) const;
  void sensorEnable(unsigned u, bool b);
  void sensorEnableAll(bool b);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _enables;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsReadoutConfigurationData::MpsReadoutConfigurationData() {
  memset(this,0,sizeof(MpsReadoutConfigurationData));
  _enables=0xffffffff;
}

bool MpsReadoutConfigurationData::usbDaqEnable(unsigned u) const {
  assert(u<8);
  return _enables.bit(u);
}

void MpsReadoutConfigurationData::usbDaqEnable(unsigned u, bool b) {
  assert(u<8);
  _enables.bit(u,b);
}

void MpsReadoutConfigurationData::usbDaqEnableAll(bool b) {
  if(b) _enables.byte(0,0xff);
  else  _enables.byte(0,0x00);
}

bool MpsReadoutConfigurationData::usbDaqIoEnable(unsigned u) const {
  assert(u<8);
  return _enables.bit(u+8);
}

void MpsReadoutConfigurationData::usbDaqIoEnable(unsigned u, bool b) {
  assert(u<8);
  _enables.bit(u+8,b);
}

void MpsReadoutConfigurationData::usbDaqIoEnableAll(bool b) {
  if(b) _enables.byte(1,0xff);
  else  _enables.byte(1,0x00);
}

bool MpsReadoutConfigurationData::pcbEnable(unsigned u) const {
  assert(u<8);
  return _enables.bit(u+16);
}

void MpsReadoutConfigurationData::pcbEnable(unsigned u, bool b) {
  assert(u<8);
  _enables.bit(u+16,b);
}

void MpsReadoutConfigurationData::pcbEnableAll(bool b) {
  if(b) _enables.byte(2,0xff);
  else  _enables.byte(2,0x00);
}

bool MpsReadoutConfigurationData::sensorEnable(unsigned u) const {
  assert(u<8);
  return _enables.bit(u+24);
}

void MpsReadoutConfigurationData::sensorEnable(unsigned u, bool b) {
  assert(u<8);
  _enables.bit(u+24,b);
}

void MpsReadoutConfigurationData::sensorEnableAll(bool b) {
  if(b) _enables.byte(3,0xff);
  else  _enables.byte(3,0x00);
}

std::ostream& MpsReadoutConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "MpsReadoutConfigurationData::print()" << std::endl;
  o << s << " Enables = " << printHex(_enables) << std::endl;

  o << s << "  Enabled USB_DAQ readouts  =";
  if(_enables.byte(0)==0) {
    o << " none" << std::endl;
  } else {
    for(unsigned i(0);i<8;i++) {
      if(usbDaqEnable(i)) o << " " << i;
    }
    o << std::endl;
  }

  o << s << "  Enabled USB_DAQ histories =";
  if(_enables.byte(1)==0) {
    o << " none" << std::endl;
  } else {
    for(unsigned i(0);i<8;i++) {
      if(usbDaqIoEnable(i)) o << " " << i;
    }
    o << std::endl;
  }

  o << s << "  Enabled PCB readouts      =";
  if(_enables.byte(2)==0) {
    o << " none" << std::endl;
  } else {
    for(unsigned i(0);i<8;i++) {
      if(pcbEnable(i)) o << " " << i;
    }
    o << std::endl;
  }

  o << s << "  Enabled sensor readouts   =";
  if(_enables.byte(3)==0) {
    o << " none" << std::endl;
  } else {
    for(unsigned i(0);i<8;i++) {
      if(sensorEnable(i)) o << " " << i;
    }
    o << std::endl;
  }

  return o;
}

#endif
#endif
