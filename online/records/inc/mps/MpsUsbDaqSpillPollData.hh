#ifndef MpsUsbDaqSpillPollData_HH
#define MpsUsbDaqSpillPollData_HH

#include <string>
#include <iostream>

#include "UtlTime.hh"


class MpsUsbDaqSpillPollData {

public:
  enum {
    versionNumber=0
  };

  MpsUsbDaqSpillPollData();

  void update(bool e=true);

  UtlTimeDifference maximumTime() const;
  void              maximumTime(UtlTimeDifference t);

  UtlTime startTime() const;
  UtlTime   endTime() const;
  UtlTime      time() const;

  UtlTimeDifference actualTime() const;

  bool timeout() const;

  unsigned numberOfPolls() const;
  void     numberOfPolls(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlTime _startTime;
  UtlTime _endTime;
  UtlTimeDifference _maximumTime;
  unsigned _numberOfPolls;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

MpsUsbDaqSpillPollData::MpsUsbDaqSpillPollData() {
  memset(this,0,sizeof(MpsUsbDaqSpillPollData));
}

void MpsUsbDaqSpillPollData::update(bool e) {
  if(e) _endTime.update();
  else  _startTime.update();
}

UtlTimeDifference MpsUsbDaqSpillPollData::maximumTime() const {
  return _maximumTime;
}

void MpsUsbDaqSpillPollData::maximumTime(UtlTimeDifference t) {
  _maximumTime=t;
}

UtlTime MpsUsbDaqSpillPollData::startTime() const {
  return _startTime;
}

UtlTime MpsUsbDaqSpillPollData::endTime() const {
  return _endTime;
}

UtlTime MpsUsbDaqSpillPollData::time() const {
  return _endTime;
}

UtlTimeDifference MpsUsbDaqSpillPollData::actualTime() const {
  return _endTime-_startTime;
}

bool MpsUsbDaqSpillPollData::timeout() const {
  return (actualTime()>_maximumTime);
}

unsigned MpsUsbDaqSpillPollData::numberOfPolls() const {
  return _numberOfPolls;
}

void MpsUsbDaqSpillPollData::numberOfPolls(unsigned n) {
  _numberOfPolls=n;
}

std::ostream& MpsUsbDaqSpillPollData::print(std::ostream &o, std::string s) const {
  o << s << "MpsUsbDaqSpillPollData::print()" << std::endl;

  o << s << " Maximum time = " << _maximumTime << " secs" << std::endl;
  o << s << " Start time = " << _startTime << std::endl;
  o << s << " End time   = " << _endTime << std::endl;

  o << s << " Actual time = " << actualTime() << " secs";
  if(timeout()) o << " = timeout";
  o << std::endl;

  o << s << " Number of polls = " << _numberOfPolls << std::endl;

  return o;
}

#endif
#endif
