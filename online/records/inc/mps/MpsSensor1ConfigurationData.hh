#include "MpsSensorV10ConfigurationData.hh"
#include "MpsSensorV12ConfigurationData.hh"

#ifdef MPS_TPAC10
typedef MpsSensorV10ConfigurationData MpsSensor1ConfigurationData;
#else
typedef MpsSensorV12ConfigurationData MpsSensor1ConfigurationData;
#endif
