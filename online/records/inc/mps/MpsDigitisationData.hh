#ifndef MpsDigitisationData_HH
#define MpsDigitisationData_HH

#include <iostream>
#include <string>
#include <stdint.h>


class MpsDigitisationData {

public:
  enum {
    versionNumber=0
  };

  MpsDigitisationData();

  const double* chargeSpread(unsigned i, unsigned j) const;

  bool read(const std::string &f);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _tu2eV;
  double _eVNoise;
  double _chargeSpread[10][10][5][5];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsDigitisationData::MpsDigitisationData() {
  memset(this,0,sizeof(MpsDigitisationData));
}

bool MpsDigitisationData::read(const std::string &f) {
  std::ifstream fin(f.c_str());

  for(unsigned i(0);i<10;i++) {
    for(unsigned j(0);j<10;j++) {
      for(unsigned k(0);k<10;k++) {
	for(unsigned l(0);l<10;l++) {
	  if(!fin) return false;
	  fin >> _chargeSpread[i][j][k][l];
	}
      }
    }
  }

  return true;
}

const double* MpsDigitisationData::chargeSpread(unsigned i, unsigned j) const {
  assert(i<50 && j<50);
  return _chargeSpread[i][j][0];
}

std::ostream& MpsDigitisationData::print(std::ostream &o, std::string s) const {
  o << s << "MpsDigitisationData::print()" << std::endl;
  return o;
}

#endif
#endif
