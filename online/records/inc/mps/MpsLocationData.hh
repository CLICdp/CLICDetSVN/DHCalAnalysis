#ifndef MpsLocationData_HH
#define MpsLocationData_HH

#include "MpsLocation.hh"


template <class Data> class MpsLocationData : public MpsLocation {

public:
  MpsLocationData();
  MpsLocationData(MpsLocation l);
  MpsLocationData(MpsLocation l, const Data &d);
  
  MpsLocation location() const;
  void location(MpsLocation l);

  const Data* data() const;
  Data*       data();
  void        data(const Data &p);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  Data _data;
};


template <class Data> 
MpsLocationData<Data>::MpsLocationData() : MpsLocation(), _data() {
}
  
template <class Data> 
MpsLocationData<Data>::MpsLocationData(MpsLocation l) :
  MpsLocation(l), _data() {
}
  
template <class Data> 
MpsLocationData<Data>::MpsLocationData(MpsLocation l, const Data &d) :
  MpsLocation(l), _data(d) {
}
  
template <class Data> 
MpsLocation MpsLocationData<Data>::location() const {
  return *((MpsLocation*)this);
}

template <class Data> 
void MpsLocationData<Data>::location(MpsLocation l) {
  *((MpsLocation*)this)=l;
}

template <class Data> 
const Data* MpsLocationData<Data>::data() const {
  return &_data;
}

template <class Data> 
Data* MpsLocationData<Data>::data() {
  return &_data;
}

template <class Data> 
void MpsLocationData<Data>::data(const Data &p) {
  _data=p;
}

template <class Data> 
std::ostream& MpsLocationData<Data>::print(std::ostream &o, std::string s) const {
  o << s << "MpsLocationData::print()" << std::endl;
  MpsLocation::print(o,s+" ");
  _data.print(o,s+" ");
  return o;
}

#ifdef CALICE_DAQ_ICC
#endif
#endif
