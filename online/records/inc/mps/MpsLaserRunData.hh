#ifndef MpsLaserRunData_HH
#define MpsLaserRunData_HH

#include <iostream>
#include <fstream>


class MpsLaserRunData {

public:
  enum {
    versionNumber=0
  };

  MpsLaserRunData();

  unsigned messageId() const;
  void     messageId(unsigned s);

  unsigned shutterSizeX() const;
  void     shutterSizeX(unsigned s);

  unsigned shutterSizeY() const;
  void     shutterSizeY(unsigned s);

  std::string softwareVersion() const;
  void        softwareVersion(const std::string &s);

  bool parse(const std::string &r);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _messageId;
  unsigned _shutterSizeX;
  unsigned _shutterSizeY;
  char _softwareVersion[12];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsLaserRunData::MpsLaserRunData() {
  memset(this,0,sizeof(MpsLaserRunData));
}

unsigned MpsLaserRunData::messageId() const {
  return _messageId;
}

void MpsLaserRunData::messageId(unsigned s) {
  _messageId=s;
}

unsigned MpsLaserRunData::shutterSizeX() const {
  return _shutterSizeX;
}

void MpsLaserRunData::shutterSizeX(unsigned s) {
  _shutterSizeX=s;
}

unsigned MpsLaserRunData::shutterSizeY() const {
  return _shutterSizeY;
}

void MpsLaserRunData::shutterSizeY(unsigned s) {
  _shutterSizeY=s;
}

std::string MpsLaserRunData::softwareVersion() const {
  return _softwareVersion;
}

void MpsLaserRunData::softwareVersion(const std::string &s) {
  for(unsigned i(0);i<12;i++) {
    if(i<s.size() && i<11) {
      _softwareVersion[i]=s[i];
    } else {
      _softwareVersion[i]='\0';
    }
  }
}

bool MpsLaserRunData::parse(const std::string &r) {

  // Check packet is for run data
  if(r.size()<17) return false;
  //if(r[0]!='3') return false;

  // Get message ID
  {char c[2];
  c[0]=r[0];
  c[1]='\0';

  std::istringstream sin(c);
  sin >> _messageId;
  if(!sin) return false;}

  // Get shutter sizes
  unsigned digit;

  _shutterSizeX=0;
  for(unsigned i(0);i<3;i++) {
    _shutterSizeX*=10;

    char c[2];
    c[0]=r[i+1];
    c[1]='\0';

    std::istringstream sin(c);
    sin >> digit;
    if(!sin) return false;

    _shutterSizeX+=digit;
  }

  _shutterSizeY=0;
  for(unsigned i(0);i<3;i++) {
    _shutterSizeY*=10;

    char c[2];
    c[0]=r[i+4];
    c[1]='\0';

    std::istringstream sin(c);
    sin >> digit;
    if(!sin) return false;

    _shutterSizeY+=digit;
  }

  // Get software version
  for(unsigned i(0);i<10;i++) {
      _softwareVersion[i]=r[i+7];
  }
  _softwareVersion[10]='\0';
  _softwareVersion[11]='\0';

  return true;
}

std::ostream& MpsLaserRunData::print(std::ostream &o, std::string s) const {
  o << s << "MpsLaserRunData::print()" << std::endl;

  o << s << " Message ID = " << _messageId << std::endl;
  o << s << " Shutter sizes: x = " << _shutterSizeX
    << ", y = " << _shutterSizeY << std::endl;
  o << s << " Software version = " << _softwareVersion << std::endl;

  return o;
}

#endif
#endif
