#ifndef MpsUsbDaqMasterConfigurationData_HH
#define MpsUsbDaqMasterConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class MpsUsbDaqMasterConfigurationData {

public:
  enum {
    versionNumber=0
  };

  MpsUsbDaqMasterConfigurationData();

  unsigned short testBits() const;
  void           testBits(unsigned short t);

  unsigned short resetLength() const;
  void           resetLength(unsigned short l);

  unsigned short spillCycleStart() const;
  void           spillCycleStart(unsigned short t);

  unsigned short bunchCrossingLength() const;
  void           bunchCrossingLength(unsigned short l);

  unsigned short bunchTrainLength() const;
  void           bunchTrainLength(unsigned short l);

  unsigned short triggerEnableMask() const;
  void           triggerEnableMask(unsigned short m);

  unsigned short triggerInversionMask() const;
  void           triggerInversionMask(unsigned short m);

  unsigned testTriggerStart() const;
  void     testTriggerStart(unsigned t);

  unsigned testTriggerEnd() const;
  void     testTriggerEnd(unsigned t);

  void zeroBools();

  bool readHistoryEnable() const;
  void readHistoryEnable(bool b);

  bool spillVetoEnable() const;
  void spillVetoEnable(bool b);

  bool readEudetEnable() const;
  void readEudetEnable(bool b);

  bool operator==(const MpsUsbDaqMasterConfigurationData &d) const;
  bool operator!=(const MpsUsbDaqMasterConfigurationData &d) const;

  void writeFile(const std::string &fileName);
  void readFile(const std::string &fileName);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned grayToBinary(unsigned g) const;
  unsigned binaryToGray(unsigned b) const;

  unsigned _testTriggerStart;
  unsigned _testTriggerEnd;

  UtlPack _preBunchLengths;
  UtlPack _bunchLengths;
  UtlPack _triggerMasks;
  UtlPack _bits;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsUsbDaqMasterConfigurationData::MpsUsbDaqMasterConfigurationData() {
  memset(this,0,sizeof(MpsUsbDaqMasterConfigurationData));

  testBits(7);
  resetLength(1386);
  spillCycleStart(6999);
  //bunchCrossingLength(16);
  bunchCrossingLength(15);
  bunchTrainLength(7999);
  triggerEnableMask(0xffff);
  triggerInversionMask(0x0000);
  testTriggerStart(8190);
  testTriggerEnd(8191);
  readHistoryEnable(true);
  spillVetoEnable(false);
  readEudetEnable(false);
}

unsigned short MpsUsbDaqMasterConfigurationData::testBits() const {
  return _bits.halfWord(0);
}

void MpsUsbDaqMasterConfigurationData::testBits(unsigned short t) {
  _bits.halfWord(0,t);
}

unsigned short MpsUsbDaqMasterConfigurationData::spillCycleStart() const {
  return _preBunchLengths.halfWord(0);
}

void MpsUsbDaqMasterConfigurationData::spillCycleStart(unsigned short t) {
  _preBunchLengths.halfWord(0,t);
}

unsigned short MpsUsbDaqMasterConfigurationData::resetLength() const {
  return _preBunchLengths.halfWord(1);
}

void MpsUsbDaqMasterConfigurationData::resetLength(unsigned short l) {
  _preBunchLengths.halfWord(1,l);
}

unsigned short MpsUsbDaqMasterConfigurationData::bunchCrossingLength() const {
  return _bunchLengths.halfWord(0);
}

void MpsUsbDaqMasterConfigurationData::bunchCrossingLength(unsigned short l) {
  _bunchLengths.halfWord(0,l);
}

unsigned short MpsUsbDaqMasterConfigurationData::bunchTrainLength() const {
  return _bunchLengths.halfWord(1);
}

void MpsUsbDaqMasterConfigurationData::bunchTrainLength(unsigned short l) {
  _bunchLengths.halfWord(1,l);
}

unsigned short MpsUsbDaqMasterConfigurationData::triggerEnableMask() const {
  return _triggerMasks.halfWord(0);
}

void MpsUsbDaqMasterConfigurationData::triggerEnableMask(unsigned short t) {
  _triggerMasks.halfWord(0,t);
}

unsigned short MpsUsbDaqMasterConfigurationData::triggerInversionMask() const {
  return _triggerMasks.halfWord(1);
}

void MpsUsbDaqMasterConfigurationData::triggerInversionMask(unsigned short t) {
  _triggerMasks.halfWord(1,t);
}

unsigned MpsUsbDaqMasterConfigurationData::testTriggerStart() const {
  return _testTriggerStart;
}

void MpsUsbDaqMasterConfigurationData::testTriggerStart(unsigned t) {
  _testTriggerStart=t;
}

unsigned MpsUsbDaqMasterConfigurationData::testTriggerEnd() const {
  return _testTriggerEnd;
}

void MpsUsbDaqMasterConfigurationData::testTriggerEnd(unsigned t) {
  _testTriggerEnd=t;
}

void MpsUsbDaqMasterConfigurationData::zeroBools() {
  _bits.halfWord(1,0);
}

bool MpsUsbDaqMasterConfigurationData::readHistoryEnable() const {
  return _bits.bit(16);
}

void MpsUsbDaqMasterConfigurationData::readHistoryEnable(bool b) {
  _bits.bit(16,b);
}

bool MpsUsbDaqMasterConfigurationData::spillVetoEnable() const {
  return _bits.bit(17);
}

void MpsUsbDaqMasterConfigurationData::spillVetoEnable(bool b) {
  _bits.bit(17,b);
}

bool MpsUsbDaqMasterConfigurationData::readEudetEnable() const {
  return _bits.bit(18);
}

void MpsUsbDaqMasterConfigurationData::readEudetEnable(bool b) {
  _bits.bit(18,b);
}

unsigned MpsUsbDaqMasterConfigurationData::grayToBinary(unsigned g) const {
  g^=(g>>16);
  g^=(g>> 8);
  g^=(g>> 4);
  g^=(g>> 2);
  g^=(g>> 1);
  return g;
}

unsigned MpsUsbDaqMasterConfigurationData::binaryToGray(unsigned b) const {
  unsigned g((b>>1)^b);
  return g;
}

bool MpsUsbDaqMasterConfigurationData::operator!=(const MpsUsbDaqMasterConfigurationData &d) const {
  if(true) {
    for(unsigned i(0);i<sizeof(MpsUsbDaqMasterConfigurationData)/4;i++) {
      if(*((&_testTriggerStart)+i)!=*((&d._testTriggerStart)+i)) {
	std::cout << "MpsUsbDaqMasterConfigurationData::operator==()  Objects differ at word " << i << std::endl;
      }
    }
  }

  for(unsigned i(0);i<sizeof(MpsUsbDaqMasterConfigurationData)/4;i++) {
    if(*((&_testTriggerStart)+i)!=*((&d._testTriggerStart)+i)) return true;
  }

  return false;
}

bool MpsUsbDaqMasterConfigurationData::operator==(const MpsUsbDaqMasterConfigurationData &d) const {
  return !(this->operator!=(d));
}

void MpsUsbDaqMasterConfigurationData::writeFile(const std::string &fileName) {
  std::ofstream fout(fileName.c_str());
  if(fout) {

    unsigned n(sizeof(MpsUsbDaqMasterConfigurationData)/4);
    unsigned *p(&_testTriggerStart);

    for(unsigned i(0);i<n;i++) {
      fout << std::setw(2) << i << " " << p[i] << std::endl;
    }
  }
}

void MpsUsbDaqMasterConfigurationData::readFile(const std::string &fileName) {
  std::ifstream fin(fileName.c_str());
  if(fin) {

    unsigned n(sizeof(MpsUsbDaqMasterConfigurationData)/4);
    unsigned *p(&_testTriggerStart);

    unsigned c;
    unsigned v;
    for(unsigned i(0);i<n;i++) {
      fin >> c >> v;
      if(c==i) p[i]=v;
    }
  }
}

std::ostream& MpsUsbDaqMasterConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "MpsUsbDaqMasterConfigurationData::print()" << std::endl;

  o << s << " Bunch lengths           = " << printHex(_bunchLengths) << std::endl;
  o << s << "  Bunch crossing length  = " << bunchCrossingLength()
    << ", bunch crossing period = " << bunchCrossingLength()+1 << " x 25ns = "
    << 25*(bunchCrossingLength()+1) << " ns" << std::endl;
  o << s << "  Bunch train length     = " << bunchTrainLength()
    << ", bunch train = " << bunchTrainLength()+1 << " BX = "
    << 25*(bunchCrossingLength()+1)*(bunchTrainLength()+1) << " ns" << std::endl;

  o << s << " Pre-bunch lengths       = " << printHex(_preBunchLengths) << std::endl;
  o << s << "  Spill cycle start      = " << spillCycleStart()
    << ", spill cycle length = " << 8192-spillCycleStart() << " BX = " 
    << 25*(bunchCrossingLength()+1)*(8192-spillCycleStart()) << " ns" << std::endl;
  o << s << "  Reset length           = " << resetLength() << " x 25ns = "
    << 25*resetLength() << " ns" << std::endl;

  o << s << " Trigger masks           = " << printHex(_triggerMasks) << std::endl;
  o << s << "  Trigger enable mask    = " << printHex(triggerEnableMask()) << std::endl;
  o << s << "  Trigger inversion mask = " << printHex(triggerInversionMask()) << std::endl;

  o << s << " Other data              = " << printHex(_bits) << std::endl;
  o << s << "  Test bits              = " << printHex(testBits()) << std::endl;
  o << s << "  Bools                  = " << printHex(_bits.halfWord(1)) << std::endl;
  if(readHistoryEnable()) o << s << "   Read history enable set true"  << std::endl;
  else                    o << s << "   Read history enable set false" << std::endl;
  if(spillVetoEnable())   o << s << "   Spill veto enable set true"  << std::endl;
  else                    o << s << "   Spill veto enable set false" << std::endl;
  if(readEudetEnable())   o << s << "   Read EUDET enable set true"  << std::endl;
  else                    o << s << "   Read EUDET enable set false" << std::endl;

  o << s << " Test trigger start          = " << _testTriggerStart << " x 25ns = "
    << 25*_testTriggerStart << " ns" << std::endl;
  o << s << " Test trigger end            = " << _testTriggerEnd << " x 25ns = "
    << 25*_testTriggerEnd << " ns" << std::endl;

  return o;
}

#endif
#endif
