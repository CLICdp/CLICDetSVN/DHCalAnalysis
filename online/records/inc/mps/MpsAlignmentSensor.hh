#ifndef MpsAlignmentSensor_HH
#define MpsAlignmentSensor_HH

#include <iostream>
#include <string>
#include <cmath>

#include "MpsSensor1Hit.hh"

class MpsAlignmentSensor {

public:
  enum {
    versionNumber=0
  };

  MpsAlignmentSensor();

  unsigned layer() const;
  void     layer(unsigned l);

  unsigned orientation() const;
  void     orientation(unsigned o);

  bool frontFacing() const;

  double z() const;
  void   z(double c);

  double xDelta() const;
  void   xDelta(double d);

  double yDelta() const;
  void   yDelta(double d);

  double angle() const;
  void   angle(double a);

  double tDelta() const;
  void   tDelta(double t);

  std::pair<double,double> localToGlobal(std::pair<double,double> l) const;
  std::pair<double,double> globalToLocal(std::pair<double,double> g) const;

  std::pair<double,double> localToGlobal(double lx, double ly) const;
  std::pair<double,double> globalToLocal(double gx, double gy) const;

  std::pair<double,double>     pixelToGlobal(std::pair<unsigned,unsigned> p) const;
  std::pair<unsigned,unsigned> globalToPixel(std::pair<double,double> g) const;

  std::pair<double,double> hitToGlobal(MpsSensor1Hit h) const;

  bool  readFile(const std::string &f);
  bool writeFile(const std::string &f) const;

  bool  read(std::ifstream &fin);
  bool write(std::ofstream &fout) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _layer;
  unsigned _orientation;
  double _z;
  double _xDelta;
  double _yDelta;
  double _angle;
  double _tDelta;
};


#ifdef CALICE_DAQ_ICC


MpsAlignmentSensor::MpsAlignmentSensor() {
}

unsigned MpsAlignmentSensor::layer() const {
  return _layer;
}

void MpsAlignmentSensor::layer(unsigned l) {
  _layer=l;
}

unsigned MpsAlignmentSensor::orientation() const {
  return _orientation;
}

void MpsAlignmentSensor::orientation(unsigned o) {
  assert(o<8);
  _orientation=o;
}

bool MpsAlignmentSensor::frontFacing() const {
  return (_orientation/4)==0;
}

double MpsAlignmentSensor::z() const {
  return _z;
}

void MpsAlignmentSensor::z(double c) {
  _z=c;
}

double MpsAlignmentSensor::xDelta() const {
  return _xDelta;
}

void MpsAlignmentSensor::xDelta(double d) {
  _xDelta=d;
}

double MpsAlignmentSensor::yDelta() const {
  return _yDelta;
}

void MpsAlignmentSensor::yDelta(double d) {
  _yDelta=d;
}

double MpsAlignmentSensor::angle() const {
  return _angle;
}

void MpsAlignmentSensor::angle(double a) {
  _angle=a;
}

double MpsAlignmentSensor::tDelta() const {
  return _tDelta;
}

void MpsAlignmentSensor::tDelta(double t) {
  _tDelta=t;
}

std::pair<double,double> MpsAlignmentSensor::localToGlobal(std::pair<double,double> l) const {
  return localToGlobal(l.first,l.second);
}

std::pair<double,double> MpsAlignmentSensor::globalToLocal(std::pair<double,double> g) const {
  return globalToLocal(g.first,g.second);
}

std::pair<double,double> MpsAlignmentSensor::localToGlobal(double lx, double ly) const {
  std::pair<double,double> g;

  double pi(acos(-1.0));
  double a(_angle+0.5*pi*(_orientation%4));

  if(_orientation>=4) lx=-lx;

  g.first =(lx-_xDelta)*cos(a)-(ly-_yDelta)*sin(a);
  g.second=(lx-_xDelta)*sin(a)+(ly-_yDelta)*cos(a);

  return g;
}

std::pair<double,double> MpsAlignmentSensor::globalToLocal(double gx, double gy) const {
  std::pair<double,double> l;

  double pi(acos(-1.0));
  double a(_angle+0.5*pi*(_orientation%4));

  l.first = gx*cos(a)+gy*sin(a)+_xDelta;
  l.second=-gx*sin(a)+gy*cos(a)+_yDelta;

  if(_orientation>=4) l.first=-l.first;

  return l;
}

std::pair<double,double> MpsAlignmentSensor::pixelToGlobal(std::pair<unsigned,unsigned> p) const {
  assert(p.first<168 && p.second<168);
  return localToGlobal(std::pair<double,double>(MpsSensor1Hit::localX(p.first),MpsSensor1Hit::localY(p.second)));
}

std::pair<unsigned,unsigned> MpsAlignmentSensor::globalToPixel(std::pair<double,double> g) const {
  std::pair<double,double> l(globalToLocal(g));
  return std::pair<unsigned,unsigned>(MpsSensor1Hit::pixelX(l.first),MpsSensor1Hit::pixelY(l.second));
}

std::pair<double,double> MpsAlignmentSensor::hitToGlobal(MpsSensor1Hit h) const {
  return pixelToGlobal(std::pair<unsigned,unsigned>(h.pixelX(),h.pixelY()));
}

bool MpsAlignmentSensor::readFile(const std::string &f) {
  std::ifstream fin(f.c_str());
  return read(fin);
}

bool MpsAlignmentSensor::writeFile(const std::string &f) const {
  std::ofstream fout(f.c_str());
  return write(fout);
}

bool MpsAlignmentSensor::read(std::ifstream &fin) {
  if(!fin) return false;
  fin >> _layer >> _orientation >> _z >> _xDelta >> _yDelta >> _angle >> _tDelta;
  //std::cout << _orientation << " " << _z << " " << _xDelta << " " << _yDelta << " " << _angle << " " << _tDelta << std::endl;

  if(!fin) return false;
  return true;
}

bool MpsAlignmentSensor::write(std::ofstream &fout) const {
  if(!fout) return false;
  fout << _layer << " " << _orientation << " " << _z << " " << _xDelta << " " << _yDelta << " " << _angle << " " << _tDelta << std::endl;
  return fout;
}

std::ostream& MpsAlignmentSensor::print(std::ostream &o, std::string s) const {
  o << s << "MpsAlignmentSensor::print()" << std::endl;

  o << s << " Layer " << _layer << ", z = " << _z << " mm, orientation = " << _orientation << " = ";
  if(_orientation==0) o << "bottom, front-facing" << std::endl;
  if(_orientation==1) o << "left, front-facing" << std::endl;
  if(_orientation==2) o << "top, front-facing" << std::endl;
  if(_orientation==3) o << "right, front-facing" << std::endl;
  if(_orientation==4) o << "bottom, back-facing" << std::endl;
  if(_orientation==5) o << "left, back-facing" << std::endl;
  if(_orientation==6) o << "top, back-facing" << std::endl;
  if(_orientation==7) o << "right, back-facing" << std::endl;

  o << s << " Delta x,y = " << _xDelta << ", " << _yDelta
    << " mm, angle = " << _angle << " rad" << std::endl;
  o << s << " Delta t = " << _tDelta << " BX" << std::endl;

  return o;
}

#endif
#endif
