#ifndef MpsUsbDaqRunData_HH
#define MpsUsbDaqRunData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class MpsUsbDaqRunData {

public:
  enum {
    versionNumber=0
  };

  MpsUsbDaqRunData();

  bool master() const;
  void master(bool m);

  bool masterFirmware() const;
  void masterFirmware(bool m);

  void zeroIds();

  unsigned char sensorId() const;
  void          sensorId(unsigned char s);

  unsigned char usbDaqAddress() const;
  void          usbDaqAddress(unsigned char a);

  unsigned firmwareVersion() const;
  void     firmwareVersion(unsigned v);

  unsigned firmwareDate() const;
  void     firmwareDate(unsigned v);

  bool operator==(const MpsUsbDaqRunData &d) const;
  bool operator!=(const MpsUsbDaqRunData &d) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _ids;
  unsigned _firmwareVersion;
  unsigned _firmwareDate;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>

#include "UtlPrintHex.hh"


MpsUsbDaqRunData::MpsUsbDaqRunData() {
  memset(this,0,sizeof(MpsUsbDaqRunData));
}

void MpsUsbDaqRunData::zeroIds() {
  _ids.word(0);
}

bool MpsUsbDaqRunData::master() const {
  return _ids.bit(16);
}

void MpsUsbDaqRunData::master(bool b) {
  _ids.bit(16,b);
}

bool MpsUsbDaqRunData::masterFirmware() const {
  return _ids.bit(17);
}

void MpsUsbDaqRunData::masterFirmware(bool b) {
  _ids.bit(17,b);
}

unsigned char MpsUsbDaqRunData::sensorId() const {
  return _ids.byte(0);
}

void MpsUsbDaqRunData::sensorId(unsigned char s) {
  _ids.byte(0,s);
}

unsigned char MpsUsbDaqRunData::usbDaqAddress() const {
  return _ids.byte(1);
}

void MpsUsbDaqRunData::usbDaqAddress(unsigned char a) {
  _ids.byte(1,a);
}

unsigned MpsUsbDaqRunData::firmwareVersion() const {
  return _firmwareVersion;
}

void MpsUsbDaqRunData::firmwareVersion(unsigned v) {
  _firmwareVersion=v;
}

unsigned MpsUsbDaqRunData::firmwareDate() const {
  return _firmwareDate;
}

void MpsUsbDaqRunData::firmwareDate(unsigned v) {
  _firmwareDate=v;
}

bool MpsUsbDaqRunData::operator!=(const MpsUsbDaqRunData &d) const {
  if(_ids!=d._ids) return true;
  if(_firmwareVersion!=d._firmwareVersion) return true;
  if(_firmwareDate!=d._firmwareDate) return true;
  return false;
}

bool MpsUsbDaqRunData::operator==(const MpsUsbDaqRunData &d) const {
  return !(this->operator!=(d));
}

std::ostream& MpsUsbDaqRunData::print(std::ostream &o, std::string s) const {
  o << s << "MpsUsbDaqRunData::print()" << std::endl;

  o << s << " ID word = " << printHex(_ids) << std::endl;
  o << s << "  USB_DAQ address = " << printHex(usbDaqAddress());
  if(master()) o << ", master";
  if(masterFirmware()) o << ", master firmware";
  o << std::endl;

  o << s << "  Sensor ID       = " << printHex(sensorId()) << std::endl;

  o << s << " Firmware version = " << printHex(_firmwareVersion) << std::endl;
  time_t fd(_firmwareDate);
  o << s << " Firmware date    = " << std::setw(11) << _firmwareDate << " = "
    << ctime(&fd); // endl built into ctime!

  return o;
}

#endif
#endif
