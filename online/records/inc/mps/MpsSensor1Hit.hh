#ifndef MpsSensor1Hit_HH
#define MpsSensor1Hit_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class MpsSensor1Hit {

public:
  MpsSensor1Hit();
  MpsSensor1Hit(unsigned d);
  MpsSensor1Hit(unsigned i, unsigned j, unsigned t);

  unsigned pixelX() const;
  void     pixelX(unsigned i);

  unsigned pixelY() const;
  void     pixelY(unsigned j);

  double localX() const;
  double localY() const;

  unsigned x() const;
  void     x(unsigned i);

  unsigned y() const;
  void     y(unsigned j);

  unsigned timeStamp() const;
  void     timeStamp(unsigned t);

  bool operator==(const MpsSensor1Hit &that) const;
  bool operator!=(const MpsSensor1Hit &that) const;

  // Conversion from pixel number to space coordinate
  static double localX(unsigned x);
  static double localY(unsigned y);

  static unsigned pixelX(double x);
  static unsigned pixelY(double y);

  void datum(unsigned d);

  unsigned octant() const;
  bool neighbour(const MpsSensor1Hit &h) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _datum;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsSensor1Hit::MpsSensor1Hit() {
}

MpsSensor1Hit::MpsSensor1Hit(unsigned d) : _datum(d) {
}

MpsSensor1Hit::MpsSensor1Hit(unsigned i, unsigned j, unsigned t) {
  pixelX(i);
  pixelY(j);
  timeStamp(t);
}

unsigned MpsSensor1Hit::pixelX() const {
  return _datum.bits(0,8);
}

void MpsSensor1Hit::pixelX(unsigned i) {
  //assert(i<168);
  if(i>0xfffffff9) i-=0xfffffff9;
  if(i>=168) std::cerr << "MpsSensor1Hit::pixelX()  ERROR i = "
		       << i << " >= 168" << std::endl;
  _datum.bits(0,8,i);
}

unsigned MpsSensor1Hit::pixelY() const {
  return _datum.bits(9,17);
}

void MpsSensor1Hit::pixelY(unsigned j) {
  assert(j<168);
  _datum.bits(9,17,j);
}

unsigned MpsSensor1Hit::y() const {
  return pixelY();
}

void MpsSensor1Hit::x(unsigned i) {
  pixelX(i);
}

unsigned MpsSensor1Hit::x() const {
  return pixelX();
}

void MpsSensor1Hit::y(unsigned j) {
  pixelY(j);
}

double MpsSensor1Hit::localX() const {
  return localX(pixelX());
}

double MpsSensor1Hit::localY() const {
  return localY(pixelY());
}

unsigned MpsSensor1Hit::timeStamp() const {
  return _datum.bits(18,31);
}

void MpsSensor1Hit::timeStamp(unsigned t) {
  assert(t<(1<<14));
  _datum.bits(18,31,t);
}

bool MpsSensor1Hit::operator==(const MpsSensor1Hit &that) const {
  return _datum==that._datum;
}

bool MpsSensor1Hit::operator!=(const MpsSensor1Hit &that) const {
  return _datum!=that._datum;
}

void MpsSensor1Hit::datum(unsigned d) {
  _datum.word(d);
}

double MpsSensor1Hit::localX(unsigned x) {
  assert(x<168);

  if(x< 42) return -4.425+0.050*(x   );
  if(x< 84) return -4.425+0.050*(x+ 5);
  if(x<126) return -4.425+0.050*(x+10);
            return -4.425+0.050*(x+15);
}

double MpsSensor1Hit::localY(unsigned y) {
  assert(y<168);

  /* Old coordinate system; see meetings on 19/06/07 and 31/08/07
  if(y<84) return 4.200-0.050*(y  );
  else     return 4.200-0.050*(y+1);
  */

  if(y<84) return 0.050*(y  )-4.200;
  else     return 0.050*(y+1)-4.200;
}

unsigned MpsSensor1Hit::octant() const {
  return 4*(pixelY()/84)+(pixelX()/42);
}

bool MpsSensor1Hit::neighbour(const MpsSensor1Hit &h) const {
  if(octant()!=h.octant()) return false;

  int x0(pixelX());
  int x1(h.pixelX());
  if(abs(x0-x1)>1) return false;

  int y0(pixelY());
  int y1(h.pixelY());

  return abs(y0-y1)<=1;
}

unsigned MpsSensor1Hit::pixelX(double x) {
  if(x>=-4.450 && x<-2.350) return (unsigned)((x+4.450)/0.05)   ;
  if(x>=-2.100 && x< 0.000) return (unsigned)((x+4.450)/0.05)- 5;
  if(x>= 0.250 && x< 2.350) return (unsigned)((x+4.450)/0.05)-10;
  if(x>= 2.600 && x< 4.700) return (unsigned)((x+4.450)/0.05)-15;
  return 0xffffffff;
}

unsigned MpsSensor1Hit::pixelY(double y) {
  /* Old coordinate system; see meetings on 19/06/07 and 31/08/07
  if(y>= 0.025 && y< 4.225) return (unsigned)((4.225-y)/0.05)  ;
  if(y>=-4.225 && y<-0.025) return (unsigned)((4.225-y)/0.05)-1;
  */

  if(y>=-4.225 && y<-0.025) return (unsigned)((4.225+y)/0.05);
  if(y>= 0.025 && y< 4.225) return (unsigned)((4.225+y)/0.05)-1;
  return 0xffffffff;
}

std::ostream& MpsSensor1Hit::print(std::ostream &o, std::string s) const {
  o << s << "MpsSensor1Hit::print()" << std::endl;
  o << s << " Datum = " << printHex(_datum.word()) << std::endl;
  o << s << "  Pixel x = " << pixelX() << ", y = " << pixelY() << std::endl;
  o << s << "  Local x = " << localX() << ", y = " << localY() << std::endl;
  o << s << "  Timestamp = " << timeStamp() << std::endl;

  return o;
}

#endif
#endif
