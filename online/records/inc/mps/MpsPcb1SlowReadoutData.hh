#ifndef MpsPcb1SlowReadoutData_HH
#define MpsPcb1SlowReadoutData_HH

#include <iostream>
#include <fstream>


class MpsPcb1SlowReadoutData {

public:
  enum {
    versionNumber=0
  };

  MpsPcb1SlowReadoutData();

  unsigned temperature() const;
  void     temperature(unsigned n);

  double celcius() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _temperature;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsPcb1SlowReadoutData::MpsPcb1SlowReadoutData() {
  memset(this,0,sizeof(MpsPcb1SlowReadoutData));
}

unsigned MpsPcb1SlowReadoutData::temperature() const {
  return _temperature;
}

void MpsPcb1SlowReadoutData::temperature(unsigned n) {
  _temperature=n;
}

double MpsPcb1SlowReadoutData::celcius() const {
  return 150.0*_temperature/4800.0;
}

std::ostream& MpsPcb1SlowReadoutData::print(std::ostream &o, std::string s) const {
  o << s << "MpsPcb1SlowReadoutData::print()" << std::endl;
  o << s << " Temperature = " << temperature() << " units = "
    << celcius() << " C" << std::endl;

  return o;
}

#endif
#endif
