#ifndef MpsEudetBunchTrainDataV1_HH
#define MpsEudetBunchTrainDataV1_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"
#include "UtlTime.hh"

#include "MpsEudetBunchTrainDatum.hh"


class MpsEudetBunchTrainDataV1 {

public:
  enum {
    versionNumber=1
  };

  MpsEudetBunchTrainDataV1();

  unsigned eudetRunNumber() const;
  void     eudetRunNumber(unsigned n);

  UtlTime eudetRunTime() const;
  void    eudetRunTime(UtlTime t);

  unsigned numberOfEudetEvents() const;
  void     numberOfEudetEvents(unsigned n);

  const MpsEudetBunchTrainDatum* data() const;
        MpsEudetBunchTrainDatum* data();

  unsigned numberOfExtraBytes() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _eudetRunNumber;
  unsigned _numberOfEudetEvents;
  UtlTime _eudetRunTime;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsEudetBunchTrainDataV1::MpsEudetBunchTrainDataV1() {
  memset(this,0,sizeof(MpsEudetBunchTrainDataV1));
}

unsigned MpsEudetBunchTrainDataV1::eudetRunNumber() const {
  return _eudetRunNumber;
}

void MpsEudetBunchTrainDataV1::eudetRunNumber(unsigned n) {
  _eudetRunNumber=n;
}

UtlTime MpsEudetBunchTrainDataV1::eudetRunTime() const {
  return _eudetRunTime;
}

void MpsEudetBunchTrainDataV1::eudetRunTime(UtlTime t) {
  _eudetRunTime=t;
}

unsigned MpsEudetBunchTrainDataV1::numberOfEudetEvents() const {
  return _numberOfEudetEvents;
}

void MpsEudetBunchTrainDataV1::numberOfEudetEvents(unsigned n) {
  _numberOfEudetEvents=n;
}

const MpsEudetBunchTrainDatum* MpsEudetBunchTrainDataV1::data() const {
  return (const MpsEudetBunchTrainDatum*)(this+1);
}

MpsEudetBunchTrainDatum* MpsEudetBunchTrainDataV1::data() {
  return (MpsEudetBunchTrainDatum*)(this+1);
}

unsigned MpsEudetBunchTrainDataV1::numberOfExtraBytes() const {
  return _numberOfEudetEvents*sizeof(MpsEudetBunchTrainDatum);
}

std::ostream& MpsEudetBunchTrainDataV1::print(std::ostream &o, std::string s) const {
  o << s << "MpsEudetBunchTrainDataV1::print()" << std::endl;
  o << s << " EUDET run number = " << _eudetRunNumber << std::endl;
  o << s << " EUDET run start time = " << _eudetRunTime << std::endl;
  o << s << " Number of EUDET events = " << _numberOfEudetEvents << std::endl;

  const MpsEudetBunchTrainDatum *p(data());
  for(unsigned i(0);i<_numberOfEudetEvents;i++) {
    p[i].print(o,s+" ");
  }

  return o;
}

#endif
#endif
