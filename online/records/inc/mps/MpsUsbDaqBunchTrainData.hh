#ifndef MpsUsbDaqBunchTrainData_HH
#define MpsUsbDaqBunchTrainData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"

#include "MpsUsbDaqBunchTrainDatum.hh"


class MpsUsbDaqBunchTrainData {

public:
  enum {
    versionNumber=0
  };

  MpsUsbDaqBunchTrainData();

  unsigned numberOfTries() const;
  void     numberOfTries(unsigned n);

  unsigned short initialStatus() const;
  void           initialStatus(unsigned short s);

  unsigned short finalStatus() const;
  void           finalStatus(unsigned short s);

  unsigned numberOfTags() const;
  void     numberOfTags(unsigned n);

  const MpsUsbDaqBunchTrainDatum* data() const;
        MpsUsbDaqBunchTrainDatum* data();

  std::vector<unsigned> tagVector() const;
  void                  tagVector(const std::vector<unsigned> &v);

  std::vector<MpsUsbDaqBunchTrainDatum> tagStarts() const;

  unsigned numberOfExtraBytes() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _numberOfTries;
  unsigned _numberOfTags;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsUsbDaqBunchTrainData::MpsUsbDaqBunchTrainData() {
  memset(this,0,sizeof(MpsUsbDaqBunchTrainData));
}

unsigned MpsUsbDaqBunchTrainData::numberOfTries() const {
  return _numberOfTries.word();
}

void MpsUsbDaqBunchTrainData::numberOfTries(unsigned n) {
  _numberOfTries.word(n);
}

unsigned short MpsUsbDaqBunchTrainData::initialStatus() const {
  return _numberOfTries.halfWord(0);
}

void MpsUsbDaqBunchTrainData::initialStatus(unsigned short s) {
  _numberOfTries.halfWord(0,s);
}

unsigned short MpsUsbDaqBunchTrainData::finalStatus() const {
  return _numberOfTries.halfWord(1);
}

void MpsUsbDaqBunchTrainData::finalStatus(unsigned short s) {
  _numberOfTries.halfWord(1,s);
}

unsigned MpsUsbDaqBunchTrainData::numberOfTags() const {
  return _numberOfTags;
}

void MpsUsbDaqBunchTrainData::numberOfTags(unsigned n) {
  _numberOfTags=n;
}

const MpsUsbDaqBunchTrainDatum* MpsUsbDaqBunchTrainData::data() const {
  return (const MpsUsbDaqBunchTrainDatum*)(this+1);
}

MpsUsbDaqBunchTrainDatum* MpsUsbDaqBunchTrainData::data() {
  return (MpsUsbDaqBunchTrainDatum*)(this+1);
}

std::vector<unsigned> MpsUsbDaqBunchTrainData::tagVector() const {
  std::vector<unsigned> v;
  const unsigned *p((const unsigned*)(this+1));
  for(unsigned i(0);i<_numberOfTags;i++) {
    v.push_back(p[i]);
  }
  return v;
}
 
void MpsUsbDaqBunchTrainData::tagVector(const std::vector<unsigned> &v) {
  unsigned *p((unsigned*)(this+1));
  for(unsigned i(0);i<v.size();i++) {
    p[i]=v[i];
  }
  _numberOfTags=v.size();
}

std::vector<MpsUsbDaqBunchTrainDatum> MpsUsbDaqBunchTrainData::tagStarts() const {
  std::vector<MpsUsbDaqBunchTrainDatum> v;
  const MpsUsbDaqBunchTrainDatum *p(data());

  unsigned nSequence(0);
  for(unsigned i(0);i<_numberOfTags;i++) {

    // End of sequence
    if(p[i].channels()==0) {
      if(v.size()>0) {
	assert(nSequence>0);
	nSequence--;
	if(nSequence>7) nSequence=7;
	v[v.size()-1].fullTimeStamp(8*v[v.size()-1].timeStamp()+nSequence);
	nSequence=0;
      }

    } else {

      // New sequence
      if(nSequence==0) {
	  v.push_back(p[i]);
	  nSequence=1;

      // Continuation of sequence
      } else {
	v[v.size()-1].channels(v[v.size()-1].channels() | p[i].channels());
	nSequence++;
      }
    }
  }

  return v;
}

unsigned MpsUsbDaqBunchTrainData::numberOfExtraBytes() const {
  return _numberOfTags*sizeof(MpsUsbDaqBunchTrainDatum);
}

std::ostream& MpsUsbDaqBunchTrainData::print(std::ostream &o, std::string s) const {
  o << s << "MpsUsbDaqBunchTrainData::print()" << std::endl;
  o << s << " TPAC1.0 Number of tries = " << _numberOfTries.word() << std::endl;
  o << s << " TPAC1.2 Status words    = " << printHex(_numberOfTries) << std::endl;

  o << s << "  Initial status = " << printHex(initialStatus()) << std::endl;
  if((initialStatus()&(1<<0))!=0) o << s << "   Spill active" << std::endl;
  if((initialStatus()&(1<<1))!=0) o << s << "   Buffer overflow" << std::endl;
  if((initialStatus()&(1<<2))!=0) o << s << "   Buffer empty" << std::endl;
  if((initialStatus()&(1<<3))!=0) o << s << "   JTAG TDO" << std::endl;
  if((initialStatus()&(1<<4))!=0) o << s << "   JTAG busy" << std::endl;
  if((initialStatus()&(1<<5))!=0) o << s << "   Spill busy" << std::endl;

  o << s << "  Final   status = " << printHex(finalStatus()) << std::endl;
  if((finalStatus()&(1<<0))!=0) o << s << "   Spill active" << std::endl;
  if((finalStatus()&(1<<1))!=0) o << s << "   Buffer overflow" << std::endl;
  if((finalStatus()&(1<<2))!=0) o << s << "   Buffer empty" << std::endl;
  if((finalStatus()&(1<<3))!=0) o << s << "   JTAG TDO" << std::endl;
  if((finalStatus()&(1<<4))!=0) o << s << "   JTAG busy" << std::endl;
  if((finalStatus()&(1<<5))!=0) o << s << "   Spill busy" << std::endl;


  o << s << " Number of tags = " << _numberOfTags << std::endl;

  /*
  const unsigned *p((const unsigned*)(this+1));
  for(unsigned i(0);i<_numberOfTags;i++) {
    o << s << "  Tag " << std::setw(4) << i << " = "
      << printHex(p[i]) << std::endl;
  }
  */

  const MpsUsbDaqBunchTrainDatum *p(data());
  for(unsigned i(0);i<_numberOfTags;i++) {
    p[i].print(o,s+" ");
  }

  return o;
}

#endif
#endif
