#ifndef MpsEudetBunchTrainDataV0_HH
#define MpsEudetBunchTrainDataV0_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"

#include "MpsEudetBunchTrainDatum.hh"


class MpsEudetBunchTrainDataV0 {

public:
  enum {
    versionNumber=0
  };

  MpsEudetBunchTrainDataV0();

  unsigned eudetRunNumber() const;
  void     eudetRunNumber(unsigned n);

  unsigned numberOfEudetEvents() const;
  void     numberOfEudetEvents(unsigned n);

  const MpsEudetBunchTrainDatum* data() const;
        MpsEudetBunchTrainDatum* data();

  unsigned numberOfExtraBytes() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _eudetRunNumber;
  unsigned _numberOfEudetEvents;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsEudetBunchTrainDataV0::MpsEudetBunchTrainDataV0() {
  memset(this,0,sizeof(MpsEudetBunchTrainDataV0));
}

unsigned MpsEudetBunchTrainDataV0::eudetRunNumber() const {
  return _eudetRunNumber;
}

void MpsEudetBunchTrainDataV0::eudetRunNumber(unsigned n) {
  _eudetRunNumber=n;
}

unsigned MpsEudetBunchTrainDataV0::numberOfEudetEvents() const {
  return _numberOfEudetEvents;
}

void MpsEudetBunchTrainDataV0::numberOfEudetEvents(unsigned n) {
  _numberOfEudetEvents=n;
}

const MpsEudetBunchTrainDatum* MpsEudetBunchTrainDataV0::data() const {
  return (const MpsEudetBunchTrainDatum*)(this+1);
}

MpsEudetBunchTrainDatum* MpsEudetBunchTrainDataV0::data() {
  return (MpsEudetBunchTrainDatum*)(this+1);
}

unsigned MpsEudetBunchTrainDataV0::numberOfExtraBytes() const {
  return _numberOfEudetEvents*sizeof(MpsEudetBunchTrainDatum);
}

std::ostream& MpsEudetBunchTrainDataV0::print(std::ostream &o, std::string s) const {
  o << s << "MpsEudetBunchTrainDataV0::print()" << std::endl;
  o << s << " EUDET run number = " << _eudetRunNumber << std::endl;
  o << s << " Number of EUDET events = " << _numberOfEudetEvents << std::endl;

  const MpsEudetBunchTrainDatum *p(data());
  for(unsigned i(0);i<_numberOfEudetEvents;i++) {
    p[i].print(o,s+" ");
  }

  return o;
}

#endif
#endif
