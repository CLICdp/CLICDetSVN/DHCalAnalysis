#ifndef MpsEudetBunchTrainDatum_HH
#define MpsEudetBunchTrainDatum_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class MpsEudetBunchTrainDatum {

public:
  MpsEudetBunchTrainDatum();
  MpsEudetBunchTrainDatum(unsigned d);

  unsigned short eudetEventNumber() const;
  void           eudetEventNumber(unsigned short n);

  unsigned timeStamp() const;
  void     timeStamp(unsigned t);

  unsigned short fullTimeStamp() const;
  void           fullTimeStamp(unsigned short t);

  void datum(unsigned d);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _datum;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsEudetBunchTrainDatum::MpsEudetBunchTrainDatum() {
}

MpsEudetBunchTrainDatum::MpsEudetBunchTrainDatum(unsigned d) : _datum(d) {
}

unsigned short MpsEudetBunchTrainDatum::eudetEventNumber() const {
  return _datum.halfWord(0);
}

void MpsEudetBunchTrainDatum::eudetEventNumber(unsigned short n) {
  return _datum.halfWord(0,n);
}

unsigned short MpsEudetBunchTrainDatum::fullTimeStamp() const {
  return 8*timeStamp();
}

void MpsEudetBunchTrainDatum::fullTimeStamp(unsigned short t) {
  timeStamp(t/8);
}

unsigned MpsEudetBunchTrainDatum::timeStamp() const {
  return _datum.halfWord(1);
}

void MpsEudetBunchTrainDatum::timeStamp(unsigned t) {
  assert(t<8192);
  _datum.halfWord(1,t);
}

void MpsEudetBunchTrainDatum::datum(unsigned d) {
  _datum.word(d);
}

std::ostream& MpsEudetBunchTrainDatum::print(std::ostream &o, std::string s) const {
  o << s << "MpsEudetBunchTrainDatum::print()" << std::endl;

  o << s << " Datum = " << printHex(_datum.word()) << std::endl;
  o << s << "  Timestamp = " << timeStamp()
    << ", EUDET event number (lowest 15 bits) = " << eudetEventNumber() << std::endl;

  return o;
}

#endif
#endif
