#ifndef MpsPcb1ConfigurationData_HH
#define MpsPcb1ConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class MpsPcb1ConfigurationData {

public:
  enum {
    versionNumber=0
  };

  MpsPcb1ConfigurationData();

  void setV10Defaults();
  void setV11Defaults();
  void setV12Defaults();
  
  // The following control the test structure pixels

  unsigned short vDebugThPos() const;
  void           vDebugThPos(unsigned short d);

  unsigned short vDebugThNeg() const;
  void           vDebugThNeg(unsigned short d);
  /*  
  int            vDebugTh() const;
  void           vDebugTh(int d);
  */
  int            debugThresholdValue() const;
  unsigned       debugThresholdCommonMode() const;
  void           debugThreshold(int t, unsigned c=3584);

  unsigned short iDebugSFBias() const;
  void           iDebugSFBias(unsigned short d);


  // The following control all the bulk pixels
  
  unsigned short iSenseColRef() const;
  void           iSenseColRef(unsigned short d);

  unsigned short iSenseBias() const;
  void           iSenseBias(unsigned short d);

  unsigned short iSenseOutBias() const;
  void           iSenseOutBias(unsigned short d);

  unsigned short iSenseCompBias() const;
  void           iSenseCompBias(unsigned short d);


  // The following control the shaper pixels
  
  unsigned short v12ThPos() const;
  void           v12ThPos(unsigned short d);

  unsigned short v12ThNeg() const;
  void           v12ThNeg(unsigned short d);
  /*
  int            v12Th() const;
  void           v12Th(int d);
  */
  int            region01ThresholdValue() const;
  unsigned       region01ThresholdCommonMode() const;
  void           region01Threshold(int t, unsigned c=2048);
  int            shaperThresholdValue() const;
  unsigned       shaperThresholdCommonMode() const;
  void           shaperThreshold(int t, unsigned c=2048);

  int            region23ThresholdValue() const;
  unsigned       region23ThresholdCommonMode() const;
  void           region23Threshold(int t, unsigned c=2048);
  int            samplerThresholdValue() const;
  unsigned       samplerThresholdCommonMode() const;
  void           samplerThreshold(int t, unsigned c=2048);

  int            regionThresholdValue(unsigned r) const;

  unsigned short i12CompBias1() const;
  void           i12CompBias1(unsigned short d);

  unsigned short i12CompBias2() const;
  void           i12CompBias2(unsigned short d);

  unsigned short i12CompBiasTrim() const;
  void           i12CompBiasTrim(unsigned short d);

  unsigned short i12PreAmpBias() const;
  void           i12PreAmpBias(unsigned short d);

  unsigned short v12PreAmpCasc() const;
  void           v12PreAmpCasc(unsigned short d);

  unsigned short i12OutBias() const;
  void           i12OutBias(unsigned short d);

  unsigned short i12ShaperBias() const;
  void           i12ShaperBias(unsigned short d);

  unsigned short i12MSOBias1() const;
  void           i12MSOBias1(unsigned short d);
  
  unsigned short v12ShaperCasc() const;
  void           v12ShaperCasc(unsigned short d);


  // The following control the sampler pixels

  unsigned short v34Reset() const;
  void           v34Reset(unsigned short d);
  
  unsigned short v34ThNeg() const;
  void           v34ThNeg(unsigned short d);

  unsigned short v34ThPos() const;
  void           v34ThPos(unsigned short d);
  /*
  int            v34Th() const;
  void           v34Th(int d);
  */
  unsigned short i34CompBias1() const;
  void           i34CompBias1(unsigned short d);

  unsigned short i34CompBias2() const;
  void           i34CompBias2(unsigned short d);

  unsigned short i34CompBiasTrim() const;
  void           i34CompBiasTrim(unsigned short d);
  
  unsigned short i34PreAmpBias() const;
  void           i34PreAmpBias(unsigned short d);

  unsigned short v34PreAmpCasc() const;
  void           v34PreAmpCasc(unsigned short d);

  unsigned short i34OutBias() const;
  void           i34OutBias(unsigned short d);

  unsigned short i34OutSFBias() const;
  void           i34OutSFBias(unsigned short d);

  unsigned short i34SFBias() const;
  void           i34SFBias(unsigned short d);

  unsigned short i34MSOBias1() const;
  void           i34MSOBias1(unsigned short d);

  unsigned short i34MSOBias2() const;
  void           i34MSOBias2(unsigned short d);


  // This is currently unused
  
  unsigned short unconnected() const;
  void           unconnected(unsigned short d);
  
  unsigned short dac(unsigned n) const;
  void           dac(unsigned n, unsigned short d);


  // Some manipulation methods

  bool operator==(const MpsPcb1ConfigurationData &d) const;
  bool operator!=(const MpsPcb1ConfigurationData &d) const;

  void writeFile(const std::string &fileName);
  void readFile(const std::string &fileName);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:

  // Direct copy from the header of
  // CaliceMaps1TestSystemInterface

  enum DAC_CHANNEL_DEFS{
    I34_OUT_BIAS=0,    //  0
    I34_MSO_BIAS2,     //
    I34_MSO_BIAS1,     //
    I34_COMP_BIAS2,    //
    I34_PRE_BIAS,      //
    I34_OUTSF_BIAS,    //
    I34_COMP_BIAS1,    //
    I34_SF_BIAS,       //
    I34_COMP_BIAS_TRIM,//  8
    DEBUGSF_BIAS,      //
    ISENSE_COLREF,     //
    I12_COMP_BIAS_TRIM,//
    I12_SHAPER_BIAS,   //
    I12_PRE_BIAS,      //
    I12_COMP2_BIAS,    //
    I12_MSO_BIAS1,     //
    I12_IOUT_BIAS,     // 16
    ISENSE_COMP_BIAS,  //
    ISENSE_IOUT_BIAS,  //
    I12_COMP1_BIAS,    //
    ISENSE_BIAS,       //
    VTH_34_NEG,        //
    DEBUG_VTH_NEG,     //
    VTH_12_NEG,        //
    VRST,              // 24
    VTH_34_POS,        //
    PREAMP_VCASC_34,   //
    DEBUG_VTH_POS,     //
    SHAPE_VCASC_12,    //
    PREAMP_VCASC_12,   //
    VTH_12_POS,        //
    UNCONNECTED        //
  };

  UtlPack _dac[16];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsPcb1ConfigurationData::MpsPcb1ConfigurationData() {
  memset(this,0,sizeof(MpsPcb1ConfigurationData));
  for(unsigned i(0);i<31;i++) {
    dac(i,0x0800);
  }

  debugThreshold(300);
  shaperThreshold(120);
  //samplerThreshold(300);
  samplerThreshold(300);

  i12MSOBias1(2300);
  i34MSOBias1(2300);
  //i12MSOBias1(3500);
  //i34MSOBias1(3500);
  //i34MSOBias2(2200);
  i34MSOBias2(1900);
  //i34MSOBias2(3000);

  /*
  //iSenseColRef(3800);  
  //iSenseBias(3500);

  i34MSOBias1(2048-128);

  i12ShaperBias(2048-256);
  shaper12VCasc(2048-128);

  i12PreAmpBias(2048+512);
  i34PreAmpBias(2048-256);
  i12CompBias2(2048-512);
  i34CompBias2(2048+512);
  */

  // New values from Jamie Crooks 13/11/07
  i34CompBias2(830);
  i34PreAmpBias(1030);
  i34OutSFBias(630);
  i34CompBias1(3100);
  i34SFBias(2450);
  i34CompBiasTrim(3000);

  iDebugSFBias(3000);

  i12CompBiasTrim(3000);
  i12ShaperBias(290);
  i12PreAmpBias(290);
  i12CompBias2(830);
  i12CompBias1(3100);

  //v34Reset(2275);
  v34Reset(2450); // 26/11/07
  v34PreAmpCasc(3400);

  v12ShaperCasc(3400);
  v12PreAmpCasc(1800);

  //setV10Defaults();
  //setV11Defaults();
  setV12Defaults();
}

void MpsPcb1ConfigurationData::setV10Defaults() {
  // ??????
  iSenseCompBias(830);
  iSenseColRef(3500);
  iSenseBias(3500);
}

void MpsPcb1ConfigurationData::setV11Defaults() {
  iSenseCompBias(830);
  iSenseColRef(3500);
  iSenseBias(3500);
}

void MpsPcb1ConfigurationData::setV12Defaults() {
  iSenseCompBias(830);
  iSenseColRef(3500);
  iSenseBias(3500);
}

unsigned short MpsPcb1ConfigurationData::vDebugThPos() const {
  return _dac[DEBUG_VTH_POS/2].halfWord(DEBUG_VTH_POS%2);
}

void MpsPcb1ConfigurationData::vDebugThPos(unsigned short d) {
  assert(d<4096);
  _dac[DEBUG_VTH_POS/2].halfWord(DEBUG_VTH_POS%2,d);
}

unsigned short MpsPcb1ConfigurationData::vDebugThNeg() const {
  return _dac[DEBUG_VTH_NEG/2].halfWord(DEBUG_VTH_NEG%2);
}

void MpsPcb1ConfigurationData::vDebugThNeg(unsigned short d) {
  assert(d<4096);
  _dac[DEBUG_VTH_NEG/2].halfWord(DEBUG_VTH_NEG%2,d);
}
/*
int MpsPcb1ConfigurationData::vDebugTh() const {
  return (int)vDebugThPos()-(int)vDebugThNeg();
}
*/
int MpsPcb1ConfigurationData::debugThresholdValue() const {
  return (int)vDebugThPos()-(int)vDebugThNeg();
}

unsigned MpsPcb1ConfigurationData::debugThresholdCommonMode() const {
  return (vDebugThPos()+vDebugThNeg())/2;
}

void MpsPcb1ConfigurationData::debugThreshold(int v, unsigned d) {
  assert(v>-4096 && v<4096);
  assert(d<4096);

  if(v>0) {
    vDebugThPos(d-v/2+v);
    vDebugThNeg(d-v/2);
  } else {
    vDebugThPos(d+v/2);
    vDebugThNeg(d+v/2-v);
  }
}

unsigned short MpsPcb1ConfigurationData::v12ThPos() const {
  return _dac[VTH_12_POS/2].halfWord(VTH_12_POS%2);
}

void MpsPcb1ConfigurationData::v12ThPos(unsigned short d) {
  assert(d<4096);
  _dac[VTH_12_POS/2].halfWord(VTH_12_POS%2,d);
}

unsigned short MpsPcb1ConfigurationData::v12ThNeg() const {
  return _dac[VTH_12_NEG/2].halfWord(VTH_12_NEG%2);
}

void MpsPcb1ConfigurationData::v12ThNeg(unsigned short d) {
  assert(d<4096);
  _dac[VTH_12_NEG/2].halfWord(VTH_12_NEG%2,d);
}
/*
int MpsPcb1ConfigurationData::v12Th() const {
  return (int)v12ThPos()-(int)v12ThNeg();
}
*/
int MpsPcb1ConfigurationData::region01ThresholdValue() const {
  return shaperThresholdValue();
}

unsigned MpsPcb1ConfigurationData::region01ThresholdCommonMode() const {
  return shaperThresholdCommonMode();
}

void MpsPcb1ConfigurationData::region01Threshold(int v, unsigned d) {
  shaperThreshold(v,d);
}

int MpsPcb1ConfigurationData::shaperThresholdValue() const {
  return (int)v12ThPos()-(int)v12ThNeg();
}

unsigned MpsPcb1ConfigurationData::shaperThresholdCommonMode() const {
  return (v12ThPos()+v12ThNeg())/2;
}

void MpsPcb1ConfigurationData::shaperThreshold(int v, unsigned d) {
  assert(v>-4096 && v<4096);
  assert(d<4096);

  if(v>0) {
    v12ThPos(d-v/2+v);
    v12ThNeg(d-v/2);
  } else {
    v12ThPos(d+v/2);
    v12ThNeg(d+v/2-v);
  }
}

int MpsPcb1ConfigurationData::region23ThresholdValue() const {
  return samplerThresholdValue();
}

unsigned MpsPcb1ConfigurationData::region23ThresholdCommonMode() const {
  return samplerThresholdCommonMode();
}

void MpsPcb1ConfigurationData::region23Threshold(int v, unsigned d) {
  samplerThreshold(v,d);
}

int MpsPcb1ConfigurationData::samplerThresholdValue() const {
  return (int)v34ThPos()-(int)v34ThNeg();
}

unsigned MpsPcb1ConfigurationData::samplerThresholdCommonMode() const {
  return (v34ThPos()+v34ThNeg())/2;
}

void MpsPcb1ConfigurationData::samplerThreshold(int v, unsigned d) {
  assert(v>-4096 && v<4096);
  assert(d<4096);

  if(v>0) {
    v34ThPos(d-v/2+v);
    v34ThNeg(d-v/2);
  } else {
    v34ThPos(d+v/2);
    v34ThNeg(d+v/2-v);
  }
}

int MpsPcb1ConfigurationData::regionThresholdValue(unsigned r) const {
  assert(r<4);
  if(r<2) return region01ThresholdValue();
  else    return region23ThresholdValue();
}

unsigned short MpsPcb1ConfigurationData::v34ThPos() const {
  return _dac[VTH_34_POS/2].halfWord(VTH_34_POS%2);
}

void MpsPcb1ConfigurationData::v34ThPos(unsigned short d) {
  assert(d<4096);
  _dac[VTH_34_POS/2].halfWord(VTH_34_POS%2,d);
}

unsigned short MpsPcb1ConfigurationData::v34ThNeg() const {
  return _dac[VTH_34_NEG/2].halfWord(VTH_34_NEG%2);
}

void MpsPcb1ConfigurationData::v34ThNeg(unsigned short d) {
  assert(d<4096);
  _dac[VTH_34_NEG/2].halfWord(VTH_34_NEG%2,d);
}
/*
int MpsPcb1ConfigurationData::v34Th() const {
  return (int)v34ThPos()-(int)v34ThNeg();
}
*/
unsigned short MpsPcb1ConfigurationData::v34Reset() const {
  return _dac[VRST/2].halfWord(VRST%2);
}

void MpsPcb1ConfigurationData::v34Reset(unsigned short d) {
  assert(d<4096);
  _dac[VRST/2].halfWord(VRST%2,d);
}

unsigned short MpsPcb1ConfigurationData::iSenseColRef() const {
  return _dac[ISENSE_COLREF/2].halfWord(ISENSE_COLREF%2);
}

void MpsPcb1ConfigurationData::iSenseColRef(unsigned short d) {
  assert(d<4096);
  _dac[ISENSE_COLREF/2].halfWord(ISENSE_COLREF%2,d);
}

unsigned short MpsPcb1ConfigurationData::v34PreAmpCasc() const {
  return _dac[PREAMP_VCASC_34/2].halfWord(PREAMP_VCASC_34%2);
}

void MpsPcb1ConfigurationData::v34PreAmpCasc(unsigned short d) {
  assert(d<4096);
  _dac[PREAMP_VCASC_34/2].halfWord(PREAMP_VCASC_34%2,d);
}

unsigned short MpsPcb1ConfigurationData::iSenseBias() const {
  return _dac[ISENSE_BIAS/2].halfWord(ISENSE_BIAS%2);
}

void MpsPcb1ConfigurationData::iSenseBias(unsigned short d) {
  assert(d<4096);
  _dac[ISENSE_BIAS/2].halfWord(ISENSE_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12CompBias1() const {
  return _dac[I12_COMP1_BIAS/2].halfWord(I12_COMP1_BIAS%2);
}

void MpsPcb1ConfigurationData::i12CompBias1(unsigned short d) {
  assert(d<4096);
  _dac[I12_COMP1_BIAS/2].halfWord(I12_COMP1_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12CompBias2() const {
  return _dac[I12_COMP2_BIAS/2].halfWord(I12_COMP2_BIAS%2);
}

void MpsPcb1ConfigurationData::i12CompBias2(unsigned short d) {
  assert(d<4096);
  _dac[I12_COMP2_BIAS/2].halfWord(I12_COMP2_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::iSenseOutBias() const {
  return _dac[ISENSE_IOUT_BIAS/2].halfWord(ISENSE_IOUT_BIAS%2);
}

void MpsPcb1ConfigurationData::iSenseOutBias(unsigned short d) {
  assert(d<4096);
  _dac[ISENSE_IOUT_BIAS/2].halfWord(ISENSE_IOUT_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::iSenseCompBias() const {
  return _dac[ISENSE_COMP_BIAS/2].halfWord(ISENSE_COMP_BIAS%2);
}

void MpsPcb1ConfigurationData::iSenseCompBias(unsigned short d) {
  assert(d<4096);
  _dac[ISENSE_COMP_BIAS/2].halfWord(ISENSE_COMP_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12OutBias() const {
  return _dac[I12_IOUT_BIAS/2].halfWord(I12_IOUT_BIAS%2);
}

void MpsPcb1ConfigurationData::i12OutBias(unsigned short d) {
  assert(d<4096);
  _dac[I12_IOUT_BIAS/2].halfWord(I12_IOUT_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12PreAmpBias() const {
  return _dac[I12_PRE_BIAS/2].halfWord(I12_PRE_BIAS%2);
}

void MpsPcb1ConfigurationData::i12PreAmpBias(unsigned short d) {
  assert(d<4096);
  _dac[I12_PRE_BIAS/2].halfWord(I12_PRE_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12ShaperBias() const {
  return _dac[I12_SHAPER_BIAS/2].halfWord(I12_SHAPER_BIAS%2);
}

void MpsPcb1ConfigurationData::i12ShaperBias(unsigned short d) {
  assert(d<4096);
  _dac[I12_SHAPER_BIAS/2].halfWord(I12_SHAPER_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12CompBiasTrim() const {
  return _dac[I12_COMP_BIAS_TRIM/2].halfWord(I12_COMP_BIAS_TRIM%2);
}

void MpsPcb1ConfigurationData::i12CompBiasTrim(unsigned short d) {
  assert(d<4096);
  _dac[I12_COMP_BIAS_TRIM/2].halfWord(I12_COMP_BIAS_TRIM%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34CompBiasTrim() const {
  return _dac[I34_COMP_BIAS_TRIM/2].halfWord(I34_COMP_BIAS_TRIM%2);
}

void MpsPcb1ConfigurationData::i34CompBiasTrim(unsigned short d) {
  assert(d<4096);
  _dac[I34_COMP_BIAS_TRIM/2].halfWord(I34_COMP_BIAS_TRIM%2,d);
}

unsigned short MpsPcb1ConfigurationData::iDebugSFBias() const {
  return _dac[DEBUGSF_BIAS/2].halfWord(DEBUGSF_BIAS%2);
}

void MpsPcb1ConfigurationData::iDebugSFBias(unsigned short d) {
  assert(d<4096);
  _dac[DEBUGSF_BIAS/2].halfWord(DEBUGSF_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34SFBias() const {
  return _dac[I34_SF_BIAS/2].halfWord(I34_SF_BIAS%2);
}

void MpsPcb1ConfigurationData::i34SFBias(unsigned short d) {
  assert(d<4096);
  _dac[I34_SF_BIAS/2].halfWord(I34_SF_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34CompBias1() const {
  return _dac[I34_COMP_BIAS1/2].halfWord(I34_COMP_BIAS1%2);
}

void MpsPcb1ConfigurationData::i34CompBias1(unsigned short d) {
  assert(d<4096);
  _dac[I34_COMP_BIAS1/2].halfWord(I34_COMP_BIAS1%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34CompBias2() const {
  return _dac[I34_COMP_BIAS2/2].halfWord(I34_COMP_BIAS2%2);
}

void MpsPcb1ConfigurationData::i34CompBias2(unsigned short d) {
  assert(d<4096);
  _dac[I34_COMP_BIAS2/2].halfWord(I34_COMP_BIAS2%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34MSOBias1() const {
  return _dac[I34_MSO_BIAS1/2].halfWord(I34_MSO_BIAS1%2);
}

void MpsPcb1ConfigurationData::i34MSOBias1(unsigned short d) {
  assert(d<4096);
  _dac[I34_MSO_BIAS1/2].halfWord(I34_MSO_BIAS1%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34MSOBias2() const {
  return _dac[I34_MSO_BIAS2/2].halfWord(I34_MSO_BIAS2%2);
}

void MpsPcb1ConfigurationData::i34MSOBias2(unsigned short d) {
  assert(d<4096);
  _dac[I34_MSO_BIAS2/2].halfWord(I34_MSO_BIAS2%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34PreAmpBias() const {
  return _dac[I34_PRE_BIAS/2].halfWord(I34_PRE_BIAS%2);
}

void MpsPcb1ConfigurationData::i34PreAmpBias(unsigned short d) {
  assert(d<4096);
  _dac[I34_PRE_BIAS/2].halfWord(I34_PRE_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34OutBias() const {
  return _dac[I34_OUT_BIAS/2].halfWord(I34_OUT_BIAS%2);
}

void MpsPcb1ConfigurationData::i34OutBias(unsigned short d) {
  assert(d<4096);
  _dac[I34_OUT_BIAS/2].halfWord(I34_OUT_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::i34OutSFBias() const {
  return _dac[I34_OUTSF_BIAS/2].halfWord(I34_OUTSF_BIAS%2);
}

void MpsPcb1ConfigurationData::i34OutSFBias(unsigned short d) {
  assert(d<4096);
  _dac[I34_OUTSF_BIAS/2].halfWord(I34_OUTSF_BIAS%2,d);
}

unsigned short MpsPcb1ConfigurationData::v12ShaperCasc() const {
  return _dac[SHAPE_VCASC_12/2].halfWord(SHAPE_VCASC_12%2);
}

void MpsPcb1ConfigurationData::v12ShaperCasc(unsigned short d) {
  assert(d<4096);
  _dac[SHAPE_VCASC_12/2].halfWord(SHAPE_VCASC_12%2,d);
}

unsigned short MpsPcb1ConfigurationData::v12PreAmpCasc() const {
  return _dac[PREAMP_VCASC_12/2].halfWord(PREAMP_VCASC_12%2);
}

void MpsPcb1ConfigurationData::v12PreAmpCasc(unsigned short d) {
  assert(d<4096);
  _dac[PREAMP_VCASC_12/2].halfWord(PREAMP_VCASC_12%2,d);
}

unsigned short MpsPcb1ConfigurationData::i12MSOBias1() const {
  return _dac[I12_MSO_BIAS1/2].halfWord(I12_MSO_BIAS1%2);
}

void MpsPcb1ConfigurationData::i12MSOBias1(unsigned short d) {
  assert(d<4096);
  _dac[I12_MSO_BIAS1/2].halfWord(I12_MSO_BIAS1%2,d);
}

unsigned short MpsPcb1ConfigurationData::unconnected() const {
  return _dac[UNCONNECTED/2].halfWord(UNCONNECTED%2);
}

void MpsPcb1ConfigurationData::unconnected(unsigned short d) {
  assert(d<4096);
  _dac[UNCONNECTED/2].halfWord(UNCONNECTED%2,d);
}

unsigned short MpsPcb1ConfigurationData::dac(unsigned n) const {
  assert(n<32);
  return _dac[n/2].halfWord(n%2);
}

void MpsPcb1ConfigurationData::dac(unsigned n, unsigned short d) {
  assert(n<32);
  assert(d<4096);
  _dac[n/2].halfWord(n%2,d);
}

bool MpsPcb1ConfigurationData::operator!=(const MpsPcb1ConfigurationData &d) const {
  for(unsigned i(0);i<16;i++) {
    if(_dac[i]!=d._dac[i]) return true;
  }  
  return false;
}

bool MpsPcb1ConfigurationData::operator==(const MpsPcb1ConfigurationData &d) const {
  return !(this->operator!=(d));
}

void MpsPcb1ConfigurationData::writeFile(const std::string &fileName) {
  std::ofstream fout(fileName.c_str());
  if(fout) {

    for(unsigned i(0);i<32;i++) {
      fout << std::setw(2) << i << " " << dac(i) << std::endl;
    }
  }
}

void MpsPcb1ConfigurationData::readFile(const std::string &fileName) {
  std::ifstream fin(fileName.c_str());
  if(fin) {

    unsigned n;
    unsigned short v;
    for(unsigned i(0);i<32;i++) {
      fin >> n >> v;
      if(n==i) dac(n,v);
    }
  }
}

std::ostream& MpsPcb1ConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "MpsPcb1ConfigurationData::print()" << std::endl;

  for(unsigned i(0);i<4;i++) {
    o << s << " DACs " << std::setw(2) << 8*i << "-"
      << std::setw(2) << 8*i+7 << " =";

    for(unsigned j(8*i);j<8*(i+1);j++) {
      o << std::setw(6) << dac(j);
    }
    
    o << std::endl;
  }
  o << s << " Test str: Threshold pos " << std::setw(4) << vDebugThPos()
    << ", neg " << std::setw(4) << vDebugThNeg() 
    << ", value " << std::setw(5) << debugThresholdValue()
    << ", common mode " << std::setw(4) << debugThresholdCommonMode() << std::endl;

  o << s << " Shaper:   Threshold pos " << std::setw(4) << v12ThPos()
    << ", neg " << std::setw(4) << v12ThNeg() 
    << ", value " << std::setw(5) << region01ThresholdValue()
    << ", common mode " << std::setw(4) << region01ThresholdCommonMode() << std::endl;
  o << s << " Shaper:   Monostable output hit bias " << std::setw(4) << i12MSOBias1() << std::endl;

  o << s << " Sampler:  Threshold pos " << std::setw(4) << v34ThPos()
    << ", neg " << std::setw(4) << v34ThNeg() 
    << ", value " << std::setw(5) << region23ThresholdValue()
    << ", common mode " << std::setw(4) << region23ThresholdCommonMode() << std::endl;
  o << s << " Sampler:  Monostable output hit bias " << std::setw(4) << i34MSOBias1()
    << ", reset bias " << std::setw(4) << i34MSOBias2() << std::endl;

  return o;
}

#endif
#endif
