#ifndef IlcRunStart_HH
#define IlcRunStart_HH

#include <string>
#include <iostream>

#include "UtlTime.hh"

#include "IlcRunType.hh"


class IlcRunStart {

public:
  enum {
    versionNumber=0
  };

  IlcRunStart();

  void reset();
  
  unsigned runNumber() const;
  void     runNumber(unsigned n);

  IlcRunType runType() const;
  void          runType(IlcRunType n);

  unsigned maximumNumberOfConfigurationsInRun() const;
  void     maximumNumberOfConfigurationsInRun(unsigned m);

  unsigned maximumNumberOfBunchTrainsInRun() const;
  void     maximumNumberOfBunchTrainsInRun(unsigned m);

  UtlTimeDifference maximumTimeOfRun() const;
  void              maximumTimeOfRun(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  IlcRunType _runType;
  unsigned _runNumber;
  unsigned _maximumNumberOfConfigurationsInRun;
  unsigned _maximumNumberOfBunchTrainsInRun;
  UtlTimeDifference _maximumTimeOfRun;
};


#ifdef CALICE_DAQ_ICC


IlcRunStart::IlcRunStart() {
  reset();
}

void IlcRunStart::reset() {
  _runNumber=0;
  _runType.type(IlcRunType::daqTest);
  _runType.version(0);
  _maximumNumberOfConfigurationsInRun=0xffffffff;
  _maximumNumberOfBunchTrainsInRun=0xffffffff;
  _maximumTimeOfRun=UtlTimeDifference(0x7fffffff,999999);
}

unsigned IlcRunStart::runNumber() const {
  return _runNumber;
}

void IlcRunStart::runNumber(unsigned n) {
  _runNumber=n;
}

IlcRunType IlcRunStart::runType() const {
  return _runType;
}

void IlcRunStart::runType(IlcRunType n) {
  _runType=n;
}

unsigned IlcRunStart::maximumNumberOfConfigurationsInRun() const {
  return _maximumNumberOfConfigurationsInRun;
}

void IlcRunStart::maximumNumberOfConfigurationsInRun(unsigned m) {
  _maximumNumberOfConfigurationsInRun=m;
}

unsigned IlcRunStart::maximumNumberOfBunchTrainsInRun() const {
  return _maximumNumberOfBunchTrainsInRun;
}

void IlcRunStart::maximumNumberOfBunchTrainsInRun(unsigned m) {
  _maximumNumberOfBunchTrainsInRun=m;
}

UtlTimeDifference IlcRunStart::maximumTimeOfRun() const {
  return _maximumTimeOfRun;
}

void IlcRunStart::maximumTimeOfRun(UtlTimeDifference n) {
  _maximumTimeOfRun=n;
}

std::ostream& IlcRunStart::print(std::ostream &o, std::string s) const {
  o << s << "IlcRunStart::print()" << std::endl;
  _runType.print(o,s+" ");
  o << s << " Run number " << std::setw(10) << _runNumber << std::endl;
  o << s << " Maximum numbers of configurations "
    << _maximumNumberOfConfigurationsInRun << ", bunch trains "
    << _maximumNumberOfBunchTrainsInRun << std::endl;
  o << s << " Maximum time of run "
    << (unsigned)(_maximumTimeOfRun.deltaTime()) << " secs" << std::endl;
  
  return o;
}

#endif
#endif
