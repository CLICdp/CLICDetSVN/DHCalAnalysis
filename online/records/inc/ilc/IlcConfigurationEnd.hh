#ifndef IlcConfigurationEnd_HH
#define IlcConfigurationEnd_HH

#include <string>
#include <iostream>


class IlcConfigurationEnd {

public:
  enum {
    versionNumber=0
  };

  IlcConfigurationEnd();

  void reset();

  unsigned configurationNumberInRun() const;
  void     configurationNumberInRun(unsigned n);

  unsigned actualNumberOfSlowReadoutsInConfiguration() const;
  void     actualNumberOfSlowReadoutsInConfiguration(unsigned m);

   unsigned actualNumberOfBunchTrainsInConfiguration() const;
  void     actualNumberOfBunchTrainsInConfiguration(unsigned m);

  UtlTimeDifference actualTimeOfConfiguration() const;
  void              actualTimeOfConfiguration(UtlTimeDifference m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _configurationNumberInRun;
  unsigned _actualNumberOfSlowReadoutsInConfiguration;
  unsigned _actualNumberOfBunchTrainsInConfiguration;
  UtlTimeDifference _actualTimeOfConfiguration;
};


#ifdef CALICE_DAQ_ICC


IlcConfigurationEnd::IlcConfigurationEnd() {
  reset();
}

void IlcConfigurationEnd::reset() {
  _configurationNumberInRun=0;
  _actualNumberOfSlowReadoutsInConfiguration=0;
  _actualNumberOfBunchTrainsInConfiguration=0;
  _actualTimeOfConfiguration=UtlTimeDifference(0,0);
}

unsigned IlcConfigurationEnd::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void IlcConfigurationEnd::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned IlcConfigurationEnd::actualNumberOfSlowReadoutsInConfiguration() const {
  return _actualNumberOfSlowReadoutsInConfiguration;
}

void IlcConfigurationEnd::actualNumberOfSlowReadoutsInConfiguration(unsigned m) {
  _actualNumberOfSlowReadoutsInConfiguration=m;
}

unsigned IlcConfigurationEnd::actualNumberOfBunchTrainsInConfiguration() const {
  return _actualNumberOfBunchTrainsInConfiguration;
}

void IlcConfigurationEnd::actualNumberOfBunchTrainsInConfiguration(unsigned m) {
  _actualNumberOfBunchTrainsInConfiguration=m;
  }

UtlTimeDifference IlcConfigurationEnd::actualTimeOfConfiguration() const {
  return _actualTimeOfConfiguration;
}

void IlcConfigurationEnd::actualTimeOfConfiguration(UtlTimeDifference m) {
  _actualTimeOfConfiguration=m;
}

std::ostream& IlcConfigurationEnd::print(std::ostream &o, std::string s) const {
  o << s << "IlcConfigurationEnd::print()" << std::endl;
  o << s << " Configuration number = "
    << std::setw(10) << _configurationNumberInRun << std::endl;
  o << s << " Actual  numbers of slow readouts = "
    << std::setw(10) << _actualNumberOfSlowReadoutsInConfiguration << std::endl;
  o << s << " Actual  numbers of bunch trains  = "
    << std::setw(10) << _actualNumberOfBunchTrainsInConfiguration << std::endl;
  o << s << " Actual  time of configuration "
    << (unsigned)(_actualTimeOfConfiguration.deltaTime()) << " secs" << std::endl;
  return o;
}

#endif
#endif
