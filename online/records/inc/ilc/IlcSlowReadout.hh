#ifndef IlcSlowReadout_HH
#define IlcSlowReadout_HH

#include <string>
#include <iostream>


class IlcSlowReadout {

public:
  enum {
    versionNumber=0
  };

  IlcSlowReadout();

  void reset();

  unsigned slowReadoutNumberInRun() const;
  void     slowReadoutNumberInRun(unsigned n);

  unsigned slowReadoutNumberInConfiguration() const;
  void     slowReadoutNumberInConfiguration(unsigned m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _slowReadoutNumberInRun;
  unsigned _slowReadoutNumberInConfiguration;
};


#ifdef CALICE_DAQ_ICC


IlcSlowReadout::IlcSlowReadout() {
  reset();
}

void IlcSlowReadout::reset() {
  _slowReadoutNumberInRun=0;
  _slowReadoutNumberInConfiguration=0;
}

unsigned IlcSlowReadout::slowReadoutNumberInRun() const {
  return _slowReadoutNumberInRun;
}

void IlcSlowReadout::slowReadoutNumberInRun(unsigned n) {
  _slowReadoutNumberInRun=n;
}

unsigned IlcSlowReadout::slowReadoutNumberInConfiguration() const {
  return _slowReadoutNumberInConfiguration;
}

void IlcSlowReadout::slowReadoutNumberInConfiguration(unsigned m) {
  _slowReadoutNumberInConfiguration=m;
}

std::ostream& IlcSlowReadout::print(std::ostream &o, std::string s) const {
  o << s << "IlcSlowReadout::print()" << std::endl;
  o << s << " Slow readout numbers in run "
    << _slowReadoutNumberInRun << ", in configuration "
    << _slowReadoutNumberInConfiguration << std::endl;
   
  return o;
}

#endif
#endif
