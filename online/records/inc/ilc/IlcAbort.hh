#ifndef IlcAbort_HH
#define IlcAbort_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"
#include "SubHeader.hh"


class IlcAbort {

public:
  enum {
    versionNumber=0
  };

  IlcAbort();
  IlcAbort(unsigned d);

  SubHeader::Group group() const;
  void             group(SubHeader::Group g);

  unsigned short groupId() const;
  void           groupId(unsigned short i);

  bool shutDown() const;
  void shutDown(bool b);

  bool sequenceEnd() const;
  void sequenceEnd(bool b);

  bool runEnd() const;
  void runEnd(bool b);

  bool configurationEnd() const;
  void configurationEnd(bool b);

  void datum(unsigned d);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _datum;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


IlcAbort::IlcAbort() : _datum(0) {
}

IlcAbort::IlcAbort(unsigned d) : _datum(d) {
}

SubHeader::Group IlcAbort::group() const {
  return (SubHeader::Group)(_datum.halfByte(4)<<12);
}

void IlcAbort::group(SubHeader::Group g) {
  _datum.halfByte(4,g>>12);
}

unsigned short IlcAbort::groupId() const {
  return _datum.halfWord(0);
}

void IlcAbort::groupId(unsigned short i) {
  _datum.halfWord(0,i);
}

bool IlcAbort::shutDown() const {
  return _datum.bit(31);
}

void IlcAbort::shutDown(bool b) {
  _datum.bit(31,b);
}

bool IlcAbort::sequenceEnd() const {
  return _datum.bit(30);
}

void IlcAbort::sequenceEnd(bool b) {
  _datum.bit(30,b);
}

bool IlcAbort::runEnd() const {
  return _datum.bit(29);
}

void IlcAbort::runEnd(bool b) {
  _datum.bit(29,b);
}

bool IlcAbort::configurationEnd() const {
  return _datum.bit(28);
}

void IlcAbort::configurationEnd(bool b) {
  _datum.bit(28,b);
}

std::ostream& IlcAbort::print(std::ostream &o, std::string s) const {
  o << s << "IlcAbort::print()" << std::endl;
  o << s << " Datum = " << printHex(_datum) << std::endl;

  o << s << "  Group = " << printHex((unsigned short)group()) << " = ";
  std::string g("unknown");
  if(group()==SubHeader::daq) g="ilc";
  if(group()==SubHeader::bml) g="bml";
  if(group()==SubHeader::mps) g="mps";
  if(group()==SubHeader::sim) g="sim";
  if(group()==SubHeader::dec) g="dec";
  o << g << std::endl;

  o << s << "  Group Id = " << groupId() << std::endl;

  if(shutDown()) o << s << "  Flag for shutDown set true" << std::endl;
  if(sequenceEnd()) o << s << "  Flag for sequenceEnd set true" << std::endl;
  if(runEnd()) o << s << "  Flag for runEnd set true" << std::endl;
  if(configurationEnd()) o << s << "  Flag for configurationEnd set true" << std::endl;

  return o;
}

#endif
#endif
