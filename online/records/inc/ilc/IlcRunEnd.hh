#ifndef IlcRunEnd_HH
#define IlcRunEnd_HH

#include <string>
#include <iostream>


class IlcRunEnd {

public:
  enum {
    versionNumber=0
  };

  IlcRunEnd();

  void reset();
  
  IlcRunType runType() const;
  void          runType(IlcRunType t);

  unsigned runNumber() const;
  void     runNumber(unsigned n);

  unsigned actualNumberOfConfigurationsInRun() const;
  void     actualNumberOfConfigurationsInRun(unsigned m);

  unsigned actualNumberOfSlowReadoutsInRun() const;
  void     actualNumberOfSlowReadoutsInRun(unsigned m);

  unsigned actualNumberOfBunchTrainsInRun() const;
  void     actualNumberOfBunchTrainsInRun(unsigned m);

  UtlTimeDifference actualTimeOfRun() const;
  void              actualTimeOfRun(UtlTimeDifference m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  IlcRunType _runType;
  unsigned _runNumber;
  unsigned _actualNumberOfConfigurationsInRun;
  unsigned _actualNumberOfSlowReadoutsInRun;
  unsigned _actualNumberOfBunchTrainsInRun;
  UtlTimeDifference _actualTimeOfRun;
};

#ifdef CALICE_DAQ_ICC

IlcRunEnd::IlcRunEnd() {
  reset();
}

void IlcRunEnd::reset() {
  _runType.type(IlcRunType::daqTest);
  _runType.version(0);

  _runNumber=0;
  _actualNumberOfConfigurationsInRun=0;
  _actualNumberOfSlowReadoutsInRun=0;
  _actualNumberOfBunchTrainsInRun=0;
  _actualTimeOfRun=UtlTimeDifference(0,0);
}

unsigned IlcRunEnd::runNumber() const {
  return _runNumber;
}

void IlcRunEnd::runNumber(unsigned n) {
  _runNumber=n;
}

IlcRunType IlcRunEnd::runType() const {
  return _runType;
}
 
void IlcRunEnd::runType(IlcRunType t) {
  _runType=t;
}

unsigned IlcRunEnd::actualNumberOfConfigurationsInRun() const {
  return _actualNumberOfConfigurationsInRun;
}

void IlcRunEnd::actualNumberOfConfigurationsInRun(unsigned m) {
  _actualNumberOfConfigurationsInRun=m;
}

unsigned IlcRunEnd::actualNumberOfSlowReadoutsInRun() const {
  return _actualNumberOfSlowReadoutsInRun;
}

void IlcRunEnd::actualNumberOfSlowReadoutsInRun(unsigned m) {
  _actualNumberOfSlowReadoutsInRun=m;
}

unsigned IlcRunEnd::actualNumberOfBunchTrainsInRun() const {
  return _actualNumberOfBunchTrainsInRun;
}

void IlcRunEnd::actualNumberOfBunchTrainsInRun(unsigned m) {
  _actualNumberOfBunchTrainsInRun=m;
}

UtlTimeDifference IlcRunEnd::actualTimeOfRun() const {
  return _actualTimeOfRun;
}

void IlcRunEnd::actualTimeOfRun(UtlTimeDifference m) {
  _actualTimeOfRun=m;
}

std::ostream& IlcRunEnd::print(std::ostream &o, std::string s) const {
  o << s << "IlcRunEnd::print()" << std::endl;
  _runType.print(o,s+" ");
  o << s << " Run number " << _runNumber << std::endl;
  o << s << " Actual numbers of configurations "
    << _actualNumberOfConfigurationsInRun << ", slow readouts "
    << _actualNumberOfSlowReadoutsInRun << ", bunch trains "
    << _actualNumberOfBunchTrainsInRun << std::endl;
  o << s << " Actual time of run "
    << (unsigned)(_actualTimeOfRun.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
