#ifndef IlcConfigurationStart_HH
#define IlcConfigurationStart_HH

#include <string>
#include <iostream>


class IlcConfigurationStart {

public:
  enum {
    versionNumber=0
  };

  IlcConfigurationStart();

  void reset();

  unsigned configurationNumberInRun() const;
  void     configurationNumberInRun(unsigned n);

  unsigned maximumNumberOfBunchTrainsInConfiguration() const;
  void     maximumNumberOfBunchTrainsInConfiguration(unsigned m);

  UtlTimeDifference maximumTimeOfConfiguration() const;
  void              maximumTimeOfConfiguration(UtlTimeDifference n);

  UtlTimeDifference minimumTimeBeforeSlowReadout() const;
  void              minimumTimeBeforeSlowReadout(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;

protected:
  unsigned _configurationNumberInRun;

  unsigned _maximumNumberOfBunchTrainsInConfiguration;
  UtlTimeDifference _maximumTimeOfConfiguration;

  UtlTimeDifference _minimumTimeBeforeSlowReadout;
};


#ifdef CALICE_DAQ_ICC


IlcConfigurationStart::IlcConfigurationStart() {
  reset();
}

void IlcConfigurationStart::reset() {
  _configurationNumberInRun=0;

  _maximumNumberOfBunchTrainsInConfiguration=0xffffffff;
  _maximumTimeOfConfiguration=UtlTimeDifference(0x7fffffff,999999);

  _minimumTimeBeforeSlowReadout=UtlTimeDifference(60,0);
}

unsigned IlcConfigurationStart::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void IlcConfigurationStart::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned IlcConfigurationStart::maximumNumberOfBunchTrainsInConfiguration() const {
  return _maximumNumberOfBunchTrainsInConfiguration;
}

void IlcConfigurationStart::maximumNumberOfBunchTrainsInConfiguration(unsigned m) {
  _maximumNumberOfBunchTrainsInConfiguration=m;
}

UtlTimeDifference IlcConfigurationStart::maximumTimeOfConfiguration() const {
  return _maximumTimeOfConfiguration;
}
 
void IlcConfigurationStart::maximumTimeOfConfiguration(UtlTimeDifference n) {
  _maximumTimeOfConfiguration=n;
}
 
UtlTimeDifference IlcConfigurationStart::minimumTimeBeforeSlowReadout() const {
  return _minimumTimeBeforeSlowReadout;
}

void IlcConfigurationStart::minimumTimeBeforeSlowReadout(UtlTimeDifference n) {
  _minimumTimeBeforeSlowReadout=n;
}

std::ostream& IlcConfigurationStart::print(std::ostream &o, std::string s) const {
  o << s << "IlcConfigurationStart::print()" << std::endl;
  o << s << " Configuration number            = "
    << std::setw(11) << _configurationNumberInRun << std::endl;
  o << s << " Maximum numbers of bunch trains = " 
    << std::setw(11) << _maximumNumberOfBunchTrainsInConfiguration << std::endl;
  o << s << " Maximum time of configuration   = " << std::setw(11)
    //<< (unsigned)(_maximumTimeOfConfiguration.deltaTime()) << " secs" << std::endl;
    << _maximumTimeOfConfiguration << " secs" << std::endl;
  o << s << " Minimum time of slow readout    = " << std::setw(11)
    //<< (unsigned)(_minimumTimeBeforeSlowReadout.deltaTime()) << " secs" << std::endl;
    << _minimumTimeBeforeSlowReadout << " secs" << std::endl;
  return o;
}

#endif
#endif
