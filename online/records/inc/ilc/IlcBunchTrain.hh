#ifndef IlcBunchTrain_HH
#define IlcBunchTrain_HH

#include <string>
#include <iostream>


class IlcBunchTrain {

public:
  enum {
    versionNumber=0
  };

  IlcBunchTrain();
  
  void reset();

  unsigned bunchTrainNumberInRun() const;
  void     bunchTrainNumberInRun(unsigned n);

  unsigned bunchTrainNumberInConfiguration() const;
  void     bunchTrainNumberInConfiguration(unsigned n);
  
  void increment();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _bunchTrainNumberInRun;
  unsigned _bunchTrainNumberInConfiguration;
};


#ifdef CALICE_DAQ_ICC

IlcBunchTrain::IlcBunchTrain() {
  reset();
}

void IlcBunchTrain::reset() {
  _bunchTrainNumberInRun=0;
  _bunchTrainNumberInConfiguration=0;
}

unsigned IlcBunchTrain::bunchTrainNumberInRun() const {
  return _bunchTrainNumberInRun;
}

void IlcBunchTrain::bunchTrainNumberInRun(unsigned n) {
  _bunchTrainNumberInRun=n;
}

unsigned IlcBunchTrain::bunchTrainNumberInConfiguration() const {
  return _bunchTrainNumberInConfiguration;
}

void IlcBunchTrain::bunchTrainNumberInConfiguration(unsigned n) {
  _bunchTrainNumberInConfiguration=n;
}

void IlcBunchTrain::increment() {
  _bunchTrainNumberInRun++;
  _bunchTrainNumberInConfiguration++;
}

std::ostream& IlcBunchTrain::print(std::ostream &o, std::string s) const {
  o << s << "IlcBunchTrain::print()" << std::endl;
  o << s << " BunchTrain numbers in run "
    << _bunchTrainNumberInRun << ", in configuration "
    << _bunchTrainNumberInConfiguration << std::endl;
  return o;
}

#endif
#endif
