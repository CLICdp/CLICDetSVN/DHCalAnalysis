#ifndef FixCrc_HH
#define FixCrc_HH

class RcdRecord;

class FixCrc {

public:
  FixCrc();
  FixCrc(unsigned p);

  bool fix(RcdRecord &r);

private:
  unsigned _printLevel;
};


#ifdef CALICE_DAQ_ICC

#include <iostream>

// records/inc/rcd
#include "RcdRecord.hh"

// records/inc/sub
#include "SubInserter.hh"
#include "SubAccessor.hh"

// records/inc/crc
#include "CrcReadoutConfigurationDataV0.hh"
#include "CrcReadoutConfigurationData.hh"


FixCrc::FixCrc() : _printLevel(0) {
}

FixCrc::FixCrc(unsigned p) : _printLevel(p) {
}

bool FixCrc::fix(RcdRecord &r) {

  SubAccessor accessor(r);

  // CrcReadoutConfigurationData V0->V1
  std::vector<const CrcReadoutConfigurationDataV0*>
    v0(accessor.access<CrcReadoutConfigurationDataV0>());
  assert(v0.size()<=1);
  
  std::vector<const CrcReadoutConfigurationData*>
    v1(accessor.access<CrcReadoutConfigurationData>());
  assert(v1.size()<=1);
  
  if(v0.size()==1 && v1.size()==0) {
    if(_printLevel>2) {
      std::cout << "FixCrc::fix()  Original CrcReadoutConfigurationDataV0 subrecord"
		<< std::endl;
      v0[0]->print(std::cout);
    }
    
    SubInserter inserter(r);
    inserter.insert<CrcReadoutConfigurationData>(CrcReadoutConfigurationData(*v0[0]));
    v1=accessor.access<CrcReadoutConfigurationData>();
    assert(v1.size()==1);
    
    if(_printLevel>2) {
      std::cout << "FixCrc::fix() Final CrcReadoutConfigurationData subrecord"
		<< std::endl;
      v1[0]->print(std::cout);
    }
  }

  return true;
}

#endif
#endif
