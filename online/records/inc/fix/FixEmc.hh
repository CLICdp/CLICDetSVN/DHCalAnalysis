#ifndef FixEmc_HH
#define FixEmc_HH

class RcdRecord;

class FixEmc {

public:
  FixEmc();
  FixEmc(unsigned p);

  bool fix(RcdRecord &r);

private:
  unsigned _printLevel;
};


#ifdef CALICE_DAQ_ICC

#include <iostream>

// record/inc/rcd
#include "RcdRecord.hh"

// record/inc/sub
#include "SubInserter.hh"
#include "SubAccessor.hh"

// record/inc/emc
#include "EmcStageRunData.hh"


FixEmc::FixEmc() : _printLevel(0) {
}

FixEmc::FixEmc(unsigned p) : _printLevel(p) {
}

bool FixEmc::fix(RcdRecord &r) {
  
  if(r.recordType()==RcdHeader::runStart) {
    
    SubAccessor accessor(r);
    std::vector<const EmcStageRunData*>
      v(accessor.access<EmcStageRunData>());
    
    // Only add subrecord if none exists
    if(v.size()==0) {
      std::vector<const DaqRunStart*> w(accessor.access<DaqRunStart>());
      assert(w.size()==1);
      
      if(w[0]->runNumber()<=100091) return true;
      if(w[0]->runNumber()>=110000) return true;
      
      SubInserter inserter(r);
      EmcStageRunData *b(inserter.insert<EmcStageRunData>());
      b->magicNumber(0x514264d);
      
      if(w[0]->runNumber()==100092) {b->xStandPosition(4000);b->yStandPosition(1350);}
      if(w[0]->runNumber()==100093) {b->xStandPosition(4000);b->yStandPosition(1350);}
      if(w[0]->runNumber()==100094) {b->xStandPosition(3965);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100095) {b->xStandPosition(3965);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100096) {b->xStandPosition(4575);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100097) {b->xStandPosition(4575);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100098) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100099) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100100) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100101) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100102) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100103) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100104) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100105) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100106) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100107) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100108) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100109) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100110) {b->xStandPosition(4575);b->yStandPosition(1341);}
      if(w[0]->runNumber()==100111) {b->xStandPosition(3965);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100112) {b->xStandPosition(3965);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100113) {b->xStandPosition(4625);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100114) {b->xStandPosition(4625);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100115) {b->xStandPosition(4625);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100116) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100117) {b->xStandPosition(   0);b->yStandPosition(   0);}
      if(w[0]->runNumber()==100118) {b->xStandPosition(3380);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100119) {b->xStandPosition(3380);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100120) {b->xStandPosition(3380);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100121) {b->xStandPosition(3380);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100122) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100123) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100124) {b->xStandPosition(4575);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100125) {b->xStandPosition(4625);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100126) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100127) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100128) {b->xStandPosition(3380);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100129) {b->xStandPosition(3380);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100130) {b->xStandPosition(4625);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100131) {b->xStandPosition(4625);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100132) {b->xStandPosition(4625);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100133) {b->xStandPosition(4625);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100134) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100135) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100136) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100137) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100138) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100139) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100140) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100141) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100142) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100143) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100144) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100145) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100146) {b->xStandPosition(4306);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100147) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100148) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100149) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100150) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100151) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100152) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100153) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100154) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100155) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100156) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100157) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100158) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100159) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100160) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100161) {b->xStandPosition(3684);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100162) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100163) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100164) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100165) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100166) {b->xStandPosition(4390);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100167) {b->xStandPosition(4390);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100168) {b->xStandPosition(4390);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100169) {b->xStandPosition(4390);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100170) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100171) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100172) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100173) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100174) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100175) {b->xStandPosition(4390);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100176) {b->xStandPosition(4390);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100177) {b->xStandPosition(4390);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100178) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100179) {b->xStandPosition(4390);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100180) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100181) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100182) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100183) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100184) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100185) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100186) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100187) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100188) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100189) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100190) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100191) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100192) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100193) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100194) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100195) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100196) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100197) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100198) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100199) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100200) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100201) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100202) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100203) {b->xStandPosition(3684);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100204) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100205) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100206) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100207) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100208) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100209) {b->xStandPosition(3995);b->yStandPosition(1951);}
      if(w[0]->runNumber()==100210) {b->xStandPosition(3995);b->yStandPosition(1950);}
      if(w[0]->runNumber()==100211) {b->xStandPosition(3995);b->yStandPosition(1950);}
      if(w[0]->runNumber()==100212) {b->xStandPosition(3995);b->yStandPosition(1950);}
      if(w[0]->runNumber()==100213) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100214) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100215) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100216) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100217) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100218) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100219) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100220) {b->xStandPosition(3995);b->yStandPosition(1321);}
      if(w[0]->runNumber()==100221) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100222) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100223) {b->xStandPosition(3995);b->yStandPosition(1664);}
      if(w[0]->runNumber()==100224) {b->xStandPosition(3995);b->yStandPosition(1664);}
      
      b->setChecksum();
      
      if(_printLevel>0) {
	std::cout << "FixEmc::fixStandPosition()  EmcStageRunData subrecord added"
		  << std::endl;
	b->print(std::cout);
      }
    }
    return true;
  }
  
  if(r.recordType()==RcdHeader::configurationStart ||
     r.recordType()==RcdHeader::configurationStop  ||
     r.recordType()==RcdHeader::configurationEnd) {

    // Cannot access using normal accessor as template is specialised
    SubAccessor accessor(r);
    std::vector<const SubHeader*> v(accessor.access(subRecordType< CrcLocationData<EmcFeConfigurationData> >()));

    // Change subrecord type to be CRC value and set crate number to ECAL value
    for(unsigned i(0);i<v.size();i++) {
      ((SubHeader*)v[i])->subRecordType(subRecordType< CrcLocationData<CrcFeConfigurationData> >());
      ((CrcLocationData<CrcFeConfigurationData>*)(v[i]->data()))->crateNumber(0xec);
    }

    return true;
  }
  
  return true;
}

#endif
#endif
