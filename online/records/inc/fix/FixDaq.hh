#ifndef FixDaq_HH
#define FixDaq_HH

#include "DaqRunType.hh"

class RcdRecord;

class FixDaq {

public:
  FixDaq();
  FixDaq(unsigned p);

  bool fix(RcdRecord &r);
  //Complete copy of fix + little extra functionality "required" by the converter
  bool fix_paranoia(RcdRecord &r, int&);

  static unsigned epochTimeToRunNumber(unsigned t);
  static DaqRunType runNumberToRunType(unsigned r);

private:
  unsigned _printLevel;
  unsigned _runNumber;
  unsigned _count[6];
};


#ifdef CALICE_DAQ_ICC

#include <vector>
#include <iostream>

// record/inc/sub
#include "SubInserter.hh"
#include "SubAccessor.hh"
#include "SubModifier.hh"

// record/inc/daq
#include "DaqRunStart.hh"
#include "DaqRunEnd.hh"
#include "DaqConfigurationStart.hh"
#include "DaqConfigurationEnd.hh"
#include "DaqAcquisitionStart.hh"
#include "DaqAcquisitionEnd.hh"


FixDaq::FixDaq() : _printLevel(0) {
}

FixDaq::FixDaq(unsigned p) : _printLevel(p) {
}

bool FixDaq::fix(RcdRecord &r) {
  
  // Check record type
  switch (r.recordType()) {
    
  case RcdHeader::runStart: {
    SubModifier accessor(r);
    
    std::vector<DaqRunStart*> v(accessor.access<DaqRunStart>());
    assert(v.size()==1);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()  Initial DaqRunStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    if(v[0]->runNumber()==0 || v[0]->runNumber()>999999) {
      v[0]->runNumber(epochTimeToRunNumber(r.recordTime().seconds()));
      v[0]->runType(DaqRunType(DaqRunType::daqTest,0));
    }

    assert(v[0]->runNumber()>0);
    v[0]->runType(runNumberToRunType(v[0]->runNumber()));

    /*
      if(v[0]->runNumber()== 80000) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80001) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80002) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80003) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80004) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80005) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80006) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80007) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80008) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80009) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80010) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80011) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80012) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80013) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80014) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80015) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80016) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80017) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80018) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80019) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80020) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80021) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80022) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80023) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80024) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80025) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80026) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80027) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80028) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80029) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80030) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80031) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80032) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80033) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80034) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80035) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80036) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}

      if(v[0]->runNumber()== 99975) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99976) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99977) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99978) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99979) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99980) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99981) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99982) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99983) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99984) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99985) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99986) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99987) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99988) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99989) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99990) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99991) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99992) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99993) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99994) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99995) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99996) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99997) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99998) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99999) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100000) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100002) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100010) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100030) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100031) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100032) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100033) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100039) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100040) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100041) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100043) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100044) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100049) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100050) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100051) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100052) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100053) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100054) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100055) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100056) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100057) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100058) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100059) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100060) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100061) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100062) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100063) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100064) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100065) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100066) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100067) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100068) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100069) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100070) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100071) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100072) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100073) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100074) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100075) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100076) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100077) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100078) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100079) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100080) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100081) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100082) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100083) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100084) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100085) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100086) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100087) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100088) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100089) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100090) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100091) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100092) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100093) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100094) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100095) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100096) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100097) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100098) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100099) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100100) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100101) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100102) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100103) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100104) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100105) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100106) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100107) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100108) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100109) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100110) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100111) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100112) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100113) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100114) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100115) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100116) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100117) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100118) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100119) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100120) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100121) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100122) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100123) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100124) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100125) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100126) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100127) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100128) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100129) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100130) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100131) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100132) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100133) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100134) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100135) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100136) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100137) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100138) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100139) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100140) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100141) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100142) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100143) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100144) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100145) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100146) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100147) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100148) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100149) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100150) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100151) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100152) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100153) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100154) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100155) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100156) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100157) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100158) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100159) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100160) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100161) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100162) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100163) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100164) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100165) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100166) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100167) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100168) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100169) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100170) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100171) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100172) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100173) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100174) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100175) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100176) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100177) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100178) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100179) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100180) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100181) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100182) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100183) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100184) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100185) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100186) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100187) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100188) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100189) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100190) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100191) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100192) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100193) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100194) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100195) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100196) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100197) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100198) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100199) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100200) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100201) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100202) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100203) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100204) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100205) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100206) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100207) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100208) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100209) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100210) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100211) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100212) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100213) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100214) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100215) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100216) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100217) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100218) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100219) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100220) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100221) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100222) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100223) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100224) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
    */
    
    _runNumber=v[0]->runNumber();
    _count[0]=0;
    _count[1]=0;
    _count[2]=0;
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()   Final DaqRunStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::runEnd: {
    SubModifier accessor(r);
    
    std::vector<DaqRunEnd*> v(accessor.access<DaqRunEnd>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqRunEnd>(true));

      if(_printLevel>0) {
	std::cout << "FixDaq::record()  DaqRunEnd subrecord added" 
		  << std::endl;
      }
    }

    assert(v.size()==1);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()  Initial DaqRunEnd subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    v[0]->runNumber(_runNumber);
    v[0]->actualNumberOfConfigurationsInRun(_count[0]);
    v[0]->actualNumberOfAcquisitionsInRun(_count[1]);
    v[0]->actualNumberOfEventsInRun(_count[2]);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()   Final DaqRunEnd subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::configurationStart: {
    SubModifier accessor(r);
    
    std::vector<DaqConfigurationStart*>
      v(accessor.access<DaqConfigurationStart>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqConfigurationStart>(true));

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  DaqConfigurationStart subrecord added" 
		  << std::endl;
      }
    } else {

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  Initial DaqConfigurationStart subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
      }
    }
    
    assert(v.size()==1);
    
    v[0]->configurationNumberInRun(_count[0]);
    
    _count[3]=0;
    _count[4]=0;
    
    if(_printLevel>2) {
      std::cout << "FixDaq::record()  Final DaqConfigurationStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::configurationEnd: {
    SubModifier accessor(r);
    
    std::vector<DaqConfigurationEnd*>
      v(accessor.access<DaqConfigurationEnd>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqConfigurationEnd>(true));

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  DaqConfigurationEnd subrecord added" 
		  << std::endl;
      }

    } else {
      if(_printLevel>2) {
	std::cout << "FixDaq::record()  Initial DaqConfigurationEnd subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
      }
    }
    

    assert(v.size()==1);
    
    v[0]->configurationNumberInRun(_count[0]);
    v[0]->actualNumberOfAcquisitionsInConfiguration(_count[3]);
    v[0]->actualNumberOfEventsInConfiguration(_count[4]);
    
    _count[0]++;
    
    if(_printLevel>2) {
	std::cout << "FixDaq::record()  Final DaqConfigurationEnd subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::acquisitionStart: {

    SubInserter inserter(r);
    DaqAcquisitionStart *p(inserter.insert<DaqAcquisitionStart>(true));

    if(_printLevel>4) {
      std::cout << "FixDaq::record()  DaqAcquisitionStart subrecord added" 
		<< std::endl;
    }
    
    p->acquisitionNumberInRun(_count[1]);
    p->acquisitionNumberInConfiguration(_count[3]);
    
    _count[5]=0;
    
    if(_printLevel>4) {
      std::cout << "FixDaq::record()  Final DaqAcquisitionStart subrecord" 
		<< std::endl;
      p->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::acquisitionEnd: {
    SubInserter inserter(r);
    DaqAcquisitionEnd *p(inserter.insert<DaqAcquisitionEnd>(true));

    if(_printLevel>4) {
      std::cout << "FixDaq::record()  DaqAcquisitionEnd subrecord added" 
		<< std::endl;
    }
    
    p->acquisitionNumberInRun(_count[1]);
    p->acquisitionNumberInConfiguration(_count[3]);
    p->actualNumberOfEventsInAcquisition(_count[5]);
    
    _count[1]++;
    _count[3]++;
    
    if(_printLevel>4) {
      std::cout << "FixDaq::record()  Final DaqAcquisitionEnd subrecord" 
		<< std::endl;
      p->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::event: {
    SubModifier accessor(r);
    
    std::vector<DaqEvent*> v(accessor.access<DaqEvent>());
    assert(v.size()==1);
    
    if(_printLevel>6) {
      std::cout << "FixDaq::record()  Initial DaqEvent subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    v[0]->eventNumberInRun(_count[2]);
    v[0]->eventNumberInConfiguration(_count[4]);
    v[0]->eventNumberInAcquisition(_count[5]);
    
    _count[2]++;
    _count[4]++;
    _count[5]++;
    
    if(_printLevel>6) {
      std::cout << "FixDaq::record()  Final DaqEvent subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  default: {
    break;
  }
  };
  
  return true;
}


//CRP Hack get runnumber by external information
//This is nearly an exact copy of the method fix ecxept that it adds functionality 
//"required" by the converter
bool FixDaq::fix_paranoia(RcdRecord &r, int &runno) {
//bool FixDaq::fix(RcdRecord &r) {

  
  // Check record type
  switch (r.recordType()) {
    
  case RcdHeader::runStart: {
    SubModifier accessor(r);
    
    std::vector<DaqRunStart*> v(accessor.access<DaqRunStart>());
    assert(v.size()==1);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()  Initial DaqRunStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    if(v[0]->runNumber()==0 || v[0]->runNumber()>999999) {
      v[0]->runNumber(epochTimeToRunNumber(r.recordTime().seconds()));
      v[0]->runType(DaqRunType(DaqRunType::daqTest,0));
    }

     //if no runnumber is delivered we set the runnumber by hand
    if ( v[0]->runNumber() == 0 ) v[0]->runNumber(runno);    
    assert(v[0]->runNumber()>0);
    if ( v[0]->runNumber() <= 100224 ) v[0]->runType(runNumberToRunType(v[0]->runNumber()));

    /*
      if(v[0]->runNumber()== 80000) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80001) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80002) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80003) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80004) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80005) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80006) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80007) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80008) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80009) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80010) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80011) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80012) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80013) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80014) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80015) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80016) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80017) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80018) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 80019) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80020) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80021) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80022) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80023) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80024) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80025) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80026) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80027) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80028) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80029) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80030) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80031) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80032) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80033) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80034) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80035) {v[0]->runType(DaqRunStart::cosmics);v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 80036) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}

      if(v[0]->runNumber()== 99975) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99976) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99977) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99978) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99979) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99980) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99981) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99982) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99983) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99984) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99985) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99986) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99987) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99988) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99989) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99990) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99991) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99992) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99993) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99994) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99995) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99996) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()== 99997) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99998) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()== 99999) {v[0]->runType(DaqRunStart::test   );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100000) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100002) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100010) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100030) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100031) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100032) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100033) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100039) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100040) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100041) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100043) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100044) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100049) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100050) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100051) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100052) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100053) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100054) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100055) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100056) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100057) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100058) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100059) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100060) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100061) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100062) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100063) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100064) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100065) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100066) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100067) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100068) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100069) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100070) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100071) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100072) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100073) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100074) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100075) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100076) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100077) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100078) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100079) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100080) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100081) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100082) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100083) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100084) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100085) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100086) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100087) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100088) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100089) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100090) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100091) {v[0]->runType(DaqRunStart::vfeDac );v[0]->runSubtype(0);}
      if(v[0]->runNumber()==100092) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100093) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100094) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100095) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100096) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100097) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100098) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100099) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100100) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100101) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100102) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100103) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100104) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100105) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100106) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100107) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100108) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100109) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100110) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100111) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100112) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100113) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100114) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100115) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100116) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100117) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100118) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100119) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100120) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100121) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100122) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100123) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100124) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100125) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100126) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100127) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100128) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100129) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100130) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100131) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100132) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100133) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(2);}
      if(v[0]->runNumber()==100134) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100135) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100136) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100137) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100138) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100139) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100140) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100141) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100142) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100143) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100144) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100145) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100146) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100147) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100148) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100149) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100150) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100151) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100152) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100153) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100154) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100155) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100156) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100157) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100158) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100159) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100160) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100161) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100162) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100163) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100164) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100165) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100166) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100167) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100168) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100169) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100170) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100171) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100172) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100173) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100174) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100175) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100176) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100177) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100178) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100179) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100180) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100181) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100182) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100183) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100184) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100185) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100186) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100187) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100188) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100189) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100190) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100191) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100192) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100193) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100194) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100195) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(1);}
      if(v[0]->runNumber()==100196) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100197) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100198) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100199) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100200) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100201) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100202) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100203) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100204) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100205) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100206) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100207) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100208) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100209) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100210) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100211) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100212) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100213) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100214) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100215) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100216) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100217) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100218) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100219) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100220) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100221) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100222) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100223) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
      if(v[0]->runNumber()==100224) {v[0]->runType(DaqRunStart::beam   );v[0]->runSubtype(3);}
    */
    
    _runNumber=v[0]->runNumber();
    _count[0]=0;
    _count[1]=0;
    _count[2]=0;
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()   Final DaqRunStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::runEnd: {
    SubModifier accessor(r);
    
    std::vector<DaqRunEnd*> v(accessor.access<DaqRunEnd>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqRunEnd>(true));

      if(_printLevel>0) {
	std::cout << "FixDaq::record()  DaqRunEnd subrecord added" 
		  << std::endl;
      }
    }

    assert(v.size()==1);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()  Initial DaqRunEnd subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    v[0]->runNumber(_runNumber);
    v[0]->actualNumberOfConfigurationsInRun(_count[0]);
    v[0]->actualNumberOfAcquisitionsInRun(_count[1]);
    v[0]->actualNumberOfEventsInRun(_count[2]);
    
    if(_printLevel>0) {
      std::cout << "FixDaq::record()   Final DaqRunEnd subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::configurationStart: {
    SubModifier accessor(r);
    
    std::vector<DaqConfigurationStart*>
      v(accessor.access<DaqConfigurationStart>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqConfigurationStart>(true));

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  DaqConfigurationStart subrecord added" 
		  << std::endl;
      }
    } else {

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  Initial DaqConfigurationStart subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
      }
    }
    
    assert(v.size()==1);
    
    v[0]->configurationNumberInRun(_count[0]);
    
    _count[3]=0;
    _count[4]=0;
    
    if(_printLevel>2) {
      std::cout << "FixDaq::record()  Final DaqConfigurationStart subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::configurationEnd: {
    SubModifier accessor(r);
    
    std::vector<DaqConfigurationEnd*>
      v(accessor.access<DaqConfigurationEnd>());

    if(v.size()==0) {
      SubInserter inserter(r);
      v.push_back(inserter.insert<DaqConfigurationEnd>(true));

      if(_printLevel>2) {
	std::cout << "FixDaq::record()  DaqConfigurationEnd subrecord added" 
		  << std::endl;
      }

    } else {
      if(_printLevel>2) {
	std::cout << "FixDaq::record()  Initial DaqConfigurationEnd subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
      }
    }
    

    assert(v.size()==1);
    
    v[0]->configurationNumberInRun(_count[0]);
    v[0]->actualNumberOfAcquisitionsInConfiguration(_count[3]);
    v[0]->actualNumberOfEventsInConfiguration(_count[4]);
    
    _count[0]++;
    
    if(_printLevel>2) {
	std::cout << "FixDaq::record()  Final DaqConfigurationEnd subrecord" 
		  << std::endl;
	v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::acquisitionStart: {

    SubInserter inserter(r);
    DaqAcquisitionStart *p(inserter.insert<DaqAcquisitionStart>(true));

    if(_printLevel>4) {
      std::cout << "FixDaq::record()  DaqAcquisitionStart subrecord added" 
		<< std::endl;
    }
    
    p->acquisitionNumberInRun(_count[1]);
    p->acquisitionNumberInConfiguration(_count[3]);
    
    _count[5]=0;
    
    if(_printLevel>4) {
      std::cout << "FixDaq::record()  Final DaqAcquisitionStart subrecord" 
		<< std::endl;
      p->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::acquisitionEnd: {
    SubInserter inserter(r);
    DaqAcquisitionEnd *p(inserter.insert<DaqAcquisitionEnd>(true));

    if(_printLevel>4) {
      std::cout << "FixDaq::record()  DaqAcquisitionEnd subrecord added" 
		<< std::endl;
    }
    
    p->acquisitionNumberInRun(_count[1]);
    p->acquisitionNumberInConfiguration(_count[3]);
    p->actualNumberOfEventsInAcquisition(_count[5]);
    
    _count[1]++;
    _count[3]++;
    
    if(_printLevel>4) {
      std::cout << "FixDaq::record()  Final DaqAcquisitionEnd subrecord" 
		<< std::endl;
      p->print(std::cout," ");
    }
    
    break;
  }
    
  case RcdHeader::event: {
    SubModifier accessor(r);
    
    std::vector<DaqEvent*> v(accessor.access<DaqEvent>());
    assert(v.size()==1);
    
    if(_printLevel>6) {
      std::cout << "FixDaq::record()  Initial DaqEvent subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    v[0]->eventNumberInRun(_count[2]);
    v[0]->eventNumberInConfiguration(_count[4]);
    v[0]->eventNumberInAcquisition(_count[5]);
    
    _count[2]++;
    _count[4]++;
    _count[5]++;
    
    if(_printLevel>6) {
      std::cout << "FixDaq::record()  Final DaqEvent subrecord" 
		<< std::endl;
      v[0]->print(std::cout," ");
    }
    
    break;
  }
    
  default: {
    break;
  }
  };
  
  return true;
}




unsigned FixDaq::epochTimeToRunNumber(unsigned t) {

  switch (t) {

  // Cosmics, Ecole Polytechnique, Dec 2004 - Jan 2005
  case 1102676945: return  80000;
  case 1102699425: return  80001;
  case 1102703739: return  80002;
  case 1102892325: return  80003;
  case 1102940489: return  80004;
  case 1102958829: return  80005;
  case 1103017999: return  80006;
  case 1103045815: return  80007;
  case 1103117304: return  80008;
  case 1103120381: return  80009;
  case 1103188085: return  80010;
  case 1103196157: return  80011;
  case 1103203713: return  80012;
  case 1103303220: return  80013;
  case 1103324633: return  80014;
  case 1103364359: return  80015;
  case 1103365394: return  80016;
  case 1103445683: return  80017;
  case 1103530113: return  80018;
  case 1103657386: return  80019;
  case 1103674352: return  80020;
  case 1103762959: return  80021;
  case 1103800363: return  80022;
  case 1103801584: return  80023;
  case 1103886841: return  80024;
  case 1103971766: return  80025;
  case 1104056062: return  80026;
  case 1104145601: return  80027;
  case 1104237209: return  80028;
  case 1104324323: return  80029;
  case 1104411373: return  80030;
  case 1104499704: return  80031;
  case 1104593375: return  80032;
  case 1104681707: return  80033;
  case 1104771321: return  80034;
  case 1104860743: return  80035;
  case 1104927176: return  80036;

  // Electron beam, DESY, Jan - Mar 2005
  case 1105008824: return  99975;
  case 1105695281: return  99976;
  case 1105703858: return  99977;
  case 1105712427: return  99978;
  case 1105973670: return  99979;
  case 1105974817: return  99980;
  case 1105975224: return  99981;
  case 1105975431: return  99982;
  case 1105976665: return  99983;
  case 1105976720: return  99984;
  case 1105979898: return  99985;
  case 1105980357: return  99986;
  case 1105981668: return  99987;
  case 1105981989: return  99988;
  case 1105984080: return  99989;
  case 1105985234: return  99990;
  case 1105985509: return  99991;
  case 1105986684: return  99992;
  case 1105994571: return  99993;
  case 1105994941: return  99994;
  case 1106002056: return  99995;
  case 1106022608: return  99996;
  case 1106053986: return  99997;
  case 1106054153: return  99998;
  case 1106057895: return  99999;
  case 1106058215: return 100000;
  case 1106095412: return 100001;
  case 1106130838: return 100002;
  case 1106132070: return 100003;
  case 1106132743: return 100004;
  case 1106145956: return 100005;
  case 1106148586: return 100006;
  case 1106149264: return 100007;
  case 1106149454: return 100008;
  case 1106149864: return 100009;
  case 1106150152: return 100010;
  case 1106163220: return 100030;
  case 1106176751: return 100031;
  case 1106187130: return 100032;
  case 1106198682: return 100033;
  case 1106229307: return 100034;
  case 1106241338: return 100035;
  case 1106247394: return 100036;
  case 1106250930: return 100037;
  case 1106251367: return 100038;
  case 1106258613: return 100039;
  case 1106272011: return 100040;
  case 1106284892: return 100041;
  case 1106317013: return 100042;
  case 1106319314: return 100043;
  case 1106379176: return 100044;
  case 1106379616: return 100049;
  case 1106379904: return 100050;
  case 1106410598: return 100051;
  case 1106441715: return 100052;
  case 1106473380: return 100053;
  case 1106498185: return 100054;
  case 1106499072: return 100055;
  case 1106524615: return 100056;
  case 1106587999: return 100057;
  case 1106620496: return 100058;
  case 1106664587: return 100059;
  case 1106670741: return 100060;
  case 1106674564: return 100061;
  case 1106678605: return 100062;
  case 1106682505: return 100063;
  case 1106729239: return 100064;
  case 1106738547: return 100065;
  case 1106818930: return 100066;
  case 1106850297: return 100067;
  case 1106860731: return 100068;
  case 1106876494: return 100069;
  case 1106886058: return 100070;
  case 1106905171: return 100071;
  case 1106906214: return 100072;
  case 1106907339: return 100073;
  case 1106908304: return 100074;
  case 1106909410: return 100075;
  case 1106912070: return 100076;
  case 1106913404: return 100077;
  case 1106914702: return 100078;
  case 1106915391: return 100079;
  case 1106915956: return 100080;
  case 1106916531: return 100081;
  case 1106917325: return 100082;
  case 1106917955: return 100083;
  case 1106925996: return 100084;
  case 1107014359: return 100085;
  case 1107026165: return 100086;
  case 1107042600: return 100087;
  case 1107052853: return 100088;
  case 1107062970: return 100089;
  case 1107336903: return 100090;
  case 1107404970: return 100091;
  case 1107451000: return 100092;
  case 1107457626: return 100093;
  case 1107461269: return 100094;
  case 1107473742: return 100095;
  case 1107492629: return 100096;
  case 1107504540: return 100097;
  case 1107517876: return 100098;
  case 1107550346: return 100099;
  case 1107550682: return 100100;
  case 1107562969: return 100101;
  case 1107572861: return 100102;
  case 1107582862: return 100103;
  case 1107596544: return 100104;
  case 1107607090: return 100105;
  case 1107619057: return 100106;
  case 1107628006: return 100107;
  case 1107640474: return 100108;
  case 1107653472: return 100109;
  case 1107665870: return 100110;
  case 1107766576: return 100111;
  case 1107766941: return 100112;
  case 1107783030: return 100113;
  case 1107792390: return 100114;
  case 1107805480: return 100115;
  case 1107806000: return 100116;
  case 1107816649: return 100117;
  case 1107817236: return 100118;
  case 1107828532: return 100119;
  case 1107839306: return 100120;
  case 1107849654: return 100121;
  case 1107874671: return 100122;
  case 1107886287: return 100123;
  case 1107895934: return 100124;
  case 1107900053: return 100125;
  case 1107911127: return 100126;
  case 1107920176: return 100127;
  case 1107930796: return 100128;
  case 1107947920: return 100129;
  case 1107966952: return 100130;
  case 1107967294: return 100131;
  case 1107977544: return 100132;
  case 1107988298: return 100133;
  case 1108005391: return 100134;
  case 1108015822: return 100135;
  case 1108017214: return 100136;
  case 1108020739: return 100137;
  case 1108031160: return 100138;
  case 1108037944: return 100139;
  case 1108048634: return 100140;
  case 1108062466: return 100141;
  case 1108064075: return 100142;
  case 1108066219: return 100143;
  case 1108076398: return 100144;
  case 1108085759: return 100145;
  case 1108096427: return 100146;
  case 1108111346: return 100147;
  case 1108125488: return 100148;
  case 1108135533: return 100149;
  case 1108136253: return 100150;
  case 1108157788: return 100151;
  case 1108168826: return 100153;
  case 1108180484: return 100154;
  case 1108453124: return 100155;
  case 1108465594: return 100156;
  case 1108465902: return 100157;
  case 1108466097: return 100158;
  case 1108481236: return 100159;
  case 1108493845: return 100160;
  case 1108502971: return 100161;
  case 1108517022: return 100162;
  case 1108524625: return 100163;
  case 1108532372: return 100164;
  case 1108539151: return 100165;
  case 1108582330: return 100166;
  case 1108586358: return 100167;
  case 1108594083: return 100168;
  case 1108600287: return 100169;
  case 1108609269: return 100170;
  case 1108618222: return 100171;
  case 1108632198: return 100172;
  case 1108645663: return 100173;
  case 1108652974: return 100174;
  case 1108659656: return 100175;
  case 1108667036: return 100176;
  case 1108681450: return 100177;
  case 1108689267: return 100178;
  case 1108712599: return 100179;
  case 1108720869: return 100180;
  case 1108729321: return 100181;
  case 1108736024: return 100182;
  case 1108977101: return 100183;
  case 1108978052: return 100184;
  case 1108980984: return 100185;
  case 1108986857: return 100186;
  case 1108992007: return 100187;
  case 1108997363: return 100188;
  case 1109002538: return 100189;
  case 1109007249: return 100190;
  case 1109014272: return 100191;
  case 1109019996: return 100192;
  case 1109025893: return 100193;
  case 1109033635: return 100194;
  case 1109040254: return 100195;
  case 1109043030: return 100196;
  case 1109051081: return 100197;
  case 1109061360: return 100198;
  case 1109072318: return 100199;
  case 1109085550: return 100200;
  case 1109094129: return 100201;
  case 1109099846: return 100202;
  case 1109105709: return 100203;
  case 1109112790: return 100204;
  case 1109128893: return 100205;
  case 1109138010: return 100206;
  case 1109145803: return 100207;
  case 1109153397: return 100208;
  case 1109156821: return 100209;
  case 1109156945: return 100210;
  case 1109164943: return 100211;
  case 1109171118: return 100212;
  case 1109176810: return 100213;
  case 1109185091: return 100214;
  case 1109191123: return 100215;
  case 1109196539: return 100216;
  case 1109202094: return 100217;
  case 1109210999: return 100218;
  case 1109218740: return 100219;
  case 1109224728: return 100220;
  case 1109230277: return 100221;
  case 1109236156: return 100222;
  case 1109244527: return 100223;
  case 1109250342: return 100224;
  };

  return 0;
}

DaqRunType FixDaq::runNumberToRunType(unsigned r) {
  switch (r) {

  // Cosmics, Ecole Polytechnique, Dec 2004 - Jan 2005
  case  80000: return DaqRunType(DaqRunType::daqTest,0);
  case  80001: return DaqRunType(DaqRunType::daqTest,0);
  case  80002: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80003: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80004: return DaqRunType(DaqRunType::daqTest,0);
  case  80005: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80006: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80007: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80008: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80009: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80010: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80011: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80012: return DaqRunType(DaqRunType::daqTest,0);
  case  80013: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80014: return DaqRunType(DaqRunType::emcCosmicsHoldScan,0);
  case  80015: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80016: return DaqRunType(DaqRunType::emcCosmicsHoldScan,0);
  case  80017: return DaqRunType(DaqRunType::emcCosmicsHoldScan,0);
  case  80018: return DaqRunType(DaqRunType::emcCosmicsHoldScan,0);
  case  80019: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80020: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80021: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80022: return DaqRunType(DaqRunType::emcVfeDac,0);
  case  80023: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80024: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80025: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80026: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80027: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80028: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80029: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80030: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80031: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80032: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80033: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80034: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80035: return DaqRunType(DaqRunType::emcCosmics,0);
  case  80036: return DaqRunType(DaqRunType::daqTest,0);
    

  // Electron beam, DESY, Jan - Mar 2005
  case  99975: return DaqRunType(DaqRunType::daqTest,0);
  case  99976: return DaqRunType(DaqRunType::daqTest,0);
  case  99977: return DaqRunType(DaqRunType::daqTest,0);
  case  99978: return DaqRunType(DaqRunType::daqTest,0);
  case  99979: return DaqRunType(DaqRunType::daqTest,0);
  case  99980: return DaqRunType(DaqRunType::daqTest,0);
  case  99981: return DaqRunType(DaqRunType::daqTest,0);
  case  99982: return DaqRunType(DaqRunType::daqTest,0);
  case  99983: return DaqRunType(DaqRunType::daqTest,0);
  case  99984: return DaqRunType(DaqRunType::daqTest,0);
  case  99985: return DaqRunType(DaqRunType::daqTest,0);
  case  99986: return DaqRunType(DaqRunType::daqTest,0);
  case  99987: return DaqRunType(DaqRunType::daqTest,0);
  case  99988: return DaqRunType(DaqRunType::daqTest,0);
  case  99989: return DaqRunType(DaqRunType::daqTest,0);
  case  99990: return DaqRunType(DaqRunType::daqTest,0);
  case  99991: return DaqRunType(DaqRunType::daqTest,0);
  case  99992: return DaqRunType(DaqRunType::emcBeam,0);
  case  99993: return DaqRunType(DaqRunType::emcBeam,1);
  case  99994: return DaqRunType(DaqRunType::emcBeam,1);
  case  99995: return DaqRunType(DaqRunType::emcBeam,1);
  case  99996: return DaqRunType(DaqRunType::emcBeam,1);
  case  99997: return DaqRunType(DaqRunType::daqTest,0);
  case  99998: return DaqRunType(DaqRunType::daqTest,0);
  case  99999: return DaqRunType(DaqRunType::daqTest,0);
  case 100000: return DaqRunType(DaqRunType::emcBeam,2);
  case 100002: return DaqRunType(DaqRunType::emcBeam,1);
  case 100010: return DaqRunType(DaqRunType::emcBeam,1);
  case 100030: return DaqRunType(DaqRunType::emcBeam,1);
  case 100031: return DaqRunType(DaqRunType::emcBeam,1);
  case 100032: return DaqRunType(DaqRunType::emcBeam,1);
  case 100033: return DaqRunType(DaqRunType::emcBeam,1);
  case 100039: return DaqRunType(DaqRunType::emcBeam,1);
  case 100040: return DaqRunType(DaqRunType::emcBeam,1);
  case 100041: return DaqRunType(DaqRunType::emcBeam,1);
  case 100043: return DaqRunType(DaqRunType::emcBeam,1);
  case 100044: return DaqRunType(DaqRunType::emcBeam,1);
  case 100049: return DaqRunType(DaqRunType::emcBeam,1);
  case 100050: return DaqRunType(DaqRunType::emcBeam,1);
  case 100051: return DaqRunType(DaqRunType::emcBeam,1);
  case 100052: return DaqRunType(DaqRunType::emcBeam,1);
  case 100053: return DaqRunType(DaqRunType::emcBeam,1);
  case 100054: return DaqRunType(DaqRunType::emcBeam,1);
  case 100055: return DaqRunType(DaqRunType::emcBeam,1);
  case 100056: return DaqRunType(DaqRunType::emcBeam,1);
  case 100057: return DaqRunType(DaqRunType::emcBeam,1);
  case 100058: return DaqRunType(DaqRunType::emcBeam,1);
  case 100059: return DaqRunType(DaqRunType::emcBeam,1);
  case 100060: return DaqRunType(DaqRunType::emcBeam,1);
  case 100061: return DaqRunType(DaqRunType::emcBeam,1);
  case 100062: return DaqRunType(DaqRunType::emcBeam,1);
  case 100063: return DaqRunType(DaqRunType::emcVfeDac,0);
  case 100064: return DaqRunType(DaqRunType::emcBeam,2);
  case 100065: return DaqRunType(DaqRunType::emcBeam,2);
  case 100066: return DaqRunType(DaqRunType::emcVfeDac,0);
  case 100067: return DaqRunType(DaqRunType::emcBeam,1);
  case 100068: return DaqRunType(DaqRunType::emcBeam,1);
  case 100069: return DaqRunType(DaqRunType::emcBeam,1);
  case 100070: return DaqRunType(DaqRunType::emcBeam,1);
  case 100071: return DaqRunType(DaqRunType::emcBeam,1);
  case 100072: return DaqRunType(DaqRunType::emcBeam,1);
  case 100073: return DaqRunType(DaqRunType::emcBeam,1);
  case 100074: return DaqRunType(DaqRunType::emcBeam,1);
  case 100075: return DaqRunType(DaqRunType::emcBeam,1);
  case 100076: return DaqRunType(DaqRunType::emcBeam,1);
  case 100077: return DaqRunType(DaqRunType::emcBeam,1);
  case 100078: return DaqRunType(DaqRunType::emcBeam,1);
  case 100079: return DaqRunType(DaqRunType::emcBeam,1);
  case 100080: return DaqRunType(DaqRunType::emcBeam,1);
  case 100081: return DaqRunType(DaqRunType::emcBeam,1);
  case 100082: return DaqRunType(DaqRunType::emcBeam,1);
  case 100083: return DaqRunType(DaqRunType::emcBeam,1);
  case 100084: return DaqRunType(DaqRunType::emcBeam,1);
  case 100085: return DaqRunType(DaqRunType::emcBeam,1);
  case 100086: return DaqRunType(DaqRunType::emcBeam,1);
  case 100087: return DaqRunType(DaqRunType::emcBeam,1);
  case 100088: return DaqRunType(DaqRunType::emcBeam,1);
  case 100089: return DaqRunType(DaqRunType::emcBeam,1);
  case 100090: return DaqRunType(DaqRunType::emcVfeDac,0);
  case 100091: return DaqRunType(DaqRunType::emcVfeDac,0);
  case 100092: return DaqRunType(DaqRunType::emcBeam,1);
  case 100093: return DaqRunType(DaqRunType::emcBeam,1);
  case 100094: return DaqRunType(DaqRunType::emcBeam,1);
  case 100095: return DaqRunType(DaqRunType::emcBeam,2);
  case 100096: return DaqRunType(DaqRunType::emcBeam,2);
  case 100097: return DaqRunType(DaqRunType::emcBeam,2);
  case 100098: return DaqRunType(DaqRunType::emcBeam,2);
  case 100099: return DaqRunType(DaqRunType::emcBeam,1);
  case 100100: return DaqRunType(DaqRunType::emcBeam,1);
  case 100101: return DaqRunType(DaqRunType::emcBeam,1);
  case 100102: return DaqRunType(DaqRunType::emcBeam,1);
  case 100103: return DaqRunType(DaqRunType::emcBeam,1);
  case 100104: return DaqRunType(DaqRunType::emcBeam,1);
  case 100105: return DaqRunType(DaqRunType::emcBeam,1);
  case 100106: return DaqRunType(DaqRunType::emcBeam,1);
  case 100107: return DaqRunType(DaqRunType::emcBeam,1);
  case 100108: return DaqRunType(DaqRunType::emcBeam,1);
  case 100109: return DaqRunType(DaqRunType::emcBeam,1);
  case 100110: return DaqRunType(DaqRunType::emcBeam,1);
  case 100111: return DaqRunType(DaqRunType::emcBeam,2);
  case 100112: return DaqRunType(DaqRunType::emcBeam,2);
  case 100113: return DaqRunType(DaqRunType::emcBeam,2);
  case 100114: return DaqRunType(DaqRunType::emcBeam,2);
  case 100115: return DaqRunType(DaqRunType::emcBeam,2);
  case 100116: return DaqRunType(DaqRunType::emcBeam,2);
  case 100117: return DaqRunType(DaqRunType::emcBeam,2);
  case 100118: return DaqRunType(DaqRunType::emcBeam,2);
  case 100119: return DaqRunType(DaqRunType::emcBeam,2);
  case 100120: return DaqRunType(DaqRunType::emcBeam,2);
  case 100121: return DaqRunType(DaqRunType::emcBeam,2);
  case 100122: return DaqRunType(DaqRunType::emcBeam,2);
  case 100123: return DaqRunType(DaqRunType::emcBeam,2);
  case 100124: return DaqRunType(DaqRunType::emcBeam,2);
  case 100125: return DaqRunType(DaqRunType::emcBeam,2);
  case 100126: return DaqRunType(DaqRunType::emcBeam,2);
  case 100127: return DaqRunType(DaqRunType::emcBeam,2);
  case 100128: return DaqRunType(DaqRunType::emcBeam,2);
  case 100129: return DaqRunType(DaqRunType::emcBeam,2);
  case 100130: return DaqRunType(DaqRunType::emcBeam,2);
  case 100131: return DaqRunType(DaqRunType::emcBeam,2);
  case 100132: return DaqRunType(DaqRunType::emcBeam,2);
  case 100133: return DaqRunType(DaqRunType::emcBeam,2);
  case 100134: return DaqRunType(DaqRunType::emcBeam,3);
  case 100135: return DaqRunType(DaqRunType::emcBeam,3);
  case 100136: return DaqRunType(DaqRunType::emcBeam,3);
  case 100137: return DaqRunType(DaqRunType::emcBeam,3);
  case 100138: return DaqRunType(DaqRunType::emcBeam,3);
  case 100139: return DaqRunType(DaqRunType::emcBeam,3);
  case 100140: return DaqRunType(DaqRunType::emcBeam,3);
  case 100141: return DaqRunType(DaqRunType::emcBeam,3);
  case 100142: return DaqRunType(DaqRunType::emcBeam,3);
  case 100143: return DaqRunType(DaqRunType::emcBeam,3);
  case 100144: return DaqRunType(DaqRunType::emcBeam,3);
  case 100145: return DaqRunType(DaqRunType::emcBeam,3);
  case 100146: return DaqRunType(DaqRunType::emcBeam,3);
  case 100147: return DaqRunType(DaqRunType::emcBeam,3);
  case 100148: return DaqRunType(DaqRunType::emcBeam,3);
  case 100149: return DaqRunType(DaqRunType::emcBeam,3);
  case 100150: return DaqRunType(DaqRunType::emcBeam,3);
  case 100151: return DaqRunType(DaqRunType::emcBeam,3);
  case 100152: return DaqRunType(DaqRunType::emcBeam,3);
  case 100153: return DaqRunType(DaqRunType::emcBeam,3);
  case 100154: return DaqRunType(DaqRunType::emcBeam,3);
  case 100155: return DaqRunType(DaqRunType::emcBeam,3);
  case 100156: return DaqRunType(DaqRunType::emcBeam,3);
  case 100157: return DaqRunType(DaqRunType::emcBeam,3);
  case 100158: return DaqRunType(DaqRunType::emcBeam,3);
  case 100159: return DaqRunType(DaqRunType::emcBeam,3);
  case 100160: return DaqRunType(DaqRunType::emcBeam,3);
  case 100161: return DaqRunType(DaqRunType::emcBeam,3);
  case 100162: return DaqRunType(DaqRunType::emcBeam,3);
  case 100163: return DaqRunType(DaqRunType::emcBeam,3);
  case 100164: return DaqRunType(DaqRunType::emcBeam,3);
  case 100165: return DaqRunType(DaqRunType::emcBeam,3);
  case 100166: return DaqRunType(DaqRunType::emcBeam,3);
  case 100167: return DaqRunType(DaqRunType::emcBeam,3);
  case 100168: return DaqRunType(DaqRunType::emcBeam,3);
  case 100169: return DaqRunType(DaqRunType::emcBeam,3);
  case 100170: return DaqRunType(DaqRunType::emcBeam,3);
  case 100171: return DaqRunType(DaqRunType::emcBeam,3);
  case 100172: return DaqRunType(DaqRunType::emcBeam,3);
  case 100173: return DaqRunType(DaqRunType::emcBeam,3);
  case 100174: return DaqRunType(DaqRunType::emcBeam,3);
  case 100175: return DaqRunType(DaqRunType::emcBeam,3);
  case 100176: return DaqRunType(DaqRunType::emcBeam,3);
  case 100177: return DaqRunType(DaqRunType::emcBeam,3);
  case 100178: return DaqRunType(DaqRunType::emcBeam,3);
  case 100179: return DaqRunType(DaqRunType::emcBeam,3);
  case 100180: return DaqRunType(DaqRunType::emcBeam,3);
  case 100181: return DaqRunType(DaqRunType::emcBeam,3);
  case 100182: return DaqRunType(DaqRunType::emcBeam,3);
  case 100183: return DaqRunType(DaqRunType::emcBeam,3);
  case 100184: return DaqRunType(DaqRunType::emcBeam,3);
  case 100185: return DaqRunType(DaqRunType::emcBeam,3);
  case 100186: return DaqRunType(DaqRunType::emcBeam,3);
  case 100187: return DaqRunType(DaqRunType::emcBeam,3);
  case 100188: return DaqRunType(DaqRunType::emcBeam,3);
  case 100189: return DaqRunType(DaqRunType::emcBeam,3);
  case 100190: return DaqRunType(DaqRunType::emcBeam,3);
  case 100191: return DaqRunType(DaqRunType::emcBeam,3);
  case 100192: return DaqRunType(DaqRunType::emcBeam,3);
  case 100193: return DaqRunType(DaqRunType::emcBeam,3);
  case 100194: return DaqRunType(DaqRunType::emcBeam,1);
  case 100195: return DaqRunType(DaqRunType::emcBeam,1);
  case 100196: return DaqRunType(DaqRunType::emcBeam,3);
  case 100197: return DaqRunType(DaqRunType::emcBeam,3);
  case 100198: return DaqRunType(DaqRunType::emcBeam,3);
  case 100199: return DaqRunType(DaqRunType::emcBeam,3);
  case 100200: return DaqRunType(DaqRunType::emcBeam,3);
  case 100201: return DaqRunType(DaqRunType::emcBeam,3);
  case 100202: return DaqRunType(DaqRunType::emcBeam,3);
  case 100203: return DaqRunType(DaqRunType::emcBeam,3);
  case 100204: return DaqRunType(DaqRunType::emcBeam,3);
  case 100205: return DaqRunType(DaqRunType::emcBeam,3);
  case 100206: return DaqRunType(DaqRunType::emcBeam,3);
  case 100207: return DaqRunType(DaqRunType::emcBeam,3);
  case 100208: return DaqRunType(DaqRunType::emcBeam,3);
  case 100209: return DaqRunType(DaqRunType::emcBeam,3);
  case 100210: return DaqRunType(DaqRunType::emcBeam,3);
  case 100211: return DaqRunType(DaqRunType::emcBeam,3);
  case 100212: return DaqRunType(DaqRunType::emcBeam,3);
  case 100213: return DaqRunType(DaqRunType::emcBeam,3);
  case 100214: return DaqRunType(DaqRunType::emcBeam,3);
  case 100215: return DaqRunType(DaqRunType::emcBeam,3);
  case 100216: return DaqRunType(DaqRunType::emcBeam,3);
  case 100217: return DaqRunType(DaqRunType::emcBeam,3);
  case 100218: return DaqRunType(DaqRunType::emcBeam,3);
  case 100219: return DaqRunType(DaqRunType::emcBeam,3);
  case 100220: return DaqRunType(DaqRunType::emcBeam,3);
  case 100221: return DaqRunType(DaqRunType::emcBeam,3);
  case 100222: return DaqRunType(DaqRunType::emcBeam,3);
  case 100223: return DaqRunType(DaqRunType::emcBeam,3);
  case 100224: return DaqRunType(DaqRunType::emcBeam,3);
  };

  return DaqRunType();
}


#endif
#endif
