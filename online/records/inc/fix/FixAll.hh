#ifndef FixAll_HH
#define FixAll_HH

// records/inc/fix
#include "FixDaq.hh"
#include "FixCrc.hh"
#include "FixEmc.hh"
#include "FixAhc.hh"

class RcdRecord;

class FixAll {

public:
  FixAll();
  FixAll(unsigned p);

  bool fix(RcdRecord &r);
  //Complete copy of fix + little extra functionality "required" by the converter
  bool fix_paranoia(RcdRecord &r, int&);

private:
  FixDaq _fixDaq;
  FixCrc _fixCrc;
  FixEmc _fixEmc;
  FixAhc _fixAhc;
};


#ifdef CALICE_DAQ_ICC

FixAll::FixAll() : _fixDaq(), _fixCrc(), _fixEmc(), _fixAhc() {
}

FixAll::FixAll(unsigned p) : _fixDaq(p), _fixCrc(p), _fixEmc(p), _fixAhc(p) {
}

bool FixAll::fix(RcdRecord &r) {
  bool reply(true);

  if(!_fixDaq.fix(r)) reply=false;
  if(!_fixCrc.fix(r)) reply=false;
  if(!_fixEmc.fix(r)) reply=false;
  if(!_fixAhc.fix(r)) reply=false;
  
  return reply;
}

bool FixAll::fix_paranoia(RcdRecord &r, int &runNumber) {
  bool reply(true);

  //CRP Hack pass runnumber to daq fixer 
  if(_fixDaq.fix_paranoia(r, runNumber)) reply=false;
  //if(!_fixDaq.fix(r)) reply=false;
  //if(!_fixCrc.fix(r)) reply=false;
  if(!_fixEmc.fix(r)) reply=false;
  if(!_fixAhc.fix(r)) reply=false;
  
  return reply;
}



#endif
#endif
