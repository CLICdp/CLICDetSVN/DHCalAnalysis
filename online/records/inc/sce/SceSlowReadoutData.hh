#ifndef SceSlowReadoutData_HH
#define SceSlowReadoutData_HH

#include <string>
#include <iostream>

//The sizes of the arrays which contain the sro information
#define NSCECMBTEMPS 2
#define NSCECMBVOLTS 9
#define NSCECMBSIZES 2
#define NSCEHBABVOLTS 1
#define NSCEHBABTEMPS 1

#include "UtlMapDefinitions.hh"


#ifdef IS_THIS_NEEDED_HERE_OR_IN_CONVERTER
class SceSlowReadoutDataV1; // DOES NOT EXIST

typedef const double* const (SceSlowReadoutDataV1::* DblFunction_Ptr)() const;
typedef const int* const (SceSlowReadoutDataV1::* UIntFunction_Ptr)() const;
typedef const float* const (SceSlowReadoutDataV1::* FloatFunction_Ptr)() const;

/** Relation between the datatype and the function to retrieve it*/
typedef std::map<std::pair<std::string, unsigned int>, DblFunction_Ptr> SceSroTypesDblMap_t;
//typedef std::map<std::pair<std::string, unsigned int>, const double* const (AhcSlowReadoutDataV1::*)() const> BmlSroTypesDblMap_t;
typedef std::map<std::pair<std::string, unsigned int>, UIntFunction_Ptr> SceSroTypesUIntMap_t;
typedef std::map<std::pair<std::string, unsigned int>, FloatFunction_Ptr> SceSroTypesFloatMap_t;
#endif

#pragma pack (4)

class SceSlowReadoutData {

public:
  enum {
    versionNumber=0
  };

  SceSlowReadoutData();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  unsigned moduleNumber() const;

  std::vector<double> getCmbTemperatures() const;

  std::vector<double>  getCmbVoltages() const;

  unsigned getLedSetting() const; 

  std::vector<unsigned>  getCmbSizes() const;

  double getHbabVoltage() const;
  std::vector<double>  getHbabVoltages() const;

  double getHbabCurrent() const;
  std::vector<double>  getHbabCurrents() const;

  DaqTypesDblMap_t getDblMap() const;
  DaqTypesUIntMap_t getUIntMap() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  unsigned _moduleNumber;
  double _cmbTemperatures[2];
  double _cmbVoltages[9];
  unsigned _ledSetting;
  unsigned _cmbSizes[2];
  double _hbabVoltage;
  double _hbabCurrent;
};

#pragma pack ()

#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


SceSlowReadoutData::SceSlowReadoutData() {
  memset(this,0,sizeof(SceSlowReadoutData));
}

bool SceSlowReadoutData::parse(std::string r) {
  memset(this,0,sizeof(SceSlowReadoutData));
  _timeStamp=0xffffffff;
  _moduleNumber=255;

  std::istringstream sin(r);
  if(!sin) return false;

  sin >> _timeStamp;
  if(!sin) return false;

  sin >> _moduleNumber;
  if(!sin) return false;

  for(unsigned i(0);i<2;i++) {
    sin >> _cmbTemperatures[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<9;i++) {
    sin >> _cmbVoltages[i];
    if(!sin) return false;
  }

  sin >> std::hex >> _ledSetting >> std::dec;
  if(!sin) return false;

  for(unsigned i(0);i<2;i++) {
    sin >> _cmbSizes[i];
    if(!sin) return false;
  }

  sin >> _hbabVoltage;
  if(!sin) return false;

  sin >> _hbabCurrent;
  if(!sin) return false;

  return true;
}

time_t SceSlowReadoutData::timeStamp() const {
  return _timeStamp;
}

void SceSlowReadoutData::timeStamp(time_t t) {
  _timeStamp=t;
}

unsigned SceSlowReadoutData::moduleNumber() const {
  return _moduleNumber;
}
std::vector<double> SceSlowReadoutData::getCmbTemperatures() const {
  std::vector<double> v;

  for(unsigned i(0);i<2;i++) {
    v.push_back(_cmbTemperatures[i]);
  }
  
  return v;
}

std::vector<double> SceSlowReadoutData::getCmbVoltages() const {
  std::vector<double> v;

  for(unsigned i(0);i<9;i++) {
    v.push_back(_cmbVoltages[i]);
  }
  
  return v;
}

unsigned SceSlowReadoutData::getLedSetting() const{
 return _ledSetting;
}

std::vector<unsigned> SceSlowReadoutData::getCmbSizes() const{
  std::vector<unsigned> v;
  for(unsigned i(0);i<2;i++) {
    v.push_back(_cmbSizes[i]);
  }
  return v;
}

double SceSlowReadoutData::getHbabVoltage() const{
  return _hbabVoltage;
}

std::vector<double> SceSlowReadoutData::getHbabVoltages() const{
  std::vector<double> v;
  v.push_back(getHbabVoltage());
  return v;
}


double SceSlowReadoutData::getHbabCurrent() const{
  return _hbabCurrent;
}

std::vector<double> SceSlowReadoutData::getHbabCurrents() const{
  std::vector<double> v;
  v.push_back(getHbabCurrent());
  return v;
}

DaqTypesDblMap_t SceSlowReadoutData::getDblMap() const {

  //Initialize the map between types and access functions in the DAQ class - doubles
  //std::cout << "In get double maps: " << &_theDblTypes << std::endl;

  DaqTypesDblMap_t _theDblTypes;
  _theDblTypes.clear();

  if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CMBTEMPERATURES", getCmbTemperatures())).second) 
    std::cout << "Problems with inserting CMBTEMPERATURES into map" << std::endl;  
  if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CMBVOLTAGES", getCmbVoltages())).second)
    std::cout << "Problems with inserting CMBVOLTAGES into map" << std::endl;  
  if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("HBABVOLTAGES", getHbabVoltages())).second)
    std::cout << "Problems with inserting HBABVOLTAGES into map" << std::endl;  
  if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("HBABCURRENTS", getHbabCurrents())).second)
    std::cout << "Problems with inserting HBABCURRENTS into map" << std::endl;  

  return _theDblTypes;
}

DaqTypesUIntMap_t SceSlowReadoutData::getUIntMap() const {
  DaqTypesUIntMap_t _theUIntTypes;
  _theUIntTypes.clear();

  if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned> >("CMBSIZES", getCmbSizes())).second)
    std::cout << "Problems with inserting CMBSIZES into map" << std::endl;  

  return _theUIntTypes;
}

std::ostream& SceSlowReadoutData::print(std::ostream &o, std::string s) const {
  o << s << "SceSlowReadoutData::print()" << std::endl;

  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " Module number = " << _moduleNumber << std::endl;

  o << s << " CMB temperature lower = " << std::setw(5)
    << _cmbTemperatures[0] << " C" << std::endl;
  o << s << " CMB temperature upper = " << std::setw(5)
    << _cmbTemperatures[1] << " C" << std::endl;

  o << s << " CMB calib U041 voltage = " << _cmbVoltages[0] << " V" << std::endl;
  o << s << " CMB 12V power  voltage = " << _cmbVoltages[1] << " V" << std::endl;
  o << s << " CMB 1.235V     voltage = " << _cmbVoltages[2] << " V" << std::endl;
  o << s << " CMB VLDA upper voltage = " << _cmbVoltages[3] << " V" << std::endl;
  o << s << " CMB VLDB upper voltage = " << _cmbVoltages[4] << " V" << std::endl;
  o << s << " CMB VLDC upper voltage = " << _cmbVoltages[5] << " V" << std::endl;
  o << s << " CMB VLDD lower voltage = " << _cmbVoltages[6] << " V" << std::endl;
  o << s << " CMB 10V bias   voltage = " << _cmbVoltages[7] << " V" << std::endl;
  o << s << " CMB calib U051 voltage = " << _cmbVoltages[8] << " V" << std::endl;

  o << s << " LED setting = " << printHex(_ledSetting) << std::endl;

  o << s << " LED pulser width  = " << std::setw(3) << _cmbSizes[0] << std::endl;
  o << s << " LED pulser height = " << std::setw(3) << _cmbSizes[1] << std::endl;

  o << s << " HBAB HV voltage  = " << _hbabVoltage << " V" << std::endl;
  o << s << " HBAB HV current  = " << _hbabCurrent << " A" << std::endl;

  return o;
}

#endif
#endif
