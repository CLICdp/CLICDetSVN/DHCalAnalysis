#ifndef SceSlowTemperatureData_HH
#define SceSlowTemperatureData_HH

#include <string>
#include <iostream>

#include "UtlMapDefinitions.hh"

#pragma pack (4)

class SceSlowTemperatureData {

public:
  enum {
    versionNumber=0
  };

  SceSlowTemperatureData();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  std::vector<double> getTemperatures() const;

  // Access functions to retrieve the maps containing the different values
  DaqTypesDblMap_t getDblMap() const;
  DaqTypesUIntMap_t getUIntMap() const;
  DaqTypesFloatMap_t getFloatMap() const;

  //Return a map of time stamps
  std::map<std::string,time_t> getTimeStamps() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  double _temperatures[8];
};

#pragma pack ()

#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


SceSlowTemperatureData::SceSlowTemperatureData() {
  memset(this,0,sizeof(SceSlowTemperatureData));
}

bool SceSlowTemperatureData::parse(std::string r) {
  memset(this,0,sizeof(SceSlowTemperatureData));
  _timeStamp=0xffffffff;

  std::istringstream sin(r);
  if(!sin) return false;

  sin >> _timeStamp;
  if(!sin) return false;

  for(unsigned i(0);i<8;i++) {
    sin >> _temperatures[i];
    if(!sin) return false;
  }

  return true;
}

time_t SceSlowTemperatureData::timeStamp() const {
  return _timeStamp;
}

void SceSlowTemperatureData::timeStamp(time_t t) {
  _timeStamp=t;
}

std::vector<double> SceSlowTemperatureData::getTemperatures() const {
  std::vector<double> v;

  for(unsigned i(0);i<8;i++) {
    v.push_back(_temperatures[i]);
  }
  
  return v;
}

DaqTypesDblMap_t SceSlowTemperatureData::getDblMap() const {

  //Initialize the map between types and access functions in the DAQ class - doubles
  DaqTypesDblMap_t _theDblTypes;
  _theDblTypes.clear();
  std::vector<double> v;

  if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("TEMPERATURES", getTemperatures())).second) 
    std::cout << "Problems with inserting SCE TEMPERATURES into map" << std::endl;  

  return _theDblTypes;
} 

DaqTypesUIntMap_t SceSlowTemperatureData::getUIntMap() const {
  DaqTypesUIntMap_t _theUIntTypes;
  _theUIntTypes.clear();
  return _theUIntTypes;
}
  
DaqTypesFloatMap_t SceSlowTemperatureData::getFloatMap() const {
  DaqTypesFloatMap_t _theFloatTypes;
  _theFloatTypes.clear();
  return _theFloatTypes;
}

std::map<std::string,time_t> SceSlowTemperatureData::getTimeStamps() const {
  std::map<std::string,time_t> _theTimeStamps;  
  _theTimeStamps.clear(); 

  if(!_theTimeStamps.insert(std::pair<std::string, time_t >("DAQTIMESTAMP", timeStamp())).second)
    std::cout << "Problems with inserting DAQTIMESTAMP into map" << std::endl;    

  return _theTimeStamps;
}

std::ostream& SceSlowTemperatureData::print(std::ostream &o, std::string s) const {
  o << s << "SceSlowTemperatureData::print()" << std::endl;

  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);

  for(unsigned i(0);i<8;i++) {
    o << s << " Temperature (voltage) " << i << " = " << std::setw(5)
      << _temperatures[i] << " V" << std::endl;
  }

  return o;
}

#endif
#endif
