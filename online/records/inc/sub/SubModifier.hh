#ifndef SubModifier_HH
#define SubModifier_HH

#include <vector>
#include <string>
#include <iostream>

#include "SubHeader.hh"
#include "SubRecordType.hh"

class RcdRecord;


class SubModifier {

public:
  SubModifier(RcdRecord &r);

  std::vector<SubHeader*> access(unsigned t=0xffffffff) const;
  template<class Payload> std::vector<Payload*> access() const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  RcdRecord &_record;
};

template<class Payload> std::vector<Payload*> SubModifier::access() const {
  
  std::vector<SubHeader*> v(access(subRecordType<Payload>()));
  
  std::vector<Payload*> w(v.size());
  for(unsigned i(0);i<v.size();i++) {
    w[i]=(Payload*)v[i]->data();
  }
  
  return w;
}


#ifdef CALICE_DAQ_ICC

#include "RcdRecord.hh"


SubModifier::SubModifier(RcdRecord &r) : _record(r) {
}

std::vector<SubHeader*> SubModifier::access(unsigned t) const {
  std::vector<SubHeader*> v;
  
  SubHeader *h((SubHeader*)_record.data());
  SubHeader *g((SubHeader*)_record.newData());
  
  for(;h!=0 && h<g;h=h->nextSubHeader()) {
    if(h->subRecordType()==t || t>0xffff) v.push_back(h);
  }
  
  return v;
}

std::ostream& SubModifier::print(std::ostream &o, std::string s) const {
  std::vector<SubHeader*> v(access());
  
  o << s << "SubModifier::print()  Number of subrecords = "
    << v.size() << std::endl;
  
  for(unsigned i(0);i<v.size();i++) {
    v[i]->print(o,s+" ");
  }
  
  return o;
}

#endif
#endif
