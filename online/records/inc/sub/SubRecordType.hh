#ifndef SubRecordType_HH
#define SubRecordType_HH

#include <string>

template<class Payload> unsigned subRecordType();

// record/inc/daq
#include "DaqRunStart.hh"
#include "DaqSequenceStart.hh"

#include "DaqConfigurationStartV0.hh"
#include "DaqConfigurationStartV1.hh"
#include "DaqConfigurationStartV2.hh"
#include "DaqConfigurationStart.hh"

#include "DaqAcquisitionStartV0.hh"
#include "DaqAcquisitionStartV1.hh"
#include "DaqAcquisitionStart.hh"

#include "DaqSlowReadout.hh"
#include "DaqSpillStart.hh"
#include "DaqTrigger.hh"
#include "DaqBurstStart.hh"
#include "DaqBurstEnd.hh"

#include "DaqTransferStart.hh"
#include "DaqEvent.hh"
#include "DaqSpillEnd.hh"
#include "DaqTransferEnd.hh"

#include "DaqAcquisitionEndV0.hh"
#include "DaqAcquisitionEndV1.hh"
#include "DaqAcquisitionEnd.hh"

#include "DaqConfigurationEndV0.hh"
#include "DaqConfigurationEndV1.hh"
#include "DaqConfigurationEnd.hh"

#include "DaqRunEndV0.hh"
#include "DaqRunEndV1.hh"
#include "DaqRunEnd.hh"
#include "DaqSequenceEnd.hh"

#include "DaqMessage.hh"
#include "DaqSoftware.hh"
#include "DaqTwoTimer.hh"
#include "DaqMultiTimer.hh"

#include "IlcRunStart.hh"
#include "IlcConfigurationStart.hh"
#include "IlcSlowReadout.hh"
#include "IlcBunchTrain.hh"
#include "IlcConfigurationEnd.hh"
#include "IlcRunEnd.hh"
#include "IlcAbort.hh"


// record/inc/crc
#include "CrcLocationData.hh"

#include "CrcVmeRunData.hh"
#include "CrcVmeConfigurationData.hh"
#include "CrcVmeEventData.hh"

#include "CrcBeRunData.hh"
#include "CrcBeConfigurationDataV0.hh"
#include "CrcBeConfigurationDataV1.hh"
#include "CrcBeConfigurationData.hh"
#include "CrcBeEventDataV0.hh"
#include "CrcBeEventDataV1.hh"
#include "CrcBeEventData.hh"

#include "CrcFeRunData.hh"
#include "CrcFeConfigurationData.hh"
#include "CrcFeEventData.hh"

#include "CrcFeFakeEventData.hh"
#include "CrcVlinkEventData.hh"

#include "CrcLm82RunData.hh"
#include "CrcLm82ConfigurationData.hh"
#include "CrcLm82SlowReadoutData.hh"

#include "CrcAdm1025RunData.hh"
#include "CrcAdm1025ConfigurationData.hh"
#include "CrcAdm1025SlowReadoutData.hh"

#include "CrcReadoutConfigurationDataV0.hh"
#include "CrcReadoutConfigurationDataV1.hh"
#include "CrcReadoutConfigurationData.hh"

#include "CrcBeTrgRunData.hh"
#include "CrcBeTrgConfigurationDataV0.hh"
#include "CrcBeTrgConfigurationDataV1.hh"
#include "CrcBeTrgConfigurationData.hh"
#include "CrcBeTrgEventData.hh"
#include "CrcBeTrgPollDataV0.hh"
#include "CrcBeTrgPollDataV1.hh"
#include "CrcBeTrgPollData.hh"
#include "TrgSpillPollData.hh"

#include "TrgReadoutConfigurationData.hh"

// record/inc/emc
#include "EmcFeConfigurationData.hh"
#include "EmcStageRunData.hh"

// record/inc/ahc
#include "AhcFeConfigurationDataV0.hh"
#include "AhcFeConfigurationDataV1.hh"
#include "AhcFeConfigurationData.hh"

#include "AhcVfeStartUpDataFine.hh"
#include "AhcVfeStartUpDataCoarse.hh"
#include "AhcVfeStartUpData.hh" // TEMP!!!
#include "AhcVfeConfigurationDataFine.hh"
#include "AhcVfeConfigurationDataCoarse.hh"
#include "AhcVfeConfigurationData.hh" // TEMP!!!

#include "AhcSlowRunDataV0.hh"
#include "AhcSlowRunDataV1.hh"
#include "AhcSlowRunData.hh"
#include "AhcSlowConfigurationData.hh"
#include "AhcSlowConfigurationDataV0.hh"
#include "AhcSlowConfigurationDataV1.hh"
#include "AhcSlowReadoutDataV0.hh"
#include "AhcSlowReadoutDataV1.hh"
#include "AhcSlowReadoutData.hh"
#include "AhcMapping.hh"

// record/inc/sce
#include "SceSlowReadoutData.hh"
#include "SceSlowTemperatureData.hh"

// record/inc/dhc
#include "DhcLocationData.hh"
#include "TtmLocationData.hh"

#include "DhcTriggerConfigurationData.hh"
#include "DhcReadoutConfigurationData.hh"
#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcDcRunData.hh"
#include "DhcFeConfigurationDataV0.hh"
#include "DhcFeConfigurationDataV1.hh"
#include "DhcFeConfigurationData.hh"
#include "DhcEventDataV0.hh"
#include "DhcEventDataV1.hh"
#include "DhcEventData.hh"
#include "DhcTriggerData.hh"
#include "TtmConfigurationDataV0.hh"
#include "TtmConfigurationDataV1.hh"
#include "TtmConfigurationData.hh"
#include "TtmTriggerData.hh"

// record/inc/dhe
//#include "DheRunData.hh"
#include "DheVfeConfigurationData4.hh"
#include "DheTriggerData.hh"
#include "DheEventData.hh"

#include "DheDifRunData.hh"
#include "DheDifConfigurationData.hh"
#include "DheDifAcquisitionData.hh"
#include "DheDifEventData.hh"

// record/inc/bml
#include "BmlLocationData.hh"

#include "BmlLc1176RunData.hh"
#include "BmlLc1176ConfigurationData.hh"
#include "BmlLc1176EventData.hh"

#include "BmlCaen767RunData.hh"
#include "BmlCaen767OpcodeData.hh"
#include "BmlCaen767ConfigurationData.hh"
#include "BmlCaen767TestData.hh"
#include "BmlCaen767TriggerData.hh"
#include "BmlCaen767EventData.hh"
#include "BmlCaen767TdcErrorData.hh"
#include "BmlCaen767ReadoutConfigurationData.hh"

#include "BmlCaen1290RunData.hh"
#include "BmlCaen1290OpcodeData.hh"
#include "BmlCaen1290ConfigurationData.hh"
#include "BmlCaen1290TestData.hh"
#include "BmlCaen1290TriggerData.hh"
#include "BmlCaen1290EventData.hh"
#include "BmlCaen1290TdcErrorData.hh"
#include "BmlCaen1290ReadoutConfigurationData.hh"

#include "BmlHodRunData.hh"
#include "BmlHodEventData.hh"

#include "BmlCernSlowRunData.hh"
#include "BmlSlowRunData.hh"
#include "BmlFnalSlowRunData.hh"
#include "BmlFnalSlowReadoutDataV0.hh"
#include "BmlFnalSlowReadoutDataV1.hh"
#include "BmlFnalSlowReadoutData.hh"
#include "BmlFnalAcquisitionData.hh"

#include "BmlLalHodoscopeRunData.hh"
#include "BmlLalHodoscopeConfigurationData.hh"
#include "BmlLalHodoscopeTriggerData.hh"
#include "BmlLalHodoscopeEventData.hh"

// record/inc/mps
#include "MpsLocationData.hh"
#include "MpsReadoutConfigurationData.hh"
#include "MpsUsbDaqRunData.hh"
#include "MpsUsbDaqConfigurationData.hh"
#include "MpsUsbDaqMasterConfigurationData.hh"
#include "MpsUsbDaqSpillPollData.hh"
#include "MpsUsbDaqBunchTrainData.hh"
#include "MpsPcb1ConfigurationData.hh"
#include "MpsPcb1SlowReadoutData.hh"
#include "MpsSensorV10ConfigurationData.hh"
#include "MpsSensorV12ConfigurationData.hh"
#include "MpsSensor1ConfigurationData.hh"
#include "MpsSensor1BunchTrainData.hh"
#include "MpsLaserRunData.hh"
#include "MpsLaserConfigurationData.hh"
#include "MpsDigitisationData.hh"
#include "MpsAlignmentSensor.hh"
#include "MpsEudetBunchTrainData.hh"
#include "MpsEudetBunchTrainDataV0.hh"
#include "MpsEudetBunchTrainDataV1.hh"

// record/inc/sim
#include "SimParticle.hh"
#include "SimLayerTrack.hh"
#include "SimLayerHits.hh"
#include "SimPrimaryData.hh"
#include "SimDetectorData.hh"
#include "SimDetectorLayer.hh"
#include "SimDetectorEnergies.hh"

// record/inc/sub
#include "SubHeader.hh"


// DAQ

#ifdef CALICE_DAQ_ICC

template<> unsigned subRecordType<DaqRunStart>() {
  return SubHeader::daq+0x000+DaqRunStart::versionNumber;
}

template<> unsigned subRecordType<DaqRunEndV0>() {
  return SubHeader::daq+0x010+DaqRunEndV0::versionNumber;
}

template<> unsigned subRecordType<DaqRunEndV1>() {
  return SubHeader::daq+0x010+DaqRunEndV1::versionNumber;
}

template<> unsigned subRecordType<DaqConfigurationStartV0>() {
  return SubHeader::daq+0x020+DaqConfigurationStartV0::versionNumber;
}

template<> unsigned subRecordType<DaqConfigurationStartV1>() {
  return SubHeader::daq+0x020+DaqConfigurationStartV1::versionNumber;
}

template<> unsigned subRecordType<DaqConfigurationStartV2>() {
  return SubHeader::daq+0x020+DaqConfigurationStartV2::versionNumber;
}

template<> unsigned subRecordType<DaqConfigurationEndV0>() {
  return SubHeader::daq+0x030+DaqConfigurationEndV0::versionNumber;
}

template<> unsigned subRecordType<DaqConfigurationEndV1>() {
  return SubHeader::daq+0x030+DaqConfigurationEndV1::versionNumber;
}

template<> unsigned subRecordType<DaqAcquisitionStartV0>() {
  return SubHeader::daq+0x040+DaqAcquisitionStartV0::versionNumber;
}

template<> unsigned subRecordType<DaqAcquisitionStartV1>() {
  return SubHeader::daq+0x040+DaqAcquisitionStartV1::versionNumber;
}

template<> unsigned subRecordType<DaqAcquisitionEndV0>() {
  return SubHeader::daq+0x050+DaqAcquisitionEndV0::versionNumber;
}

template<> unsigned subRecordType<DaqAcquisitionEndV1>() {
  return SubHeader::daq+0x050+DaqAcquisitionEndV1::versionNumber;
}

template<> unsigned subRecordType<DaqEvent>() {
  return SubHeader::daq+0x060+DaqEvent::versionNumber;
}

template<> unsigned subRecordType<DaqSpillStart>() {
  return SubHeader::daq+0x070+DaqSpillStart::versionNumber;
}

template<> unsigned subRecordType<DaqSpillEnd>() {
  return SubHeader::daq+0x080+DaqSpillEnd::versionNumber;
}

template<> unsigned subRecordType<DaqTransferStart>() {
  return SubHeader::daq+0x090+DaqTransferStart::versionNumber;
}

template<> unsigned subRecordType<DaqTransferEnd>() {
  return SubHeader::daq+0x0a0+DaqTransferEnd::versionNumber;
}

template<> unsigned subRecordType<DaqMessage>() {
  return SubHeader::daq+0x0b0+DaqMessage::versionNumber;
}

template<> unsigned subRecordType<DaqSoftware>() {
  return SubHeader::daq+0x0c0+DaqSoftware::versionNumber;
}

template<> unsigned subRecordType<DaqSlowReadout>() {
  return SubHeader::daq+0x0d0+DaqSlowReadout::versionNumber;
}

template<> unsigned subRecordType<DaqSequenceStart>() {
  return SubHeader::daq+0x0e0+DaqSequenceStart::versionNumber;
}

template<> unsigned subRecordType<DaqSequenceEnd>() {
  return SubHeader::daq+0x0f0+DaqSequenceEnd::versionNumber;
}

template<> unsigned subRecordType<DaqTwoTimer>() {
  return SubHeader::daq+0x100+DaqTwoTimer::versionNumber;
}

template<> unsigned subRecordType<DaqMultiTimer>() {
  return SubHeader::daq+0x110+DaqMultiTimer::versionNumber;
}

template<> unsigned subRecordType<DaqTrigger>() {
  return SubHeader::daq+0x120+DaqTrigger::versionNumber;
}

template<> unsigned subRecordType<DaqBurstStart>() {
  return SubHeader::daq+0x130+DaqBurstStart::versionNumber;
}

template<> unsigned subRecordType<DaqBurstEnd>() {
  return SubHeader::daq+0x140+DaqBurstEnd::versionNumber;
}

template<> unsigned subRecordType<IlcRunStart>() {
  return SubHeader::daq+0x800+IlcRunStart::versionNumber;
}

template<> unsigned subRecordType<IlcRunEnd>() {
  return SubHeader::daq+0x810+IlcRunEnd::versionNumber;
}

template<> unsigned subRecordType<IlcConfigurationStart>() {
  return SubHeader::daq+0x820+IlcConfigurationStart::versionNumber;
}

template<> unsigned subRecordType<IlcConfigurationEnd>() {
  return SubHeader::daq+0x830+IlcConfigurationEnd::versionNumber;
}

template<> unsigned subRecordType<IlcBunchTrain>() {
  return SubHeader::daq+0x840+IlcBunchTrain::versionNumber;
}

template<> unsigned subRecordType<IlcSlowReadout>() {
  return SubHeader::daq+0x850+IlcSlowReadout::versionNumber;
}

template<> unsigned subRecordType<IlcAbort>() {
  return SubHeader::daq+0x860+IlcAbort::versionNumber;
}

// CRC

template<> unsigned subRecordType<CrcLocationData <CrcVmeRunData> >() {
  return SubHeader::crc+0x000+CrcVmeRunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcVmeConfigurationData> >() {
  return SubHeader::crc+0x010+CrcVmeConfigurationData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcVmeEventData> >() {
  return SubHeader::crc+0x020+CrcVmeEventData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeRunData> >() {
  return SubHeader::crc+0x030+CrcBeRunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeConfigurationDataV0> >() {
  return SubHeader::crc+0x040+CrcBeConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeConfigurationDataV1> >() {
  return SubHeader::crc+0x040+CrcBeConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeEventDataV0> >() {
  return SubHeader::crc+0x050+CrcBeEventDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeEventDataV1> >() {
  return SubHeader::crc+0x050+CrcBeEventDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcFeRunData> >() {
  return SubHeader::crc+0x060+CrcFeRunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcFeConfigurationData> >() {
  return SubHeader::crc+0x070+CrcFeConfigurationData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcFeEventData> >() {
  return SubHeader::crc+0x080+CrcFeEventData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcVlinkEventData> >() {
  return SubHeader::crc+0x100+CrcVlinkEventData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcFeFakeEventData> >() {
  return SubHeader::crc+0x110+CrcFeFakeEventData::versionNumber;
}

template<> unsigned subRecordType<CrcReadoutConfigurationDataV0>() {
  return SubHeader::crc+0x200+CrcReadoutConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcReadoutConfigurationDataV1>() {
  return SubHeader::crc+0x200+CrcReadoutConfigurationDataV1::versionNumber;
}


// EMC

template<> unsigned subRecordType<CrcLocationData <EmcFeConfigurationData> >() {
  return SubHeader::emc+0x000+EmcFeConfigurationData::versionNumber;
}


// AHC

template<> unsigned subRecordType<CrcLocationData <AhcFeConfigurationDataV0> >() {
  return SubHeader::ahc+0x000+AhcFeConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <AhcFeConfigurationDataV1> >() {
  return SubHeader::ahc+0x000+AhcFeConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <AhcVfeConfigurationDataFine> >() {
  return SubHeader::ahc+0x010+AhcVfeConfigurationDataFine::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <AhcVfeStartUpDataFine> >() {
  return SubHeader::ahc+0x020+AhcVfeStartUpDataFine::versionNumber;
}

template<> unsigned subRecordType<AhcSlowRunDataV0>() {
  return SubHeader::ahc+0x030+AhcSlowRunDataV0::versionNumber;
}

template<> unsigned subRecordType<AhcSlowRunDataV1>() {
  return SubHeader::ahc+0x030+AhcSlowRunDataV1::versionNumber;
}

template<> unsigned subRecordType<AhcSlowConfigurationDataV0>() {
  return SubHeader::ahc+0x040+AhcSlowConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<AhcSlowConfigurationDataV1>() {
  return SubHeader::ahc+0x040+AhcSlowConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<AhcSlowReadoutDataV0>() {
  return SubHeader::ahc+0x050+AhcSlowReadoutDataV0::versionNumber;
}

template<> unsigned subRecordType<AhcSlowReadoutDataV1>() {
  return SubHeader::ahc+0x050+AhcSlowReadoutDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <AhcVfeStartUpDataCoarse> >() {
  return SubHeader::ahc+0x060+AhcVfeStartUpDataCoarse::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <AhcVfeConfigurationDataCoarse> >() {
  return SubHeader::ahc+0x070+AhcVfeConfigurationDataCoarse::versionNumber;
}

template<> unsigned subRecordType<AhcMapping>() {
  return SubHeader::ahc+0x080+AhcMapping::versionNumber;
}


// SCE

template<> unsigned subRecordType<SceSlowReadoutData>() {
  return SubHeader::sce+0x000+SceSlowReadoutData::versionNumber;
}

template<> unsigned subRecordType<SceSlowTemperatureData>() {
  return SubHeader::sce+0x001+SceSlowTemperatureData::versionNumber;
}


// DHC

template<> unsigned subRecordType<DhcReadoutConfigurationData>() {
  return SubHeader::dhc+0x010+DhcReadoutConfigurationData::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcFeConfigurationDataV0> >() {
  return SubHeader::dhc+0x020+DhcFeConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcFeConfigurationDataV1> >() {
  return SubHeader::dhc+0x020+DhcFeConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcBeConfigurationData> >() {
  return SubHeader::dhc+0x030+DhcBeConfigurationData::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcEventDataV0> >() {
  return SubHeader::dhc+0x040+DhcEventDataV0::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcEventDataV1> >() {
  return SubHeader::dhc+0x040+DhcEventDataV1::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcDcConfigurationData> >() {
  return SubHeader::dhc+0x050+DhcDcConfigurationData::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcTriggerData> >() {
  return SubHeader::dhc+0x060+DhcTriggerData::versionNumber;
}

template<> unsigned subRecordType<DhcLocationData <DhcDcRunData> >() {
  return SubHeader::dhc+0x070+DhcDcRunData::versionNumber;
}

template<> unsigned subRecordType<TtmLocationData <TtmConfigurationDataV0> >() {
  return SubHeader::dhc+0x100+TtmConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<TtmLocationData <TtmConfigurationDataV1> >() {
  return SubHeader::dhc+0x100+TtmConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<TtmLocationData <TtmTriggerData> >() {
  return SubHeader::dhc+0x110+TtmTriggerData::versionNumber;
}

template<> unsigned subRecordType<DhcTriggerConfigurationData>() {
  return SubHeader::dhc+0x120+DhcTriggerConfigurationData::versionNumber;
}

// DHE
/*
template<> unsigned subRecordType<DheRunData>() {
  return SubHeader::dhe+0x000+DheRunData::versionNumber;
}
*/
template<> unsigned subRecordType< CrcLocationData<DheVfeConfigurationData4> >() {
  return SubHeader::dhe+0x010+DheVfeConfigurationData4::versionNumber;
}

template<> unsigned subRecordType<DheTriggerData>() {
  return SubHeader::dhe+0x020+DheTriggerData::versionNumber;
}

template<> unsigned subRecordType<DheEventData>() {
  return SubHeader::dhe+0x030+DheEventData::versionNumber;
}

template<> unsigned subRecordType<DheDifRunData>() {
  return SubHeader::dhe+0x040+DheDifRunData::versionNumber;
}

template<> unsigned subRecordType<DheDifConfigurationData>() {
  return SubHeader::dhe+0x050+DheDifConfigurationData::versionNumber;
}

template<> unsigned subRecordType<DheDifAcquisitionData>() {
  return SubHeader::dhe+0x060+DheDifAcquisitionData::versionNumber;
}

template<> unsigned subRecordType<DheDifEventData>() {
  return SubHeader::dhe+0x070+DheDifEventData::versionNumber;
}


// BML

template<> unsigned subRecordType<BmlLc1176RunData>() {
  return SubHeader::bml+0x000+BmlLc1176RunData::versionNumber;
}

template<> unsigned subRecordType<BmlLc1176ConfigurationData>() {
  return SubHeader::bml+0x010+BmlLc1176ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLc1176EventData>() {
  return SubHeader::bml+0x020+BmlLc1176EventData::versionNumber;
}

template<> unsigned subRecordType<BmlHodRunData>() {
  return SubHeader::bml+0x030+BmlHodRunData::versionNumber;
}
/*
template<> unsigned subRecordType<BmlHodConfigurationData>() {
  return SubHeader::bml+0x040+BmlHodConfigurationData::versionNumber;
}
*/
template<> unsigned subRecordType<BmlHodEventData>() {
  return SubHeader::bml+0x050+BmlHodEventData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767RunData> >() {
  return SubHeader::bml+0x060+BmlCaen767RunData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767OpcodeData> >() {
  return SubHeader::bml+0x070+BmlCaen767OpcodeData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767ConfigurationData> >() {
  return SubHeader::bml+0x080+BmlCaen767ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767TriggerData> >() {
  return SubHeader::bml+0x090+BmlCaen767TriggerData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767EventData> >() {
  return SubHeader::bml+0x0a0+BmlCaen767EventData::versionNumber;
}

template<> unsigned subRecordType<BmlCaen767ReadoutConfigurationData>() {
  return SubHeader::bml+0x0b0+BmlCaen767ReadoutConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767TestData> >() {
  return SubHeader::bml+0x0c0+BmlCaen767TestData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen767TdcErrorData> >() {
  return SubHeader::bml+0x0d0+BmlCaen767TdcErrorData::versionNumber;
}

template<> unsigned subRecordType<BmlCernSlowRunData>() {
  return SubHeader::bml+0x0e0+BmlCernSlowRunData::versionNumber;
}

template<> unsigned subRecordType<BmlFnalSlowRunData>() {
  return SubHeader::bml+0x0f0+BmlFnalSlowRunData::versionNumber;
}

template<> unsigned subRecordType<BmlLalHodoscopeRunData>() {
  return SubHeader::bml+0x100+BmlLalHodoscopeRunData::versionNumber;
}

template<> unsigned subRecordType<BmlLalHodoscopeConfigurationData>() {
  return SubHeader::bml+0x110+BmlLalHodoscopeConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLalHodoscopeTriggerData>() {
  return SubHeader::bml+0x120+BmlLalHodoscopeTriggerData::versionNumber;
}

template<> unsigned subRecordType<BmlLalHodoscopeEventData>() {
  return SubHeader::bml+0x130+BmlLalHodoscopeEventData::versionNumber;
}

template<> unsigned subRecordType<BmlFnalSlowReadoutDataV0>() {
  return SubHeader::bml+0x140+BmlFnalSlowReadoutDataV0::versionNumber;
}

template<> unsigned subRecordType<BmlFnalSlowReadoutDataV1>() {
  return SubHeader::bml+0x140+BmlFnalSlowReadoutDataV1::versionNumber;
}

template<> unsigned subRecordType<BmlFnalAcquisitionData>() {
  return SubHeader::bml+0x150+BmlFnalAcquisitionData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290RunData> >() {
  return SubHeader::bml+0x160+BmlCaen1290RunData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290OpcodeData> >() {
  return SubHeader::bml+0x170+BmlCaen1290OpcodeData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290ConfigurationData> >() {
  return SubHeader::bml+0x180+BmlCaen1290ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290TriggerData> >() {
  return SubHeader::bml+0x190+BmlCaen1290TriggerData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290EventData> >() {
  return SubHeader::bml+0x1a0+BmlCaen1290EventData::versionNumber;
}

template<> unsigned subRecordType<BmlCaen1290ReadoutConfigurationData>() {
  return SubHeader::bml+0x1b0+BmlCaen1290ReadoutConfigurationData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290TestData> >() {
  return SubHeader::bml+0x1c0+BmlCaen1290TestData::versionNumber;
}

template<> unsigned subRecordType<BmlLocationData <BmlCaen1290TdcErrorData> >() {
  return SubHeader::bml+0x1d0+BmlCaen1290TdcErrorData::versionNumber;
}


// TRG

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgRunData> >() {
  return SubHeader::trg+0x000+CrcBeTrgRunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgConfigurationDataV0> >() {
  return SubHeader::trg+0x010+CrcBeTrgConfigurationDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgConfigurationDataV1> >() {
  return SubHeader::trg+0x010+CrcBeTrgConfigurationDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgEventData> >() {
  return SubHeader::trg+0x020+CrcBeTrgEventData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgPollDataV0> >() {
  return SubHeader::trg+0x030+CrcBeTrgPollDataV0::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcBeTrgPollDataV1> >() {
  return SubHeader::trg+0x030+CrcBeTrgPollDataV1::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <TrgSpillPollData> >() {
  return SubHeader::trg+0x040+TrgSpillPollData::versionNumber;
}

template<> unsigned subRecordType<TrgReadoutConfigurationData>() {
  return SubHeader::trg+0x210+TrgReadoutConfigurationData::versionNumber;
}


// SLW

template<> unsigned subRecordType<CrcLocationData <CrcLm82RunData> >() {
  return SubHeader::slw+0x000+CrcLm82RunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcLm82ConfigurationData> >() {
  return SubHeader::slw+0x010+CrcLm82ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcLm82SlowReadoutData> >() {
  return SubHeader::slw+0x020+CrcLm82SlowReadoutData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcAdm1025RunData> >() {
  return SubHeader::slw+0x030+CrcAdm1025RunData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcAdm1025ConfigurationData> >() {
  return SubHeader::slw+0x040+CrcAdm1025ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<CrcLocationData <CrcAdm1025SlowReadoutData> >() {
  return SubHeader::slw+0x050+CrcAdm1025SlowReadoutData::versionNumber;
}

template<> unsigned subRecordType<EmcStageRunData>() {
  return SubHeader::slw+0x060+EmcStageRunData::versionNumber;
}


// MPS

template<> unsigned subRecordType<MpsReadoutConfigurationData>() {
  return SubHeader::mps+0x000+MpsReadoutConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsUsbDaqRunData> >() {
  return SubHeader::mps+0x010+MpsUsbDaqRunData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsUsbDaqConfigurationData> >() {
  return SubHeader::mps+0x020+MpsUsbDaqConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsUsbDaqBunchTrainData> >() {
  return SubHeader::mps+0x030+MpsUsbDaqBunchTrainData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsPcb1ConfigurationData> >() {
  return SubHeader::mps+0x050+MpsPcb1ConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsPcb1SlowReadoutData> >() {
  return SubHeader::mps+0x060+MpsPcb1SlowReadoutData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsSensorV10ConfigurationData> >() {
  return SubHeader::mps+0x080+MpsSensorV10ConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsSensor1BunchTrainData> >() {
  return SubHeader::mps+0x090+MpsSensor1BunchTrainData::versionNumber;
}

template<> unsigned subRecordType<MpsLaserRunData>() {
  return SubHeader::mps+0x0a0+MpsLaserRunData::versionNumber;
}

template<> unsigned subRecordType<MpsLaserConfigurationData>() {
  return SubHeader::mps+0x0b0+MpsLaserConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsSensorV12ConfigurationData> >() {
  return SubHeader::mps+0x0c0+MpsSensorV12ConfigurationData::versionNumber;
}

template<> unsigned subRecordType<MpsDigitisationData>() {
  return SubHeader::mps+0x0d0+MpsDigitisationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsUsbDaqMasterConfigurationData> >() {
  return SubHeader::mps+0x0e0+MpsUsbDaqMasterConfigurationData::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsUsbDaqSpillPollData> >() {
  return SubHeader::mps+0x0f0+MpsUsbDaqSpillPollData::versionNumber;
}

template<> unsigned subRecordType<MpsAlignmentSensor>() {
  return SubHeader::mps+0x100+MpsAlignmentSensor::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsEudetBunchTrainDataV0> >() {
  return SubHeader::mps+0x110+MpsEudetBunchTrainDataV0::versionNumber;
}

template<> unsigned subRecordType< MpsLocationData<MpsEudetBunchTrainDataV1> >() {
  return SubHeader::mps+0x110+MpsEudetBunchTrainDataV1::versionNumber;
}


// SIM

template<> unsigned subRecordType<SimDetectorData>() {
  return SubHeader::sim+0x000+SimDetectorData::versionNumber;
}

template<> unsigned subRecordType<SimPrimaryData>() {
  return SubHeader::sim+0x001+SimPrimaryData::versionNumber;
}

template<> unsigned subRecordType<SimDetectorEnergies>() {
  return SubHeader::sim+0x002+SimDetectorEnergies::versionNumber;
}

template<> unsigned subRecordType<SimParticle>() {
  return SubHeader::sim+0x003+SimParticle::versionNumber;
}

template<> unsigned subRecordType<SimLayerTrack>() {
  return SubHeader::sim+0x004+SimLayerTrack::versionNumber;
}

template<> unsigned subRecordType<SimLayerHits>() {
  return SubHeader::sim+0x005+SimLayerHits::versionNumber;
}

//#ifdef CALICE_DAQ_ICC
#endif
#endif
