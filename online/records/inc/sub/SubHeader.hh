#ifndef SubHeader_HH
#define SubHeader_HH

#include <string>
#include <iostream>
#include <iomanip>


class SubHeader {

public:
  enum Group {
    daq=0x0000,
    trg=0x1000,
    crc=0x3000,
    emc=0x4000,
    ahc=0x5000,
    dhc=0x6000,
    bml=0x7000,
    slw=0x8000,
    dhe=0x9000,
    mps=0xa000,
    sce=0xb000,
    sim=0xc000,
    dec=0xd000
  };

  SubHeader(unsigned short t=0, unsigned short n=0);

  unsigned subRecordType() const;
  void subRecordType(unsigned n);

  unsigned numberOfBytes() const;
  void numberOfBytes(unsigned n);
  unsigned totalNumberOfBytes() const;

  unsigned header() const;
  SubHeader* nextSubHeader() const;

  const void* data() const;
  void* data();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;

  //bool write(std::ostream &o) const;
  //bool read(std::istream &i);


private:
  unsigned short _subRecordType;
  unsigned short _numberOfBytes;
};


#ifdef CALICE_DAQ_ICC


SubHeader::SubHeader(unsigned short t, unsigned short n) :
  _subRecordType(t), _numberOfBytes(n) {
}

unsigned SubHeader::subRecordType() const {
  return _subRecordType;
}

void SubHeader::subRecordType(unsigned n) {
  _subRecordType=n;
}

unsigned SubHeader::numberOfBytes() const {
  return _numberOfBytes;
}

void SubHeader::numberOfBytes(unsigned n) {
  _numberOfBytes=n;
}

unsigned SubHeader::totalNumberOfBytes() const {
  unsigned n=_numberOfBytes+sizeof(SubHeader);
  return n+(4-n%4)%4; // Round up to multiple of 4
  }

unsigned SubHeader::header() const {
  return *((unsigned*)this);
}

SubHeader* SubHeader::nextSubHeader() const {
  return (SubHeader*)((char*)(this)+totalNumberOfBytes());
}

const void* SubHeader::data() const {
  return this+1;
}

void* SubHeader::data() {
  return this+1;
}

std::ostream& SubHeader::print(std::ostream &o, std::string s) const {
  o << s << "SubHeader::print()  Sub Record Type 0x" 
    << std::hex << std::setfill('0') << std::setw(4) << _subRecordType 
    << std::dec << std::setfill(' ')
    << " Number of bytes stored " << std::setw(5) << _numberOfBytes 
    << ", total " << std::setw(8) << totalNumberOfBytes() << std::endl;
  return o;
}

/*
bool SubHeader::write(std::ostream &o) const {
  o << _subRecordType << " " << _numberOfBytes;
  return true;
}

bool SubHeader::read(std::istream &i) {
  i >> _subRecordType >> _numberOfBytes;
  return true;
}
*/

#endif
#endif
