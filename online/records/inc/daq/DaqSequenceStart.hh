#ifndef DaqSequenceStart_HH
#define DaqSequenceStart_HH

#include <string>
#include <iostream>

#include "DaqSequenceType.hh"


class DaqSequenceStart {

public:
  enum {
    versionNumber=0
  };

  DaqSequenceStart();

  void reset();

  DaqSequenceType sequenceType() const;
  void            sequenceType(DaqSequenceType n);

  unsigned maximumNumberOfRuns() const;
  void     maximumNumberOfRuns(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  DaqSequenceType _sequenceType;
  unsigned _maximumNumberOfRuns;
};


#ifdef CALICE_DAQ_ICC


DaqSequenceStart::DaqSequenceStart() {
  reset();
}

void DaqSequenceStart::reset() {
  _sequenceType.type(DaqSequenceType::daqSimple);
  _maximumNumberOfRuns=1;
}

DaqSequenceType DaqSequenceStart::sequenceType() const {
  return _sequenceType;
}

void DaqSequenceStart::sequenceType(DaqSequenceType n) {
  _sequenceType=n;
}

unsigned DaqSequenceStart::maximumNumberOfRuns() const {
  return _maximumNumberOfRuns;
}

void DaqSequenceStart::maximumNumberOfRuns(unsigned m) {
  _maximumNumberOfRuns=m;
}

std::ostream& DaqSequenceStart::print(std::ostream &o, std::string s) const {
  o << s << "DaqSequenceStart::print()" << std::endl;
  _sequenceType.print(o,s+" ");
  o << s << " Maximum number of runs "
    << _maximumNumberOfRuns << std::endl;
  
  return o;
}

#endif
#endif
