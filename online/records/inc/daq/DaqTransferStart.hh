#ifndef DaqTransferStart_HH
#define DaqTransferStart_HH

#include <string>
#include <iostream>


class DaqTransferStart {

public:
  enum {
    versionNumber=0
  };

  DaqTransferStart();

  void reset();

  unsigned transferNumberInRun() const;
  void     transferNumberInRun(unsigned n);

  unsigned transferNumberInConfiguration() const;
  void     transferNumberInConfiguration(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


protected:
  unsigned _transferNumberInRun;
  unsigned _transferNumberInConfiguration;
};


#ifdef CALICE_DAQ_ICC


DaqTransferStart::DaqTransferStart() {
  reset();
}

void DaqTransferStart::reset() {
  _transferNumberInRun=0;
  _transferNumberInConfiguration=0;
}

unsigned DaqTransferStart::transferNumberInRun() const {
  return _transferNumberInRun;
}

void DaqTransferStart::transferNumberInRun(unsigned n) {
  _transferNumberInRun=n;
}

unsigned DaqTransferStart::transferNumberInConfiguration() const {
  return _transferNumberInConfiguration;
}

void DaqTransferStart::transferNumberInConfiguration(unsigned m) {
  _transferNumberInConfiguration=m;
}

std::ostream& DaqTransferStart::print(std::ostream &o, std::string s) const {
  o << s << "DaqTransferStart::print() Transfer numbers in run "
    << _transferNumberInRun << ", in configuration "
    << _transferNumberInConfiguration << std::endl;
  return o;
}

#endif
#endif
