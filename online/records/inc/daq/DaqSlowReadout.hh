#ifndef DaqSlowReadout_HH
#define DaqSlowReadout_HH

#include <string>
#include <iostream>


class DaqSlowReadout {

public:
  enum {
    versionNumber=0
  };

  DaqSlowReadout();

  void reset();

  unsigned slowReadoutNumberInRun() const;
  void     slowReadoutNumberInRun(unsigned n);

  unsigned slowReadoutNumberInConfiguration() const;
  void     slowReadoutNumberInConfiguration(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


protected:
  unsigned _slowReadoutNumberInRun;
  unsigned _slowReadoutNumberInConfiguration;
};


#ifdef CALICE_DAQ_ICC


DaqSlowReadout::DaqSlowReadout() {
  reset();
}

void DaqSlowReadout::reset() {
  _slowReadoutNumberInRun=0;
  _slowReadoutNumberInConfiguration=0;
}

unsigned DaqSlowReadout::slowReadoutNumberInRun() const {
  return _slowReadoutNumberInRun;
}

void DaqSlowReadout::slowReadoutNumberInRun(unsigned n) {
  _slowReadoutNumberInRun=n;
}

unsigned DaqSlowReadout::slowReadoutNumberInConfiguration() const {
  return _slowReadoutNumberInConfiguration;
}

void DaqSlowReadout::slowReadoutNumberInConfiguration(unsigned m) {
  _slowReadoutNumberInConfiguration=m;
}

std::ostream& DaqSlowReadout::print(std::ostream &o, std::string s) const {
  o << s << "DaqSlowReadout::print()" << std::endl;
  o << s << " Slow readout numbers in run "
    << _slowReadoutNumberInRun << ", in configuration "
    << _slowReadoutNumberInConfiguration << std::endl;
   
  return o;
}

#endif
#endif
