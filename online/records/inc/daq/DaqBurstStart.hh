#ifndef DaqBurstStart_HH
#define DaqBurstStart_HH

#include <string>
#include <iostream>


class DaqBurstStart {

public:
  enum {
    versionNumber=0
  };

  DaqBurstStart();
  
  void reset();

  unsigned burstNumberInRun() const;
  void     burstNumberInRun(unsigned n);

  unsigned burstNumberInConfiguration() const;
  void     burstNumberInConfiguration(unsigned n);

  unsigned burstNumberInAcquisition() const;
  void     burstNumberInAcquisition(unsigned n);

  unsigned firstTriggerNumberInRun() const;
  void     firstTriggerNumberInRun(unsigned n);

  unsigned firstTriggerNumberInConfiguration() const;
  void     firstTriggerNumberInConfiguration(unsigned n);
  
  unsigned firstTriggerNumberInAcquisition() const;
  void     firstTriggerNumberInAcquisition(unsigned n);

  unsigned maximumNumberOfExtraTriggersInBurst() const;
  void     maximumNumberOfExtraTriggersInBurst(unsigned m);

  UtlTimeDifference maximumTimeOfBurst() const;
  void              maximumTimeOfBurst(UtlTimeDifference m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _burstNumberInRun;
  unsigned _burstNumberInConfiguration;
  unsigned _burstNumberInAcquisition;
  unsigned _firstTriggerNumberInRun;
  unsigned _firstTriggerNumberInConfiguration;
  unsigned _firstTriggerNumberInAcquisition;
  unsigned _maximumNumberOfExtraTriggersInBurst;
  UtlTimeDifference _maximumTimeOfBurst;
};


#ifdef CALICE_DAQ_ICC

DaqBurstStart::DaqBurstStart() {
  reset();
}

void DaqBurstStart::reset() {
  _burstNumberInRun=0;
  _burstNumberInConfiguration=0;
  _burstNumberInAcquisition=0;
  _firstTriggerNumberInRun=0;
  _firstTriggerNumberInConfiguration=0;
  _firstTriggerNumberInAcquisition=0;
  _maximumNumberOfExtraTriggersInBurst=499;
  _maximumTimeOfBurst=UtlTimeDifference(0x7fffffff,999999);
}

unsigned DaqBurstStart::burstNumberInRun() const {
  return _burstNumberInRun;
}

void DaqBurstStart::burstNumberInRun(unsigned n) {
  _burstNumberInRun=n;
}

unsigned DaqBurstStart::burstNumberInConfiguration() const {
  return _burstNumberInConfiguration;
}

void DaqBurstStart::burstNumberInConfiguration(unsigned n) {
  _burstNumberInConfiguration=n;
}

unsigned DaqBurstStart::burstNumberInAcquisition() const {
  return _burstNumberInAcquisition;
}

void DaqBurstStart::burstNumberInAcquisition(unsigned n) {
  _burstNumberInAcquisition=n;
}

unsigned DaqBurstStart::firstTriggerNumberInRun() const {
  return _firstTriggerNumberInRun;
}

void DaqBurstStart::firstTriggerNumberInRun(unsigned n) {
  _firstTriggerNumberInRun=n;
}

unsigned DaqBurstStart::firstTriggerNumberInConfiguration() const {
  return _firstTriggerNumberInConfiguration;
}

void DaqBurstStart::firstTriggerNumberInConfiguration(unsigned n) {
  _firstTriggerNumberInConfiguration=n;
}

unsigned DaqBurstStart::firstTriggerNumberInAcquisition() const {
  return _firstTriggerNumberInAcquisition;
}

void DaqBurstStart::firstTriggerNumberInAcquisition(unsigned n) {
  _firstTriggerNumberInAcquisition=n;
}

unsigned DaqBurstStart::maximumNumberOfExtraTriggersInBurst() const {
  return _maximumNumberOfExtraTriggersInBurst;
}

void DaqBurstStart::maximumNumberOfExtraTriggersInBurst(unsigned m) {
  _maximumNumberOfExtraTriggersInBurst=m;
}

UtlTimeDifference DaqBurstStart::maximumTimeOfBurst() const {
  return _maximumTimeOfBurst;
}
 
void DaqBurstStart::maximumTimeOfBurst(UtlTimeDifference m) {
  _maximumTimeOfBurst=m;
}

std::ostream& DaqBurstStart::print(std::ostream &o, std::string s) const {
  o << s << "DaqBurstStart::print() Burst numbers in run "
    << _burstNumberInRun << ", in configuration "
    << _burstNumberInConfiguration << ", in acquisition "
    << _burstNumberInAcquisition << std::endl;
  o << s << " First trigger numbers in run "
    << _firstTriggerNumberInRun << ", in configuration "
    << _firstTriggerNumberInConfiguration << ", in acquisition "
    << _firstTriggerNumberInAcquisition << std::endl;
  o << s << " Maximum number of extra triggers in burst " << 
    _maximumNumberOfExtraTriggersInBurst << std::endl;
  o << s << " Maximum time of burst "
    << _maximumTimeOfBurst.deltaTime() << " secs" << std::endl;

  return o;
}

#endif
#endif
