#ifndef DaqConfigurationStartV0_HH
#define DaqConfigurationStartV0_HH

#include <iostream>


class DaqConfigurationStartV0 {

public:
  enum {
    versionNumber=0
  };

  DaqConfigurationStartV0();

  void reset();

  unsigned configurationNumberInRun() const;
  void configurationNumberInRun(unsigned n);

  unsigned maximumNumberOfAcquisitionsInConfiguration() const;
  void maximumNumberOfAcquisitionsInConfiguration(unsigned m);

  unsigned maximumNumberOfEventsInConfiguration() const;
  void maximumNumberOfEventsInConfiguration(unsigned m);

  void increment();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;

protected:
  unsigned _configurationNumberInRun;
  unsigned _maximumNumberOfAcquisitionsInConfiguration;
  unsigned _maximumNumberOfEventsInConfiguration;
};


#ifdef CALICE_DAQ_ICC

DaqConfigurationStartV0::DaqConfigurationStartV0() {
  reset();
}

void DaqConfigurationStartV0::reset() {
  _configurationNumberInRun=0;
  _maximumNumberOfAcquisitionsInConfiguration=0xffffffff;
  _maximumNumberOfEventsInConfiguration=0xffffffff;
}

unsigned DaqConfigurationStartV0::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void DaqConfigurationStartV0::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned DaqConfigurationStartV0::maximumNumberOfAcquisitionsInConfiguration() const {
  return _maximumNumberOfAcquisitionsInConfiguration;
}

void DaqConfigurationStartV0::maximumNumberOfAcquisitionsInConfiguration(unsigned m) {
  _maximumNumberOfAcquisitionsInConfiguration=m;
}

unsigned DaqConfigurationStartV0::maximumNumberOfEventsInConfiguration() const {
  return _maximumNumberOfEventsInConfiguration;
}

void DaqConfigurationStartV0::maximumNumberOfEventsInConfiguration(unsigned m) {
  _maximumNumberOfEventsInConfiguration=m;
}

void DaqConfigurationStartV0::increment() {
  _configurationNumberInRun++;
}

std::ostream& DaqConfigurationStartV0::print(std::ostream &o, std::string s) const {
  o << s << "DaqConfigurationStartV0::print() Configuration number "
    << _configurationNumberInRun
    << ", maximum numbers of acquisitions "
    << _maximumNumberOfAcquisitionsInConfiguration << ", events "
      << _maximumNumberOfEventsInConfiguration << std::endl;
  return o;
}

#endif
#endif
