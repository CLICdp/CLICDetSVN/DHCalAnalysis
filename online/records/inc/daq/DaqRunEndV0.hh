#ifndef DaqRunEndV0_HH
#define DaqRunEndV0_HH

#include <string>
#include <iostream>

class DaqRunEndV0 {

public:
  enum {
    versionNumber=0
  };

  DaqRunEndV0();

  void reset();
  
  unsigned runNumber() const;
  void     runNumber(unsigned n);

  unsigned actualNumberOfConfigurationsInRun() const;
  void     actualNumberOfConfigurationsInRun(unsigned m);

  unsigned actualNumberOfAcquisitionsInRun() const;
  void     actualNumberOfAcquisitionsInRun(unsigned m);

  unsigned actualNumberOfEventsInRun() const;
  void     actualNumberOfEventsInRun(unsigned m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _runNumber;
  unsigned _actualNumberOfConfigurationsInRun;
  unsigned _actualNumberOfAcquisitionsInRun;
  unsigned _actualNumberOfEventsInRun;
};

#ifdef CALICE_DAQ_ICC

DaqRunEndV0::DaqRunEndV0() {
  reset();
}

void DaqRunEndV0::reset() {
  _runNumber=0;
  _actualNumberOfConfigurationsInRun=0;
  _actualNumberOfAcquisitionsInRun=0;
  _actualNumberOfEventsInRun=0;
}

unsigned DaqRunEndV0::runNumber() const {
  return _runNumber;
}

void DaqRunEndV0::runNumber(unsigned n) {
  _runNumber=n;
}

unsigned DaqRunEndV0::actualNumberOfConfigurationsInRun() const {
  return _actualNumberOfConfigurationsInRun;
}

void DaqRunEndV0::actualNumberOfConfigurationsInRun(unsigned m) {
  _actualNumberOfConfigurationsInRun=m;
}

unsigned DaqRunEndV0::actualNumberOfAcquisitionsInRun() const {
  return _actualNumberOfAcquisitionsInRun;
}

void DaqRunEndV0::actualNumberOfAcquisitionsInRun(unsigned m) {
  _actualNumberOfAcquisitionsInRun=m;
}

unsigned DaqRunEndV0::actualNumberOfEventsInRun() const {
  return _actualNumberOfEventsInRun;
}

void DaqRunEndV0::actualNumberOfEventsInRun(unsigned m) {
  _actualNumberOfEventsInRun=m;
}

std::ostream& DaqRunEndV0::print(std::ostream &o, std::string s) const {
  o << s << "DaqRunEndV0::print()  Run number " << _runNumber << std::endl;
  o << s << " Actual numbers of configurations "
    << _actualNumberOfConfigurationsInRun << ", acquisitions "
    << _actualNumberOfAcquisitionsInRun << ", events "
      << _actualNumberOfEventsInRun << std::endl;
  
  return o;
}

#endif
#endif
