#ifndef DaqTransferEnd_HH
#define DaqTransferEnd_HH

#include <string>
#include <iostream>


class DaqTransferEnd {

public:
  enum {
    versionNumber=0
  };

  DaqTransferEnd();

  void reset();

  unsigned transferNumberInRun() const;
  void     transferNumberInRun(unsigned n);

  unsigned transferNumberInConfiguration() const;
  void     transferNumberInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInTransfer() const;
  void     actualNumberOfEventsInTransfer(unsigned m);

  UtlTimeDifference actualTimeOfTransfer() const;
  void              actualTimeOfTransfer(UtlTimeDifference n);
 
  std::ostream& print(std::ostream &o, std::string s="") const;


protected:
  unsigned _transferNumberInRun;
  unsigned _transferNumberInConfiguration;
  unsigned _actualNumberOfEventsInTransfer;
  UtlTimeDifference _actualTimeOfTransfer;
};


#ifdef CALICE_DAQ_ICC

DaqTransferEnd::DaqTransferEnd() {
  reset();
}

void DaqTransferEnd::reset() {
  _transferNumberInRun=0;
  _transferNumberInConfiguration=0;
  _actualNumberOfEventsInTransfer=0;
  _actualTimeOfTransfer=UtlTimeDifference(0,0);
}

unsigned DaqTransferEnd::transferNumberInRun() const {
  return _transferNumberInRun;
}

void DaqTransferEnd::transferNumberInRun(unsigned n) {
  _transferNumberInRun=n;
}

unsigned DaqTransferEnd::transferNumberInConfiguration() const {
  return _transferNumberInConfiguration;
}

void DaqTransferEnd::transferNumberInConfiguration(unsigned m) {
  _transferNumberInConfiguration=m;
}

unsigned DaqTransferEnd::actualNumberOfEventsInTransfer() const {
  return _actualNumberOfEventsInTransfer;
}

void DaqTransferEnd::actualNumberOfEventsInTransfer(unsigned m) {
  _actualNumberOfEventsInTransfer=m;
}

UtlTimeDifference DaqTransferEnd::actualTimeOfTransfer() const {
  return _actualTimeOfTransfer;
}
  
void DaqTransferEnd::actualTimeOfTransfer(UtlTimeDifference n) {
  _actualTimeOfTransfer=n;
}
 
std::ostream& DaqTransferEnd::print(std::ostream &o, std::string s) const {
  o << s << "DaqTransferEnd::print() Transfer numbers in run "
    << _transferNumberInRun << ", in configuration "
    << _transferNumberInConfiguration
    << ", actual numbers of events "
    << _actualNumberOfEventsInTransfer << std::endl;
  o << s << "  Actual time of transfer "
    << (unsigned)(_actualTimeOfTransfer.deltaTime()) << " secs" << std::endl;
  return o;
}

#endif
#endif
