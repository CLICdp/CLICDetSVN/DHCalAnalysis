#ifndef DaqConfigurationEndV0_HH
#define DaqConfigurationEndV0_HH

#include <string>
#include <iostream>


class DaqConfigurationEndV0 {

public:
  enum {
    versionNumber=0
  };

  DaqConfigurationEndV0();

  void reset();

  unsigned configurationNumberInRun() const;
  void     configurationNumberInRun(unsigned n);

  unsigned actualNumberOfAcquisitionsInConfiguration() const;
  void     actualNumberOfAcquisitionsInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInConfiguration() const;
  void     actualNumberOfEventsInConfiguration(unsigned m);

  bool increment();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _configurationNumberInRun;
  unsigned _actualNumberOfAcquisitionsInConfiguration;
  unsigned _actualNumberOfEventsInConfiguration;
};

#ifdef CALICE_DAQ_ICC

DaqConfigurationEndV0::DaqConfigurationEndV0() {
  reset();
}

void DaqConfigurationEndV0::reset() {
  _configurationNumberInRun=0;
  _actualNumberOfAcquisitionsInConfiguration=0;
  _actualNumberOfEventsInConfiguration=0;
}

unsigned DaqConfigurationEndV0::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void DaqConfigurationEndV0::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned DaqConfigurationEndV0::actualNumberOfAcquisitionsInConfiguration() const {
  return _actualNumberOfAcquisitionsInConfiguration;
}

void DaqConfigurationEndV0::actualNumberOfAcquisitionsInConfiguration(unsigned m) {
  _actualNumberOfAcquisitionsInConfiguration=m;
}

unsigned DaqConfigurationEndV0::actualNumberOfEventsInConfiguration() const {
  return _actualNumberOfEventsInConfiguration;
}

void DaqConfigurationEndV0::actualNumberOfEventsInConfiguration(unsigned m) {
  _actualNumberOfEventsInConfiguration=m;
  }

bool DaqConfigurationEndV0::increment() {
  _configurationNumberInRun++;
  return true;
}

std::ostream& DaqConfigurationEndV0::print(std::ostream &o, std::string s) const {
  o << s << "DaqConfigurationEndV0::print() Configuration number "
    << _configurationNumberInRun
      << ", actual numbers of acquisitions "
    << _actualNumberOfAcquisitionsInConfiguration << " events "
    << _actualNumberOfEventsInConfiguration << std::endl;
  return o;
}

#endif
#endif
