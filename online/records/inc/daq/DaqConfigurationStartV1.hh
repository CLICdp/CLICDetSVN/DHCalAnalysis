#ifndef DaqConfigurationStartV1_HH
#define DaqConfigurationStartV1_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

class DaqConfigurationStartV0;


class DaqConfigurationStartV1 {

public:
  enum {
    versionNumber=1
  };

  DaqConfigurationStartV1();
  DaqConfigurationStartV1(const DaqConfigurationStartV0 &d);

  void reset();

  unsigned configurationNumberInRun() const;
  void     configurationNumberInRun(unsigned n);

  // Limits configuration

  unsigned maximumNumberOfAcquisitionsInConfiguration() const;
  void     maximumNumberOfAcquisitionsInConfiguration(unsigned m);

  unsigned maximumNumberOfEventsInConfiguration() const;
  void     maximumNumberOfEventsInConfiguration(unsigned m);

  UtlTimeDifference maximumTimeOfConfiguration() const;
  void              maximumTimeOfConfiguration(UtlTimeDifference n);

  // Limits acquisition

  unsigned maximumNumberOfEventsInAcquisition() const;
  void     maximumNumberOfEventsInAcquisition(unsigned m);
 
  UtlTimeDifference maximumTimeOfAcquisition() const;
  void              maximumTimeOfAcquisition(UtlTimeDifference n);

  // Limits spill

  UtlTimeDifference maximumTimeOfSpill() const;
  void              maximumTimeOfSpill(UtlTimeDifference n);

  // Limits event

  UtlTimeDifference maximumTimeOfEvent() const;
  void              maximumTimeOfEvent(UtlTimeDifference n);

  // Limits slow readout
  UtlTimeDifference minimumTimeBeforeSlowReadout() const;
  void              minimumTimeBeforeSlowReadout(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;

protected:
  unsigned _configurationNumberInRun;

  unsigned _maximumNumberOfAcquisitionsInConfiguration;
  unsigned _maximumNumberOfEventsInConfiguration;
  UtlTimeDifference _maximumTimeOfConfiguration;

  unsigned _maximumNumberOfEventsInAcquisition;
  UtlTimeDifference _maximumTimeOfAcquisition;

  UtlTimeDifference _maximumTimeOfSpill;

  UtlTimeDifference _maximumTimeOfEvent;

  UtlTimeDifference _minimumTimeBeforeSlowReadout;
};


#ifdef CALICE_DAQ_ICC

#include "DaqConfigurationStartV0.hh"


DaqConfigurationStartV1::DaqConfigurationStartV1() {
  reset();
}

DaqConfigurationStartV1::DaqConfigurationStartV1(const DaqConfigurationStartV0 &d) {
  reset();
  _configurationNumberInRun=d.configurationNumberInRun();
  _maximumNumberOfAcquisitionsInConfiguration=d.maximumNumberOfAcquisitionsInConfiguration();
  _maximumNumberOfEventsInConfiguration=d.maximumNumberOfEventsInConfiguration();
}

void DaqConfigurationStartV1::reset() {
  _configurationNumberInRun=0;
  _maximumNumberOfAcquisitionsInConfiguration=0xffffffff;
  _maximumNumberOfEventsInConfiguration=0xffffffff;
  _maximumTimeOfConfiguration=UtlTimeDifference(0x7fffffff,999999);
  _maximumNumberOfEventsInAcquisition=0xffffffff;
  _maximumTimeOfAcquisition=UtlTimeDifference(0x7fffffff,999999);
  _maximumTimeOfSpill=UtlTimeDifference(0x7fffffff,999999);
  _maximumTimeOfEvent=UtlTimeDifference(0x7fffffff,999999);
  _minimumTimeBeforeSlowReadout=UtlTimeDifference(60,0);
}

unsigned DaqConfigurationStartV1::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void DaqConfigurationStartV1::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned DaqConfigurationStartV1::maximumNumberOfAcquisitionsInConfiguration() const {
  return _maximumNumberOfAcquisitionsInConfiguration;
}

void DaqConfigurationStartV1::maximumNumberOfAcquisitionsInConfiguration(unsigned m) {
  _maximumNumberOfAcquisitionsInConfiguration=m;
}

unsigned DaqConfigurationStartV1::maximumNumberOfEventsInConfiguration() const {
  return _maximumNumberOfEventsInConfiguration;
}

void DaqConfigurationStartV1::maximumNumberOfEventsInConfiguration(unsigned m) {
  _maximumNumberOfEventsInConfiguration=m;
}

UtlTimeDifference DaqConfigurationStartV1::maximumTimeOfConfiguration() const {
  return _maximumTimeOfConfiguration;
}
 
void DaqConfigurationStartV1::maximumTimeOfConfiguration(UtlTimeDifference n) {
  _maximumTimeOfConfiguration=n;
}

unsigned DaqConfigurationStartV1::maximumNumberOfEventsInAcquisition() const {
  return _maximumNumberOfEventsInAcquisition;
}
 
void DaqConfigurationStartV1::maximumNumberOfEventsInAcquisition(unsigned m) {
  _maximumNumberOfEventsInAcquisition=m;
}
 
UtlTimeDifference DaqConfigurationStartV1::maximumTimeOfAcquisition() const {
  return _maximumTimeOfAcquisition;
}
                                                                               
void DaqConfigurationStartV1::maximumTimeOfAcquisition(UtlTimeDifference n) {
  _maximumTimeOfAcquisition=n;
}

UtlTimeDifference DaqConfigurationStartV1::maximumTimeOfSpill() const {
  return _maximumTimeOfSpill;
}

void DaqConfigurationStartV1::maximumTimeOfSpill(UtlTimeDifference n) {
  _maximumTimeOfSpill=n;
}
 
UtlTimeDifference DaqConfigurationStartV1::maximumTimeOfEvent() const {
  return _maximumTimeOfEvent;
}

void DaqConfigurationStartV1::maximumTimeOfEvent(UtlTimeDifference n) {
  _maximumTimeOfEvent=n;
}
 
UtlTimeDifference DaqConfigurationStartV1::minimumTimeBeforeSlowReadout() const {
  return _minimumTimeBeforeSlowReadout;
}

void DaqConfigurationStartV1::minimumTimeBeforeSlowReadout(UtlTimeDifference n) {
  _minimumTimeBeforeSlowReadout=n;
}
 
std::ostream& DaqConfigurationStartV1::print(std::ostream &o, std::string s) const {
  o << s << "DaqConfigurationStartV1::print()" << std::endl;
  o << s << " Configuration number            = "
    << std::setw(11) << _configurationNumberInRun << std::endl;
  o << s << " Maximum numbers of acquisitions = " 
    << std::setw(11) << _maximumNumberOfAcquisitionsInConfiguration << std::endl;
  o << s << " Maximum numbers of events       = " 
    << std::setw(11) << _maximumNumberOfEventsInConfiguration << ", "
    << std::setw(11) << _maximumNumberOfEventsInAcquisition << std::endl;
  o << s << " Maximum time of configuration   = " << std::setw(11)
    << (unsigned)(_maximumTimeOfConfiguration.deltaTime()) << " secs" << std::endl;
  o << s << " Maximum time of acquisition     = " << std::setw(11)
    << (unsigned)(_maximumTimeOfAcquisition.deltaTime()) << " secs" << std::endl;
  o << s << " Maximum time of spill           = " << std::setw(11)
    << (unsigned)(_maximumTimeOfSpill.deltaTime()) << " secs" << std::endl;
  o << s << " Maximum time of event           = " << std::setw(11)
    << (unsigned)(_maximumTimeOfEvent.deltaTime()) << " secs" << std::endl;
  o << s << " Minimum time of slow readout    = " << std::setw(11)
    << (unsigned)(_minimumTimeBeforeSlowReadout.deltaTime()) << " secs" << std::endl;
  return o;
}

#endif
#endif
