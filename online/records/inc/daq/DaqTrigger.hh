#ifndef DaqTrigger_HH
#define DaqTrigger_HH

#include <string>
#include <iostream>


class DaqTrigger {

public:
  enum {
    versionNumber=0
  };

  DaqTrigger();
  
  void reset();

  unsigned triggerNumberInRun() const;
  void     triggerNumberInRun(unsigned n);

  unsigned triggerNumberInConfiguration() const;
  void     triggerNumberInConfiguration(unsigned n);
  
  unsigned triggerNumberInAcquisition() const;
  void     triggerNumberInAcquisition(unsigned n);
  
  bool bufferFull() const;

  bool crcBufferFull() const;
  void crcBufferFull(bool b);

  bool tdcBufferFull() const;
  void tdcBufferFull(bool b);

  bool inSpill() const;
  void inSpill(bool b);

  //void increment();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _triggerNumberInRun;
  unsigned _triggerNumberInConfiguration;
  unsigned _triggerNumberInAcquisition;
  UtlPack _flags;
};


#ifdef CALICE_DAQ_ICC

DaqTrigger::DaqTrigger() {
  reset();
}

void DaqTrigger::reset() {
  _triggerNumberInRun=0;
  _triggerNumberInConfiguration=0;
  _triggerNumberInAcquisition=0;
  _flags=0;
}

unsigned DaqTrigger::triggerNumberInRun() const {
  return _triggerNumberInRun;
}

void DaqTrigger::triggerNumberInRun(unsigned n) {
  _triggerNumberInRun=n;
}

unsigned DaqTrigger::triggerNumberInConfiguration() const {
  return _triggerNumberInConfiguration;
}

void DaqTrigger::triggerNumberInConfiguration(unsigned n) {
  _triggerNumberInConfiguration=n;
}

unsigned DaqTrigger::triggerNumberInAcquisition() const {
  return _triggerNumberInAcquisition;
}

void DaqTrigger::triggerNumberInAcquisition(unsigned n) {
  _triggerNumberInAcquisition=n;
}

bool DaqTrigger::bufferFull() const {
  return _flags.bit(0) || _flags.bit(2);
}

bool DaqTrigger::crcBufferFull() const {
  return _flags.bit(0);
}

void DaqTrigger::crcBufferFull(bool b) {
  _flags.bit(0,b);
}

bool DaqTrigger::inSpill() const {
  return _flags.bit(1);
}

void DaqTrigger::inSpill(bool b) {
  _flags.bit(1,b);
}

bool DaqTrigger::tdcBufferFull() const {
  return _flags.bit(2);
}

void DaqTrigger::tdcBufferFull(bool b) {
  _flags.bit(2,b);
}

std::ostream& DaqTrigger::print(std::ostream &o, std::string s) const {
  o << s << "DaqTrigger::print() Trigger numbers in run "
    << _triggerNumberInRun << ", in configuration "
    << _triggerNumberInConfiguration << ", in acquisition "
    << triggerNumberInAcquisition() << std::endl;

  o << s << " Flags: ";
  if(bufferFull()) o << " At least one buffer full";
  else             o << " All buffers not full";
  if(crcBufferFull()) o << ", CRC buffers full";
  else                o << ", CRC buffers not full";
  if(tdcBufferFull()) o << ", TDC buffers full";
  else                o << ", TDC buffers not full";
  if(inSpill())    o << ", In spill";
  else             o << ", Out of spill";
  o << std::endl;

  return o;
}

#endif
#endif
