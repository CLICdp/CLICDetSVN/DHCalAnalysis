#ifndef DaqAcquisitionStartV1_HH
#define DaqAcquisitionStartV1_HH

#include <string>
#include <iostream>

class DaqAcquisitionStartV0;


class DaqAcquisitionStartV1 {

public:
  enum {
    versionNumber=1
  };

  DaqAcquisitionStartV1();
  DaqAcquisitionStartV1(const DaqAcquisitionStartV0 &d);

  void reset();

  unsigned acquisitionNumberInRun() const;
  void     acquisitionNumberInRun(unsigned n);

  unsigned acquisitionNumberInConfiguration() const;
  void     acquisitionNumberInConfiguration(unsigned m);

  unsigned maximumNumberOfEventsInAcquisition() const;
  void     maximumNumberOfEventsInAcquisition(unsigned m);

  UtlTimeDifference maximumTimeOfAcquisition() const;
  void              maximumTimeOfAcquisition(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _acquisitionNumberInRun;
  unsigned _acquisitionNumberInConfiguration;
  unsigned _maximumNumberOfEventsInAcquisition;
  UtlTimeDifference _maximumTimeOfAcquisition;
};


#ifdef CALICE_DAQ_ICC

#include "DaqAcquisitionStartV0.hh"


DaqAcquisitionStartV1::DaqAcquisitionStartV1() {
  reset();
}

DaqAcquisitionStartV1::DaqAcquisitionStartV1(const DaqAcquisitionStartV0 &d) {
  reset();
  _acquisitionNumberInRun=d.acquisitionNumberInRun();
  _acquisitionNumberInConfiguration=d.acquisitionNumberInConfiguration();
  _maximumNumberOfEventsInAcquisition=d.maximumNumberOfEventsInAcquisition();
}

void DaqAcquisitionStartV1::reset() {
  _acquisitionNumberInRun=0;
  _acquisitionNumberInConfiguration=0;
  _maximumNumberOfEventsInAcquisition=0xffffffff;
  _maximumTimeOfAcquisition=UtlTimeDifference(0x7fffffff,999999);
}

unsigned DaqAcquisitionStartV1::acquisitionNumberInRun() const {
  return _acquisitionNumberInRun;
}

void DaqAcquisitionStartV1::acquisitionNumberInRun(unsigned n) {
  _acquisitionNumberInRun=n;
}

unsigned DaqAcquisitionStartV1::acquisitionNumberInConfiguration() const {
  return _acquisitionNumberInConfiguration;
}

void DaqAcquisitionStartV1::acquisitionNumberInConfiguration(unsigned m) {
  _acquisitionNumberInConfiguration=m;
}

unsigned DaqAcquisitionStartV1::maximumNumberOfEventsInAcquisition() const {
  return _maximumNumberOfEventsInAcquisition;
}

void DaqAcquisitionStartV1::maximumNumberOfEventsInAcquisition(unsigned m) {
  _maximumNumberOfEventsInAcquisition=m;
}

UtlTimeDifference DaqAcquisitionStartV1::maximumTimeOfAcquisition() const {
  return _maximumTimeOfAcquisition;
}
 
void DaqAcquisitionStartV1::maximumTimeOfAcquisition(UtlTimeDifference n) {
  _maximumTimeOfAcquisition=n;
}

std::ostream& DaqAcquisitionStartV1::print(std::ostream &o, std::string s) const {
  o << s << "DaqAcquisitionStartV1::print()" << std::endl;
  o << s << " Acquisition numbers in run "
    << _acquisitionNumberInRun << ", in configuration "
    << _acquisitionNumberInConfiguration << std::endl;
  o << s << " Maximum numbers of events in acquisition "
    << _maximumNumberOfEventsInAcquisition << std::endl;
  o << s << " Maximum time of acquisition "
    << (unsigned)(_maximumTimeOfAcquisition.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
