#ifndef DaqRunEndV1_HH
#define DaqRunEndV1_HH

#include <string>
#include <iostream>

class DaqRunEndV0;


class DaqRunEndV1 {

public:
  enum {
    versionNumber=1
  };

  DaqRunEndV1();
  DaqRunEndV1(const DaqRunEndV0 &d);

  void reset();
  
  DaqRunType runType() const;
  void       runType(DaqRunType t);

  unsigned runNumber() const;
  void     runNumber(unsigned n);

  unsigned actualNumberOfConfigurationsInRun() const;
  void     actualNumberOfConfigurationsInRun(unsigned m);

  unsigned actualNumberOfSlowReadoutsInRun() const;
  void     actualNumberOfSlowReadoutsInRun(unsigned m);

  unsigned actualNumberOfAcquisitionsInRun() const;
  void     actualNumberOfAcquisitionsInRun(unsigned m);

  unsigned actualNumberOfEventsInRun() const;
  void     actualNumberOfEventsInRun(unsigned m);

  UtlTimeDifference actualTimeOfRun() const;
  void              actualTimeOfRun(UtlTimeDifference m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  DaqRunType _runType;
  unsigned _runNumber;
  unsigned _actualNumberOfConfigurationsInRun;
  unsigned _actualNumberOfSlowReadoutsInRun;
  unsigned _actualNumberOfAcquisitionsInRun;
  unsigned _actualNumberOfEventsInRun;
  UtlTimeDifference _actualTimeOfRun;
};

#ifdef CALICE_DAQ_ICC

DaqRunEndV1::DaqRunEndV1() {
  reset();
}

DaqRunEndV1::DaqRunEndV1(const DaqRunEndV0 &d) {
  reset();
  _runNumber=d.runNumber();
  _actualNumberOfConfigurationsInRun=d.actualNumberOfConfigurationsInRun();
  _actualNumberOfAcquisitionsInRun=d.actualNumberOfAcquisitionsInRun();
  _actualNumberOfEventsInRun=d.actualNumberOfEventsInRun();
}

void DaqRunEndV1::reset() {
  _runType.type(DaqRunType::daqTest);
  _runType.version(0);

  _runNumber=0;
  _actualNumberOfConfigurationsInRun=0;
  _actualNumberOfSlowReadoutsInRun=0;
  _actualNumberOfAcquisitionsInRun=0;
  _actualNumberOfEventsInRun=0;
  _actualTimeOfRun=UtlTimeDifference(0,0);
}

unsigned DaqRunEndV1::runNumber() const {
  return _runNumber;
}

void DaqRunEndV1::runNumber(unsigned n) {
  _runNumber=n;
}

DaqRunType DaqRunEndV1::runType() const {
  return _runType;
}
 
void DaqRunEndV1::runType(DaqRunType t) {
  _runType=t;
}

unsigned DaqRunEndV1::actualNumberOfConfigurationsInRun() const {
  return _actualNumberOfConfigurationsInRun;
}

void DaqRunEndV1::actualNumberOfConfigurationsInRun(unsigned m) {
  _actualNumberOfConfigurationsInRun=m;
}

unsigned DaqRunEndV1::actualNumberOfSlowReadoutsInRun() const {
  return _actualNumberOfSlowReadoutsInRun;
}

void DaqRunEndV1::actualNumberOfSlowReadoutsInRun(unsigned m) {
  _actualNumberOfSlowReadoutsInRun=m;
}

unsigned DaqRunEndV1::actualNumberOfAcquisitionsInRun() const {
  return _actualNumberOfAcquisitionsInRun;
}

void DaqRunEndV1::actualNumberOfAcquisitionsInRun(unsigned m) {
  _actualNumberOfAcquisitionsInRun=m;
}

unsigned DaqRunEndV1::actualNumberOfEventsInRun() const {
  return _actualNumberOfEventsInRun;
}

void DaqRunEndV1::actualNumberOfEventsInRun(unsigned m) {
  _actualNumberOfEventsInRun=m;
}

UtlTimeDifference DaqRunEndV1::actualTimeOfRun() const {
  return _actualTimeOfRun;
}

void DaqRunEndV1::actualTimeOfRun(UtlTimeDifference m) {
  _actualTimeOfRun=m;
}

std::ostream& DaqRunEndV1::print(std::ostream &o, std::string s) const {
  o << s << "DaqRunEndV1::print()" << std::endl;
  _runType.print(o,s+" ");
  o << s << " Run number " << _runNumber << std::endl;
  o << s << " Actual numbers of configurations "
    << _actualNumberOfConfigurationsInRun << ", slow readouts "
    << _actualNumberOfSlowReadoutsInRun << ", acquisitions "
    << _actualNumberOfAcquisitionsInRun << ", events "
    << _actualNumberOfEventsInRun << std::endl;
  o << s << " Actual time of run "
    << (unsigned)(_actualTimeOfRun.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
