#ifndef DaqAcquisitionStartV0_HH
#define DaqAcquisitionStartV0_HH

#include <string>
#include <iostream>

class DaqAcquisitionStartV0 {

public:
  enum {
    versionNumber=0
  };

  DaqAcquisitionStartV0();

  void reset();

  unsigned acquisitionNumberInRun() const;
  void     acquisitionNumberInRun(unsigned n);

  unsigned acquisitionNumberInConfiguration() const;
  void     acquisitionNumberInConfiguration(unsigned m);

  unsigned maximumNumberOfEventsInAcquisition() const;
  void     maximumNumberOfEventsInAcquisition(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


protected:
  unsigned _acquisitionNumberInRun;
  unsigned _acquisitionNumberInConfiguration;
  unsigned _maximumNumberOfEventsInAcquisition;
};


#ifdef CALICE_DAQ_ICC

DaqAcquisitionStartV0::DaqAcquisitionStartV0() {
  reset();
}

void DaqAcquisitionStartV0::reset() {
  _acquisitionNumberInRun=0;
  _acquisitionNumberInConfiguration=0;
  _maximumNumberOfEventsInAcquisition=0xffffffff;
}

unsigned DaqAcquisitionStartV0::acquisitionNumberInRun() const {
  return _acquisitionNumberInRun;
}

void DaqAcquisitionStartV0::acquisitionNumberInRun(unsigned n) {
  _acquisitionNumberInRun=n;
}

unsigned DaqAcquisitionStartV0::acquisitionNumberInConfiguration() const {
  return _acquisitionNumberInConfiguration;
}

void DaqAcquisitionStartV0::acquisitionNumberInConfiguration(unsigned m) {
  _acquisitionNumberInConfiguration=m;
}

unsigned DaqAcquisitionStartV0::maximumNumberOfEventsInAcquisition() const {
  return _maximumNumberOfEventsInAcquisition;
}

void DaqAcquisitionStartV0::maximumNumberOfEventsInAcquisition(unsigned m) {
  _maximumNumberOfEventsInAcquisition=m;
}

std::ostream& DaqAcquisitionStartV0::print(std::ostream &o, std::string s) const {
  o << s << "DaqAcquisitionStart::print()" << std::endl;
  o << s << " Acquisition numbers in run "
    << _acquisitionNumberInRun << ", in configuration "
    << _acquisitionNumberInConfiguration << std::endl;
  o << s << " Maximum numbers of events in acquisition "
    << _maximumNumberOfEventsInAcquisition << std::endl;
  return o;
}

#endif
#endif
