#ifndef DaqSpillEnd_HH
#define DaqSpillEnd_HH

#include <string>
#include <iostream>

class DaqSpillEndV0;


class DaqSpillEnd {

public:
  enum {
    versionNumber=0
  };

  DaqSpillEnd();

  void reset();

  unsigned spillNumberInRun() const;
  void     spillNumberInRun(unsigned n);

  unsigned spillNumberInConfiguration() const;
  void     spillNumberInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInSpill() const;
  void     actualNumberOfEventsInSpill(unsigned m);

  UtlTimeDifference actualTimeOfSpill() const;
  void              actualTimeOfSpill(UtlTimeDifference n);
 
  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _spillNumberInRun;
  unsigned _spillNumberInConfiguration;
  unsigned _actualNumberOfEventsInSpill;
  UtlTimeDifference _actualTimeOfSpill;
};


#ifdef CALICE_DAQ_ICC

DaqSpillEnd::DaqSpillEnd() {
  reset();
}

void DaqSpillEnd::reset() {
  _spillNumberInRun=0;
  _spillNumberInConfiguration=0;
  _actualNumberOfEventsInSpill=0;
  _actualTimeOfSpill=UtlTimeDifference(0,0);
}

unsigned DaqSpillEnd::spillNumberInRun() const {
  return _spillNumberInRun;
}

void DaqSpillEnd::spillNumberInRun(unsigned n) {
  _spillNumberInRun=n;
}

unsigned DaqSpillEnd::spillNumberInConfiguration() const {
  return _spillNumberInConfiguration;
}

void DaqSpillEnd::spillNumberInConfiguration(unsigned m) {
  _spillNumberInConfiguration=m;
}

unsigned DaqSpillEnd::actualNumberOfEventsInSpill() const {
  return _actualNumberOfEventsInSpill;
}

void DaqSpillEnd::actualNumberOfEventsInSpill(unsigned m) {
  _actualNumberOfEventsInSpill=m;
}

UtlTimeDifference DaqSpillEnd::actualTimeOfSpill() const {
  return _actualTimeOfSpill;
}
  
void DaqSpillEnd::actualTimeOfSpill(UtlTimeDifference n) {
  _actualTimeOfSpill=n;
}
 
std::ostream& DaqSpillEnd::print(std::ostream &o, std::string s) const {
  o << s << "DaqSpillEnd::print() Spill numbers in run "
    << _spillNumberInRun << ", in configuration "
    << _spillNumberInConfiguration
    << ", actual numbers of events "
    << _actualNumberOfEventsInSpill << std::endl;
  o << s << "  Actual time of spill "
    << (unsigned)(_actualTimeOfSpill.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
