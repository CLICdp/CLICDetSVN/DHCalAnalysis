#ifndef DaqAcquisitionEndV1_HH
#define DaqAcquisitionEndV1_HH

#include <string>
#include <iostream>

class DaqAcquisitionEndV0;


class DaqAcquisitionEndV1 {

public:
  enum {
    versionNumber=1
  };

  DaqAcquisitionEndV1();
  DaqAcquisitionEndV1(const DaqAcquisitionEndV0 &d);

  void reset();

  unsigned acquisitionNumberInRun() const;
  void     acquisitionNumberInRun(unsigned n);

  unsigned acquisitionNumberInConfiguration() const;
  void     acquisitionNumberInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInAcquisition() const;
  void     actualNumberOfEventsInAcquisition(unsigned m);

  UtlTimeDifference actualTimeOfAcquisition() const;
  void              actualTimeOfAcquisition(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _acquisitionNumberInRun;
  unsigned _acquisitionNumberInConfiguration;
  unsigned _actualNumberOfEventsInAcquisition;
  UtlTimeDifference _actualTimeOfAcquisition;
};


#ifdef CALICE_DAQ_ICC

#include "DaqAcquisitionEndV0.hh"


DaqAcquisitionEndV1::DaqAcquisitionEndV1() {
  reset();
}

DaqAcquisitionEndV1::DaqAcquisitionEndV1(const DaqAcquisitionEndV0 &d) {
  reset();
  _acquisitionNumberInRun=d.acquisitionNumberInRun();
  _acquisitionNumberInConfiguration=d.acquisitionNumberInConfiguration();
  _actualNumberOfEventsInAcquisition=d.actualNumberOfEventsInAcquisition();
}

void DaqAcquisitionEndV1::reset() {
  _acquisitionNumberInRun=0;
  _acquisitionNumberInConfiguration=0;
  _actualNumberOfEventsInAcquisition=0;
  _actualTimeOfAcquisition=UtlTimeDifference(0,0);
}

unsigned DaqAcquisitionEndV1::acquisitionNumberInRun() const {
  return _acquisitionNumberInRun;
}

void DaqAcquisitionEndV1::acquisitionNumberInRun(unsigned n) {
  _acquisitionNumberInRun=n;
}

unsigned DaqAcquisitionEndV1::acquisitionNumberInConfiguration() const {
  return _acquisitionNumberInConfiguration;
}

void DaqAcquisitionEndV1::acquisitionNumberInConfiguration(unsigned m) {
  _acquisitionNumberInConfiguration=m;
}

unsigned DaqAcquisitionEndV1::actualNumberOfEventsInAcquisition() const {
  return _actualNumberOfEventsInAcquisition;
}

void DaqAcquisitionEndV1::actualNumberOfEventsInAcquisition(unsigned m) {
  _actualNumberOfEventsInAcquisition=m;
}

UtlTimeDifference DaqAcquisitionEndV1::actualTimeOfAcquisition() const {
  return _actualTimeOfAcquisition;
}
 
void DaqAcquisitionEndV1::actualTimeOfAcquisition(UtlTimeDifference n) {
  _actualTimeOfAcquisition=n;
}

std::ostream& DaqAcquisitionEndV1::print(std::ostream &o, std::string s) const {
  o << s << "DaqAcquisitionEndV1::print()" << std::endl;
  o << s << " Acquisition numbers in run "
    << _acquisitionNumberInRun << ", in configuration "
    << _acquisitionNumberInConfiguration << std::endl;
  o << s << "  Actual numbers of events in acquisition "
    << _actualNumberOfEventsInAcquisition << std::endl;
  o << s << "  Actual time of acquisition "
    << (unsigned)(_actualTimeOfAcquisition.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
