#ifndef DaqAcquisitionEndV0_HH
#define DaqAcquisitionEndV0_HH

#include <string>
#include <iostream>

class DaqAcquisitionEndV0 {

public:
  enum {
    versionNumber=0
  };

  DaqAcquisitionEndV0();

  void reset();

  unsigned acquisitionNumberInRun() const;
  void     acquisitionNumberInRun(unsigned n);

  unsigned acquisitionNumberInConfiguration() const;
  void     acquisitionNumberInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInAcquisition() const;
  void     actualNumberOfEventsInAcquisition(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


protected:
  unsigned _acquisitionNumberInRun;
  unsigned _acquisitionNumberInConfiguration;
  unsigned _actualNumberOfEventsInAcquisition;
};


#ifdef CALICE_DAQ_ICC

DaqAcquisitionEndV0::DaqAcquisitionEndV0() {
  reset();
}

void DaqAcquisitionEndV0::reset() {
  _acquisitionNumberInRun=0;
  _acquisitionNumberInConfiguration=0;
  _actualNumberOfEventsInAcquisition=0;
}

unsigned DaqAcquisitionEndV0::acquisitionNumberInRun() const {
  return _acquisitionNumberInRun;
}

void DaqAcquisitionEndV0::acquisitionNumberInRun(unsigned n) {
  _acquisitionNumberInRun=n;
}

unsigned DaqAcquisitionEndV0::acquisitionNumberInConfiguration() const {
  return _acquisitionNumberInConfiguration;
}

void DaqAcquisitionEndV0::acquisitionNumberInConfiguration(unsigned m) {
  _acquisitionNumberInConfiguration=m;
}

unsigned DaqAcquisitionEndV0::actualNumberOfEventsInAcquisition() const {
  return _actualNumberOfEventsInAcquisition;
}

void DaqAcquisitionEndV0::actualNumberOfEventsInAcquisition(unsigned m) {
  _actualNumberOfEventsInAcquisition=m;
}

std::ostream& DaqAcquisitionEndV0::print(std::ostream &o, std::string s) const {
  o << s << "DaqAcquisitionEndV0::print() Acquisition numbers in run "
    << _acquisitionNumberInRun << ", in configuration "
    << _acquisitionNumberInConfiguration
    << ", actual numbers of events "
    << _actualNumberOfEventsInAcquisition << std::endl;
  return o;
}

#endif
#endif
