#ifndef DaqRunStart_HH
#define DaqRunStart_HH

#include <string>
#include <iostream>

#include "UtlTime.hh"

#include "DaqRunType.hh"


class DaqRunStart {

public:
  enum {
    versionNumber=0
  };

  DaqRunStart();

  void reset();
  
  unsigned runNumber() const;
  void     runNumber(unsigned n);

  DaqRunType runType() const;
  void       runType(DaqRunType n);

  unsigned maximumNumberOfConfigurationsInRun() const;
  void     maximumNumberOfConfigurationsInRun(unsigned m);

  unsigned maximumNumberOfAcquisitionsInRun() const;
  void     maximumNumberOfAcquisitionsInRun(unsigned m);

  unsigned maximumNumberOfEventsInRun() const;
  void     maximumNumberOfEventsInRun(unsigned m);

  UtlTimeDifference maximumTimeOfRun() const;
  void              maximumTimeOfRun(UtlTimeDifference n);

  std::string runLabel() const;
  std::string runTitle() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  DaqRunType _runType;
  unsigned _runNumber;
  unsigned _maximumNumberOfConfigurationsInRun;
  unsigned _maximumNumberOfAcquisitionsInRun;
  unsigned _maximumNumberOfEventsInRun;
  UtlTimeDifference _maximumTimeOfRun;
};


#ifdef CALICE_DAQ_ICC


DaqRunStart::DaqRunStart() {
  reset();
}

void DaqRunStart::reset() {
  _runNumber=0;
  _runType.type(DaqRunType::daqTest);
  _runType.version(0);
  _maximumNumberOfConfigurationsInRun=0xffffffff;
  _maximumNumberOfAcquisitionsInRun=0xffffffff;
  _maximumNumberOfEventsInRun=0xffffffff;
  _maximumTimeOfRun=UtlTimeDifference(0x7fffffff,999999);
}

unsigned DaqRunStart::runNumber() const {
  return _runNumber;
}

void DaqRunStart::runNumber(unsigned n) {
  _runNumber=n;
}

DaqRunType DaqRunStart::runType() const {
  return _runType;
}

void DaqRunStart::runType(DaqRunType n) {
  _runType=n;
}

unsigned DaqRunStart::maximumNumberOfConfigurationsInRun() const {
  return _maximumNumberOfConfigurationsInRun;
}

void DaqRunStart::maximumNumberOfConfigurationsInRun(unsigned m) {
  _maximumNumberOfConfigurationsInRun=m;
}

unsigned DaqRunStart::maximumNumberOfAcquisitionsInRun() const {
  return _maximumNumberOfAcquisitionsInRun;
}

void DaqRunStart::maximumNumberOfAcquisitionsInRun(unsigned m) {
  _maximumNumberOfAcquisitionsInRun=m;
}

unsigned DaqRunStart::maximumNumberOfEventsInRun() const {
  return _maximumNumberOfEventsInRun;
}

void DaqRunStart::maximumNumberOfEventsInRun(unsigned m) {
  _maximumNumberOfEventsInRun=m;
}

UtlTimeDifference DaqRunStart::maximumTimeOfRun() const {
  return _maximumTimeOfRun;
}

void DaqRunStart::maximumTimeOfRun(UtlTimeDifference n) {
  _maximumTimeOfRun=n;
}

std::string DaqRunStart::runLabel() const {
  std::ostringstream sout;
  sout << "Run" << std::setw(6) << std::setfill('0') << _runNumber;
  return sout.str();
}

std::string DaqRunStart::runTitle() const {
  std::ostringstream sout;
  sout << "Run " << _runNumber;
  return sout.str();
}

std::ostream& DaqRunStart::print(std::ostream &o, std::string s) const {
  o << s << "DaqRunStart::print()" << std::endl;
  _runType.print(o,s+" ");
  o << s << " Run number " << std::setw(10) << _runNumber << std::endl;
  o << s << " Maximum numbers of configurations "
    << _maximumNumberOfConfigurationsInRun << ", acquisitions "
    << _maximumNumberOfAcquisitionsInRun << ", events "
    << _maximumNumberOfEventsInRun << std::endl;
  o << s << " Maximum time of run "
    << (unsigned)(_maximumTimeOfRun.deltaTime()) << " secs" << std::endl;
  
  return o;
}

#endif
#endif
