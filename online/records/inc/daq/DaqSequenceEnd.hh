#ifndef DaqSequenceEnd_HH
#define DaqSequenceEnd_HH

#include <string>
#include <iostream>

#include "DaqSequenceType.hh"


class DaqSequenceEnd {

public:
  enum {
    versionNumber=0
  };

  DaqSequenceEnd();

  void reset();

  DaqSequenceType sequenceType() const;
  void            sequenceType(DaqSequenceType n);

  unsigned actualNumberOfRuns() const;
  void     actualNumberOfRuns(unsigned m);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  DaqSequenceType _sequenceType;
  unsigned _actualNumberOfRuns;
};


#ifdef CALICE_DAQ_ICC


DaqSequenceEnd::DaqSequenceEnd() {
  reset();
}

void DaqSequenceEnd::reset() {
  _sequenceType.type(DaqSequenceType::daqSimple);
  _actualNumberOfRuns=1;
}

DaqSequenceType DaqSequenceEnd::sequenceType() const {
  return _sequenceType;
}

void DaqSequenceEnd::sequenceType(DaqSequenceType n) {
  _sequenceType=n;
}

unsigned DaqSequenceEnd::actualNumberOfRuns() const {
  return _actualNumberOfRuns;
}

void DaqSequenceEnd::actualNumberOfRuns(unsigned m) {
  _actualNumberOfRuns=m;
}

std::ostream& DaqSequenceEnd::print(std::ostream &o, std::string s) const {
  o << s << "DaqSequenceEnd::print()" << std::endl;
  _sequenceType.print(o,s+" ");
  o << s << " Actual number of runs "
    << _actualNumberOfRuns << std::endl;
  
  return o;
}

#endif
#endif
