#ifndef DaqMessage_HH
#define DaqMessage_HH

#include <string>
#include <iostream>

class DaqMessage {

public:
  enum {
    versionNumber=0
  };

  DaqMessage();
  DaqMessage(const char* const m);
  DaqMessage(const std::string &m);

  std::string message() const;
  void message(const std::string &m);
  void message(const char* const m);
  
  unsigned numberOfBytes() const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  char _message[128];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

DaqMessage::DaqMessage() {
  _message[127]='\0';
  _message[0]='\0';
}

DaqMessage::DaqMessage(const char* const m) {
  _message[127]='\0';
  message(m);
}

DaqMessage::DaqMessage(const std::string &m) {
  _message[127]='\0';
  message(m);
}

std::string DaqMessage::message() const {
  return _message;
}

void DaqMessage::message(const char* const m) {
  strncpy(_message,m,127);
}

void DaqMessage::message(const std::string &m) {
  if(m.size()<128) {
    strncpy(_message,m.c_str(),m.size());
    _message[m.size()]='\0';
  } else {
    strncpy(_message,m.c_str(),127);
  }
}

unsigned DaqMessage::numberOfBytes() const {
  return strlen(_message)+1;
}

std::ostream& DaqMessage::print(std::ostream &o, std::string s) const {
  o << s << "DaqMessage::print()  Length = " << numberOfBytes() << std::endl;
  o << s << " Message = \"" << _message << "\"" << std::endl;
  return o;
}

#endif
#endif
