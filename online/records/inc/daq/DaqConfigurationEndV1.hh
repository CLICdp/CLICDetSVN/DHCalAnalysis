#ifndef DaqConfigurationEndV1_HH
#define DaqConfigurationEndV1_HH

#include <string>
#include <iostream>

class DaqConfigurationEndV0;


class DaqConfigurationEndV1 {

public:
  enum {
    versionNumber=1
  };

  DaqConfigurationEndV1();
  DaqConfigurationEndV1(const DaqConfigurationEndV0 &d);

  void reset();

  unsigned configurationNumberInRun() const;
  void     configurationNumberInRun(unsigned n);

  unsigned actualNumberOfSlowReadoutsInConfiguration() const;
  void     actualNumberOfSlowReadoutsInConfiguration(unsigned m);

  unsigned actualNumberOfAcquisitionsInConfiguration() const;
  void     actualNumberOfAcquisitionsInConfiguration(unsigned m);

  unsigned actualNumberOfEventsInConfiguration() const;
  void     actualNumberOfEventsInConfiguration(unsigned m);

  UtlTimeDifference actualTimeOfConfiguration() const;
  void              actualTimeOfConfiguration(UtlTimeDifference m);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _configurationNumberInRun;
  unsigned _actualNumberOfSlowReadoutsInConfiguration;
  unsigned _actualNumberOfAcquisitionsInConfiguration;
  unsigned _actualNumberOfEventsInConfiguration;
  UtlTimeDifference _actualTimeOfConfiguration;
};


#ifdef CALICE_DAQ_ICC

#include "DaqConfigurationEndV0.hh"


DaqConfigurationEndV1::DaqConfigurationEndV1() {
  reset();
}

DaqConfigurationEndV1::DaqConfigurationEndV1(const DaqConfigurationEndV0 &d) {
  reset();
  _configurationNumberInRun=d.configurationNumberInRun();
  _actualNumberOfAcquisitionsInConfiguration=d.actualNumberOfAcquisitionsInConfiguration();
  _actualNumberOfEventsInConfiguration=d.actualNumberOfEventsInConfiguration();
}

void DaqConfigurationEndV1::reset() {
  _configurationNumberInRun=0;
  _actualNumberOfSlowReadoutsInConfiguration=0;
  _actualNumberOfAcquisitionsInConfiguration=0;
  _actualNumberOfEventsInConfiguration=0;
  _actualTimeOfConfiguration=UtlTimeDifference(0,0);
}

unsigned DaqConfigurationEndV1::configurationNumberInRun() const {
  return _configurationNumberInRun;
}

void DaqConfigurationEndV1::configurationNumberInRun(unsigned n) {
  _configurationNumberInRun=n;
}

unsigned DaqConfigurationEndV1::actualNumberOfSlowReadoutsInConfiguration() const {
  return _actualNumberOfSlowReadoutsInConfiguration;
}

void DaqConfigurationEndV1::actualNumberOfSlowReadoutsInConfiguration(unsigned m) {
  _actualNumberOfSlowReadoutsInConfiguration=m;
}

unsigned DaqConfigurationEndV1::actualNumberOfAcquisitionsInConfiguration() const {
  return _actualNumberOfAcquisitionsInConfiguration;
}

void DaqConfigurationEndV1::actualNumberOfAcquisitionsInConfiguration(unsigned m) {
  _actualNumberOfAcquisitionsInConfiguration=m;
}

unsigned DaqConfigurationEndV1::actualNumberOfEventsInConfiguration() const {
  return _actualNumberOfEventsInConfiguration;
}

void DaqConfigurationEndV1::actualNumberOfEventsInConfiguration(unsigned m) {
  _actualNumberOfEventsInConfiguration=m;
  }

UtlTimeDifference DaqConfigurationEndV1::actualTimeOfConfiguration() const {
  return _actualTimeOfConfiguration;
}

void DaqConfigurationEndV1::actualTimeOfConfiguration(UtlTimeDifference m) {
  _actualTimeOfConfiguration=m;
}

std::ostream& DaqConfigurationEndV1::print(std::ostream &o, std::string s) const {
  o << s << "DaqConfigurationEndV1::print()" << std::endl;
  o << s << " Configuration number = "
    << std::setw(10) << _configurationNumberInRun << std::endl;
  o << s << " Actual  numbers of slow readouts = "
    << std::setw(10) << _actualNumberOfSlowReadoutsInConfiguration << std::endl;
  o << s << " Actual  numbers of acquisitions  = "
    << std::setw(10) << _actualNumberOfAcquisitionsInConfiguration << std::endl;
  o << s << " Actual  numbers of events        = "
    << std::setw(10) << _actualNumberOfEventsInConfiguration << std::endl;
  o << s << " Actual  time of configuration "
    << (unsigned)(_actualTimeOfConfiguration.deltaTime()) << " secs" << std::endl;
  return o;
}

#endif
#endif
