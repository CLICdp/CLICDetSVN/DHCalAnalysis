#ifndef DaqTwoTimer_HH
#define DaqTwoTimer_HH

#include <string>
#include <iostream>
#include <iomanip>

#include "UtlTime.hh"


class DaqTwoTimer {

public:
  enum {
    versionNumber=0
  };

  DaqTwoTimer(bool u=true);

  unsigned timerId() const;
  void     timerId(unsigned n);

  UtlTime startTime() const;
  void setStartTime();

  UtlTime endTime() const;
  void setEndTime();
    
  UtlTimeDifference timeDifference() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _timerId;
  UtlTime _time[2];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

DaqTwoTimer::DaqTwoTimer(bool u) {
  memset(this,0,sizeof(DaqTwoTimer));
  if(u) _time[0].update();
}

unsigned DaqTwoTimer::timerId() const {
  return _timerId;
}

void DaqTwoTimer::timerId(unsigned n) {
  _timerId=n;
}

UtlTime DaqTwoTimer::startTime() const {
  return _time[0];
}

void DaqTwoTimer::setStartTime() {
  _time[0].update();
}

UtlTime DaqTwoTimer::endTime() const {
  return _time[1];
}

void DaqTwoTimer::setEndTime() {
  _time[1].update();
}

UtlTimeDifference DaqTwoTimer::timeDifference() const {
  return _time[1]-_time[0];
}

std::ostream& DaqTwoTimer::print(std::ostream &o, std::string s) const {
  o << s << "DaqTwoTimer::print()" << std::endl;

  o << s << " Timer Id  = " << printHex(_timerId) << std::endl;

  o << s << " Start time = " << _time[0] << std::endl;
  o << s << " End time   = " << _time[1] << std::endl;
  o << s << " Difference = " << timeDifference() << std::endl;
  
  return o;
}

#endif
#endif
