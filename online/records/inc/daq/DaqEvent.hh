#ifndef DaqEvent_HH
#define DaqEvent_HH

#include <string>
#include <iostream>


class DaqEvent {

public:
  enum {
    versionNumber=0
  };

  DaqEvent();
  
  void reset();

  unsigned eventNumberInRun() const;
  void     eventNumberInRun(unsigned n);

  unsigned eventNumberInConfiguration() const;
  void     eventNumberInConfiguration(unsigned n);
  
  unsigned eventNumberInAcquisition() const;
  void     eventNumberInAcquisition(unsigned n);
  
  void increment();

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _eventNumberInRun;
  unsigned _eventNumberInConfiguration;
  unsigned _eventNumberInAcquisition;
};


#ifdef CALICE_DAQ_ICC

DaqEvent::DaqEvent() {
  reset();
}

void DaqEvent::reset() {
  _eventNumberInRun=0;
  _eventNumberInConfiguration=0;
  _eventNumberInAcquisition=0;
}

unsigned DaqEvent::eventNumberInRun() const {
  return _eventNumberInRun;
}

void DaqEvent::eventNumberInRun(unsigned n) {
  _eventNumberInRun=n;
}

unsigned DaqEvent::eventNumberInConfiguration() const {
  return _eventNumberInConfiguration;
}

void DaqEvent::eventNumberInConfiguration(unsigned n) {
  _eventNumberInConfiguration=n;
}

unsigned DaqEvent::eventNumberInAcquisition() const {
  return _eventNumberInAcquisition;
  }

void DaqEvent::eventNumberInAcquisition(unsigned n) {
  _eventNumberInAcquisition=n;
}

void DaqEvent::increment() {
  _eventNumberInRun++;
  _eventNumberInConfiguration++;
  _eventNumberInAcquisition++;
}

std::ostream& DaqEvent::print(std::ostream &o, std::string s) const {
  o << s << "DaqEvent::print() Event numbers in run "
    << _eventNumberInRun << ", in configuration "
    << _eventNumberInConfiguration << ", in acquisition "
    << _eventNumberInAcquisition << std::endl;
  return o;
}

#endif
#endif
