#ifndef DaqSpillStart_HH
#define DaqSpillStart_HH

#include <string>
#include <iostream>

class DaqSpillStartV0;


class DaqSpillStart {

public:
  enum {
    versionNumber=0
  };

  DaqSpillStart();

  void reset();

  unsigned spillNumberInRun() const;
  void     spillNumberInRun(unsigned n);

  unsigned spillNumberInConfiguration() const;
  void     spillNumberInConfiguration(unsigned m);

  unsigned maximumNumberOfEventsInSpill() const;
  void     maximumNumberOfEventsInSpill(unsigned m);

  UtlTimeDifference maximumTimeOfSpill() const;
  void              maximumTimeOfSpill(UtlTimeDifference n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  unsigned _spillNumberInRun;
  unsigned _spillNumberInConfiguration;
  unsigned _maximumNumberOfEventsInSpill;
  UtlTimeDifference _maximumTimeOfSpill;
};


#ifdef CALICE_DAQ_ICC


DaqSpillStart::DaqSpillStart() {
  reset();
}

void DaqSpillStart::reset() {
  _spillNumberInRun=0;
  _spillNumberInConfiguration=0;
  _maximumNumberOfEventsInSpill=511;
  _maximumTimeOfSpill=UtlTimeDifference(0x7fffffff,999999);
}

unsigned DaqSpillStart::spillNumberInRun() const {
  return _spillNumberInRun;
}

void DaqSpillStart::spillNumberInRun(unsigned n) {
  _spillNumberInRun=n;
}

unsigned DaqSpillStart::spillNumberInConfiguration() const {
  return _spillNumberInConfiguration;
}

void DaqSpillStart::spillNumberInConfiguration(unsigned m) {
  _spillNumberInConfiguration=m;
}

unsigned DaqSpillStart::maximumNumberOfEventsInSpill() const {
  return _maximumNumberOfEventsInSpill;
}

void DaqSpillStart::maximumNumberOfEventsInSpill(unsigned m) {
  _maximumNumberOfEventsInSpill=m;
}

UtlTimeDifference DaqSpillStart::maximumTimeOfSpill() const {
  return _maximumTimeOfSpill;
}
 
void DaqSpillStart::maximumTimeOfSpill(UtlTimeDifference n) {
  _maximumTimeOfSpill=n;
}

std::ostream& DaqSpillStart::print(std::ostream &o, std::string s) const {
  o << s << "DaqSpillStart::print() Spill numbers in run "
    << _spillNumberInRun << ", in configuration "
    << _spillNumberInConfiguration
    << ", maximum numbers of events "
    << _maximumNumberOfEventsInSpill << std::endl;
  o << s << " Maximum time of spill "
    << (unsigned)(_maximumTimeOfSpill.deltaTime()) << " secs" << std::endl;

  return o;
}

#endif
#endif
