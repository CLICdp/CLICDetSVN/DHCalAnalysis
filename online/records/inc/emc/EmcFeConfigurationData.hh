#ifndef EmcFeConfigurationData_HH
#define EmcFeConfigurationData_HH

#include <string>
#include <iostream>

#include "CrcFeConfigurationData.hh"

#include "EmcVfeControl.hh"


class EmcFeConfigurationData : public CrcFeConfigurationData {

public:
  enum {
    versionNumber=0
  };

  EmcFeConfigurationData();

  EmcVfeControl vfeControl() const;
  void          vfeControl(EmcVfeControl v);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //EmcVfeControl _vfeControl;
};


#ifdef CALICE_DAQ_ICC

EmcFeConfigurationData::EmcFeConfigurationData() {
}

EmcVfeControl EmcFeConfigurationData::vfeControl() const {
  return CrcFeConfigurationData::vfeControl();
}

void EmcFeConfigurationData::vfeControl(EmcVfeControl v) {
  CrcFeConfigurationData::vfeControl(v.data());
}

std::ostream& EmcFeConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "EmcFeConfigurationData::print()" << std::endl;
  CrcFeConfigurationData::print(o,s+" ");

  vfeControl().print(o,s+"  ");

  return o;
}

#endif
#endif
