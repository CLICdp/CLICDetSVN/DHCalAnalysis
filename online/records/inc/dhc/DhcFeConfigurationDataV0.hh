//
// $Id: DhcFeConfigurationDataV0.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef DhcFeConfigurationDataV0_HH
#define DhcFeConfigurationDataV0_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class DhcFeConfigurationDataV0 {

public:
  enum {
    versionNumber=0
  };

  enum ShapeTime {
    t65=0,
    t85=1,
    t100=2,
    t125=3,
    endOfDcrShapingTineEnum
  };

public:
  DhcFeConfigurationDataV0();

  const unsigned char* chipid() const;
  void          chipid(unsigned char n);

  const unsigned char* plsr() const;
  void          plsr(unsigned char n);

  const unsigned char* intd() const;
  void          intd(unsigned char n);

  const unsigned char* shp2() const;
  void          shp2(unsigned char n);

  const unsigned char* shp1() const;
  void          shp1(unsigned char n);

  const unsigned char* blrd() const;
  void          blrd(unsigned char n);

  const unsigned char* vtnd() const;
  void          vtnd(unsigned char n);

  const unsigned char* vtpd() const;
  void          vtpd(unsigned char n);

  const unsigned char* dcr() const;
  void          dcr(unsigned char n);

  const unsigned char dcrCapSelect() const;
  void                dcrCapSelect(unsigned char n);

  const bool dcrLowGain() const;
  void       dcrLowGain(bool b);

  const bool dcrDisableBaselineRestore() const;
  void       dcrDisableBaselineRestore(bool b);
  
  const bool dcrAccept() const;
  void       dcrAccept(bool b);

  const bool dcrActivatePipeline() const;
  void       dcrActivatePipeline(bool b);

  const bool dcrExternalTrigger() const;
  void       dcrExternalTrigger(bool b);

  const unsigned char* inj() const;
  void inj(const unsigned char* n);
  void inj(long long n);

  const unsigned char* kill() const;
  void kill(const unsigned char* n);
  void kill(long long n);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  static const std::string _dcrShapingTime[endOfDcrShapingTineEnum];

private:
  unsigned char _chipid;
  unsigned char _plsr;
  unsigned char _intd;
  unsigned char _shp2;
  unsigned char _shp1;
  unsigned char _blrd;
  unsigned char _vtnd;
  unsigned char _vtpd;
  unsigned char _dcr;
  unsigned char _inj[8];
  unsigned char _kill[8];
  unsigned char _padding[3]; // Make a multiple of 4 bytes
};

#ifdef CALICE_DAQ_ICC

DhcFeConfigurationDataV0::DhcFeConfigurationDataV0() {
  _chipid=21;
  _plsr=0;
  _intd=139;
  _shp2=121;
  _shp1=116;
  _blrd=81;
  _vtnd=0;
  _vtpd=255;
  _dcr=0;
  memset(_inj,0,sizeof(_inj));
  memset(_kill,0,sizeof(_kill));
}

const unsigned char* DhcFeConfigurationDataV0::chipid() const {
  return &_chipid;
}

void DhcFeConfigurationDataV0::chipid(unsigned char n) {
  _chipid=n;
}

const unsigned char* DhcFeConfigurationDataV0::plsr() const {
  return &_plsr;
}

void DhcFeConfigurationDataV0::plsr(unsigned char n) {
  _plsr=n;
}

const unsigned char* DhcFeConfigurationDataV0::intd() const {
  return &_intd;
}

void DhcFeConfigurationDataV0::intd(unsigned char n) {
  _intd=n;
}

const unsigned char* DhcFeConfigurationDataV0::shp2() const {
  return &_shp2;
}

void DhcFeConfigurationDataV0::shp2(unsigned char n) {
  _shp2=n;
}

const unsigned char* DhcFeConfigurationDataV0::shp1() const {
  return &_shp1;
}

void DhcFeConfigurationDataV0::shp1(unsigned char n) {
  _shp1=n;
}

const unsigned char* DhcFeConfigurationDataV0::blrd() const {
  return &_blrd;
}

void DhcFeConfigurationDataV0::blrd(unsigned char n) {
  _blrd=n;
}

const unsigned char* DhcFeConfigurationDataV0::vtnd() const {
  return &_vtnd;
}

void DhcFeConfigurationDataV0::vtnd(unsigned char n) {
  _vtnd=n;
}

const unsigned char* DhcFeConfigurationDataV0::vtpd() const {
  return &_vtpd;
}

void DhcFeConfigurationDataV0::vtpd(unsigned char n) {
  _vtpd=n;
}

const unsigned char* DhcFeConfigurationDataV0::dcr() const {
  return &_dcr;
}

void DhcFeConfigurationDataV0::dcr(unsigned char n) {
  _dcr=n;
}

const unsigned char DhcFeConfigurationDataV0::dcrCapSelect() const {
  return (_dcr & 0xc0) >> 6;
}

void DhcFeConfigurationDataV0::dcrCapSelect(unsigned char n) {
  assert(n<4);
  _dcr &= 0x3f;
  _dcr |= (n<<6);
}

const bool DhcFeConfigurationDataV0::dcrLowGain() const {
  return _dcr & 0x20;
}

void DhcFeConfigurationDataV0::dcrLowGain(bool b) {
  ( b ? _dcr |= 0x20 : _dcr &= ~0x20 );
}

const bool DhcFeConfigurationDataV0::dcrDisableBaselineRestore() const {
  return _dcr & 0x10;
}

void DhcFeConfigurationDataV0::dcrDisableBaselineRestore(bool b) {
  ( b ? _dcr |= 0x10 : _dcr &= ~0x10 );
}
  
const bool DhcFeConfigurationDataV0::dcrAccept() const {
  return _dcr & 0x08;
}

void DhcFeConfigurationDataV0::dcrAccept(bool b) {
  ( b ? _dcr |= 0x08 : _dcr &= ~0x08 );
}

const bool DhcFeConfigurationDataV0::dcrActivatePipeline() const {
  return _dcr & 0x04;
}

void DhcFeConfigurationDataV0::dcrActivatePipeline(bool b) {
  ( b ? _dcr |= 0x04 : _dcr &= ~0x04 );
}

const bool DhcFeConfigurationDataV0::dcrExternalTrigger() const {
  return _dcr & 0x02;
}

void DhcFeConfigurationDataV0::dcrExternalTrigger(bool b) {
  ( b ? _dcr |= 0x02 : _dcr &= ~0x02 );
}

const unsigned char* DhcFeConfigurationDataV0::inj() const {
  return _inj;
}

void  DhcFeConfigurationDataV0::inj(const unsigned char* n) {
  memcpy(_inj,n,sizeof(_inj));
}

void  DhcFeConfigurationDataV0::inj(long long n) {
  memcpy(_inj,&n,sizeof(_inj));
}

const unsigned char* DhcFeConfigurationDataV0::kill() const {
  return _kill;
}

void  DhcFeConfigurationDataV0::kill(const unsigned char* n) {
  memcpy(_kill,n,sizeof(_kill));
}

void  DhcFeConfigurationDataV0::kill(long long n) {
  memcpy(_kill,&n,sizeof(_kill));
}

std::ostream& DhcFeConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "DhcFeConfigurationDataV0::print()" << std::endl;

  o << s << " ChipID  = " << printHex(_chipid) << std::endl;
  o << s << " PLSR    = " << printHex(_plsr) << std::endl;
  o << s << " IntD    = " << printHex(_intd) << std::endl;
  o << s << " Shp2D   = " << printHex(_shp2) << std::endl;
  o << s << " Shp1D   = " << printHex(_shp1) << std::endl;
  o << s << " BlrD    = " << printHex(_blrd) << std::endl;
  o << s << " VtnD    = " << printHex(_vtnd) << std::endl;
  o << s << " VtpD    = " << printHex(_vtpd) << std::endl;
  o << s << " DCR     = " << printHex(_dcr) << std::endl;

  o << s << "   Shaping Time is " << _dcrShapingTime[dcrCapSelect()];
  o << s << std::endl;
  o << s << "   Gain is " << (dcrLowGain() ? "Low,RPC" : "High,GEM");
  o << s << std::endl;
  o << s << "   Baseline Restore is " << (dcrDisableBaselineRestore() ? "Disabled" : "Enabled");
  o << s << std::endl;
  o << s << "   Chip wide mask is " << (dcrAccept() ? "Accept" : "Mask");
  o << s << std::endl;
  o << s << "   Pipeline is " << (dcrActivatePipeline() ? "Enabled" : "Disabled");
  o << s << std::endl;
  o << s << "   Trigger is " << (dcrExternalTrigger() ? "External" : "Internal");
  o << s << std::endl;

  o << s << " Inj     = ";
  for(unsigned i(0); i<sizeof(_inj); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_inj[i]) << std::dec;
  o << s << std::endl;

  o << s << " Kill    = ";
  for(unsigned i(0); i<sizeof(_kill); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_kill[i]) << std::dec;
  o << s << std::setfill(' ');
  o << s << std::endl;

  return o;
}

const std::string DhcFeConfigurationDataV0::_dcrShapingTime[]={
  "65ns","85ns","100ns","125ns"
};

#endif //CALICE_DAQ_ICC
#endif // DhcFeConfigurationDataV0_HH
