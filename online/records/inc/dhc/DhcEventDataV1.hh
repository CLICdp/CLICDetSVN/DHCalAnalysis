//
// $Id: DhcEventDataV1.hh,v 1.6 2008/04/01 18:00:32 jls Exp $
//

#ifndef DhcEventDataV1_HH
#define DhcEventDataV1_HH

#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

#include "UtlPrintHex.hh"
#include "UtlPack.hh"

#include "DhcFeHitDataV1.hh"


class DhcEventDataV1 {

public:
  enum {
    versionNumber=1
  };

  DhcEventDataV1();

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);

  const unsigned* data() const;
  unsigned*       data();

  const DhcFeHitDataV1* feData(unsigned f) const;
  DhcFeHitDataV1*       feData(unsigned f);

  std::vector< std::vector<const DhcFeHitDataV1*>*>* events(unsigned t=24) const;

  bool verify(bool p=false) const;

  std::ostream& print(std::ostream &o, std::string s="", bool raw=false) const;


private:
  unsigned _numberOfWords;
};

#ifdef CALICE_DAQ_ICC

DhcEventDataV1::DhcEventDataV1() {
  memset(this,0,sizeof(DhcEventDataV1));
}

unsigned DhcEventDataV1::numberOfWords() const {
  return _numberOfWords;
}

void DhcEventDataV1::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const unsigned* DhcEventDataV1::data() const {
  return (&_numberOfWords)+1;
}

unsigned* DhcEventDataV1::data() {
  return (&_numberOfWords)+1;
}

const DhcFeHitDataV1* DhcEventDataV1::feData(unsigned f) const {

  unsigned n(4*f);
  if (_numberOfWords<n) return 0;

  return (const DhcFeHitDataV1*)(data()+n);
}

DhcFeHitDataV1* DhcEventDataV1::feData(unsigned f) {

  unsigned n(4*f);
  if (_numberOfWords<n) return 0;

  return (DhcFeHitDataV1*)(data()+n);
}

std::vector< std::vector<const DhcFeHitDataV1*>*>*
DhcEventDataV1::events(unsigned t) const {

  std::vector< std::vector<const DhcFeHitDataV1*>*>* ev =
    new std::vector< std::vector<const DhcFeHitDataV1*>*>;

  if (_numberOfWords>0) {

    double tslast = feData(0)->timestamp();
    ev->push_back(new std::vector<const DhcFeHitDataV1*>);
    (ev->back())->push_back(feData(0));

    for (unsigned i(1); i<_numberOfWords/4; i++) {

      if (feData(i)->trg()) {
	double timestamp = feData(i)->timestamp();
	if (fabs(timestamp - tslast) > t)
	  ev->push_back(new std::vector<const DhcFeHitDataV1*>);
	tslast = timestamp;
      }
      (ev->back())->push_back(feData(i));
    }
  }
  return ev;
}

bool DhcEventDataV1::verify(bool p) const {
  if(_numberOfWords==0) return true;

  for(unsigned i(0);i<_numberOfWords/4; i++) {
    const DhcFeHitDataV1* hits(feData(i));
    if (!hits->verify()) return false;
  }
  return true;
}

std::ostream& DhcEventDataV1::print(std::ostream &o, std::string s, bool raw) const {
  o << s << "DhcEventDataV1::print()  Number of words = " 
    << numberOfWords() << std::endl;
  if(_numberOfWords==0) return o;

  if(raw) {
    const unsigned *d(data());
    for(unsigned i(0);i<_numberOfWords;i++) {
      o << s << " Data word " << std::setw(5) << i << " = " << printHex(d[i]) << std::endl;
    }
    return o;
  }

  for(unsigned i(0);i<_numberOfWords/4; i++) {
    const DhcFeHitDataV1* hits(feData(i));
    hits->print(o, " ");
  }
  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcEventDataV1_HH
