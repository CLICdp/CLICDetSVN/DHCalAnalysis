//
// $Id: DhcEventDataV0.hh,v 1.6 2008/04/01 18:00:32 jls Exp $
//

#ifndef DhcEventDataV0_HH
#define DhcEventDataV0_HH

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cmath>

#include "UtlPrintHex.hh"
#include "UtlPack.hh"

#include "DhcFeHitDataV0.hh"


class DhcEventDataV0 {

public:
  enum {
    versionNumber=0
  };

  DhcEventDataV0();

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);

  const unsigned* data() const;
  unsigned*       data();

  const DhcFeHitDataV0* feData(unsigned f) const;
  DhcFeHitDataV0*       feData(unsigned f);

  std::vector< std::vector<const DhcFeHitDataV0*>*>* events(unsigned t=24) const;

  bool verify(bool p=false) const;

  std::ostream& print(std::ostream &o, std::string s="", bool raw=false) const;


private:
  unsigned _numberOfWords;
};

#ifdef CALICE_DAQ_ICC

DhcEventDataV0::DhcEventDataV0() {
  memset(this,0,sizeof(DhcEventDataV0));
}

unsigned DhcEventDataV0::numberOfWords() const {
  return _numberOfWords;
}

void DhcEventDataV0::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const unsigned* DhcEventDataV0::data() const {
  return (&_numberOfWords)+1;
}

unsigned* DhcEventDataV0::data() {
  return (&_numberOfWords)+1;
}

const DhcFeHitDataV0* DhcEventDataV0::feData(unsigned f) const {

  unsigned n(4*f);
  if (_numberOfWords<n) return 0;

  return (const DhcFeHitDataV0*)(data()+n);
}

DhcFeHitDataV0* DhcEventDataV0::feData(unsigned f) {

  unsigned n(4*f);
  if (_numberOfWords<n) return 0;

  return (DhcFeHitDataV0*)(data()+n);
}

std::vector< std::vector<const DhcFeHitDataV0*>*>*
DhcEventDataV0::events(unsigned t) const {

  std::vector< std::vector<const DhcFeHitDataV0*>*>* ev =
    new std::vector< std::vector<const DhcFeHitDataV0*>*>;

  if (_numberOfWords>0) {

    double tslast = feData(0)->timestamp();
    ev->push_back(new std::vector<const DhcFeHitDataV0*>);
    (ev->back())->push_back(feData(0));

    for (unsigned i(1); i<_numberOfWords/4; i++) {

      if (feData(i)->trg()) {
	double timestamp = feData(i)->timestamp();
	if (fabs(timestamp - tslast) > t)
	  ev->push_back(new std::vector<const DhcFeHitDataV0*>);
	tslast = timestamp;
      }
      (ev->back())->push_back(feData(i));
    }
  }
  return ev;
}

bool DhcEventDataV0::verify(bool p) const {
  if(_numberOfWords==0) return true;

  for(unsigned i(0);i<_numberOfWords/4; i++) {
    const DhcFeHitDataV0* hits(feData(i));
    if (!hits->verify()) return false;
  }
  return true;
}

std::ostream& DhcEventDataV0::print(std::ostream &o, std::string s, bool raw) const {
  o << s << "DhcEventDataV0::print()  Number of words = " 
    << numberOfWords() << std::endl;
  if(_numberOfWords==0) return o;

  if(raw) {
    const unsigned *d(data());
    for(unsigned i(0);i<_numberOfWords;i++) {
      o << s << " Data word " << std::setw(5) << i << " = " << printHex(d[i]) << std::endl;
    }
    return o;
  }

  if (!verify())
    o << s << " ChkSum error " << std::endl;

  for(unsigned i(0);i<_numberOfWords/4; i++) {
    const DhcFeHitDataV0* hits(feData(i));
    hits->print(o, " ");
  }
  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcEventDataV0_HH
