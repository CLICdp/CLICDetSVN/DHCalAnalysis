#ifndef DhcDcRunData_HH
#define DhcDcRunData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class DhcDcRunData {

public:
  enum {
    versionNumber=0
  };

public:
  DhcDcRunData();

  const unsigned char* chipid() const;
  void          chipid(unsigned char n);

  const unsigned char* serial() const;
  void serial(const unsigned char* n);
  void serial(const unsigned b, const unsigned char);

  const unsigned char* pllCount() const;
  void                 pllCount(unsigned char n);

  const unsigned char* syncCount() const;
  void                 syncCount(unsigned char n);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned char _chipid;
  unsigned char _serial[2];
  unsigned char _pllCount;
  unsigned char _syncCount;
  unsigned char _padding[3]; // Make multiple of 4 bytes
};

#ifdef CALICE_DAQ_ICC

DhcDcRunData::DhcDcRunData() {
  _chipid=31;
  memset(_serial,0xff,sizeof(_serial));
  _pllCount=0;
  _syncCount=0;
}

const unsigned char* DhcDcRunData::chipid() const {
  return &_chipid;
}

void DhcDcRunData::chipid(unsigned char n) {
  _chipid=n;
}

const unsigned char* DhcDcRunData::serial() const {
  return _serial;
}

void  DhcDcRunData::serial(const unsigned char* n) {
  memcpy(_serial,n,sizeof(_serial));
}

void DhcDcRunData::serial(const unsigned b, const unsigned char n) {
  _serial[b] = n;
}

const unsigned char* DhcDcRunData::pllCount() const {
  return &_pllCount;
}

void DhcDcRunData::pllCount(unsigned char n) {
  _pllCount=n;
}

const unsigned char* DhcDcRunData::syncCount() const {
  return &_syncCount;
}

void DhcDcRunData::syncCount(unsigned char n) {
  _syncCount=n;
}

std::ostream& DhcDcRunData::print(std::ostream &o, std::string s) const {
  o << s << "DhcDcRunData::print()" << std::endl;

  o << s << " ChipID       = " << printHex(_chipid) << std::endl;

  o << s << " PLL Count = " << printHex(_pllCount) << std::endl;
  o << s << " Sync Count = " << printHex(_syncCount) << std::endl;

  o << s << " Serial    = ";
  for(unsigned i(0); i<sizeof(_serial); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_serial[i]) << std::dec;
  o << s << std::setfill(' ');
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcDcRunData_HH
