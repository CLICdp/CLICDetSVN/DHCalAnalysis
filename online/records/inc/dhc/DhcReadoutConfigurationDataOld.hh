//
// $Id: DhcReadoutConfigurationDataOld.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef DhcReadoutConfigurationDataOld_HH
#define DhcReadoutConfigurationDataOld_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"
#include "UtlTime.hh"


class DhcReadoutConfigurationDataOld {

public:
  enum {
    versionNumber=0
  };

  DhcReadoutConfigurationDataOld();

  unsigned char crateNumber() const;
  void          crateNumber(unsigned char c);

  bool slotFeEnable(unsigned s, unsigned f) const;
  void slotFeEnable(unsigned s, unsigned f, bool b);

  unsigned short slotFeEnables(unsigned s) const;
  void           slotFeEnables(unsigned s, unsigned short b);

  unsigned char slotFeRevision(unsigned s) const;
  void          slotFeRevision(unsigned s, unsigned char v);

  bool slotEnable(unsigned s) const;
  void slotEnable(unsigned s, bool b);

  unsigned bltSize() const;
  void     bltSize(unsigned n);

  UtlTimeDifference pollInterval() const;
  void     pollInterval(UtlTimeDifference t);

  UtlTimeDifference beWaitInterval() const;
  void     beWaitInterval(UtlTimeDifference t);

  bool triggerByWaiting() const;
  void triggerByWaiting(bool b);

  bool triggerInternally() const;
  void triggerInternally(bool b);

  bool triggerExternally() const;
  void triggerExternally(bool b);

  UtlPack mode() const;
  void mode(UtlPack m);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  UtlPack _numbers;
  UtlPack _slotEnable;
  UtlPack _slotFeEnable[10];
  UtlPack _slotFeRevision[5];
  unsigned _bltSize;
  UtlTimeDifference _pollInterval;
  UtlTimeDifference _beWaitInterval;
  UtlPack _mode;
};

#ifdef CALICE_DAQ_ICC

DhcReadoutConfigurationDataOld::DhcReadoutConfigurationDataOld() {
  memset(this,0,sizeof(DhcReadoutConfigurationDataOld));

  for(unsigned i(2);i<=21;i++) {
    slotEnable(i,false);
    for(unsigned f(0);f<12;f++) slotFeEnable(i,f,false);
    slotFeRevision(i,3);
  }

  pollInterval(0);
  beWaitInterval(UtlTimeDifference(0,400));
  triggerByWaiting(true);
}

unsigned char DhcReadoutConfigurationDataOld::crateNumber() const {
  return _numbers.byte(0);
}

void DhcReadoutConfigurationDataOld::crateNumber(unsigned char c) {
  _numbers.byte(0,c);
}

bool DhcReadoutConfigurationDataOld::slotFeEnable(unsigned s, unsigned f) const {
  assert(s>=2 && s<=21 && f<12);
  return _slotFeEnable[(s-2)/2].bit(16*((s-2)%2)+f);
}

void DhcReadoutConfigurationDataOld::slotFeEnable(unsigned s, unsigned f, bool b) {
  assert(s>=2 && s<=21 && f<12);
  _slotFeEnable[(s-2)/2].bit(16*((s-2)%2)+f,b);
}

unsigned short DhcReadoutConfigurationDataOld::slotFeEnables(unsigned s) const {
  assert(s>=2 && s<=21);
  return _slotFeEnable[(s-2)/2].halfWord((s-2)%2);
}

void DhcReadoutConfigurationDataOld::slotFeEnables(unsigned s, unsigned short b) {
  assert(s>=2 && s<=21);
  _slotFeEnable[(s-2)/2].halfWord((s-2)%2,b);
}

unsigned char DhcReadoutConfigurationDataOld::slotFeRevision(unsigned s) const {
  assert(s>=2 && s<=21);
  return _slotFeRevision[(s-2)/4].byte((s-2)%4);
}

void DhcReadoutConfigurationDataOld::slotFeRevision(unsigned s, unsigned char v) {
  assert(s>=2 && s<=21);
  _slotFeRevision[(s-2)/4].byte((s-2)%4,v);
}

bool DhcReadoutConfigurationDataOld::slotEnable(unsigned s) const { 
  assert(s>=2 && s<=21);
  return _slotEnable.bit(s);
}

void DhcReadoutConfigurationDataOld::slotEnable(unsigned s, bool b) { 
  assert(s>=2 && s<=21);
  if(!b) for(unsigned f(0);f<12;f++) slotFeEnable(s,f,false);
  _slotEnable.bit(s,b);
}

unsigned DhcReadoutConfigurationDataOld::bltSize() const {
  return _bltSize;
}

void DhcReadoutConfigurationDataOld::bltSize(unsigned n) {
  _bltSize=n;
}

UtlTimeDifference DhcReadoutConfigurationDataOld::pollInterval() const {
  return _pollInterval;
}

void DhcReadoutConfigurationDataOld::pollInterval(UtlTimeDifference t) {
  _pollInterval=t;
}

UtlTimeDifference DhcReadoutConfigurationDataOld::beWaitInterval() const {
  return _beWaitInterval;
}

void DhcReadoutConfigurationDataOld::beWaitInterval(UtlTimeDifference t) {
  _beWaitInterval=t;
}

UtlPack DhcReadoutConfigurationDataOld::mode() const {
  return _mode;
}

void DhcReadoutConfigurationDataOld::mode(UtlPack m) {
  _mode.word(m.word());
}

bool DhcReadoutConfigurationDataOld::triggerByWaiting() const {
  return _mode.bit(0);
}

void DhcReadoutConfigurationDataOld::triggerByWaiting(bool b) {
  _mode.byte(0,0);
  _mode.bit(0,b);
}

bool DhcReadoutConfigurationDataOld::triggerInternally() const {
  return _mode.bit(1);
}

void DhcReadoutConfigurationDataOld::triggerInternally(bool b) {
  _mode.byte(0,0);
  _mode.bit(1,b);
}

bool DhcReadoutConfigurationDataOld::triggerExternally() const {
  return _mode.bit(2);
}

void DhcReadoutConfigurationDataOld::triggerExternally(bool b) {
  _mode.byte(0,0);
  _mode.bit(2,b);
}

std::ostream& DhcReadoutConfigurationDataOld::print(std::ostream &o, std::string s) const {
  o << s << "DhcReadoutConfigurationDataOld::print()" << std::endl;

  o << s << " Crate number  = " << printHex(_numbers.byte(0)) << std::endl;
  for(unsigned i(2);i<=21;i++) {
    if(slotEnable(i)) {
      o << s << "  Slot " << std::setw(2) << i << "," << std::endl;
      o << s << "     FE enables = "
	<< printHex(_slotFeEnable[(i-2)/2].halfWord((i-2)%2)) << std::endl;
      o << s << "     FE revision = "
	<< printHex(_slotFeRevision[(i-2)/4].byte((i-2)%4)) << std::endl;
    }
  }
  o << s << " BLT Size           = " << _bltSize << std::endl;
  o << s << " Poll interval      = " << _pollInterval << std::endl;
  o << s << " Be Wait interval   = " << _beWaitInterval << std::endl;
  if(triggerByWaiting()) o << s << "  Triggered By Waiting" << std::endl;
  if(triggerInternally()) o << s << "  Triggered Internally" << std::endl;
  if(triggerExternally()) o << s << "  Triggered Externally" << std::endl;
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcReadoutConfigurationDataOld_HH
