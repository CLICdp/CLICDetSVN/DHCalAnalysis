//
// $Id: DhcLocation.hh,v 1.5 2007/12/20 22:14:55 jls Exp $
//

#ifndef DhcLocation_HH
#define DhcLocation_HH

#include <string>
#include <iostream>

#include "UtlLocation.hh"
#include "UtlPrintHex.hh"

class DhcLocation : public UtlLocation {

public:
  enum DhcComponent {
    dc0,dc1,dc2,dc3,dc4,dc5,dc6,dc7,dc8,dc9,dc10,dc11,be,feBroadcast,
    endOfDhcComponentEnum
  };
    
  DhcLocation();
  DhcLocation(unsigned char c, unsigned char s, unsigned char f, unsigned char l=0);
  DhcLocation(unsigned char c, unsigned char s, DhcComponent f, unsigned char l=0);
  
  DhcComponent       dhcComponent() const;
  void               dhcComponent(DhcComponent f);
  std::string        dhcComponentName() const;
  static std::string dhcComponentName(DhcComponent c);

  bool verify() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  static const std::string _dhcComponentName[endOfDhcComponentEnum];
};

#ifdef CALICE_DAQ_ICC

DhcLocation::DhcLocation() : UtlLocation() {
}

DhcLocation::DhcLocation(unsigned char c, unsigned char s, unsigned char f, unsigned char l) : 
  UtlLocation(c,s,f,l) {
}
  
DhcLocation::DhcLocation(unsigned char c, unsigned char s, DhcComponent f, unsigned char l) :
  UtlLocation(c,s,f,l) {
}

DhcLocation::DhcComponent DhcLocation::dhcComponent() const {
  return (DhcComponent)componentNumber();
}
  
void DhcLocation::dhcComponent(DhcComponent f) {
  componentNumber((unsigned char)f);
}
  
std::string DhcLocation::dhcComponentName() const {
  return dhcComponentName(dhcComponent());
}

std::string DhcLocation::dhcComponentName(DhcComponent c) {
  if(c<endOfDhcComponentEnum) return _dhcComponentName[c];
  return "Unknown";
}

bool DhcLocation::verify() const {
  return slotNumber()<22 &&
    dhcComponent()<endOfDhcComponentEnum;
}

std::ostream& DhcLocation::print(std::ostream &o, std::string s) const {
  o << s << "DhcLocation::print()" << std::endl;

  o << s << " Crate number     = "
    << (unsigned)crateNumber();
  o << std::endl;

  o << s << " Slot number      = "
    << (unsigned)slotNumber();
  if(slotBroadcast()) o << " = Slot broadcast";
  else if(slotNumber()>21) o << " = Unphysical";
  o << std::endl;

  o << s << " DHC Component    = " << std::setw(4) << (unsigned)dhcComponent() 
    << " = " << dhcComponentName() << std::endl;

  return o;
}

const std::string DhcLocation::_dhcComponentName[]={
  "dc0","dc1","dc2","dc3","dc4","dc5","dc6","dc7","dc8","dc9","dc10","dc11","be"," febroadcast"};

#endif // CALICE_DAQ_ICC
#endif // DhcLocation_HH 
