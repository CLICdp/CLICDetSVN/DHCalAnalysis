#ifndef DhcTriggerConfigurationData_HH
#define DhcTriggerConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"
#include "UtlTime.hh"


class DhcTriggerConfigurationData {

public:
  enum {
    versionNumber=0
  };

  DhcTriggerConfigurationData();

  unsigned char crateNumber() const;
  void          crateNumber(unsigned char c);

  UtlTimeDifference pollInterval() const;
  void     pollInterval(UtlTimeDifference t);

  bool triggerByWaiting() const;
  void triggerByWaiting(bool b);

  bool triggerInternally() const;
  void triggerInternally(bool b);

  bool triggerExternally() const;
  void triggerExternally(bool b);

  UtlPack mode() const;
  void mode(UtlPack m);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  UtlPack _numbers;
  UtlTimeDifference _pollInterval;
  UtlPack _mode;
};

#ifdef CALICE_DAQ_ICC

DhcTriggerConfigurationData::DhcTriggerConfigurationData() {
  memset(this,0,sizeof(DhcTriggerConfigurationData));

  pollInterval(0);
  triggerByWaiting(true);
}

unsigned char DhcTriggerConfigurationData::crateNumber() const {
  return _numbers.byte(0);
}

void DhcTriggerConfigurationData::crateNumber(unsigned char c) {
  _numbers.byte(0,c);
}

UtlTimeDifference DhcTriggerConfigurationData::pollInterval() const {
  return _pollInterval;
}

void DhcTriggerConfigurationData::pollInterval(UtlTimeDifference t) {
  _pollInterval=t;
}

UtlPack DhcTriggerConfigurationData::mode() const {
  return _mode;
}

void DhcTriggerConfigurationData::mode(UtlPack m) {
  _mode.word(m.word());
}

bool DhcTriggerConfigurationData::triggerByWaiting() const {
  return _mode.bit(0);
}

void DhcTriggerConfigurationData::triggerByWaiting(bool b) {
  _mode.byte(0,0);
  _mode.bit(0,b);
}

bool DhcTriggerConfigurationData::triggerInternally() const {
  return _mode.bit(1);
}

void DhcTriggerConfigurationData::triggerInternally(bool b) {
  _mode.byte(0,0);
  _mode.bit(1,b);
}

bool DhcTriggerConfigurationData::triggerExternally() const {
  return _mode.bit(2);
}

void DhcTriggerConfigurationData::triggerExternally(bool b) {
  _mode.byte(0,0);
  _mode.bit(2,b);
}

std::ostream& DhcTriggerConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "DhcTriggerConfigurationData::print()" << std::endl;

  o << s << " Crate number  = " << printHex(_numbers.byte(0)) << std::endl;
  o << s << " Poll interval      = " << _pollInterval << std::endl;
  if(triggerByWaiting()) o << s << "  Triggered By Waiting" << std::endl;
  if(triggerInternally()) o << s << "  Triggered Internally" << std::endl;
  if(triggerExternally()) o << s << "  Triggered Externally" << std::endl;
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcTriggerConfigurationData_HH
