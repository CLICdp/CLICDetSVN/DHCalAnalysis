//
// $Id: DhcFeConfigurationDataV1.hh,v 1.6 2008/02/18 15:03:31 jls Exp $
//

#ifndef DhcFeConfigurationDataV1_HH
#define DhcFeConfigurationDataV1_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"
#include "UtlMapDefinitions.hh"

class DhcFeConfigurationDataV1 {

public:
  enum {
    versionNumber=1
  };

  enum ShapeTime {
    t65=0,
    t85=1,
    t100=2,
    t125=3,
    endOfDcrShapingTineEnum
  };

public:
  DhcFeConfigurationDataV1();

  const unsigned char* chipid() const;
  void          chipid(unsigned char n);

  const unsigned char* plsr() const;
  void          plsr(unsigned char n);

  const unsigned char* intd() const;
  void          intd(unsigned char n);

  const unsigned char* shp2() const;
  void          shp2(unsigned char n);

  const unsigned char* shp1() const;
  void          shp1(unsigned char n);

  const unsigned char* blrd() const;
  void          blrd(unsigned char n);

  const unsigned char* vtnd() const;
  void          vtnd(unsigned char n);

  const unsigned char* vtpd() const;
  void          vtpd(unsigned char n);

  const unsigned char* dcr() const;
  void          dcr(unsigned char n);

  const unsigned char dcrCapSelect() const;
  void                dcrCapSelect(unsigned char n);

  const bool dcrLowGain() const;
  void       dcrLowGain(bool b);

  const bool dcrDisableBaselineRestore() const;
  void       dcrDisableBaselineRestore(bool b);
  
  const bool dcrAccept() const;
  void       dcrAccept(bool b);

  const bool dcrActivatePipeline() const;
  void       dcrActivatePipeline(bool b);

  const bool dcrExternalTrigger() const;
  void       dcrExternalTrigger(bool b);

  const unsigned char* inj() const;
  void inj(const unsigned char* n);
  void inj(const unsigned long long n);
  void inj(const unsigned b, const unsigned char);

  const unsigned char* kill() const;
  void kill(const unsigned char* n);
  void kill(const unsigned long long n);
  void kill(const unsigned b, const unsigned char);

  //CRP functions to support storage of configuration data in calice data base
  std::vector<unsigned int>  getinj() const;
  std::vector<unsigned int>  getkill() const;
  std::vector<unsigned int>  getpadding() const;
  // Access functions to retrieve the maps containing the different values
  DaqTypesDblMap_t getDblMap() const;
  DaqTypesUIntMap_t getUIntMap() const;
  DaqTypesFloatMap_t getFloatMap() const;
  //Return a map of time stamps
  std::map<std::string,time_t> getTimeStamps() const;
  //end CRP

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  static const std::string _dcrShapingTime[endOfDcrShapingTineEnum];

private:
  unsigned char _chipid;
  unsigned char _plsr;
  unsigned char _intd;
  unsigned char _shp2;
  unsigned char _shp1;
  unsigned char _blrd;
  unsigned char _vtnd;
  unsigned char _vtpd;
  unsigned char _dcr;
  unsigned char _inj[8];
  unsigned char _kill[8];
  unsigned char _padding[3]; // Make a multiple of 4 bytes
};

#ifdef CALICE_DAQ_ICC

#include "time.h"

DhcFeConfigurationDataV1::DhcFeConfigurationDataV1() {
  _chipid=26;
  _plsr=0;
  _intd=139;
  _shp2=121;
  _shp1=116;
  _blrd=81;
  _vtnd=0;
  _vtpd=255;
  _dcr=0;
  memset(_inj,0,sizeof(_inj));
  memset(_kill,0,sizeof(_kill));
}

const unsigned char* DhcFeConfigurationDataV1::chipid() const {
  return &_chipid;
}

void DhcFeConfigurationDataV1::chipid(unsigned char n) {
  _chipid=n;
}

const unsigned char* DhcFeConfigurationDataV1::plsr() const {
  return &_plsr;
}

void DhcFeConfigurationDataV1::plsr(unsigned char n) {
  _plsr=n;
}

const unsigned char* DhcFeConfigurationDataV1::intd() const {
  return &_intd;
}

void DhcFeConfigurationDataV1::intd(unsigned char n) {
  _intd=n;
}

const unsigned char* DhcFeConfigurationDataV1::shp2() const {
  return &_shp2;
}

void DhcFeConfigurationDataV1::shp2(unsigned char n) {
  _shp2=n;
}

const unsigned char* DhcFeConfigurationDataV1::shp1() const {
  return &_shp1;
}

void DhcFeConfigurationDataV1::shp1(unsigned char n) {
  _shp1=n;
}

const unsigned char* DhcFeConfigurationDataV1::blrd() const {
  return &_blrd;
}

void DhcFeConfigurationDataV1::blrd(unsigned char n) {
  _blrd=n;
}

const unsigned char* DhcFeConfigurationDataV1::vtnd() const {
  return &_vtnd;
}

void DhcFeConfigurationDataV1::vtnd(unsigned char n) {
  _vtnd=n;
}

const unsigned char* DhcFeConfigurationDataV1::vtpd() const {
  return &_vtpd;
}

void DhcFeConfigurationDataV1::vtpd(unsigned char n) {
  _vtpd=n;
}

const unsigned char* DhcFeConfigurationDataV1::dcr() const {
  return &_dcr;
}

void DhcFeConfigurationDataV1::dcr(unsigned char n) {
  _dcr=n;
}

const unsigned char DhcFeConfigurationDataV1::dcrCapSelect() const {
  return (_dcr & 0xc0) >> 6;
}

void DhcFeConfigurationDataV1::dcrCapSelect(unsigned char n) {
  assert(n<4);
  _dcr &= 0x3f;
  _dcr |= (n<<6);
}

const bool DhcFeConfigurationDataV1::dcrLowGain() const {
  return _dcr & 0x20;
}

void DhcFeConfigurationDataV1::dcrLowGain(bool b) {
  ( b ? _dcr |= 0x20 : _dcr &= ~0x20 );
}

const bool DhcFeConfigurationDataV1::dcrDisableBaselineRestore() const {
  return _dcr & 0x10;
}

void DhcFeConfigurationDataV1::dcrDisableBaselineRestore(bool b) {
  ( b ? _dcr |= 0x10 : _dcr &= ~0x10 );
}
  
const bool DhcFeConfigurationDataV1::dcrAccept() const {
  return _dcr & 0x08;
}

void DhcFeConfigurationDataV1::dcrAccept(bool b) {
  ( b ? _dcr |= 0x08 : _dcr &= ~0x08 );
}

const bool DhcFeConfigurationDataV1::dcrActivatePipeline() const {
  return _dcr & 0x04;
}

void DhcFeConfigurationDataV1::dcrActivatePipeline(bool b) {
  ( b ? _dcr |= 0x04 : _dcr &= ~0x04 );
}

const bool DhcFeConfigurationDataV1::dcrExternalTrigger() const {
  return _dcr & 0x02;
}

void DhcFeConfigurationDataV1::dcrExternalTrigger(bool b) {
  ( b ? _dcr |= 0x02 : _dcr &= ~0x02 );
}

const unsigned char* DhcFeConfigurationDataV1::inj() const {
  return _inj;
}

void  DhcFeConfigurationDataV1::inj(const unsigned char* n) {
  memcpy(_inj,n,sizeof(_inj));
}

void  DhcFeConfigurationDataV1::inj(const unsigned long long n) {
  memcpy(_inj,&n,sizeof(_inj));
}

void DhcFeConfigurationDataV1::inj(const unsigned b, const unsigned char n) {
  _inj[b] = n;
}

const unsigned char* DhcFeConfigurationDataV1::kill() const {
  return _kill;
}

void  DhcFeConfigurationDataV1::kill(const unsigned char* n) {
  memcpy(_kill,n,sizeof(_kill));
}

void  DhcFeConfigurationDataV1::kill(const unsigned long long n) {
  memcpy(_kill,&n,sizeof(_kill));
}

void DhcFeConfigurationDataV1::kill(const unsigned b, const unsigned char n) {
  _kill[b] = n;
}


//CRP Access functions to create maps which are stored in the db by userlib classes via the converter 
std::vector<unsigned int>  DhcFeConfigurationDataV1::getinj() const {
  std::vector<unsigned int> v;
  v.clear();
  const unsigned char* injPtr(inj());
  for(unsigned int i=0;i<sizeof(_inj);i++) v.push_back(static_cast<unsigned int>(injPtr[i]));
  return v;
}


std::vector<unsigned int>  DhcFeConfigurationDataV1::getkill() const {
  std::vector<unsigned int> v;
  v.clear();
  const unsigned char* killPtr(kill());
  for(unsigned int i=0;i<sizeof(_kill);i++) v.push_back(static_cast<unsigned int>(killPtr[i]));
  return v;
}

std::vector<unsigned int>  DhcFeConfigurationDataV1::getpadding() const {
  std::vector<unsigned int> v;
  v.clear();
  for(unsigned int i=0;i<sizeof(_padding);i++) v.push_back(static_cast<unsigned int>(_padding[i]));
  return v;
}



DaqTypesUIntMap_t DhcFeConfigurationDataV1::getUIntMap() const {
  //Initialize the map between types and access functions in the DAQ class - unsigned ints
  DaqTypesUIntMap_t _theUIntTypes;
  _theUIntTypes.clear();
  std::pair<std::map<std::string,std::vector<unsigned int> >::iterator,bool> ret;
  //if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("CHIPID",   )).second)
  //  std::cout << "Problems with inserting CHIPID into map" << std::endl;  
  //ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("CHIPID", std::vector<unsigned int> ()  )).first->second.push_back(static_cast<unsigned int>(_chipid));
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("CHIPID", std::vector<unsigned int> ()  ));
  if(!ret.second) std::cout << "Problems with inserting CHIPID into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_chipid)); 
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("PLSR", std::vector<unsigned int> () ));
  if(!ret.second) std::cout << "Problems with inserting PLSR into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_plsr));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("INTD", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting INTD into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_intd));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("SHP2", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting SHP2 into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_shp2));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("SHP1", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting SHP1 into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_shp1));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("BLRD", std::vector<unsigned int> () ));
  if(!ret.second) std::cout << "Problems with inserting BLRD into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_blrd));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("VTND", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting VTND into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_vtnd));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("VTPD", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting VTPD into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_vtpd)); 
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCR", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting DCR into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(_dcr));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRCAPCELECT", std::vector<unsigned int> ()  )); 
  if(!ret.second) std::cout << "Problems with inserting DCRCAPCELECT into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(dcrCapSelect()));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRSHAPINGTIME", std::vector<unsigned int> ()  )); 
  if(!ret.second) std::cout << "Problems with inserting DCRSHAPINGTIME into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(*(_dcrShapingTime[dcrCapSelect()].c_str())));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRLOWGAIN", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting DCRLOWGAIN into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(dcrLowGain()));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRDISABLEBASELINERESTORE", std::vector<unsigned int> ()  )); 
  if(!ret.second) std::cout << "Problems with inserting DCRDISABLEBASELINERESTORE into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(dcrDisableBaselineRestore()));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRACCEPT", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting DCRACCEPT into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(dcrAccept()));
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCRACTIVATEPIPELINE", std::vector<unsigned int> () )); 
  if(!ret.second) std::cout << "Problems with inserting DCRACTIVATEPIPELINE into map" << std::endl;  
  else ret.first->second.push_back(static_cast<unsigned int>(dcrActivatePipeline())); 
  //
  ret=_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("DCREXTERNALTRIGGER", std::vector<unsigned int> ()  )); 
  if(!ret.second) std::cout << "Problems with inserting DCREXTERNALTRIGGER into map" << std::endl;  
  else { 
   std::string::const_iterator it;
   for ( it=_dcrShapingTime[dcrCapSelect()].begin(); it < _dcrShapingTime[dcrCapSelect()].end(); it++ ) ret.first->second.push_back(static_cast<unsigned int>(*it));
  }
  //
  if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("INJ", getinj())).second) std::cout << "Problems with inserting INJ into map" << std::endl;  
  if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("KILL", getkill())).second) std::cout << "Problems with inserting KILL into map" << std::endl;  
  if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned int> >("PADDING", getpadding())).second) std::cout << "Problems with inserting PADDING into map" << std::endl;  
  return _theUIntTypes;
} 


DaqTypesDblMap_t DhcFeConfigurationDataV1::getDblMap() const {
  //Initialize the map between types and access functions in the DAQ class - doubles
  DaqTypesDblMap_t _theDblTypes;
  _theDblTypes.clear();
  return _theDblTypes;
} 

DaqTypesFloatMap_t DhcFeConfigurationDataV1::getFloatMap() const {
  //Initialize the map between types and access functions in the DAQ class - floats
  DaqTypesFloatMap_t _theFloatTypes;
  _theFloatTypes.clear();
  return _theFloatTypes;
} 

 //Implemented here for convenience and to be in synch with similar classes 
  std::map<std::string,time_t> DhcFeConfigurationDataV1::getTimeStamps() const {
    std::map<std::string,time_t> _theTimeStamps;
    _theTimeStamps.clear();
    return _theTimeStamps;
   }


//End CRP


std::ostream& DhcFeConfigurationDataV1::print(std::ostream &o, std::string s) const {
  o << s << "DhcFeConfigurationDataV1::print()" << std::endl;

  o << s << " ChipID  = " << printHex(_chipid) << std::endl;
  o << s << " PLSR    = " << printHex(_plsr) << std::endl;
  o << s << " IntD    = " << printHex(_intd) << std::endl;
  o << s << " Shp2D   = " << printHex(_shp2) << std::endl;
  o << s << " Shp1D   = " << printHex(_shp1) << std::endl;
  o << s << " BlrD    = " << printHex(_blrd) << std::endl;
  o << s << " VtnD    = " << printHex(_vtnd) << std::endl;
  o << s << " VtpD    = " << printHex(_vtpd) << std::endl;
  o << s << " DCR     = " << printHex(_dcr) << std::endl;

  o << s << "   Shaping Time is " << _dcrShapingTime[dcrCapSelect()];
  o << s << std::endl;
  o << s << "   Gain is " << (dcrLowGain() ? "Low,RPC" : "High,GEM");
  o << s << std::endl;
  o << s << "   Baseline Restore is " << (dcrDisableBaselineRestore() ? "Disabled" : "Enabled");
  o << s << std::endl;
  o << s << "   Chip wide mask is " << (dcrAccept() ? "Accept" : "Mask");
  o << s << std::endl;
  o << s << "   Pipeline is " << (dcrActivatePipeline() ? "Enabled" : "Disabled");
  o << s << std::endl;
  o << s << "   Trigger is " << (dcrExternalTrigger() ? "External" : "Internal");
  o << s << std::endl;

  o << s << " Inj     = ";
  for(unsigned i(0); i<sizeof(_inj); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_inj[i]) << std::dec;
  o << s << std::endl;

  o << s << " Kill    = ";
  for(unsigned i(0); i<sizeof(_kill); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_kill[i]) << std::dec;
  o << s << std::setfill(' ');
  o << s << std::endl;

  //Added by RP
  o << s << " Padding    = ";
  for(unsigned i(0); i<sizeof(_padding); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_padding[i]) << std::dec;
  o << s << std::setfill(' ');
  //o << s << std::endl;

  return o;
}

const std::string DhcFeConfigurationDataV1::_dcrShapingTime[]={
  "65ns","85ns","100ns","125ns"
};

#endif // CALICE_DAQ_ICC
#endif // DhcFeConfigurationDataV1_HH
