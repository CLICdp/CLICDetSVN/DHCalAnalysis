//
// $Id: DhcFeHitDataV1.hh,v 1.2 2008/01/31 13:46:30 jls Exp $
//

#ifndef DhcFeHitDataV1_HH
#define DhcFeHitDataV1_HH

#include <string>
#include <iostream>
#include <utility>
#include <vector>

#include "UtlPack.hh"

class DhcFeHitDataV1 {

public:
  static double ccell;
  static double cell;
  static double cmin;
  static double cmax;

  DhcFeHitDataV1();

  void     vmead(unsigned char n);
  unsigned vmead() const;
  unsigned dcalad() const;
  unsigned dcolad() const;
  unsigned dconad() const;
  unsigned timestamp() const;


  unsigned hitsHi() const;
  unsigned hitsLo() const;
  unsigned trg() const;
  unsigned dbt() const;
  unsigned err() const;
  unsigned char chksum() const;
  unsigned char versum() const;

  bool verify() const;

  static std::pair<double, double>* hitCoord(const unsigned& dcalad,
					     const unsigned& chan);
  std::vector< std::pair<double, double>*>* hitCoords() const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _data[4];
};

#ifdef CALICE_DAQ_ICC

DhcFeHitDataV1::DhcFeHitDataV1() {
}

void DhcFeHitDataV1::vmead(unsigned char n) {
  _data[0].bits(29,30,n);
}

unsigned DhcFeHitDataV1::vmead() const {
  return _data[0].bits(29,30);
}

unsigned DhcFeHitDataV1::dcalad() const {
  return _data[0].bits(24,28);
}

unsigned DhcFeHitDataV1::dcolad() const {
  return _data[0].bits(20,23);
}

unsigned DhcFeHitDataV1::dconad() const {
  return _data[0].bits(16,19);
}

unsigned DhcFeHitDataV1::timestamp() const {
  return  _data[0].bits(0,15)<<8 | _data[1].byte(3);
}

unsigned DhcFeHitDataV1::hitsHi() const {
  return _data[1].bits(0,23)<<8 | _data[2].byte(3);
}

unsigned DhcFeHitDataV1::hitsLo() const {
  return _data[2].bits(0,23)<<8 | _data[3].byte(3);
}

unsigned DhcFeHitDataV1::dbt() const {
  return _data[3].bit(23);
}

unsigned DhcFeHitDataV1::trg() const {
  return _data[3].bit(15);
}

unsigned DhcFeHitDataV1::err() const {
  return _data[3].bits(8,11);
}

unsigned char DhcFeHitDataV1::chksum() const {
  return _data[3].byte(0);
}

unsigned char DhcFeHitDataV1::versum() const {
  // compute chksum
  unsigned char sum(0);

  for (int i(0); i<15; i++)
    sum += static_cast<unsigned char>(_data[i/4].byte(3-(i%4)));

  return sum;
}

bool DhcFeHitDataV1::verify() const {

  return (versum() == chksum());
}

double DhcFeHitDataV1::ccell = 8.0;
double DhcFeHitDataV1::cell = DhcFeHitDataV1::ccell/8;;
double DhcFeHitDataV1::cmin = DhcFeHitDataV1::cell/2;
double DhcFeHitDataV1::cmax = DhcFeHitDataV1::ccell/2 - DhcFeHitDataV1::cell/2;

std::pair<double, double>*
DhcFeHitDataV1::hitCoord(const unsigned& dcalad,
		       const unsigned& chan) {

  std::pair<double, double>* p = new std::pair<double, double>;

  switch (chan/16) {
  case 0:
    p->first = cmax - (chan%4)*cell;
    p->second = cmin + (chan/4)*cell;
    break;

  case 1:
    p->first = -cmin - (chan%4)*cell;
    p->second = cmax - ((chan-16)/4)*cell;
    break;

  case 2:
    p->first = -cmax + (chan%4)*cell;
    p->second = -cmin - ((chan-32)/4)*cell;
    break;

  case 3:
    p->first = cmin + (chan%4)*cell;
    p->second = -cmax + ((chan-48)/4)*cell;
    break;

  default:
    break;
  }

  p->first += (dcalad%6)*ccell+ccell/2;
  p->second += (dcalad/6)*ccell+ccell/2;

  return p;
}

std::vector< std::pair<double, double>*>*
DhcFeHitDataV1::hitCoords() const {

  std::vector< std::pair<double, double>*>* pv =
    new std::vector< std::pair<double, double>*>;

  for (unsigned i(0); i<64; i++) {
    UtlPack hits = (i<32) ?
      UtlPack(hitsLo()) : UtlPack(hitsHi());

    if (hits.bit(i%32)) {
      std::pair<double, double>* p(hitCoord(dcalad(), i));
      pv->push_back(p);
    }
  }
  return pv;
}

std::ostream& DhcFeHitDataV1::print(std::ostream &o, std::string s) const {

  o << s << std::hex;
  o << s << "vmead:" << vmead();
  o << s << "dcalad:" << std::setfill('0') << std::setw(2) << dcalad();
  o << s << "dcolad:" << std::setfill('0') << std::setw(2) << dcolad();
  o << s << "dconad:" << std::setfill('0') << std::setw(2) << dconad();
  o << s << "trg:" << trg();
  o << s << "err:" << err();
  o << s << "hits:";
  o << s << std::setfill('0') << std::setw(8) << hitsHi();
  o << s << std::setfill('0') << std::setw(8) << hitsLo();
  o << s << "ts:" << std::dec << timestamp();
  o << s << "chksum:" << std::setfill('0') << std::setw(2)
    << std::hex <<static_cast<unsigned>(chksum());
  if (!verify())
    o << s << " calc: " << static_cast<unsigned>(versum());
  o << s << std::dec << std::setfill(' ');
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcFeHitDataV1_HH
