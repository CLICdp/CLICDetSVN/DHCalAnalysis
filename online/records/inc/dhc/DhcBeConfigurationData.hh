//
// $Id: DhcBeConfigurationData.hh,v 1.5 2008/01/24 22:59:17 jls Exp $
//

#ifndef DhcBeConfigurationData_HH
#define DhcBeConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class DhcBeConfigurationData {

public:
  enum {
    versionNumber=0
  };

public:
  DhcBeConfigurationData();

  bool runModeEnable() const;
  void runModeEnable(bool b);

  bool circularModeEnable() const;
  void circularModeEnable(bool b);

  bool standaloneModeEnable() const;
  void standaloneModeEnable(bool b);

  bool timestampSortDisable() const;
  void timestampSortDisable(bool b);

  unsigned dconEnable() const;
  void     dconEnable(unsigned n);

  unsigned setOptions() const;
  unsigned resetOptions() const;

  void csr(unsigned n);
  bool locked() const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  UtlPack _csr;
  UtlPack _dcon_enable;
};

#ifdef CALICE_DAQ_ICC

DhcBeConfigurationData::DhcBeConfigurationData() {
  _csr=0;
  _dcon_enable=0;
}

bool DhcBeConfigurationData::runModeEnable() const {
  return _csr.bit(0);
}

void DhcBeConfigurationData::runModeEnable(bool b) {
  _csr.bit(0,b);
}

bool DhcBeConfigurationData::circularModeEnable() const {
  return _csr.bit(1);
}

void DhcBeConfigurationData::circularModeEnable(bool b) {
  _csr.bit(1,b);
}

bool DhcBeConfigurationData::standaloneModeEnable() const {
  return _csr.bit(2);
}

void DhcBeConfigurationData::standaloneModeEnable(bool b) {
  _csr.bit(2,b);
}

bool DhcBeConfigurationData::timestampSortDisable() const {
  return _csr.bit(3);
}

void DhcBeConfigurationData::timestampSortDisable(bool b) {
  _csr.bit(3,b);
}

unsigned DhcBeConfigurationData::dconEnable() const {
  return _dcon_enable.word();
}

void DhcBeConfigurationData::dconEnable(unsigned n) {
  _dcon_enable.word(n);
}

unsigned DhcBeConfigurationData::setOptions() const {
  return _csr.halfWord(0);
}

unsigned DhcBeConfigurationData::resetOptions() const {
  return _csr.halfWord(0)<<16;
}

void DhcBeConfigurationData::csr(unsigned n) {
  _csr=n;
}

bool DhcBeConfigurationData::locked() const {
  return ((_dcon_enable.halfWord(0)&0xfff) == (_dcon_enable.halfWord(1)&0xfff));
}

std::ostream& DhcBeConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "DhcBeConfigurationData::print()" << std::endl;

  o << s << " CSR            = " << printHex(_csr,0) << std::endl;
  o << s << " DCON_Enable    = " << printHex(_dcon_enable,0) << std::endl;
  for (unsigned i(0); i<12; i++)
    if (_dcon_enable.bit(i) != _dcon_enable.bit(i+16))
      o << s << " DCON input " << i << " not locked" << std::endl;
  return o;
}

#endif //  CALICE_DAQ_ICC
#endif // DhcBeConfigurationData_HH
