#ifndef DhcTriggerData_HH
#define DhcTriggerData_HH

#include <string>
#include <iostream>

#include "UtlPrintHex.hh"


class DhcTriggerData {

public:
  enum {
    versionNumber=0
  };

  DhcTriggerData();

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);

  const unsigned* data() const;
  unsigned*       data();

  std::ostream& print(std::ostream &o, std::string s="", bool raw=false) const;

private:
  unsigned _numberOfWords;
};

#ifdef CALICE_DAQ_ICC

DhcTriggerData::DhcTriggerData() {
  memset(this,0,sizeof(DhcTriggerData));
}

unsigned DhcTriggerData::numberOfWords() const {
  return _numberOfWords;
}

void DhcTriggerData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const unsigned* DhcTriggerData::data() const {
  return (&_numberOfWords)+1;
}

unsigned* DhcTriggerData::data() {
  return (&_numberOfWords)+1;
}

std::ostream& DhcTriggerData::print(std::ostream &o, std::string s, bool raw) const {
  o << s << "DhcTriggerData::print()  Number of words = " 
    << numberOfWords() << std::endl;
  if(_numberOfWords==0) return o;

  const unsigned *d(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    o << s << " Data word " << std::setw(5) << i << " = " << printHex(d[i], raw) << std::endl;
  }
  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcTriggerData_HH
