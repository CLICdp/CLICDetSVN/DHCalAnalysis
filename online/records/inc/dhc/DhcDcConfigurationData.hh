//
// $Id$
//

#ifndef DhcDcConfigurationData_HH
#define DhcDcConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class DhcDcConfigurationData {

public:
  enum {
    versionNumber=0
  };

public:
  DhcDcConfigurationData();

  const unsigned char* chipid() const;
  void          chipid(unsigned char n);

  const bool controlTriggerTimestampEnable() const;
  void       controlTriggerTimestampEnable(bool b);

  const bool controlZeroSuppressionEnable() const;
  void       controlZeroSuppressionEnable(bool b);

  const unsigned char* control() const;
  void          control(unsigned char n);

  const unsigned char* triggerWidth() const;
  void          triggerWidth(unsigned char n);

  const unsigned char* triggerDelay() const;
  void          triggerDelay(unsigned char n);

  const unsigned char* enable() const;
  void enable(const unsigned char* n);
  void enable(const unsigned b, const unsigned char);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned char _chipid;
  unsigned char _control;
  unsigned char _triggerWidth;
  unsigned char _triggerDelay;
  unsigned char _enable[3];
  unsigned char _padding; // Make multiple of 4 bytes
};

#ifdef CALICE_DAQ_ICC

DhcDcConfigurationData::DhcDcConfigurationData() {
  _chipid=31;
  _control=3;
  _triggerWidth=7;
  _triggerDelay=0;
  memset(_enable,0xff,sizeof(_enable));
}

const unsigned char* DhcDcConfigurationData::chipid() const {
  return &_chipid;
}

void DhcDcConfigurationData::chipid(unsigned char n) {
  _chipid=n;
}

const bool DhcDcConfigurationData::controlTriggerTimestampEnable() const {
  return _control & 0x02;
}

void DhcDcConfigurationData::controlTriggerTimestampEnable(bool b) {
  ( b ? _control |= 0x02 : _control &= ~0x02 );
}

const bool DhcDcConfigurationData::controlZeroSuppressionEnable() const {
  return _control & 0x01;
}

void DhcDcConfigurationData::controlZeroSuppressionEnable(bool b) {
  ( b ? _control |= 0x01 : _control &= ~0x01 );
}

const unsigned char* DhcDcConfigurationData::control() const {
  return &_control;
}

void DhcDcConfigurationData::control(unsigned char n) {
  _control=n;
}

const unsigned char* DhcDcConfigurationData::triggerWidth() const {
  return &_triggerWidth;
}

void DhcDcConfigurationData::triggerWidth(unsigned char n) {
  _triggerWidth=n;
}

const unsigned char* DhcDcConfigurationData::triggerDelay() const {
  return &_triggerDelay;
}

void DhcDcConfigurationData::triggerDelay(unsigned char n) {
  _triggerDelay=n;
}

const unsigned char* DhcDcConfigurationData::enable() const {
  return _enable;
}

void  DhcDcConfigurationData::enable(const unsigned char* n) {
  memcpy(_enable,n,sizeof(_enable));
}

void DhcDcConfigurationData::enable(const unsigned b, const unsigned char n) {
  _enable[b] = n;
}

std::ostream& DhcDcConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "DhcDcConfigurationData::print()" << std::endl;

  o << s << " ChipID       = " << printHex(_chipid) << std::endl;
  o << s << " Control      = " << printHex(_control) << std::endl;
  o << s << "   Trigger Timestamp Enable is " << (controlTriggerTimestampEnable() ? "On" : "Off");
  o << s << std::endl;

  o << s << "   Zero Suppression Enable is " << (controlZeroSuppressionEnable() ? "On" : "Off");
  o << s << std::endl;
  o << s << " TriggerWidth = " << printHex(_triggerWidth) << std::endl;
  o << s << " TriggerDelay = " << printHex(_triggerDelay) << std::endl;

  o << s << " Enable    = ";
  for(unsigned i(0); i<sizeof(_enable); i++)
    o << s << std::setfill('0') << std::setw(2)
      << std::hex << static_cast<unsigned int>(_enable[i]) << std::dec;
  o << s << std::setfill(' ');
  o << s << std::endl;

  return o;
}

#endif //  CALICE_DAQ_ICC
#endif // DhcDcConfigurationData_HH
