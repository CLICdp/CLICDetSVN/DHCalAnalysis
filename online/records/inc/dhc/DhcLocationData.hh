//
// $Id: DhcLocationData.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef DhcLocationData_HH
#define DhcLocationData_HH

#include "DhcLocation.hh"

template <class Data> class DhcLocationData : public DhcLocation {

public:
  DhcLocationData();
  DhcLocationData(DhcLocation l);
  DhcLocationData(DhcLocation l, const Data &d);
  
  DhcLocation location() const;
  void location(DhcLocation l);

  const Data* data() const;
  Data*       data();
  void        data(Data &p);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  Data _data;
};


template <class Data> 
DhcLocationData<Data>::DhcLocationData() : DhcLocation(), _data() {
}
  
template <class Data> 
DhcLocationData<Data>::DhcLocationData(DhcLocation l) :
  DhcLocation(l), _data() {
}
  
template <class Data> 
DhcLocationData<Data>::DhcLocationData(DhcLocation l, const Data &d) :
  DhcLocation(l), _data(d) {
}
  
template <class Data> 
DhcLocation DhcLocationData<Data>::location() const {
  return *((DhcLocation*)this);
}

template <class Data> 
void DhcLocationData<Data>::location(DhcLocation l) {
  *((DhcLocation*)this)=l;
}

template <class Data> 
const Data* DhcLocationData<Data>::data() const {
  return &_data;
}

template <class Data> 
Data* DhcLocationData<Data>::data() {
  return &_data;
}

template <class Data> 
void DhcLocationData<Data>::data(Data &p) {
  _data=p;
}

template <class Data> 
std::ostream& DhcLocationData<Data>::print(std::ostream &o, std::string s) const {
  o << s << "DhcLocationData::print()" << std::endl;
  DhcLocation::print(o,s+" ");
  _data.print(o,s+" ");
  return o;
}

#ifdef CALICE_DAQ_ICC
#endif // CALICE_DAQ_ICC
#endif // DhcLocationData_HH
