//
// $Id: DhcReadoutConfigurationData.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef DhcReadoutConfigurationData_HH
#define DhcReadoutConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"
#include "UtlTime.hh"


class DhcReadoutConfigurationData {

public:
  enum {
    versionNumber=0
  };

  DhcReadoutConfigurationData();

  //CRP to get the full word called numbers, the crate number can be retrieved
  //from it
  unsigned int numbers() const;

  unsigned char crateNumber() const;
  void          crateNumber(unsigned char c);

  bool slotFeEnable(unsigned s, unsigned f) const;
  void slotFeEnable(unsigned s, unsigned f, bool b);

  unsigned short slotFeEnables(unsigned s) const;
  void           slotFeEnables(unsigned s, unsigned short b);

  unsigned char slotFeRevision(unsigned s) const;
  void          slotFeRevision(unsigned s, unsigned char v);

  //CRP to get the full slotEnable word, the actual configuration can be 
  //retrieved from it
  unsigned int slotEnable() const;

  bool slotEnable(unsigned s) const;
  void slotEnable(unsigned s, bool b);

  UtlTimeDifference beWaitInterval() const;
  void     beWaitInterval(UtlTimeDifference t);

  void reset();

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  UtlPack _numbers;
  UtlPack _slotEnable;
  UtlPack _slotFeEnable[10];
  UtlPack _slotFeRevision[5];
  UtlTimeDifference _beWaitInterval;
};

#ifdef CALICE_DAQ_ICC

DhcReadoutConfigurationData::DhcReadoutConfigurationData() {
  memset(this,0,sizeof(DhcReadoutConfigurationData));
}

void DhcReadoutConfigurationData::reset() {

  for(unsigned i(2);i<=21;i++) {
    slotEnable(i,false);
    for(unsigned f(0);f<12;f++) slotFeEnable(i,f,false);
    slotFeRevision(i,3);
  }
  beWaitInterval(UtlTimeDifference(0,1000));
}

//CRP see above 
unsigned int DhcReadoutConfigurationData::numbers() const {
  return _numbers.word();
}
//CRP

unsigned char DhcReadoutConfigurationData::crateNumber() const {
  return _numbers.byte(0);
}

void DhcReadoutConfigurationData::crateNumber(unsigned char c) {
  _numbers.byte(0,c);
}

bool DhcReadoutConfigurationData::slotFeEnable(unsigned s, unsigned f) const {
  assert(s>=2 && s<=21 && f<12);
  return _slotFeEnable[(s-2)/2].bit(16*((s-2)%2)+f);
}

void DhcReadoutConfigurationData::slotFeEnable(unsigned s, unsigned f, bool b) {
  assert(s>=2 && s<=21 && f<12);
  _slotFeEnable[(s-2)/2].bit(16*((s-2)%2)+f,b);
}

unsigned short DhcReadoutConfigurationData::slotFeEnables(unsigned s) const {
  assert(s>=2 && s<=21);
  return _slotFeEnable[(s-2)/2].halfWord((s-2)%2);
}

void DhcReadoutConfigurationData::slotFeEnables(unsigned s, unsigned short b) {
  assert(s>=2 && s<=21);
  _slotFeEnable[(s-2)/2].halfWord((s-2)%2,b);
}

unsigned char DhcReadoutConfigurationData::slotFeRevision(unsigned s) const {
  assert(s>=2 && s<=21);
  return _slotFeRevision[(s-2)/4].byte((s-2)%4);
}

void DhcReadoutConfigurationData::slotFeRevision(unsigned s, unsigned char v) {
  assert(s>=2 && s<=21);
  _slotFeRevision[(s-2)/4].byte((s-2)%4,v);
}

//CRP see above 
unsigned int DhcReadoutConfigurationData::slotEnable() const {
  return _slotEnable.word();
}
//CRP

bool DhcReadoutConfigurationData::slotEnable(unsigned s) const { 
  assert(s>=2 && s<=21);
  return _slotEnable.bit(s);
}

void DhcReadoutConfigurationData::slotEnable(unsigned s, bool b) { 
  assert(s>=2 && s<=21);
  if(!b) for(unsigned f(0);f<12;f++) slotFeEnable(s,f,false);
  _slotEnable.bit(s,b);
}

UtlTimeDifference DhcReadoutConfigurationData::beWaitInterval() const {
  return _beWaitInterval;
}

void DhcReadoutConfigurationData::beWaitInterval(UtlTimeDifference t) {
  _beWaitInterval=t;
}

std::ostream& DhcReadoutConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "DhcReadoutConfigurationData::print()" << std::endl;

  o << s << " Crate number  = " << printHex(_numbers.byte(0)) << std::endl;
  for(unsigned i(2);i<=21;i++) {
    if(slotEnable(i)) {
      o << s << "  Slot " << std::setw(2) << i << "," << std::endl;
      o << s << "     FE enables = "
	<< printHex(_slotFeEnable[(i-2)/2].halfWord((i-2)%2)) << std::endl;
      o << s << "     FE revision = "
	<< printHex(_slotFeRevision[(i-2)/4].byte((i-2)%4)) << std::endl;
    }
  }
  o << s << " Be Wait interval   = " << _beWaitInterval << std::endl;
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // DhcReadoutConfigurationData_HH
