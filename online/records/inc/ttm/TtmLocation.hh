//
// $Id: TtmLocation.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef TtmLocation_HH
#define TtmLocation_HH

#include <string>
#include <iostream>

#include "UtlLocation.hh"
#include "UtlPrintHex.hh"

class TtmLocation : public UtlLocation {

public:
  enum TtmComponent {
    master,slave,
    endOfTtmComponentEnum
  };

  TtmLocation();
  TtmLocation(unsigned char c, unsigned char s);
  
  TtmComponent       ttmComponent() const;
  void               ttmComponent(TtmComponent f);
  std::string        ttmComponentName() const;
  static std::string ttmComponentName(TtmComponent c);

  bool verify() const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  static const std::string _ttmComponentName[endOfTtmComponentEnum];
};

#ifdef CALICE_DAQ_ICC

TtmLocation::TtmLocation() : UtlLocation() {
}

TtmLocation::TtmLocation(unsigned char c, unsigned char s) :
  UtlLocation(c,s,0) {
}
  
TtmLocation::TtmComponent TtmLocation::ttmComponent() const {
  return (TtmComponent)componentNumber();
}
  
void TtmLocation::ttmComponent(TtmComponent f) {
  componentNumber((unsigned char)f);
}
  
std::string TtmLocation::ttmComponentName() const {
  return ttmComponentName(ttmComponent());
}

std::string TtmLocation::ttmComponentName(TtmComponent c) {
  if(c<endOfTtmComponentEnum) return _ttmComponentName[c];
  return "Unknown";
}

bool TtmLocation::verify() const {
  return slotNumber()<22 &&
    ttmComponent()<endOfTtmComponentEnum;
}

std::ostream& TtmLocation::print(std::ostream &o, std::string s) const {
  o << s << "TtmLocation::print()" << std::endl;

  o << s << " Crate number     = "
    << (unsigned)crateNumber();
  o << std::endl;

  o << s << " Slot number      = "
    << (unsigned)slotNumber();
  o << std::endl;

  o << s << " TTM Component    = " << std::setw(4) << (unsigned)ttmComponent() 
    << " = " << ttmComponentName() << std::endl;

  return o;
}

const std::string TtmLocation::_ttmComponentName[]={
  "Master","Slave"};

#endif // CALICE_DAQ_ICC
#endif // TtmLocation_HH 
