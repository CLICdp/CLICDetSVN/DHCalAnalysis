//
// $Id: TtmConfigurationDataV0.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef TtmConfigurationDataV0_HH
#define TtmConfigurationDataV0_HH

#include <string>
#include <iostream>
#include <stdint.h>

#include "UtlPack.hh"


class TtmConfigurationDataV0 {

public:
  enum {
    versionNumber=0
  };
  
  enum Signal {
    trigger=1,
    tcal=2,
    reset=4
  };
  
  TtmConfigurationDataV0();
  
  void vcs(unsigned v);
  void tcs(unsigned t);
  
  unsigned vmeFirmware() const;
  void vmeFirmware(unsigned v);
  
  unsigned tdFirmware() const;
  void tdFirmware(unsigned t);
  
  unsigned triggerDelay() const;
  void triggerDelay(unsigned d);

  bool tcsRamSignal() const;
  void tcsRamSignal(bool b);

  bool tcsInternalReset() const;
  void tcsInternalReset(bool b);

  bool tcsInternalTcal() const;
  void tcsInternalTcal(bool b);

  bool tcsInternalTrigger() const;
  void tcsInternalTrigger(bool b);

  bool tcsEnableTriggerDelay() const;
  void tcsEnableTriggerDelay(bool b);

  unsigned ramDepth() const;
  void     ramDepth(unsigned n);

  const unsigned* ramData() const;
  unsigned*       ramData();

  void     ramLoad(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned FirmwareMonth(UtlPack r) const;
  unsigned FirmwareDay(UtlPack r) const;
  unsigned FirmwareYearMsb(UtlPack r) const;
  unsigned FirmwareYearLsb(UtlPack r) const;
  unsigned FirmwareVersion(UtlPack r) const;
  unsigned FirmwareRevision(UtlPack r) const;

private:
  UtlPack _vcs;
  uint32_t _oscUnlockCounter;
  uint32_t _gpsUnlockCounter;
  UtlPack _vmeFirmware;
  UtlPack _tdFirmware;
  uint32_t _gpsTransfer;
  uint32_t _gpsReceive;
  UtlPack _tcs;
  uint32_t _triggerDelay;
  UtlPack _manualSignals;
  uint32_t _ramLoopCount;
  uint32_t _ramDepth;
  uint32_t _ramData[8];
};

#ifdef CALICE_DAQ_ICC

TtmConfigurationDataV0::TtmConfigurationDataV0() {
  _ramDepth=8;
  memset(_ramData,0,sizeof(_ramData));
}

void TtmConfigurationDataV0::vcs(unsigned v) {
  _vcs.word(v);
}

void TtmConfigurationDataV0::tcs(unsigned t) {
  _tcs.word(t);
}

unsigned TtmConfigurationDataV0::vmeFirmware() const {
  return _vmeFirmware.word();
}

void TtmConfigurationDataV0::vmeFirmware(unsigned v) {
  _vmeFirmware.word(v);
}

unsigned TtmConfigurationDataV0::tdFirmware() const {
  return _tdFirmware.word();
}

void TtmConfigurationDataV0::tdFirmware(unsigned t) {
  _tdFirmware.word(t);
}

unsigned TtmConfigurationDataV0::triggerDelay() const {
  return _triggerDelay;
}

void TtmConfigurationDataV0::triggerDelay(unsigned d) {
  _triggerDelay=d;
}

bool TtmConfigurationDataV0::tcsRamSignal() const {
  return _tcs.bit(7);
}

void TtmConfigurationDataV0::tcsRamSignal(bool b) {
  _tcs.bit(7,b);
}

bool TtmConfigurationDataV0::tcsInternalReset() const {
  return _tcs.bit(4);
}

void TtmConfigurationDataV0::tcsInternalReset(bool b) {
  _tcs.bit(4,b);
}

bool TtmConfigurationDataV0::tcsInternalTcal() const {
  return _tcs.bit(3);
}

void TtmConfigurationDataV0::tcsInternalTcal(bool b) {
  _tcs.bit(3,b);
}

bool TtmConfigurationDataV0::tcsInternalTrigger() const {
  return _tcs.bit(2);
}

void TtmConfigurationDataV0::tcsInternalTrigger(bool b) {
  _tcs.bit(2,b);
}

bool TtmConfigurationDataV0::tcsEnableTriggerDelay() const {
  return _tcs.bit(10);
}

void TtmConfigurationDataV0::tcsEnableTriggerDelay(bool b) {
  _tcs.bit(10,b);
}


unsigned TtmConfigurationDataV0::FirmwareMonth(UtlPack r) const {
  return r.bits(28,31);
}

unsigned TtmConfigurationDataV0::FirmwareDay(UtlPack r) const {
  return r.bits(23,27);
}

unsigned TtmConfigurationDataV0::FirmwareYearMsb(UtlPack r) const {
  return r.bits(19,22);
}

unsigned TtmConfigurationDataV0::FirmwareYearLsb(UtlPack r) const {
  return r.bits(15,18);
}

unsigned TtmConfigurationDataV0::FirmwareVersion(UtlPack r) const {
  return r.bits(12,14);
}

unsigned TtmConfigurationDataV0::FirmwareRevision(UtlPack r) const {
  return r.bits(9,11);
}

unsigned TtmConfigurationDataV0::ramDepth() const {
  return _ramDepth;
}

void TtmConfigurationDataV0::ramDepth(unsigned n) {
  assert(n<=sizeof(_ramData)/sizeof(uint32_t));
  _ramDepth = n;
}

const unsigned* TtmConfigurationDataV0::ramData() const {
  return _ramData;
}

unsigned* TtmConfigurationDataV0::ramData() {
  return _ramData;
}

void TtmConfigurationDataV0::ramLoad(unsigned n) {
  assert((n+2)<=ramDepth());

  // initialize
  memset(_ramData,0,sizeof(_ramData));

  // set TCAL signal
  _ramData[1]=reset;

  // set Trigger signal
  _ramData[n]=tcal|trigger;
}

std::ostream& TtmConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "TtmConfigurationDataV0::print()" << std::endl;

  o << s << " VME Firmware Version "
    << FirmwareVersion(_vmeFirmware) << " Rev " << FirmwareRevision(_vmeFirmware) << " "
    << FirmwareDay(_vmeFirmware) << "-" << FirmwareMonth(_vmeFirmware) << "-"
    << FirmwareYearMsb(_vmeFirmware) << FirmwareYearLsb(_vmeFirmware);
  o << s << std::endl;
  o << s << " TD Firmware Version "
    << FirmwareVersion(_tdFirmware) << " Rev " << FirmwareRevision(_tdFirmware) << " "
    << FirmwareDay(_tdFirmware) << "-" << FirmwareMonth(_tdFirmware) << "-"
    << FirmwareYearMsb(_tdFirmware) << FirmwareYearLsb(_tdFirmware);
  o << s << std::endl;
  o << s << " Trigger Source is " << (tcsInternalTrigger() ? "Internal" : "External");
  o << s << std::endl;
  o << s << " Reset Source is " << (tcsInternalReset() ? "Internal" : "Oscillator");
  o << s << std::endl;
  o << s << " TCAL Source is " << (tcsInternalTcal() ? "Internal" : "External");
  o << s << std::endl;
  o << s << " Signal Source is " << (tcsRamSignal() ? "RAM" : "Register");
  o << s << std::endl;
  o << s << " Trigger Delay is " << (tcsEnableTriggerDelay() ? "Enabled" : "Disabled");
  if (tcsEnableTriggerDelay())
    o << s << ": " << triggerDelay()*100 << " ns" ;
  o << s << std::endl;
  o << s << " RAM depth is " << ramDepth();
  o << s << std::endl;
  if (ramDepth()) {
    const unsigned* ram(ramData());
    o << s << " RAM     = ";
    for(unsigned i(0); i<ramDepth(); i++)
      o << s << std::setfill('0') << std::setw(2)
	<< std::hex << static_cast<unsigned int>(ram[i]) << std::dec;
  }
  o << s << std::setfill(' ');
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // TtmConfigurationDataV0_HH
