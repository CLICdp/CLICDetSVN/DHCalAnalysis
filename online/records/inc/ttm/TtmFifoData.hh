#ifndef TtmFifoData_HH
#define TtmFifoData_HH


class TtmFifoData {

public:
  TtmFifoData(const unsigned& size,
	      const unsigned* ptr);
  virtual ~TtmFifoData();

  unsigned numberOfTriggerTS() const;
  unsigned numberOfFifoATS() const;
  unsigned numberOfFifoBTS() const;

  const unsigned* triggerTS() const;
  const unsigned* fifoATS() const;
  const unsigned* fifoBTS() const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned* _data;

};

#ifdef CALICE_DAQ_ICC

TtmFifoData::TtmFifoData(const unsigned& size,
			 const unsigned* ptr)
  : _data(new unsigned[size*sizeof(unsigned)])
{
  memcpy(_data, ptr, size*sizeof(unsigned));
}

TtmFifoData::~TtmFifoData()
{
  delete [] _data;
}

unsigned
TtmFifoData::numberOfTriggerTS() const {
  return *_data;
}

unsigned
TtmFifoData::numberOfFifoATS() const {
  return *(_data+numberOfTriggerTS()+1);
}

unsigned
TtmFifoData::numberOfFifoBTS() const {
  return *(_data+numberOfTriggerTS()+numberOfFifoATS()+2);
}

const unsigned*
TtmFifoData::triggerTS() const {
  return _data+1;
}

const unsigned*
TtmFifoData::fifoATS() const {
  return _data+numberOfTriggerTS()+2;
}

const unsigned*
TtmFifoData::fifoBTS() const {
  return _data+numberOfTriggerTS()+numberOfFifoATS()+3;
}

std::ostream& TtmFifoData::print(std::ostream &o, std::string s) const {

  o << s << "TtmFifoData::print()  Number of TriggerTS = " << numberOfTriggerTS() << std::endl;
  o << s << "TTM TTS " << *triggerTS() << " " ;
  if (numberOfFifoATS())
    o << s << "FIFOA " << *fifoATS() << " ";
  if (numberOfFifoBTS())
    o << s  << "FIFOB " << *fifoBTS() << " ";
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // TtmFifoData_HH
