//
// $Id: TtmLocationData.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef TtmLocationData_HH
#define TtmLocationData_HH

#include "TtmLocation.hh"

template <class Data> class TtmLocationData : public TtmLocation {

public:
  TtmLocationData();
  TtmLocationData(TtmLocation l);
  TtmLocationData(TtmLocation l, const Data &d);
  
  TtmLocation location() const;
  void location(TtmLocation l);

  const Data* data() const;
  Data*       data();
  void        data(Data &p);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  Data _data;
};

#ifdef CALICE_DAQ_ICC

template <class Data> 
TtmLocationData<Data>::TtmLocationData() : TtmLocation(), _data() {
}
  
template <class Data> 
TtmLocationData<Data>::TtmLocationData(TtmLocation l) :
  TtmLocation(l), _data() {
}
  
template <class Data> 
TtmLocationData<Data>::TtmLocationData(TtmLocation l, const Data &d) :
  TtmLocation(l), _data(d) {
}
  
template <class Data> 
TtmLocation TtmLocationData<Data>::location() const {
  return *((TtmLocation*)this);
}

template <class Data> 
void TtmLocationData<Data>::location(TtmLocation l) {
  *((TtmLocation*)this)=l;
}

template <class Data> 
const Data* TtmLocationData<Data>::data() const {
  return &_data;
}

template <class Data> 
Data* TtmLocationData<Data>::data() {
  return &_data;
}

template <class Data> 
void TtmLocationData<Data>::data(Data &p) {
  _data=p;
}

template <class Data> 
std::ostream& TtmLocationData<Data>::print(std::ostream &o, std::string s) const {
  o << s << "TtmLocationData::print()" << std::endl;
  TtmLocation::print(o,s+" ");
  _data.print(o,s+" ");
  return o;
}

#endif // CALICE_DAQ_ICC
#endif // TtmLocationData_HH
