//
// $Id: TtmConfigurationDataV1.hh,v 1.1 2008/06/27 10:34:05 meyern Exp $
//

#ifndef TtmConfigurationDataV1_HH
#define TtmConfigurationDataV1_HH

#include <string>
#include <iostream>
#include <stdint.h>

#include "UtlPack.hh"


class TtmConfigurationDataV1 {

public:
  enum {
    versionNumber=1
  };
  
  enum Signal {
    trigger=1,
    tcal=2,
    reset=4
  };
  
  TtmConfigurationDataV1();
  
  void cs(unsigned r);
  
  unsigned Firmware() const;
  void Firmware(unsigned f);
  
  unsigned triggerDelay() const;
  void triggerDelay(unsigned d);

  bool csRamSignal() const;
  void csRamSignal(bool b);

  bool csInternalReset() const;
  void csInternalReset(bool b);

  bool csInternalTcal() const;
  void csInternalTcal(bool b);

  bool csInternalTrigger() const;
  void csInternalTrigger(bool b);

  bool csEnableTriggerDelay() const;
  void csEnableTriggerDelay(bool b);

  unsigned ramDepth() const;
  void     ramDepth(unsigned n);

  const unsigned* ramData() const;
  unsigned*       ramData();

  void     ramLoad(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned FirmwareMonth(UtlPack r) const;
  unsigned FirmwareDay(UtlPack r) const;
  unsigned FirmwareYearMsb(UtlPack r) const;
  unsigned FirmwareYearLsb(UtlPack r) const;
  unsigned FirmwareVersion(UtlPack r) const;
  unsigned FirmwareRevision(UtlPack r) const;

private:
  UtlPack _Firmware;
  UtlPack _cs;
  uint32_t _triggerDelay;
  UtlPack _manualSignals;
  uint32_t _ramLoopCount;
  uint32_t _ramDepth;
  uint32_t _ramData[8];
};

#ifdef CALICE_DAQ_ICC

TtmConfigurationDataV1::TtmConfigurationDataV1() {
  _ramDepth=8;
  memset(_ramData,0,sizeof(_ramData));
}

void TtmConfigurationDataV1::cs(unsigned r) {
  _cs.word(r);
}

unsigned TtmConfigurationDataV1::Firmware() const {
  return _Firmware.word();
}

void TtmConfigurationDataV1::Firmware(unsigned f) {
  _Firmware.word(f);
}

unsigned TtmConfigurationDataV1::triggerDelay() const {
  return _triggerDelay;
}

void TtmConfigurationDataV1::triggerDelay(unsigned d) {
  _triggerDelay=d;
}

bool TtmConfigurationDataV1::csRamSignal() const {
  return _cs.bit(11);
}

void TtmConfigurationDataV1::csRamSignal(bool b) {
  _cs.bit(11,b);
}

bool TtmConfigurationDataV1::csInternalReset() const {
  return _cs.bit(12);
}

void TtmConfigurationDataV1::csInternalReset(bool b) {
  _cs.bit(12,b);
}

bool TtmConfigurationDataV1::csInternalTcal() const {
  return _cs.bit(9);
}

void TtmConfigurationDataV1::csInternalTcal(bool b) {
  _cs.bit(9,b);
}

bool TtmConfigurationDataV1::csInternalTrigger() const {
  return _cs.bit(8);
}

void TtmConfigurationDataV1::csInternalTrigger(bool b) {
  _cs.bit(8,b);
}

bool TtmConfigurationDataV1::csEnableTriggerDelay() const {
  return _cs.bit(14);
}

void TtmConfigurationDataV1::csEnableTriggerDelay(bool b) {
  _cs.bit(14,b);
}


unsigned TtmConfigurationDataV1::FirmwareMonth(UtlPack r) const {
  return r.bits(28,31);
}

unsigned TtmConfigurationDataV1::FirmwareDay(UtlPack r) const {
  return r.bits(23,27);
}

unsigned TtmConfigurationDataV1::FirmwareYearMsb(UtlPack r) const {
  return r.bits(19,22);
}

unsigned TtmConfigurationDataV1::FirmwareYearLsb(UtlPack r) const {
  return r.bits(15,18);
}

unsigned TtmConfigurationDataV1::FirmwareVersion(UtlPack r) const {
  return r.bits(12,14);
}

unsigned TtmConfigurationDataV1::FirmwareRevision(UtlPack r) const {
  return r.bits(9,11);
}

unsigned TtmConfigurationDataV1::ramDepth() const {
  return _ramDepth;
}

void TtmConfigurationDataV1::ramDepth(unsigned n) {
  assert(n<=sizeof(_ramData)/sizeof(uint32_t));
  _ramDepth = n;
}

const unsigned* TtmConfigurationDataV1::ramData() const {
  return _ramData;
}

unsigned* TtmConfigurationDataV1::ramData() {
  return _ramData;
}

void TtmConfigurationDataV1::ramLoad(unsigned n) {
  assert((n+2)<=ramDepth());

  // initialize
  memset(_ramData,0,sizeof(_ramData));

  // set TCAL signal
  _ramData[1]=reset;

  // set Trigger signal
  _ramData[n]=tcal|trigger;
}

std::ostream& TtmConfigurationDataV1::print(std::ostream &o, std::string s) const {
  o << s << "TtmConfigurationDataV1::print()" << std::endl;

  o << s << " Firmware Version "
    << FirmwareVersion(_Firmware) << " Rev " << FirmwareRevision(_Firmware) << " "
    << FirmwareDay(_Firmware) << "-" << FirmwareMonth(_Firmware) << "-"
    << FirmwareYearMsb(_Firmware) << FirmwareYearLsb(_Firmware);
  o << s << std::endl;
  o << s << " Trigger Source is " << (csInternalTrigger() ? "Internal" : "External");
  o << s << std::endl;
  o << s << " Reset Source is " << (csInternalReset() ? "Internal" : "Oscillator");
  o << s << std::endl;
  o << s << " TCAL Source is " << (csInternalTcal() ? "Internal" : "External");
  o << s << std::endl;
  o << s << " Signal Source is " << (csRamSignal() ? "RAM" : "Register");
  o << s << std::endl;
  o << s << " Trigger Delay is " << (csEnableTriggerDelay() ? "Enabled" : "Disabled");
  if (csEnableTriggerDelay())
    o << s << ": " << triggerDelay()*100 << " ns" ;
  o << s << std::endl;
  o << s << " RAM depth is " << ramDepth();
  o << s << std::endl;
  if (ramDepth()) {
    const unsigned* ram(ramData());
    o << s << " RAM     = ";
    for(unsigned i(0); i<ramDepth(); i++)
      o << s << std::setfill('0') << std::setw(2)
	<< std::hex << static_cast<unsigned int>(ram[i]) << std::dec;
  }
  o << s << std::setfill(' ');
  o << s << std::endl;

  return o;
}

#endif // CALICE_DAQ_ICC
#endif // TtmConfigurationDataV1_HH
