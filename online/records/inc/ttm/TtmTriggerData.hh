#ifndef TtmTriggerData_HH
#define TtmTriggerData_HH

#include <string>
#include <iostream>

#include "UtlPrintHex.hh"


class TtmTriggerData {

public:
  enum {
    versionNumber=0
  };

  TtmTriggerData();

  unsigned numberOfWords() const;
  void     numberOfWords(unsigned n);

  const unsigned* data() const;
  unsigned*       data();

  std::ostream& print(std::ostream &o, std::string s="", bool raw=false) const;

private:
  unsigned _numberOfWords;
};

#ifdef CALICE_DAQ_ICC

TtmTriggerData::TtmTriggerData() {
  memset(this,0,sizeof(TtmTriggerData));
}

unsigned TtmTriggerData::numberOfWords() const {
  return _numberOfWords;
}

void TtmTriggerData::numberOfWords(unsigned n) {
  _numberOfWords=n;
}

const unsigned* TtmTriggerData::data() const {
  return (&_numberOfWords)+1;
}

unsigned* TtmTriggerData::data() {
  return (&_numberOfWords)+1;
}

std::ostream& TtmTriggerData::print(std::ostream &o, std::string s, bool raw) const {
  o << s << "TtmTriggerData::print()  Number of words = " 
    << numberOfWords() << std::endl;
  if(_numberOfWords==0) return o;

  const unsigned *d(data());
  for(unsigned i(0);i<_numberOfWords;i++) {
    o << s << " Data word " << std::setw(5) << i << " = " << printHex(d[i], raw) << std::endl;
  }
  return o;
}

#endif // CALICE_DAQ_ICC
#endif // TtmTriggerData_HH
