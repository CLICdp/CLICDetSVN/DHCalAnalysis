#ifndef UtlMapDefinitions_HH
#define UtlMapDefinitions_HH

#include <map>
#include <vector>
#include <string>

typedef std::map<std::string, std::vector<double>       > DaqTypesDblMap_t;
typedef std::map<std::string, std::vector<unsigned int> > DaqTypesUIntMap_t;
typedef std::map<std::string, std::vector<int>          > DaqTypesIntMap_t;
typedef std::map<std::string, std::vector<float>        > DaqTypesFloatMap_t;

#endif
