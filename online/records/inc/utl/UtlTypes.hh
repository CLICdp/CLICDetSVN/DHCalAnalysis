#ifndef UtlTypes_HH
#define UtlTypes_HH

namespace CaliceDaq {

  typedef unsigned       uint32_t;
  typedef unsigned short uint16_t;
  typedef unsigned char  uint08_t;
  typedef unsigned char  uint8_t;
  
  typedef int            int32_t;
  typedef int short      int16_t;
  typedef char           int08_t;
  typedef char           int8_t;

};  // namespace CaliceDaq

#endif
