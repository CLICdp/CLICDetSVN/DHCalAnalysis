#ifndef UtlTime_HH
#define UtlTime_HH

#include <sys/time.h>

#include <string>
#include <iostream>

class UtlTimeDifference;

//class UtlTime : public timeval { // 16 bytes in 64-bit
class UtlTime {
  friend class UtlTimeDifference;

public:
  UtlTime(int s, int u);
  UtlTime(bool sett=false);

  int update();

  time_t timeStamp() const;
  int seconds() const;
  int microseconds() const;
  double time() const;

  UtlTime operator+(const UtlTimeDifference &d) const;
  UtlTime& operator+=(const UtlTimeDifference &d);
  UtlTime operator-(const UtlTimeDifference &d) const;
  UtlTime& operator-=(const UtlTimeDifference &d);
  UtlTimeDifference operator-(const UtlTime &t) const;

  bool operator==(const UtlTime &that) const;
  bool operator!=(const UtlTime &that) const;
  bool operator>(const UtlTime &that) const;
  bool operator<(const UtlTime &that) const;

  std::ostream& print(std::ostream &o, std::string s="") const;

  //bool write(std::ostream &o) const;
  //bool read(std::istream &i) const;

private:
  void correctSigns();

  int tv_sec;
  int tv_usec;
};


class UtlTimeDifference {
  friend class UtlTime;

public:
  UtlTimeDifference();
  UtlTimeDifference(int s, int u=0);
  UtlTimeDifference(const UtlTime& t);
  UtlTimeDifference(double d);

  UtlTime operator+(const UtlTime& t);
  UtlTimeDifference operator-(const UtlTimeDifference& t);
  bool operator>(const UtlTimeDifference& t);
  bool operator<(const UtlTimeDifference& t);

  int seconds() const;
  int microseconds() const;
  double deltaTime() const;
  
  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  void correctSigns();

  int dt_sec;
  int dt_usec;
};

std::ostream& operator<<(std::ostream &o, const UtlTime &tv);


#ifdef CALICE_DAQ_ICC

#include <cstring>
#include <cstdio>


UtlTime::UtlTime(int s, int u) {
  tv_sec=s;
  tv_usec=u;
  correctSigns();
}

UtlTime::UtlTime(bool sett) {
  tv_sec=0;
  tv_usec=0;
  if(sett) update();
}

int UtlTime::update() {
  timeval tv;
  int r(gettimeofday(&tv,0));
  tv_sec=tv.tv_sec;
  tv_usec=tv.tv_usec;
  return r;
}

time_t UtlTime::timeStamp() const {
  return tv_sec;
}

int UtlTime::seconds() const {
  return tv_sec;
}

int UtlTime::microseconds() const {
  return tv_usec;
}

double UtlTime::time() const {
    return tv_sec+0.000001*tv_usec;
}

UtlTime UtlTime::operator+(const UtlTimeDifference &d) const {
  UtlTime t(tv_sec+d.dt_sec,tv_usec+d.dt_usec);
  t.correctSigns();
  return t;
}

UtlTime& UtlTime::operator+=(const UtlTimeDifference &d) {
  tv_sec+=d.dt_sec;
  tv_usec+=d.dt_usec;
  correctSigns();
  return *this;
}

UtlTime UtlTime::operator-(const UtlTimeDifference &d) const {
  UtlTime t(tv_sec-d.dt_sec,tv_usec-d.dt_usec);
  t.correctSigns();
  return t;
}

UtlTime& UtlTime::operator-=(const UtlTimeDifference &d) {
  tv_sec-=d.dt_sec;
  tv_usec-=d.dt_usec;
  correctSigns();
  return *this;
}

UtlTimeDifference UtlTime::operator-(const UtlTime &t) const {
  UtlTimeDifference d(tv_sec-t.tv_sec,tv_usec-t.tv_usec);
  d.correctSigns();
  return d;
}

bool UtlTime::operator==(const UtlTime &that) const {
  if(tv_sec != that.tv_sec) return false;
  if(tv_usec!=that.tv_usec) return false;
  return true;
}

bool UtlTime::operator!=(const UtlTime &that) const {
  return !((*this)==that);
}

bool UtlTime::operator>(const UtlTime &that) const {
  if(tv_sec >that.tv_sec ) return true;
  if(tv_usec>that.tv_usec) return true;
  return false;
}

bool UtlTime::operator<(const UtlTime &that) const {
  if(tv_sec <that.tv_sec ) return true;
  if(tv_usec<that.tv_usec) return true;
  return false;
}

std::ostream& UtlTime::print(std::ostream &o, std::string s) const {
  o << s << "UtlTime::print() " << tv_sec << " sec  " 
    << tv_usec << " usec" << std::endl;
  return o;
}

/*
bool UtlTime::write(std::ostream &o) const {
  if(!o) return false;
  o << tv_sec << " " << tv_usec;
  return true;
}

bool UtlTime::read(std::istream &i) const {
  if(!i) return false;
  i >> static_cast<int>(tv_sec) >> static_cast<int>(tv_usec);
  return true;
}
*/

void UtlTime::correctSigns() {
  if(tv_usec<0) {
    tv_usec+=1000000;
    tv_sec--;
  }
  if(tv_usec>=1000000) {
    tv_usec-=1000000;
    tv_sec++;
  }
  if(tv_sec<0) {
    tv_sec=0;
    tv_usec=0;
  }
}

std::ostream& operator<<(std::ostream &o, const UtlTime &tv) {
  
  // Should be replaced by ostringstream code at some point...
  
  char t[34],c[26];
  time_t ts(tv.timeStamp());
  strcpy(c,ctime(&ts));
  strncpy(t,c+11,8);
  strncpy(t+17,c,11);
  strncpy(t+28,c+20,4);
  sprintf(t+8,":%3.3i:%3.3i",tv.microseconds()/1000,tv.microseconds()%1000);
  t[16]=' ';
  t[32]='\0';

  o << t;
  return o;
}

UtlTimeDifference::UtlTimeDifference() : dt_sec(0), dt_usec(0) {
  correctSigns();
}

UtlTimeDifference::UtlTimeDifference(int s, int u) : dt_sec(s), dt_usec(u) {
  correctSigns();
}

UtlTimeDifference::UtlTimeDifference(const UtlTime& t) {
  UtlTime n(true);
  *this=n-t;
}

UtlTimeDifference::UtlTimeDifference(double d) {
  dt_sec=(int)d;
  dt_usec=(int)((d-dt_sec)*1.0e6);
  correctSigns();
}

UtlTime UtlTimeDifference::operator+(const UtlTime& t) {
  return UtlTime(dt_sec+t.tv_sec,dt_usec+t.tv_usec);
}

UtlTimeDifference UtlTimeDifference::operator-(const UtlTimeDifference& t) {
  return UtlTimeDifference(dt_sec-t.dt_sec,dt_usec-t.dt_usec);
}

bool UtlTimeDifference::operator>(const UtlTimeDifference& t) {
  if(dt_sec>t.dt_sec) return true;
  if(dt_sec<t.dt_sec) return false;
  return dt_usec>t.dt_usec;
}

bool UtlTimeDifference::operator<(const UtlTimeDifference& t) {
  if(dt_sec<t.dt_sec) return true;
  if(dt_sec>t.dt_sec) return false;
  return dt_usec<t.dt_usec;
}

int UtlTimeDifference::seconds() const {
  return dt_sec;
}

int UtlTimeDifference::microseconds() const {
  return dt_usec;
}

double UtlTimeDifference::deltaTime() const {
  return dt_sec+0.000001*dt_usec;
}

std::ostream& UtlTimeDifference::print(std::ostream &o, std::string s) const {
  o << s << "UtlTimeDifference::print() " << dt_sec << " sec  " 
    << dt_usec << " usec" << std::endl;
  return o;
}

void UtlTimeDifference::correctSigns() {
  while(dt_usec<0) {
    dt_usec+=1000000;
    dt_sec--;
  }
  
  while(dt_usec>999999) {
    dt_usec-=1000000;
    dt_sec++;
  }
}

std::ostream& operator<<(std::ostream &o, const UtlTimeDifference &dt) {
  int dts(dt.seconds());
  int dtu(dt.microseconds());

  if(dts==-1) o << '-';

  if(dts<0) {
    dts++;
    dtu=1000000-dtu;
  }

  o << dts << ".";
  if(dtu<100000) o << "0";
  if(dtu< 10000) o << "0";
  if(dtu<  1000) o << "0";
  if(dtu<   100) o << "0";
  if(dtu<    10) o << "0";
  o << dtu;

  return o;
}
  
#endif
#endif
