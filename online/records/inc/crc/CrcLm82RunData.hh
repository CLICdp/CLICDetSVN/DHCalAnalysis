#ifndef CrcLm82RunData_HH
#define CrcLm82RunData_HH

#include <string>
#include <iostream>


class CrcLm82RunData {

public:
  enum {
    versionNumber=0
  };

  CrcLm82RunData();
  
  unsigned char manufacturerId() const;
  void          manufacturerId(unsigned char n);
  
  unsigned char steppingCode() const;
  void          steppingCode(unsigned char n);
    
  std::ostream& print(std::ostream &o, std::string s="") const;

  
private:
  unsigned _general;
};

#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcLm82RunData::CrcLm82RunData() {
  memset(this,0,sizeof(CrcLm82RunData));
}
  
unsigned char CrcLm82RunData::manufacturerId() const {
  return _general&0x000000ff;
}
  
void CrcLm82RunData::manufacturerId(unsigned char n) {
  _general&=0x0000ff00;
  _general|=n;
}
  
unsigned char CrcLm82RunData::steppingCode() const {
  return (_general&0x0000ff00)>>8;
}
  
void CrcLm82RunData::steppingCode(unsigned char n) {
  _general&=0x000000ff;
  _general|=(n<<8);
}
    
std::ostream& CrcLm82RunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcLm82RunData::print()" << std::endl;
  o << s << " Manufacturer id " << printHex(manufacturerId()) << std::endl;
  o << s << " Stepping code   " << printHex(steppingCode()) << std::endl;
  return o;
}
  
#endif
#endif
