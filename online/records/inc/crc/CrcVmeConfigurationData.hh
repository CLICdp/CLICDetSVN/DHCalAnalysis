#ifndef CrcVmeConfigurationData_HH
#define CrcVmeConfigurationData_HH

#include <string>
#include <iostream>

class CrcVmeConfigurationData {

public:
  enum {
    versionNumber=0
  };

  CrcVmeConfigurationData();

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
};


#ifdef CALICE_DAQ_ICC

CrcVmeConfigurationData::CrcVmeConfigurationData() {
}

std::ostream& CrcVmeConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "CrcVmeConfigurationData::print()" << std::endl;
  return o;
}

#endif
#endif
