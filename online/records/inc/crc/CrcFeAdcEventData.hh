#ifndef CrcFeAdcEventData_HH
#define CrcFeAdcEventData_HH

#include <string>
#include <iostream>

class CrcFeAdcEventData {

public:
  CrcFeAdcEventData();

  unsigned flags() const;
  void     flags(unsigned f);

  short int adc(unsigned chip) const;
  void      adc(unsigned chip, short int a);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  short int _adcs[12];
  unsigned _flags;
  //unsigned cludge[6]; // for V3.7_2
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"


CrcFeAdcEventData::CrcFeAdcEventData() {
  for(unsigned i(0);i<12;i++) _adcs[i]=0;
  _flags=0;
}

unsigned CrcFeAdcEventData::flags() const {
  return _flags;
}

void CrcFeAdcEventData::flags(unsigned f) {
  _flags=f;
}

short int CrcFeAdcEventData::adc(unsigned chip) const {
  return _adcs[chip];
  /* THOUGHT TO BE NEEDED BY OZ
     if(chip<6) return _adcs[chip];
     else {
     if((chip%2)==0) return _adcs[chip+1];
     else            return _adcs[chip-1];
     }
  */
}

void CrcFeAdcEventData::adc(unsigned chip, short int a) {
  _adcs[chip]=a;
  /* THOUGHT TO BE NEEDED BY OZ
     if(chip<6) _adcs[chip]=a;
     else {
     if((chip%2)==0) _adcs[chip+1]=a;
     else            _adcs[chip-1]=a;
     }
  */
}

std::ostream& CrcFeAdcEventData::print(std::ostream &o, std::string s) const {
  o << s << "CrcFeAdcEventData::print()" << std::endl;
  for(unsigned i(0);i<12;i++) {
    o << s << " Adc  " << std::setw(2) << i << " = " 
      << printHex(_adcs[i]) << std::endl;
  }
  o << s << " Flags = " << printHex(_flags) << std::endl;
  return o;
}

#endif
#endif
