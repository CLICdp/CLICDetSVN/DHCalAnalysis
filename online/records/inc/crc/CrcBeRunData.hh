#ifndef CrcBeRunData_HH
#define CrcBeRunData_HH

#include <string>
#include <iostream>


class CrcBeRunData {

public:
  enum {
    versionNumber=0
  };

  CrcBeRunData();

  unsigned firmwareId() const;
  void firmwareId(unsigned c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _firmwareId;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

CrcBeRunData::CrcBeRunData() {
  memset(this,0,sizeof(CrcBeRunData));
}

unsigned CrcBeRunData::firmwareId() const {
  return _firmwareId;
}

void CrcBeRunData::firmwareId(unsigned c) {
  _firmwareId=c;
}

std::ostream& CrcBeRunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeRunData::print()" << std::endl;
  o << s << " Firmware id   = " << printHex(firmwareId()) << std::endl;
  return o;
}

#endif
#endif
