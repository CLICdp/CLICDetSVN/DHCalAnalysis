#ifndef CrcBeEventDataV1_HH
#define CrcBeEventDataV1_HH

#include <string>
#include <iostream>

#include "CrcBeEventDataV0.hh"


class CrcBeEventDataV1 {

public:
  enum {
    versionNumber=1
  };

  CrcBeEventDataV1();
  CrcBeEventDataV1(const CrcBeEventDataV0 &c);

  unsigned status() const;
  void status(unsigned n);

  unsigned l1aCounter() const;
  void l1aCounter(unsigned n);

  unsigned bxCounter() const;
  void bxCounter(unsigned n);

  unsigned qdrFrameCounter() const;
  void qdrFrameCounter(unsigned n);

  unsigned qdrDataCounter() const;
  void qdrDataCounter(unsigned n);

  unsigned totalFrameCounter() const;
  void totalFrameCounter(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _status;
  unsigned _l1aCounter;
  unsigned _bxCounter;
  unsigned _qdrFrameCounter;
  unsigned _qdrDataCounter;
  unsigned _totalFrameCounter;
};


#ifdef CALICE_DAQ_ICC

CrcBeEventDataV1::CrcBeEventDataV1() {
}

CrcBeEventDataV1::CrcBeEventDataV1(const CrcBeEventDataV0& c) {
  _status=c.status();
  _l1aCounter=c.l1aCounter();
  _bxCounter=c.bxCounter();
  _qdrFrameCounter=c.qdrFrameCounter();
  _qdrDataCounter=c.qdrDataCounter();
  _totalFrameCounter=c.totalFrameCounter();
}

unsigned CrcBeEventDataV1::status() const {
  return _status;
}

void CrcBeEventDataV1::status(unsigned n) {
  _status=n;
}

unsigned CrcBeEventDataV1::l1aCounter() const {
  return _l1aCounter;
}

void CrcBeEventDataV1::l1aCounter(unsigned n) {
  _l1aCounter=n;
}

unsigned CrcBeEventDataV1::bxCounter() const {
  return _bxCounter;
}

void CrcBeEventDataV1::bxCounter(unsigned n) {
  _bxCounter=n;
}

unsigned CrcBeEventDataV1::qdrFrameCounter() const {
  return _qdrFrameCounter;
}

void CrcBeEventDataV1::qdrFrameCounter(unsigned n) {
  _qdrFrameCounter=n;
}

unsigned CrcBeEventDataV1::qdrDataCounter() const {
  return _qdrDataCounter;
}

void CrcBeEventDataV1::qdrDataCounter(unsigned n) {
  _qdrDataCounter=n;
}

unsigned CrcBeEventDataV1::totalFrameCounter() const {
  return _totalFrameCounter;
}

void CrcBeEventDataV1::totalFrameCounter(unsigned n) {
  _totalFrameCounter=n;
}

std::ostream& CrcBeEventDataV1::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeEventDataV1::print()" << std::endl;
  o << s << " Status       = " << printHex(_status) << std::endl;
  o << s << " L1A counter         = " << _l1aCounter << std::endl;
  o << s << " BX counter          = " << _bxCounter << std::endl;
  o << s << " QDR frame counter   = " << _qdrFrameCounter << std::endl;
  o << s << " QDR data counter    = " << _qdrDataCounter << std::endl;
  o << s << " Total frame counter = " << _totalFrameCounter << std::endl;
  return o;
}

#endif
#endif
