#ifndef CrcFeEventData_HH
#define CrcFeEventData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"
#include "CrcFeConfigurationData.hh"


class CrcFeEventData {

public:
  enum {
    versionNumber=0
  };

  CrcFeEventData();

  unsigned triggerCounter() const;
  void     triggerCounter(unsigned n);

  UtlPack  spyRegister() const;
  void     spyRegister(UtlPack n);

  unsigned numberOfFifoWords() const;
  bool fifoEmpty() const;
  bool fifoFull() const;
  bool srOut(CrcFeConfigurationData::Connector c) const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _triggerCounter;
  UtlPack  _spyRegister;
};


#ifdef CALICE_DAQ_ICC

CrcFeEventData::CrcFeEventData() {
}

unsigned CrcFeEventData::triggerCounter() const {
  return _triggerCounter;
  }

void CrcFeEventData::triggerCounter(unsigned n) {
  _triggerCounter=n;
}

UtlPack CrcFeEventData::spyRegister() const {
  return _spyRegister;
}

void CrcFeEventData::spyRegister(UtlPack n) {
  _spyRegister=n;
}

unsigned CrcFeEventData::numberOfFifoWords() const {
  return _spyRegister.halfWord(0);
}

bool CrcFeEventData::fifoEmpty() const {
  return _spyRegister.bit(17);
}

bool CrcFeEventData::fifoFull() const {
  return _spyRegister.bit(16);
}

bool CrcFeEventData::srOut(CrcFeConfigurationData::Connector c) const {
  if(c==CrcFeConfigurationData::bot) return _spyRegister.bit(21);
  else                               return _spyRegister.bit(20);
}

std::ostream& CrcFeEventData::print(std::ostream &o, std::string s) const {
  o << s << "CrcFeEventData::print()" << std::endl;
  o << s << " Trigger counter = " << triggerCounter() << std::endl;
  
  o << s << " Spy register = " << printHex(spyRegister()) << std::endl;
  o << s << "  Number of FIFO words = "
    << std::setw(6) << numberOfFifoWords();
  if(fifoEmpty() || fifoFull()) {
    if(fifoEmpty()) o << ", empty";
    if(fifoFull())  o << ", full";
  }
  o << std::endl;    

  if(_spyRegister.bit(18)) o << s << "  Mystery bit 18 true" << std::endl;
  if(_spyRegister.bit(19)) o << s << "  Mystery bit 19 true" << std::endl;
  
  o << s << "  SROUT for bot ";
  if(srOut(CrcFeConfigurationData::bot)) o << "true";
  else                                   o << "false"; 
  o << ", for top ";
  if(srOut(CrcFeConfigurationData::top)) o << "true";
  else                                   o << "false"; 
  o << std::endl;    
  
  if(_spyRegister.bit(22)) o << s << "  Mystery bit 22 true" << std::endl;
  if(_spyRegister.bit(23)) o << s << "  Mystery bit 23 true" << std::endl;

  return o;
}

#endif
#endif
