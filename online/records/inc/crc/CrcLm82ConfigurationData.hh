#ifndef CrcLm82ConfigurationData_HH
#define CrcLm82ConfigurationData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class CrcLm82ConfigurationData {

public:
  enum {
    versionNumber=0
  };

  CrcLm82ConfigurationData();
  
  unsigned char configuration() const;
  void          configuration(unsigned char n);
  
  char criticalLimit() const;
  void criticalLimit(char n);
  
  char localLimit() const;
  void localLimit(char n);
  
  char remoteLimit() const;
  void remoteLimit(char n);
    
  bool operator!=(const CrcLm82ConfigurationData &c);
  bool operator==(const CrcLm82ConfigurationData &c);

  std::ostream& print(std::ostream &o, std::string s="") const;
  
  
private:
  UtlPack _limits;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcLm82ConfigurationData::CrcLm82ConfigurationData() {
  memset(this,0,sizeof(CrcLm82ConfigurationData));
  //_limits.word(0x287c7b7a);
  _limits.word(0x3c7c7b7a); // Turn off board crash
}
  
unsigned char CrcLm82ConfigurationData::configuration() const {
  return _limits.byte(3);
}
  
void CrcLm82ConfigurationData::configuration(unsigned char n) {
  _limits.byte(3,n);
}
  
char CrcLm82ConfigurationData::criticalLimit() const {
  return _limits.byte(2);
}
  
void CrcLm82ConfigurationData::criticalLimit(char n) {
  _limits.byte(2,n);
}
  
char CrcLm82ConfigurationData::localLimit() const {
  return _limits.byte(1);
}
  
void CrcLm82ConfigurationData::localLimit(char n) {
  _limits.byte(1,n);
}
  
char CrcLm82ConfigurationData::remoteLimit() const {
  return _limits.byte(0);
}
  
void CrcLm82ConfigurationData::remoteLimit(char n) {
  _limits.byte(0,n);
}
    
bool CrcLm82ConfigurationData::operator!=(const CrcLm82ConfigurationData &c) {
  return _limits!=c._limits;
}

bool CrcLm82ConfigurationData::operator==(const CrcLm82ConfigurationData &c) {
  return !operator!=(c);
}

std::ostream& CrcLm82ConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "CrcLm82ConfigurationData::print()" << std::endl;
  o << s << " Configuration   " << printHex(configuration()) << std::endl;
  o << s << " Critical limit " << std::setw(4) << (int)criticalLimit() << " C" << std::endl;
  o << s << " Local limit    " << std::setw(4) << (int)localLimit() << " C" << std::endl;
  o << s << " Remote limit   " << std::setw(4) << (int)remoteLimit() << " C" << std::endl;
  return o;
}

#endif
#endif
