#ifndef CrcBeTrgPollDataV0_HH
#define CrcBeTrgPollDataV0_HH

#include <string>
#include <iostream>

#include "UtlTime.hh"


class CrcBeTrgPollDataV0 {

public:
  enum {
    versionNumber=0
  };

  CrcBeTrgPollDataV0();

  UtlTime time() const;
  void update();

  unsigned numberOfTries() const;
  unsigned tries() const;
  void tries(unsigned c);
  bool maximumTries() const;
  void maximumTries(bool b);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlTime _time;
  unsigned _tries;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

CrcBeTrgPollDataV0::CrcBeTrgPollDataV0() {
  memset(this,0,sizeof(this));
}

UtlTime CrcBeTrgPollDataV0::time() const {
  return _time;
}

void CrcBeTrgPollDataV0::update() {
  _time.update();
}

unsigned CrcBeTrgPollDataV0::numberOfTries() const {
  return _tries&0x7fffffff;
}

unsigned CrcBeTrgPollDataV0::tries() const {
  return _tries&0x7fffffff;
}

void CrcBeTrgPollDataV0::tries(unsigned c) {
  _tries&=   0x80000000;
  _tries|=(c&0x7fffffff);
}

bool CrcBeTrgPollDataV0::maximumTries() const {
  return (_tries&0x80000000)!=0;
}

void CrcBeTrgPollDataV0::maximumTries(bool b) {
  if(b) _tries|=0x80000000;
  else  _tries&=0x7fffffff;
}

std::ostream& CrcBeTrgPollDataV0::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgPollDataV0::print()" << std::endl;
  o << s << " Time = " << _time << std::endl;
  o << s << " Number of tries = " << tries();
  if(maximumTries()) o << " = maximum";
  o << std::endl;
  return o;
}

#endif
#endif
