#ifndef CrcBeEventDataV0_HH
#define CrcBeEventDataV0_HH

#include <string>
#include <iostream>


class CrcBeEventDataV0 {

public:
  enum {
    versionNumber=0
  };

  CrcBeEventDataV0();

  unsigned status() const;
  void status(unsigned n);

  unsigned signalCatch() const;
  void signalCatch(unsigned n);

  unsigned l1aCounter() const;
  void l1aCounter(unsigned n);

  unsigned bxCounter() const;
  void bxCounter(unsigned n);

  unsigned qdrFrameCounter() const;
  void qdrFrameCounter(unsigned n);

  unsigned qdrDataCounter() const;
  void qdrDataCounter(unsigned n);

  unsigned totalFrameCounter() const;
  void totalFrameCounter(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _status;
  unsigned _signalCatch;
  unsigned _l1aCounter;
  unsigned _bxCounter;
  unsigned _qdrFrameCounter;
  unsigned _qdrDataCounter;
  unsigned _totalFrameCounter;
};


#ifdef CALICE_DAQ_ICC

CrcBeEventDataV0::CrcBeEventDataV0() {
}

unsigned CrcBeEventDataV0::status() const {
  return _status;
}

void CrcBeEventDataV0::status(unsigned n) {
  _status=n;
}

unsigned CrcBeEventDataV0::signalCatch() const {
  return _signalCatch;
}

void CrcBeEventDataV0::signalCatch(unsigned n) {
  _signalCatch=n;
}

unsigned CrcBeEventDataV0::l1aCounter() const {
  return _l1aCounter;
}

void CrcBeEventDataV0::l1aCounter(unsigned n) {
  _l1aCounter=n;
}

unsigned CrcBeEventDataV0::bxCounter() const {
  return _bxCounter;
}

void CrcBeEventDataV0::bxCounter(unsigned n) {
  _bxCounter=n;
}

unsigned CrcBeEventDataV0::qdrFrameCounter() const {
  return _qdrFrameCounter;
}

void CrcBeEventDataV0::qdrFrameCounter(unsigned n) {
  _qdrFrameCounter=n;
}

unsigned CrcBeEventDataV0::qdrDataCounter() const {
  return _qdrDataCounter;
}

void CrcBeEventDataV0::qdrDataCounter(unsigned n) {
  _qdrDataCounter=n;
}

unsigned CrcBeEventDataV0::totalFrameCounter() const {
  return _totalFrameCounter;
}

void CrcBeEventDataV0::totalFrameCounter(unsigned n) {
  _totalFrameCounter=n;
}

std::ostream& CrcBeEventDataV0::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeEventDataV0::print()" << std::endl;
  o << s << " Status       = " << printHex(_status) << std::endl;
  o << s << " Signal catch = " << printHex(_signalCatch) << std::endl;
  o << s << " L1A counter         = " << _l1aCounter << std::endl;
  o << s << " BX counter          = " << _bxCounter << std::endl;
  o << s << " QDR frame counter   = " << _qdrFrameCounter << std::endl;
  o << s << " QDR data counter    = " << _qdrDataCounter << std::endl;
  o << s << " Total frame counter = " << _totalFrameCounter << std::endl;
  return o;
}

#endif
#endif
