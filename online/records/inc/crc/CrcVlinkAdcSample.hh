#ifndef CrcVlinkAdcSample_HH
#define CrcVlinkAdcSample_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class CrcVlinkAdcSample {

public:
  CrcVlinkAdcSample();

  unsigned flags() const;
  void     flags(unsigned n);

  short int adc(unsigned chip) const;
  void      adc(unsigned chip, short int a);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  short int _adc[12];
  UtlPack _flags;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

CrcVlinkAdcSample::CrcVlinkAdcSample() {
  memset(this,0,sizeof(CrcVlinkAdcSample));    
}

unsigned CrcVlinkAdcSample::flags() const {
  return _flags.word();
}

void CrcVlinkAdcSample::flags(unsigned n) {
  _flags.word(n);
}

short int CrcVlinkAdcSample::adc(unsigned chip) const {
  assert(chip<12);
  return _adc[chip];
}

void CrcVlinkAdcSample::adc(unsigned chip, short int a) {
  assert(chip<12);
  _adc[chip]=a;
}

std::ostream& CrcVlinkAdcSample::print(std::ostream &o, std::string s) const {
  o << s << "CrcVlinkAdcSample::print()" << std::endl;

  o << s << " Top ADCs = " << std::setw(7) << _adc[0] << std::setw(7) << _adc[1]
    << std::setw(7) << _adc[2] << std::setw(7) << _adc[3] << std::setw(7) << _adc[4]
    << std::setw(7) << _adc[5] << std::endl;

  o << s << " Bot ADCs = " << std::setw(7) << _adc[6] << std::setw(7) << _adc[7]
    << std::setw(7) << _adc[8] << std::setw(7) << _adc[9] << std::setw(7) << _adc[10]
    << std::setw(7) << _adc[11] << std::endl;

  o << s << " Flags = " << printHex(_flags.word()) << std::endl;

  return o;
}

#endif
#endif
