#ifndef CrcReadoutConfigurationDataV0_HH
#define CrcReadoutConfigurationDataV0_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class CrcReadoutConfigurationDataV0 {

public:
  enum {
    versionNumber=0
  };

  CrcReadoutConfigurationDataV0();

  unsigned vmePeriod() const;
  void vmePeriod(unsigned n);

  unsigned bePeriod() const;
  void bePeriod(unsigned n);

  unsigned fePeriod() const;
  void fePeriod(unsigned n);

  UtlPack mode() const;
  bool beTrgSquirt() const;
  bool vlinkSpill() const;
  bool vlinkBlt() const;
  void beTrgMode(unsigned char n);
  void vlinkMode(unsigned char n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _vmePeriod;
  unsigned _bePeriod;
  unsigned _fePeriod;
  UtlPack _mode;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

CrcReadoutConfigurationDataV0::CrcReadoutConfigurationDataV0() {
  memset(this,0,sizeof(CrcReadoutConfigurationDataV0));
}

unsigned CrcReadoutConfigurationDataV0::vmePeriod() const {
  return _vmePeriod;
}

void CrcReadoutConfigurationDataV0::vmePeriod(unsigned n) {
  _vmePeriod=n;
}

unsigned CrcReadoutConfigurationDataV0::bePeriod() const {
  return _bePeriod;
}

void CrcReadoutConfigurationDataV0::bePeriod(unsigned n) {
  _bePeriod=n;
  }

unsigned CrcReadoutConfigurationDataV0::fePeriod() const {
  return _fePeriod;
}

void CrcReadoutConfigurationDataV0::fePeriod(unsigned n) {
  _fePeriod=n;
}

UtlPack CrcReadoutConfigurationDataV0::mode() const {
  return _mode;
}

bool CrcReadoutConfigurationDataV0::beTrgSquirt() const {
  return _mode.bit( 8+0);
}

bool CrcReadoutConfigurationDataV0::vlinkSpill() const {
  return _mode.bit(24+0);
}

bool CrcReadoutConfigurationDataV0::vlinkBlt() const {
  return _mode.bit(24+1);
}

void CrcReadoutConfigurationDataV0::beTrgMode(unsigned char n) {
  _mode.byte(1,n);
}

void CrcReadoutConfigurationDataV0::vlinkMode(unsigned char n) {
  _mode.byte(3,n);
}

std::ostream& CrcReadoutConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "CrcReadoutConfigurationDataV0::print()" << std::endl;
  o << s << " VME period = " << _vmePeriod << std::endl;
  o << s << " BE period  = " << _bePeriod << std::endl;
  o << s << " FE period  = " << _fePeriod << std::endl;
  
  o << s << " BE mode     = " << printHex(_mode.byte(0)) << std::endl;
  
  o << s << " BE-Trg mode = " << printHex(_mode.byte(1)) << std::endl;
  if(beTrgSquirt()) o << s << "  Be-Trg fifo readout using squirt" << std::endl;
  else              o << s << "  Be-Trg standard fifo readout" << std::endl;
  
  o << s << " FE mode     = " << printHex(_mode.byte(2)) << std::endl;
  
  o << s << " Vlink mode  = " << printHex(_mode.byte(3)) << std::endl;
  if(vlinkSpill()) o << s << "  Vlink readout at end of spill" << std::endl;
  else             o << s << "  Vlink readout per event" << std::endl;
  if(vlinkBlt()) o << s << "  Vlink readout using block transfer" << std::endl;
  else           o << s << "  Vlink readout using standard transfer" << std::endl;
  
  return o;
}

#endif
#endif
