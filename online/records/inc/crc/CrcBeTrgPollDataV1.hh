#ifndef CrcBeTrgPollDataV1_HH
#define CrcBeTrgPollDataV1_HH

#include <string>
#include <iostream>

#include "UtlTime.hh"


class CrcBeTrgPollDataV1 {

public:
  enum {
    versionNumber=1
  };

  CrcBeTrgPollDataV1();

  void update(bool e=true);

  UtlTimeDifference maximumTime() const;
  void              maximumTime(UtlTimeDifference t);

  UtlTime startTime() const;
  UtlTime   endTime() const;
  UtlTime      time() const;

  UtlTimeDifference actualTime() const;

  bool timeout() const;

  unsigned numberOfPolls() const;
  void     numberOfPolls(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlTime _startTime;
  UtlTime _endTime;
  UtlTimeDifference _maximumTime;
  unsigned _numberOfPolls;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

CrcBeTrgPollDataV1::CrcBeTrgPollDataV1() {
  memset(this,0,sizeof(CrcBeTrgPollDataV1));
}

void CrcBeTrgPollDataV1::update(bool e) {
  if(e) _endTime.update();
  else  _startTime.update();
}

UtlTimeDifference CrcBeTrgPollDataV1::maximumTime() const {
  return _maximumTime;
}

void CrcBeTrgPollDataV1::maximumTime(UtlTimeDifference t) {
  _maximumTime=t;
}

UtlTime CrcBeTrgPollDataV1::startTime() const {
  return _startTime;
}

UtlTime CrcBeTrgPollDataV1::endTime() const {
  return _endTime;
}

UtlTime CrcBeTrgPollDataV1::time() const {
  return _endTime;
}

UtlTimeDifference CrcBeTrgPollDataV1::actualTime() const {
  return _endTime-_startTime;
}

bool CrcBeTrgPollDataV1::timeout() const {
  return (actualTime()>_maximumTime);
}

unsigned CrcBeTrgPollDataV1::numberOfPolls() const {
  return _numberOfPolls;
}

void CrcBeTrgPollDataV1::numberOfPolls(unsigned n) {
  _numberOfPolls=n;
}

std::ostream& CrcBeTrgPollDataV1::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgPollDataV1::print()" << std::endl;

  o << s << " Maximum time = " << _maximumTime << " secs" << std::endl;
  o << s << " Start time = " << _startTime << std::endl;
  o << s << " End time   = " << _endTime << std::endl;

  o << s << " Actual time = " << actualTime() << " secs";
  if(timeout()) o << " = timeout";
  o << std::endl;

  o << s << " Number of polls = " << _numberOfPolls << std::endl;

  return o;
}

#endif
#endif
