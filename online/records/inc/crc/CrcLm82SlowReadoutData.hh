#ifndef CrcLm82SlowReadoutData_HH
#define CrcLm82SlowReadoutData_HH

#include <string>
#include <iostream>


class CrcLm82SlowReadoutData {

public:
  enum {
    versionNumber=0
  };

  CrcLm82SlowReadoutData();

  unsigned char status() const;
  void          status(unsigned char n);
    
  char localTemperature() const;
  void localTemperature(char n);
    
  char remoteTemperature() const;
  void remoteTemperature(char n);
  
  std::ostream& print(std::ostream &o, std::string s="") const;
  
  
private:
  unsigned _values;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcLm82SlowReadoutData::CrcLm82SlowReadoutData() {
  memset(this,0,sizeof(CrcLm82SlowReadoutData));
}

unsigned char CrcLm82SlowReadoutData::status() const {
  return (_values&0x00ff0000)>>16;
}
  
void CrcLm82SlowReadoutData::status(unsigned char n) {
  _values&=0xff00ffff;
  _values|=(n<<16);
}
    
char CrcLm82SlowReadoutData::localTemperature() const {
  return (_values&0x0000ff00)>>8;
}
  
void CrcLm82SlowReadoutData::localTemperature(char n) {
  _values&=0xffff00ff;
  _values|=(n<<8);
}
    
char CrcLm82SlowReadoutData::remoteTemperature() const {
  return _values&0x000000ff;
}
  
void CrcLm82SlowReadoutData::remoteTemperature(char n) {
  _values&=0xffffff00;
  _values|=n;
}
  
std::ostream& CrcLm82SlowReadoutData::print(std::ostream &o, std::string s) const {
  o << s << "CrcLm82SlowReadoutData::print()" << std::endl;
  o << s << " Status " << printHex(status()) << std::endl;
  o << s << " Local temperature  " << std::setw(4) << (int)localTemperature()  << " C" << std::endl;
  o << s << " Remote temperature " << std::setw(4) << (int)remoteTemperature() << " C" << std::endl;
  return o;
}
  
#endif
#endif
