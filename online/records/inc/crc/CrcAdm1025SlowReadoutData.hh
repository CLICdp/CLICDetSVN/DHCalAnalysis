#ifndef CrcAdm1025SlowReadoutData_HH
#define CrcAdm1025SlowReadoutData_HH

#include <string>
#include <iostream>

#include "CrcAdm1025Voltages.hh"


class CrcAdm1025SlowReadoutData {

public:
  enum {
    versionNumber=0
  };

  CrcAdm1025SlowReadoutData();

  unsigned short status() const;
  void status(unsigned short n);
  void status1(unsigned char n);
  void status2(unsigned char n);
  
  char localTemperature() const;
  void localTemperature(char n);
  
  char remoteTemperature() const;
  void remoteTemperature(char n);
  
  CrcAdm1025Voltages voltages() const;
  void voltages(CrcAdm1025Voltages v);
  
  std::ostream& print(std::ostream &o, std::string s="") const;
  
  
private:
  unsigned _status;
  CrcAdm1025Voltages _voltages;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcAdm1025SlowReadoutData::CrcAdm1025SlowReadoutData() {
  memset(this,0,sizeof(CrcAdm1025SlowReadoutData));
}

unsigned short CrcAdm1025SlowReadoutData::status() const {
  return (_status&0xffff0000)>>16;
}

void CrcAdm1025SlowReadoutData::status(unsigned short n) {
  _status&=0x0000ffff;
  _status|=(n<<16);
}
  
void CrcAdm1025SlowReadoutData::status1(unsigned char n) {
  _status&=0xff00ffff;
  _status|=(n<<16);
}

void CrcAdm1025SlowReadoutData::status2(unsigned char n) {
  _status&=0x00ffffff;
  _status|=(n<<24);
}

char CrcAdm1025SlowReadoutData::localTemperature() const {
  return (_status&0x0000ff00)>>8;
}

void CrcAdm1025SlowReadoutData::localTemperature(char n) {
  _status&=0xffff00ff;
  _status|=(n<<8);
  }

char CrcAdm1025SlowReadoutData::remoteTemperature() const {
  return _status&0x000000ff;
}

void CrcAdm1025SlowReadoutData::remoteTemperature(char n) {
  _status&=0xffffff00;
  _status|=n&0xff;
}

CrcAdm1025Voltages CrcAdm1025SlowReadoutData::voltages() const {
  return _voltages;
  }

void CrcAdm1025SlowReadoutData::voltages(CrcAdm1025Voltages v) {
  _voltages=v;
}

std::ostream& CrcAdm1025SlowReadoutData::print(std::ostream &o, std::string s) const {
  o << s << "CrcAdm1025SlowReadoutData::print()" << std::endl;
  o << s << " Status " << printHex(status()) << std::endl;
  o << s << " Local temperature  " << std::setw(4) << (int)localTemperature() << " C" << std::endl;
  o << s << " Remote temperature " << std::setw(4) << (int)remoteTemperature() << " C" << std::endl;
  _voltages.print(o,s+" ");
  return o;
}

#endif
#endif
