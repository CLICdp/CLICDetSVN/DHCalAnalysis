#ifndef CrcBeTrgConfigurationDataV0_HH
#define CrcBeTrgConfigurationDataV0_HH

#include <string>
#include <iostream>


class CrcBeTrgConfigurationDataV0 {

public:
  enum {
    versionNumber=0
  };

  CrcBeTrgConfigurationDataV0();

  unsigned inputEnable() const;
  void     inputEnable(unsigned n);

  unsigned outputEnable() const;
  void     outputEnable(unsigned n);
  unsigned generalEnable() const;
  void     generalEnable(unsigned n);

  unsigned oscillationPeriod() const;
  void     oscillationPeriod(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _inputEnable;
  unsigned _outputEnable;
  unsigned _oscillationPeriod;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

CrcBeTrgConfigurationDataV0::CrcBeTrgConfigurationDataV0() {
  memset(this,0,sizeof(CrcBeTrgConfigurationDataV0));
  _outputEnable=0x1;
}

unsigned CrcBeTrgConfigurationDataV0::inputEnable() const {
  return _inputEnable;
}

void CrcBeTrgConfigurationDataV0::inputEnable(unsigned n) {
  _inputEnable=n;
}

unsigned CrcBeTrgConfigurationDataV0::outputEnable() const {
  return _outputEnable;
}

void CrcBeTrgConfigurationDataV0::outputEnable(unsigned n) {
  _outputEnable=n;
}

unsigned CrcBeTrgConfigurationDataV0::generalEnable() const {
  return _outputEnable;
}

void CrcBeTrgConfigurationDataV0::generalEnable(unsigned n) {
  _outputEnable=n;
}

unsigned CrcBeTrgConfigurationDataV0::oscillationPeriod() const {
  return _oscillationPeriod;
}

void CrcBeTrgConfigurationDataV0::oscillationPeriod(unsigned n) {
  _oscillationPeriod=n;
}

std::ostream& CrcBeTrgConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgConfigurationDataV0::print()" << std::endl;
  o << s << " Input enable  = " << printHex(_inputEnable) << std::endl;
  o << s << " Output enable = " << printHex(_outputEnable) << std::endl;
  o << s << " Oscillation period = " << _oscillationPeriod << " x 25ns = "
    << 0.000000025*_oscillationPeriod << " s" << std::endl;
  return o;
}

#endif
#endif
