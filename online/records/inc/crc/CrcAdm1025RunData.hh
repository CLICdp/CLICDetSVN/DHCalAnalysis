#ifndef CrcAdm1025RunData_HH
#define CrcAdm1025RunData_HH

#include <string>
#include <iostream>

class CrcAdm1025RunData {

public:
  enum {
    versionNumber=0
  };

  CrcAdm1025RunData();
  
  unsigned char manufacturerId() const;
  void manufacturerId(unsigned char n);

  unsigned char steppingCode() const;
  void steppingCode(unsigned char n);

  unsigned char vid() const;
  void vid(unsigned char n);

  std::ostream& print(std::ostream &o, std::string s="") const;
  
  
private:
  unsigned _general;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcAdm1025RunData::CrcAdm1025RunData() {
  memset(this,0,sizeof(CrcAdm1025RunData));
}

unsigned char CrcAdm1025RunData::manufacturerId() const {
  return _general&0x000000ff;
}

void CrcAdm1025RunData::manufacturerId(unsigned char n) {
  _general&=0xffffff00;
  _general|=n;
}

unsigned char CrcAdm1025RunData::steppingCode() const {
  return (_general&0x0000ff00)>>8;
}

void CrcAdm1025RunData::steppingCode(unsigned char n) {
  _general&=0xffff00ff;
  _general|=(n<<8);
}

unsigned char CrcAdm1025RunData::vid() const {
  return (_general&0x00ff0000)>>16;
}

void CrcAdm1025RunData::vid(unsigned char n) {
  _general&=0xff00ffff;
  _general|=(n<<16);
}

std::ostream& CrcAdm1025RunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcAdm1025RunData::print()" << std::endl;
  o << s << " Manufacturer id " << printHex(manufacturerId()) << std::endl;
  o << s << " Stepping code   " << printHex(steppingCode()) << std::endl;
  o << s << " VID bits        " << printHex(vid()) << std::endl;
  return o;
}

#endif
#endif
