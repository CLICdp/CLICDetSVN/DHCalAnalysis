#ifndef CrcBeTrgRunData_HH
#define CrcBeTrgRunData_HH

#include <string>
#include <iostream>


class CrcBeTrgRunData {

public:
  enum {
    versionNumber=0
  };

  CrcBeTrgRunData();

  unsigned firmwareId() const;
  void     firmwareId(unsigned n);

  unsigned firmwareDate() const;
  void     firmwareDate(unsigned n);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _firmwareId;
  unsigned _firmwareDate;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>

#include "UtlPrintHex.hh"

CrcBeTrgRunData::CrcBeTrgRunData() {
  memset(this,0,sizeof(CrcBeTrgRunData));
}

unsigned CrcBeTrgRunData::firmwareId() const {
  return _firmwareId;
}

void CrcBeTrgRunData::firmwareId(unsigned n) {
  _firmwareId=n;
}

unsigned CrcBeTrgRunData::firmwareDate() const {
  return _firmwareDate;
}

void CrcBeTrgRunData::firmwareDate(unsigned n) {
  _firmwareDate=n;
}

std::ostream& CrcBeTrgRunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgRunData::print()" << std::endl;
  o << s << " Firmware id   = " << printHex(_firmwareId) << std::endl;
  time_t fd(_firmwareDate);
  o << s << " Firmware date = " << std::setw(11) << _firmwareDate << " = " 
    << ctime(&fd); // endl built into ctime!
  return o;
}

#endif
#endif
