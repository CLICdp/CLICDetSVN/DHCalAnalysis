#ifndef CrcLocationData_HH
#define CrcLocationData_HH

#include "CrcLocation.hh"

template <class Data> class CrcLocationData : public CrcLocation {

public:
  CrcLocationData();
  CrcLocationData(CrcLocation l);
  CrcLocationData(CrcLocation l, const Data &d);
  
  CrcLocation location() const;
  void location(CrcLocation l);

  const Data* data() const;
  Data*       data();
  void        data(Data &p);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  Data _data;
};


template <class Data> 
CrcLocationData<Data>::CrcLocationData() : CrcLocation(), _data() {
}
  
template <class Data> 
CrcLocationData<Data>::CrcLocationData(CrcLocation l) :
  CrcLocation(l), _data() {
}
  
template <class Data> 
CrcLocationData<Data>::CrcLocationData(CrcLocation l, const Data &d) :
  CrcLocation(l), _data(d) {
}
  
template <class Data> 
CrcLocation CrcLocationData<Data>::location() const {
  return *((CrcLocation*)this);
}

template <class Data> 
void CrcLocationData<Data>::location(CrcLocation l) {
  *((CrcLocation*)this)=l;
}

template <class Data> 
const Data* CrcLocationData<Data>::data() const {
  return &_data;
}

template <class Data> 
Data* CrcLocationData<Data>::data() {
  return &_data;
}

template <class Data> 
void CrcLocationData<Data>::data(Data &p) {
  _data=p;
}

template <class Data> 
std::ostream& CrcLocationData<Data>::print(std::ostream &o, std::string s) const {
  o << s << "CrcLocationData::print()" << std::endl;
  CrcLocation::print(o,s+" ");
  _data.print(o,s+" ");
  return o;
}

#ifdef CALICE_DAQ_ICC
#endif
#endif
