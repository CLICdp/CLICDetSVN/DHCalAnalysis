#ifndef CrcEpromMessage_HH
#define CrcEpromMessage_HH

#include <string>
#include <iostream>


class CrcEpromMessage {

public:
  CrcEpromMessage();

  unsigned numberOfWords() const;

  unsigned numberOfBytes() const;
  void     numberOfBytes(unsigned b);

  unsigned time() const;
  void     time(unsigned t);
  
  std::string message() const;

  unsigned* data();
  
  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _time;
  unsigned _numberOfBytes;
};


#ifdef CALICE_DAQ_ICC

CrcEpromMessage::CrcEpromMessage() : _time(0), _numberOfBytes(0) {
}

unsigned CrcEpromMessage::numberOfWords() const {
  return (_numberOfBytes+3)/4;
}

unsigned CrcEpromMessage::numberOfBytes() const {
  return _numberOfBytes;
}

void CrcEpromMessage::numberOfBytes(unsigned b) {
  _numberOfBytes=b;
}

unsigned CrcEpromMessage::time() const {
  return _time;
}

void CrcEpromMessage::time(unsigned t) {
  _time=t;
}

std::string CrcEpromMessage::message() const {
  std::string s;
  const unsigned char *c((const unsigned char*)(&_numberOfBytes+1));
  for(unsigned i(0);i<_numberOfBytes;i++) {
    s+=c[i]; //strcpy?
  }
  return s;
}

unsigned* CrcEpromMessage::data() {
  return &_numberOfBytes+1;
}

std::ostream& CrcEpromMessage::print(std::ostream &o, std::string s) const {
  o << s << "CrcEpromMessage::print()" << std::endl;

  time_t ts(_time);
  o << s << " Timestamp = " << _time << " = "
    << ctime(&ts); // endl built into ctime!
  o << s << " Number of bytes = " << _numberOfBytes << std::endl;
  o << s << message() << std::endl;
  return o;
}

#endif
#endif
