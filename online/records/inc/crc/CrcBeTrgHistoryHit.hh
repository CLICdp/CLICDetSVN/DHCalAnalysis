#ifndef CrcBeTrgHistoryHit_HH
#define CrcBeTrgHistoryHit_HH

#include <string>
#include <iostream>


class CrcBeTrgHistoryHit {

public:
  CrcBeTrgHistoryHit();

  unsigned startTime() const;
  void     startTime(unsigned s);

  unsigned endTime() const;
  void     endTime(unsigned e);

  unsigned length() const;
  void     length(unsigned l);

  bool overlap(const CrcBeTrgHistoryHit &h) const;

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned _startTime;
  unsigned _length;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>


CrcBeTrgHistoryHit::CrcBeTrgHistoryHit() {
  memset(this,0,sizeof(CrcBeTrgHistoryHit));
}

unsigned CrcBeTrgHistoryHit::startTime() const {
  return _startTime;
}

void CrcBeTrgHistoryHit::startTime(unsigned s) {
  _startTime=s;
}

unsigned CrcBeTrgHistoryHit::endTime() const {
  return _startTime+_length;
}

void CrcBeTrgHistoryHit::endTime(unsigned e) {
  if(e<_startTime) _length=0;
  else             _length=e-_startTime;
}

unsigned CrcBeTrgHistoryHit::length() const {
  return _length;
}

void CrcBeTrgHistoryHit::length(unsigned l) {
  _length=l;
}

bool CrcBeTrgHistoryHit::overlap(const CrcBeTrgHistoryHit &h) const {
  return !((startTime()>h.endTime()) || (endTime()<h.startTime()));
}

std::ostream& CrcBeTrgHistoryHit::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgHistoryHit::print()" << std::endl;
  o << s << " Start time = " << _startTime << std::endl;
  o << s << " End time   = " << endTime() << std::endl;
  o << s << " Length     = " << _length << std::endl;
  return o;
}

#endif
#endif
