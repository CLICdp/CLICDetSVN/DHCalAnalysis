#ifndef CrcBeTrgEventDataTwoOOFour_HH
#define CrcBeTrgEventDataTwoOOFour_HH

#include <string>
#include <iostream>
#include <cassert>

class CrcBeTrgEventData2004 {

public:
  enum {
    versionNumber=0
  };

  enum {
    maxFifoLength=256,
    maxWordLength=maxFifoLength+8
  };

  CrcBeTrgEventData2004();

  unsigned inputStatus() const;
  void inputStatus(unsigned n);

  unsigned inputCatch() const;
  void inputCatch(unsigned n);

  unsigned triggerBusyStatus() const;
  void triggerBusyStatus(unsigned n);

  unsigned initialFifoStatus() const;
  void initialFifoStatus(unsigned n);

  unsigned finalFifoStatus() const;
  void finalFifoStatus(unsigned n);

  unsigned triggerCounter() const;
  void triggerCounter(unsigned n);

  unsigned prebusyTriggerCounter() const;
  void prebusyTriggerCounter(unsigned n);

  unsigned numberOfFifoWords() const;
  void numberOfFifoWords(unsigned n);

  const unsigned* fifoData() const;
  unsigned* fifoData();

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _inputStatus;
  unsigned _inputCatch;
  unsigned _triggerBusyStatus;
  unsigned _prebusyTriggerCounter;
  unsigned _triggerCounter;
  unsigned _initialFifoStatus;
  unsigned _finalFifoStatus;
  unsigned _numberOfFifoWords;
};


//#ifdef CALICE_DAQ_ICC

#include <cstring>

CrcBeTrgEventData2004::CrcBeTrgEventData2004() {
  memset(this,0,sizeof(CrcBeTrgEventData2004));
}

unsigned CrcBeTrgEventData2004::inputStatus() const {
  return _inputStatus;
}

void CrcBeTrgEventData2004::inputStatus(unsigned n) {
  _inputStatus=n;
}

unsigned CrcBeTrgEventData2004::inputCatch() const {
  return _inputCatch;
}

void CrcBeTrgEventData2004::inputCatch(unsigned n) {
  _inputCatch=n;
}

unsigned CrcBeTrgEventData2004::triggerBusyStatus() const {
  return _triggerBusyStatus;
}

void CrcBeTrgEventData2004::triggerBusyStatus(unsigned n) {
  _triggerBusyStatus=n;
}

unsigned CrcBeTrgEventData2004::initialFifoStatus() const {
  return _initialFifoStatus;
}

void CrcBeTrgEventData2004::initialFifoStatus(unsigned n) {
  _initialFifoStatus=n;
}

unsigned CrcBeTrgEventData2004::finalFifoStatus() const {
  return _finalFifoStatus;
}

void CrcBeTrgEventData2004::finalFifoStatus(unsigned n) {
  _finalFifoStatus=n;
}

unsigned CrcBeTrgEventData2004::triggerCounter() const {
  return _triggerCounter;
}

void CrcBeTrgEventData2004::triggerCounter(unsigned n) {
  _triggerCounter=n;
}

unsigned CrcBeTrgEventData2004::prebusyTriggerCounter() const {
  return _prebusyTriggerCounter;
}

void CrcBeTrgEventData2004::prebusyTriggerCounter(unsigned n) {
  _prebusyTriggerCounter=n;
}

unsigned CrcBeTrgEventData2004::numberOfFifoWords() const {
  return _numberOfFifoWords;
}

void CrcBeTrgEventData2004::numberOfFifoWords(unsigned n) {
  assert(n<=maxFifoLength);
  _numberOfFifoWords=n;
}

const unsigned* CrcBeTrgEventData2004::fifoData() const {
  return &_numberOfFifoWords+1;
}

unsigned* CrcBeTrgEventData2004::fifoData() {
  return &_numberOfFifoWords+1;
}

std::ostream& CrcBeTrgEventData2004::print(std::ostream &o, std::string s) const {
  o << s << "CrcBeTrgEventData2004::print()" << std::endl;
  o << s << " Prebusy trigger counter  = " << std::setw(10) << _prebusyTriggerCounter << std::endl;
  o << s << " Trigger counter          = " << std::setw(10) << _triggerCounter << std::endl;
  o << s << " Input status             = " << printHex(_inputStatus) << std::endl;
  o << s << " Input catch              = " << printHex(_inputCatch) << std::endl;
  o << s << " Trigger busy status      = " << printHex(_triggerBusyStatus) << std::endl;
  o << s << " Initial fifo status      = " << printHex(_initialFifoStatus) << std::endl;
  o << s << " Final fifo Status        = " << printHex(_finalFifoStatus) << std::endl;
  o << s << " Number of fifo words     = " << _numberOfFifoWords << std::endl;
  
  const unsigned *p(fifoData());
  for(unsigned i(0);i<_numberOfFifoWords;i++) {
    o << s << "  Fifo word " << std::setw(4) << i << " = " << printHex(p[i]) << std::endl;
  }
  
  return o;
}

//#endif
#endif
