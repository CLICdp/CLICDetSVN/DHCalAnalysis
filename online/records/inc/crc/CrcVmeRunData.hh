#ifndef CrcVmeRunData_HH
#define CrcVmeRunData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

#include "CrcEpromMessage.hh"


class CrcVmeRunData {

public:
  enum {
    versionNumber=0
  };

  CrcVmeRunData();

  unsigned firmwareId() const;
  void     firmwareId(unsigned c);

  unsigned clockSelect() const;
  void     clockSelect(unsigned c);

  unsigned char serialNumber() const;
  void          serialNumber(unsigned char c);

  unsigned epromHeader() const;
  void     epromHeader(unsigned i, unsigned char c);

  unsigned char* data();

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _firmwareId;
  unsigned _clockSelect;
  UtlPack _epromHeader;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"

CrcVmeRunData::CrcVmeRunData() {
  memset(this,0,sizeof(CrcVmeRunData));
}

unsigned CrcVmeRunData::firmwareId() const {
  return _firmwareId;
}

void CrcVmeRunData::firmwareId(unsigned c) {
  _firmwareId=c;
}

unsigned CrcVmeRunData::clockSelect() const {
  return _clockSelect;
}

void CrcVmeRunData::clockSelect(unsigned c) {
  _clockSelect=c;
}

unsigned char CrcVmeRunData::serialNumber() const {
  return _epromHeader.byte(0);
}

void CrcVmeRunData::serialNumber(unsigned char c) {
  _epromHeader.byte(0,c);
}

unsigned CrcVmeRunData::epromHeader() const {
  return _epromHeader.word();
}

void CrcVmeRunData::epromHeader(unsigned i, unsigned char c) {
  _epromHeader.byte(i,c);
}

unsigned char* CrcVmeRunData::data() {
  if(_epromHeader.halfWord(1)==0) return 0;
  return (unsigned char*)(&_epromHeader+1);
}

std::ostream& CrcVmeRunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcVmeRunData::print()" << std::endl;
  o << s << " Firmware id  = " << printHex(_firmwareId) << std::endl;
  o << s << " Clock select = " << printHex(_clockSelect) << std::endl;
  o << s << " Eprom header = " << printHex(_epromHeader.word()) << std::endl;

  if(_epromHeader.halfWord(1)>0) {
    const CrcEpromMessage *m((const CrcEpromMessage*)(&_epromHeader+1));
    m->print(o,s+" ");
  }

  return o;
}

#endif
#endif
