#ifndef CrcVmeEventData_HH
#define CrcVmeEventData_HH

#include <string>
#include <iostream>


class CrcVmeEventData {

public:
  enum {
    versionNumber=0
  };

  CrcVmeEventData();

  unsigned status() const;
  void     status(unsigned c);

  unsigned ttcCounter() const;
  void     ttcCounter(unsigned c);

  unsigned backplaneCounter() const;
  void     backplaneCounter(unsigned c);

  std::ostream& print(std::ostream &o, std::string s="") const;

private:
  unsigned _status;
  unsigned _ttcCounter;
  unsigned _backplaneCounter;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


CrcVmeEventData::CrcVmeEventData() {
  memset(this,0,sizeof(CrcVmeEventData));
}

unsigned CrcVmeEventData::status() const {
  return _status;
}

void CrcVmeEventData::status(unsigned c) {
  _status=c;
}

unsigned CrcVmeEventData::ttcCounter() const {
  return _ttcCounter;
}

void CrcVmeEventData::ttcCounter(unsigned c) {
  _ttcCounter=c;
}

unsigned CrcVmeEventData::backplaneCounter() const {
  return _backplaneCounter;
}

void CrcVmeEventData::backplaneCounter(unsigned c) {
  _backplaneCounter=c;
}

std::ostream& CrcVmeEventData::print(std::ostream &o, std::string s) const {
  o << s << "CrcVmeEventData::print()" << std::endl;
  o << s << " Status  = " << printHex(_status) << std::endl;
  o << s << " TTC Counter       = " << _ttcCounter << std::endl;
  o << s << " Backplane counter = " << _backplaneCounter << std::endl;
  return o;
}

#endif
#endif
