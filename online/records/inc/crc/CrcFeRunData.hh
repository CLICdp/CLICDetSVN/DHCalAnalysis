#ifndef CrcFeRunData_HH
#define CrcFeRunData_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

#include "CrcFeConfigurationData.hh"


class CrcFeRunData {

public:
  enum {
    versionNumber=0
  };

  enum VfeType {
    unconnected=0,
    ecalRight=9,
    ecalLeft=10,
    ecalFull=11,
    ahcalRight=13,
    ahcalLeft=14,
    unknown=15
  };

  CrcFeRunData();

  unsigned firmwareId() const;
  void     firmwareId(unsigned c);

  UtlPack vfeType() const;
  VfeType  vfeType(CrcFeConfigurationData::Connector c) const;
  void     vfeType(UtlPack c);

  unsigned linkArray() const;
  void     linkArray(unsigned c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _firmwareId;
  unsigned _linkArray;
  //unsigned _vfeType;
  UtlPack _vfeType;
};


#ifdef CALICE_DAQ_ICC

CrcFeRunData::CrcFeRunData() {
}

unsigned CrcFeRunData::firmwareId() const {
  return _firmwareId;
}

void CrcFeRunData::firmwareId(unsigned c) {
  _firmwareId=c;
}

UtlPack CrcFeRunData::vfeType() const {
  return _vfeType;
}

CrcFeRunData::VfeType CrcFeRunData::vfeType(CrcFeConfigurationData::Connector c) const {
  unsigned char type(_vfeType.halfByte(0));
  if(c==CrcFeConfigurationData::top) type=_vfeType.halfByte(1);

  if(type== 0) return unconnected;
  if(type== 9) return ecalRight;
  if(type==10) return ecalLeft;
  if(type==11) return ecalFull;
  if(type==13) return ahcalRight;
  if(type==14) return ahcalLeft;
  
  return unknown;
}

void CrcFeRunData::vfeType(UtlPack c) {
  _vfeType=c;
}

unsigned CrcFeRunData::linkArray() const {
  return _linkArray;
}

void CrcFeRunData::linkArray(unsigned c) {
  _linkArray=c;
}

std::ostream& CrcFeRunData::print(std::ostream &o, std::string s) const {
  o << s << "CrcFeRunData::print()" << std::endl;
  o << s << " Firmware Id = " << printHex(firmwareId()) << std::endl;
  o << s << " Link array  = " << printHex(linkArray()) << std::endl;
  o << s << " VFE type    = " << printHex(vfeType()) << std::endl;

  o << s << "   VFE type bot/A = " << vfeType(CrcFeConfigurationData::bot) << " = ";
  if(vfeType(CrcFeConfigurationData::bot)==unconnected) o << "unconnected";
  if(vfeType(CrcFeConfigurationData::bot)==ecalRight)   o << "ECAL right ";
  if(vfeType(CrcFeConfigurationData::bot)==ecalLeft)    o << "ECAL left  ";
  if(vfeType(CrcFeConfigurationData::bot)==ecalFull)    o << "ECAL full  ";
  if(vfeType(CrcFeConfigurationData::bot)==ahcalRight)  o << "AHCAL right";
  if(vfeType(CrcFeConfigurationData::bot)==ahcalLeft)   o << "AHCAL left ";
  if(vfeType(CrcFeConfigurationData::bot)==unknown)     o << "unknown    ";

  o << ",   VFE type top/B = " << vfeType(CrcFeConfigurationData::top) << " = ";
  if(vfeType(CrcFeConfigurationData::top)==unconnected) o << "unconnected" << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==ecalRight)   o << "ECAL right " << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==ecalLeft)    o << "ECAL left  " << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==ecalFull)    o << "ECAL full  " << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==ahcalRight)  o << "AHCAL right" << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==ahcalLeft)   o << "AHCAL left " << std::endl;
  if(vfeType(CrcFeConfigurationData::top)==unknown)     o << "unknown    " << std::endl;
  
  return o;
}

#endif
#endif
