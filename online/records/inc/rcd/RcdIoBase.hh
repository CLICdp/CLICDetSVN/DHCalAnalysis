#ifndef RcdIoBase_HH
#define RcdIoBase_HH

#include <string>
#include <iostream>


class RcdIoBase {

public:
  RcdIoBase();
  virtual ~RcdIoBase();

  virtual bool open(const std::string &file);
  virtual bool close();

  bool isOpen() const;
  void printLevel(unsigned p);
  unsigned numberOfBytes() const;


protected:
  virtual void reset();

  bool _isOpen;
  unsigned _printLevel;
  std::string _fileName;
  unsigned _numberOfBytes;
};


#ifdef CALICE_DAQ_ICC


RcdIoBase::RcdIoBase() : _isOpen(false), _printLevel(1), _numberOfBytes(0) {
}

RcdIoBase::~RcdIoBase() {
}

bool RcdIoBase::open(const std::string &file) {
  reset();

  _fileName=file;
  if(_printLevel>0) std::cout << "RcdIoBase::open()   Request to open file "
			      << _fileName << std::endl;
  _isOpen=true;

  return true;
}

bool RcdIoBase::close() {
  if(_printLevel>0) std::cout << "RcdIoBase::close()  Request to close file "
			      << _fileName << std::endl;
  reset();

  return true;
}

bool RcdIoBase::isOpen() const {
  return _isOpen;
}

void RcdIoBase::printLevel(unsigned p) {
  _printLevel=p;
}

unsigned RcdIoBase::numberOfBytes() const {
  return _numberOfBytes;
}

void RcdIoBase::reset() {
  _numberOfBytes=0;
  _fileName="";
  _isOpen=false;
}

#endif
#endif
