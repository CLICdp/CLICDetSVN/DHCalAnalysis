#ifndef RcdWriter_HH
#define RcdWriter_HH

#include <string>
#include <iostream>

#include "RcdIoBase.hh"

class RcdRecord;


class RcdWriter : public RcdIoBase {

public:
  RcdWriter();
  virtual ~RcdWriter();

  virtual bool write(const RcdRecord&);
};


#ifdef CALICE_DAQ_ICC

#include "RcdRecord.hh"


RcdWriter::RcdWriter() {
}
 
RcdWriter::~RcdWriter() {
}

bool RcdWriter::write(const RcdRecord&) {
  return true;
}
 
#endif
#endif
