#ifndef RcdReader_HH
#define RcdReader_HH

#include <string>
#include <iostream>

#include "RcdIoBase.hh"

class RcdRecord;


class RcdReader : public RcdIoBase {

public:
  RcdReader();
  virtual ~RcdReader();

  virtual bool read(RcdRecord&);
};


#ifdef CALICE_DAQ_ICC

#include "RcdRecord.hh"


RcdReader::RcdReader() {
}

RcdReader::~RcdReader() {
}

bool RcdReader::read(RcdRecord&) {
  return false;
}

#endif
#endif
