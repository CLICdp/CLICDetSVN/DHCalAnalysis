#ifndef RcdArena_HH
#define RcdArena_HH

#include "RcdArray.hh"

#ifndef CALICE_DAQ_SIZE
//#define CALICE_DAQ_SIZE 64*1024 // For standard ECAL+AHCAL running
//#define CALICE_DAQ_SIZE 512*1024 // For trigger tests 14/07/06
//#define CALICE_DAQ_SIZE 256*1024 // For 4xMAPS
#define CALICE_DAQ_SIZE 32*1024*1024 // For simulation
#endif

typedef RcdArray<CALICE_DAQ_SIZE> RcdArena;
//typedef RcdArray<2048*CALICE_DAQ_SIZE> RcdArenas;

#endif
