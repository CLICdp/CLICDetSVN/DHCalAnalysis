#ifndef RcdArray_HH
#define RcdArray_HH

#include <cstring>

#include "RcdRecord.hh"


template <unsigned TotalNumberOfBytes> class RcdArray : public RcdRecord {

public:
  RcdArray();
  RcdArray(const RcdHeader &h);
  RcdArray(const RcdRecord &b);
  RcdArray(const RcdArray<TotalNumberOfBytes> &a);

  unsigned arraySizeInBytes() const;
  bool overflow() const;

private:
  char _array[TotalNumberOfBytes-sizeof(RcdRecord)];
};


template <unsigned TotalNumberOfBytes>
RcdArray<TotalNumberOfBytes>::RcdArray() :
  RcdRecord() {
}

template <unsigned TotalNumberOfBytes>
RcdArray<TotalNumberOfBytes>::RcdArray(const RcdHeader &h) :
  RcdRecord(h) {
}

template <unsigned TotalNumberOfBytes>
RcdArray<TotalNumberOfBytes>::RcdArray(const RcdRecord &b) :
  RcdRecord(b) {
}

template <unsigned TotalNumberOfBytes>
RcdArray<TotalNumberOfBytes>::RcdArray(const RcdArray<TotalNumberOfBytes> &a) :
  RcdRecord() {
  memcpy(this,&a,a.totalNumberOfBytes());
}

template <unsigned TotalNumberOfBytes>
unsigned RcdArray<TotalNumberOfBytes>::arraySizeInBytes() const {
  return TotalNumberOfBytes;
}

template <unsigned TotalNumberOfBytes>
bool RcdArray<TotalNumberOfBytes>::overflow() const {
  return this->totalNumberOfBytes()>TotalNumberOfBytes;
}

#ifdef CALICE_DAQ_ICC
#endif
#endif
