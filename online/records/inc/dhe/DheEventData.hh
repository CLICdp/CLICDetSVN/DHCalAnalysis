#ifndef DheEventData_HH
#define DheEventData_HH

#include <string>
#include <iostream>


class DheEventData {

public:
  enum {
    versionNumber=0
  };

  DheEventData();

  unsigned numberOfHits() const;
  void     numberOfHits(unsigned n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _numberOfHits;
};


#ifdef CALICE_DAQ_ICC


DheEventData::DheEventData() : _numberOfHits(0) {
}

unsigned DheEventData::numberOfHits() const {
  return _numberOfHits;
}
 
void DheEventData::numberOfHits(unsigned n) {
  _numberOfHits=n;
}
 
std::ostream& DheEventData::print(std::ostream &o, std::string s) const {
  o << s << "DheEventData::print()" << std::endl;

  o << s << " Number of hits = " << _numberOfHits << std::endl;

  const UtlPack *p((const UtlPack*)(this+1));
  for(unsigned i(0);i<_numberOfHits;i++) {
    o << s << "  Hit " << std::setw(6) << " = " << printHex(p[i]) << std::endl;
  }
  
  return o;
}

#endif
#endif
