#ifndef DheDifConfigurationData_HH
#define DheDifConfigurationData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class DheDifConfigurationData {

public:
  enum {
    versionNumber=0
  };

  DheDifConfigurationData();

  bool write() const;
  void write(bool w);

  bool readoutCommandEnable() const;
  void readoutCommandEnable(bool e);

  bool skipEventReadout() const;
  void skipEventReadout(bool s);

  unsigned triggerLimit() const;
  void     triggerLimit(unsigned r);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _label;
  unsigned _triggerLimit;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


DheDifConfigurationData::DheDifConfigurationData() {
  memset(this,0,sizeof(DheDifConfigurationData));
  _triggerLimit=0xc8;
}

bool DheDifConfigurationData::write() const {
  return _label.bit(0);
}

void DheDifConfigurationData::write(bool w) {
  return _label.bit(0,w);
}

bool DheDifConfigurationData::readoutCommandEnable() const {
  return _label.bit(1);
}

void DheDifConfigurationData::readoutCommandEnable(bool e) {
  return _label.bit(1,e);
}

bool DheDifConfigurationData::skipEventReadout() const {
  return _label.bit(2);
}

void DheDifConfigurationData::skipEventReadout(bool s) {
  return _label.bit(2,s);
}

unsigned DheDifConfigurationData::triggerLimit() const {
  return _triggerLimit;
}

void DheDifConfigurationData::triggerLimit(unsigned r) {
  _triggerLimit=r;
}

std::ostream& DheDifConfigurationData::print(std::ostream &o, std::string s) const {
  o << s << "DheDifConfigurationData::print()" << std::endl;

  o << s << " Label            = " << printHex(_label) << std::endl;
  if(write()) o << s << "  Write data" << std::endl;
  else        o << s << "  Read data" << std::endl;
  if(readoutCommandEnable()) o << s << "  Readout commands enabled" << std::endl;
  else                       o << s << "  Readout commands disabled" << std::endl;
  if(skipEventReadout()) o << s << "  Event readout skipped" << std::endl;
  else                   o << s << "  Event readout active" << std::endl;

  o << s << " Trigger limit = " << printHex(_triggerLimit) << std::endl;
  return o;
}

#endif
#endif
