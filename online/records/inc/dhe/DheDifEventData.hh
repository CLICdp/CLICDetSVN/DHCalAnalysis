#ifndef DheDifEventData_HH
#define DheDifEventData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class DheDifEventData {

public:
  enum {
    versionNumber=0
  };

  DheDifEventData();

  unsigned triggerCounter() const;
  void     triggerCounter(unsigned r);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _triggerCounter;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


DheDifEventData::DheDifEventData() {
  memset(this,0,sizeof(DheDifEventData));
}

unsigned DheDifEventData::triggerCounter() const {
  return _triggerCounter;
}

void DheDifEventData::triggerCounter(unsigned r) {
  _triggerCounter=r;
}

std::ostream& DheDifEventData::print(std::ostream &o, std::string s) const {
  o << s << "DheDifEventData::print()" << std::endl;
  o << s << " Trigger counter = " << printHex(_triggerCounter) << std::endl;
  return o;
}

#endif
#endif
