#ifndef DheDifRunData_HH
#define DheDifRunData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class DheDifRunData {

public:
  enum {
    versionNumber=0
  };

  DheDifRunData();

  unsigned fixedRegister() const;
  void     fixedRegister(unsigned r);

  unsigned idRegister() const;
  void     idRegister(unsigned r);

  unsigned testRegister() const;
  void     testRegister(unsigned r);

  unsigned controlRegister() const;
  void     controlRegister(unsigned r);

  unsigned triggerCounter() const;
  void     triggerCounter(unsigned r);

  unsigned runRegister() const;
  void     runRegister(unsigned r);

  unsigned firmwareVersion() const;
  void     firmwareVersion(unsigned r);

  const unsigned* bufferData() const;
  void            bufferData(unsigned n, const unsigned *p);

  bool verifyBufferData() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _fixedRegister;
  unsigned _idRegister;
  unsigned _testRegister;
  unsigned _controlRegister;
  unsigned _triggerCounter;
  unsigned _runRegister;
  unsigned _firmwareVersion;
  unsigned _bufferData[4];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


DheDifRunData::DheDifRunData() {
  memset(this,0,sizeof(DheDifRunData));
}

unsigned DheDifRunData::fixedRegister() const {
  return _fixedRegister;
}

void DheDifRunData::fixedRegister(unsigned r) {
  _fixedRegister=r;
}

unsigned DheDifRunData::idRegister() const {
  return _idRegister;
}

void DheDifRunData::idRegister(unsigned r) {
  _idRegister=r;
}

unsigned DheDifRunData::testRegister() const {
  return _testRegister;
}

void DheDifRunData::testRegister(unsigned r) {
  _testRegister=r;
}

unsigned DheDifRunData::controlRegister() const {
  return _controlRegister;
}

void DheDifRunData::controlRegister(unsigned r) {
  _controlRegister=r;
}

unsigned DheDifRunData::triggerCounter() const {
  return _triggerCounter;
}

void DheDifRunData::triggerCounter(unsigned r) {
  _triggerCounter=r;
}

unsigned DheDifRunData::runRegister() const {
  return _runRegister;
}

void DheDifRunData::runRegister(unsigned r) {
  _runRegister=r;
}

unsigned DheDifRunData::firmwareVersion() const {
  return _firmwareVersion;
}

void DheDifRunData::firmwareVersion(unsigned r) {
  _firmwareVersion=r;
}

const unsigned* DheDifRunData::bufferData() const {
  return _bufferData;
}

void DheDifRunData::bufferData(unsigned n, const unsigned *p) {
  for(unsigned i(0);i<4;i++) {
    if(i<n) _bufferData[i]=p[i];
    else    _bufferData[i]=0xdeaddead;
  }
}

bool DheDifRunData::verifyBufferData() const {
  return
    _bufferData[0]==0xfffffffa   &&
    _bufferData[1]==_runRegister &&
    _bufferData[2]==0xfffffffb   &&
    _bufferData[3]==0xffffffff;
}

std::ostream& DheDifRunData::print(std::ostream &o, std::string s) const {
  o << s << "DheDifRunData::print()" << std::endl;

  o << s << " Fixed register     = " << printHex(_fixedRegister) << std::endl;
  o << s << " ID register        = " << printHex(_idRegister) << std::endl;
  o << s << " Test register      = " << printHex(_testRegister) << std::endl;
  o << s << " Control register   = " << printHex(_controlRegister) << std::endl;
  o << s << " Trigger counter    = " << printHex(_triggerCounter) << std::endl;
  o << s << " Run register       = " << printHex(_runRegister) << std::endl;
  o << s << " Firmware version   = " << printHex(_firmwareVersion) << std::endl;

  o << s << " Buffer data word 0 = " << printHex(_bufferData[0]) << std::endl;
  o << s << " Buffer data word 1 = " << printHex(_bufferData[1]) << std::endl;
  o << s << " Buffer data word 2 = " << printHex(_bufferData[2]) << std::endl;
  o << s << " Buffer data word 3 = " << printHex(_bufferData[3]) << std::endl;
  if(verifyBufferData()) o << s << "  Buffer data pass verification" << std::endl;
  else                   o << s << "  Buffer data fail verification" << std::endl;

  return o;
}

#endif
#endif
