#ifndef DheDifAcquisitionData_HH
#define DheDifAcquisitionData_HH

#include <iostream>
#include <fstream>

#include "UtlPack.hh"


class DheDifAcquisitionData {

public:
  enum {
    versionNumber=0
  };

  DheDifAcquisitionData();

  unsigned triggerCounter() const;
  void     triggerCounter(unsigned r);

  unsigned numberOfDataBufferWords() const;
  void     numberOfDataBufferWords(unsigned r);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _triggerCounter;
  unsigned _numberOfDataBufferWords;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


DheDifAcquisitionData::DheDifAcquisitionData() {
  memset(this,0,sizeof(DheDifAcquisitionData));
}

unsigned DheDifAcquisitionData::triggerCounter() const {
  return _triggerCounter;
}

void DheDifAcquisitionData::triggerCounter(unsigned r) {
  _triggerCounter=r;
}

unsigned DheDifAcquisitionData::numberOfDataBufferWords() const {
  return _numberOfDataBufferWords;
}

void DheDifAcquisitionData::numberOfDataBufferWords(unsigned r) {
  _numberOfDataBufferWords=r;
}

std::ostream& DheDifAcquisitionData::print(std::ostream &o, std::string s) const {
  o << s << "DheDifAcquisitionData::print()" << std::endl;

  o << s << " Trigger counter register    = " << printHex(_triggerCounter) << std::endl;
  o << s << " Number of data buffer words = " << _numberOfDataBufferWords << std::endl;
  return o;
}

#endif
#endif
