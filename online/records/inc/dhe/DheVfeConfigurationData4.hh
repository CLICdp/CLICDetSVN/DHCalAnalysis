#ifndef DheVfeConfigurationData4_HH
#define DheVfeConfigurationData4_HH

#include <string>
#include <iostream>

#include "DheVfeSlowData.hh"

class DheVfeConfigurationData4 {

public:
  enum {
    versionNumber=0
  };

  DheVfeConfigurationData4();

  UtlPack verificationData() const;
  void    verificationData(UtlPack v);

  DheVfeSlowData slowData(unsigned asic) const;
  void           slowData(unsigned asic, DheVfeSlowData s);

  bool operator!=(const DheVfeConfigurationData4 &c);
  bool operator==(const DheVfeConfigurationData4 &c);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _verificationData;
  DheVfeSlowData _slowData[4];
};


#ifdef CALICE_DAQ_ICC


DheVfeConfigurationData4::DheVfeConfigurationData4() : _verificationData(0xaaaa5555) {
}

UtlPack DheVfeConfigurationData4::verificationData() const {
  return _verificationData;
}
 
void DheVfeConfigurationData4::verificationData(UtlPack v) {
  _verificationData=v;
}
 
DheVfeSlowData DheVfeConfigurationData4::slowData(unsigned asic) const {
  assert(asic<4);
  return _slowData[asic];
}

void DheVfeConfigurationData4::slowData(unsigned asic, DheVfeSlowData s) {
  assert(asic<4);
  _slowData[asic]=s;
}

bool DheVfeConfigurationData4::operator!=(const DheVfeConfigurationData4 &c) {
  unsigned *a((unsigned*)this);
  unsigned *b((unsigned*)&c);
  for(unsigned i(0);i<sizeof(DheVfeConfigurationData4)/4;i++) {
    if(a[i]!=b[i]) return true;
  }
  return false;
}

bool DheVfeConfigurationData4::operator==(const DheVfeConfigurationData4 &c) {
  return !operator!=(c);
}

std::ostream& DheVfeConfigurationData4::print(std::ostream &o, std::string s) const {
  o << s << "DheVfeConfigurationData4::print()" << std::endl;

  o << s << " Verification data = " << printHex(_verificationData) << std::endl;

  for(unsigned i(0);i<4;i++) {
    o << s << " ASIC " << std::setw(2) << i << " ";
    _slowData[i].print(o,s+" ");
  }
  
  return o;
}

#endif
#endif
