#ifndef DheTriggerData_HH
#define DheTriggerData_HH

#include <string>
#include <iostream>


class DheTriggerData {

public:
  enum {
    versionNumber=0
  };

  DheTriggerData();

  unsigned triggerNumber() const;
  void     triggerNumber(unsigned n);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _triggerNumber;
};


#ifdef CALICE_DAQ_ICC


DheTriggerData::DheTriggerData() : _triggerNumber(999) {
}

unsigned DheTriggerData::triggerNumber() const {
  return _triggerNumber;
}
 
void DheTriggerData::triggerNumber(unsigned t) {
  _triggerNumber=t;
}
 
std::ostream& DheTriggerData::print(std::ostream &o, std::string s) const {
  o << s << "DheTriggerData::print()" << std::endl;

  o << s << " Trigger number = " << _triggerNumber << std::endl;
  
  return o;
}

#endif
#endif
