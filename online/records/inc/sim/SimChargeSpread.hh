#ifndef SimChargeSpread_HH
#define SimChargeSpread_HH

#include <iostream>
#include <string>


class SimChargeSpread {

public:
  SimChargeSpread();

  double fraction(int i, int j) const;
  void   fraction(int i, int j, double f);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _fraction[5][5];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"


SimChargeSpread::SimChargeSpread() {
  memset(this,0,sizeof(SimChargeSpread));
  fraction(0,0,1.0);
}

double SimChargeSpread::fraction(int i, int j) const {
  assert(abs(i)<=2);
  assert(abs(j)<=2);
  return _fraction[i+2][j+2];
}

void SimChargeSpread::fraction(int i, int j, double f) {
  assert(abs(i)<=2);
  assert(abs(j)<=2);
  assert(f>=0.0 && f<=1.0);
  _fraction[i+2][j+2]=f;
}

std::ostream& SimChargeSpread::print(std::ostream &o, std::string s) const {
  o << s << "SimChargeSpread::print()" << std::endl;

  o << s << " Fractions" << std::endl;
  for(int i(-2);i<=2;i++) {
    o << s;
    for(int j(-2);j<=2;j++) {
      o << " " << std::setw(10) << fraction(i,j);
    }
    o << std::endl;
  }

  return o;
}

#endif
#endif
