#ifndef SimParticle_HH
#define SimParticle_HH

#include <iostream>
#include <string>
#include <stdint.h>

#include "SimDetectorBoxId.hh"


class SimParticle {

public:
  enum {
    versionNumber=0
  };

  SimParticle();

  int  particleId() const;
  void particleId(int p);

  bool primary() const;
  void setPrimary();

  SimDetectorBoxId::Detector boundary() const;
  bool inward() const;
  void boundary(SimDetectorBoxId::Detector d, bool i);

  // GEANT4 units = MeV
  double mass() const;
  void   mass(double m);

  double px() const;
  void   px(double px);
  double py() const;
  void   py(double py);
  double pz() const;
  void   pz(double pz);

  double p() const;
  double energy() const;
  double kineticEnergy() const;

  // GEANT4 units = mm
  double x() const;
  void   x(double x);
  double y() const;
  void   y(double y);
  double z() const;
  void   z(double z);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  int32_t _particleId;
  int32_t _boundary;

  double _mass;
  double _momentum[3];
  double _position[3];
};


#ifdef CALICE_DAQ_ICC

#include <iomanip>
#include <cmath>

#include "UtlPrintHex.hh"


SimParticle::SimParticle() {
  memset(this,0,sizeof(SimParticle));
}

bool SimParticle::primary() const {
  return _boundary==0;
}

void SimParticle::setPrimary() {
  _boundary=0;
}

SimDetectorBoxId::Detector SimParticle::boundary() const {
  return (SimDetectorBoxId::Detector)abs(_boundary);
}

bool SimParticle::inward() const {
  return _boundary>=0;
}

void SimParticle::boundary(SimDetectorBoxId::Detector d, bool i) {
  if(i) _boundary= d;
  else  _boundary=-d;
}

int SimParticle::particleId() const {
  return _particleId;
}

void SimParticle::particleId(int p) {
  _particleId=p;
}

double SimParticle::mass() const {
  return _mass;
}

void SimParticle::mass(double m) {
  assert(m>=0.0);
  _mass=m;
}

double SimParticle::x() const {
  return _position[0];
}

void SimParticle::x(double x) {
  _position[0]=x;
}

double SimParticle::y() const {
  return _position[1];
}

void SimParticle::y(double y) {
  _position[1]=y;
}

double SimParticle::z() const {
  return _position[2];
}

void SimParticle::z(double z) {
  _position[2]=z;
}

double SimParticle::px() const {
  return _momentum[0];
}

void SimParticle::px(double px) {
  _momentum[0]=px;
}

double SimParticle::py() const {
  return _momentum[1];
}

void SimParticle::py(double py) {
  _momentum[1]=py;
}

double SimParticle::pz() const {
  return _momentum[2];
}

void SimParticle::pz(double pz) {
  _momentum[2]=pz;
}

double SimParticle::p() const {
  return sqrt(px()*px()+py()*py()+pz()*pz());
}

double SimParticle::energy() const {
  return sqrt(px()*px()+py()*py()+pz()*pz()+mass()*mass());
}

double SimParticle::kineticEnergy() const {
  return energy()-mass();
}

std::ostream& SimParticle::print(std::ostream &o, std::string s) const {
  o << s << "SimParticle::print()" << std::endl;

  o << s << " Particle Id = " << std::setw(10) << _particleId;

  std::string pname("unknown");
  if(_particleId==   22) pname="photon";
  if(_particleId==  -11) pname="positron";
  if(_particleId==   11) pname="electron";
  if(_particleId==  -13) pname="muon+";
  if(_particleId==   13) pname="muon-";
  if(_particleId==  211) pname="pion+";
  if(_particleId== -211) pname="pion-";
  if(_particleId== 2112) pname="neutron";
  if(_particleId==-2112) pname="anti-neutron";
  if(_particleId== 2212) pname="proton";
  if(_particleId==-2212) pname="anti-proton";
  o << " = " << pname << std::endl;

  o << s << " Boundary = " << std::setw(10) << _boundary << std::endl;
  if(primary()) {
    o << s << "  Primary" << std::endl;
  } else {
    o << s << "  Detector " << boundary() << " = "
      << SimDetectorBoxId::detectorName(boundary());
    if(inward()) o << ", inward"  << std::endl;
    else         o << ", outward" << std::endl;
  }

  o << s << " Momentum = " << std::setw(10) << px() << ", "
    << std::setw(10) << py() << ", " << std::setw(10) << pz() << " MeV/c" << std::endl;
  o << s << " Mass = " << std::setw(10) << mass()
    << " MeV/c2, energy = " << std::setw(10) << energy() << " MeV,"
    << " kinetic energy = " << std::setw(10) << kineticEnergy() << " MeV" << std::endl;
  o << s << " Position = " << std::setw(10) << x() << ", "
    << std::setw(10) << y() << ", " << std::setw(10) << z() << " mm" << std::endl;

  return o;
}

#endif
#endif
