#ifndef SimBoxTrack_HH
#define SimBoxTrack_HH

#include <iostream>
#include <string>

#include "SimDetectorBoxId.hh"

class SimStep;

class SimBoxTrack : public SimDetectorBoxId {

public:
  enum {
    versionNumber=0
  };

  SimBoxTrack();

  int32_t particleId() const;
  void    particleId(int32_t p);

  bool entry() const;
  void entry(bool e);

  bool exit() const;
  void exit(bool e);

  // GEANT4 units = mm
  double xStart() const;
  void   xStart(double x);
  double yStart() const;
  void   yStart(double y);
  double zStart() const;
  void   zStart(double z);

  double xEnd() const;
  //void   xEnd(double x);
  double yEnd() const;
  //void   yEnd(double y);
  double zEnd() const;
  //void   zEnd(double z);

  double totalLength() const;
  double totalEnergy() const;
  double xAverage() const;
  double yAverage() const;
  double zAverage() const;

  unsigned numberOfSteps() const;
  const SimStep* steps() const;
  const SimStep* step(unsigned n) const;
  void     addStep(const SimStep &s);
  SimStep* addStep();

  unsigned numberOfExtraBytes() const;
  bool overflow() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  int32_t  _particleId;
  uint32_t _numberOfSteps;

  double _startPosition[3];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"

#include "SimStep.hh"


SimBoxTrack::SimBoxTrack() {
  memset(this,0,sizeof(SimBoxTrack));
}

int SimBoxTrack::particleId() const {
  return _particleId;
}

void SimBoxTrack::particleId(int p) {
  _particleId=p;
}

bool SimBoxTrack::entry() const {
  return _label.bit(16);
}

void SimBoxTrack::entry(bool b) {
  _label.bit(16,b);
}

bool SimBoxTrack::exit() const {
  return _label.bit(17);
}

void SimBoxTrack::exit(bool b) {
  _label.bit(17,b);
}

double SimBoxTrack::xStart() const {
  return _startPosition[0];
}

void SimBoxTrack::xStart(double x) {
  _startPosition[0]=x;
}

double SimBoxTrack::yStart() const {
  return _startPosition[1];
}

void SimBoxTrack::yStart(double y) {
  _startPosition[1]=y;
}

double SimBoxTrack::zStart() const {
  return _startPosition[2];
}

void SimBoxTrack::zStart(double z) {
  _startPosition[2]=z;
}

double SimBoxTrack::xEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[0];
  } else {
    return step(_numberOfSteps-1)->xEnd();
  }
}
/*
void SimBoxTrack::xEnd(double x) {
  _exitPosition[0]=x;
}
*/
double SimBoxTrack::yEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[1];
  } else {
    return step(_numberOfSteps-1)->yEnd();
  }
}
/*
void SimBoxTrack::yEnd(double y) {
  _exitPosition[1]=y;
}
*/
double SimBoxTrack::zEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[2];
  } else {
    return step(_numberOfSteps-1)->zEnd();
  }
}
/*
void SimBoxTrack::zEnd(double z) {
  _exitPosition[2]=z;
}
*/
double SimBoxTrack::totalLength() const {
  double l(0.0);
  for(unsigned i(0);i<_numberOfSteps;i++) {
    l+=step(i)->length();
  }
  return l;
}

double SimBoxTrack::totalEnergy() const {
  double e(0.0);
  for(unsigned i(0);i<_numberOfSteps;i++) {
    e+=step(i)->energy();
  }
  return e;
}

double SimBoxTrack::xAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double x(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      x+=step(i)->length()*step(i)->x();
    }
    return x/l;

  } else {
    return step(0)->x();
  }
}

double SimBoxTrack::yAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double y(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      y+=step(i)->length()*step(i)->y();
    }
    return y/l;

  } else {
    return step(0)->y();
  }
}

double SimBoxTrack::zAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double z(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      z+=step(i)->length()*step(i)->z();
    }
    return z/l;

  } else {
    return step(0)->z();
  }
}

unsigned SimBoxTrack::numberOfSteps() const {
  return _numberOfSteps;
}

const SimStep* SimBoxTrack::steps() const {
  return (const SimStep*)(this+1);
}

const SimStep* SimBoxTrack::step(unsigned n) const {
  if(n<_numberOfSteps) return ((const SimStep*)(this+1))+n;
  else return 0;
}

void SimBoxTrack::addStep(const SimStep &s) {
  if(!overflow()) {
    *(((SimStep*)(this+1))+_numberOfSteps)=s;
    _numberOfSteps++;
  }
}

SimStep* SimBoxTrack::addStep() {
  if(overflow()) return 0;
  return ((SimStep*)(this+1))+(_numberOfSteps++);
}

unsigned SimBoxTrack::numberOfExtraBytes() const {
  return _numberOfSteps*sizeof(SimStep);
}

bool SimBoxTrack::overflow() const {
  return (sizeof(SimBoxTrack)+(_numberOfSteps+1)*sizeof(SimStep))>=65536;
}

std::ostream& SimBoxTrack::print(std::ostream &o, std::string s) const {
  o << s << "SimBoxTrack::print()" << std::endl;

  o << s << " Particle Id = " << std::setw(10) << _particleId << std::endl;

  SimDetectorBoxId::print(o,s+" ");
  if(entry() && exit())   o << s << "   Entry and exit from box" << std::endl;
  if(entry() && !exit())  o << s << "   Entry only, stopped in box" << std::endl;
  if(!entry() && exit())  o << s << "   Exit only, started in box" << std::endl;
  if(!entry() && !exit()) o << s << "   Contained within box" << std::endl;

  o << s << " Start position = "
    << std::setw(10) << xStart() << ", "
    << std::setw(10) << yStart() << ", "
    << std::setw(10) << zStart() << " mm" << std::endl;
  
  o << s << " End   position = "
    << std::setw(10) << xEnd() << ", "
    << std::setw(10) << yEnd() << ", "
    << std::setw(10) << zEnd() << " mm" << std::endl;

  o << s << " Total length = " << std::setw(10) << totalLength()
    << " mm, energy = " << std::setw(10) << totalEnergy() << " MeV" << std::endl;
  o << s << " Average x,y,z = " << std::setw(10) << xAverage()
    << " mm, " << std::setw(10) << yAverage() << " mm"
    << std::setw(10) << zAverage() << " mm" << std::endl;

  o << s << " Number of steps = " << _numberOfSteps;
  if(overflow()) o << " = overflow limit";
  o << std::endl;

  for(unsigned i(0);i<_numberOfSteps;i++) {
    step(i)->print(o,s+" ");
  }

  return o;
}

#endif
#endif
