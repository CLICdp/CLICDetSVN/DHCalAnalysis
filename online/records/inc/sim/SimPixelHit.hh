#ifndef SimPixelHit_HH
#define SimPixelHit_HH

#include <iostream>
#include <string>
#include <stdint.h>


class SimPixelHit {

public:
  SimPixelHit();
  SimPixelHit(int16_t i, int16_t j);

  int  i() const;
  void i(int16_t ii);

  int  j() const;
  void j(int16_t jj);

  bool operator!=(const SimPixelHit& that) const;
  bool operator==(const SimPixelHit& that) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  int16_t _i;
  int16_t _j;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


SimPixelHit::SimPixelHit() {
}

SimPixelHit::SimPixelHit(int16_t i, int16_t j) : _i(i), _j(j) {
}

int SimPixelHit::i() const {
  return _i;
}

void SimPixelHit::i(int16_t ii) {
  _i=ii;
}

int SimPixelHit::j() const {
  return _j;
}

void SimPixelHit::j(int16_t jj) {
  _j=jj;
}

bool SimPixelHit::operator==(const SimPixelHit& that) const {
  if(_i!=that._i) return false;
  if(_j!=that._j) return false;
  return true;
}

bool SimPixelHit::operator!=(const SimPixelHit& that) const {
  return !((*this)==that);
}

std::ostream& SimPixelHit::print(std::ostream &o, std::string s) const {
  o << s << "SimPixelHit::print() Pixel i,j " 
    << std::setw(6) << _i << ", " << std::setw(6) << _j << std::endl;
  return o;
}

#endif
#endif
