#ifndef SimDetectorData_HH
#define SimDetectorData_HH

#include <iostream>
#include <string>

#include "SimDetectorBox.hh"


class SimDetectorData : public SimDetectorBox {

public:
  enum {
    versionNumber=0
  };

  SimDetectorData();
  
  void resize(double e=0.1);

  unsigned numberOfBoxes() const;
  const SimDetectorBox* boxes() const;
  const SimDetectorBox* box(unsigned n) const;
  void addBox(const SimDetectorBox &l);

  unsigned numberOfExtraBytes() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  uint32_t _numberOfBoxes;
  uint32_t _spare;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>

#include <iomanip>
#include <cstring>
#include <cmath>

#include "UtlPrintHex.hh"


SimDetectorData::SimDetectorData() {
  memset(this,0,sizeof(SimDetectorData));
}

void SimDetectorData::resize(double e) {
  double extreme[3][2]={{1.0e12,-1.0e12},
			{1.0e12,-1.0e12},
			{1.0e12,-1.0e12}};

  const SimDetectorBox *sdb(boxes());
  for(unsigned i(0);i<_numberOfBoxes;i++) {
    if(sdb[i].xRight() <extreme[0][0]) extreme[0][0]=sdb[i].xRight();
    if(sdb[i].xLeft()  >extreme[0][1]) extreme[0][1]=sdb[i].xLeft();
    if(sdb[i].yBottom()<extreme[1][0]) extreme[1][0]=sdb[i].yBottom();
    if(sdb[i].yTop()   >extreme[1][1]) extreme[1][1]=sdb[i].yTop();
    if(sdb[i].zFront() <extreme[2][0]) extreme[2][0]=sdb[i].zFront();
    if(sdb[i].zBack()  >extreme[2][1]) extreme[2][1]=sdb[i].zBack();
  }

  xCentre(  0.5*(extreme[0][1]+extreme[0][0]));
  xHalfSize(0.5*(extreme[0][1]-extreme[0][0])+e);
  yCentre(  0.5*(extreme[1][1]+extreme[1][0]));
  yHalfSize(0.5*(extreme[1][1]-extreme[1][0])+e);
  zCentre(  0.5*(extreme[2][1]+extreme[2][0]));
  zHalfSize(0.5*(extreme[2][1]-extreme[2][0])+e);
}

unsigned SimDetectorData::numberOfBoxes() const {
  return _numberOfBoxes;
}

const SimDetectorBox* SimDetectorData::boxes() const {
  return (const SimDetectorBox*)(this+1);
}

const SimDetectorBox* SimDetectorData::box(unsigned n) const {
  if(n<_numberOfBoxes) return ((const SimDetectorBox*)(this+1))+n;
  else return 0;
}

void SimDetectorData::addBox(const SimDetectorBox &l) {
  *(((SimDetectorBox*)(this+1))+_numberOfBoxes)=l;
  _numberOfBoxes++;
}

unsigned SimDetectorData::numberOfExtraBytes() const {
  return _numberOfBoxes*sizeof(SimDetectorBox);
}

std::ostream& SimDetectorData::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorData::print()" << std::endl;

  SimDetectorBox::print(o,s+" ");

  o << s << " Number of contained boxes " << _numberOfBoxes << std::endl;
  for(unsigned i(0);i<_numberOfBoxes;i++) {
    box(i)->print(o,s+"  ");
  }

  return o;
}

#endif
#endif
