#ifndef SimDetectorEnergy_HH
#define SimDetectorEnergy_HH

#include <iostream>
#include <string>

#include "SimDetectorBoxId.hh"


class SimDetectorEnergy : public SimDetectorBoxId {

public:
  SimDetectorEnergy();

  // GEANT4 units = MeV
  double energy() const;
  void   energy(double e);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _energy;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"


SimDetectorEnergy::SimDetectorEnergy() {
  memset(this,0,sizeof(SimDetectorEnergy));
}

double SimDetectorEnergy::energy() const {
  return _energy;
}

void SimDetectorEnergy::energy(double e) {
  _energy=e;
}

std::ostream& SimDetectorEnergy::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorEnergy::print()" << std::endl;
  SimDetectorBoxId::print(o,s+" ");
  o << s << " Energy = " << std::setw(10) << energy() << std::endl;

  return o;
}

#endif
#endif
