#ifndef SimDetectorEnergies_HH
#define SimDetectorEnergies_HH

#include <iostream>
#include <string>

#include "SimDetectorEnergy.hh"


class SimDetectorEnergies : public SimDetectorBoxId {

public:
  enum {
    versionNumber=0
  };

  SimDetectorEnergies();
  
  double detectorEnergy() const;
  void   detectorEnergy(double e);

  void   addDetectorEnergy(double e);

  double totalEnergy() const;
  double totalEnergy(SimDetectorBoxId::Type t) const;

  unsigned numberOfBoxes() const;
  const SimDetectorEnergy* boxEnergies() const;
  const SimDetectorEnergy* boxEnergy(unsigned n) const;
  void addEnergy(const SimDetectorEnergy &e);

  unsigned numberOfExtraBytes() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  uint32_t _numberOfBoxes;
  uint32_t _spare;
  double _detectorEnergy;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>

#include <iomanip>
#include <cstring>
#include <cmath>

#include "UtlPrintHex.hh"


SimDetectorEnergies::SimDetectorEnergies() {
  memset(this,0,sizeof(SimDetectorEnergies));
}

double SimDetectorEnergies::detectorEnergy() const {
  return _detectorEnergy;
}

void SimDetectorEnergies::detectorEnergy(double e) {
  _detectorEnergy=e;
}

void SimDetectorEnergies::addDetectorEnergy(double e) {
  _detectorEnergy+=e;
}

double SimDetectorEnergies::totalEnergy() const {
  double e(_detectorEnergy);
  for(unsigned i(0);i<_numberOfBoxes;i++) {
    e+=boxEnergy(i)->energy();
  }
  return e;
}

double SimDetectorEnergies::totalEnergy(SimDetectorBoxId::Type t) const {
  if(t==type()) return _detectorEnergy;
  double e(0.0);
  for(unsigned i(0);i<_numberOfBoxes;i++) {
    if(boxEnergy(i)->type()==t) {
      e+=boxEnergy(i)->energy();
    }
  }
  return e;
}

unsigned SimDetectorEnergies::numberOfBoxes() const {
  return _numberOfBoxes;
}

const SimDetectorEnergy* SimDetectorEnergies::boxEnergies() const {
  return (const SimDetectorEnergy*)(this+1);
}

const SimDetectorEnergy* SimDetectorEnergies::boxEnergy(unsigned n) const {
  if(n<_numberOfBoxes) return ((const SimDetectorEnergy*)(this+1))+n;
  else return 0;
}

void SimDetectorEnergies::addEnergy(const SimDetectorEnergy &e) {
  *(((SimDetectorEnergy*)(this+1))+_numberOfBoxes)=e;
  _numberOfBoxes++;
}

unsigned SimDetectorEnergies::numberOfExtraBytes() const {
  return _numberOfBoxes*sizeof(SimDetectorEnergy);
}

std::ostream& SimDetectorEnergies::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorEnergies::print()" << std::endl;

  o << s << " Detector energy = " << std::setw(10) << detectorEnergy() << " MeV" << std::endl;
  o << s << " Total energy = " << std::setw(10) << totalEnergy() << " MeV" << std::endl;
  for(int i(0);i<100;i++) {
    double e(totalEnergy((SimDetectorBoxId::Type)(type()+i)));
    if(e>0.0) {
      o << s << " Total energy for type " 
	<< SimDetectorBoxId::typeName((SimDetectorBoxId::Type)(type()+i))
	<< " = " << std::setw(10) << e << " MeV" << std::endl;
    }
  }

  o << s << " Number of box energies " << _numberOfBoxes << std::endl;
  for(unsigned i(0);i<_numberOfBoxes;i++) {
    boxEnergy(i)->print(o,s+" ");
  }

  return o;
}

#endif
#endif
