#ifndef SimPrimaryData_HH
#define SimPrimaryData_HH

#include <iostream>
#include <string>


class SimPrimaryData : public SimParticle {

public:
  enum {
    versionNumber=0
  };

  SimPrimaryData();

  double smearMatrix(unsigned i, unsigned j) const;
  void   smearMatrix(unsigned i, unsigned j, double e);

  const double* smearMatrix() const;
  void          smearMatrix(const double *p);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  static unsigned matrixElement(unsigned i, unsigned j);

  double _smearMatrix[21];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>

#include <iomanip>
#include <cstring>
#include <cmath>

#include "UtlPrintHex.hh"


SimPrimaryData::SimPrimaryData() {
  memset(this,0,sizeof(SimPrimaryData));
}

unsigned SimPrimaryData::matrixElement(unsigned i, unsigned j) {
  assert(i<5);
  assert(j<5);

  unsigned ii(i);
  unsigned jj(j);
  if(i<j) {
    ii=j;
    jj=i;
  }

  unsigned n(0);
  for(unsigned k(0);k<5;k++) {
    for(unsigned l(k);l<5;l++) {
      if(ii==l && jj==k) return n;
      n++;
    }
  }

  assert(false);
}

double SimPrimaryData::smearMatrix(unsigned i, unsigned j) const {
  return _smearMatrix[matrixElement(i,j)];
}

void SimPrimaryData::smearMatrix(unsigned i, unsigned j, double e) {
  _smearMatrix[matrixElement(i,j)]=e;
}

const double* SimPrimaryData::smearMatrix() const {
  return _smearMatrix;
}

void SimPrimaryData::smearMatrix(const double *p) {
  for(unsigned i(0);i<21;i++) {
    _smearMatrix[i]=p[i];
  }
}

std::ostream& SimPrimaryData::print(std::ostream &o, std::string s) const {
  o << s << "SimPrimaryData::print()" << std::endl;

  SimParticle::print(o,s+" ");

  o << s << " Smearing matrix" << std::endl;
  for(unsigned i(0);i<5;i++) {
    o << s;
    for(unsigned j(0);j<5;j++) {
      o << std::setw(10) << smearMatrix(i,j);
    }
    o << std::endl;
  }

  return o;
}

#endif
#endif
