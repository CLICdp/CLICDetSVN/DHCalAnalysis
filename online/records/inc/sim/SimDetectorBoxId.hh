#ifndef SimDetectorBoxId_HH
#define SimDetectorBoxId_HH

#include <iostream>
#include <string>

#include "UtlPack.hh"


class SimDetectorBoxId {

public:
  enum Detector {
    hall,
    vertex,
    tracker,
    ecal,
    hcal,
    trigger,
    outside,
    endOfDetectorEnum
  };

  enum Type {
    hallVolume   =0x10000*hall,
    endOfHallTypes,

    vertexVolume =0x10000*vertex,
    endOfVertexTypes,

    trackerVolume=0x10000*tracker,
    trackerSiSensor,
    trackerScintillator,
    trackerMechanics,
    fortisCase,
  	fortisSensor,
    endOfTrackerTypes,

    ecalVolume   =0x10000*ecal,
    ecalConverter,
    ecalSiBulk,
    ecalSiEpitaxial,
    ecalPcb,
    ecalMechanics,
    endOfEcalTypes,

    hcalVolume   =0x10000*hcal,
    endOfHcalTypes,

    triggerVolume=0x10000*trigger,
    triggerScintillator,
    triggerMechanics,
    endOfTriggerTypes,

    outsideVolume=0x10000*outside,
    endOfOutsideTypes
  };

  SimDetectorBoxId();

  Detector detector() const;

  std::string        detectorName() const;
  static std::string detectorName(Detector d);

  bool detectorVolume() const;

  Type type() const;
  void type(Type t);

  std::string        typeName() const;
  static std::string typeName(Type t);

  uint16_t layer() const;
  void     layer(uint16_t l);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


protected:
  UtlPack _label;

private:
  Type _type;

  static const std::string _detectorName[endOfDetectorEnum];

  static const std::string _hallTypeName[endOfHallTypes-hallVolume];
  static const std::string _vertexTypeName[endOfVertexTypes-vertexVolume];
  static const std::string _trackerTypeName[endOfTrackerTypes-trackerVolume];
  static const std::string _ecalTypeName[endOfEcalTypes-ecalVolume];
  static const std::string _hcalTypeName[endOfHcalTypes-hcalVolume];
  static const std::string _triggerTypeName[endOfTriggerTypes-triggerVolume];
  static const std::string _outsideTypeName[endOfOutsideTypes-outsideVolume];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


SimDetectorBoxId::SimDetectorBoxId() {
}

SimDetectorBoxId::Detector SimDetectorBoxId::detector() const {
  return (Detector)(_type/0x10000);
}

std::string SimDetectorBoxId::detectorName() const {
  return detectorName(detector());
}

bool SimDetectorBoxId::detectorVolume() const {
  return _type==hallVolume
    || _type==vertexVolume
    || _type==trackerVolume
    || _type==ecalVolume
    || _type==hcalVolume
    || _type==triggerVolume
    || _type==outsideVolume;
}

std::string SimDetectorBoxId::detectorName(Detector d) {
  if(d<endOfDetectorEnum) return _detectorName[d];
  else                    return "unknown";
}

SimDetectorBoxId::Type SimDetectorBoxId::type() const {
  return _type;
}

void SimDetectorBoxId::type(Type t) {
  _type=t;
}

std::string SimDetectorBoxId::typeName() const {
  return typeName(_type);
}

std::string SimDetectorBoxId::typeName(Type t) {
  if(t>=hallVolume && t<endOfHallTypes) {
    return _hallTypeName[t-hallVolume];
  }

  if(t>=vertexVolume && t<endOfVertexTypes) {
    return _vertexTypeName[t-vertexVolume];
  }

  if(t>=trackerVolume && t<endOfTrackerTypes) {
    return _trackerTypeName[t-trackerVolume];
  }

  if(t>=ecalVolume && t<endOfEcalTypes) {
    return _ecalTypeName[t-ecalVolume];
  }

  if(t>=hcalVolume && t<endOfHcalTypes) {
    return _hcalTypeName[t-hcalVolume];
  }

  if(t>=triggerVolume && t<endOfTriggerTypes) {
    return _triggerTypeName[t-triggerVolume];
  }

  if(t>=outsideVolume && t<endOfOutsideTypes) {
    return _outsideTypeName[t-outsideVolume];
  }

  return "unknown";
}

uint16_t SimDetectorBoxId::layer() const {
  return _label.halfWord(0);
}

void SimDetectorBoxId::layer(uint16_t l) {
  _label.halfWord(0,l);
}

std::ostream& SimDetectorBoxId::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorBoxId::print()" << std::endl;

  o << s << " Detector = " << detector() << " = " << detectorName()
    << ", type = " << _type << " = " << typeName() << std::endl;
  o << s << " Label = " << printHex(_label) << std::endl;
  o << s << "  Layer = " << std::setw(6) << layer() << std::endl;

  return o;
}

const std::string SimDetectorBoxId::_detectorName[]={
  "Hall",
  "Vertex",
  "Tracker",
  "Ecal",
  "Hcal",
  "Trigger",
  "Outside"
};

const std::string SimDetectorBoxId::_hallTypeName[]={
  "HallVolume",
};

const std::string SimDetectorBoxId::_outsideTypeName[]={
  "OutsideVolume",
};

const std::string SimDetectorBoxId::_vertexTypeName[]={
  "VertexVolume"
};

const std::string SimDetectorBoxId::_trackerTypeName[]={
  "TrackerVolume",
  "TrackerSiSensor",
  "TrackerScintillator",
  "TrackerMechanics",
  "fortisCase",
  "fortisSensor"
};

const std::string SimDetectorBoxId::_ecalTypeName[]={
  "EcalVolume",
  "EcalConverter",
  "EcalSiBulk",
  "EcalSiEpitaxial",
  "EcalPcb",
  "EcalMechanics"
};

const std::string SimDetectorBoxId::_hcalTypeName[]={
  "HcalVolume"
};

const std::string SimDetectorBoxId::_triggerTypeName[]={
  "TriggerVolume",
  "TriggerScintillator",
  "TriggerMechanics"
};

#endif
#endif
