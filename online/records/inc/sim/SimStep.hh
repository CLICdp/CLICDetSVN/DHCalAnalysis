#ifndef SimStep_HH
#define SimStep_HH

#include <stdint.h>

#include <iostream>
#include <string>


class SimStep {

public:
  enum {
    versionNumber=0
  };

  SimStep();

  // GEANT4 units = MeV
  double energy() const;
  void   energy(double e);

  // GEANT4 units = mm
  double length() const;

  double x() const;
  double y() const;
  double z() const;

  double xStart() const;
  double yStart() const;
  double zStart() const;

  double xEnd() const;
  void   xEnd(double x);
  double yEnd() const;
  void   yEnd(double y);
  double zEnd() const;
  void   zEnd(double z);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  // This MUST only be used within SimBoxTrack!!!
  double _energy;
  double _position[3];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"


SimStep::SimStep() {
  memset(this,0,sizeof(SimStep));
}

double SimStep::energy() const {
  return _energy;
}

void SimStep::energy(double e) {
  assert(e>=0.0);
  //if(e==0.0) std::cout << "SimStep::energy() ERROR e = 0" << std::endl;
  _energy=e;
}

double SimStep::length() const {
  return sqrt((_position[0]-_position[-4+0])*(_position[0]-_position[-4+0])+
	      (_position[1]-_position[-4+1])*(_position[1]-_position[-4+1])+
	      (_position[2]-_position[-4+2])*(_position[2]-_position[-4+2]));
}

double SimStep::x() const {
  return 0.5*(_position[0]+_position[-4+0]);
}

double SimStep::y() const {
  return 0.5*(_position[1]+_position[-4+1]);
}

double SimStep::z() const {
  return 0.5*(_position[2]+_position[-4+2]);
}

double SimStep::xStart() const {
  return _position[-4+0];
}
/*
void SimStep::xStart(double x) {
  _position[-4+0]=x;
}
*/
double SimStep::yStart() const {
  return _position[-4+1];
}
/*
void SimStep::yStart(double y) {
  _position[-4+1]=y;
}
*/
double SimStep::zStart() const {
  return _position[-4+2];
}
/*
void SimStep::zStart(double z) {
  _position[-4+2]=z;
}
*/
double SimStep::xEnd() const {
  return _position[0];
}

void SimStep::xEnd(double x) {
  _position[0]=x;
}

double SimStep::yEnd() const {
  return _position[1];
}

void SimStep::yEnd(double y) {
  _position[1]=y;
}

double SimStep::zEnd() const {
  return _position[2];
}

void SimStep::zEnd(double z) {
  _position[2]=z;
}

std::ostream& SimStep::print(std::ostream &o, std::string s) const {
  o << s << "SimStep::print()" << std::endl;

  o << s << " Energy = " << std::setw(10) << energy()
    << " MeV, length = " << std::setw(10) << length() << " mm" << std::endl;

  o << s << " Start  position = " << std::setw(10) << xStart() << ", "
    << std::setw(10) << yStart() << ", " << std::setw(10) << zStart() << " mm" << std::endl;
  o << s << " Centre position = " << std::setw(10) << x() << ", "
    << std::setw(10) << y() << ", " << std::setw(10) << z() << " mm" << std::endl;
  o << s << " End    position = " << std::setw(10) << xEnd() << ", "
    << std::setw(10) << yEnd() << ", " << std::setw(10) << zEnd() << " mm" << std::endl;

  return o;
}

#endif
#endif
