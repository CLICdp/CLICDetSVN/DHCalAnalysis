#ifndef SimLayerHits_HH
#define SimLayerHits_HH

#include <iostream>
#include <string>
#include <stdint.h>

class SimPixelHit;

class SimLayerHits {

public:
  enum {
    versionNumber=0
  };

  SimLayerHits();

  unsigned layer() const;
  void     layer(uint32_t l);

  unsigned numberOfHits() const;
  const SimPixelHit* hits() const;
  const SimPixelHit* hit(unsigned n) const;
  void addHit(const SimPixelHit &s);

  unsigned numberOfExtraBytes() const;
  bool overflow() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  uint32_t _layer;
  uint32_t _numberOfHits;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"

#include "SimPixelHit.hh"


SimLayerHits::SimLayerHits() {
  memset(this,0,sizeof(SimLayerHits));
}

unsigned SimLayerHits::layer() const {
  return _layer;
}

void SimLayerHits::layer(uint32_t l) {
  _layer=l;
}

unsigned SimLayerHits::numberOfHits() const {
  return _numberOfHits;
}

const SimPixelHit* SimLayerHits::hit(unsigned n) const {
  if(n<_numberOfHits) return ((const SimPixelHit*)(this+1))+n;
  else return 0;
}

const SimPixelHit* SimLayerHits::hits() const {
  if(_numberOfHits>0) return ((const SimPixelHit*)(this+1));
  else return 0;
}

void SimLayerHits::addHit(const SimPixelHit &s) {
  if(!overflow()) {
    *(((SimPixelHit*)(this+1))+_numberOfHits)=s;
    _numberOfHits++;
  }
}

unsigned SimLayerHits::numberOfExtraBytes() const {
  return _numberOfHits*sizeof(SimPixelHit);
}

bool SimLayerHits::overflow() const {
  return (sizeof(SimLayerHits)+(_numberOfHits+1)*sizeof(SimPixelHit))>=65536;
}

std::ostream& SimLayerHits::print(std::ostream &o, std::string s) const {
  o << s << "SimLayerHits::print()" << std::endl;
  o << s << " Layer = " << _layer << std::endl;

  o << s << " Number of hits = " << _numberOfHits;
  if(overflow()) o << " = overflow limit";
  o << std::endl;

  for(unsigned i(0);i<_numberOfHits;i++) {
    hit(i)->print(o,s+"  ");
  }

  return o;
}

#endif
#endif
