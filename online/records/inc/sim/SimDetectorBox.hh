#ifndef SimDetectorBox_HH
#define SimDetectorBox_HH

#include <iostream>
#include <string>
#include <stdint.h>

#include "SimDetectorBoxId.hh"


class SimDetectorBox : public SimDetectorBoxId {

public:
  enum Material {
    air,
    tungsten,
    silicon,
    pcb,
    aluminium,
    scintillator,
    steel,
    copper,
    lead,
    endOfMaterialEnum,
  };

  SimDetectorBox();

  Material material() const;
  void     material(Material m);

  std::string        materialName() const;
  static std::string materialName(Material m);

  bool sensitive() const;
  void sensitive(bool b);

  double xCentre() const;
  void   xCentre(double x);
  double yCentre() const;
  void   yCentre(double y);
  double zCentre() const;
  void   zCentre(double z);

  double xRight() const;
  double yBottom() const;
  double zFront() const;

  double xLeft() const;
  double yTop() const;
  double zBack() const;

  double xWidth() const;
  void   xWidth(double x);
  double yWidth() const;
  void   yWidth(double y);
  double zThickness() const;
  void   zThickness(double z);

  double xHalfSize() const;
  void   xHalfSize(double x);
  double yHalfSize() const;
  void   yHalfSize(double y);
  double zHalfSize() const;
  void   zHalfSize(double z);

  double radiationLength() const;
  void   radiationLength(double r);


  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  static const std::string _materialName[endOfMaterialEnum];

  Material _material;
  uint32_t _spare;

  double _xCentre;
  double _yCentre;
  double _zCentre;

  double _xHalfSize;
  double _yHalfSize;
  double _zHalfSize;

  double _radiationLength;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


SimDetectorBox::SimDetectorBox() {
}

SimDetectorBox::Material SimDetectorBox::material() const {
  return _material;
}

void SimDetectorBox::material(Material m) {
  _material=m;
}

std::string SimDetectorBox::materialName() const {
  return materialName(_material);
}

std::string SimDetectorBox::materialName(Material m) {
  assert(m<endOfMaterialEnum);
  return _materialName[m];
}

bool SimDetectorBox::sensitive() const {
  return _label.bit(16);
}

void SimDetectorBox::sensitive(bool b) {
  _label.bit(16,b);
}

double SimDetectorBox::xCentre() const {
  return _xCentre;
}

void SimDetectorBox::xCentre(double x) {
  _xCentre=x;
}

double SimDetectorBox::yCentre() const {
  return _yCentre;
}

void SimDetectorBox::yCentre(double y) {
  _yCentre=y;
}

double SimDetectorBox::zCentre() const {
  return _zCentre;
}

void SimDetectorBox::zCentre(double z) {
  _zCentre=z;
}

double SimDetectorBox::xWidth() const {
  return 2.0*_xHalfSize;
}

void SimDetectorBox::xWidth(double x) {
  assert(x>=0.0);
  _xHalfSize=0.5*x;
}

double SimDetectorBox::yWidth() const {
  return 2.0*_yHalfSize;
}

void SimDetectorBox::yWidth(double y) {
  assert(y>=0.0);
  _yHalfSize=0.5*y;
}

double SimDetectorBox::zThickness() const {
  return 2.0*_zHalfSize;
}

void SimDetectorBox::zThickness(double z) {
  assert(z>=0.0);
  _zHalfSize=0.5*z;
}

double SimDetectorBox::xHalfSize() const {
  return _xHalfSize;
}

void SimDetectorBox::xHalfSize(double x) {
  assert(x>=0.0);
  _xHalfSize=x;
}

double SimDetectorBox::yHalfSize() const {
  return _yHalfSize;
}

void SimDetectorBox::yHalfSize(double y) {
  assert(y>=0.0);
  _yHalfSize=y;
}

double SimDetectorBox::zHalfSize() const {
  return _zHalfSize;
}

void SimDetectorBox::zHalfSize(double z) {
  assert(z>=0.0);
  _zHalfSize=z;
}

double SimDetectorBox::xRight() const {
  return _xCentre-_xHalfSize;
}

double SimDetectorBox::yBottom() const {
  return _yCentre-_yHalfSize;
}

double SimDetectorBox::zFront() const {
  return _zCentre-_zHalfSize;
}

double SimDetectorBox::xLeft() const {
  return _xCentre+_xHalfSize;
}

double SimDetectorBox::yTop() const {
  return _yCentre+_yHalfSize;
}

double SimDetectorBox::zBack() const {
  return _zCentre+_zHalfSize;
}

double SimDetectorBox::radiationLength() const {
  return _radiationLength;
}

void SimDetectorBox::radiationLength(double r) {
  assert(r>=0.0);
  _radiationLength=r;
}

std::ostream& SimDetectorBox::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorBox::print()" << std::endl;

  SimDetectorBoxId::print(o,s+" ");
  if(sensitive()) o << s << "  Volume is sensitive" << std::endl;
  else            o << s << "  Volume is not sensitive" << std::endl;

  o << s << " Material = " << _material
    << " = " << materialName() << std::endl;

  o << s << " X centre = " << _xCentre
    << ", width = " << xWidth() << " mm" << std::endl;
  o << s << " Y centre = " << _yCentre
    << ", width = " << yWidth() << " mm" << std::endl;
  o << s << " Z centre = " << _zCentre
    << ", thickness = " << zThickness() << " mm" << std::endl;
  o << s << " Radiation length = " << _radiationLength << " X0" << std::endl;

  return o;
}

const std::string SimDetectorBox::_materialName[]={
  "air",
  "tungsten",
  "silicon",
  "pcb",
  "aluminium",
  "scintillator",
  "steel",
  "copper",
  "lead"
};

#endif
#endif
