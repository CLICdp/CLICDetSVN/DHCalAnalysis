#ifndef SimChargeSpreadData_HH
#define SimChargeSpreadData_HH

#include <iostream>
#include <string>
#include <stdint.h>

#include "SimChargeSpread.hh"


class SimChargeSpreadData {

public:
  enum {
    versionNumber=0
  };

  SimChargeSpreadData();

  SimChargeSpread chargeSpread(double dx, double dy, double dz) const;

  bool read(const std::string &f);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _xyPixelSize;
  double _zDepth;
  double _chargeSpread[11][11][5][5];
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


SimChargeSpreadData::SimChargeSpreadData() {
  memset(this,0,sizeof(SimChargeSpreadData));

  _xyPixelSize=0.05;
  _zDepth=0.012;

  for(unsigned i(0);i<11;i++) {
    for(unsigned j(0);j<11;j++) {
      _chargeSpread[i][j][2][2]=1.0;
    }
  }
}

bool SimChargeSpreadData::read(const std::string &f) {
  std::ifstream fin(f.c_str());
  if(!fin) return false;
  fin >> _xyPixelSize;
  std::cout << "pixel size " <<  _xyPixelSize << std::endl;
  if(!fin) return false;
  fin >> _zDepth;
  std::cout << "z depth " <<  _zDepth << std::endl;

  for(unsigned jj(0);jj<6;jj++) {
    unsigned j(5-jj);
    for(unsigned ii(0);ii<=jj;ii++) {
      unsigned i(5+ii);

      for(unsigned l(0);l<5;l++) {
	for(unsigned k(0);k<5;k++) {
	  if(!fin) return false;
	  double c;
	  fin >> c;
	  _chargeSpread[   i][   j][  k][  l]=c;
	  _chargeSpread[10-i][   j][4-k][  l]=c;
	  _chargeSpread[   i][10-j][  k][4-l]=c;
	  _chargeSpread[10-i][10-j][4-k][4-l]=c;
	  _chargeSpread[   j][   i][  l][  k]=c;
	  _chargeSpread[10-j][   i][4-l][  k]=c;
	  _chargeSpread[   j][10-i][  l][4-k]=c;
	  _chargeSpread[10-j][10-i][4-l][4-k]=c;
	}
      }
    }
  }



  return true;
}

SimChargeSpread SimChargeSpreadData::chargeSpread(double dx, double dy, double dz) const {
  assert(dx>=0.0 && dx<_xyPixelSize);
  assert(dy>=0.0 && dy<_xyPixelSize);
  assert(dz>=0.0 && dz<=(_zDepth+0.000001));

  /*
  unsigned i((unsigned)(0.5+10.0*dx/_xyPixelSize));
  unsigned j((unsigned)(0.5+10.0*dy/_xyPixelSize));
  assert(i<11 && j<11);

  return *((SimChargeSpread*)_chargeSpread[i][j][0]);
  */

  unsigned i((unsigned)(10.0*dx/_xyPixelSize));
  unsigned j((unsigned)(10.0*dy/_xyPixelSize));
  assert(i<10 && j<10);

  SimChargeSpread scs;
  for(int k(0);k<5;k++) {
    for(int l(0);l<5;l++) {
      scs.fraction(k-2,l-2,0.25*(_chargeSpread[i  ][j  ][k][l]+
				 _chargeSpread[i  ][j+1][k][l]+
				 _chargeSpread[i+1][j  ][k][l]+
				 _chargeSpread[i+1][j+1][k][l]));
    }
  }
  return scs;
}

std::ostream& SimChargeSpreadData::print(std::ostream &o, std::string s) const {
  o << s << "SimChargeSpreadData::print()" << std::endl;
  return o;
}

#endif
#endif
