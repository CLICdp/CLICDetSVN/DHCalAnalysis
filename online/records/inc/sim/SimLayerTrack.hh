#ifndef SimLayerTrack_HH
#define SimLayerTrack_HH

#include <iostream>
#include <string>

#include "SimDetectorBoxId.hh"

class SimStep;

class SimLayerTrack : public SimDetectorBoxId {

public:
  enum {
    versionNumber=0
  };

  SimLayerTrack();

  SimDetectorBoxId boxId() const;
  void             boxId(SimDetectorBoxId b);

  int32_t particleId() const;
  void    particleId(int32_t p);

  bool entry() const;
  void entry(bool e);

  bool exit() const;
  void exit(bool e);

  bool charged() const;
  void charged(bool b);

  // GEANT4 units = mm
  double xStart() const;
  void   xStart(double x);
  double yStart() const;
  void   yStart(double y);
  double zStart() const;
  void   zStart(double z);

  double xEnd() const;
  //void   xEnd(double x);
  double yEnd() const;
  //void   yEnd(double y);
  double zEnd() const;
  //void   zEnd(double z);

  double totalLength() const;
  double totalEnergy() const;
  double xAverage() const;
  double yAverage() const;
  double zAverage() const;

  unsigned numberOfSteps() const;
  const SimStep* steps() const;
  const SimStep* step(unsigned n) const;
  void     addStep(const SimStep &s);
  SimStep* addStep();

  unsigned numberOfExtraBytes() const;
  bool overflow() const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  int32_t  _particleId;
  uint32_t _numberOfSteps;

  double _startPosition[3];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>
#include <iomanip>

#include "UtlPrintHex.hh"

#include "SimStep.hh"


SimLayerTrack::SimLayerTrack() {
  memset(this,0,sizeof(SimLayerTrack));
}

SimDetectorBoxId SimLayerTrack::boxId() const {
  SimDetectorBoxId b;
  b.type(type());
  b.layer(layer());
  return b;
}

void SimLayerTrack::boxId(SimDetectorBoxId b) {
  type(b.type());
  layer(b.layer());
}

int SimLayerTrack::particleId() const {
  return _particleId;
}

void SimLayerTrack::particleId(int p) {
  _particleId=p;
}

bool SimLayerTrack::entry() const {
  return _label.bit(16);
}

void SimLayerTrack::entry(bool b) {
  _label.bit(16,b);
}

bool SimLayerTrack::exit() const {
  return _label.bit(17);
}

void SimLayerTrack::exit(bool b) {
  _label.bit(17,b);
}

bool SimLayerTrack::charged() const {
  return _label.bit(18);
}

void SimLayerTrack::charged(bool b) {
  _label.bit(18,b);
}

double SimLayerTrack::xStart() const {
  return _startPosition[0];
}

void SimLayerTrack::xStart(double x) {
  _startPosition[0]=x;
}

double SimLayerTrack::yStart() const {
  return _startPosition[1];
}

void SimLayerTrack::yStart(double y) {
  _startPosition[1]=y;
}

double SimLayerTrack::zStart() const {
  return _startPosition[2];
}

void SimLayerTrack::zStart(double z) {
  _startPosition[2]=z;
}

double SimLayerTrack::xEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[0];
  } else {
    return step(_numberOfSteps-1)->xEnd();
  }
}
/*
void SimLayerTrack::xEnd(double x) {
  _exitPosition[0]=x;
}
*/
double SimLayerTrack::yEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[1];
  } else {
    return step(_numberOfSteps-1)->yEnd();
  }
}
/*
void SimLayerTrack::yEnd(double y) {
  _exitPosition[1]=y;
}
*/
double SimLayerTrack::zEnd() const {
  if(_numberOfSteps==0) {
    return _startPosition[2];
  } else {
    return step(_numberOfSteps-1)->zEnd();
  }
}
/*
void SimLayerTrack::zEnd(double z) {
  _exitPosition[2]=z;
}
*/
double SimLayerTrack::totalLength() const {
  double l(0.0);
  for(unsigned i(0);i<_numberOfSteps;i++) {
    l+=step(i)->length();
  }
  return l;
}

double SimLayerTrack::totalEnergy() const {
  double e(0.0);
  for(unsigned i(0);i<_numberOfSteps;i++) {
    e+=step(i)->energy();
  }
  return e;
}

double SimLayerTrack::xAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double x(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      x+=step(i)->length()*step(i)->x();
    }
    return x/l;

  } else {
    return step(0)->x();
  }
}

double SimLayerTrack::yAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double y(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      y+=step(i)->length()*step(i)->y();
    }
    return y/l;

  } else {
    return step(0)->y();
  }
}

double SimLayerTrack::zAverage() const {
  double l(totalLength());
  if(l>0.0) {
    double z(0.0);
    for(unsigned i(0);i<_numberOfSteps;i++) {
      z+=step(i)->length()*step(i)->z();
    }
    return z/l;

  } else {
    return step(0)->z();
  }
}

unsigned SimLayerTrack::numberOfSteps() const {
  return _numberOfSteps;
}

const SimStep* SimLayerTrack::steps() const {
  return (const SimStep*)(this+1);
}

const SimStep* SimLayerTrack::step(unsigned n) const {
  if(n<_numberOfSteps) return ((const SimStep*)(this+1))+n;
  else return 0;
}

void SimLayerTrack::addStep(const SimStep &s) {
  if(!overflow()) {
    *(((SimStep*)(this+1))+_numberOfSteps)=s;
    _numberOfSteps++;
  }
}

SimStep* SimLayerTrack::addStep() {
  if(overflow()) return 0;
  return ((SimStep*)(this+1))+(_numberOfSteps++);
}

unsigned SimLayerTrack::numberOfExtraBytes() const {
  return _numberOfSteps*sizeof(SimStep);
}

bool SimLayerTrack::overflow() const {
  return (sizeof(SimLayerTrack)+(_numberOfSteps+1)*sizeof(SimStep))>=65536;
}

std::ostream& SimLayerTrack::print(std::ostream &o, std::string s) const {
  o << s << "SimLayerTrack::print()" << std::endl;

  o << s << " Particle Id = " << std::setw(10) << _particleId << std::endl;

  SimDetectorBoxId::print(o,s+" ");
  if(entry() && exit())   o << s << "   Entry and exit from box" << std::endl;
  if(entry() && !exit())  o << s << "   Entry only, stopped in box" << std::endl;
  if(!entry() && exit())  o << s << "   Exit only, started in box" << std::endl;
  if(!entry() && !exit()) o << s << "   Contained within box" << std::endl;
  if(charged()) o << s << "   Charged particle" << std::endl;
  else          o << s << "   Neutral particle" << std::endl;

  o << s << " Start position = "
    << std::setw(10) << xStart() << ", "
    << std::setw(10) << yStart() << ", "
    << std::setw(10) << zStart() << " mm" << std::endl;
  
  o << s << " End   position = "
    << std::setw(10) << xEnd() << ", "
    << std::setw(10) << yEnd() << ", "
    << std::setw(10) << zEnd() << " mm" << std::endl;

  o << s << " Total length = " << std::setw(10) << totalLength()
    << " mm, energy = " << std::setw(10) << totalEnergy() << " MeV" << std::endl;
  o << s << " Average x,y,z = " << std::setw(10) << xAverage()
    << " mm, " << std::setw(10) << yAverage() << " mm"
    << std::setw(10) << zAverage() << " mm" << std::endl;

  o << s << " Number of steps = " << _numberOfSteps;
  if(overflow()) o << " = overflow limit";
  o << std::endl;

  for(unsigned i(0);i<_numberOfSteps;i++) {
    step(i)->print(o,s+" ");
  }

  return o;
}

#endif
#endif
