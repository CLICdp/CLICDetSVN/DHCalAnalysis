#ifndef SimDetectorLayer_HH
#define SimDetectorLayer_HH

#include <iostream>
#include <string>
#include <stdint.h>


class SimDetectorLayer {

public:
  SimDetectorLayer();

  unsigned detectorId() const;
  void     detectorId(unsigned i);

  unsigned detectorIdLayer() const;
  void     detectorIdLayer(unsigned l);

  unsigned materialId() const;
  void     materialId(unsigned i);

  double zCentre() const;
  void   zCentre(double z);

  double zThickness() const;
  void   zThickness(double z);

  double zHalfSize() const;
  void   zHalfSize(double z);

  double radiationLength() const;
  void   radiationLength(double r);


  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  uint32_t _detectorId;
  uint32_t _detectorIdLayer;
  uint32_t _materialId;
  uint32_t _spare;

  double _zCentre;
  double _zSize;
  double _radiationLength;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


SimDetectorLayer::SimDetectorLayer() {
}

unsigned SimDetectorLayer::detectorId() const {
  return _detectorId;
}

void SimDetectorLayer::detectorId(unsigned i) {
  _detectorId=i;
}

unsigned SimDetectorLayer::detectorIdLayer() const {
  return _detectorIdLayer;
}

void SimDetectorLayer::detectorIdLayer(unsigned i) {
  _detectorIdLayer=i;
}

void SimDetectorLayer::materialId(unsigned i) {
  _materialId=i;
}

unsigned SimDetectorLayer::materialId() const {
  return _materialId;
}

double SimDetectorLayer::zCentre() const {
  return _zCentre;
}

void SimDetectorLayer::zCentre(double z) {
  _zCentre=z;
}

double SimDetectorLayer::zThickness() const {
  return 2.0*_zSize;
}

void SimDetectorLayer::zThickness(double z) {
  assert(z>=0.0);
  _zSize=0.5*z;
}

double SimDetectorLayer::zHalfSize() const {
  return _zSize;
}

void SimDetectorLayer::zHalfSize(double z) {
  assert(z>=0.0);
  _zSize=z;
}

double SimDetectorLayer::radiationLength() const {
  return _radiationLength;
}

void SimDetectorLayer::radiationLength(double r) {
  assert(r>=0.0);
  _radiationLength=r;
}

std::ostream& SimDetectorLayer::print(std::ostream &o, std::string s) const {
  o << s << "SimDetectorLayer::print()" << std::endl;

  o << s << " Detector id = " << _detectorId << std::endl;
  o << s << " Detector id layer = " << _detectorIdLayer << std::endl;
  o << s << " Material id = " << _materialId << std::endl;

  o << s << " Z centre = " << _zCentre
    << ", thickness = " << zThickness() << " mm" << std::endl;
  o << s << " Z front = " << _zCentre-_zSize
    << ", back = " << _zCentre+_zSize << " mm" << std::endl;
  o << s << " Z radiation length = " << _radiationLength << " X0" << std::endl;

  return o;
}

#endif
#endif
