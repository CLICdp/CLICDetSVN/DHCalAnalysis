#ifndef AhcModuleSlowReadoutData_HH
#define AhcModuleSlowReadoutData_HH

#include <string>
#include <iostream>


class AhcModuleSlowReadoutData {

public:
  enum {
    versionNumber=0
  };

  AhcModuleSlowReadoutData();
  

  unsigned moduleNumber() const;
  void     moduleNumber(unsigned m);

  unsigned short temperature(unsigned i) const;
  void           temperature(unsigned i, unsigned short t);

  unsigned highVoltage(unsigned i) const;
  void     highVoltage(unsigned i, unsigned v);

  unsigned short current(unsigned i) const;
  void           current(unsigned i, unsigned short c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned _moduleNumber;
  unsigned short _temperature[12];
  unsigned _highVoltage[2];
  unsigned short _current[6];
};


#ifdef CALICE_DAQ_ICC

#include <cassert>


AhcModuleSlowReadoutData::AhcModuleSlowReadoutData() {
  _temperature[11]=0xffff;
  _current[5]=0xffff;
}

unsigned AhcModuleSlowReadoutData::moduleNumber() const {
  return _moduleNumber;
}

void AhcModuleSlowReadoutData::moduleNumber(unsigned m) {
  _moduleNumber=m;
}

unsigned short AhcModuleSlowReadoutData::temperature(unsigned i) const {
  assert(i<11);
  return _temperature[i];
}

void AhcModuleSlowReadoutData::temperature(unsigned i, unsigned short t) {
  assert(i<11);
  _temperature[i]=t;
}

unsigned AhcModuleSlowReadoutData::highVoltage(unsigned i) const {
  assert(i<2);
  return _highVoltage[i];
}

void AhcModuleSlowReadoutData::highVoltage(unsigned i, unsigned v) {
  assert(i<2);
  _highVoltage[i]=v;
}

unsigned short AhcModuleSlowReadoutData::current(unsigned i) const {
  assert(i<5);
  return _current[i];
}

void AhcModuleSlowReadoutData::current(unsigned i, unsigned short c) {
  assert(i<5);
  _current[i]=c;
}

std::ostream& AhcModuleSlowReadoutData::print(std::ostream &o, std::string s) const {
  o << s << "AhcModuleSlowReadoutData::print()" << std::endl;
  o << s << " Module number = " << std::setw(2) << _moduleNumber << std::endl;

  for(unsigned i(0);i<11;i++) {
    o << s << " Temperature " << std::setw(2) << i << " = "
      << std::setw(5) << _temperature[i] << " x 0.01 C = "
      << std::setw(5) << 0.01*_temperature[i] << " C" << std::endl;
  }

  for(unsigned i(0);i<2;i++) {
    o << s << " High voltage " << i << " = "
      << std::setw(5) << _highVoltage[i] << " x 0.001 V = "
      << std::setw(6) << 0.001*_highVoltage[i] << " V" << std::endl;
  }

  for(unsigned i(0);i<5;i++) {
    o << s << " Current " << i << " = "
      << std::setw(5) << _current[i] << " x 0.001 A = "
      << std::setw(6) << 0.001*_current[i] << " A" << std::endl;
  }

  return o;
}

#endif
#endif
