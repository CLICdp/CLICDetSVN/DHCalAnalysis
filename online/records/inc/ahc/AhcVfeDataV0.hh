#ifndef AhcVfeDataV0_HH
#define AhcVfeDataV0_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

#include "AhcVfeShiftRegister.hh"


class AhcVfeDataV0 {

public:
  AhcVfeDataV0();

  AhcVfeShiftRegister shiftRegister() const;
  void                shiftRegister(AhcVfeShiftRegister s);

  unsigned char dac(unsigned d) const;
  void          dac(unsigned d, unsigned char c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  unsigned char _dac[18];
  AhcVfeShiftRegister _shiftRegister;
  //UtlPack _data[5];
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"


AhcVfeDataV0::AhcVfeDataV0() {
  //shiftRegister(0x0010);
  for(unsigned i(0);i<18;i++) dac(i,128);
}

AhcVfeShiftRegister AhcVfeDataV0::shiftRegister() const {
  return _shiftRegister;
  //  return _data[4].halfWord(1);
}

void AhcVfeDataV0::shiftRegister(AhcVfeShiftRegister s) {
  _shiftRegister=s;
  //  _data[4].halfWord(1,s);
}

unsigned char AhcVfeDataV0::dac(unsigned d) const {
  assert(d<18);
  //  return _data[d/4].byte(n%4);
  return _dac[d];
}

void AhcVfeDataV0::dac(unsigned d, unsigned char c) {
  assert(d<18);
  //  _data[d/4].byte(d%4,c);
  _dac[d]=c;
}

std::ostream& AhcVfeDataV0::print(std::ostream &o, std::string s) const {
  o << s << "AhcVfeDataV0::print()" << std::endl;

  _shiftRegister.print(o,s+" ");
  
  o << s << " DACs =";
  for(unsigned i(0);i<18;i++) o << std::setw(4) << (unsigned)dac(i);
  o << std::endl;

  return o;
}

#endif
#endif
