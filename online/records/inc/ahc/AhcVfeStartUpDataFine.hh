#ifndef AhcVfeStartUpDataFine_HH
#define AhcVfeStartUpDataFine_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

#include "CrcFeConfigurationData.hh"

class AhcFeConfigurationDataV0;


class AhcVfeStartUpDataFine {

public:
  enum {
    versionNumber=0
  };
 
  AhcVfeStartUpDataFine();
  AhcVfeStartUpDataFine(const AhcFeConfigurationDataV0 &a);

  UtlPack verificationData() const;
  void    verificationData(UtlPack v);

  unsigned char dac(unsigned hab, unsigned d) const;
  void          dac(unsigned hab, unsigned d, unsigned char a);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _verificationData;
  unsigned char _dac[12][18];
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"

#include "AhcFeConfigurationDataV0.hh"


AhcVfeStartUpDataFine::AhcVfeStartUpDataFine() : _verificationData(0xaaaa5555) {
  for(unsigned i(0);i<12;i++) {
    for(unsigned j(0);j<18;j++) {
      dac(i,j,18*i+j);
    }
  }
}

AhcVfeStartUpDataFine::AhcVfeStartUpDataFine(const AhcFeConfigurationDataV0 &a) {
  _verificationData=0xffffffff;
  for(unsigned i(0);i<12;i++) {
    for(unsigned j(0);j<18;j++) {
      dac(i,j,a.dac(i,j));
    }
  }  
}

UtlPack AhcVfeStartUpDataFine::verificationData() const {
  return _verificationData;
}
 
void AhcVfeStartUpDataFine::verificationData(UtlPack v) {
  _verificationData=v;
}

unsigned char AhcVfeStartUpDataFine::dac(unsigned hab, unsigned d) const {
  assert(hab<12 && d<18);
  return _dac[hab][d];
}

void AhcVfeStartUpDataFine::dac(unsigned hab, unsigned d, unsigned char a) {
  assert(hab<12 && d<18);
  _dac[hab][d]=a;
}

std::ostream& AhcVfeStartUpDataFine::print(std::ostream &o, std::string s) const {
  o << s << "AhcVfeStartUpDataFine::print()" << std::endl;

  o << s << " Verification data = " << printHex(_verificationData) << std::endl;

  for(unsigned i(0);i<12;i++) {
    o << s << " HAB " << std::setw(2) << i << " DACs = ";
    for(unsigned j(0);j<18;j++) {
      o << std::setw(4) << (unsigned)dac(i,j);
    }
    o << std::endl;
  }  
  
  return o;
}

#endif
#endif
