#ifndef AhcVfeConfigurationDataCoarse_HH
#define AhcVfeConfigurationDataCoarse_HH

#include <string>
#include <iostream>

#include "AhcVfeControl.hh"
#include "AhcVfeShiftRegister.hh"


class AhcVfeConfigurationDataCoarse {

public:
  enum {
    versionNumber=0
  };

  AhcVfeConfigurationDataCoarse();

  UtlPack verificationData() const;
  void    verificationData(UtlPack v);

  AhcVfeShiftRegister shiftRegister(unsigned hab) const;
  void                shiftRegister(unsigned hab, AhcVfeShiftRegister s);

  bool operator!=(const AhcVfeConfigurationDataCoarse &c);
  bool operator==(const AhcVfeConfigurationDataCoarse &c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _verificationData;
  AhcVfeShiftRegister _shiftRegister[8];
};


#ifdef CALICE_DAQ_ICC


AhcVfeConfigurationDataCoarse::AhcVfeConfigurationDataCoarse() : _verificationData(0xaaaa5555) {
}

UtlPack AhcVfeConfigurationDataCoarse::verificationData() const {
  return _verificationData;
}
 
void AhcVfeConfigurationDataCoarse::verificationData(UtlPack v) {
  _verificationData=v;
}
 
AhcVfeShiftRegister AhcVfeConfigurationDataCoarse::shiftRegister(unsigned hab) const {
  assert(hab<12);

  // Numbering is 0-3 and 7-11
  if(     hab<4) return _shiftRegister[hab  ];
  else if(hab>7) return _shiftRegister[hab-4];
  else assert(false);
}

void AhcVfeConfigurationDataCoarse::shiftRegister(unsigned hab, AhcVfeShiftRegister s) {
  assert(hab<12);

  // Numbering is 0-3 and 7-11
  if(     hab<4) _shiftRegister[hab  ]=s;
  else if(hab>7) _shiftRegister[hab-4]=s;
  else assert(false);
}

bool AhcVfeConfigurationDataCoarse::operator!=(const AhcVfeConfigurationDataCoarse &c) {
  unsigned *a((unsigned*)this);
  unsigned *b((unsigned*)&c);
  for(unsigned i(0);i<sizeof(AhcVfeConfigurationDataCoarse)/4;i++) {
    if(a[i]!=b[i]) return true;
  }
  return false;
}

bool AhcVfeConfigurationDataCoarse::operator==(const AhcVfeConfigurationDataCoarse &c) {
  return !operator!=(c);
}

std::ostream& AhcVfeConfigurationDataCoarse::print(std::ostream &o, std::string s) const {
  o << s << "AhcVfeConfigurationDataCoarse::print()" << std::endl;

  o << s << " Verification data = " << printHex(_verificationData) << std::endl;

  for(unsigned i(0);i<8;i++) {
    o << s << " HAB " << std::setw(2);
    if(i<4) o << i << " ";
    else    o << i+4 << " ";
    _shiftRegister[i].print(o,s+" ");
  }
  
  return o;
}

#endif
#endif
