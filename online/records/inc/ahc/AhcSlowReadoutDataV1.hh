#ifndef AhcSlowReadoutDataV1_HH
#define AhcSlowReadoutDataV1_HH


#include <string>
#include <iostream>
#include <map>
#include "UtlMapDefinitions.hh"


//The sizes of the arrays which contain the sro information
#define NCMBTEMPS 7
#define NCMBVOLTS 9
#define NCMBSIZES 2
#define NCMBVALUES 2
#define NHBABTEMPS 4
#define NHBABVOLTS 12



#pragma pack (4)
class AhcSlowReadoutDataV1 {

public:
  enum {
    versionNumber=1
  };

  AhcSlowReadoutDataV1();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  unsigned moduleNumber() const;


  //CRP access functions defined by RP
  std::vector<double> getCmbTemperatures() const;
  std::vector<double> getCmbVoltages() const;
  std::vector<double> getCmbValues() const; 
  std::vector<double> getHbabTemperatures() const;
  std::vector<double> getHbabVoltages() const;
  std::vector<unsigned> getCmbSizes() const;


  const unsigned getLedSetting() const; 

  //Initialize the map between types and access functions in the DAQ class - doubles

  //doubles
  DaqTypesDblMap_t getDblMap() const;
  //unsigned ints
  DaqTypesUIntMap_t getUIntMap(); 

  //For completeness ... floats
  DaqTypesFloatMap_t getFloatMap(); 


  std::ostream& print(std::ostream &o, std::string s="") const;


private:

   //use array dimensions as given in  define statements above
  /*time_t _timeStamp;
  unsigned _moduleNumber;
  double _cmbTemperatures[7];
  double _cmbVoltages[9];
  unsigned _ledSetting;
  unsigned _cmbSizes[2];
  double _cmbValues[2];
  double _hbabTemperatures[4];
  double _hbabVoltages[12];*/

  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  unsigned _moduleNumber;
  double _cmbTemperatures[NCMBTEMPS];
  double _cmbVoltages[NCMBVOLTS];
  unsigned _ledSetting;
  unsigned _cmbSizes[NCMBSIZES];
  double _cmbValues[NCMBVALUES];
  double _hbabTemperatures[NHBABTEMPS];
  double _hbabVoltages[NHBABVOLTS];

};
#pragma pack ()


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


AhcSlowReadoutDataV1::AhcSlowReadoutDataV1() {
  memset(this,0,sizeof(AhcSlowReadoutDataV1));
}

bool AhcSlowReadoutDataV1::parse(std::string r) {
  _timeStamp=0xffffffff;
  _moduleNumber=255;
  memset(_cmbTemperatures,0,36*sizeof(double)+sizeof(unsigned));

  std::istringstream sin(r);
  if(!sin) return false;

  sin >> _timeStamp;
  if(!sin) return false;

  sin >> _moduleNumber;
  if(!sin) return false;

  for(unsigned i(0);i<7;i++) {
    sin >> _cmbTemperatures[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<9;i++) {
    sin >> _cmbVoltages[i];
    if(!sin) return false;
  }

  sin >> std::hex >> _ledSetting >> std::dec;
  if(!sin) return false;

  for(unsigned i(0);i<2;i++) {
    sin >> _cmbSizes[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<2;i++) {
    sin >> _cmbValues[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<4;i++) {
    sin >> _hbabTemperatures[i];
    if(!sin) return false;
  }

  for(unsigned i(0);i<12;i++) {
    sin >> _hbabVoltages[i];
    if(!sin) return false;
  }

  return true;
}

time_t AhcSlowReadoutDataV1::timeStamp() const {
  return _timeStamp;
}

void AhcSlowReadoutDataV1::timeStamp(time_t t) {
  _timeStamp=t;
}

unsigned AhcSlowReadoutDataV1::moduleNumber() const {
  return _moduleNumber;
}

//CRP Access functions added 11/8/06

std::vector<double> AhcSlowReadoutDataV1::getCmbTemperatures() const {
  std::vector<double> v;
  v.clear();
  for(unsigned i(0);i<NCMBTEMPS;i++) v.push_back(_cmbTemperatures[i]);
  return v;
}

std::vector<double> AhcSlowReadoutDataV1::getCmbVoltages() const {
  //std::vector<double> v(NCMBVOLTS);
  std::vector<double> v;
  v.clear();
  for(unsigned i(0);i<NCMBVOLTS;i++) v.push_back(_cmbVoltages[i]);
  return v;
}


const unsigned AhcSlowReadoutDataV1::getLedSetting() const{
 return _ledSetting;
}

std::vector<unsigned> AhcSlowReadoutDataV1::getCmbSizes() const {
  std::vector<unsigned> v(NCMBSIZES);
  for(unsigned i(0);i<NCMBSIZES;i++) v[i]=_cmbSizes[i];
  return v;
}


//const unsigned* const AhcSlowReadoutDataV1::getCmbSizes() const{
//  return _cmbSizes;
//}


std::vector<double> AhcSlowReadoutDataV1::getCmbValues() const{
  std::vector<double> v(NCMBVALUES);
  for(unsigned i(0);i<NCMBVALUES;i++) v[i]=_cmbValues[i];
  return v;
}

std::vector<double> AhcSlowReadoutDataV1::getHbabTemperatures() const{
  std::vector<double> v(NHBABTEMPS);
  for(unsigned i(0);i<NHBABTEMPS;i++) v[i]=_hbabTemperatures[i];
  return v;
}

std::vector<double> AhcSlowReadoutDataV1::getHbabVoltages() const{
  std::vector<double> v(NHBABVOLTS);
  for(unsigned i(0);i<NHBABVOLTS;i++) v[i]=_hbabVoltages[i];
  return v;
}


 DaqTypesDblMap_t AhcSlowReadoutDataV1::getDblMap() const { 
    DaqTypesDblMap_t _theDblTypes;
    _theDblTypes.clear();
    //std::vector<double> dummyvec=getCmbTemperatures();
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CMBTEMPERATURES", getCmbTemperatures())).second) std::cout << "Problems with inserting CMBTEMPERATURES into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CMBVOLTAGES", getCmbVoltages())).second) std::cout << "Problems with inserting CMBVOLTAGES into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("CMBVALUES", getCmbValues())).second) std::cout << "Problems with inserting CMBVALUES into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("HBABTEMPERATURES", getHbabTemperatures())).second) std::cout << "Problems with inserting HBABTEMPERATURES into map" << std::endl;  
    if(!_theDblTypes.insert(std::pair<std::string,std::vector<double> >("HBABVOLTAGES", getHbabVoltages()
)).second) std::cout << "Problems with inserting HBABVOLTAGES into map" << std::endl;  
    return _theDblTypes;
  }; 


DaqTypesUIntMap_t AhcSlowReadoutDataV1::getUIntMap() { 
  DaqTypesUIntMap_t _theUIntTypes;
  _theUIntTypes.clear();
  if(!_theUIntTypes.insert(std::pair<std::string,std::vector<unsigned> >("CMBSIZES", getCmbSizes())).second) std::cout << "Problems with inserting CMBSIZES into map" << std::endl;  
  return _theUIntTypes;
};



DaqTypesFloatMap_t AhcSlowReadoutDataV1::getFloatMap() { 
  DaqTypesFloatMap_t _theFloatTypes;
  _theFloatTypes.clear();
  return _theFloatTypes;
};



std::ostream& AhcSlowReadoutDataV1::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowReadoutDataV1::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " Module number = " << _moduleNumber << std::endl;

  for(unsigned i(0);i<5;i++) {
    o << s << " CMB temperature " << std::setw(2) << i+1
      << "    = " << std::setw(5) << _cmbTemperatures[i] << " C" << std::endl;
  }
  o << s << " CMB temperature lower = " << std::setw(5)
    << _cmbTemperatures[5] << " C" << std::endl;
  o << s << " CMB temperature upper = " << std::setw(5)
    << _cmbTemperatures[6] << " C" << std::endl;

  o << s << " CMB calib U041 voltage = " << _cmbVoltages[0] << " V" << std::endl;
  o << s << " CMB 12V power  voltage = " << _cmbVoltages[1] << " V" << std::endl;
  o << s << " CMB 1.235V     voltage = " << _cmbVoltages[2] << " V" << std::endl;
  o << s << " CMB VLDA upper voltage = " << _cmbVoltages[3] << " V" << std::endl;
  o << s << " CMB VLDB upper voltage = " << _cmbVoltages[4] << " V" << std::endl;
  o << s << " CMB VLDC upper voltage = " << _cmbVoltages[5] << " V" << std::endl;
  o << s << " CMB VLDD lower voltage = " << _cmbVoltages[6] << " V" << std::endl;
  o << s << " CMB 10V bias   voltage = " << _cmbVoltages[7] << " V" << std::endl;
  o << s << " CMB calib U051 voltage = " << _cmbVoltages[8] << " V" << std::endl;

  o << s << " LED setting = " << printHex(_ledSetting) << std::endl;

  o << s << " LED pulser width  = " << std::setw(3) << _cmbSizes[0] << std::endl;
  o << s << " LED pulser height = " << std::setw(3) << _cmbSizes[1] << std::endl;

  //CRP corrected 11/8/06
  //o << s << " CMB 12V external = " << _cmbValues[2] << " V" << std::endl;
  //o << s << " CMB 12V external = " << _cmbValues[3] << " V" << std::endl;
  o << s << " CMB 12V external = " << _cmbValues[0] << " V" << std::endl;
  o << s << " CMB 12V external = " << _cmbValues[1] << " V" << std::endl;

  o << s << " HBAB temperature top 1 = " << _hbabTemperatures[0] << " C" << std::endl;
  o << s << " HBAB temperature top 2 = " << _hbabTemperatures[1] << " C" << std::endl;
  o << s << " HBAB temperature bot 1 = " << _hbabTemperatures[2] << " C" << std::endl;
  o << s << " HBAB temperature bot 2 = " << _hbabTemperatures[3] << " C" << std::endl;

  o << s << " HBAB HV top voltage  = " << _hbabVoltages[ 0] << " V" << std::endl;
  o << s << " HBAB HV bot voltage  = " << _hbabVoltages[ 1] << " V" << std::endl;
  o << s << " HBAB HV top current  = " << _hbabVoltages[ 2] << " A" << std::endl;
  o << s << " HBAB HV bot current  = " << _hbabVoltages[ 3] << " A" << std::endl;
  o << s << " HBAB LV top voltage  = " << _hbabVoltages[ 4] << " V" << std::endl;
  o << s << " HBAB LV bot voltage  = " << _hbabVoltages[ 5] << " V" << std::endl;
  o << s << " HBAB LV top current  = " << _hbabVoltages[ 6] << " A" << std::endl;
  o << s << " HBAB LV bot current  = " << _hbabVoltages[ 7] << " A" << std::endl;
  o << s << " HBAB LVn top voltage = " << _hbabVoltages[ 8] << " V" << std::endl;
  o << s << " HBAB LVn bot voltage = " << _hbabVoltages[ 9] << " V" << std::endl;
  o << s << " HBAB LVn top current = " << _hbabVoltages[10] << " A" << std::endl;
  o << s << " HBAB LVn bot current = " << _hbabVoltages[11] << " A" << std::endl;

  return o;
}

#endif
//#pragma pack()
#endif
