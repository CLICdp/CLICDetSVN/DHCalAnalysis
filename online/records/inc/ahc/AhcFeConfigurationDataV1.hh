#ifndef AhcFeConfigurationDataV1_HH
#define AhcFeConfigurationDataV1_HH

#include <string>
#include <iostream>

#include "CrcFeConfigurationData.hh"

#include "AhcVfeControl.hh"

class AhcFeConfigurationDataV0;


class AhcFeConfigurationDataV1 : public CrcFeConfigurationData {

public:
  enum {
    versionNumber=1
  };

  AhcFeConfigurationDataV1();
  AhcFeConfigurationDataV1(const AhcFeConfigurationDataV0 &a);

  AhcVfeControl vfeControl() const;
  void          vfeControl(AhcVfeControl v);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //AhcVfeControl _vfeControl;
};


#ifdef CALICE_DAQ_ICC

#include "AhcFeConfigurationDataV0.hh"


AhcFeConfigurationDataV1::AhcFeConfigurationDataV1() {
}

AhcFeConfigurationDataV1::AhcFeConfigurationDataV1(const AhcFeConfigurationDataV0 &a) {
  CrcFeConfigurationData::operator=(a);
  vfeControl(a.vfeControl());
}

AhcVfeControl AhcFeConfigurationDataV1::vfeControl() const {
  return CrcFeConfigurationData::vfeControl();
}
 
void AhcFeConfigurationDataV1::vfeControl(AhcVfeControl v) {
  CrcFeConfigurationData::vfeControl(v.data());
}

std::ostream& AhcFeConfigurationDataV1::print(std::ostream &o, std::string s) const {
  o << s << "AhcFeConfigurationDataV1::print()" << std::endl;

  CrcFeConfigurationData::print(o,s+" ");

  vfeControl().print(o,s+"  ");
  
  return o;
}

#endif
#endif
