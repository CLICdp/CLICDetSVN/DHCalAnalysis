#ifndef AhcSlowRunDataV1_HH
#define AhcSlowRunDataV1_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"

class AhcSlowRunDataV0;


class AhcSlowRunDataV1 {

public:
  enum {
    versionNumber=1
  };

  AhcSlowRunDataV1();
  AhcSlowRunDataV1(const AhcSlowRunDataV0 &a);

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  unsigned module(unsigned p) const;

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  UtlPack _modules[10];
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>

#include "AhcSlowRunDataV0.hh"


AhcSlowRunDataV1::AhcSlowRunDataV1() {
  memset(this,0,sizeof(AhcSlowRunDataV1));
}

AhcSlowRunDataV1::AhcSlowRunDataV1(const AhcSlowRunDataV0 &a) {
  _timeStamp=a.timeStamp();
  memset(this,0,sizeof(AhcSlowRunDataV1));
}

bool AhcSlowRunDataV1::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp;
  if(!sin) return false;

  for(unsigned i(0);i<10;i++) _modules[i]=0;

  unsigned m(0);
  for(unsigned i(0);i<38 && sin;i++) {
    sin >> m;
    _modules[i/4].byte(i%4,m&0xff);
  }

  return sin;

  // CLUDGE
  /*
  for(unsigned i(0);i<38;i++) {
    _modules[i/4].byte(i%4,i+1);
  }

  return true;
  */
}

time_t AhcSlowRunDataV1::timeStamp() const {
  return _timeStamp;
}

void AhcSlowRunDataV1::timeStamp(time_t t) {
  _timeStamp=t;
}

unsigned AhcSlowRunDataV1::module(unsigned p) const {
  assert(p<38);
  return _modules[p/4].byte(p%4);
}

std::ostream& AhcSlowRunDataV1::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowRunDataV1::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " Modules =";
  for(unsigned i(0);i<38;i++) {
    if(i==19) o << std::endl << "           ";
    o << std::setw(3) << module(i);
  }
  o << std::endl;

  return o;
}

#endif
#endif
