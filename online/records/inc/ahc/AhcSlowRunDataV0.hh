#ifndef AhcSlowRunDataV0_HH
#define AhcSlowRunDataV0_HH

#include <string>
#include <iostream>


class AhcSlowRunDataV0 {

public:
  enum {
    versionNumber=0
  };

  AhcSlowRunDataV0();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


AhcSlowRunDataV0::AhcSlowRunDataV0() {
  memset(this,0,sizeof(AhcSlowRunDataV0));
}

bool AhcSlowRunDataV0::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp;
  return sin;
}

time_t AhcSlowRunDataV0::timeStamp() const {
  return _timeStamp;
}

void AhcSlowRunDataV0::timeStamp(time_t t) {
  _timeStamp=t;
}

std::ostream& AhcSlowRunDataV0::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowRunDataV0::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);

  return o;
}

#endif
#endif
