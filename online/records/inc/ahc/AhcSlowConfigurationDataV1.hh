#ifndef AhcSlowConfigurationDataV1_HH
#define AhcSlowConfigurationDataV1_HH

#include <string>
#include <iostream>


class AhcSlowConfigurationDataV1 {

public:
  enum {
    versionNumber=1
  };

  AhcSlowConfigurationDataV1();
  AhcSlowConfigurationDataV1(const AhcSlowConfigurationDataV0 &a);

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  int  xPosition() const;
  void xPosition(int x);

  double mmXPosition() const;
  void   mmXPosition(double x);

  int  yPosition() const;
  void yPosition(int y);

  double mmYPosition() const;
  void   mmYPosition(double y);

  int  zPosition() const;
  void zPosition(int z);

  double mmZPosition() const;
  void   mmZPosition(double z);

  int  cPosition() const;
  void cPosition(int c);

  double degreeCPosition() const;
  void   degreeCPosition(double c);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  int _xPosition;
  int _yPosition;
  int _zPosition;
  int _cPosition;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


AhcSlowConfigurationDataV1::AhcSlowConfigurationDataV1() {
  memset(this,0,sizeof(AhcSlowConfigurationDataV1));
}

AhcSlowConfigurationDataV1::AhcSlowConfigurationDataV1(const AhcSlowConfigurationDataV0 &a) {
  memset(this,0,sizeof(AhcSlowConfigurationDataV1));
  _timeStamp=a.timeStamp();
  _xPosition=a.xPosition();
  _yPosition=a.yPosition();
}

bool AhcSlowConfigurationDataV1::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp;
  if(!sin) return false;

  sin >> _xPosition;
  if(!sin) return false;

  sin >> _yPosition;
  if(!sin) return false;

  sin >> _zPosition;
  // TEMP!!! ALLOW OLD SHORTER STRING FOR NOW ALSO
  //if(!sin) return false;
  if(!sin) {
    _zPosition=0;
    _cPosition=0;
    return true;
  }

  sin >> _cPosition;
  return sin;
}

time_t AhcSlowConfigurationDataV1::timeStamp() const {
  return _timeStamp;
}

void AhcSlowConfigurationDataV1::timeStamp(time_t t) {
  _timeStamp=t;
}

int AhcSlowConfigurationDataV1::xPosition() const {
  return _xPosition;
}

void AhcSlowConfigurationDataV1::xPosition(int x) {
  _xPosition=x;
}

double AhcSlowConfigurationDataV1::mmXPosition() const {
  return 0.1*_xPosition;
}

void AhcSlowConfigurationDataV1::mmXPosition(double x) {
  _xPosition=(int)(10.0*x);
}

int AhcSlowConfigurationDataV1::yPosition() const {
  return _yPosition;
}

void AhcSlowConfigurationDataV1::yPosition(int y) {
  _yPosition=y;
}

double AhcSlowConfigurationDataV1::mmYPosition() const {
  return 0.1*_yPosition;
}

void AhcSlowConfigurationDataV1::mmYPosition(double y) {
  _yPosition=(int)(10.0*y);
}

int AhcSlowConfigurationDataV1::zPosition() const {
  return _zPosition;
}

void AhcSlowConfigurationDataV1::zPosition(int z) {
  _zPosition=z;
}

double AhcSlowConfigurationDataV1::mmZPosition() const {
  return 0.1*_zPosition;
}

void AhcSlowConfigurationDataV1::mmZPosition(double z) {
  _zPosition=(int)(10.0*z);
}

int AhcSlowConfigurationDataV1::cPosition() const {
  return _cPosition;
}

void AhcSlowConfigurationDataV1::cPosition(int c) {
  _cPosition=c;
}

double AhcSlowConfigurationDataV1::degreeCPosition() const {
  return 0.1*_cPosition;
}

void AhcSlowConfigurationDataV1::degreeCPosition(double c) {
  _cPosition=(int)(10.0*c);
}

std::ostream& AhcSlowConfigurationDataV1::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowConfigurationDataV1::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " X position = " << std::setw(6) << _xPosition << " x 0.1 mm = "
    << std::setw(6) << mmXPosition() << " mm" << std::endl;
  o << s << " Y position = " << std::setw(6) << _yPosition << " x 0.1 mm = "
    << std::setw(6) << mmYPosition() << " mm" << std::endl;
  o << s << " Z position = " << std::setw(6) << _zPosition << " x 0.1 mm = "
    << std::setw(6) << mmZPosition() << " mm" << std::endl;
  o << s << " C position = " << std::setw(6) << _cPosition << " x 0.1 degrees = "
    << std::setw(6) << degreeCPosition() << " degrees" << std::endl;

  return o;
}

#endif
#endif
