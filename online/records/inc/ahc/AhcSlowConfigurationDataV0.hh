#ifndef AhcSlowConfigurationDataV0_HH
#define AhcSlowConfigurationDataV0_HH

#include <string>
#include <iostream>


class AhcSlowConfigurationDataV0 {

public:
  enum {
    versionNumber=0
  };

  AhcSlowConfigurationDataV0();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  int  xPosition() const;
  void xPosition(int x);

  double mmXPosition() const;
  void   mmXPosition(double x);

  int  yPosition() const;
  void yPosition(int y);

  double mmYPosition() const;
  void   mmYPosition(double y);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  int _xPosition;
  int _yPosition;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>


AhcSlowConfigurationDataV0::AhcSlowConfigurationDataV0() {
  memset(this,0,sizeof(AhcSlowConfigurationDataV0));
}

bool AhcSlowConfigurationDataV0::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp;
  if(!sin) return false;

  sin >> _xPosition;
  if(!sin) return false;

  sin >> _yPosition;
  return sin;
}

time_t AhcSlowConfigurationDataV0::timeStamp() const {
  return _timeStamp;
}

void AhcSlowConfigurationDataV0::timeStamp(time_t t) {
  _timeStamp=t;
}

int AhcSlowConfigurationDataV0::xPosition() const {
  return _xPosition;
}

void AhcSlowConfigurationDataV0::xPosition(int x) {
  _xPosition=x;
}

double AhcSlowConfigurationDataV0::mmXPosition() const {
  return 0.1*_xPosition;
}

void AhcSlowConfigurationDataV0::mmXPosition(double x) {
  _xPosition=(int)(10.0*x);
}

int AhcSlowConfigurationDataV0::yPosition() const {
  return _yPosition;
}

void AhcSlowConfigurationDataV0::yPosition(int y) {
  _yPosition=y;
}

double AhcSlowConfigurationDataV0::mmYPosition() const {
  return 0.1*_yPosition;
}

void AhcSlowConfigurationDataV0::mmYPosition(double y) {
  _yPosition=(int)(10.0*y);
}

std::ostream& AhcSlowConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowConfigurationDataV0::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " X position = " << std::setw(6) << _xPosition << " x 0.1 mm = "
    << std::setw(6) << mmXPosition() << " mm" << std::endl;
  o << s << " Y position = " << std::setw(6) << _yPosition << " x 0.1 mm = "
    << std::setw(6) << mmYPosition() << " mm" << std::endl;

  return o;
}

#endif
#endif
