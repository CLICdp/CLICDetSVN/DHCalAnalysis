#ifndef AhcVfeStartUpDataCoarse_HH
#define AhcVfeStartUpDataCoarse_HH

#include <string>
#include <iostream>

#include "UtlPack.hh"


class AhcVfeStartUpDataCoarse {

public:
  enum {
    versionNumber=0
  };
 
  AhcVfeStartUpDataCoarse();

  UtlPack verificationData() const;
  void    verificationData(UtlPack v);

  unsigned char dac(unsigned hab, unsigned d) const;
  void          dac(unsigned hab, unsigned d, unsigned char a);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPack _verificationData;
  unsigned char _dac[8][18];
};


#ifdef CALICE_DAQ_ICC

#include "UtlPrintHex.hh"


AhcVfeStartUpDataCoarse::AhcVfeStartUpDataCoarse() : _verificationData(0xaaaa5555) {
  for(unsigned i(0);i<12;i++) {
    for(unsigned j(0);j<18;j++)
      if (i<4 || i >7){
      dac(i,j,18*i+j);
    }
  }
}

UtlPack AhcVfeStartUpDataCoarse::verificationData() const {
  return _verificationData;
}
 
void AhcVfeStartUpDataCoarse::verificationData(UtlPack v) {
  _verificationData=v;
}

unsigned char AhcVfeStartUpDataCoarse::dac(unsigned hab, unsigned d) const {
  assert(d<18);
  assert(hab<4 || hab >7);

  // Numbering is 0-3 and 8-11
  if(     hab<4) return _dac[hab  ][d];
  else if(hab>7) return _dac[hab-4][d];
  else assert(false);
}

void AhcVfeStartUpDataCoarse::dac(unsigned hab, unsigned d, unsigned char a) {
  assert(d<18);
  assert(hab<4 || hab >7);

  // Numbering is 0-3 and 8-11
  if(     hab<4) _dac[hab  ][d]=a;
  else if(hab>7) _dac[hab-4][d]=a;
  else assert(false);
}

std::ostream& AhcVfeStartUpDataCoarse::print(std::ostream &o, std::string s) const {
  o << s << "AhcVfeStartUpDataCoarse::print()" << std::endl;

  o << s << " Verification data = " << printHex(_verificationData) << std::endl;

  for(unsigned i(0);i<12;i++)
    if(i < 4 || i > 7) {
      o << s << " HAB " << std::setw(2);
      o << i;

      o << " DACs = ";
      for(unsigned j(0);j<18;j++) {
	o << std::setw(4) << (unsigned)dac(i,j);
      }
      o << std::endl;
    }  
  
  return o;
}

#endif
#endif
