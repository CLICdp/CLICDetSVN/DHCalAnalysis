#ifndef AhcVfeShiftRegister_HH
#define AhcVfeShiftRegister_HH

#include <string>
#include <iostream>

#include "UtlPackHalf.hh"


class AhcVfeShiftRegister {

public:
  enum Bit {
    swCr      = 0,
    swG       = 4,
    swBuf     = 5,
    swCf      = 6,
    swCp0     =10,
    swRc6Spe2 =11,
    sw2Rc6    =12,
    swRc6Spe1 =13,
    swRc6Spe0 =14,
    swRc6     =15
  };

  AhcVfeShiftRegister();
  AhcVfeShiftRegister(UtlPackHalf s);

  unsigned short data() const;
  void           data(unsigned short d);

  bool shapingCapacitor(unsigned c) const;
  void shapingCapacitor(unsigned c, bool e);

  bool gainCapacitor(unsigned g) const;
  void gainCapacitor(unsigned g, bool e);

  bool injectionResistor() const;
  void injectionResistor(bool e);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  UtlPackHalf _data;
};


#ifdef CALICE_DAQ_ICC


AhcVfeShiftRegister::AhcVfeShiftRegister() : _data(0x01d8) {
}

AhcVfeShiftRegister::AhcVfeShiftRegister(UtlPackHalf s) : _data(s) {
}

unsigned short AhcVfeShiftRegister::data() const {
  return _data.halfWord();
}

void AhcVfeShiftRegister::data(unsigned short d) {
  _data=d;
}

bool AhcVfeShiftRegister::shapingCapacitor(unsigned s) const {
  assert(s<4);
  return _data.bit(s);
}

void AhcVfeShiftRegister::shapingCapacitor(unsigned s, bool e) {
  assert(s<4);
  _data.bit(s,e);
}

bool AhcVfeShiftRegister::gainCapacitor(unsigned g) const {
  assert(g<4);
  return !_data.bit(6+g);
}

void AhcVfeShiftRegister::gainCapacitor(unsigned g, bool e) {
  assert(g<4);
  _data.bit(6+g,!e);
}

bool AhcVfeShiftRegister::injectionResistor() const {
  return _data.bit(10);
}

void AhcVfeShiftRegister::injectionResistor(bool e) {
  _data.bit(10,e);
}

std::ostream& AhcVfeShiftRegister::print(std::ostream &o, std::string s) const {
  o << s << "AhcVfeShiftRegister::print()" << std::endl;
  o << s << " Shift register = " << printHex(_data) << std::endl;

  o << s << "  Shaping capacitors 0-3 enabled =";
  for(unsigned i(0);i<4;i++) {
    if(shapingCapacitor(i)) o << " Y";
    else                    o << " N";
  }
  o << std::endl;

  o << s << "  Gain    capacitors 0-3 enabled =";
  for(unsigned i(0);i<4;i++) {
    if(gainCapacitor(i)) o << " Y";
    else                 o << " N";
  }
  o << std::endl;

  if(injectionResistor()) o << s << "  Injection resistor enabled" << std::endl;
  else                    o << s << "  Injection resistor disabled" << std::endl;

  return o;
}

#endif
#endif
