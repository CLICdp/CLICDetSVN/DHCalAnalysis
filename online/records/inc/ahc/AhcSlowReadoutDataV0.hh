#ifndef AhcSlowReadoutDataV0_HH
#define AhcSlowReadoutDataV0_HH

#include <string>
#include <iostream>

class AhcModuleSlowReadoutData;

class AhcSlowReadoutDataV0 {

public:
  enum {
    versionNumber=0
  };

  AhcSlowReadoutDataV0();

  bool parse(std::string r);
  
  time_t timeStamp() const;
  void   timeStamp(time_t t);

  unsigned numberOfModules() const;
  void     numberOfModules(unsigned n);
  
  const AhcModuleSlowReadoutData* moduleData(unsigned i) const;
        AhcModuleSlowReadoutData* moduleData(unsigned i);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  //time_t _timeStamp; // 8 bytes on 64-bit
  unsigned _timeStamp;
  unsigned _numberOfModules;
};


#ifdef CALICE_DAQ_ICC

#include <time.h>
#include <cstring>

#include "AhcModuleSlowReadoutData.hh"


AhcSlowReadoutDataV0::AhcSlowReadoutDataV0() {
  memset(this,0,sizeof(AhcSlowReadoutDataV0));
}

bool AhcSlowReadoutDataV0::parse(std::string r) {
  std::istringstream sin(r);
  sin >> _timeStamp;
  if(!sin) return false;

  _numberOfModules=0;
  unsigned x(0);
  sin >> x;

  while(sin) {
    _numberOfModules++;

    AhcModuleSlowReadoutData *p(moduleData(_numberOfModules-1));
    p->moduleNumber(x);

    for(unsigned i(0);i<11;i++) {
      sin >> x;
      if(!sin) return false;
      p->temperature(i,x);
    }

    for(unsigned i(0);i<2;i++) {
      sin >> x;
      if(!sin) return false;
      p->highVoltage(i,x);
    }

    for(unsigned i(0);i<5;i++) {
      sin >> x;
      if(!sin) return false;
      p->current(i,x);
    }

    sin >> x;
  }

  return true;
}

time_t AhcSlowReadoutDataV0::timeStamp() const {
  return _timeStamp;
}

void AhcSlowReadoutDataV0::timeStamp(time_t t) {
  _timeStamp=t;
}

unsigned AhcSlowReadoutDataV0::numberOfModules() const {
  return _numberOfModules;
  }

void AhcSlowReadoutDataV0::numberOfModules(unsigned n) {
  _numberOfModules=n;
}

const AhcModuleSlowReadoutData* AhcSlowReadoutDataV0::moduleData(unsigned i) const {
  assert(i<_numberOfModules);
  return ((const AhcModuleSlowReadoutData*)(this+1))+i;
}

AhcModuleSlowReadoutData* AhcSlowReadoutDataV0::moduleData(unsigned i) {
  assert(i<_numberOfModules);
  return ((AhcModuleSlowReadoutData*)(this+1))+i;
}

std::ostream& AhcSlowReadoutDataV0::print(std::ostream &o, std::string s) const {
  o << s << "AhcSlowReadoutDataV0::print()" << std::endl;
  time_t ts(timeStamp());
  o << s << " Timestamp = " << _timeStamp << " = " << ctime(&ts);
  o << s << " Number of modules = " << _numberOfModules << std::endl;

  for(unsigned i(0);i<_numberOfModules;i++) {
    o << s << " Module " << std::setw(2) << i << " ";
    moduleData(i)->print(o,s+" ");
  }

  return o;
}

#endif
#endif
