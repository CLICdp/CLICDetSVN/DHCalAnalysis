#ifndef AhcFeConfigurationDataV0_HH
#define AhcFeConfigurationDataV0_HH

#include <string>
#include <iostream>

#include "CrcFeConfigurationData.hh"

#include "AhcVfeDataV0.hh"
#include "AhcVfeControl.hh"


class AhcFeConfigurationDataV0 : public CrcFeConfigurationData {

public:
  enum {
    versionNumber=0
  };

  AhcFeConfigurationDataV0();

  AhcVfeControl vfeControl() const;
  void          vfeControl(AhcVfeControl v);

  AhcVfeShiftRegister shiftRegister(unsigned hab) const;
  void                shiftRegister(unsigned hab, AhcVfeShiftRegister s);

  unsigned char dac(unsigned hab, unsigned d) const;
  void          dac(unsigned hab, unsigned d, unsigned char c);

  // These should not really be used; saved for backwards compatibility
  const AhcVfeDataV0* srData(unsigned hab=0) const;
        AhcVfeDataV0* srData(unsigned hab=0);
  const AhcVfeDataV0* srData(Connector c, unsigned n=0) const;
        AhcVfeDataV0* srData(Connector c, unsigned n=0);

  std::ostream& print(std::ostream &o, std::string s="") const;


private:
  AhcVfeControl _vfeControl;
  AhcVfeDataV0 _srData[12];
};


#ifdef CALICE_DAQ_ICC



AhcFeConfigurationDataV0::AhcFeConfigurationDataV0() {
}

AhcVfeControl AhcFeConfigurationDataV0::vfeControl() const {
  return _vfeControl;
}

void AhcFeConfigurationDataV0::vfeControl(AhcVfeControl v) {
  _vfeControl=v;
}

AhcVfeShiftRegister AhcFeConfigurationDataV0::shiftRegister(unsigned hab) const {
  assert(hab<12);
  return _srData[hab].shiftRegister();
}

void AhcFeConfigurationDataV0::shiftRegister(unsigned hab, AhcVfeShiftRegister s) {
  assert(hab<12);
  _srData[hab].shiftRegister(s);
}

unsigned char AhcFeConfigurationDataV0::dac(unsigned hab, unsigned d) const {
  assert(hab<12 && d<18);
  return _srData[hab].dac(d);
}

void AhcFeConfigurationDataV0::dac(unsigned hab, unsigned d, unsigned char c) {
  assert(hab<12 && d<18);
  _srData[hab].dac(d,c);
}

const AhcVfeDataV0* AhcFeConfigurationDataV0::srData(unsigned hab) const {
  assert(hab<12);
  return _srData+hab;
}

AhcVfeDataV0* AhcFeConfigurationDataV0::srData(unsigned hab) {
  assert(hab<12);
  return _srData+hab;
}

const AhcVfeDataV0* AhcFeConfigurationDataV0::srData(Connector c, unsigned n) const {
  assert(n<6);
  return _srData+6*c+n;
}

AhcVfeDataV0* AhcFeConfigurationDataV0::srData(Connector c, unsigned n) {
  assert(n<6);
  return _srData+6*c+n;
}

std::ostream& AhcFeConfigurationDataV0::print(std::ostream &o, std::string s) const {
  o << s << "AhcFeConfigurationDataV0::print()" << std::endl;
  CrcFeConfigurationData::print(o,s+" ");

  _vfeControl.print(o,s+" ");

  for(unsigned i(0);i<12;i++) {
    o << s << " HAB " << std::setw(2) << i << " Shift register and DACs" << std::endl;
    _srData[i].print(o,s+"  ");
  }
  
  return o;
}

#endif
#endif
