#include <iostream>
#include <sstream>

#include "DaqRunType.hh"

using namespace std;


int main(int argc, const char **argv) {

  for(unsigned t(0);t<DaqRunType::endOfTypeEnum;t++) {
    if(DaqRunType::knownType((DaqRunType::Type)t)) {

      std::cout << std::endl << "Known DaqRunType = " 
		<< printHex((unsigned char)t) << std::endl;

      DaqRunType d;
      d.type((DaqRunType::Type)t);
      d.print(std::cout," ");
      std::cout << d.typeComment() << std::endl;

      std::string tn(d.typeName());
      std::string tc(d.typeComment());
      for(unsigned i(0);i<tn.size();i++) {
	assert(tn[i]==tc[i]);
      }

    }
  }
  
  return 0;
}
