#include <time.h>

#include <sstream>
#include <iostream>

using namespace std;

int main(int argc, char *argv[]) {
  if(argc<=1) {
    cout << "Usage: printTime <value>" << endl;
    return 1;
  }

  unsigned n(0);
  istringstream i(argv[1]);
  i >> n;

  cout << ctime((const time_t*)&n);
  return 0;
}
