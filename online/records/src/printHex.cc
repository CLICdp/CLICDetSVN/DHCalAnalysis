#include <iostream>
#include <sstream>

#include "UtlPrintHex.hh"

using namespace std;

int main(int argc, char *argv[]) {
  if(argc<=1) {
    cout << "Usage: printHex <value>" << endl;
    return 1;
  }

  unsigned n(0);
  if(argv[1][0]=='0' && argv[1][1]=='b') {
    istringstream i(&argv[1][2]);
    char d('0');
    while(d=='0' || d=='1') {
      i >> d;
      cout << "d = " << d << endl;
      if(d=='0' || d=='1') {
	n<<=1;
	if(d=='1') n++;
      }
    }

  } else if(argv[1][0]=='0' && argv[1][1]=='x') {
    istringstream i(argv[1]);
    i >> hex >> n;

  } else {
    istringstream i(argv[1]);
    i >> n;
  }

  cout << printHex(n) << endl;  

  return 0;
}
