#include <iostream>

#include "RcdArena.hh"
#include "SubRecordType.hh"

int main() {
  std::cout << "sizeof(unsigned)== " << sizeof(unsigned) << std::endl;
  std::cout << "sizeof(int)== " << sizeof(int) << std::endl;
  std::cout << "sizeof(unsigned short)== " << sizeof(unsigned short) << std::endl;
  std::cout << "sizeof(double)== " << sizeof(double) << std::endl;
  std::cout << "sizeof(time_t)== " << sizeof(time_t) << std::endl;
  std::cout << "sizeof(timeval)== " << sizeof(timeval) << std::endl;
  std::cout << std::endl;

  if(sizeof(uint32_t)!=4) {
    std::cout << "sizeof(uint32_t)== " << sizeof(uint32_t) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(uint16_t)!=2) {
    std::cout << "sizeof(uint16_t)== " << sizeof(uint16_t) << " != 2" << std::endl;
    assert(false);
  }

  if(sizeof(uint8_t)!=1) {
    std::cout << "sizeof(uint8_t)== " << sizeof(uint8_t) << " != 1" << std::endl;
    assert(false);
  }

  if(sizeof(int32_t)!=4) {
    std::cout << "sizeof(int32_t)== " << sizeof(int32_t) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(int16_t)!=2) {
    std::cout << "sizeof(int16_t)== " << sizeof(int16_t) << " != 2" << std::endl;
    assert(false);
  }

  if(sizeof(int8_t)!=1) {
    std::cout << "sizeof(int8_t)== " << sizeof(int8_t) << " != 1" << std::endl;
    assert(false);
  }

  if(sizeof(AhcFeConfigurationDataV0)!=332) {
    std::cout << "sizeof(AhcFeConfigurationDataV0)== " << sizeof(AhcFeConfigurationDataV0) << " != 332" << std::endl;
    assert(false);
  }

  if(sizeof(AhcFeConfigurationDataV1)!=88) {
    std::cout << "sizeof(AhcFeConfigurationDataV1)== " << sizeof(AhcFeConfigurationDataV1) << " != 88" << std::endl;
    assert(false);
  }

  if(sizeof(AhcMapping)!=240) {
    std::cout << "sizeof(AhcMapping)== " << sizeof(AhcMapping) << " != 240" << std::endl;
    assert(false);
  }

  if(sizeof(AhcModuleSlowReadoutData)!=48) {
    std::cout << "sizeof(AhcModuleSlowReadoutData)== " << sizeof(AhcModuleSlowReadoutData) << " != 48" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowConfigurationDataV0)!=12) {
    std::cout << "sizeof(AhcSlowConfigurationDataV0)== " << sizeof(AhcSlowConfigurationDataV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowConfigurationDataV1)!=20) {
    std::cout << "sizeof(AhcSlowConfigurationDataV1)== " << sizeof(AhcSlowConfigurationDataV1) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowReadoutDataV0)!=8) {
    std::cout << "sizeof(AhcSlowReadoutDataV0)== " << sizeof(AhcSlowReadoutDataV0) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowReadoutDataV1)!=292) {
    std::cout << "sizeof(AhcSlowReadoutDataV1)== " << sizeof(AhcSlowReadoutDataV1) << " != 292" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowRunDataV0)!=4) {
    std::cout << "sizeof(AhcSlowRunDataV0)== " << sizeof(AhcSlowRunDataV0) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(AhcSlowRunDataV1)!=44) {
    std::cout << "sizeof(AhcSlowRunDataV1)== " << sizeof(AhcSlowRunDataV1) << " != 44" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeConfigurationDataCoarse)!=20) {
    std::cout << "sizeof(AhcVfeConfigurationDataCoarse)== " << sizeof(AhcVfeConfigurationDataCoarse) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeConfigurationDataFine)!=28) {
    std::cout << "sizeof(AhcVfeConfigurationDataFine)== " << sizeof(AhcVfeConfigurationDataFine) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeConfigurationData)!=28) {
    std::cout << "sizeof(AhcVfeConfigurationData)== " << sizeof(AhcVfeConfigurationData) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeControl)!=4) {
    std::cout << "sizeof(AhcVfeControl)== " << sizeof(AhcVfeControl) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeDataV0)!=20) {
    std::cout << "sizeof(AhcVfeDataV0)== " << sizeof(AhcVfeDataV0) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeShiftRegister)!=2) {
    std::cout << "sizeof(AhcVfeShiftRegister)== " << sizeof(AhcVfeShiftRegister) << " != 2" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeStartUpDataCoarse)!=148) {
    std::cout << "sizeof(AhcVfeStartUpDataCoarse)== " << sizeof(AhcVfeStartUpDataCoarse) << " != 148" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeStartUpDataFine)!=220) {
    std::cout << "sizeof(AhcVfeStartUpDataFine)== " << sizeof(AhcVfeStartUpDataFine) << " != 220" << std::endl;
    assert(false);
  }

  if(sizeof(AhcVfeStartUpData)!=220) {
    std::cout << "sizeof(AhcVfeStartUpData)== " << sizeof(AhcVfeStartUpData) << " != 220" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767ConfigurationData)!=8) {
    std::cout << "sizeof(BmlCaen767ConfigurationData)== " << sizeof(BmlCaen767ConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767EventData)!=8) {
    std::cout << "sizeof(BmlCaen767EventData)== " << sizeof(BmlCaen767EventData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767EventDatum)!=4) {
    std::cout << "sizeof(BmlCaen767EventDatum)== " << sizeof(BmlCaen767EventDatum) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767OpcodeData)!=32) {
    std::cout << "sizeof(BmlCaen767OpcodeData)== " << sizeof(BmlCaen767OpcodeData) << " != 32" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767ReadoutConfigurationData)!=8) {
    std::cout << "sizeof(BmlCaen767ReadoutConfigurationData)== " << sizeof(BmlCaen767ReadoutConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767RunData)!=72) {
    std::cout << "sizeof(BmlCaen767RunData)== " << sizeof(BmlCaen767RunData) << " != 72" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767StatusRegister)!=4) {
    std::cout << "sizeof(BmlCaen767StatusRegister)== " << sizeof(BmlCaen767StatusRegister) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767TdcErrorData)!=8) {
    std::cout << "sizeof(BmlCaen767TdcErrorData)== " << sizeof(BmlCaen767TdcErrorData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767TestData)!=8) {
    std::cout << "sizeof(BmlCaen767TestData)== " << sizeof(BmlCaen767TestData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen767TriggerData)!=8) {
    std::cout << "sizeof(BmlCaen767TriggerData)== " << sizeof(BmlCaen767TriggerData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290ConfigurationData)!=16) {
    std::cout << "sizeof(BmlCaen1290ConfigurationData)== " << sizeof(BmlCaen1290ConfigurationData) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290EventData)!=12) {
    std::cout << "sizeof(BmlCaen1290EventData)== " << sizeof(BmlCaen1290EventData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290EventDatum)!=4) {
    std::cout << "sizeof(BmlCaen1290EventDatum)== " << sizeof(BmlCaen1290EventDatum) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290OpcodeData)!=68) {
    std::cout << "sizeof(BmlCaen1290OpcodeData)== " << sizeof(BmlCaen1290OpcodeData) << " != 68" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290ReadoutConfigurationData)!=8) {
    std::cout << "sizeof(BmlCaen1290ReadoutConfigurationData)== " << sizeof(BmlCaen1290ReadoutConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290RunData)!=60) {
    std::cout << "sizeof(BmlCaen1290RunData)== " << sizeof(BmlCaen1290RunData) << " != 60" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290StatusRegister)!=4) {
    std::cout << "sizeof(BmlCaen1290StatusRegister)== " << sizeof(BmlCaen1290StatusRegister) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCaen1290TdcErrorData)!=8) {
    std::cout << "sizeof(BmlCaen1290TdcErrorData)== " << sizeof(BmlCaen1290TdcErrorData) << " != 8" << std::endl;
    assert(false);
  }

  //if(sizeof(BmlCaen1290TestData)!=8) {
  //  std::cout << "sizeof(BmlCaen1290TestData)== " << sizeof(BmlCaen1290TestData) << " != 8" << std::endl;
  //  assert(false);
  //}

  if(sizeof(BmlCaen1290TriggerData)!=12) {
    std::cout << "sizeof(BmlCaen1290TriggerData)== " << sizeof(BmlCaen1290TriggerData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(BmlCernSlowRunData)!=1024) {
    std::cout << "sizeof(BmlCernSlowRunData)== " << sizeof(BmlCernSlowRunData) << " != 1024" << std::endl;
    assert(false);
  }

  if(sizeof(BmlFnalAcquisitionData)!=84) {
    std::cout << "sizeof(BmlFnalAcquisitionData)== " << sizeof(BmlFnalAcquisitionData) << " != 84" << std::endl;
    assert(false);
  }

  if(sizeof(BmlFnalSlowReadoutDataV0)!=224) {
    std::cout << "sizeof(BmlFnalSlowReadoutDataV0)== " << sizeof(BmlFnalSlowReadoutDataV0) << " != 224" << std::endl;
    assert(false);
  }

  if(sizeof(BmlFnalSlowReadoutDataV1)!=280) {
    std::cout << "sizeof(BmlFnalSlowReadoutDataV1)== " << sizeof(BmlFnalSlowReadoutDataV1) << " != 280" << std::endl;
    assert(false);
  }

  if(sizeof(BmlFnalSlowRunData)!=284) {
    std::cout << "sizeof(BmlFnalSlowRunData)== " << sizeof(BmlFnalSlowRunData) << " != 284" << std::endl;
    assert(false);
  }

  if(sizeof(BmlHodEventData)!=8) {
    std::cout << "sizeof(BmlHodEventData)== " << sizeof(BmlHodEventData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlHodRunData)!=16) {
    std::cout << "sizeof(BmlHodRunData)== " << sizeof(BmlHodRunData) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLalHodoscopeConfigurationData)!=276) {
    std::cout << "sizeof(BmlLalHodoscopeConfigurationData)== " << sizeof(BmlLalHodoscopeConfigurationData) << " != 276" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLalHodoscopeEventData)!=12) {
    std::cout << "sizeof(BmlLalHodoscopeEventData)== " << sizeof(BmlLalHodoscopeEventData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLalHodoscopeRunData)!=20) {
    std::cout << "sizeof(BmlLalHodoscopeRunData)== " << sizeof(BmlLalHodoscopeRunData) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLalHodoscopeTriggerData)!=4) {
    std::cout << "sizeof(BmlLalHodoscopeTriggerData)== " << sizeof(BmlLalHodoscopeTriggerData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLc1176ConfigurationData)!=4) {
    std::cout << "sizeof(BmlLc1176ConfigurationData)== " << sizeof(BmlLc1176ConfigurationData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLc1176Csr)!=4) {
    std::cout << "sizeof(BmlLc1176Csr)== " << sizeof(BmlLc1176Csr) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLc1176EventData)!=8) {
    std::cout << "sizeof(BmlLc1176EventData)== " << sizeof(BmlLc1176EventData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLc1176EventDatum)!=4) {
    std::cout << "sizeof(BmlLc1176EventDatum)== " << sizeof(BmlLc1176EventDatum) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLc1176RunData)!=4) {
    std::cout << "sizeof(BmlLc1176RunData)== " << sizeof(BmlLc1176RunData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlLocation)!=4) {
    std::cout << "sizeof(BmlLocation)== " << sizeof(BmlLocation) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(BmlSlowRunData)!=1024) {
    std::cout << "sizeof(BmlSlowRunData)== " << sizeof(BmlSlowRunData) << " != 1024" << std::endl;
    assert(false);
  }

  if(sizeof(CrcAdm1025ConfigurationData)!=24) {
    std::cout << "sizeof(CrcAdm1025ConfigurationData)== " << sizeof(CrcAdm1025ConfigurationData) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(CrcAdm1025RunData)!=4) {
    std::cout << "sizeof(CrcAdm1025RunData)== " << sizeof(CrcAdm1025RunData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcAdm1025SlowReadoutData)!=12) {
    std::cout << "sizeof(CrcAdm1025SlowReadoutData)== " << sizeof(CrcAdm1025SlowReadoutData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(CrcAdm1025Voltages)!=8) {
    std::cout << "sizeof(CrcAdm1025Voltages)== " << sizeof(CrcAdm1025Voltages) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeConfigurationDataV0)!=20) {
    std::cout << "sizeof(CrcBeConfigurationDataV0)== " << sizeof(CrcBeConfigurationDataV0) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeConfigurationDataV1)!=16) {
    std::cout << "sizeof(CrcBeConfigurationDataV1)== " << sizeof(CrcBeConfigurationDataV1) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeEventDataV0)!=28) {
    std::cout << "sizeof(CrcBeEventDataV0)== " << sizeof(CrcBeEventDataV0) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeEventDataV1)!=24) {
    std::cout << "sizeof(CrcBeEventDataV1)== " << sizeof(CrcBeEventDataV1) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeRunData)!=4) {
    std::cout << "sizeof(CrcBeRunData)== " << sizeof(CrcBeRunData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgConfigurationDataV0)!=12) {
    std::cout << "sizeof(CrcBeTrgConfigurationDataV0)== " << sizeof(CrcBeTrgConfigurationDataV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgConfigurationDataV1)!=60) {
    std::cout << "sizeof(CrcBeTrgConfigurationDataV1)== " << sizeof(CrcBeTrgConfigurationDataV1) << " != 60" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgEventData)!=40) {
    std::cout << "sizeof(CrcBeTrgEventData)== " << sizeof(CrcBeTrgEventData) << " != 40" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgHistoryHit)!=8) {
    std::cout << "sizeof(CrcBeTrgHistoryHit)== " << sizeof(CrcBeTrgHistoryHit) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgPollDataV0)!=12) {
    std::cout << "sizeof(CrcBeTrgPollDataV0)== " << sizeof(CrcBeTrgPollDataV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgPollDataV1)!=28) {
    std::cout << "sizeof(CrcBeTrgPollDataV1)== " << sizeof(CrcBeTrgPollDataV1) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(CrcBeTrgRunData)!=8) {
    std::cout << "sizeof(CrcBeTrgRunData)== " << sizeof(CrcBeTrgRunData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcEpromMessage)!=8) {
    std::cout << "sizeof(CrcEpromMessage)== " << sizeof(CrcEpromMessage) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcFeConfigurationData)!=88) {
    std::cout << "sizeof(CrcFeConfigurationData)== " << sizeof(CrcFeConfigurationData) << " != 88" << std::endl;
    assert(false);
  }

  if(sizeof(CrcFeEventData)!=8) {
    std::cout << "sizeof(CrcFeEventData)== " << sizeof(CrcFeEventData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcFeFakeEventData)!=4) {
    std::cout << "sizeof(CrcFeFakeEventData)== " << sizeof(CrcFeFakeEventData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcFeRunData)!=12) {
    std::cout << "sizeof(CrcFeRunData)== " << sizeof(CrcFeRunData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(CrcLm82ConfigurationData)!=4) {
    std::cout << "sizeof(CrcLm82ConfigurationData)== " << sizeof(CrcLm82ConfigurationData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcLm82RunData)!=4) {
    std::cout << "sizeof(CrcLm82RunData)== " << sizeof(CrcLm82RunData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcLm82SlowReadoutData)!=4) {
    std::cout << "sizeof(CrcLm82SlowReadoutData)== " << sizeof(CrcLm82SlowReadoutData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcLocation)!=4) {
    std::cout << "sizeof(CrcLocation)== " << sizeof(CrcLocation) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcReadoutConfigurationDataV0)!=16) {
    std::cout << "sizeof(CrcReadoutConfigurationDataV0)== " << sizeof(CrcReadoutConfigurationDataV0) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(CrcReadoutConfigurationDataV1)!=52) {
    std::cout << "sizeof(CrcReadoutConfigurationDataV1)== " << sizeof(CrcReadoutConfigurationDataV1) << " != 52" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkAdcSample)!=28) {
    std::cout << "sizeof(CrcVlinkAdcSample)== " << sizeof(CrcVlinkAdcSample) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkEventData)!=4) {
    std::cout << "sizeof(CrcVlinkEventData)== " << sizeof(CrcVlinkEventData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkFeData)!=8) {
    std::cout << "sizeof(CrcVlinkFeData)== " << sizeof(CrcVlinkFeData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkFeHeader)!=16) {
    std::cout << "sizeof(CrcVlinkFeHeader)== " << sizeof(CrcVlinkFeHeader) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkHeader)!=8) {
    std::cout << "sizeof(CrcVlinkHeader)== " << sizeof(CrcVlinkHeader) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkTrailer)!=8) {
    std::cout << "sizeof(CrcVlinkTrailer)== " << sizeof(CrcVlinkTrailer) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVlinkTrgData)!=8) {
    std::cout << "sizeof(CrcVlinkTrgData)== " << sizeof(CrcVlinkTrgData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVmeConfigurationData)!=1) {
    std::cout << "sizeof(CrcVmeConfigurationData)== " << sizeof(CrcVmeConfigurationData) << " != 1" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVmeEventData)!=12) {
    std::cout << "sizeof(CrcVmeEventData)== " << sizeof(CrcVmeEventData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(CrcVmeRunData)!=12) {
    std::cout << "sizeof(CrcVmeRunData)== " << sizeof(CrcVmeRunData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(TrgReadoutConfigurationData)!=36) {
    std::cout << "sizeof(TrgReadoutConfigurationData)== " << sizeof(TrgReadoutConfigurationData) << " != 36" << std::endl;
    assert(false);
  }

  if(sizeof(TrgSpillPollData)!=60) {
    std::cout << "sizeof(TrgSpillPollData)== " << sizeof(TrgSpillPollData) << " != 60" << std::endl;
    assert(false);
  }

  if(sizeof(DaqAcquisitionEndV0)!=12) {
    std::cout << "sizeof(DaqAcquisitionEndV0)== " << sizeof(DaqAcquisitionEndV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(DaqAcquisitionEndV1)!=20) {
    std::cout << "sizeof(DaqAcquisitionEndV1)== " << sizeof(DaqAcquisitionEndV1) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DaqAcquisitionStartV0)!=12) {
    std::cout << "sizeof(DaqAcquisitionStartV0)== " << sizeof(DaqAcquisitionStartV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(DaqAcquisitionStartV1)!=20) {
    std::cout << "sizeof(DaqAcquisitionStartV1)== " << sizeof(DaqAcquisitionStartV1) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DaqBurstEnd)!=40) {
    std::cout << "sizeof(DaqBurstEnd)== " << sizeof(DaqBurstEnd) << " != 40" << std::endl;
    assert(false);
  }

  if(sizeof(DaqBurstStart)!=36) {
    std::cout << "sizeof(DaqBurstStart)== " << sizeof(DaqBurstStart) << " != 36" << std::endl;
    assert(false);
  }

  if(sizeof(DaqConfigurationEndV0)!=12) {
    std::cout << "sizeof(DaqConfigurationEndV0)== " << sizeof(DaqConfigurationEndV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(DaqConfigurationEndV1)!=24) {
    std::cout << "sizeof(DaqConfigurationEndV1)== " << sizeof(DaqConfigurationEndV1) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(DaqConfigurationStartV0)!=12) {
    std::cout << "sizeof(DaqConfigurationStartV0)== " << sizeof(DaqConfigurationStartV0) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(DaqConfigurationStartV1)!=56) {
    std::cout << "sizeof(DaqConfigurationStartV1)== " << sizeof(DaqConfigurationStartV1) << " != 56" << std::endl;
    assert(false);
  }

  if(sizeof(DaqConfigurationStartV2)!=60) {
    std::cout << "sizeof(DaqConfigurationStartV2)== " << sizeof(DaqConfigurationStartV2) << " != 60" << std::endl;
    assert(false);
  }

  if(sizeof(DaqEvent)!=12) {
    std::cout << "sizeof(DaqEvent)== " << sizeof(DaqEvent) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(DaqMessage)!=128) {
    std::cout << "sizeof(DaqMessage)== " << sizeof(DaqMessage) << " != 128" << std::endl;
    assert(false);
  }

  if(sizeof(DaqMultiTimer)!=16) {
    std::cout << "sizeof(DaqMultiTimer)== " << sizeof(DaqMultiTimer) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DaqRunEndV0)!=16) {
    std::cout << "sizeof(DaqRunEndV0)== " << sizeof(DaqRunEndV0) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DaqRunEndV1)!=32) {
    std::cout << "sizeof(DaqRunEndV1)== " << sizeof(DaqRunEndV1) << " != 32" << std::endl;
    assert(false);
  }

  if(sizeof(DaqRunStart)!=28) {
    std::cout << "sizeof(DaqRunStart)== " << sizeof(DaqRunStart) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(DaqRunType)!=4) {
    std::cout << "sizeof(DaqRunType)== " << sizeof(DaqRunType) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSequenceEnd)!=8) {
    std::cout << "sizeof(DaqSequenceEnd)== " << sizeof(DaqSequenceEnd) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSequenceStart)!=8) {
    std::cout << "sizeof(DaqSequenceStart)== " << sizeof(DaqSequenceStart) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSequenceType)!=4) {
    std::cout << "sizeof(DaqSequenceType)== " << sizeof(DaqSequenceType) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSlowReadout)!=8) {
    std::cout << "sizeof(DaqSlowReadout)== " << sizeof(DaqSlowReadout) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSoftware)!=180) {
    std::cout << "sizeof(DaqSoftware)== " << sizeof(DaqSoftware) << " != 180" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSpillEnd)!=20) {
    std::cout << "sizeof(DaqSpillEnd)== " << sizeof(DaqSpillEnd) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DaqSpillStart)!=20) {
    std::cout << "sizeof(DaqSpillStart)== " << sizeof(DaqSpillStart) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DaqTransferEnd)!=20) {
    std::cout << "sizeof(DaqTransferEnd)== " << sizeof(DaqTransferEnd) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DaqTransferStart)!=8) {
    std::cout << "sizeof(DaqTransferStart)== " << sizeof(DaqTransferStart) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DaqTrigger)!=16) {
    std::cout << "sizeof(DaqTrigger)== " << sizeof(DaqTrigger) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DaqTwoTimer)!=20) {
    std::cout << "sizeof(DaqTwoTimer)== " << sizeof(DaqTwoTimer) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(DhcBeConfigurationData)!=8) {
    std::cout << "sizeof(DhcBeConfigurationData)== " << sizeof(DhcBeConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DhcDcConfigurationData)!=8) {
    std::cout << "sizeof(DhcDcConfigurationData)== " << sizeof(DhcDcConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DhcDcRunData)!=8) {
    std::cout << "sizeof(DhcDcRunData)== " << sizeof(DhcDcRunData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DhcEventDataV0)!=4) {
    std::cout << "sizeof(DhcEventDataV0)== " << sizeof(DhcEventDataV0) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DhcEventDataV1)!=4) {
    std::cout << "sizeof(DhcEventDataV1)== " << sizeof(DhcEventDataV1) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DhcFeConfigurationDataV0)!=28) {
    std::cout << "sizeof(DhcFeConfigurationDataV0)== " << sizeof(DhcFeConfigurationDataV0) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(DhcFeConfigurationDataV1)!=28) {
    std::cout << "sizeof(DhcFeConfigurationDataV1)== " << sizeof(DhcFeConfigurationDataV1) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(DhcFeHitDataV0)!=16) {
    std::cout << "sizeof(DhcFeHitDataV0)== " << sizeof(DhcFeHitDataV0) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DhcFeHitDataV1)!=16) {
    std::cout << "sizeof(DhcFeHitDataV1)== " << sizeof(DhcFeHitDataV1) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DhcLocation)!=4) {
    std::cout << "sizeof(DhcLocation)== " << sizeof(DhcLocation) << " != 4" << std::endl;
    assert(false);
  }

  //if(sizeof(DhcReadoutConfigurationData)!=64) { OLD???
  if(sizeof(DhcReadoutConfigurationData)!=76) {
    std::cout << "sizeof(DhcReadoutConfigurationData)== " << sizeof(DhcReadoutConfigurationData) << " != 76" << std::endl;
    assert(false);
  }

  if(sizeof(DhcTriggerConfigurationData)!=16) {
    std::cout << "sizeof(DhcTriggerConfigurationData)== " << sizeof(DhcTriggerConfigurationData) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(DhcTriggerData)!=4) {
    std::cout << "sizeof(DhcTriggerData)== " << sizeof(DhcTriggerData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DheEventData)!=4) {
    std::cout << "sizeof(DheEventData)== " << sizeof(DheEventData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DheTriggerData)!=4) {
    std::cout << "sizeof(DheTriggerData)== " << sizeof(DheTriggerData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(DheVfeConfigurationData4)!=292) {
    std::cout << "sizeof(DheVfeConfigurationData4)== " << sizeof(DheVfeConfigurationData4) << " != 292" << std::endl;
    assert(false);
  }

  if(sizeof(DheVfeSlowData)!=72) {
    std::cout << "sizeof(DheVfeSlowData)== " << sizeof(DheVfeSlowData) << " != 72" << std::endl;
    assert(false);
  }

  if(sizeof(DheDifAcquisitionData)!=8) {
    std::cout << "sizeof(DheDifAcquisitionData)== " << sizeof(DheDifAcquisitionData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DheDifConfigurationData)!=8) {
    std::cout << "sizeof(DheDifConfigurationData)== " << sizeof(DheDifConfigurationData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(DheDifRunData)!=44) {
    std::cout << "sizeof(DheDifRunData)== " << sizeof(DheDifRunData) << " != 44" << std::endl;
    assert(false);
  }

  if(sizeof(DheDifEventData)!=4) {
    std::cout << "sizeof(DheDifEventData)== " << sizeof(DheDifEventData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(EmcFeConfigurationData)!=88) {
    std::cout << "sizeof(EmcFeConfigurationData)== " << sizeof(EmcFeConfigurationData) << " != 88" << std::endl;
    assert(false);
  }

  if(sizeof(EmcStageRunData)!=40) {
    std::cout << "sizeof(EmcStageRunData)== " << sizeof(EmcStageRunData) << " != 40" << std::endl;
    assert(false);
  }

  if(sizeof(EmcVfeControl)!=4) {
    std::cout << "sizeof(EmcVfeControl)== " << sizeof(EmcVfeControl) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(IlcBunchTrain)!=8) {
    std::cout << "sizeof(IlcBunchTrain)== " << sizeof(IlcBunchTrain) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(IlcConfigurationEnd)!=20) {
    std::cout << "sizeof(IlcConfigurationEnd)== " << sizeof(IlcConfigurationEnd) << " != 20" << std::endl;
    assert(false);
  }

  if(sizeof(IlcConfigurationStart)!=24) {
    std::cout << "sizeof(IlcConfigurationStart)== " << sizeof(IlcConfigurationStart) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(IlcRunEnd)!=28) {
    std::cout << "sizeof(IlcRunEnd)== " << sizeof(IlcRunEnd) << " != 28" << std::endl;
    assert(false);
  }

  if(sizeof(IlcRunStart)!=24) {
    std::cout << "sizeof(IlcRunStart)== " << sizeof(IlcRunStart) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(IlcRunType)!=4) {
    std::cout << "sizeof(IlcRunType)== " << sizeof(IlcRunType) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(IlcSlowReadout)!=8) {
    std::cout << "sizeof(IlcSlowReadout)== " << sizeof(IlcSlowReadout) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(MpsDigitisationData)!=20016) {
    std::cout << "sizeof(MpsDigitisationData)== " << sizeof(MpsDigitisationData) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(MpsLaserConfigurationData)!=32) {
    std::cout << "sizeof(MpsLaserConfigurationData)== " << sizeof(MpsLaserConfigurationData) << " != 32" << std::endl;
    assert(false);
  }

  if(sizeof(MpsLaserRunData)!=24) {
    std::cout << "sizeof(MpsLaserRunData)== " << sizeof(MpsLaserRunData) << " != 24" << std::endl;
    assert(false);
  }

  if(sizeof(MpsLocation)!=4) {
    std::cout << "sizeof(MpsLocation)== " << sizeof(MpsLocation) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsPcb1ConfigurationData)!=64) {
    std::cout << "sizeof(MpsPcb1ConfigurationData)== " << sizeof(MpsPcb1ConfigurationData) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(MpsPcb1SlowReadoutData)!=4) {
    std::cout << "sizeof(MpsPcb1SlowReadoutData)== " << sizeof(MpsPcb1SlowReadoutData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsReadoutConfigurationData)!=4) {
    std::cout << "sizeof(MpsReadoutConfigurationData)== " << sizeof(MpsReadoutConfigurationData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsSensor1BunchTrainData)!=16) {
    std::cout << "sizeof(MpsSensor1BunchTrainData)== " << sizeof(MpsSensor1BunchTrainData) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(MpsSensor1BunchTrainDatum)!=4) {
    std::cout << "sizeof(MpsSensor1BunchTrainDatum)== " << sizeof(MpsSensor1BunchTrainDatum) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsSensor1Hit)!=4) {
    std::cout << "sizeof(MpsSensor1Hit)== " << sizeof(MpsSensor1Hit) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsSensorV10ConfigurationData)!=20160) {
    std::cout << "sizeof(MpsSensorV10ConfigurationData)== " << sizeof(MpsSensorV10ConfigurationData) << " != 20160" << std::endl;
    assert(false);
  }

  if(sizeof(MpsSensorV12ConfigurationData)!=28224) {
    std::cout << "sizeof(MpsSensorV12ConfigurationData)== " << sizeof(MpsSensorV12ConfigurationData) << " != 28224" << std::endl;
    assert(false);
  }

  if(sizeof(MpsUsbDaqBunchTrainData)!=8) {
    std::cout << "sizeof(MpsUsbDaqBunchTrainData)== " << sizeof(MpsUsbDaqBunchTrainData) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(MpsUsbDaqBunchTrainDatum)!=4) {
    std::cout << "sizeof(MpsUsbDaqBunchTrainDatum)== " << sizeof(MpsUsbDaqBunchTrainDatum) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(MpsUsbDaqConfigurationData)!=112) {
    std::cout << "sizeof(MpsUsbDaqConfigurationData)== " << sizeof(MpsUsbDaqConfigurationData) << " != 112" << std::endl;
    assert(false);
  }

  if(sizeof(MpsUsbDaqRunData)!=12) {
    std::cout << "sizeof(MpsUsbDaqRunData)== " << sizeof(MpsUsbDaqRunData) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(MpsEudetBunchTrainDataV0)!=8) {
    std::cout << "sizeof(MpsEudetBunchTrainDataV0)== " << sizeof(MpsEudetBunchTrainDataV0) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(MpsEudetBunchTrainDataV1)!=16) {
    std::cout << "sizeof(MpsEudetBunchTrainDataV1)== " << sizeof(MpsEudetBunchTrainDataV1) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(MpsEudetBunchTrainDatum)!=4) {
    std::cout << "sizeof(MpsEudetBunchTrainDatum)== " << sizeof(MpsEudetBunchTrainDatum) << " != 4" << std::endl;
    assert(false);
  }

  /*
  if(sizeof(RcdArena)!=4*CALICE_DAQ_SIZE) {
    std::cout << "sizeof(RcdArena)== " << sizeof(RcdArena) << " != 262144" << std::endl;
    assert(false);
  }
  */
  if(sizeof(RcdHeader)!=12) {
    std::cout << "sizeof(RcdHeader)== " << sizeof(RcdHeader) << " != 12" << std::endl;
    assert(false);
  }

  if(sizeof(RcdRecord)!=16) {
    std::cout << "sizeof(RcdRecord)== " << sizeof(RcdRecord) << " != 16" << std::endl;
    assert(false);
  }

  if(sizeof(SceSlowReadoutData)!=124) {
    std::cout << "sizeof(SceSlowReadoutData)== " << sizeof(SceSlowReadoutData) << " != 124" << std::endl;
    assert(false);
  }

  if(sizeof(SceSlowTemperatureData)!=68) {
    std::cout << "sizeof(SceSlowTemperatureData)== " << sizeof(SceSlowTemperatureData) << " != 68" << std::endl;
    assert(false);
  }

  if(sizeof(SimDetectorData)!=80) {
    std::cout << "sizeof(SimDetectorData)== " << sizeof(SimDetectorData) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimDetectorLayer)!=40) {
    std::cout << "sizeof(SimDetectorLayer)== " << sizeof(SimDetectorLayer) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimLayerHits)!=8) {
    std::cout << "sizeof(SimLayerHits)== " << sizeof(SimLayerHits) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimLayerTrack)!=40) {
    std::cout << "sizeof(SimLayerTrack)== " << sizeof(SimLayerTrack) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimParticle)!=64) {
    std::cout << "sizeof(SimParticle)== " << sizeof(SimParticle) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimPixelHit)!=4) {
    std::cout << "sizeof(SimPixelHit)== " << sizeof(SimPixelHit) << " != 64" << std::endl;
    assert(false);
  }

  if(sizeof(SimPrimaryData)!=232) {
    std::cout << "sizeof(SimPrimaryData)== " << sizeof(SimPrimaryData) << " != 332" << std::endl;
    assert(false);
  }

  if(sizeof(SimStep)!=32) {
    std::cout << "sizeof(SimStep)== " << sizeof(SimStep) << " != 40" << std::endl;
    assert(false);
  }

  if(sizeof(SubHeader)!=4) {
    std::cout << "sizeof(SubHeader)== " << sizeof(SubHeader) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(TtmConfigurationDataV0)!=80) {
    std::cout << "sizeof(TtmConfigurationDataV0)== " << sizeof(TtmConfigurationDataV0) << " != 80" << std::endl;
    assert(false);
  }

  if(sizeof(TtmConfigurationDataV1)!=56) {
    std::cout << "sizeof(TtmConfigurationDataV1)== " << sizeof(TtmConfigurationDataV1) << " != 56" << std::endl;
    assert(false);
  }

  if(sizeof(TtmLocation)!=4) {
    std::cout << "sizeof(TtmLocation)== " << sizeof(TtmLocation) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(TtmTriggerData)!=4) {
    std::cout << "sizeof(TtmTriggerData)== " << sizeof(TtmTriggerData) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(UtlLocation)!=4) {
    std::cout << "sizeof(UtlLocation)== " << sizeof(UtlLocation) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(UtlPackHalf)!=2) {
    std::cout << "sizeof(UtlPackHalf)== " << sizeof(UtlPackHalf) << " != 2" << std::endl;
    assert(false);
  }

  if(sizeof(UtlPack)!=4) {
    std::cout << "sizeof(UtlPack)== " << sizeof(UtlPack) << " != 4" << std::endl;
    assert(false);
  }

  if(sizeof(UtlTimeDifference)!=8) {
    std::cout << "sizeof(UtlTimeDifference)== " << sizeof(UtlTimeDifference) << " != 8" << std::endl;
    assert(false);
  }

  if(sizeof(UtlTime)!=8) {
    std::cout << "sizeof(UtlTime)== " << sizeof(UtlTime) << " != 8" << std::endl;
    assert(false);
  }

  std::cout << "All class sizes correct" << std::endl;

  return 0;
}
