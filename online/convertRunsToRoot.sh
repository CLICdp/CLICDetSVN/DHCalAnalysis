#/bin/bash

dataDir=data/run   # directory of the binary data files
rootDir=rootTrees  # directory to store root files
minLayersActive=0  # minimum number of layers with hits in an event

for fileName in `ls ${dataDir}`; do
  runNumber=${fileName:3:6}
  outFile=${rootDir}/dhcTree-Run${runNumber}.root
  if [ -f ${outFile} ]; then
    echo "Skipping run ${runNumber}, file ${outFile} exists."
  else
    echo "Converting run ${runNumber} ..."
    dhcTree -r ${runNumber} -l ${minLayersActive} -w ${rootDir}
    sleep 120
  fi
done
