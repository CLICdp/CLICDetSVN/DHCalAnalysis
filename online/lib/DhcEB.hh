//
// $Id$
//

#ifndef DhcEB_HH
#define DhcEB_HH

#include <iostream>
#include <vector>
#include <map>
#include <deque>
#include <algorithm>

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "DaqEvent.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"

#include "DhcFeHitData.hh"
#include "DhcCableMap.hh"
#include "TtmFifoData.hh"

namespace DhcEb {

bool tscomp(DhcFeHitData* i, DhcFeHitData* j) {
	return (i->timestamp() < j->timestamp());
}

const unsigned TS_MAX = 10000000;            // 1 sec
const unsigned TS_MAX_SPAN = 7 * TS_MAX / 10;    // max range expected
const unsigned TS_MAX_FLOOR = 9 * TS_MAX / 10;   // smallest ts expected before reset
const unsigned TS_ALLOW = 2;                 // 200 nsec
const unsigned BUFFER_COUNT = 16;            // number of counter reset bufffers
}

class DhcEB: public RcdUserRO {

public:
	DhcEB(const unsigned& runNumber);
	virtual ~DhcEB() {
	}

	virtual bool record(const RcdRecord &r) = 0;
	virtual unsigned ttm_flag() = 0;

	virtual const std::vector<DhcFeHitData*>& raw_hitdata() const;
	virtual const std::vector<std::vector<DhcFeHitData*>*>& built_hitdata() const;
	virtual const std::vector<std::vector<DhcPadPts*>*>& built_points();

	virtual void hits2pts(DhcFeHitData* hd, std::vector<DhcPadPts*>* pts);
	virtual void show_points(std::vector<DhcPadPts*>* pts);
	virtual const unsigned layers(std::vector<DhcPadPts*>* pts) const;
	virtual const unsigned maxPtsPerLayer(std::vector<DhcPadPts*>* pts) const;

	virtual void clear();

protected:
	std::vector<DhcFeHitData*> _raw_hitdata;
	std::vector<std::vector<DhcFeHitData*>*> _built_hitdata;
	std::vector<std::vector<DhcPadPts*>*> _built_points;

	virtual void clear_hitdata() = 0;
	virtual void clear_points();

	DhcCableMap* _cmap;
};

class DhcEBIntTrg: public DhcEB {

public:
	DhcEBIntTrg(const unsigned& runNumber);
	virtual ~DhcEBIntTrg() {
	}

	bool record(const RcdRecord &r);
	unsigned ttm_flag();

protected:
	std::map<unsigned, std::vector<DhcFeHitData*>*> _hitd;
	std::map<unsigned, unsigned> _creset;
	unsigned _reset;

	void update();
	void clear_hitdata();
};

class DhcEBExtTrg: public DhcEB {

public:
	DhcEBExtTrg(const unsigned& runNumber);
	virtual ~DhcEBExtTrg() {
	}

	bool record(const RcdRecord &r);
	unsigned ttm_flag();

protected:
	void clear_hitdata();

private:
	std::deque<TtmFifoData*> _ttmtts;
	unsigned _ttmFlag;
};

#ifdef CALICE_DAQ_ICC

DhcEB::DhcEB(const unsigned& runNumber) : RcdUserRO(9) {
	// connect to SQL db.
	_cmap = new DhcCableMap(runNumber);
}

const std::vector<DhcFeHitData*>& DhcEB::raw_hitdata() const {
	return _raw_hitdata;
}

const std::vector<std::vector<DhcFeHitData*>*>& DhcEB::built_hitdata() const {
	return _built_hitdata;
}

const std::vector<std::vector<DhcPadPts*>*>& DhcEB::built_points() {

	if (!_built_hitdata.empty()) {

		std::vector<std::vector<DhcFeHitData*>*>::const_iterator ihd(_built_hitdata.begin());

		for (; ihd!=_built_hitdata.end(); ihd++) {

			std::vector<DhcFeHitData*>* hits(*ihd);
			std::vector<DhcFeHitData*>::const_iterator ih(hits->begin());

			std::vector<DhcPadPts*>* pts = new std::vector<DhcPadPts*>;

			for (ih=hits->begin(); ih!=hits->end(); ih++)
			// get the space points for this DCAL hit data
			hits2pts(*(ih), pts);

			_built_points.push_back(pts);
		}
	}
	return _built_points;
}

const unsigned DhcEB::maxPtsPerLayer(std::vector<DhcPadPts*>* pts) const {

	std::vector<DhcPadPts*>::const_iterator ip(pts->begin());
	std::map<double, unsigned> ptcount;

	for (; ip!=pts->end(); ip++)
	ptcount[(*ip)->Pz]++;

	unsigned count(0);
	std::map<double, unsigned>::const_iterator iptcount(ptcount.begin());
	for (; iptcount!=ptcount.end(); iptcount++)
	count = (*iptcount).second > count ? (*iptcount).second : count;

	return count;
}

const unsigned DhcEB::layers(std::vector<DhcPadPts*>* pts) const {

	std::vector<DhcPadPts*>::const_iterator ip(pts->begin());
	std::vector<double> pz;
	std::vector<double>::const_iterator ipz;

	for (; ip!=pts->end(); ip++) {
		ipz = find(pz.begin(), pz.end(), (*ip)->Pz);
		if (ipz == pz.end())
		pz.push_back((*ip)->Pz);
	}
	return pz.size();
}

void DhcEB::show_points(std::vector<DhcPadPts*>* pts) {

	std::vector<DhcPadPts*>::const_iterator ip(pts->begin());

	std::cout << pts->size() << " points" << std::endl;

	for (ip=pts->begin(); ip!=pts->end(); ip++)
	std::cout << "(" << (*ip)->Px << "," << (*ip)->Py << "," << (*ip)->Pz << "," << (*ip)->Ts << ") ";

	std::cout << std::endl;
}

void DhcEB::clear() {

	clear_hitdata();
	clear_points();
}

void DhcEB::clear_points() {

	if (!_built_points.empty()) {

		std::vector<std::vector<DhcPadPts*>*>::const_iterator ie( _built_points.begin());
		for (; ie!=_built_points.end(); ie++) {

			std::vector<DhcPadPts*>* pts(*ie);
			std::vector<DhcPadPts*>::const_iterator ip(pts->begin());

			// Cleanup
			for (ip=pts->begin(); ip!=pts->end(); ip++)
			delete *ip;

			delete *ie;
		}
	}
	_built_points.clear();
}

void DhcEB::hits2pts(DhcFeHitData* hd, std::vector<DhcPadPts*>* pts) {

	// map the hits to space coords
	std::vector< std::pair<double, double>*>* hits = hd->hitCoords();
	std::vector< std::pair<double, double>*>::const_iterator pt;

	// origin for this FE board
	unsigned crate = hd->vmead() + 220;
	unsigned slot = hd->dcolad() + 4;
	unsigned dconad = hd->dconad();
	unsigned t = hd->timestamp();
	DhcPadPts* org = _cmap->origin(crate, slot, dconad);

	if (org) {
		for (pt=hits->begin(); pt!=hits->end(); pt++) {
			float x = org->Px;
			float y = org->Py;
			float z = org->Pz;
			if (org->Px > 0) {
				x -= (*pt)->first;
				y += (*pt)->second;
			}
			else {
				x += (*pt)->first;
				y -= (*pt)->second;
			}

			// save the coords as DhcPadPts objects
			pts->push_back(new DhcPadPts(x, y, z, t));

			delete *pt;
		}
	}
	delete hits;
}

DhcEBIntTrg::DhcEBIntTrg(const unsigned& runNumber)
: DhcEB(runNumber)
{}

unsigned DhcEBIntTrg::ttm_flag() {
	return 0;
}

bool DhcEBIntTrg::record(const RcdRecord &r) {

	SubAccessor accessor(r);

	switch(r.recordType()) {

		case RcdHeader::configurationStart: {

			std::vector<const DhcReadoutConfigurationData*>
			ro(accessor.access<DhcReadoutConfigurationData>());

			assert(ro.size()>0);

			std::vector<const DhcReadoutConfigurationData*>::const_iterator rt;
			for (rt=ro.begin(); rt!=ro.end(); rt++) {

				for (unsigned s(4); s<20; s++) {
					for (unsigned f(0); f<12; f++) {
						if ((*rt)->slotFeEnable(s,f)) {

							UtlPack loc(0);
							loc.byte(3, ((*rt)->crateNumber()));
							loc.byte(2, s);
							loc.byte(1, f);

							// map this FE board
							_creset[loc.word()] = 0;

							for (unsigned b(0); b<DhcEb::BUFFER_COUNT; b++) {
								loc.byte(0, b);
								_hitd[loc.word()] = new std::vector<DhcFeHitData*>;
							}
						}
					}
				}
			}
			_reset=1;
			break;
		}

		case RcdHeader::configurationEnd: {

			_reset++;
			update();

			break;
		}

		case RcdHeader::trigger: {

			std::map<unsigned, unsigned>::iterator it(_creset.begin());
			bool domerge(true);

			std::vector<const DaqTrigger*>
			dt(accessor.access<DaqTrigger>());

			if (dt.size()==1) {
				// check if counter reset was missed on first readout for some DCOLs
				if (dt[0]->triggerNumberInAcquisition() == 1) {

					unsigned clo(0);
					unsigned chi(0);
					unsigned chan(0);
					for (it=_creset.begin(); it!=_creset.end(); it++) {
						// total reset counts for first, second half of readout
						if (chan++ < _creset.size()/2)
						clo += (*it).second;
						else
						chi += (*it).second;
					}

					if (clo > chi) {
						// if resets occurred only on earlier readouts, make corrections
#ifdef DHC_DEBUG    
						std::cout << "correcting initial reset counts" << std::endl;
#endif // DHC_DEBUG
						for (it=_creset.begin(); it!=_creset.end(); it++) {
							// make  DCOL reset counts equal
							(*it).second = 1;

							// clear unused buffer 1 data
							_hitd[(*it).first + 1]->clear();
						}
					}
				}
			}

#ifdef DHC_DEBUG    

			std::cout << r.recordTime() << "    "
			<< _creset.size() << " channels,   "
			<< _reset << " counter resets" << std::endl;

			for (unsigned s(4); s<20; s++) {
				for (unsigned c(220); c<=221; c++) {
					for (unsigned d(0); d<12; d++) {
						UtlPack loc(0);
						loc.byte(3, c);
						loc.byte(2, s);
						loc.byte(1, d);
						if (_creset.find(loc.word()) == _creset.end())
						std::cout << "   ";
						else
						std::cout << std::setw(2) << _creset[loc.word()] << " ";
					}
					std::cout << "    ";
				}
				std::cout << std::endl;
			}

#endif // DHC_DEBUG
			for (it=_creset.begin(); it!=_creset.end(); it++) {
				// do not merge until all reset counts are equal
				if ((*it).second<_reset) {
					domerge=false;
					break;
				}
			}

			if (domerge) {

				_reset++;
				update();

			}
			break;
		}

		case RcdHeader::event: {

			std::vector<const DaqEvent*>
			de(accessor.access<DaqEvent>());

			unsigned nEvent(0);
			if (de.size()==1)
			nEvent = de[0]->eventNumberInAcquisition();

			std::vector<const DhcLocationData<DhcEventData>*>
			v(accessor.access<DhcLocationData<DhcEventData> >());

			for (unsigned s(0); s<v.size(); s++) {
				if (v[s]->data()->numberOfWords()) {

					UtlPack loc(0);
					loc.byte(3, (v[s]->crateNumber()));
					loc.byte(2, (v[s]->slotNumber()));

					const DhcEventData* fed(v[s]->data());
					unsigned n(v[s]->data()->numberOfWords()/4);

					// reject bad records
					if (nEvent == 0)
					if (!fed->verify())
					continue;

					std::map<unsigned, std::vector<DhcFeHitData*> > vFet;
					for (unsigned i(0); i<n; i++) {

						DhcFeHitData* hits = new DhcFeHitData(*(fed->feData(i)));

						// reject data from FE not mapped
						loc.byte(1,hits->dconad());
						if (_creset.find(loc.word()) == _creset.end()) {
							delete hits;
							continue;
						}

						// sanity checks
						if ((hits->timestamp() > DhcEb::TS_MAX)
								|| (!hits->verify())
								|| (hits->err())
								|| (hits->trg())) {
							delete hits;
							continue;
						}

						hits->vmead(v[s]->crateNumber()&3);
						vFet[hits->dconad()].push_back(hits);
					}

					std::map<unsigned, std::vector<DhcFeHitData*> >::iterator fit(vFet.begin());
					for (; fit!=vFet.end(); fit++) {

						// check for no data
						if ((*fit).second.empty())
						continue;

						loc.byte(1, (*fit).first);
						unsigned bufno = _creset[loc.word()]%DhcEb::BUFFER_COUNT;
						std::vector<DhcFeHitData*>* hitv(_hitd[loc.word()+bufno]);

						if (!hitv->empty()) {
							unsigned tslast = hitv->back()->timestamp();
							std::vector<DhcFeHitData*>::iterator it((*fit).second.begin());

							if (abs((*it)->timestamp() - tslast) > DhcEb::TS_MAX_SPAN) {
								_creset[loc.word()]++;
								bufno = _creset[loc.word()]%DhcEb::BUFFER_COUNT;
							}
						}

						// sort this record by timestamps
						sort((*fit).second.begin(), (*fit).second.end(), DhcEb::tscomp);

						unsigned tslast = (*fit).second.back()->timestamp();
						unsigned tsfirst = (*fit).second.front()->timestamp();

						// store hit data in appropriate buffer
						std::vector<DhcFeHitData*>::const_iterator vt((*fit).second.begin());
						for (; vt!=(*fit).second.end(); vt++) {
							if ((tslast - (*vt)->timestamp()) > DhcEb::TS_MAX_SPAN)
							_hitd[loc.word()+(bufno+1)%DhcEb::BUFFER_COUNT]->push_back((*vt));
							else
							_hitd[loc.word()+bufno]->push_back((*vt));
						}

						// test for buffer switch within this record
						if ((tslast - tsfirst) > DhcEb::TS_MAX_SPAN) {

							if (tslast < DhcEb::TS_MAX_FLOOR) {
								// probably bad data, disable buffers
								_creset.erase(loc.word());
							}
							else
							_creset[loc.word()]++;
						}
					}
				}
			}
			break;
		}

		default: {
			break;
		}
	};

	return true;
}

void DhcEBIntTrg::update() {

	std::map<unsigned, std::vector<DhcFeHitData*>*>::const_iterator it;
	unsigned bufno = (_reset-2)%DhcEb::BUFFER_COUNT;

	std::vector<std::vector<DhcFeHitData*>*> dcol;
	std::vector<std::vector<DhcFeHitData*> > result;

	// sort
	for (it=_hitd.begin(); it!=_hitd.end(); it++) {
		UtlPack loc((*it).first);
		if (loc.byte(0)%DhcEb::BUFFER_COUNT == bufno) {
			std::vector<DhcFeHitData*>* dhc = (*it).second;

			if (!dhc->empty()) {

				sort(dhc->begin(), dhc->end(), DhcEb::tscomp);

				dcol.push_back(dhc);
			}
		}
	}

	// merge
	if (!dcol.empty()) {

		result.push_back(std::vector<DhcFeHitData*>(*dcol[0]));
		for (unsigned i(1); i<dcol.size(); i++) {

			result.push_back(std::vector<DhcFeHitData*>(result[i-1].size()+dcol[i]->size()));

			merge(result[i-1].begin(), result[i-1].end(),
					dcol[i]->begin(), dcol[i]->end(),
					result[i].begin(), DhcEb::tscomp);
		}
	}

	if (!result.empty()) {

		_raw_hitdata = *result.rbegin();
		std::vector<DhcFeHitData*>::const_iterator ih(_raw_hitdata.begin());

		for (ih++; ih<_raw_hitdata.end(); ih++) {

			if (((*ih)->timestamp() - (*(ih-1))->timestamp()) < DhcEb::TS_ALLOW ) {

				// timestamps match
				std::vector<DhcFeHitData*>* ehits = new std::vector<DhcFeHitData*>;

				ehits->push_back(*(ih-1));

				while (((*ih)->timestamp() - (*(ih-1))->timestamp()) < DhcEb::TS_ALLOW ) {

					ehits->push_back(*ih);

					// check for end of data
					if (++ih==_raw_hitdata.end())
					break;
				}
				_built_hitdata.push_back(ehits);
			}
		}
	}
}

void DhcEBIntTrg::clear_hitdata() {

	if (!_built_hitdata.empty()) {

		// clear hit data vectors
		std::vector<std::vector<DhcFeHitData*>*>::const_iterator ihd(_built_hitdata.begin());

		for (; ihd!=_built_hitdata.end(); ihd++)
		delete *ihd;

		_built_hitdata.clear();
		_raw_hitdata.clear();

		// Cleanup hit data buffers
		std::map<unsigned, std::vector<DhcFeHitData*>*>::const_iterator it;
		unsigned bufno = (_reset-2)%DhcEb::BUFFER_COUNT;

		for (it=_hitd.begin(); it!=_hitd.end(); it++) {
			UtlPack loc((*it).first);
			if (loc.byte(0)%DhcEb::BUFFER_COUNT == bufno) {
				std::vector<DhcFeHitData*>* dhc = (*it).second;

				if (!dhc->empty()) {

					std::vector<DhcFeHitData*>::const_iterator dt(dhc->begin());

					for (dt=dhc->begin(); dt!=dhc->end(); dt++)
					delete *dt;

					dhc->clear();
				}
			}
		}
	}
}

DhcEBExtTrg::DhcEBExtTrg(const unsigned& runNumber)
: DhcEB(runNumber)
{}

bool DhcEBExtTrg::record(const RcdRecord &r) {

	SubAccessor accessor(r);

	switch(r.recordType()) {

		case RcdHeader::trigger: {

			std::vector<const TtmLocationData<TtmTriggerData>*>
			v(accessor.access<TtmLocationData<TtmTriggerData> >());

			assert(v.size()==1);

			unsigned n(v[0]->data()->numberOfWords());
			const unsigned* data(v[0]->data()->data());

			_ttmtts.push_back(new TtmFifoData(n, data));
			break;
		}

		case RcdHeader::event: {

			std::vector<const DhcLocationData<DhcEventData>*>
			v(accessor.access<DhcLocationData<DhcEventData> >());

			for (unsigned s(0); s<v.size(); s++) {

				const DhcEventData* fed(v[s]->data());
				unsigned n(v[s]->data()->numberOfWords()/4);

				for (unsigned i(0); i<n; i++) {

					DhcFeHitData* hits = (DhcFeHitData*)fed->feData(i);

					if ((!hits->verify())
							|| (hits->err())
							|| (hits->trg()))
					continue;

					hits->vmead(v[s]->crateNumber()&3);
					_raw_hitdata.push_back(hits);
				}
			}
			if (!_raw_hitdata.empty()) {

				// sort by timestamps
				sort(_raw_hitdata.begin(), _raw_hitdata.end(), DhcEb::tscomp);

				_built_hitdata.push_back(&_raw_hitdata);
			}

			// set TTM FIFO flag
			TtmFifoData* ttmtts = _ttmtts.front();
			unsigned tts = *ttmtts->triggerTS();
			_ttmFlag = 0;

			const unsigned *fifoATS(ttmtts->fifoATS());
			for (unsigned i(0); i<ttmtts->numberOfFifoATS(); i++)
			if (abs(tts - *fifoATS++) < 2)
			_ttmFlag |= 1;

			const unsigned *fifoBTS(ttmtts->fifoBTS());
			for (unsigned i(0); i<ttmtts->numberOfFifoBTS(); i++)
			if (abs(tts - *fifoBTS++) < 2)
			_ttmFlag |= 2;

			_ttmtts.pop_front();

#ifdef DHC_DEBUG    

			if (_ttmFlag) {
				std::cout << r.recordTime();
				std::cout << " ttmFlag " << _ttmFlag << " - "
				<< _raw_hitdata.size() << " chips with hits"
				<< std::endl;
			}

#endif // DHC_DEBUG
			break;
		}

		default: {
			break;
		}
	};

	return true;
}

unsigned DhcEBExtTrg::ttm_flag() {
	return _ttmFlag;;
}

void DhcEBExtTrg::clear_hitdata() {

	if (!_built_hitdata.empty()) {

		// clear hit data vectors
		_built_hitdata.clear();
		_raw_hitdata.clear();
	}
}

#endif // CALICE_DAQ_ICC
#endif // DhcEB_HH
