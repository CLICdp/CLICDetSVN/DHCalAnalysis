#include "DhcEvent.hh"
#include "DhcClustering.hh"

#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;

DhcEvent::~DhcEvent() {
   	layerHits.clear();
    layerClusters2D.clear();
    clusters3D.clear();
    std::set<const DhcHit*>::const_iterator it = hits.begin();
    for ( ; it != hits.end(); it++) {
        const DhcHit* hit = *it;
        delete hit;
        hit = 0;
    }
    hits.clear();
    std::set<const WireChamberHit*>::const_iterator itW = wireChamberHits.begin();
    for ( ; itW != wireChamberHits.end(); itW++) {
        const WireChamberHit* hit = *itW;
        delete hit;
        hit = 0;
    }
    wireChamberHits.clear();
}

void DhcEvent::Clear(Option_t* option) {
	layerHits.clear();
    layerClusters2D.clear();
    clusters3D.clear();
    std::set<const DhcHit*>::const_iterator it = hits.begin();
    for ( ; it != hits.end(); it++) {
        const DhcHit* hit = *it;
        delete hit;
        hit = 0;
    }
    hits.clear();
    std::set<const WireChamberHit*>::const_iterator itW = wireChamberHits.begin();
    for ( ; itW != wireChamberHits.end(); itW++) {
        const WireChamberHit* hit = *itW;
        delete hit;
        hit = 0;
    }
    wireChamberHits.clear();
}

DhcHit* DhcEvent::addHit(double x, double y, double z, int layer, int timeStamp) {
	DhcHit* hit = new DhcHit(x, y, z, layer, timeStamp);
	hits.insert(hit);
	isClustered = false;
	return hit;
}

WireChamberHit* DhcEvent::addWireChamberHit(double x, double y, double z, int layer) {
	WireChamberHit* hit = new WireChamberHit(x, y, z, layer);
	wireChamberHits.insert(hit);
	return hit;
}

map<int, set<const DhcHit*> > DhcEvent::getLayerHitsMap() const {
	if (!isClustered) {
		findClusters();
	}
	return layerHits;
}

set<const DhcHit*> DhcEvent::getLayerHits(int layer) const {
	if (!isClustered) {
		findClusters();
	}
	map<int, set<const DhcHit*> >::const_iterator entry = layerHits.find(layer);
	if (entry == layerHits.end()) {
		return set<const DhcHit*>();
	}
	return entry->second;
}

vector<DhcCluster> DhcEvent::get2DClustersInLayer(int layer) const {
	if (!isClustered) {
		findClusters();
	}
	map<int, vector<DhcCluster> >::const_iterator entry = layerClusters2D.find(layer);
	if (entry == layerClusters2D.end()) {
		return vector<DhcCluster>();
	}
	return entry->second;
}

map<int, vector<DhcCluster> > DhcEvent::get2DClusters(int minHits) const {
    if (!isClustered) {
    	findClusters();
    }
	if (minHits < 2){
    	return layerClusters2D;
    }
    map<int, vector<DhcCluster> > reducedClusterMap;
    map<int, vector<DhcCluster> >::const_iterator map_it = layerClusters2D.begin();
    for ( ; map_it != layerClusters2D.end(); map_it++) {
    	int z = map_it->first;
    	const vector<DhcCluster>& layerClusters = map_it->second;
    	vector<DhcCluster> reducedLayerClusters;
    	vector<DhcCluster>::const_iterator cluster_it = layerClusters.begin();
    	for ( ; cluster_it != layerClusters.end(); cluster_it++) {
    		if (cluster_it->getSize() >= minHits) {
    			reducedLayerClusters.push_back(*cluster_it);
    		}
    	}
    	reducedClusterMap.insert(make_pair(z, reducedLayerClusters));
    }
    return reducedClusterMap;
}

vector<DhcCluster> DhcEvent::get3DClusters(int minHits) const {
	if (!isClustered) {
		findClusters();
	}

	if (minHits < 2) {
		return clusters3D;
	}

	vector<DhcCluster> reducedClusterList;
	vector<DhcCluster>::const_iterator cluster_it = clusters3D.begin();
	for ( ; cluster_it != clusters3D.end(); cluster_it++) {
		if (cluster_it->getSize() >= minHits) {
			reducedClusterList.push_back(*cluster_it);
		}
	}

	return reducedClusterList;
}

void DhcEvent::printClusters() const {
    map<int, vector<DhcCluster> >::const_iterator map_it = layerClusters2D.begin();
    cout << "2D Clusters:" << endl;
    for (; map_it != layerClusters2D.end(); ++map_it) {
    	int layer = map_it->first;
    	vector<DhcCluster> clusters = map_it->second;
    	cout << "\tlayer: " << layer << ", clusters: " << clusters.size() << endl;
        vector<DhcCluster>::const_iterator cluster_it = clusters.begin();
        for (; cluster_it != clusters.end(); ++cluster_it) {
            cout << "\t\t" << cluster_it->toString() << endl;
        }
    }
    cout << "3D Clusters:" << endl;
    vector<DhcCluster>::const_iterator cluster_it = clusters3D.begin();
    for (; cluster_it != clusters3D.end(); ++cluster_it) {
        cout << "\t" << cluster_it->toString() << endl;
    }
}

// Sorts the hits in the event by z;
// Clusters the hits in an event. Simple NN clusterer.
// const list<vector<const DhcHit*> >&
void DhcEvent::findClusters(double xMargin, double yMargin, int layerMargin) const {
	// need to set boolean first to avoid infinite loop
	isClustered = true;
	layerHits = DhcClustering::sortHitsByLayer(hits);
	layerClusters2D = DhcClustering::findAllLayerClusters(this, xMargin, yMargin);
	clusters3D = DhcClustering::find3DClusters(layerClusters2D, xMargin, yMargin, layerMargin);
	
	// calculate dependend variables
	nHits = hits.size();
	nHitLayers = nHitLayers = layerHits.size();
	density = (double) nHits / (double) nHitLayers;

  	map<int, set<const DhcHit*> >::const_iterator entry = layerHits.begin();
    double zSum = 0.;
    double layerSum = 0.;
    maxHitsPerLayer = numeric_limits<int>::min();
    int hitsInPreviousLayer = 0;
    int previousLayer = 0;
    interactionLayer = -1;
    for (; entry != layerHits.end(); ++entry) {
    	int layer = entry->first;
    	int hitsPerLayer = entry->second.size();
    	layerSum += (double) (layer * hitsPerLayer);
    	if (hitsPerLayer > 0) {
    	    double z = (*entry->second.begin())->Z();
    	    zSum += z * hitsPerLayer;
    	}
    	if (hitsPerLayer > maxHitsPerLayer) {
    		maxHitsPerLayer = hitsPerLayer;
    	}
    	if (interactionLayer == -1 && hitsPerLayer >= minClustersForInteractionLayer && hitsInPreviousLayer >= minClustersForInteractionLayer) {
    		interactionLayer = previousLayer;
    	}
    	hitsInPreviousLayer = hitsPerLayer;
    	previousLayer = layer;
    }
    baryCenterZ = zSum / (double) nHits;
    baryCenterLayer = layerSum / (double) nHits;
}

int DhcEvent::getInteractionLayer(int minClusters) {
    if (minClusters != minClustersForInteractionLayer) {
        isClustered = false;
        minClustersForInteractionLayer = minClusters;
    }
	if (!isClustered) {
	    findClusters();
	}
	return interactionLayer;
}

double DhcEvent::getBaryCenterLayer() const {
    if (!isClustered) {
    	findClusters();
	}
	return baryCenterLayer;
}

double DhcEvent::getBaryCenterZ() const {
    if (!isClustered) {
    	findClusters();
	}
	return baryCenterZ;
}

double DhcEvent::getDensity() const {
    if (!isClustered) {
        findClusters();
    }
    return density;
}

int DhcEvent::getNHits() const {
    if (nHits == 0 || !isClustered) {
        nHits = hits.size();
    }
    return nHits;
}

int DhcEvent::getNHitLayers() const {
	if (!isClustered) {
		findClusters();
	}
	return nHitLayers;
}

int DhcEvent::getMaxHitsPerLayer() const {
	if (!isClustered) {
		findClusters();
	}
	return maxHitsPerLayer;
}
