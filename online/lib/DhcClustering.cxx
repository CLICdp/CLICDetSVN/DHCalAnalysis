#include "DhcClustering.hh"

#include <set>
#include <list>
#include <algorithm>
#include <cmath>
#include <iostream>

using std::abs;
using std::cout;
using std::endl;
using std::set;
using std::list;
using std::vector;
using std::map;
using std::make_pair;

bool DhcClustering::compareHitsInZ(const DhcHit* h1, const DhcHit* h2) {
	return h1->Z() < h2->Z();
}

bool DhcClustering::compareHitsByLayer(const DhcHit* h1, const DhcHit* h2) {
	return h1->Layer() < h2->Layer();
}

/**
 * Creates a vector of clusters in 3d starting from a map of layer index to a list of 2d clusters in that layer.
 */
vector<DhcCluster> DhcClustering::find3DClusters(const map<int, vector<DhcCluster> >& clusterMap2D, double marginX,
		double marginY, int layerMargin) {
	vector<DhcCluster> clusters3D;

    //cout << "Starting 3D clustering ..." << endl;    
    
	// store the open clusters and the last z position
	int previousLayer = 0;
	list<DhcCluster> previousClusters;

	map<int, vector<DhcCluster> >::const_iterator map_it = clusterMap2D.begin();
	for (; map_it != clusterMap2D.end(); map_it++) {
		int currentLayer = map_it->first;
		const vector<DhcCluster>& currentClusters = map_it->second;
		if (previousClusters.empty()) {
			// nothing to to, just store the clusters from this layer and continue
			previousClusters.insert(previousClusters.end(), currentClusters.begin(), currentClusters.end());
		} else if (abs(currentLayer - previousLayer) > layerMargin) {
			// the last layer is to far away move all previous clusters to the final list
			clusters3D.insert(clusters3D.end(), previousClusters.begin(), previousClusters.end());
			// keep only the clusters from the current layer for the next iteration
			previousClusters.clear();
			previousClusters.insert(previousClusters.end(), currentClusters.begin(), currentClusters.end());
		} else {
			vector<DhcCluster>::const_iterator currentCluster_it = currentClusters.begin();
			for (; currentCluster_it != currentClusters.end(); currentCluster_it++) {
				DhcCluster currentCluster = *currentCluster_it;
				list<DhcCluster>::iterator previousCluster_it = previousClusters.begin();
				while (previousCluster_it != previousClusters.end()) {
					bool mergeClusters = false;
					DhcCluster& previousCluster = *previousCluster_it;
					if (consistentInX(currentCluster, previousCluster, marginX)
							and consistentInY(currentCluster, previousCluster)) {
						const set<const DhcHit*> currentHits = currentCluster.getHits();
						set<const DhcHit*>::const_iterator currentHit_it = currentHits.begin();
						const set<const DhcHit*> previousHits = previousCluster.getHits();
						for (; currentHit_it != currentHits.end(); currentHit_it++) {
							const DhcHit* currentHit = *currentHit_it;
							set<const DhcHit*>::const_iterator previousHit_it = previousHits.begin();
							for (; previousHit_it != previousHits.end(); previousHit_it++) {
								const DhcHit* previousHit = *previousHit_it;
								if (consistentInX(*currentHit, *previousHit, marginX)
										and consistentInY(*currentHit, *previousHit, marginY)
										and consistentInLayer(*currentHit, *previousHit, layerMargin)) {
									mergeClusters = true;
									break;
								}
							}
							if (mergeClusters) {
								break;
							}
						}
					}
					if (mergeClusters) {
						// merge the matching cluster into the current and delete it from the list of possible clusters
						currentCluster.mergeCluster(previousCluster);
						list<DhcCluster>::iterator deleteMe = previousCluster_it;
						previousCluster_it++;
						previousClusters.erase(deleteMe);
						continue;
					}
					previousCluster_it++;
				}

				// add the merged cluster to the list of previous clusters for future merging
				previousClusters.push_back(currentCluster);
			}
		}

		previousLayer = currentLayer;
	}

	clusters3D.insert(clusters3D.end(), previousClusters.begin(), previousClusters.end());

	return clusters3D;
}

map<int, vector<DhcCluster> > DhcClustering::findAllLayerClusters(const DhcEvent* event, double marginX, double marginY) {
    //cout << "Starting 2D clustering ..." << endl; 
	std::map<int, std::set<const DhcHit*> > layerHitsMap = event->getLayerHitsMap();
	map<int, vector<DhcCluster> > foundClusters;
	std::map<int, std::set<const DhcHit*> >::const_iterator entry = layerHitsMap.begin();
	for (; entry != layerHitsMap.end(); entry++) {
		int z = entry->first;
		set<const DhcHit*> layerHits = entry->second;
		vector<DhcCluster> layerClusters = findClustersXY(layerHits, marginX, marginY);
		foundClusters.insert(make_pair(z, layerClusters));
	}
	return foundClusters;
}

map<int, set<const DhcHit*> > DhcClustering::sortHitsByLayer(const set<const DhcHit*>& hits) {
    //cout << "Starting hit sorting ..." << endl; 
	map<int, set<const DhcHit*> > hitMap;
	vector<const DhcHit*> hitList;
	set<const DhcHit*>::const_iterator hit_it = hits.begin();
	for (; hit_it != hits.end(); hit_it++) {
		hitList.push_back(*hit_it);
	}
	sort(hitList.begin(), hitList.end(), compareHitsInZ);
	while (not hitList.empty()) {
		set<const DhcHit*> layerHits;
		vector<const DhcHit*>::iterator hit_it = hitList.begin();
		int thisLayer = (*hit_it)->Layer();
		for (; hit_it != hitList.end() && (*hit_it)->Layer() == thisLayer; ++hit_it) {
			layerHits.insert(*hit_it);
		}
		hitMap.insert(make_pair(thisLayer, layerHits));
		hitList.erase(hitList.begin(), hit_it);
	}
	return hitMap;
}

vector<DhcCluster> DhcClustering::findClustersXY(const set<const DhcHit*>& hits, double marginX, double marginY) {
	set<const DhcHit*>::const_iterator hit_it = hits.begin();
	list<DhcCluster> openClusters;
	vector<DhcCluster> finalClusters;

	list<DhcCluster>::iterator openCluster_it;
	while (hit_it != hits.end()) {
		const DhcHit* hit = (*hit_it);
		openCluster_it = openClusters.begin();
		while (openCluster_it != openClusters.end()) {
			set<const DhcHit*> clusterHits = (*openCluster_it).getHits();
			set<const DhcHit*>::iterator clusterHit_it = clusterHits.begin();
			while (clusterHit_it != clusterHits.end()) {
				const DhcHit* clusterHit = *clusterHit_it;
				if (consistentInX(*clusterHit, *hit, marginX) and consistentInY(*clusterHit, *hit, marginY)) {
					(*openCluster_it).addHit(hit);
					hit = 0;
					break;
				}
				clusterHit_it++;
			}
			if (not hit) {
				break;
			}
			openCluster_it++;
		}
		if (hit) {
			DhcCluster newCluster;
			newCluster.addHit(hit);
			openClusters.push_back(newCluster);
		}
		hit_it++;
	}

	openCluster_it = openClusters.begin();
	while (openCluster_it != openClusters.end()) {
		DhcCluster& openCluster = *openCluster_it;
		list<DhcCluster>::iterator otherCluster_it = openClusters.begin();
		while (otherCluster_it != openClusters.end()) {
			DhcCluster& otherCluster = *otherCluster_it;
			if (openCluster_it == otherCluster_it
					or not (consistentInX(openCluster, otherCluster, marginX)
							and consistentInY(openCluster, otherCluster, marginY))) {
				otherCluster_it++;
				continue;
			}
			bool clustersMatch = false;
			set<const DhcHit*>::const_iterator clusterHits_it = openCluster.getHits().begin();
			while (clusterHits_it != openCluster.getHits().end()) {
				const DhcHit* clusterHit = *clusterHits_it;
				set<const DhcHit*>::const_iterator otherClusterHits_it = otherCluster.getHits().begin();
				while (otherClusterHits_it != otherCluster.getHits().end()) {
					const DhcHit* otherClusterHit = *otherClusterHits_it;
					if (consistentInX(*clusterHit, *otherClusterHit, marginX)
							and consistentInY(*clusterHit, *otherClusterHit, marginY)) {
						clustersMatch = true;
						break;
					}
					otherClusterHits_it++;
				}
				if (clustersMatch == true) {
					break;
				}
				clusterHits_it++;
			}
			list<DhcCluster>::iterator deleteMe = otherCluster_it;
			otherCluster_it++;
			if (clustersMatch) {
				openCluster.mergeCluster(otherCluster);
				openClusters.erase(deleteMe);
			}
		}
		finalClusters.push_back(openCluster);
		openCluster_it++;
	}
	return finalClusters;
}

bool DhcClustering::consistentInX(const DhcHit& h1, const DhcHit& h2, double xMargin) {
	return abs(h1.X()-h2.X()) <= xMargin;
}

bool DhcClustering::consistentInY(const DhcHit& h1, const DhcHit& h2, double yMargin) {
	return abs(h1.Y() - h2.Y()) <= yMargin;
}

bool DhcClustering::consistentInZ(const DhcHit& h1, const DhcHit& h2, double zMargin) {
	return abs(h1.Z() - h2.Z()) <= zMargin;
}

bool DhcClustering::consistentInLayer(const DhcHit& h1, const DhcHit& h2, int layerMargin) {
	return abs(h1.Layer() - h2.Layer()) <= layerMargin;
}

bool DhcClustering::consistentInX(const DhcCluster& c1, const DhcCluster& c2, double margin) {
	return ((c1.getMinX() <= c2.getMaxX() + margin) and (c2.getMinX() <= c1.getMaxX() + margin));
}

bool DhcClustering::consistentInY(const DhcCluster& c1, const DhcCluster& c2, double margin) {
	return ((c1.getMinY() <= c2.getMaxY() + margin) and (c2.getMinY() <= c1.getMaxY() + margin));
}

bool DhcClustering::consistentInZ(const DhcCluster& c1, const DhcCluster& c2, double margin) {
	return ((c1.getMinZ() <= c2.getMaxZ() + margin) and (c2.getMinZ() <= c1.getMaxZ() + margin));
}

bool DhcClustering::consistentInLayer(const DhcCluster& c1, const DhcCluster& c2, int layerMargin) {
	return ((c1.getMinLayer() <= c2.getMaxLayer() + layerMargin)
			and (c2.getMinLayer() <= c1.getMaxLayer() + layerMargin));
}
