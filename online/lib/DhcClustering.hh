#ifndef DHCCLUSTERING_HH
#define DHCCLUSTERING_HH

#include "DhcHit.hh"
#include "DhcCluster.hh"
#include "DhcEvent.hh"

#include <map>
#include <vector>
#include <set>

namespace DhcClustering {

bool compareHitsInZ(const DhcHit* h1, const DhcHit* h2);
bool compareHitsByLayer(const DhcHit* h1, const DhcHit* h2);

std::vector<DhcCluster> find3DClusters(const std::map<int, std::vector<DhcCluster> >& clusterMap2D,
		double marginX = 10., double marginY = 10., int layerMargin = 1);
std::map<int, std::vector<DhcCluster> > findAllLayerClusters(const DhcEvent* event, double marginX = 10.,
		double marginY = 10.);
std::vector<DhcCluster> findClustersXY(const std::set<const DhcHit*>& hits, double marginX = 10., double marginY = 10.);
std::map<int, std::set<const DhcHit*> > sortHitsByLayer(const std::set<const DhcHit*>& hits);

bool consistentInX(const DhcHit& h1, const DhcHit& h2, double xMargin = 10.);
bool consistentInY(const DhcHit& h1, const DhcHit& h2, double yMargin = 10.);
bool consistentInZ(const DhcHit& h1, const DhcHit& h2, double zMargin = 28.);
bool consistentInLayer(const DhcHit& h1, const DhcHit& h2, int layerMargin = 1);

bool consistentInX(const DhcCluster& c1, const DhcCluster& c2, double xMargin = 10.);
bool consistentInY(const DhcCluster& c1, const DhcCluster& c2, double yMargin = 10.);
bool consistentInZ(const DhcCluster& c1, const DhcCluster& c2, double zMargin = 28.);
bool consistentInLayer(const DhcCluster& c1, const DhcCluster& c2, int layerMargin = 1);

}
#endif
