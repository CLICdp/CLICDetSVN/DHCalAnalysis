
#include "TClonesArray.h"
#include "TTimeStamp.h"
#include "TBits.h"
#include "DhcTree.hh"


using namespace std;


double euclidianDistance(const DhcHit * hit1, const DhcHit * hit2) {
    int x = hit1->GetPx() - hit2->GetPx();
    int y = hit1->GetPy() - hit2->GetPy();
    int z = hit1->GetPz() - hit2->GetPz();
    return sqrt(x * x + y * y + z * z);
}
