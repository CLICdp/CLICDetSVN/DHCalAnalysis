#include <iostream>

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcTree.hh"
#include "DhcTrack.hh"

#include "TTree.h"
#include "TFile.h"

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"
#include <TGClient.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGTab.h>

#include "TH1D.h"
#include "TGraph.h"
#include "TMultiGraph.h"


class SliceStats {

public:
  SliceStats() :
    nExpected(0), nObserved(0), nHits(0)
  {};

  float nExpected;
  float nObserved;
  float nHits;

};

const char *tabnames[] = { "Zenith Angle",
			   "ChiSq",
			   "Expected Rate",
			   "Efficiency",
			   "Multiplicity"};

class CMainFrame : public TGMainFrame {

public:
  CMainFrame(const TGWindow *p,UInt_t w,UInt_t h)
    : TGMainFrame(p,w,h)
{
  SetCleanup(kDeepCleanup);
}

private:
void CloseWindow()
{
  gApplication->Terminate();
}

};

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned maxX(argh.optionArgument('x',480,"Maximum abs(x) (mm)"));
  const unsigned maxY(argh.optionArgument('y',480,"Maximum abs(y) (mm)"));
  const unsigned width(argh.optionArgument('w',1200,"Width"));
  const unsigned height(argh.optionArgument('v',600,"Height"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const unsigned minLayers(argh.optionArgument('l',3,"Minimum layer cut"));
  const unsigned maxLayers(argh.optionArgument('m',39,"Maximum layer cut"));
  const std::string rootPath(argh.optionArgument('f',"root","Path of root tree data"));
  const bool printCanvas(argh.option('p',"Print canvas"));

  const unsigned maxPoints(10);
  const unsigned maxDev(20);
  const float xmax(maxX);
  const float ymax(maxY);
  const float xbin(960);
  const float ybin(320);
  const float zbin(27);
  const float zmax(maxLayers*zbin);
  const float chimax(2);

  if(argh.help()) return 0;

  std::map<unsigned, SliceStats> stats;
  std::map<unsigned, SliceStats >::const_iterator ie;

  std::vector<std::pair<double, double> > slopes;
  std::vector<std::pair<double, double> > chisqs;
  std::vector<std::pair<double, double> >::const_iterator ivp;

  std::ostringstream sout;
  sout << rootPath << "/dhcTree-Run" << runNumber << ".root";

  TFile* tfile = new TFile(sout.str().c_str());
  TTree* tree = (TTree*)tfile->Get("DhcTree");

  Event* event = new Event();
  TBranch* branch = tree->GetBranch("EventBranch");
  branch->SetAddress(&event);

  Int_t nevt = tree->GetEntries();
  for (Int_t i(0); i<nevt; i++) {

    branch->GetEntry(i);
    TClonesArray* pts = event->GetHits();
    
    Int_t nhits(event->GetNhits());

    if (minLayers <= event->GetNhitLayers() &&
	maxPoints >= event->GetMaxHitsPerLayer()) {

      DhcTrack track;
      std::set<Int_t> layer;

      for (Int_t j(0); j<nhits; j++) {
	DhcHit *hit((DhcHit*)((*pts)[j]));

	if (abs(hit->GetPx()) < xmax &&
	    abs(hit->GetPy()) < ymax &&
	    abs(hit->GetPz()) < zmax) {

	  track.addHit(hit);
	  layer.insert(hit->GetPz());
	}
      }

      if (minLayers <= layer.size()) {

	LineFitter lxz, lyz;
	std::set<ClusterPt*> xz_points, yz_points;

	std::set<Int_t>::const_iterator il(layer.begin());
	for (; il!=layer.end(); il++) {

	  std::set<DhcCluster*>* clusters(track.getLayerClusters(*il));

	  std::set<DhcCluster*>::const_iterator ic(clusters->begin());
	  if (clusters->size() == 1) {
	    lxz.addPoint(new ClusterPt((*ic)->getX(), (*ic)->getZ(), (*ic)->getSize()));
	    lyz.addPoint(new ClusterPt((*ic)->getY(), (*ic)->getZ(), (*ic)->getSize()));
	  }

	  for (; ic!=clusters->end(); ic++) {
	    xz_points.insert(new ClusterPt((*ic)->getX(), (*ic)->getZ(), (*ic)->getSize()));
	    yz_points.insert(new ClusterPt((*ic)->getY(), (*ic)->getZ(), (*ic)->getSize()));
	  }

	  clusters->clear();
	  delete clusters;
	}

	if (lxz.fitPoints() && lyz.fitPoints())
	  if (lxz.chi2 < chimax && lyz.chi2 < chimax) {

	    slopes.push_back(std::make_pair(lxz.slope, lyz.slope));
	    chisqs.push_back(std::make_pair(lxz.chi2, lyz.chi2));
	 
	    for (float z(0); z<zmax; z+=zbin) {

	      float x((z-lxz.intercept)/lxz.slope);
	      float y((z-lyz.intercept)/lyz.slope);

	      if (fabs(x) < xmax && fabs(y) < ymax) {

		unsigned char ix((x + xmax) / xbin);
		unsigned char iy((y + ymax) / ybin);
		unsigned char iz(z / zbin);
		UtlPack loc(0);
		loc.byte(0, ix);
		loc.byte(1, iy);
		loc.byte(2, iz);

		stats[loc.word()].nExpected++;

		std::set<ClusterPt*>::const_iterator ixz(xz_points.begin());
		std::set<ClusterPt*>::const_iterator iyz(yz_points.begin());
		bool hxz(false);
		bool hyz(false);
	      
		for (; ixz!= xz_points.end(); ixz++)
		  if (fabs((*ixz)->second - z) < maxDev)
		    if (fabs((*ixz)->first - x) < maxDev) {
		      hxz = true;
		      break;
		    }

		for (; iyz!= yz_points.end(); iyz++)
		  if (fabs((*iyz)->second - z) < maxDev)
		    if (fabs((*iyz)->first - y) < maxDev) {
		      hyz = true;
		      break;
		    }

		if (hxz && hyz) {
		  stats[loc.word()].nObserved++;
		  stats[loc.word()].nHits += (*ixz)->nHits;
		}
	      }
	    }
	  }
	std::set<ClusterPt*>::const_iterator ixz(xz_points.begin());
	std::set<ClusterPt*>::const_iterator iyz(yz_points.begin());
	for (; ixz!=xz_points.end(); ixz++)
	  delete *ixz;
	for (; iyz!=yz_points.end(); iyz++)
	  delete *iyz;
      }
    }
    event->Clear();
  }

  TApplication application("Cosmic Ray Scan Application",0,0);
  gROOT->SetStyle("Bold");
  gStyle->SetOptStat("emr");
  gStyle->SetTitleFontSize(0.04);
  gStyle->SetLabelSize(0.04, "xyz");
  gStyle->SetPalette(1,0);

  TGTab               *fTab;
  TRootEmbeddedCanvas *fEc[5];
  TGCompositeFrame    *fF[5];
  TGLayoutHints       *fL[5];

  CMainFrame* mf = new CMainFrame(gClient->GetRoot(), width, height);
  fTab = new TGTab(mf, 300, 300);

  TGCompositeFrame *tf;
  for (unsigned t(0); t<5; t++) {

    tf = fTab->AddTab(tabnames[t]);
    fL[t] = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
			      kLHintsExpandY, 5, 5, 5, 5);
    fF[t] = new TGCompositeFrame(tf, 60, 60, kHorizontalFrame);

    fEc[t] = new TRootEmbeddedCanvas (tabnames[t],fF[t],width,height);
    if (t<2)
      fEc[t]->GetCanvas()->Divide(3,1);
    else
      fEc[t]->GetCanvas()->Divide(1,1);

    fF[t]->AddFrame(fEc[t], fL[t]);
    tf->AddFrame(fF[t], fL[t]);
  }

  mf->AddFrame(fTab);

  mf->SetWindowName("Muon Beam Analysis");

  mf->MapSubwindows();
  mf->Resize(mf->GetDefaultSize());
  mf->MapWindow();

  double PI2(3.14159/2);
  TH1D xangle("xzAngle", "Zenith angle xz-plane", 100, -PI2, PI2);
  TH1D yangle("yzAngle", "Zenith angle yz-plane", 100, -PI2, PI2);
  TH1D angle("zAngle", "Zenith angle", 100, 0, PI2);

  for (ivp=slopes.begin(); ivp!=slopes.end(); ivp++) {
    double xang((*ivp).first < 0 ?
		-(atan((*ivp).first) + PI2) : PI2 - atan((*ivp).first));
    double yang((*ivp).second < 0 ?
		-(atan((*ivp).second) + PI2) : PI2 - atan((*ivp).second));
    double ang(PI2 - atan(pow((pow((*ivp).first,-2) + pow((*ivp).second,-2)), -0.5)));

    xangle.Fill(xang);
    yangle.Fill(yang);
    angle.Fill(ang);
  }

  fEc[0]->GetCanvas()->cd(1);
  xangle.Draw();

  fEc[0]->GetCanvas()->cd(2);
  yangle.Draw();

  fEc[0]->GetCanvas()->cd(3);
  angle.Draw();

  TH1D xchisq("xzChisq", "ChiSq/NDF xz-plane", 100, 0, chimax);
  TH1D ychisq("yzChisq", "ChiSq/NDF yz-plane", 100, 0, chimax);

  for (ivp=chisqs.begin(); ivp!=chisqs.end(); ivp++) {
    xchisq.Fill((*ivp).first);
    ychisq.Fill((*ivp).second);
  }

  fEc[1]->GetCanvas()->cd(1);
  xchisq.Draw();

  fEc[1]->GetCanvas()->cd(2);
  ychisq.Draw();

  for (unsigned t(0); t<2; t++) {
    fEc[t]->GetCanvas()->Update();
    if (printCanvas) {
      std::ostringstream sout;
      sout << "root/" + std::string(fEc[t]->GetCanvas()->GetName()) + ".png";
      fEc[t]->GetCanvas()->Print(sout.str().c_str(), "png");
    }
  }

  Double_t layer[stats.size()];
  Double_t effic[3][stats.size()/3];
  Double_t multi[3][stats.size()/3];
  Double_t hits[3][stats.size()/3];

  for (ie=stats.begin(); ie!=stats.end(); ie++) {
    UtlPack loc((*ie).first);
    unsigned r(loc.byte(1));
    unsigned l(loc.byte(2));
    layer[l] = l+1;
    effic[r][l] = (((*ie).second).nObserved)/(((*ie).second).nExpected);
    multi[r][l] = (((*ie).second).nHits)/(((*ie).second).nObserved);
    hits[r][l] = (((*ie).second).nExpected);
  }

  TMultiGraph *mge = new TMultiGraph();
  TMultiGraph *mgm = new TMultiGraph();
  TMultiGraph *mgh = new TMultiGraph();
  TGraph* grh[3];
  TGraph* gre[3];
  TGraph* grm[3];

  fEc[2]->GetCanvas()->cd(1);
  for (unsigned i(0); i<3; i++) {
    grh[i] = new TGraph(stats.size()/3, layer, hits[i]);
    grh[i]->SetMarkerColor(2+i);
    grh[i]->SetMarkerStyle(20+i);
    mgh->SetTitle("Expected Hits");
    mgh->SetMinimum(0);
    mgh->Add(grh[i]);
  }
  mgh->Draw("ap");
  mgh->GetXaxis()->SetTitle("LayerZ");

  fEc[3]->GetCanvas()->cd(1);
  for (unsigned i(0); i<3; i++) {
    gre[i] = new TGraph(stats.size()/3, layer, effic[i]);
    gre[i]->SetMarkerColor(2+i);
    gre[i]->SetMarkerStyle(20+i);
    mge->SetTitle("Efficiency");
    mge->SetMinimum(0);
    mge->SetMaximum(1);
    mge->Add(gre[i]);
  }
  mge->Draw("ap");
  mge->GetXaxis()->SetTitle("LayerZ");

  fEc[4]->GetCanvas()->cd(1);
  for (unsigned i(0); i<3; i++) {
    grm[i] = new TGraph(stats.size()/3, layer, multi[i]);
    grm[i]->SetMarkerColor(2+i);
    grm[i]->SetMarkerStyle(20+i);
    mgm->SetTitle("Multiplicity");
    mgm->SetMinimum(0);
    mgm->SetMaximum(2.5);
    mgm->Add(grm[i]);
  }
  mgm->Draw("ap");
  mgm->GetXaxis()->SetTitle("LayerZ");

  for (unsigned t(2); t<=4; t++) {
    fEc[t]->GetCanvas()->Update();
    if (printCanvas) {
      std::ostringstream sout;
      sout << "root/" + std::string(fEc[t]->GetCanvas()->GetName()) + ".png";
      fEc[t]->GetCanvas()->Print(sout.str().c_str(), "png");
    }
  }

  application.Run();

}
