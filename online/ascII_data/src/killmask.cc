#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <bitset>
#include <cassert>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"

#include "RcdArena.hh"
#include "RunReader.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcConfigWriter.hh"
#include "DhcFeHitData.hh"


class DhcKillMask : public RcdUserRO {

public:
  DhcKillMask();
  virtual ~DhcKillMask() {}

  void setNoiseCut(const double& hz=1.0);
  void setCrate(const unsigned& c);
  void setSlot(const unsigned& s);
  void setDcon(const unsigned& d);

  bool record(const RcdRecord &r);
  bool update(RcdRecord &r);

  std::vector<UtlPack>* locations();
  std::vector<double>* noiseRates();
  std::vector< std::pair<double, double>*>* noiseCoords();
  std::vector< std::pair<double, double>*>* killCoords(RcdRecord &r, const unsigned &d);

  double killed() const;

private:
  std::map<unsigned, DhcLocationData<DhcFeConfigurationData>*> _fe;
  std::map<unsigned, std::vector<unsigned>*> _killmap;

  UtlTime _start;
  UtlTimeDifference _elapse;

  double _thresh;
  unsigned _crate;
  unsigned _slot;
};

DhcKillMask::DhcKillMask()
  : RcdUserRO(9) {}

void DhcKillMask::setNoiseCut(const double& hz)
{  _thresh = hz;
}

void DhcKillMask::setCrate(const unsigned& c)
{
  _crate = c;
}

void DhcKillMask::setSlot(const unsigned& s)
{
  _slot = s;
}

bool DhcKillMask::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::acquisitionStart: {
    _start = r.RcdHeader::recordTime();
    break;
  }

  case RcdHeader::acquisitionEnd: {
    _elapse = r.RcdHeader::recordTime() -_start;
    break;
  }

  case RcdHeader::configurationStart: {

    std::vector<const DhcLocationData<DhcFeConfigurationData>*>
      fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

    assert(fe.size()>0);
    for (unsigned f(0); f<fe.size(); f++) {
      if (fe[f]->location().slotNumber() == _slot &&
	  fe[f]->location().crateNumber() == _crate) {

	UtlPack loc(0);
	loc.byte(3, fe[f]->location().crateNumber());
	loc.byte(2, fe[f]->location().slotNumber());
	loc.byte(1, fe[f]->location().dhcComponent());
	loc.byte(0, *fe[f]->data()->chipid());

	if (_fe.find(loc.word()) == _fe.end())
	  _fe[loc.word()] = new DhcLocationData<DhcFeConfigurationData>(*fe[f]);
      }
    }
    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    for (unsigned s(0); s<v.size(); s++) {
      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
	const DhcFeHitData* hits(v[s]->data()->feData(i));
	if (!hits->trg() && !hits->err()) {

	  UtlPack loc(0);
	  loc.byte(3, (v[s]->crateNumber()));
	  loc.byte(2, (v[s]->slotNumber()));
	  loc.byte(1, (hits->dconad()));
	  loc.byte(0, (hits->dcalad())); 

	  if (_killmap.find(loc.word()) == _killmap.end())
	    _killmap[loc.word()] = new std::vector<unsigned>(64,0);

	  std::vector<unsigned>* noise = _killmap[loc.word()];

	  std::bitset<64> hitsHi(hits->hitsHi());
	  std::bitset<64> hitsLo(hits->hitsLo());
	  std::bitset<64> hit(hitsHi<<32 | hitsLo);

	  for (int i(0); i<64; i++) {
	    if (hit.test(i))
	      (*noise)[i]++;
	  }
	}
      }
    }
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}


bool DhcKillMask::update(RcdRecord &r)
{
  SubInserter inserter(r);
  r.initialise(RcdHeader::doNothing);

  assert(_fe.size()>0);
  std::map<unsigned, DhcLocationData<DhcFeConfigurationData>*>::iterator it;
  for (it=_fe.begin(); it!=_fe.end(); it++) {

    UtlPack loc((*it).first);
    unsigned long long killmask(0);
    if ((*it).second->write())
      memcpy(&killmask, ((*it).second)->data()->kill(), sizeof(killmask));

    if (_killmap.find(loc.word()) != _killmap.end()) {
      std::vector<unsigned>* noise = _killmap[loc.word()];

      // update killmask
      for (unsigned i(0); i<64; i++) {
	if ((double)(*noise)[i]/(double)_elapse.seconds() > _thresh)
	  killmask |= (unsigned long long)(1)<<i;
      }
      ((*it).second)->data()->kill(killmask);
    }

    // save fe config data with non-zero killmasks
    if (killmask)
      inserter.insert< DhcLocationData<DhcFeConfigurationData> >((*it).second);
  }

  return true;
}

std::vector<UtlPack>* DhcKillMask::locations() {

  assert(_fe.size()>0);
  std::vector<UtlPack>* locations = new std::vector<UtlPack>;

  std::map<unsigned, DhcLocationData<DhcFeConfigurationData>*>::iterator it;
  for (it=_fe.begin(); it!=_fe.end(); it++) {

    UtlPack loc((*it).first);

    if (_killmap.find(loc.word()) != _killmap.end()) {
      std::vector<unsigned>* noise = _killmap[loc.word()];

      for (unsigned i(0); i<64; i++) {
	if ((*noise)[i])
	  locations->push_back(loc);
      }
    }
  }

  return locations;
}

std::vector<double>* DhcKillMask::noiseRates() {

  assert(_fe.size()>0);
  std::vector<double>* rates = new std::vector<double>;

  std::map<unsigned, DhcLocationData<DhcFeConfigurationData>*>::iterator it;
  for (it=_fe.begin(); it!=_fe.end(); it++) {

    UtlPack loc((*it).first);

    if (_killmap.find(loc.word()) != _killmap.end()) {
      std::vector<unsigned>* noise = _killmap[loc.word()];

      for (unsigned i(0); i<64; i++) {
	if ((*noise)[i])
	  rates->push_back((double)(*noise)[i]/(double)_elapse.seconds());
      }
    }
  }

  return rates;
}

std::vector< std::pair<double, double>*>* DhcKillMask::noiseCoords() {

  assert(_fe.size()>0);
  std::vector< std::pair<double, double>*>* coords
    = new std::vector< std::pair<double, double>*>;

  std::map<unsigned, DhcLocationData<DhcFeConfigurationData>*>::iterator it;
  for (it=_fe.begin(); it!=_fe.end(); it++) {

    UtlPack loc((*it).first);

    if (_killmap.find(loc.word()) != _killmap.end()) {
      std::vector<unsigned>* noise = _killmap[loc.word()];

      for (unsigned i(0); i<64; i++) {
	if ((*noise)[i])
	  coords->push_back(DhcFeHitData::hitCoord(loc.byte(0), i));
      }
    }
  }

  return coords;
}

std::vector< std::pair<double, double>*>* DhcKillMask::killCoords(RcdRecord &r,
								  const unsigned &dcon)
{
  SubAccessor accessor(r);

  std::vector<const DhcLocationData<DhcFeConfigurationData>*>
    fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

  std::vector< std::pair<double, double>*>* coords
    = new std::vector< std::pair<double, double>*>;

  for (unsigned f(0); f<fe.size(); f++) {

    unsigned dconad = fe[f]->location().dhcComponent();
    if (dconad == dcon) {
      unsigned dcalad = *fe[f]->data()->chipid();
      unsigned long long killmask(0);
      memcpy(&killmask, fe[f]->data()->kill(), sizeof(killmask));

      if (killmask)
	for (unsigned i(0); i<64; i++)
	  if (killmask & (unsigned long long)(1)<<i)
	    coords->push_back(DhcFeHitData::hitCoord(dcalad, i));
    }
  }

  return coords;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const std::string noiseCut(argh.optionArgument('n',"100","Noise rate cut (Hz)"));
  const unsigned crate(argh.optionArgument('c',220,"Crate"));
  const unsigned slot(argh.optionArgument('s',5,"Slot"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));

  if(argh.help()) return 0;

  DhcKillMask dkm;
  dkm.setCrate(crate);
  dkm.setSlot(slot);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.open(runNumber);
  
  while(reader.read(*arena))
    dkm.record(*arena);

  reader.close();

  std::istringstream sin(noiseCut);
  double thresh;
  sin >> thresh;

  dkm.setNoiseCut(thresh);
  dkm.update(*arena);

  DhcConfigWriter configWriter("cfg/dhcal/generic/killmask.xml");
  assert(configWriter.save(*arena));

  std::vector<UtlPack>* locations = dkm.locations();
  std::vector<double>* rates = dkm.noiseRates();
  std::vector< std::pair<double, double>*>* coords = dkm.noiseCoords();

  TApplication _application("KillMask Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("e");

  TCanvas *canvas[12];
  TH1D hist[12][2];
  TH2D cmap[12][2];

  for (unsigned dcon(0); dcon<12; dcon++) {

    bool dcon_active(false);
    std::vector<UtlPack>::iterator lt(locations->begin());
    for (; lt!=locations->end(); lt++) {
      if ((*lt).byte(1) == dcon) {
	dcon_active=true;
	break;
      }
    }

    if (dcon_active) {

      std::ostringstream sout;
      sout << "DCON-" << dcon << "-Noise Rates";

      canvas[dcon]=new TCanvas(sout.str().c_str(),sout.str().c_str(),
			       20+20*dcon,40+20*dcon,800,600);
      canvas[dcon]->Divide(2,2);

      hist[dcon][0].SetNameTitle("Value",
				 (std::string("Noise Rates < ")+noiseCut+std::string(" Hz")).c_str());
      hist[dcon][0].SetBins(100000, thresh/100000, thresh);
      hist[dcon][0].GetXaxis()->SetTitle("Hz");
      hist[dcon][1].SetNameTitle("Value",
				 (std::string("Noise Rates > ")+noiseCut+std::string(" Hz")).c_str());
      hist[dcon][1].SetBins(100000, thresh, thresh*100000);
      hist[dcon][1].GetXaxis()->SetTitle("Hz");

      cmap[dcon][0].SetNameTitle("Position", "Pad positions of noise hits");
      cmap[dcon][0].SetBins(48, -48., 0, 32, 0, 32.);
      cmap[dcon][1].SetNameTitle("Position", "Pad positions of kill masks");
      cmap[dcon][1].SetBins(48, -48., 0, 32, 0, 32.);

      std::vector<double>::iterator it(rates->begin());
      for (lt=locations->begin(); lt!=locations->end(); lt++, it++) {
	if ((*lt).byte(1) == dcon) {
	  if ((*it) <= thresh)
	    hist[dcon][0].Fill(*it);
	  if ((*it) > thresh)
	    hist[dcon][1].Fill(*it);
	}
      }

      std::vector< std::pair<double, double>*>::iterator ct(coords->begin());
      lt=locations->begin();
      it=rates->begin();
      for (; ct!=coords->end(); ct++, lt++, it++) {
	if ((*lt).byte(1) == dcon) {
	  cmap[dcon][0].Fill(-(*ct)->first, (*ct)->second, (*it));
	}
      }

      std::vector< std::pair<double, double>*>* killed = dkm.killCoords(*arena, dcon);
      for (ct=killed->begin(); ct!=killed->end(); ct++, it++) {
	cmap[dcon][1].Fill(-(*ct)->first, (*ct)->second);
      }
    
      canvas[dcon]->cd(1)->SetLogx(1);
      hist[dcon][0].Draw();
      canvas[dcon]->cd(2)->SetLogx(1);
      hist[dcon][1].Draw();
      canvas[dcon]->cd(3)->SetLogz(0);
      cmap[dcon][0].Draw("lego");
      canvas[dcon]->cd(4)->SetLogz(0);
      cmap[dcon][1].Draw("box");
      canvas[dcon]->Update();
    }
  }
  _application.Run();
}
