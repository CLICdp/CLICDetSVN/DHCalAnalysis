#include <iostream>
#include <sstream>
#include <stack>
#include <utility>
#include <math.h>
#include <cstdlib>

#include "UtlArguments.hh"

#include <TROOT.h>
#include <TError.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TApplication.h>
#include <TGClient.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGButton.h>
#include <TGLabel.h>
#include <TGSlider.h>
#include <TGStatusBar.h>
#include <TGFrame.h>

#include "TView3D.h"
#include "TPolyMarker3D.h"
#include "TPolyLine3D.h"
#include "TTimer.h"
#include "TText.h"
#include "TTree.h"
#include "TFile.h"

#include "DhcGeom.hh"
#include "DhcTree.hh"
#include "DhcTrack.hh"


class EDispFrame : public TGMainFrame , public TTimer {

  enum ECommandIdentifiers {
    B__PAUSE,
    B__NEXT,
    B__PREVIOUS,
    B__PRINT,
    B__EXIT,
    S_DELAY,
  };

private:
  TRootEmbeddedCanvas *fCanvas;
  TGStatusBar* fStatus;;
  TGTextButton* fPause;
  TGTextButton* fDraw;
  TGTextButton* fPrevious;
  TGLabel* fFaster;
  TGLabel* fSlower;
  TGHSlider* fDelay;
  TPad *pad[5];
  TView *view[5];
  TText *ttmFlag;
  Int_t *triggerBits;

  TPolyMarker3D* _pm3d;
  TPolyLine3D* _pl3d;

  unsigned _runNumber;
  unsigned _eventNumber;
  unsigned _sleepSecs;
  unsigned _minLayers;
  unsigned _maxPoints;
  unsigned _minPoints;
  unsigned _maxTDelta;
  std::string _rootPath;

  std::string _rpcGeom;
  double _bbox;
  double _wangle;
  double _zpivot;
  unsigned _nviews;

  DhcGeom* _dhcgeom;
  bool _doFit;

  TFile* _tfile;
  TTree* _tree;
  TBranch* _branch;
  Event* _event;
  Int_t _maxEvent;
  std::stack<unsigned> _eventStack;

  void setup();
  void cleanup();

  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
  Bool_t Notify();
  bool _running;

  bool DoDraw();
  void DoPrint();

public:
  EDispFrame(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h);
  virtual ~EDispFrame() {}

  void initialize();
  void runNumber(const unsigned& r);
  void rpcGeom(const std::string& g);
  void rootPath(const std::string& f);
  void sleepSecs(const unsigned& s);
  void minLayers(const unsigned& l);
  void maxPoints(const unsigned& p);
  void minPoints(const unsigned& p);
  void maxTDelta(const unsigned& t);
  void bBox(const double& b);
  void wAngle(const double& a);
  void doFit(const bool& b);
};

EDispFrame::EDispFrame(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h)
  : TGMainFrame(p,w,h),
    _pm3d(0),
    _pl3d(0),
    _eventNumber(0xffffffff),
    _nviews(0),
    _doFit(false)
{
  SetCleanup(kDeepCleanup);

  fCanvas = new TRootEmbeddedCanvas ("Event Display",this,w,h);
  AddFrame(fCanvas, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY));

  TGHorizontalFrame* hFrame = new TGHorizontalFrame(this, w, 40);
  fStatus = new TGStatusBar(hFrame, w/2, 40);
  fStatus->SetText("Processing data. Please wait..." ,0);
  hFrame->AddFrame(fStatus, new TGLayoutHints(kLHintsLeft, 4,4,3,4));
  
  TGTextButton* exit = new TGTextButton(hFrame, "&Exit", B__EXIT);
  exit->Associate(this);
  hFrame->AddFrame(exit, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  TGTextButton* print = new TGTextButton(hFrame, "Pr&int", B__PRINT);
  print->Associate(this);
  hFrame->AddFrame(print, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fDraw = new TGTextButton(hFrame, "&Next", B__NEXT);
  fDraw->Associate(this);
  hFrame->AddFrame(fDraw, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fPrevious = new TGTextButton(hFrame, "Pre&vious", B__PREVIOUS);
  fPrevious->Associate(this);
  hFrame->AddFrame(fPrevious, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fPause = new TGTextButton(hFrame, "&Pause", B__PAUSE);
  fPause->Associate(this);
  hFrame->AddFrame(fPause, new TGLayoutHints(kLHintsRight, 4,4,3,4));

  fSlower = new TGLabel(hFrame, "slower");
  hFrame->AddFrame(fSlower, new TGLayoutHints(kLHintsRight, 0,4,10,0));

  fDelay = new TGHSlider(hFrame, 80, kSlider1|kScaleBoth, S_DELAY);
  fDelay->Associate(this);
  fDelay->SetRange(0,8);
  hFrame->AddFrame(fDelay, new TGLayoutHints(kLHintsRight, 0,0,3,4));

  fFaster = new TGLabel(hFrame, "faster");
  hFrame->AddFrame(fFaster, new TGLayoutHints(kLHintsRight, 4,0,10,0));

  AddFrame(hFrame, new TGLayoutHints(kLHintsCenterX, 2,2,2,2));

  SetWindowName("DHCal RPC Event Display");

  MapSubwindows();
  MapWindow();
  MoveResize(x, y);
}

void EDispFrame::initialize()
{
  std::ostringstream sout;
  sout << _rootPath << "/dhcTree-Run" << _runNumber << ".root";

  _tfile = new TFile(sout.str().c_str());
  _tree = (TTree*)_tfile->Get("DhcTree");

  _event = new Event();
  _branch = _tree->GetBranch("EventBranch");
  _branch->SetAddress(&_event);
  _maxEvent = _tree->GetEntries();

  if (_rpcGeom == "Slice") {
    _dhcgeom = new DhcSliceGeom();
  }
  else if (_rpcGeom == "Cubic") {
    _dhcgeom = new DhcCubicGeom();
  }
  else if (_rpcGeom == "CubicTC") {
    _dhcgeom = new DhcCubicTCGeom();
   }
  else if (_rpcGeom == "CubicWTC") {
    _dhcgeom = new DhcCubicWTCGeom();
   }
  else if (_rpcGeom == "Cuboid") {
    _dhcgeom = new DhcCuboidGeom();
  }
  else {
    std::cerr << "Geometry " << _rpcGeom << " not implemented" << std::endl;
    CloseWindow();
  }

  if (_minPoints)
    _maxPoints = 0xffffffff;

  setup();

  fDelay->SetPosition(_sleepSecs);
  SetTime(_sleepSecs*1000+20);
  gSystem->AddTimer(this);
  _running=true;
}

void EDispFrame::CloseWindow()
{
  cleanup();

  gApplication->Terminate();
}

Bool_t EDispFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{
  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_BUTTON:

      switch(parm1) {
      case B__PAUSE:
	if (_running) {
	  _running=false;
	  TTimer::TurnOff();
	  fPause->SetText("&Run");
	} else {
	  _running=true;
	  TTimer::Reset();
	  TTimer::TurnOn();
	  fPause->SetText(" &Pause  ");
	}	  
	break;

      case B__NEXT:
	_eventNumber++;
	fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

	if (!DoDraw())
	  _eventNumber--;;
	break;

      case B__PREVIOUS:
	if (!_eventStack.empty()) {
	  _eventStack.pop();
	  _eventNumber = _eventStack.top();
	}
	else
	  _eventNumber = 0;
	fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

	DoDraw();
	_eventStack.pop();
	break;

      case B__PRINT:
	DoPrint();
	break;

      case B__EXIT:
	CloseWindow();   // this also terminates theApp
	break;

      default:
	break;
      }
      
    default:
      break;
    }

  case kC_HSLIDER:
    switch (GET_SUBMSG(msg)) {
    
    case kSL_RELEASE:

      switch(parm1) {
      case S_DELAY:
	_sleepSecs = fDelay->GetPosition();
	SetTime(_sleepSecs*1000+20);
	TTimer::Reset();
	break;

      default:
	break;
      }

    default:
      break;
    }

  default:
    break;
  }

  return kTRUE;
}

Bool_t EDispFrame::Notify() {

  _eventNumber++;
  fPrevious->SetEnabled(_eventNumber == 0 ? kFALSE : kTRUE);

  if (!DoDraw())
    _eventNumber--;;

  TTimer::Reset();
  return kFALSE;
}

void EDispFrame::cleanup()
{
  // clear points
  if (_pm3d)
    delete _pm3d;;

  // clear lines
  if (_pl3d)
    delete _pl3d;;

  for (int i=0; i<_nviews; i++) {
    if (view[i])
      delete view[i];
    if (pad[i])
      delete pad[i];
  }
}

void EDispFrame::setup()
{
  int iret;
  double rmin[3] = {-_bbox, -_bbox, 0};
  double rmax[3] = {_bbox, _bbox, 2*_bbox};

  // distort bbox based on geometry
  if (_rpcGeom == "CubicTC" || _rpcGeom == "CubicWTC") {
    rmax[2]*=2.4;
  }
  else if (_rpcGeom == "Cuboid") {
    rmax[2]*=1.5;
  }
  double elong = (rmax[2]-rmin[2]) / (rmax[1]-rmin[1]);
  double vSplit = elong/(elong+1);

  TGeoManager::SetVerboseLevel(0);
  _dhcgeom->transform(new TGeoCombiTrans(_zpivot*sin(_wangle/57.2958), 0, 0,
					 new TGeoRotation("y-axis",-90,_wangle,90)));
  TGeoVolume* dhcLayout(_dhcgeom->layout(_bbox));
  const std::vector<DhcView*> views(_dhcgeom->views());
  _nviews = views.size();

  for (int i=0; i<_nviews; i++) {

    fCanvas->GetCanvas()->cd();
    switch (i) {
    case 0:
      pad[i] = (_nviews == 5) ?
	new TPad("Top", "Top", 0, 0, 0.33, 0.33) :
	new TPad("Top", "Top", 0, 0, 1-vSplit, vSplit);
      break;

    case 1:
      pad[i] = (_nviews == 5) ?
	new TPad("Front", "Front", 0, 0.33, 0.33, 0.67) :
	new TPad("Front", "Front", 0, vSplit, 1-vSplit, 1);
      break;

    case 2:
      pad[i] = (_nviews == 5) ?
	new TPad("Side", "Side", 0, 0.67, 0.33, 1) :
	new TPad("Side", "Side", 1-vSplit, vSplit, 1, 1);
      break;

    case 3:
      pad[i] = (_nviews == 5) ?
	new TPad("Pers1", "Pers1", 0.33, 0, 1, 0.5) :
	new TPad("Pers1", "Pers1", 1-vSplit, 0, 1, vSplit);
      break;

    case 4:
      pad[i] = new TPad("Pers2", "Pers2", 0.33, 0.5, 1, 1);
      break;

    default:
      break;
    }

    pad[i]->Draw();
    pad[i]->cd();

    dhcLayout->Draw();

    view[i] = new TView3D(1,rmin,rmax);

    DhcView* v(views[i]);
    view[i]->SetView(v->phi, v->theta, v->psi, iret);
    view[i]->ZoomView(pad[i],v->zoom);

    pad[i]->SetTheta(90 - v->theta);
    pad[i]->SetPhi(-90 - v->phi);

    switch (i) {
    case 0: // top
    case 1: // front
    case 2: // side
      view[i]->SetParallel();
      break;

    case 3:
    case 4:
      view[i]->SetPerspective();
      view[i]->SetDview(2*view[i]->GetDview());
      view[i]->SetDproj(2*view[i]->GetDproj());

      if (elong > 2) {
	view[i]->MoveWindow('l');
	view[i]->MoveWindow('i');
      }
      break;

    default:
      break;
    }
  }

  triggerBits = new Int_t;
  ttmFlag = 0;
}

bool EDispFrame::DoDraw()
{
  while (_eventNumber < _maxEvent) {
    _branch->GetEntry(_eventNumber);

    if (_minLayers <= _event->GetNhitLayers() &&
	_minPoints <=_event->GetMaxHitsPerLayer() &&
	_maxPoints >= _event->GetMaxHitsPerLayer())
      break;

    _event->Clear();
    _eventNumber++;
  }

  if (_eventNumber < _maxEvent) {

    _eventStack.push(_eventNumber);

    TClonesArray* pts = _event->GetHits();
    Int_t nhits(_event->GetNhits());

    std::ostringstream sout;
    sout << "Run " << _runNumber << ", "
	 << "Event " << _event->GetHeader()->GetEvtNum() << ", "
	 << "Time " << _event->GetHeader()->GetEvtTime()->AsString("cl");

    fStatus->SetText(sout.str().c_str(),0);

    fCanvas->GetCanvas()->cd();
  
    // clear TPolyMarker3D points
    if (_pm3d)
      delete _pm3d;;
  
    _pm3d = new TPolyMarker3D(nhits, 20);
    _pm3d->SetMarkerSize(0.5);
    _pm3d->SetMarkerColor(kRed);

    Int_t ts(((DhcHit*)((*pts)[nhits/2]))->GetTs());

    DhcTrack track;
    std::set<Int_t> layer;

    for (Int_t j(0); j<nhits; j++) {
      DhcHit *hits((DhcHit*)((*pts)[j]));
      if (abs(hits->GetTs() - ts) < _maxTDelta) {

	double w(hits->GetPz() < 1200 ? (_wangle/57.2958) : 0);
	double x((hits->GetPx()*cos(w) + (10*_zpivot - hits->GetPz())*sin(w))/10);
	double y((double)(hits->GetPy()/10.));
	double z((hits->GetPx()*sin(w) + hits->GetPz()*cos(w))/10);

	_pm3d->SetPoint(j, x, y, z);

	track.addHit(hits);
	layer.insert(hits->GetPz());
      }
    }

    if (_doFit) {

      LineFitter lxz, lyz;

      std::set<Int_t>::const_iterator il(layer.begin());
      for (; il!=layer.end(); il++) {

	std::set<DhcCluster*>* clusters(track.getLayerClusters(*il));

	if (clusters->size() == 1) {
	  std::set<DhcCluster*>::const_iterator ic(clusters->begin());
	  lxz.addPoint(new ClusterPt((*ic)->getX(), (*ic)->getZ(), (*ic)->getSize()));
	  lyz.addPoint(new ClusterPt((*ic)->getY(), (*ic)->getZ(), (*ic)->getSize()));
	}
	clusters->clear();
	delete clusters;
      }

      // clear TPolyLine3D points
      if (_pl3d)
	delete _pl3d;;
  
      _pl3d = new TPolyLine3D(8);
      _pl3d->SetLineColor(kGreen);
      _pl3d->SetLineWidth(2);

      if (lxz.fitPoints() && lyz.fitPoints()) {

	if (lxz.chi2 > 100 || lyz.chi2 > 100)
	  _pl3d->SetLineColor(kRed);

	Int_t nl(0);
	for (il=layer.begin(); il!=layer.end(); il++, nl++) {
	  float z(*il);
	  float x((z-lxz.intercept)/lxz.slope);
	  float y((z-lyz.intercept)/lyz.slope);
	  _pl3d->SetPoint(nl, x/10, y/10, z/10);
	}
      }
    }

    if (ttmFlag)
      delete ttmFlag;

    // draw points in all views
    for (int i=0; i<_nviews; i++) {
      pad[i]->cd();
      _pm3d->Draw();

      if (_doFit)
	_pl3d->Draw();

      if (i == _nviews-1) {
	std::ostringstream sout;
	_event->GetTriggerBits().Get(triggerBits);
	switch (*triggerBits) {
	case 1:
	  sout << "A ";
	  break;
	case 2:
	  sout << "B ";
	  break;
	case 3:
	  sout << "A&B ";
	  break;
	default:
	  break;
	}
	ttmFlag = new TText(0.9, 0.9, sout.str().c_str());
	ttmFlag->SetTextAlign(33);
	ttmFlag->SetTextSize(0.05);
	ttmFlag->Draw();
      }
      pad[i]->Draw();
    }
    fCanvas->GetCanvas()->Update();

    _event->Clear();
    return true;
  }
  else
    return false;
}

void EDispFrame::DoPrint()
{
  std::ostringstream sout;
  sout << "root/"
       << "Run_" << _runNumber
       << "_Event_" << _event->GetHeader()->GetEvtNum()
       << ".png";

  fCanvas->GetCanvas()->Print(sout.str().c_str(), "png");
}

void EDispFrame::runNumber(const unsigned& r) {
  _runNumber = r;
}
void EDispFrame::sleepSecs(const unsigned& s) {
  _sleepSecs = s;
}
void EDispFrame::minLayers(const unsigned& l) {
  _minLayers = l;
}
void EDispFrame::maxPoints(const unsigned& p) {
  _maxPoints = p;
}
void EDispFrame::minPoints(const unsigned& p) {
  _minPoints = p;
}
void EDispFrame::maxTDelta(const unsigned& t) {
  _maxTDelta = t;
}
void EDispFrame::rpcGeom(const std::string& g) {
  _rpcGeom = g;
}
void EDispFrame::rootPath(const std::string& f) {
  _rootPath = f;
}
void EDispFrame::bBox(const double& b) {
  _bbox = b;
}
void EDispFrame::wAngle(const double& a) {
  _wangle = a;
  _zpivot = 20;
}
void EDispFrame::doFit(const bool& b) {
  _doFit = b;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const int xpos(argh.optionArgument('x',0,"X-pos"));
  const int ypos(argh.optionArgument('y',0,"Y-pos"));
  const unsigned width(argh.optionArgument('w',680,"Width"));
  const unsigned height(argh.optionArgument('v',680,"Height"));
  const unsigned sleepSecs(argh.optionArgument('s',2,"Hold display for s seconds"));
  const unsigned minLayers(argh.optionArgument('l',8,"Minimum hit layers for displayed events"));
  const unsigned maxPoints(argh.optionArgument('p',300,"Maximump points/layer for displayed events"));
  const unsigned minPoints(argh.optionArgument('q',0,"Minimum points/layer for displayed events"));
  const unsigned maxTDelta(argh.optionArgument('t',2,"Max timestamp diff for displayed events"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string rootPath(argh.optionArgument('f',"root","Path where root data is located"));
  const std::string rpcGeom(argh.optionArgument('g',"CubicWTC","Geometry (Slice|Cubic|CubicTC|Cuboid)"));
  const int angle(argh.optionArgument('a',0,"Rotation angle (degrees) of DHCal"));
  const bool doFit(argh.option('d',"Try line fit"));

  if(argh.help()) return 0;

  gROOT->SetStyle();
  gErrorIgnoreLevel=kWarning;
  gStyle->SetCanvasPreferGL(false);

  TApplication _application("Event Display Application",0,0);

  EDispFrame fEd(gClient->GetRoot(), xpos, ypos, width, height);

  // set parameters
  fEd.runNumber(runNumber);
  fEd.rootPath(rootPath);
  fEd.sleepSecs(sleepSecs);
  fEd.minLayers(minLayers);
  fEd.maxPoints(maxPoints);
  fEd.minPoints(minPoints);
  fEd.maxTDelta(maxTDelta);
  fEd.rpcGeom(rpcGeom);
  fEd.bBox(55);
  fEd.wAngle((double)angle);
  fEd.doFit(doFit);
  fEd.initialize();

  _application.Run();

}
