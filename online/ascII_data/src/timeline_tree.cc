#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TGraphAsymmErrors.h"
#include "TCanvas.h"
#include "TH1F.h"

#include "DhcTree.hh"

#include <TSystem.h>
#include "TTree.h"
#include "TFile.h"

#include "UtlPack.hh"
#include "UtlArguments.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const unsigned maxHz(argh.optionArgument('s',10000,"Histogram timescale (Hz)"));
  const unsigned minLayers(argh.optionArgument('l',32,"Minimum hit layers for displayed events"));
  const std::string rootPath(argh.optionArgument('w',"root","Path where root file is written"));

  if(argh.help()) return 0;

  std::map<unsigned, std::vector<double> > sizeEvt;
  std::map<unsigned, std::vector<double> > timeEvt;
  std::map<unsigned, std::vector<unsigned> > tdeltaEvt;
  int tslast(0xffffff);
  int toffset(0);

  std::ostringstream sout;
  sout << rootPath << "/dhcTree-Run" << runNumber << ".root";

  TFile* tfile = new TFile(sout.str().c_str());
  TTree* tree = (TTree*)tfile->Get("DhcTree");

  Event* event = new Event();
  TBranch* branch = tree->GetBranch("EventBranch");
  branch->SetAddress(&event);

  Int_t nevt = tree->GetEntries();
  for (Int_t i(0); i<nevt; i++) {

    branch->GetEntry(i);

    EventHeader* hdr = event->GetHeader();

    TClonesArray* pts = event->GetHits();
    
    Int_t nhits(event->GetNhits());
    Int_t ts(((DhcHit*)((*pts)[nhits/2]))->GetTs());

    unsigned layers(event->GetNhitLayers());
    if (minLayers <= layers) {

      if (tslast < 0xffffff) {

	if (ts < tslast) {
	  tslast -= (int)1e7;
	  toffset += (int)1e7;
	}
	sizeEvt[0].push_back((double)nhits);
	timeEvt[0].push_back((double)((ts + toffset)/1e7));
	tdeltaEvt[0].push_back(ts - tslast);
      }
      tslast = ts;
    }

    event->Clear();
  }

  TApplication _application("Noise Time Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eo");

  TCanvas canvas("Timestamp Timeline/Dist", "Timestamp Timeline/Dist", 800, 600);
  canvas.Divide(1,2);

  TGraphAsymmErrors timeLine(timeEvt[0].size(), &timeEvt[0].front(), &sizeEvt[0].front());
  TH1F timeDelta("timeDelta","timeDelta", 100, 0, double(1./double(maxHz)));

  std::vector<double>::iterator ite(sizeEvt[0].begin());
  for (unsigned i(0); i<timeLine.GetN(); i++) { 
    timeLine.SetPointEYhigh(i, 0);
    timeLine.SetPointEYlow(i, *ite++);
  }

  canvas.cd(1)->SetLogy(1);
  timeLine.SetTitle("Event size Time Line");
  timeLine.GetHistogram()->GetXaxis()->SetTimeDisplay(1);
  timeLine.GetHistogram()->GetXaxis()->SetTimeOffset(13*60*60);

  timeLine.Draw("APZ");

  canvas.cd(2)->SetLogy(1);
  timeDelta.SetNameTitle("Event tDelta", "Event tDelta");
  timeDelta.GetXaxis()->SetTitle("Event tDelta (sec)");

  std::vector<unsigned>::iterator it(tdeltaEvt[0].begin());
  for (; it!=tdeltaEvt[0].end(); it++)
    timeDelta.Fill((*it)/1e7);

  timeDelta.Draw();

  canvas.Update();

  _application.Run();

}
