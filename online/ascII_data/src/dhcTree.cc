#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"
#include "DhcTree.hh"

#include <TSystem.h>
#include "TTree.h"
#include "TFile.h"


class DhcTreeWriter {

private:

  TFile* _tfile;
  TTree* _tree;

  RcdArena* _arena;
  RunReader _reader;

  DhcEB* _dhceb;

  unsigned _runNumber;
  std::string _runPath;
  std::string _rootPath;
  unsigned _minLayers;
  bool _intTrigger;

public:
  DhcTreeWriter();
  virtual ~DhcTreeWriter();

  bool initialize();
  void record();

  void runNumber(const unsigned& r);
  void runPath(const std::string& f);
  void rootPath(const std::string& w);
  void minLayers(const unsigned& l);

};


DhcTreeWriter::DhcTreeWriter()
  : _dhceb(0) {}

DhcTreeWriter::~DhcTreeWriter()
{}

bool DhcTreeWriter::initialize()
{
  _arena = new RcdArena;
  _reader.directory(_runPath);
  assert(_reader.open(_runNumber));

  // determine run type
  while (!_dhceb) {

    _reader.read(*_arena);
    switch (_arena->recordType()) {

    case RcdHeader::runStart: {
      // Access the DaqRunStart
      SubAccessor accessor(*_arena);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::dhcNoise: {
	_dhceb = new DhcEBIntTrg(_runNumber);
	_intTrigger = true;
	break;
      }
      case DaqRunType::beamData:
      case DaqRunType::dhcBeam:
      case DaqRunType::dhcCosmics: {
	_dhceb = new DhcEBExtTrg(_runNumber);
	_intTrigger = false;
	break;
      }

      default: {
	std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
	return false;
	break;
	}
      }
    }
    default:
      break;
    }
  }

  std::ostringstream sout;
  sout << _rootPath << "/dhcTree-Run" << _runNumber << ".root";

  _tfile = new TFile(sout.str().c_str(), "recreate");
  _tree = new TTree("DhcTree", "DhcTree");

  return true;
}

void DhcTreeWriter::record()
{
  Event* event = new Event();
  _tree->Branch("EventBranch", "Event", &event);

  std::vector<std::vector<DhcPadPts*>*> pts;
  std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;

  Int_t eventNumber(0);
  Int_t eventNumberInRun(-1);
  UtlTime recordTime;

  while (1) {

    if (pts.empty()) {
      // read more records until some events are built
      while(_reader.read(*_arena)) {

	if (_arena->totalNumberOfBytes() > sizeof(*_arena)/2)
	  continue;

	if (_arena->recordType() == RcdHeader::event) {
	  eventNumberInRun++;
	  // savelast record time
	  recordTime = _arena->recordTime();
	}
	_dhceb->record(*_arena);

	if (!_dhceb->raw_hitdata().empty()) {

	  // get copy of events
	  pts = _dhceb->built_points();
	  break;
	}
      }
      // if still no events, at end of data
      if (pts.empty())
	break;

      TTimeStamp eventTime(recordTime.timeStamp(), 1000*recordTime.microseconds());

      for (ipts=pts.begin(); ipts!=pts.end(); ipts++) {
	unsigned nHitLayers(_dhceb->layers(*ipts));
	if (_minLayers <= nHitLayers) {

	  if (_intTrigger)
	    event->SetHeader(eventNumber++, eventTime);
	  else
	    event->SetHeader(eventNumberInRun, eventTime);

	  event->SetNhitLayers(nHitLayers);
	  event->SetMaxHitsPerLayer(_dhceb->maxPtsPerLayer(*ipts));

	  const unsigned ttmflag(_dhceb->ttm_flag());
	  event->SetTriggerBits((const Int_t*) &ttmflag);

	  std::vector<DhcPadPts*>::const_iterator ib((*ipts)->begin());
	  std::vector<DhcPadPts*>::const_iterator ie((*ipts)->end());

	  for (; ib!=ie; ib++)
	    event->AddHit((Int_t)(10*(*ib)->Px),
			  (Int_t)(10*(*ib)->Py),
			  (Int_t)(10*(*ib)->Pz),
			  (*ib)->Ts);
	  _tree->Fill();
	  event->Clear();
	}
      }
      if(ipts==pts.end()) {
	// clear events
	_dhceb->clear();
	pts.clear();
      }
    }
  }
  _reader.close();

  _tfile->Write();

  delete _dhceb;
  delete _arena;
}

void DhcTreeWriter::runNumber(const unsigned& r) {
  _runNumber = r;
}
void DhcTreeWriter::runPath(const std::string& f) {
  _runPath = f;
}
void DhcTreeWriter::rootPath(const std::string& w) {
  _rootPath = w;
}
void DhcTreeWriter::minLayers(const unsigned& l) {
  _minLayers = l;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const std::string rootPath(argh.optionArgument('w',"root","Path where root file is written"));
  const unsigned minLayers(argh.optionArgument('l',4,"Min hit layers for recorded events"));

  if(argh.help()) return 0;

  DhcTreeWriter* tw = new DhcTreeWriter;
  // set parameters
  tw->runNumber(runNumber);
  tw->runPath(runPath);
  tw->rootPath(rootPath);
  tw->minLayers(minLayers);

  if (tw->initialize())
    tw->record();
}
