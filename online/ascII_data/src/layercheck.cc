#include <iostream>
#include <map>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TRootEmbeddedCanvas.h"
#include "TCanvas.h"
#include "TGFrame.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TH1F.h"
#include "TAxis.h"
#include "THStack.h"
#include "TLegend.h"
#include "TSpectrum.h"
#include "TText.h"

#include "TSystem.h"
#include "TTimer.h"

#include "DhcEB.hh"

const unsigned MIN_CONFIG_TIME = 30;   // minimum duration (sec) of useful configuration


class DhcHitCounts : public RcdUserRO {

public:
  DhcHitCounts(unsigned l, unsigned p);
  virtual ~DhcHitCounts() {}

  bool record(const RcdRecord &r);
  void update();

  const std::map<unsigned, std::map<double, double>*>& hpl_count() const;
  const std::vector<std::pair<double, unsigned> >& hpe_count() const;
  UtlTime _recordTime;

private:
  std::map<unsigned, std::map<double, double>*> _hplCount;
  std::map<double, double>* _rawCount;
  std::map<double, double>  _layerz;
  std::vector<std::pair<double, unsigned> > _hpeCount;
  double _evtCount;

  DhcEB* _dhceb;
  unsigned _minLayers;
  unsigned _maxPoints;
  unsigned _configurationNumber;
};

DhcHitCounts::DhcHitCounts(unsigned l, unsigned p)
  : RcdUserRO(9), _dhceb(0), _minLayers(l), _maxPoints(p)
{}

const std::map<unsigned, std::map<double, double>*>& DhcHitCounts::hpl_count() const
{
  return _hplCount;
}

const std::vector<std::pair<double, unsigned> >& DhcHitCounts::hpe_count() const
{
  return _hpeCount;
}

bool DhcHitCounts::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {
  case RcdHeader::runStart: {

    std::vector<const DaqRunStart*>
      v(accessor.extract<DaqRunStart>());
    assert(v.size()==1);

    DaqRunType runType=v[0]->runType();
    unsigned runNumber=v[0]->runNumber();

    switch(runType.type()) {

    case DaqRunType::dhcNoise: {
      _dhceb = new DhcEBIntTrg(runNumber);
      break;
    }

    case DaqRunType::dhcBeam:
    case DaqRunType::beamData: {
      _dhceb = new DhcEBExtTrg(runNumber);
      break;
    }

    default: {
      std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
      exit(0);
    }
    }
    // map z-coord to layer number
    DhcCableMap cmap(runNumber);
    DhcPadPts* pt;

    for (unsigned s(4); s<20; s++)
      for (unsigned d(0); d<12; d++)
	if ((pt = cmap.origin(0xdc, s, d)) != 0)
	  _layerz[pt->Pz] = 1;

    double layer(1);
    std::map<double, double>::iterator mit;
    for (mit=_layerz.begin(); mit!=_layerz.end(); mit++)
      (*mit).second = layer++;

    break;
  }

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);
    _rawCount = new std::map<double,double>;
    _configurationNumber = v[0]->configurationNumberInRun();
    _evtCount = 0;
    _recordTime = r.recordTime();

    _dhceb->record(r);
    break;
  }

  default: {  
    _dhceb->record(r);

    if (!_dhceb->raw_hitdata().empty()) {
      std::vector<std::vector<DhcPadPts*>*> pts;
      std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;

      // get copy of events
      pts = _dhceb->built_points();

      for (ipts=pts.begin(); ipts!=pts.end(); ipts++) {
	if (_minLayers <= _dhceb->layers(*ipts) &&
	    _maxPoints > _dhceb->maxPtsPerLayer(*ipts)) {

	  _hpeCount.push_back(std::make_pair((*ipts)->size(), _dhceb->ttm_flag()));
	  _evtCount++;

	  std::vector<DhcPadPts*>::const_iterator ip((*ipts)->begin());
	  for (; ip!=(*ipts)->end(); ip++)
	    (*_rawCount)[_layerz[(*ip)->Pz]]++;
	}
      }
      // clear events
      _dhceb->clear();
      pts.clear();
    }
    if (r.recordType() == RcdHeader::configurationEnd) {

      if (UtlTimeDifference(r.recordTime() - _recordTime).seconds() > MIN_CONFIG_TIME)
	update();

      _recordTime = r.recordTime();
    }
    break;
  }
  };

  return true;
}

void DhcHitCounts::update() {

  std::map<double, double>::iterator hit(_rawCount->begin());
  for (; hit!=_rawCount->end(); hit++)
    (*hit).second /= _evtCount;

  _hplCount[_configurationNumber] = _rawCount;
}

class LcMonitor : public TGMainFrame , public TTimer {

public:
  LcMonitor(const TGWindow *p,Int_t x,Int_t y,UInt_t w,UInt_t h)
    : TGMainFrame(p,w,h),
      _maximumTimeOfAcquisition(UtlTimeDifference(120,0)),
      _canvas(0), _mgraph(0), _hitsPerEvent0(0),_hitsPerEvent1(0),
      _stack(0), _legend0(0), _legend1(0)
  {
    _canvas = new TRootEmbeddedCanvas ("DHCal Layer Check",this,w,h);
    AddFrame(_canvas);

    _pad[0] = new TPad("HPL","HPL", 0, 0, 0.5, 1);
    _pad[0]->SetRightMargin(0.05);
    _pad[0]->Draw();
    _pad[1] = new TPad("HPE","HPE", 0.5, 0, 1, 1);
    _pad[1]->SetRightMargin(0.05);
    _pad[1]->Draw();

    SetWindowName("DHCal RPC Layer Check");

    MapSubwindows();
    MapWindow();
    MoveResize(x, y);
  }

  virtual ~LcMonitor()
  {
    _reader->close();
    delete _arena;
  }

  void CloseWindow()
  {
    gApplication->Terminate();
  }

  void initialize() {

    _dhchits = new DhcHitCounts(_minLayers, _maxPoints);

    _arena = new RcdArena;
    _reader = new RunReader;

    _reader->directory(_runPath);
    assert(_reader->open(_runNumber));

    SetTime(_sleepSecs*1000);
    gSystem->AddTimer(this);
  }

  void reset() {

    if (_mgraph)
      delete _mgraph;

    if (_hitsPerEvent0)
      delete _hitsPerEvent0;

    if (_hitsPerEvent1)
      delete _hitsPerEvent1;

    if (_hitsPerEvent2)
      delete _hitsPerEvent2;

    if (_hitsPerEvent3)
      delete _hitsPerEvent3;

    if (_stack)
      delete(_stack);

    if (_legend0)
      delete _legend0;

    if (_legend1)
      delete _legend1;

    _hitsPerLayer.clear();
    _peakPos.clear();
  }

  Bool_t Notify() {

    // Actions after timer time-out
    while(_reader->read(*_arena)) {
      _dhchits->record(*_arena);

      // wait for configuration to end
      if (_arena->recordType() == RcdHeader::acquisitionStart &&
	  (UtlTime(true)-_arena->recordTime()) < _maximumTimeOfAcquisition) {
	break;
      }
    }

    reset();

    std::map<unsigned, std::map<double, double>*> hitCounts = _dhchits->hpl_count();
    std::map<unsigned, std::map<double, double>*>::const_iterator mit(hitCounts.begin());

    if (hitCounts.size() > 0) {

      std::vector<TGraph*>::const_iterator it;
      std::vector<TGraph*>::const_reverse_iterator rit;

      std::map<double, double>* hitCount;
      std::map<double, double>::const_iterator hit;

      for (; mit!=hitCounts.end(); mit++) {

	hitCount = (*mit).second;
	
	TGraph* graph = new TGraph;
	_hitsPerLayer.push_back(graph);
      
	unsigned n(0);
	for (hit=hitCount->begin(); hit!=hitCount->end(); hit++)
	  graph->SetPoint(n++, (*hit).first, (*hit).second);
      }

      _canvas->GetCanvas()->cd();
      _pad[0]->cd();
      _mgraph = new TMultiGraph;
      _legend0 = new TLegend(0.4,0.77,0.99,0.92);

      std::ostringstream sout;
      sout << "Updated: " << _dhchits->_recordTime;
      _legend0->SetHeader(sout.str().c_str());
      _legend0->SetTextSize(0.035);

      it = _hitsPerLayer.begin();
      (*it)->SetMarkerStyle(23);
      (*it)->SetFillStyle(3001);
      (*it)->SetFillColor(kRed);
      _mgraph->Add(*it);
      _legend0->AddEntry(*it, "Start of Run");

      if (_hitsPerLayer.size() > 1) {
	rit = _hitsPerLayer.rbegin();
	(*rit)->SetMarkerStyle(22);
	(*rit)->SetFillStyle(3001);
	(*rit)->SetFillColor(kGreen);
	_mgraph->Add(*rit);
	_legend0->AddEntry(*rit, "Most Recent");
      }

      _mgraph->SetTitle("Hit Count vs. Layer");
      _mgraph->Draw("ABP");
      _mgraph->GetXaxis()->SetTitle("layer");
      _mgraph->GetYaxis()->SetTitle("Hits/Event");

      _legend0->Draw();
      

      _pad[1]->cd();
      _hitsPerEvent0 = new TH1F("hitsPerEvent0","hitsPerEvent", 100, 0, 0);

      std::vector<std::pair<double, unsigned> > hpe = _dhchits->hpe_count();
      std::vector<std::pair<double, unsigned> >::const_iterator ih(hpe.begin());
      for (; ih!=hpe.end(); ih++)
	_hitsPerEvent0->Fill(ih->first);

      TSpectrum spec;
      Int_t nPeaks(spec.Search(_hitsPerEvent0,2,"goff"));
      Float_t* peakX(spec.GetPositionX());
      Float_t hmax(0);
      for (Int_t i(0); i<nPeaks; i++, peakX++)
	hmax = *peakX > hmax ? *peakX : hmax;
      hmax = 100*(ceil(hmax/50));

      delete _hitsPerEvent0;

      _hitsPerEvent0 = new TH1F("hitsPerEvent0","hitsPerEvent", 100, 0, hmax);
      _hitsPerEvent1 = new TH1F("hitsPerEvent1","hitsPerEvent", 100, 0, hmax);
      _hitsPerEvent2 = new TH1F("hitsPerEvent2","hitsPerEvent", 100, 0, hmax);
      _hitsPerEvent3 = new TH1F("hitsPerEvent3","hitsPerEvent", 100, 0, hmax);
      _stack = new THStack("hs", "Hits Per Event");
      _legend1 = new TLegend(0.6,0.77,0.99,0.92);
      _legend1->SetTextSize(0.035);

      for (ih=hpe.begin(); ih!=hpe.end(); ih++) {
	_hitsPerEvent0->Fill(ih->first);
	if (ih->second == 1)
	  _hitsPerEvent1->Fill(ih->first);
	else if (ih->second == 2)
	  _hitsPerEvent2->Fill(ih->first);
	else if (ih->second == 3)
	  _hitsPerEvent3->Fill(ih->first);
      }

      _hitsPerEvent0->SetFillStyle(3001);
      _hitsPerEvent0->SetFillColor(kRed);
      _hitsPerEvent1->SetFillStyle(3001);
      _hitsPerEvent1->SetFillColor(kGreen);
      _hitsPerEvent2->SetFillStyle(3001);
      _hitsPerEvent2->SetFillColor(kBlue);
      _hitsPerEvent3->SetFillStyle(3001);
      _hitsPerEvent3->SetFillColor(kYellow);

      _legend1->AddEntry(_hitsPerEvent0, "All Triggers");
      _legend1->AddEntry(_hitsPerEvent1, "Cerenkov Tag A");
      _legend1->AddEntry(_hitsPerEvent2, "Cerenkov Tag B");
      _legend1->AddEntry(_hitsPerEvent3, "Cerenkov Tag A&B");

      _stack->Add(_hitsPerEvent0);
      _stack->Add(_hitsPerEvent1);
      _stack->Add(_hitsPerEvent2);
      _stack->Add(_hitsPerEvent3);

      _stack->SetNameTitle("Hits per Event", "Hits per Event");
      _stack->Draw("nostack");

      _legend1->Draw();
      _canvas->GetCanvas()->Update();
    }

    TTimer::Reset();
    return kFALSE;
  }

  void runNumber(const unsigned& r) {
  _runNumber = r;
  }
  void runPath(const std::string& f) {
  _runPath = f;
  }
  void sleepSecs(const unsigned& s) {
  _sleepSecs = s;
  }
  void minLayers(const unsigned& l) {
  _minLayers = l;
  }
  void maxPoints(const unsigned& p) {
  _maxPoints = p;
  }

private:
  std::string _runPath;
  unsigned _runNumber;
  unsigned _minLayers;
  unsigned _maxPoints;
  int _sleepSecs;

  DhcHitCounts* _dhchits;

  RcdArena* _arena;
  RunReader* _reader;

  UtlTimeDifference _maximumTimeOfAcquisition;

  TRootEmbeddedCanvas* _canvas;
  TPad* _pad[2];
  TMultiGraph* _mgraph;
  TH1F* _hitsPerEvent0;
  TH1F* _hitsPerEvent1;
  TH1F* _hitsPerEvent2;
  TH1F* _hitsPerEvent3;
  THStack* _stack;
  TLegend* _legend0;
  TLegend* _legend1;
  std::vector<TGraph*> _hitsPerLayer;
  std::vector<TText*> _peakPos;

};


int main(int argc, const char **argv) 
{
  UtlArguments argh(argc,argv);

  const int xpos(argh.optionArgument('x',750,"X-pos"));
  const int ypos(argh.optionArgument('y',  0,"Y-pos"));
  const unsigned width(argh.optionArgument('w',800,"Width"));
  const unsigned height(argh.optionArgument('v',350,"Height"));
  const unsigned minLayers(argh.optionArgument('l',4,"Min hit layers for displayed events"));
  const unsigned maxPoints(argh.optionArgument('p',300,"Max points/layer for displayed events"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const int sleepSecs(argh.optionArgument('s',300,"Sleep period (secs)"));

  if(argh.help()) return 0;

  gROOT->SetStyle();
  gStyle->SetOptStat("eo");

  TApplication application("Layer Check Application",0,0);
  LcMonitor* lcm = new LcMonitor(gClient->GetRoot(), xpos, ypos, width, height);

  lcm->runNumber(runNumber);
  lcm->runPath(runPath);
  lcm->sleepSecs(sleepSecs);
  lcm->minLayers(minLayers);
  lcm->maxPoints(maxPoints);
  lcm->initialize();

  application.Run();
}
