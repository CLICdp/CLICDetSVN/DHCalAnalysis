#include <string>
#include <sstream>
#include <map>
#include <algorithm>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TCanvas.h"

#include <TGClient.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TGFrame.h>
#include <TGTab.h>

#include "TH2D.h"
#include "TFile.h"


class CScan {

private:

  std::map<unsigned, TH2D*> _hxyr;
  std::map<double, unsigned> _layerz;

  TFile* _hfile;

  RcdArena* _arena;
  RunReader _reader;

  DhcEB* _dhceb;

  unsigned _runNumber;
  std::string _runPath;
  std::string _rootPath;

public:
  CScan();
  virtual ~CScan();

  bool initialize();
  void record();

  void runNumber(const unsigned& r);
  void runPath(const std::string& f);
  void rootPath(const std::string& w);

  const std::map<unsigned, TH2D*>& raw_hitdata() const;
};


class CScanFrame : public TGMainFrame {

private:

  virtual void CloseWindow();

  CScan*   _cscan;

public:
  CScanFrame(const TGWindow *p,UInt_t w,UInt_t h, CScan* scan);
  virtual ~CScanFrame();

  void update();
};


CScan::CScan()
  : _dhceb(0) {}

CScan::~CScan()
{}

bool CScan::initialize()
{
  _arena = new RcdArena;
  _reader.directory(_runPath);
  assert(_reader.open(_runNumber));

  // determine run type
  while (!_dhceb) {

    _reader.read(*_arena);
    switch (_arena->recordType()) {

    case RcdHeader::runStart: {
      // Access the DaqRunStart
      SubAccessor accessor(*_arena);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::beamData:
      case DaqRunType::dhcBeam:
      case DaqRunType::dhcNoise:
      case DaqRunType::dhcQinj:
      case DaqRunType::dhcCosmics: {
	_dhceb = new DhcEBExtTrg(_runNumber);
	break;
      }

      default: {
	std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
	return false;
	break;
	}
      }
    }
    default:
      break;
    }
  }

  // map z-coord to layer number
  DhcCableMap cmap(_runNumber);
  DhcPadPts* pt;

  for (unsigned s(4); s<20; s++)
    for (unsigned d(0); d<12; d++)
      if ((pt = cmap.origin(0xdc, s, d)) != 0)
	_layerz[pt->Pz] = 1;

  unsigned layer(1);
  std::map<double, unsigned>::iterator mit;
  for (mit=_layerz.begin(); mit!=_layerz.end(); mit++)
    (*mit).second = layer++;

  return true;
}

void CScan::record()
{
  while(_reader.read(*_arena)) {

    if (_arena->totalNumberOfBytes() > sizeof(*_arena)/2)
      continue;

    _dhceb->record(*_arena);

    if (!_dhceb->raw_hitdata().empty()) {

      std::vector<DhcFeHitData*>::const_iterator hb(_dhceb->raw_hitdata().begin());
      std::vector<DhcFeHitData*>::const_iterator he(_dhceb->raw_hitdata().end());

      for (; hb!=he; hb++) {

	std::vector<DhcPadPts*> pts;
	_dhceb->hits2pts(*hb, &pts);

	std::vector<DhcPadPts*>::const_iterator ip(pts.begin());
	for (; ip!=pts.end(); ip++) {

	  unsigned layer(_layerz[(*ip)->Pz]);
	  if (_hxyr.find(layer) == _hxyr.end()) {
	    std::ostringstream sout;
	    sout << "Layer " << layer << " Raw Hit XY";
	    _hxyr[layer] = new TH2D(sout.str().c_str(),"Pad Pos",100,-50,50,100,-50,50);
	  }
	  _hxyr[layer]->Fill((*ip)->Px, (*ip)->Py);
	  delete *ip;
	}
      }

      // clear events
      _dhceb->clear();
    }
  }
  _reader.close();

  std::ostringstream sout;
  sout << _rootPath << "/cscan-Run" << _runNumber << ".root";

  _hfile = new TFile(sout.str().c_str(), "recreate");

  std::map<unsigned, TH2D*>::const_iterator ht;
  for  (ht=_hxyr.begin(); ht!=_hxyr.end(); ht++)
    _hfile->Add((*ht).second);

  _hfile->Write();

  delete _dhceb;
  delete _arena;
}

void CScan::runNumber(const unsigned& r) {
  _runNumber = r;
}
void CScan::runPath(const std::string& f) {
  _runPath = f;
}
void CScan::rootPath(const std::string& w) {
  _rootPath = w;
}

const std::map<unsigned, TH2D*>&
CScan::raw_hitdata() const {
  return _hxyr;
}


CScanFrame::CScanFrame(const TGWindow *p,UInt_t w,UInt_t h, CScan* scan)
  : TGMainFrame(p,w,h),
    _cscan(scan)
{
  SetCleanup(kDeepCleanup);
}

CScanFrame::~CScanFrame()
{}

void CScanFrame::CloseWindow()
{
  gApplication->Terminate();
}

void CScanFrame::update()
{
  std::map<unsigned, TH2D*> _hxyr = _cscan->raw_hitdata();
  if (_hxyr.empty()) {
    std::cout << "No data to analyze" << std::endl;
    CloseWindow();
  }

  TGTab* fTabs = new TGTab(this, 20, 20);
  TGLayoutHints* hints = new TGLayoutHints(kLHintsTop | kLHintsLeft |
					   kLHintsExpandX | kLHintsExpandY,
					   5, 5, 5, 5);
  unsigned lmin[3] = { 1, 20, 39};
  unsigned lmax[3] = {19, 38, 54};
  lmax[2] = std::min(lmax[2],_hxyr.rbegin()->first);
  unsigned smax(lmax[2]>=lmin[2] ? 3 : 2);

  for (unsigned s(0); s<smax; s++) {

    std::ostringstream sout;
    sout << "Layers " << lmin[s] << "-" << lmax[s];

    TGCompositeFrame* tfs = fTabs->AddTab(sout.str().c_str());
    TGCompositeFrame* cFs = new TGCompositeFrame(tfs, 20, 20, kHorizontalFrame);

    TGTab* fTab = new TGTab(tfs, 20, 20);

    unsigned lLo(std::max(lmin[s],_hxyr.begin()->first));
    unsigned lHi(std::min(lmax[s],_hxyr.rbegin()->first));

    for (unsigned layer(lLo); layer<=lHi; layer++) {

      std::ostringstream sout;
      sout << layer;

      TGCompositeFrame* tf = fTab->AddTab(sout.str().c_str());
      TGCompositeFrame* cF = new TGCompositeFrame(tf, 20, 20, kHorizontalFrame);

      TRootEmbeddedCanvas* eC = new TRootEmbeddedCanvas (sout.str().c_str(),cF,
							 GetWidth(), GetHeight());
      eC->GetCanvas()->Divide(2, 1);

      cF->AddFrame(eC, hints);
      tf->AddFrame(cF, hints);

      int c(0);
      if (_hxyr.find(layer) != _hxyr.end()) {
	eC->GetCanvas()->cd(++c)->SetLogz(0);
	_hxyr[layer]->Draw("colz");

	eC->GetCanvas()->cd(++c)->SetLogz(0);
	_hxyr[layer]->Draw("surf3");
      }
      eC->GetCanvas()->Update();
    }
    tfs->AddFrame(fTab);
  }
  AddFrame(fTabs);

  SetWindowName("DHCAL+TCMT RPC Layer Scan");

  MapSubwindows();
  Resize();
  MapWindow();
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned width(argh.optionArgument('x',800,"Width"));
  const unsigned height(argh.optionArgument('y',400,"Height"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const std::string rootPath(argh.optionArgument('w',"root","Path where root file is written"));

  if(argh.help()) return 0;

  TApplication _application("DHCal RPC Layer Scan Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat(11);
  gStyle->SetLabelSize(0.03, "xyz");

  CScan* scan = new CScan;
  // set parameters
  scan->runNumber(runNumber);
  scan->runPath(runPath);
  scan->rootPath(rootPath);

  if (scan->initialize()) {

    scan->record();

    CScanFrame* frame = new CScanFrame(gClient->GetRoot(), width, height, scan);
    frame->update();

    _application.Run();
  }
}
