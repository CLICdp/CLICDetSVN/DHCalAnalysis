#include <iostream>
#include <sstream>
#include <vector>
#include <deque>
#include <map>
#include <string>
#include <stdexcept>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcFeHitData.hh"
#include "TtmFifoData.hh"


class ChkDhcTts : public RcdUserRO {

public:
  ChkDhcTts();
  virtual ~ChkDhcTts() {}

  bool record(const RcdRecord &r);
  bool report();

  const std::vector<double>& hit_time() const;
  const std::vector<double>& trg_time() const;

private:
  std::deque<BmlCaen1290EventData*> _tdcevt;
  unsigned* _tdcPool;
  unsigned* _tdcPtr;

  std::deque<TtmFifoData*> _ttmtts;
  std::map<unsigned, unsigned> _enableMap;

  unsigned _outOfTime;
  unsigned _missingData;
  unsigned _noData;
  unsigned _earlyData;
  unsigned _lateData;
  unsigned _triggerDelay;
  unsigned _triggerStart;
  unsigned _configurationNumber;
  unsigned _acquisitionNumber;

  std::vector<double> _hitTime;
  std::vector<double> _trgTime;

  unsigned _nTts;
  unsigned _nAts;
  unsigned _nBts;
  unsigned _nABts;

};

ChkDhcTts::ChkDhcTts()
  : RcdUserRO(9), _outOfTime(0), _missingData(0), _noData(0), _earlyData(0), _lateData(0),
    _nTts(0), _nAts(0), _nBts(0),_nABts(0)
{
  _tdcPool = new unsigned[0x10000];
}

const std::vector<double>& ChkDhcTts::hit_time() const
{
  return _hitTime;
}

const std::vector<double>& ChkDhcTts::trg_time() const
{
  return _trgTime;
}

bool ChkDhcTts::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::configurationStart: {

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);
    _configurationNumber = v[0]->configurationNumberInRun();

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    assert(be.size()>0);

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
    for (bit=be.begin(); bit!=be.end(); bit++) {
      if (!(*bit)->write()) {

	UtlPack loc(0);
	loc.byte(2, ((*bit)->crateNumber()));
	loc.byte(1, ((*bit)->slotNumber()));

	UtlPack dconEnabled(0);
	dconEnabled.word(((*bit)->data()->dconEnable()));

	_enableMap[loc.word()] = dconEnabled.bits(0,11);
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    assert(dc.size()>0);
    _triggerDelay = static_cast<unsigned int>(*dc[0]->data()->triggerDelay()) + 1;
    _triggerStart = 21 - _triggerDelay;

    break;
  }

  case RcdHeader::acquisitionStart: {

    std::vector<const DaqAcquisitionStart*>
      v(accessor.extract<DaqAcquisitionStart>());
    assert(v.size()==1);
    _acquisitionNumber = v[0]->acquisitionNumberInRun();

    _tdcPtr = _tdcPool;

#ifdef DHC_DEBUG    
    std::cout << " acquisitionNymberInRun " << _acquisitionNumber << std::endl;
#endif // DHC_DEBUG

    break;
  }

  case RcdHeader::trigger: {

    {
      std::vector<const TtmLocationData<TtmTriggerData>*>
	v(accessor.access<TtmLocationData<TtmTriggerData> >());

      assert(v.size()==1);

      unsigned n(v[0]->data()->numberOfWords());
      const unsigned* data(v[0]->data()->data());
      
      _ttmtts.push_back(new TtmFifoData(n, data));
    }
    {
      std::vector<const BmlLocationData<BmlCaen1290EventData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290EventData> >());

      if(v.size()==1) {
	_tdcevt.push_back(new (_tdcPtr) BmlCaen1290EventData);
	_tdcPtr = (unsigned*)mempcpy(_tdcPtr, v[0]->data(),
				     sizeof(BmlCaen1290EventData)+
				     sizeof(unsigned)*v[0]->data()->numberOfWords());
      }
    }
    break;
  }

  case RcdHeader::event: {

    TtmFifoData* ttmtts = _ttmtts.front();
    BmlCaen1290EventData* tdcevt(_tdcevt.empty() ? 0 : _tdcevt.front());

    std::vector<const BmlLocationData<BmlCaen1290EventData>*>
      vt(accessor.access<BmlLocationData<BmlCaen1290EventData> >());

#ifdef DHC_DEBUG    

    ttmtts->print(std::cout);
    if (tdcevt) tdcevt->print(std::cout);
    /*
    if(vt.size()>0)
      (vt[0]->data())->print(std::cout);
    */
#endif // DHC_DEBUG

    if (tdcevt) _tdcevt.pop_front();

    std::vector<const CrcLocationData<CrcVlinkEventData>* > 
      cv(accessor.access< CrcLocationData<CrcVlinkEventData> >());

    bool oscTrg(false);
    for(unsigned i(0); i < cv.size();i++) {
      const CrcVlinkTrgData *td(cv[i]->data()->trgData());
      if (td) {
	if (td->lineHistory(19).size() == 1) {
	  std::vector<std::vector<CrcBeTrgHistoryHit> > trig;
	  for (unsigned i(0); i<25 ; i++)
	    trig.push_back(td->lineHistory(i));
	  if (trig[24].size())
	    oscTrg = true;

#ifdef DHC_DEBUG    
	  for (unsigned i(0); i<25 ; i++)
	    std::cout <<  " L" << i << " " << trig[i].size();
	  std::cout << std::endl;
#endif // DHC_DEBUG

	}
      }
    }

    _nTts += ttmtts->numberOfTriggerTS();
    if (ttmtts->numberOfFifoATS())
      _nAts++;
    if (ttmtts->numberOfFifoBTS())
      _nBts++;

#ifdef DHC_DEBUG    
    if (ttmtts->numberOfFifoATS() > 1) {
      const unsigned *fifoATS(ttmtts->fifoATS());
      for (unsigned i(0); i<ttmtts->numberOfFifoATS(); i++)
	std::cout << "    fifoATS " << *fifoATS++ << std::endl;
    }
    if (ttmtts->numberOfFifoBTS() > 1) {
      const unsigned *fifoBTS(ttmtts->fifoBTS());
      for (unsigned i(0); i<ttmtts->numberOfFifoBTS(); i++)
	std::cout << "    fifoBTS " << *fifoBTS++ << std::endl;
    }
#endif // DHC_DEBUG

    unsigned tts = *ttmtts->triggerTS();
    _ttmtts.pop_front();

    if (!oscTrg)
      _trgTime.push_back(_acquisitionNumber*1e7 + tts);

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    bool inTime(true);
    bool missingData(false);
    bool noData(true);
    bool dataEarly(false);
    bool dataLate(false);

    for(unsigned s(0);s<v.size();s++) {

      UtlPack loc(0);
      loc.byte(2, (v[s]->crateNumber()));
      loc.byte(1, (v[s]->slotNumber()));

      UtlPack dconTrg(0);

      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {

	noData = false;
	const DhcFeHitData* hits(v[s]->data()->feData(i));

	if (!hits->trg()) {
	  double tDelta((double)tts - (double)hits->timestamp());
	  _hitTime.push_back(tDelta);
	  if (tDelta > _triggerStart) {
#ifdef DHC_DEBUG    
	    std::cout << "Data early -"
		      << " C:" << (int)loc.byte(2) << " S:" << (int)loc.byte(1)
		      << " F:" << hits->dconad() << " A:" << hits->dcalad()
		      << " tsDelta:" << tDelta << std::endl;
#endif // DHC_DEBUG
	    dataEarly=true;
	  }
	  if (tDelta < -1) { // Qinj signal lags trigger by 1 
#ifdef DHC_DEBUG    
	    std::cout << "Data late -"
		      << " C:" << (int)loc.byte(2) << " S:" << (int)loc.byte(1)
		      << " F:" << hits->dconad() << " A:" << hits->dcalad()
		      << " tsDelta:" << tDelta << std::endl;
#endif // DHC_DEBUG
	    dataLate=true;
	  }
	  continue;
	}

	dconTrg.bit(hits->dconad(), 1);

	unsigned tDelta = hits->timestamp() >= _triggerDelay ?
	  hits->timestamp() - tts : (hits->timestamp()+10000000) - tts;
	if (tDelta != _triggerDelay) {
#ifdef DHC_DEBUG    
	  std::cout << "Out of time -"
		    << " C:" << (int)loc.byte(2) << " S:" << (int)loc.byte(1)
		    << " F:" << hits->dconad()
		    << " tDelta: " << tDelta
		    << " triggerDelay: " << _triggerDelay << std::endl;
#endif // DHC_DEBUG
	  inTime=false;
	}
      }

#ifdef DHC_DEBUG    
      if (dataEarly || dataLate || !inTime)
	v[s]->print(std::cout, " ");
#endif // DHC_DEBUG

      if (dconTrg.word() != _enableMap[loc.word()]) {
	missingData = true;
#ifdef DHC_DEBUG    
	v[s]->print(std::cerr, " ");
#endif // DHC_DEBUG
      }
    }

    if (!inTime)
      _outOfTime++;

    if (dataEarly)
      _earlyData++;
    if (dataLate)
      _lateData++;
    
    if (noData) {
      _noData++;

      std::vector<const DaqEvent*> de(accessor.extract<DaqEvent>());
      assert(de.size()==1);

      std::cerr << "No event data." << std::endl;
      de[0]->print(std::cerr, " ");
      break;
    }

    if (missingData)
      _missingData++;

    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

bool ChkDhcTts::report()
{
  std::cout << _outOfTime << " Out of Time Events" << std::endl;
  std::cout << _missingData << " events with missing data" << std::endl;
  std::cout << _noData << " events with no data" << std::endl;
  std::cout << _earlyData << " events with early data (timestamp too small)" << std::endl;
  std::cout << _lateData << " events with late data (timestamp too large)" << std::endl;
  std::cout << (float)_nAts/(float)_nTts << " Outer  "
	    << (float)_nBts/(float)_nTts << " Inner" << std::endl;

  return true;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));

  if(argh.help()) return 0;

  ChkDhcTts dhctts;

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena))
    dhctts.record(*arena);

  dhctts.report();
  reader.close();

  delete arena;
}
