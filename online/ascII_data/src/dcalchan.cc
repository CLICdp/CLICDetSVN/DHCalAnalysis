#include <iostream>
#include <sstream>
#include <vector>
#include <map>
#include <bitset>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcFeHitData.hh"
#include "DhcCableMap.hh"


class DhcDcalChan : public RcdUserRO {

public:
  DhcDcalChan(const unsigned& runNumber,
	      const bool& verbose);
  virtual ~DhcDcalChan() {}

  bool record(const RcdRecord &r);

private:
  std::map<unsigned, bool> _lockMap;
  std::map<unsigned, unsigned> _enableMap;
  std::map<unsigned, std::string> _dconmap;
  std::map<unsigned, std::vector<unsigned>*> _dcalmap;
  bool _verbose;

  DhcCableMap* _cmap;
};

DhcDcalChan::DhcDcalChan(const unsigned& runNumber,
			 const bool& verbose)
  : RcdUserRO(9),_verbose(verbose) 
{
  // connect to SQL db.
  _cmap = new DhcCableMap(runNumber);
}

bool DhcDcalChan::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::configurationStart: {

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    assert(be.size()>0);

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
    for (bit=be.begin(); bit!=be.end(); bit++) {
      if (!(*bit)->write()) {

	UtlPack loc(0);
	loc.byte(3, ((*bit)->crateNumber()));
	loc.byte(2, ((*bit)->slotNumber()));

	UtlPack dconEnabled(0);
	dconEnabled.word(((*bit)->data()->dconEnable()));

	for (unsigned i(0); i<12; i++) {
	  loc.byte(1, i);
	  _lockMap[loc.word()] = (dconEnabled.bit(i) == dconEnabled.bit(i+16));
	}
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    assert(dc.size()>0);

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>::const_iterator it;
    for (it=dc.begin(); it!=dc.end(); it++) {

      UtlPack loc(0);
      loc.byte(3, (*it)->location().crateNumber());
      loc.byte(2, (*it)->location().slotNumber());
      loc.byte(1, (*it)->dhcComponent());

      if ((*it)->location().write())
	continue;

      // test for DCOL port not locked
      if (!_lockMap[loc.word()])
	continue;

      const unsigned char* enable((*it)->data()->enable());
      UtlPack ena(0);
      ena.byte(2, enable[2]);
      ena.byte(1, enable[1]);
      ena.byte(0, enable[0]);

      _enableMap[loc.word()] = ena.word();
    }

    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    for (unsigned s(0); s<v.size(); s++) {

      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
	const DhcFeHitData* hits(v[s]->data()->feData(i));
	if (!hits->trg() && !hits->err()) {

	  UtlPack loc(0);
	  loc.byte(3, (v[s]->crateNumber()));
	  loc.byte(2, (v[s]->slotNumber()));
	  loc.byte(1, (hits->dconad()));
	  loc.byte(0, (hits->dcalad())); 

	  if (_dcalmap.find(loc.word()) == _dcalmap.end())
	    _dcalmap[loc.word()] = new std::vector<unsigned>(64,0);

	  std::vector<unsigned>* dcals  = _dcalmap[loc.word()];

	  std::bitset<64> hitsHi(hits->hitsHi());
	  std::bitset<64> hitsLo(hits->hitsLo());
	  std::bitset<64> hit(hitsHi<<32 | hitsLo);

	  for (int i(0); i<64; i++) {
	    if (hit.test(i))
	      (*dcals)[i]++;
	  }
	}
      }
    }
    break;
  }

  case RcdHeader::runEnd: {

    std::vector<const DhcLocationData<DhcDcRunData>*>
      dc(accessor.access< DhcLocationData<DhcDcRunData> >());

    assert(dc.size()>0);
    for (unsigned d(0); d<dc.size(); d++) {

      UtlPack loc(0);
      loc.byte(3, dc[d]->location().crateNumber());
      loc.byte(2, dc[d]->location().slotNumber());
      loc.byte(1, dc[d]->location().dhcComponent());

      // test for DCOL port not locked
      if (!_lockMap[loc.word()])
	continue;

      const unsigned char* _serial = dc[d]->data()->serial();

      std::ostringstream sout;
      for(unsigned i(0); i<2; i++)
	sout << std::setfill('0') << std::setw(2)
	     << std::hex << static_cast<unsigned int>(_serial[i]) << std::dec
	     << std::setfill(' ');

      _dconmap[loc.word()] = sout.str();
    }

    unsigned deadChip(0);
    std::map<unsigned, std::string>::iterator it;
    for (it=_dconmap.begin(); it!=_dconmap.end(); it++) {

      UtlPack loc((*it).first);
      std::string serno((*it).second);

      std::bitset<32> enabled(0);
      if (_enableMap.find(loc.word()) != _enableMap.end())
	enabled = _enableMap[loc.word()];

      // check for any deadchannesl
      bool dead(false);
      for  (int i(0); i<24; i++) {

	// test for chip enabled
	if (!enabled.test(i))
	  continue;

	loc.byte(0,i);
	if (_dcalmap.find(loc.word()) == _dcalmap.end()) {
	  deadChip++;
	  if (_verbose) {
	  std::cout << "Board #0x" << serno;
	  std::cout << ", Side "
		      << _cmap->label(loc.byte(3), loc.byte(2), loc.byte(1))
		      << " (" << (int)loc.byte(3) <<','
		      << (int)loc.byte(2) <<','
		      << (int)loc.byte(1) << ")";
	  std::cout << ", DEAD CHIP: " << i << std::endl;
	  }
	}
	else {
	  std::vector<unsigned>* dcals  = _dcalmap[loc.word()];
	  std::vector<unsigned>::iterator dt(dcals->begin());

	  for (; dt!=dcals->end(); dt++) {
	    if (!(*dt)) {
	      dead=true;
	      break;
	    }
	  }
	}
      }
      if (dead) {
	if (_verbose) {
	  std::cout << "Board serno: 0x" << serno;
	  std::cout << " (" << (int)loc.byte(3) <<','
		    << (int)loc.byte(2) <<','
		    << (int)loc.byte(1) << ")";
	  std::cout << " Dead chip-chan ";
	}
      }
      // print dead channels
      for  (int i(0); i<24; i++) {

	// test for chip enabled
	if (!enabled.test(i))
	  continue;

	loc.byte(0,i);
	if (_dcalmap.find(loc.word()) != _dcalmap.end()) {
	  std::vector<unsigned>* dcals  = _dcalmap[loc.word()];
	  std::vector<unsigned>::iterator dt(dcals->begin());

	  for (; dt!=dcals->end(); dt++) {
	    if (!(*dt)) {
	      if (_verbose) {
		std::cout << std::dec << i << "-" << (dt-dcals->begin()) << " ";
		std::pair<double, double>* coord(DhcFeHitData::hitCoord(i,(dt-dcals->begin())));
		std::cout << "(" << coord->first << "," << coord->second << ") ";
		delete coord;
	      }
	    }
	  }
	}
      }
      if (_verbose)
	if (dead)
	  std::cout << std::endl;
    }
    if (deadChip)
      std::cout << " " << deadChip << " DEAD Chips" << std::endl;
    else
      std::cout << " No DEAD chips" << std::endl;
    break;
  }

  default: {  
    break;
  }
  };

  return true;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const bool verbose(argh.option('v',"Verbose print"));

  if(argh.help()) return 0;

  DhcDcalChan ddc(runNumber, verbose);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);
  
  while(reader.read(*arena)) {

    if (arena->totalNumberOfBytes() > sizeof(*arena)/2)
      continue;

    ddc.record(*arena);
  }
  reader.close();
  delete arena;
}
