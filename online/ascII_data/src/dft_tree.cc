#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TH1F.h"
#include "TVirtualFFT.h"

#include "DhcTree.hh"

#include <TSystem.h>
#include "TTree.h"
#include "TFile.h"

#include "UtlPack.hh"
#include "UtlArguments.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string rootPath(argh.optionArgument('w',"root","Path where root file is written"));

  const unsigned minLayers(argh.optionArgument('l',32,"Minimum hit layers for displayed events"));

  const unsigned tbin(argh.optionArgument('n',1000,"Number of time bins"));
  const std::string _tmin(argh.optionArgument('m',"45.0","Minimum time (sec)"));
  const std::string _tmax(argh.optionArgument('x',"46.0","Maximum time (sec)"));
  const std::string fftOpt(argh.optionArgument('o',"MAG","FFT display option (RE|IM|MAG|PH)"));

  if(argh.help()) return 0;

  double tmin(0);
  double tmax(0);
  std::stringstream(_tmin) >> tmin;
  std::stringstream(_tmax) >> tmax;

  std::map<unsigned, std::vector<double> > timeEvt;
  int tslast(0xffffff);
  int toffset(0);

  std::ostringstream sout;
  sout << rootPath << "/dhcTree-Run" << runNumber << ".root";

  TFile* tfile = new TFile(sout.str().c_str());
  TTree* tree = (TTree*)tfile->Get("DhcTree");

  Event* event = new Event();
  TBranch* branch = tree->GetBranch("EventBranch");
  branch->SetAddress(&event);

  Int_t nevt = tree->GetEntries();
  for (Int_t i(0); i<nevt; i++) {

    branch->GetEntry(i);

    EventHeader* hdr = event->GetHeader();

    TClonesArray* pts = event->GetHits();
    
    Int_t nhits(event->GetNhits());
    Int_t ts(((DhcHit*)((*pts)[nhits/2]))->GetTs());

    unsigned layers(event->GetNhitLayers());
    if (minLayers <= layers) {

      if (tslast < 0xffffff) {

	if (ts < tslast) {
	  tslast -= (int)1e7;
	  toffset += (int)1e7;
	}
	timeEvt[0].push_back((double)((ts + toffset)/1e7));
      }
      tslast = ts;
    }

    event->Clear();
  }

  TApplication _application("Noise Time Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eo");

  TCanvas canvas("Timeline DFT", "Timeline DFT", 800, 600);
  canvas.Divide(1,2);

  TH1F timeDist("timeDist","timeDist", tbin, tmin, tmax);

  canvas.cd(1)->SetLogy(1);
  timeDist.SetNameTitle("Event tDist", "Event tDist");
  timeDist.GetXaxis()->SetTitle("Event tDist (sec)");

  std::vector<double>::iterator it(timeEvt[0].begin());
  for (; it!=timeEvt[0].end(); it++)
    timeDist.Fill((*it));

  timeDist.Draw();

  canvas.cd(2)->SetLogy(0);
  TH1* hp(0);
  TVirtualFFT::SetTransform(0);
  hp = timeDist.FFT(hp, fftOpt.c_str());
  hp->Draw();

  canvas.Update();

  _application.Run();

}
