#include <iostream>
#include <sstream>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "EbDhcRecordsTrig.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  //unsigned slot(argh.optionArgument('s',3,"Slot"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  unsigned tsRange(argh.optionArgument('t',3,"inclusive timestamp range for hits"));
  const std::string outPath
       (argh.optionArgument
       ('w',"","output directory,put / after directroy name, default current directory, e.g.: data/"));
  const std::string runPath(argh.optionArgument('f',"data/run","e.g.: dataSkt/run"));
  unsigned ecalThreshold(argh.optionArgument('e',1,"ecal mip threshold, 1=0.5"));

  if(argh.help()) return 0;

  //TApplication _application("dhcal plots",0,0);
  //gROOT->Reset();

  /*const std::string runPath;

  // dhcal stand-alone
  if (runNumber > 599999 && runNumber < 610000) 
    runPath << "data/run";
 
  // dhcal combined
  if (runNumber > 609999 && runNumber < 620000)
    runPath << "dataSkt/run";

  // default if run number out of range
  if (runNumber > 619999 || runNumber < 600000) {
    runPath << "data/run";
    std::cout<<std::endl" WARNING, run number is not for dhcal combined or stand-alone";
    std::cout<<std::endl;
  }*/

  std::ostringstream sout;
  sout << runNumber;

  EbDhcRecordsTrig* ebmain = 
    new EbDhcRecordsTrig(runNumber,outPath,tsRange,ecalThreshold);
  //ebmain->openWriter(runNumber, path);
  //ebmain->timestampRange(tsRange);

// initialize eb parameters
///*
  RcdArena* arenaIP(new RcdArena);
  RunReader readerIP;

//  readerIP.printLevel(0);
  readerIP.directory(runPath);
  assert(readerIP.open(runNumber));
  while(readerIP.read(*arenaIP)) {
    if(ebmain!=0) {
       ebmain->eventIP(*arenaIP);
    }
  }

  readerIP.close();
  delete arenaIP;
// done initializing parameters
//*/

// run event builder on data
  RcdArena* arena(new RcdArena);
  RunReader reader;

  reader.directory(runPath);
  assert(reader.open(runNumber));
  while(reader.read(*arena)) {
    if(ebmain!=0) {
       ebmain->event(*arena); // builds events
       ebmain->writeEvents();
    }
  }

  //ebmain->update();
  //_application.Run(1);
  ebmain->writeRoot();
  delete arena;
  reader.close();

  
  //ebmain->closeWriter(); // outputMode: 0-ascii, 1-bin
  ebmain->report();


  delete ebmain;

}
