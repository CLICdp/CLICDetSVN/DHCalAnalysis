#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdWriterAsc.hh"

#include "DaqRunStart.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

#include "ChkPrint.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const std::string runPath(argh.optionArgument('f',"cfg/dhcal/beam/dhcConfig","Config data file"));

  if(argh.help()) return 0;

  RcdReaderAsc *reader=new RcdReaderAsc();
  reader->open(runPath);

  RcdArena &arena(*(new RcdArena));
  ChkPrint hn;

  hn.enableType(true);

  hn.enable(subRecordType< DhcReadoutConfigurationData >(),true);
  hn.enable(subRecordType< DhcTriggerConfigurationData >(),true);
  hn.enable(subRecordType< DhcLocationData<DhcBeConfigurationData> >(),true);
  hn.enable(subRecordType< DhcLocationData<DhcDcConfigurationData> >(),true);
  hn.enable(subRecordType< DhcLocationData<DhcFeConfigurationData> >(),true);
  hn.enable(subRecordType< TtmLocationData<TtmConfigurationData> >(),true);

  reader->read(arena);
  hn.record(arena);

  reader->close();
  delete reader;
}
