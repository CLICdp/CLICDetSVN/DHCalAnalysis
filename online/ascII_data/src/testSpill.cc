#include <iostream>

#include <signal.h>
#include <unistd.h>

#include "BmlCaenV2718Device.hh"

bool continueLoop;

void signalHandler(int signal) {
  continueLoop=false;
}


int main(int argc, const char **argv) {

  // Catch signals
  signal(SIGTERM,signalHandler);
  signal(SIGINT ,signalHandler);

  BmlCaenV2718Device* device(new BmlCaenV2718Device(0, 0));

  if (!device->alive())
    {
      std::cerr << " Error opening the device:" << std::endl;
      exit(1);
    }

  continueLoop=true;;
  while (continueLoop)
    {
      if (device->inSpill())
	std::cout << " In spill" << std::endl;
      else
	std::cout << " Wating for spill" << std::endl;
      sleep(1);
    }

  delete device;
}
