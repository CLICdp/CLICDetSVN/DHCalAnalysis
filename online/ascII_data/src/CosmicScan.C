void CosmicScan() {

  gROOT->SetStyle("Bold");

  TCanvas *c1 = new TCanvas("c1", "CosmicScan", 600, 800);
  c1->Divide(1, 2);

  TMultiGraph *mge = new TMultiGraph();
  TMultiGraph *mgm = new TMultiGraph();

  const Int_t n = 8;
  Double_t layer[] = { 0., 10., 20., 30., 40., 50., 60., 70. };
  Double_t effic[] = {
    0.861,  0.932,  0.909,  0.952,  0.941,  0.918,  0.86,   0.768,
  };
  Double_t multip[] = {
    1.39,   1.67,   1.39,   1.69,   1.54,   1.53,   1.71,   1.3,
  };

  TGraph *gre1 = new TGraph(n, layer, effic);
  gre1->SetMarkerColor(kBlue);
  gre1->SetMarkerStyle(21);
  mge->SetTitle("Efficiency");
  mge->SetMinimum(0);
  mge->SetMaximum(1);
  mge->Add(gre1);

  c1->cd(1);
  mge->Draw("ap");
  mge->GetXaxis()->SetTitle("LayerZ (cm)");

  TGraph *grm1 = new TGraph(n, layer, multip);
  grm1->SetMarkerColor(kRed);
  grm1->SetMarkerStyle(20);
  mgm->SetTitle("Multiplicity");
  mgm->SetMinimum(0);
  mgm->SetMaximum(2);
  mgm->Add(grm1);

  c1->cd(2);
  mgm->Draw("ap");
  mgm->GetXaxis()->SetTitle("LayerZ (cm)");

  c1->Update();
}
