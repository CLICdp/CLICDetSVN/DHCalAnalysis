#include <bitset>
#include <cassert>

#include <TApplication.h>
#include <TGClient.h>
#include <TGCanvas.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TGFrame.h>
#include <TGTab.h>
#include <TGMenu.h>
#include <TGDockableFrame.h>

#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TF1.h"

#include "UtlPack.hh"
#include "RcdRecord.hh"
#include "SubAccessor.hh"

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlArguments.hh"

#include "DhcFeHitData.hh"


Double_t scurve(double *x, double *p)
 {
    return (1.0 - erf((x[0] - p[0])/p[1]))/2.0;
  }

const char *tabnames[] = { "S Curves",
			   "HitMaps",
			   "50% Effic Fits",
			   "50% Effic Hists",
			   "TimeStamps" };

enum {
  MAX_DCON = 12,
  MAX_CHIP = 24,
  MAX_CHAN = 64
};

struct hitStats {
  double _tslast;
  double _trgs;
  double _hits[MAX_CHIP][MAX_CHAN];
  TGraphErrors _graph[MAX_CHIP][MAX_CHAN];
  TH1D _hist[MAX_CHIP];
  TH2D _hitmap[MAX_CHIP];
  TH1D _histt[2];
}; 


class QScan {

private:
  std::map<unsigned, struct hitStats*> _stats;

  unsigned _crate;
  unsigned _slot;
  unsigned _runNumber;
  std::string _runPath;

  UtlPack _injMask;
  unsigned _triggerDelay;
  unsigned _vtpd;
  unsigned _configuration;

  void record(RcdRecord &r);

public:
  QScan(unsigned c, unsigned s);
  virtual ~QScan() {}

  void runNumber(const unsigned& r);
  void runPath(const std::string& f);

  void fileio();
  void setup();

  struct hitStats* hit_stats(unsigned d);
};

class QScanFrame : public TGMainFrame {

  enum ECommandIdentifiers {
    M_FILE_PRINT,
    M_FILE_EXIT,
  };

private:
  TGDockableFrame    *fMenuDock;
  TGMenuBar          *fMenuBar;
  TGPopupMenu        *fMenuFile;
  TGLayoutHints      *fMenuBarLayout, *fMenuBarItemLayout;

  TGTab               *fTab;
  TRootEmbeddedCanvas *fEc[5];
  TGCompositeFrame    *fF[5];
  TGLayoutHints       *fL[5];

  QScan* _qscan;
  unsigned _dcon;

  TMultiGraph _mgraph[MAX_CHIP];
  TGraphErrors _fits[MAX_CHIP];
  
  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);

public:
  QScanFrame(const TGWindow *p,UInt_t w,UInt_t h, QScan* q, unsigned d);
  virtual ~QScanFrame() {}

  void update();
};


QScan::QScan(unsigned c, unsigned s)
  : _crate(c),
    _slot(s)
{}

struct hitStats* QScan::hit_stats(unsigned d)
{
  if (_stats.find(d) == _stats.end())
    return 0;
  else
    return _stats[d];
}

void QScan::record(RcdRecord &r) {
  SubAccessor accessor(r);

  // Check record type
  switch (r.recordType()) {

  case RcdHeader::configurationStart: {
    std::vector<const DhcLocationData<DhcFeConfigurationData>*>
      fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

    if (fe.size()>0) {
      _vtpd = *fe[0]->data()->vtpd();
      _injMask.word(*fe[0]->data()->inj());

      for (unsigned s(1); s<fe.size(); s++) {
	if (fe[s]->crateNumber() == _crate && fe[s]->slotNumber() == _slot) {
	  unsigned dcon = fe[s]->dhcComponent();
	  if (_stats.find(dcon) == _stats.end())
	    _stats[dcon] = new struct hitStats;

	_stats[dcon]->_tslast=0;
	_stats[dcon]->_trgs=0.0;
	}
      }
    }

    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    for (unsigned d(0); d<dc.size(); d++) {
      if (dc[d]->crateNumber() == _crate && dc[d]->slotNumber() == _slot) {
	_triggerDelay = *dc[d]->data()->triggerDelay();
	break;
      }
    }

    std::vector<const DaqConfigurationStart*> v(accessor.access<DaqConfigurationStart>());
    if(v.size()==1)
      if (v[0]->configurationNumberInRun() == 0)
	setup();

    break;
  }

  case RcdHeader::configurationEnd: {
    std::map<unsigned, struct hitStats*>::const_iterator it;

    std::vector<const DaqConfigurationEnd*> v(accessor.access<DaqConfigurationEnd>());
    if(v.size()==1)
      _configuration = v[0]->configurationNumberInRun()+1;

    if (_configuration%4 == 0) {
      for (it=_stats.begin(); it!=_stats.end(); it++) {
	struct hitStats* stats = (*it).second;

	for (unsigned a(0); a<MAX_CHIP; a++) {
	  for (unsigned c(0); c<MAX_CHAN; c++) {
	    if (stats->_trgs>0)
	      stats->_graph[a][c].SetPoint(_configuration/4-1, _vtpd,
					   stats->_hits[a][c]/stats->_trgs);
	    stats->_hits[a][c]=0.0;
	  }
	}
      }
    }
    break;
  }

  case RcdHeader::event: {
    std::map<unsigned, struct hitStats*>::const_iterator it;

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());
      
    for (unsigned s(0); s<v.size(); s++) {
      if (v[s]->crateNumber() == _crate && v[s]->slotNumber() == _slot) {
	for (it=_stats.begin(); it!=_stats.end(); it++) {
	  unsigned dcon = (*it).first;
	  struct hitStats* stats = (*it).second;

	  if (!v[s]->data()->verify())
	    std::cout << "Chksum Error" << std::endl;

	  for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
	    const DhcFeHitData* hits(v[s]->data()->feData(i));
	    if (hits->dconad() == dcon) {
	      if (hits->trg()) {
		double timestamp = hits->timestamp();
		stats->_histt[0].Fill(timestamp/10000.);
		if ( stats->_tslast > 0 ) {
		  double elapse = timestamp > stats->_tslast ?
		    timestamp - stats->_tslast :
		    timestamp + 10000000 - stats->_tslast;
		  stats->_histt[1].Fill(elapse/10000.);
		}		  
		stats->_tslast = timestamp;
		stats->_trgs++;
	    }
	    else {
	      // cut on timestamp relative to trigger timestamp
	      if (hits->timestamp() != (stats->_tslast-_triggerDelay))
		continue;

	      std::bitset<64> hitsHi(hits->hitsHi());
	      std::bitset<64> hitsLo(hits->hitsLo());
	      std::bitset<64> hitsAll(hitsHi<<32 | hitsLo);

	      unsigned a(hits->dcalad());
	      if (a<MAX_CHIP) {
		for (unsigned c(0); c<MAX_CHAN; c++) {
		  if (_injMask.bit(c%4)) {
		    double hit = hitsAll.test(c) ? 1.0 : 0.0;
		    stats->_hitmap[a].Fill(c%8, c/8, hit);
		    stats->_hits[a][c]+=hit;
		  }
		}
	      }
	    }
	    }
	  }
	}
      }
    }
    break;
  }

  default: {
    break;
  }
  }
}

void QScan::fileio() {

  RcdArena* arena(new RcdArena);
  RunReader reader;

  reader.directory(_runPath);
  assert(reader.open(_runNumber));
  
  while(reader.read(*arena)) {
    record(*arena);
  }
  reader.close();

  delete arena;
}


void QScan::setup() {

  std::map<unsigned, struct hitStats*>::const_iterator it;

  for (it=_stats.begin(); it!=_stats.end(); it++) {
    unsigned dcon = (*it).first;
    struct hitStats* stats = (*it).second;

    stats->_histt[0].SetNameTitle("Value", "Trigger Timestamp");
    stats->_histt[0].SetBins(100, 0.0, 100.);
    stats->_histt[0].GetXaxis()->SetTitle("msec");
    stats->_histt[1].SetNameTitle("Interval", "Trigger Timestamp");
    stats->_histt[1].SetBins(110, 0.0, 110.);
    stats->_histt[1].GetXaxis()->SetTitle("msec");

    for (unsigned a(0);a<MAX_CHIP;a++) {

      std::ostringstream sout;
      sout << " Chip" << a;

      stats->_hist[a].SetNameTitle("50% Setting", (sout.str()+" Threshold").c_str());
      stats->_hist[a].SetBins(80, 80.0, 160.);
      stats->_hist[a].GetXaxis()->SetTitle("vtpd");

      stats->_hitmap[a].SetNameTitle("Hits",(sout.str()+" Hit Map").c_str());
      stats->_hitmap[a].SetBins(8, 0.0, 8.,8, 0.0, 8.);

      for (unsigned c(0); c<MAX_CHAN; c++) {

	stats->_graph[a][c].SetLineColor(4);
	stats->_graph[a][c].SetMarkerColor(1);
	stats->_graph[a][c].SetMarkerStyle(6);
	
      }
    }
  }
}

void QScan::runNumber(const unsigned& r) {
  _runNumber = r;
}
void QScan::runPath(const std::string& f) {
  _runPath = f;
}

QScanFrame::QScanFrame(const TGWindow *p,UInt_t w,UInt_t h,
	     QScan* q, unsigned d)
  : TGMainFrame(p,w,h),
    _qscan(q),
    _dcon(d)
{

  SetCleanup(kDeepCleanup);

  fMenuDock = new TGDockableFrame(this);
  AddFrame(fMenuDock, new TGLayoutHints(kLHintsExpandX, 0, 0, 1, 0));
  fMenuDock->SetWindowName("Qscan Menu");

  fMenuBarLayout = new TGLayoutHints(kLHintsTop | kLHintsExpandX);
  fMenuBarItemLayout = new TGLayoutHints(kLHintsTop | kLHintsLeft, 0, 4, 0, 0);

  fMenuFile = new TGPopupMenu(fClient->GetRoot());
  fMenuFile->AddEntry("&Print", M_FILE_PRINT);
  fMenuFile->AddSeparator();
  fMenuFile->AddEntry("E&xit", M_FILE_EXIT);

  fMenuFile->Associate(this);

  fMenuBar = new TGMenuBar(fMenuDock, 1, 1, kHorizontalFrame);
  fMenuBar->AddPopup("&File", fMenuFile, fMenuBarItemLayout);

  fMenuDock->AddFrame(fMenuBar, fMenuBarLayout);

  fTab = new TGTab(this, 300, 300);

  TGCompositeFrame *tf;
  for (unsigned t(0); t<5; t++) {

    tf = fTab->AddTab(tabnames[t]);
    fL[t] = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX |
			      kLHintsExpandY, 5, 5, 5, 5);
    fF[t] = new TGCompositeFrame(tf, 60, 60, kHorizontalFrame);

    fEc[t] = new TRootEmbeddedCanvas (tabnames[t],fF[t],w,h);
    if (t<4)
      fEc[t]->GetCanvas()->Divide(MAX_CHIP/4, 4);
    else
      fEc[t]->GetCanvas()->Divide(2, 1);

    fF[t]->AddFrame(fEc[t], fL[t]);
    tf->AddFrame(fF[t], fL[t]);
  }

  AddFrame(fTab, fL[0]);

  std::ostringstream sout;
  sout << "DCON" << _dcon << "-QinjScan";

  SetWindowName(sout.str().c_str());

  MapSubwindows();
  Resize(GetDefaultSize());
  MapWindow();

}

void QScanFrame::CloseWindow()
{
  gApplication->Terminate(0);
}

Bool_t QScanFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t)
{

  switch (GET_MSG(msg)) {

  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {

    case kCM_MENUSELECT:
      //printf("Pointer over menu entry, id=%ld\n", parm1);
      break;

    case kCM_MENU:
      switch (parm1) {

      case M_FILE_PRINT:
	{
	  std::ostringstream sout;
	  sout << "data/QinjScan-" << tabnames[fTab->GetCurrent()] << ".png";
	  fEc[fTab->GetCurrent()]->GetCanvas()->Print(sout.str().c_str(), "png");
	}
	break;

      case M_FILE_EXIT:
	CloseWindow();   // this also terminates theApp
	break;

      default:
	break;
      }

    default:
      break;
    }

  default:
    break;
  }

  return kTRUE;
}

void QScanFrame::update() {

  struct hitStats* stats = _qscan->hit_stats(_dcon);

  Double_t xfirst,xlast,yfirst,ylast;
  for (unsigned a(0); a<MAX_CHIP; a++) {

    std::ostringstream sout;
    sout << " Chip" << a;

    _mgraph[a].SetTitle((sout.str()+" Efficiency vs Threshold").c_str());

    _fits[a].SetTitle((sout.str()+" Threshold vs ChanNo").c_str());

    for (unsigned c(0); c<MAX_CHAN; c++) {

      _mgraph[a].Add(&stats->_graph[a][c], "lp");

      Int_t npoints = stats->_graph[a][c].GetN();
      assert(npoints>0);
      stats->_graph[a][c].GetPoint(0,xfirst,yfirst);
      stats->_graph[a][c].GetPoint(npoints-1,xlast,ylast);

      if (yfirst > ylast) {

	TF1* sfit = new TF1("scurve", scurve, xfirst, xlast, 2);
	Double_t xstep((xlast-xfirst)/npoints);
	sfit->SetParameters(120, 2*xstep);
	sfit->SetParLimits(1, xstep/2, 16*xstep);
	sfit->SetLineWidth(1);

	if (stats->_graph[a][c].Fit(sfit, "Q")) {
	  std::cout << "Fit failed for "
		    << " DCON " << _dcon
		    << " DCal " << a
		    << " Chan " << c << std::endl;
	}
	_fits[a].SetPoint(c, c, sfit->GetParameter(0));
	_fits[a].SetPointError(c, 0, sfit->GetParError(0));
	stats->_hist[a].Fill(sfit->GetParameter(0));

	delete sfit;
      }
    }

    fEc[0]->GetCanvas()->cd(a+1);
    gPad->SetFillStyle(4100);

    _mgraph[a].Draw("a");

    fEc[1]->GetCanvas()->cd(a+1);
    stats->_hitmap[a].Draw();

    if (_fits[a].GetN() > 0) {
      fEc[2]->GetCanvas()->cd(a+1);
      _fits[a].SetMinimum(xfirst);
      _fits[a].SetMaximum(xlast);

      _fits[a].SetMarkerColor(1);
      _fits[a].SetMarkerStyle(6);
      _fits[a].Draw("ap");
    }
    fEc[3]->GetCanvas()->cd(a+1);
    stats->_hist[a].Draw();

    fEc[4]->GetCanvas()->cd(1)->SetLogy(0);
    stats->_histt[0].Draw();
    fEc[4]->GetCanvas()->cd(2)->SetLogy(1);
    stats->_histt[1].Draw();
  }

  for (unsigned t(0); t<5; t++) {
    fEc[t]->GetCanvas()->Update();
  }
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned width(argh.optionArgument('x',1024,"Width"));
  const unsigned height(argh.optionArgument('y',768,"Height"));
  const unsigned crate(argh.optionArgument('c',0xdc,"Crate"));
  const unsigned slot(argh.optionArgument('s',4,"Slot"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  TApplication _application("QinjScan Application",0,0);

  QScan* scan = new QScan(crate, slot);

  // set parameters
  scan->runNumber(runNumber);
  scan->runPath(runPath);
  scan->fileio();

  std::vector<QScanFrame*> frames;
  for (unsigned d(0); d<MAX_DCON; d++)
    if (scan->hit_stats(d))
      frames.push_back(new QScanFrame(gClient->GetRoot(), width, height, scan, d));
  
  std::vector<QScanFrame*>::const_iterator fit(frames.begin());
  for (; fit!=frames.end(); fit++)
    (*fit)->update();

  _application.Run(1);

}
