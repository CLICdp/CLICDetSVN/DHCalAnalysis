#include <iostream>
#include <string>
#include <map>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TStyle.h"

#include "TCanvas.h"
#include "TRandom2.h"
#include "THStack.h"
#include "TH1D.h"
#include "TH2I.h"
#include "TF1.h"

#include "RcdArena.hh"
#include "RunReader.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcEB.hh"

class MCPadPts {

public:
  MCPadPts(double& x, double& y, double& z) :
    Px(x), Py(y), Pz(z), Ts(0)
  {}

  MCPadPts(double& x, double& y, double& z, double& t) :
    Px(x), Py(y), Pz(z), Ts(t)
  {}

public:
  double Px;
  double Py;
  double Pz;
  double Ts;
};

bool tscomp(MCPadPts i, MCPadPts j)
{
  return (i.Pz < j.Pz);
}


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned rpcId(argh.optionArgument('c',0,"Chamber ID, 2-byte dcol-dcon #"));
  const unsigned tsAllow(argh.optionArgument('t',0,"Consecutive timestamp cut"));
  const unsigned xyAllow(argh.optionArgument('p',0,"Nearest pad cut"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));

  if(argh.help()) return 0;

  DhcEB* dhceb(new DhcEBIntTrg(runNumber));

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  reader.open(runNumber);

  std::vector<std::vector<DhcPadPts*>*> pts;
  std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;
  std::map<unsigned, std::vector<unsigned> > tdeltaEvt;
  std::map<unsigned, std::vector<unsigned> > tdeltaTs;
  std::map<unsigned, std::vector<double> > rdelta;
  std::map<unsigned, std::vector<DhcPadPts*>*> xyzt;

  int tslast[6] = {0xffffff,0xffffff,0xffffff,0xffffff,0xffffff,0xffffff};

  UtlTime _start;
  UtlTimeDifference _elapse;

  while (1) {

    if (pts.empty()) {
      // read more records until some events are built
      while(reader.read(*arena)) {

	switch(arena->recordType()) {

	case RcdHeader::acquisitionStart: {
	  _start = arena->RcdHeader::recordTime();
	  break;
	}

	case RcdHeader::acquisitionEnd: {
	  _elapse = arena->RcdHeader::recordTime() -_start;
	  break;
	}

	default:
	  break;
	}

	dhceb->record(*arena);

	if (!dhceb->raw_hitdata().empty()) {
	  std::vector<DhcFeHitData*>::const_iterator ih(dhceb->raw_hitdata().begin());
	  std::vector<DhcFeHitData*>::const_iterator ihe(dhceb->raw_hitdata().end());

	  for (; ih<ihe; ih++) {
	    //unsigned id((*ih)->dcolad()<<8 | (*ih)->dconad());
	    unsigned id(rpcId);

	    if (xyzt.find(id) == xyzt.end())
	      xyzt[id] = new std::vector<DhcPadPts*>;

	    dhceb->hits2pts(*ih, xyzt[id]);
	  }

	  std::map<unsigned, std::vector<DhcPadPts*>*>::iterator mit(xyzt.begin());
	  for (; mit!=xyzt.end(); mit++) {

	    std::vector<DhcPadPts*>::const_iterator it((*mit).second->begin());
	    std::vector<DhcPadPts*>::const_iterator ite((*mit).second->end());

	    for (++it; it<ite; it++) {
	      unsigned tDelta(((*it)->Ts - (*(it-1))->Ts));
	      double rDelta(sqrt(pow(((*it)->Px - (*(it-1))->Px),2) +
				 pow(((*it)->Py - (*(it-1))->Py),2))); 

	      if (tsAllow <= tDelta && xyAllow <= rDelta) {
		tdeltaTs[(*mit).first].push_back(tDelta);
		rdelta[(*mit).first].push_back(rDelta);
	      }		
	      delete *(it-1);
	    }
	    (*mit).second->clear();
	  }

	  // get copy of events
	  pts = dhceb->built_points();
	  break;
	}
      }
      // if still no events, at end of data
      if (pts.empty())
	break;

      for (unsigned i(0); i<6; i++) {
	for (ipts=pts.begin(); ipts!=pts.end(); ipts++) {

	  unsigned layers(dhceb->layers(*ipts));
	  if ((1<<i) <= layers) {
	    std::vector<DhcPadPts*>::iterator epts((*ipts)->begin());

	    if (tslast[i] < 0xffffff)
	      tdeltaEvt[i].push_back((*epts)->Ts - tslast[i]);

	    tslast[i] = (*epts)->Ts;
	  }
	}
	tslast[i] -= (int)1e7;
      }

      for (ipts=pts.begin(); ipts!=pts.end(); ipts++) {

	if (dhceb->layers(*ipts) > 2) {

	  std::vector<DhcPadPts*>::iterator epts((*ipts)->begin());
	  for (++epts; epts!=(*ipts)->end(); epts++) {

	    unsigned tDelta(((*epts)->Ts - (*(epts-1))->Ts));
	    double rDelta(sqrt(pow(((*epts)->Px - (*(epts-1))->Px),2) +
			       pow(((*epts)->Py - (*(epts-1))->Py),2))); 

	    if (tsAllow <= tDelta && xyAllow <= rDelta) {
	      tdeltaTs[rpcId+0x10000].push_back(tDelta);
	      rdelta[rpcId+0x10000].push_back(rDelta);
	    }		
	  }
	}
      }

      if(ipts==pts.end()) {
	// clear events
	dhceb->clear();
	pts.clear();
      }
    }
  }
  reader.close();

  delete dhceb;
  delete arena;


  TApplication _application("Noise Time Profile Application",0,0);
  gROOT->SetStyle();
  gStyle->SetOptStat("eo");
  gStyle->SetOptFit(1100);
  gStyle->SetStatFontSize(0.024);

  TCanvas *canvas[5];
  TH1D tdelta[2][6];
  double tmax[2][6] = {3e-4, 3e-3, 3e-2, 6e-2, 3e-1, 1.0};
  double par[2][6];
  TH1D tdeltaHits[8];
  TH2I txy;
  TH2I txyEvt;
  TH2I txyMC;

  for (unsigned p(0); p<2; p++) {

    for (unsigned i(0); i<6; i++) {
      std::ostringstream sout;
      sout << "At least " << (1<<i) << " Layer(s)";

      tdelta[p][i].SetNameTitle("Event tDelta", sout.str().c_str());
      tdelta[p][i].GetXaxis()->SetTitle("Event tDelta (sec)");
      tdelta[p][i].SetBins(100, 0, tmax[p][i]);
    }

    for (unsigned i(0); i<6; i++) {
      std::vector<unsigned>::iterator it(tdeltaEvt[i].begin());
      for (; it!=tdeltaEvt[i].end(); it++)
	tdelta[p][i].Fill((*it)/1e7);
    }

    std::ostringstream sout;
    sout << "Event Time Profiles - Pass " << p+1;

    canvas[p] = new TCanvas(sout.str().c_str(), sout.str().c_str(),
			    20+20*p, 40+20*p, 800, 600);
    canvas[p]->Divide(3,2);

    for (unsigned i(0); i<6; i++) {
      canvas[p]->cd(i+1)->SetLogy(1);

      TF1* tfit = new TF1("tdk", "expo(0)", 0, tmax[p][i]);
      tfit->SetRange(tmax[p][i]/100, tmax[p][i]);
      if (p==1) {
	tfit->SetParameter(0, par[0][i]);
	tfit->SetParameter(0, par[0][i]);
      }
      if (p==0) {
	tdelta[p][i].Fit(tfit, "QR");
	par[0][i] = tfit->GetParameter(0);
	par[1][i] = tfit->GetParameter(1);

	tmax[1][i] = (log(0.1) - tfit->GetParameter(0))/tfit->GetParameter(1);
	if (fabs(tmax[1][i]-tmax[0][i]) < tmax[0][i]/10)
	  tmax[1][i] = tmax[0][i];
      }
      else
	tdelta[p][i].Fit(tfit, "Q");

      tdelta[p][i].Draw();
    }
    canvas[p]->Update();
  }

  tdeltaHits[0].GetXaxis()->SetTitle("Timestamp Delta");
  tdeltaHits[0].SetBins(1000, 0, 1000);

  txy.SetBins(200, 0, 200, 150, 0, 150);
  txy.GetXaxis()->SetTitle("Hit tDelta (sec)");
  txy.GetYaxis()->SetTitle("Hit rDelta (cm)");
  txy.GetXaxis()->SetLabelSize(0.03);
  txy.GetXaxis()->SetTitleSize(0.03);
  txy.GetXaxis()->SetTitleOffset(2);
  txy.GetYaxis()->SetLabelSize(0.03);
  txy.GetYaxis()->SetTitleSize(0.03);
  txy.GetYaxis()->SetTitleOffset(2);

  {
    std::vector<unsigned>::iterator it(tdeltaTs[rpcId].begin());
    std::vector<double>::iterator rt(rdelta[rpcId].begin());
    for (; it!=tdeltaTs[rpcId].end(); it++, rt++) {
      tdeltaHits[0].Fill((*it));
      txy.Fill((*it), (*rt));
    }
  }

  canvas[2] = new TCanvas("Timestamp Delta Profile", "Timestamp Delta Profile",
			    20+20*2, 40+20*2, 800, 600);
  canvas[2]->Divide(3,2);

  int nRate, nSec;
  unsigned np(2);
  for (unsigned p(0); p<np; p++)
    par[p][0] = 0;

  for (unsigned i(0); i<6; i++) {
    canvas[2]->cd(i+1)->SetLogy(1);

    TF1* tfit;
    if (np==2)
      tfit = new TF1("tdk", "expo(0)", 0, 1000);
    else if (np==4)
      tfit = new TF1("tdk", "expo(0)+expo(2)", 0, 1000);

    tfit->SetRange(10*i, 1000);

    for (unsigned p(0); p<np; p++)
      tfit->SetParameter(p,par[p][0]);

    if (tdeltaHits[i].Fit(tfit, "QR"))
      tdeltaHits[i].Fit(tfit, "QR");

    for (unsigned p(0); p<np; p++) {
      par[p][0] = tfit->GetParameter(p);
    }

    nRate = int(-1e7 * tfit->GetParameter(1));
    std::ostringstream sout;
    sout << "Tmin " << 10*i
	 <<", Fitted Rate " << nRate << " events/sec";
    tdeltaHits[i].SetNameTitle("Time Stamp Delta", sout.str().c_str());

    tdeltaHits[i].Draw();
    tdeltaHits[i+1]=tdeltaHits[i];
  }
  canvas[2]->Update();

  nSec = int(_elapse.deltaTime());
  std::cout << nRate << " events/sec for " << nSec << " secs" << std::endl;

  // make copy
  txy.Copy(txyEvt);

  {
    std::vector<unsigned>::iterator it(tdeltaTs[rpcId+0x10000].begin());
    std::vector<double>::iterator rt(rdelta[rpcId+0x10000].begin());
    for (; it!=tdeltaTs[rpcId+0x10000].end(); it++, rt++) {
      txyEvt.Fill((*it), (*rt), -1);
    }
  }

  canvas[3] = new TCanvas("Timestamp Delta R Profile",
			  "Timestamp Delta R Profile",
			  20+20*3, 40+20*3, 800, 600);
  canvas[3]->Divide(2,2);

  canvas[3]->cd(1)->SetLogz(1);
  txy.SetTitle("All Hits");
  txy.Draw("lego");

  canvas[3]->cd(2)->SetLogz(1);
  txyEvt.SetTitle("Cosmic Ray Hits Removed");
  txyEvt.Draw("lego");
  canvas[3]->Update();


  TRandom2* r = new TRandom2();
  std::vector< std::vector<MCPadPts>*> mcpts;

  for (unsigned i(0); i<nSec; i++) {
    mcpts.push_back(new std::vector<MCPadPts>);

    for (unsigned j(0); j<nRate; j++) {
      double x(r->Uniform(96));
      double y(r->Uniform(96));
      double z(r->Uniform(1e7));
      mcpts[i]->push_back(MCPadPts(x, y, z));
    }
    // time order
    sort(mcpts[i]->begin(), mcpts[i]->end(), tscomp);
  }

  txy.Copy(txyMC);
  txyMC.Reset();

  for (unsigned i(0); i<nSec; i++) {
    std::vector<MCPadPts>::const_iterator it(mcpts[i]->begin());
    for (++it; it<mcpts[i]->end(); it++) {
      double rDelta(sqrt(pow(((*it).Px - (*(it-1)).Px),2) +
			 pow(((*it).Py - (*(it-1)).Py),2)));
      double tDelta(((*it).Pz - (*(it-1)). Pz));
      if (tsAllow <= tDelta && xyAllow <= rDelta)
	txyMC.Fill(tDelta, rDelta);
    }
  }

  canvas[3]->cd(3)->SetLogz(1);
  txyMC.SetTitle("MC Uniform Hits");
  txyMC.Draw("lego");
  canvas[3]->Update();

  THStack stack("hs", "Delta R Projections");
  TH1D* txyEvtPy = txyEvt.ProjectionY("Delta R");
  txyEvtPy->SetFillStyle(3001);
  txyEvtPy->SetFillColor(kRed);
  TH1D* txyMCPy = txyMC.ProjectionY("Delta R - MC random");
  txyMCPy->SetFillStyle(3001);
  txyMCPy->SetFillColor(kGreen);

  stack.Add(txyEvtPy);
  stack.Add(txyMCPy);

  canvas[3]->cd(4)->SetLogy(1);
  stack.Draw("nostack");
  canvas[3]->Update();

  TF1* tfits = new TF1("tdks", "expo(0)", 0, 200);
  tfits->SetRange(0, 200);
  TObjArray aSlices;
  txyEvt.FitSlicesX(tfits,0,-1,0,"QNR",&aSlices);

  TH1F* txy_1 = (TH1F*)aSlices[1];
  TH1F* txy_chi2 = (TH1F*)aSlices[2];

  canvas[4] = new TCanvas("Fitted Rates", "Fitted Rates",
			  20+20*4, 40+20*4, 800, 600);
  canvas[4]->Divide(1,2);

  canvas[4]->cd(1)->SetLogy(1);
  txy_1->Scale(-1e7);
  txy_1->Draw();
  canvas[4]->cd(2)->SetLogy(1);
  txy_chi2->Draw();
  canvas[4]->Update();

  _application.Run();

}
