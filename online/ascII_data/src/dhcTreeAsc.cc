#include <iostream>
#include <map>

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcCableMap.hh"
#include "DhcTree.hh"

#include "TTree.h"
#include "TFile.h"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const unsigned minLayers(argh.optionArgument('l',3,"Minimum layer cut"));
  const unsigned maxLayers(argh.optionArgument('m',40,"Maximum layer cut"));
  const std::string rootPath(argh.optionArgument('f',"root","Path of root tree data"));

  if(argh.help()) return 0;

  std::ostringstream sout;
  sout << rootPath << "/dhcTree-Run" << runNumber << ".root";

  std::map<Int_t, unsigned> layerz;
  // map z-coord to layer number
  DhcCableMap cmap(runNumber);
  DhcPadPts* pt;

  for (unsigned s(4); s<20; s++)
    for (unsigned d(0); d<12; d++)
      if ((pt = cmap.origin(0xdc, s, d)) != 0)
	layerz[(Int_t)(10*pt->Pz)] = 1;

  unsigned layer(1);
  std::map<Int_t, unsigned>::iterator mit;
  for (mit=layerz.begin(); mit!=layerz.end(); mit++)
    (*mit).second = layer++;

  int ts(0);
  int neg(-1);
  int zero(0);
  int xyoffset(-475);

  TFile* tfile = new TFile(sout.str().c_str());
  TTree* tree = (TTree*)tfile->Get("DhcTree");

  Event* event = new Event();
  TBranch* branch = tree->GetBranch("EventBranch");
  branch->SetAddress(&event);

  Int_t nevt = tree->GetEntries();
  for (Int_t i(0); i<nevt; i++) {

    branch->GetEntry(i);
    TClonesArray* pts = event->GetHits();
    
    Int_t nhits(event->GetNhits());

    if (minLayers <= event->GetNhitLayers()) {
	
      std::cout << ts << "\t"
		<< neg << "\t"
		<< zero << "\t"
		<< neg
		<< std::endl;

      for (Int_t j(0); j<nhits; j++) {
	DhcHit *hit((DhcHit*)((*pts)[j]));

	std::cout << hit->GetTs() << "\t"
		  << (hit->GetPx()-xyoffset)/10 << "\t"
		  << (hit->GetPy()-xyoffset)/10 << "\t"
		  << layerz[hit->GetPz()]
		  << std::endl;
      }
    }
    event->Clear();
  }
}
