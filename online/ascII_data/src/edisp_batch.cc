// prototype event display

#include <sstream>
#include <map>
#include <utility>
#include <vector>
#include <cassert>

#include "RcdArena.hh"
#include "RcdRecord.hh"
#include "RunReader.hh"

#include "DaqEvent.hh"
#include "SubAccessor.hh"

#include "UtlArguments.hh"

#include "DhcEventData.hh"
#include "DhcFeHitData.hh"

#include <TROOT.h>
#include <TError.h>
#include <TStyle.h>
#include <TSystem.h>
#include <TApplication.h>
#include <TCanvas.h>

#include "TView3D.h"
#include "TPolyMarker3D.h"

#include "DhcEB.hh"
#include "DhcGeom.hh"


class EDispFrame {

private:
  TCanvas* _canvas;
  TPad *pad[5];
  TView *view[5];

  TPolyMarker3D* _pm3d;
  std::vector<std::vector<DhcPadPts*>*> pts;
  std::vector<std::vector<DhcPadPts*>*>::const_iterator ipts;

  std::string _file;
  UtlTime _recordTime;
  unsigned _runNumber;
  unsigned _eventNumber;
  unsigned _maxEvents;
  unsigned _minLayers;
  unsigned _maxPoints;
  unsigned _minPoints;
  unsigned _maxTDelta;
  std::string _rpcGeom;
  std::string _runPath;
  double _bbox;
  double _wangle;
  double _zpivot;
  unsigned _nviews;

  RcdArena* _arena;
  RunReader _reader;

  DhcEB* _dhceb;
  DhcGeom* _dhcgeom;

  void setup(double& bbox);
  void cleanup();

  virtual void CloseWindow();
  bool DoDraw();
  void DoPrint();

public:
  EDispFrame(UInt_t w,UInt_t h);
  virtual ~EDispFrame();

  void run();
  void initialize();
  void runNumber(const unsigned& r);
  void minLayers(const unsigned& l);
  void maxEvents(const unsigned& n);
  void maxPoints(const unsigned& p);
  void minPoints(const unsigned& p);
  void maxTDelta(const unsigned& t);
  void rpcGeom(const std::string& g);
  void runPath(const std::string& f);
  void bBox(const double& b);
  void wAngle(const double& a);
};

EDispFrame::EDispFrame(UInt_t w,UInt_t h)
  : _pm3d(0), _eventNumber(0), _nviews(0), _dhceb(0)
{
  _canvas = new TCanvas(" ", " ", 20, 20, w, h);

  // remove existing file
  unlink("root/animation.gif");
}

void EDispFrame::CloseWindow()
{
  gApplication->Terminate();
}

void EDispFrame::run()
{
  while (_eventNumber < _maxEvents) {
    if (DoDraw())
      DoPrint();
  }
}

void EDispFrame::initialize()
{
  _arena = new RcdArena;
  _reader.directory(_runPath);
  assert(_reader.open(_runNumber));

  // determine run type
  while (!_dhceb) {

    _reader.read(*_arena);
    switch (_arena->recordType()) {

    case RcdHeader::runStart: {
      // Access the DaqRunStart
      SubAccessor accessor(*_arena);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      DaqRunType runType=v[0]->runType();

      switch(runType.type()) {

      case DaqRunType::dhcNoise: {
	_dhceb = new DhcEBIntTrg(_runNumber);
	break;
      }

      case DaqRunType::beamData:
      case DaqRunType::dhcBeam:
      case DaqRunType::dhcQinj:
      case DaqRunType::dhcCosmics: {
	_dhceb = new DhcEBExtTrg(_runNumber);
	break;
      }

      default: {
	std::cerr << "Run type " << runType.typeName() << " not supported" << std::endl;
	CloseWindow();
	break;
	}
      }
    }
    default:
      break;
    }
  }

  if (_rpcGeom == "Slice") {
    _dhcgeom = new DhcSliceGeom();
  }
  else if (_rpcGeom == "Cubic") {
    _dhcgeom = new DhcCubicGeom();
  }
  else if (_rpcGeom == "CubicTC") {
    _dhcgeom = new DhcCubicTCGeom();
  }
  else if (_rpcGeom == "Cuboid") {
    _dhcgeom = new DhcCuboidGeom();
  }
  else {
    std::cerr << "Geometry " << _rpcGeom << " not implemented" << std::endl;
    CloseWindow();
  }

  if (_minPoints)
    _maxPoints = 0xffffffff;

  setup(_bbox);
}

EDispFrame::~EDispFrame()
{
  _reader.close();
  cleanup();
}

void EDispFrame::cleanup()
{
  // clear points
  if (_pm3d)
    delete _pm3d;;

  for (int i=0; i<_nviews; i++) {
    if (view[i])
      delete view[i];
    if (pad[i])
      delete pad[i];
  }

  if (_dhceb)
    delete _dhceb;

  delete _arena;
}

void EDispFrame::setup(double& bbox)
{
  int iret;
  double rmin[3] = {-bbox, -bbox, 0};
  double rmax[3] = {bbox, bbox, 2*bbox};

  // distort bbox based on geometry
  if (_rpcGeom == "CubicTC") {
    rmax[2]*=2.4;
  }
  else if (_rpcGeom == "Cuboid") {
    rmax[2]*=1.5;
  }
  double elong = (rmax[2]-rmin[2]) / (rmax[1]-rmin[1]);
  double vSplit = elong/(elong+1);

  TGeoManager::SetVerboseLevel(0);
  _dhcgeom->transform(new TGeoCombiTrans(_zpivot*sin(_wangle/57.2958), 0, 0,
					 new TGeoRotation("y-axis",-90,_wangle,90)));
  TGeoVolume* dhcLayout(_dhcgeom->layout(bbox));
  const std::vector<DhcView*> views(_dhcgeom->views());
  _nviews = views.size();

  for (int i=0; i<_nviews; i++) {

    _canvas->cd();
    switch (i) {
    case 0:
      pad[i] = (_nviews == 5) ?
	new TPad("Top", "Top", 0, 0, 0.33, 0.33) :
	new TPad("Top", "Top", 0, 0, 1-vSplit, vSplit);
      break;

    case 1:
      pad[i] = (_nviews == 5) ?
	new TPad("Front", "Front", 0, 0.33, 0.33, 0.67) :
	new TPad("Front", "Front", 0, vSplit, 1-vSplit, 1);
      break;

    case 2:
      pad[i] = (_nviews == 5) ?
	new TPad("Side", "Side", 0, 0.67, 0.33, 1) :
	new TPad("Side", "Side", 1-vSplit, vSplit, 1, 1);
      break;

    case 3:
      pad[i] = (_nviews == 5) ?
	new TPad("Pers1", "Pers1", 0.33, 0, 1, 0.5) :
	new TPad("Pers1", "Pers1", 1-vSplit, 0, 1, vSplit);
      break;

    case 4:
      pad[i] = new TPad("Pers2", "Pers2", 0.33, 0.5, 1, 1);
      break;

    default:
      break;
    }

    pad[i]->Draw();
    pad[i]->cd();

    dhcLayout->Draw();

    view[i] = new TView3D(1,rmin,rmax);

    DhcView* v(views[i]);
    view[i]->SetView(v->phi, v->theta, v->psi, iret);
    view[i]->ZoomView(pad[i],v->zoom);

    pad[i]->SetTheta(90 - v->theta);
    pad[i]->SetPhi(-90 - v->phi);

    switch (i) {
    case 0: // top
    case 1: // front
    case 2: // side
      view[i]->SetParallel();
      break;

    case 3:
    case 4:
      view[i]->SetPerspective();
      view[i]->SetDview(2*view[i]->GetDview());
      view[i]->SetDproj(2*view[i]->GetDproj());

      if (elong > 2) {
	view[i]->MoveWindow('l');
	view[i]->MoveWindow('i');
      }
      break;

    default:
      break;
    }
  }
}

bool EDispFrame::DoDraw()
{
  bool status(false);

  // if no more sets of pts to plot,, read more events
  if (pts.empty()) {
    // read more records until some events are built
    while(_reader.read(*_arena)) {
      _dhceb->record(*_arena);

      if (!_dhceb->built_hitdata().empty()) {
	// get copy of events
	pts = _dhceb->built_points();
	ipts = pts.begin();
	// savelast record ime
	_recordTime = _arena->recordTime();
	break;
      }
    }
  }

  // if still no points to draw, must be at end of data
  if (pts.empty()) {
    CloseWindow();
    return true;
  }

  // cut on minimum no. of layers
  while ( ipts!=pts.end() &&
	  (_dhceb->layers(*ipts) < _minLayers ||
	   _dhceb->maxPtsPerLayer(*ipts) < _minPoints ||
	   _dhceb->maxPtsPerLayer(*ipts) > _maxPoints ))
    ipts++;

  if(ipts!=pts.end()) {
    // next vector  of points
    std::vector<DhcPadPts*>* _pts = *ipts;

    _canvas->cd();

    // clear TPolyMarker3D points
    if (_pm3d)
      delete _pm3d;;
    
    _pm3d = new TPolyMarker3D(_pts->size(), 8);
    _pm3d->SetMarkerSize(0.5);
    _pm3d->SetMarkerColor(kRed);

    std::vector<DhcPadPts*>::const_iterator pt(_pts->begin());
    unsigned ts((*(pt+_pts->size()/2))->Ts);

    for (unsigned j=0; j<_pts->size(); j++, pt++) {
      if (abs((*pt)->Ts - ts) < _maxTDelta)
	_pm3d->SetPoint(j, (*pt)->Px, (*pt)->Py, (*pt)->Pz);
    }

    // draw points in all views
    for (int i=0; i<_nviews; i++) {
      pad[i]->cd();
      _pm3d->Draw();
      pad[i]->Draw();
    }

    _eventNumber++;
    _canvas->Update();
    status = true;
    ipts++;
  }

  // check for end of events
  if(ipts==pts.end()) {
    // clear events
    _dhceb->clear();
    pts.clear();
  }
  return status;
}

void EDispFrame::DoPrint()
{
  _canvas->Print("root/animation.gif+10");
}

void EDispFrame::runNumber(const unsigned& r) {
  _runNumber = r;
}
void EDispFrame::minLayers(const unsigned& l) {
  _minLayers = l;
}
void EDispFrame::maxEvents(const unsigned& n) {
  _maxEvents = n;
}
void EDispFrame::maxPoints(const unsigned& p) {
  _maxPoints = p;
}
void EDispFrame::minPoints(const unsigned& p) {
  _minPoints = p;
}
void EDispFrame::maxTDelta(const unsigned& t) {
  _maxTDelta = t;
}
void EDispFrame::rpcGeom(const std::string& g) {
  _rpcGeom = g;
}
void EDispFrame::runPath(const std::string& f) {
  _runPath = f;
}
void EDispFrame::bBox(const double& b) {
  _bbox = b;
}
void EDispFrame::wAngle(const double& a) {
  _wangle = a;
  _zpivot = 20;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned width(argh.optionArgument('x',680,"Width"));
  const unsigned height(argh.optionArgument('y',680,"Height"));
  const unsigned minLayers(argh.optionArgument('l',8,"Minimum hit layers for displayed events"));
  const unsigned maxEvents(argh.optionArgument('n',100,"Maximump number of displayed events"));
  const unsigned maxPoints(argh.optionArgument('p',300,"Maximump points/layer for displayed events"));
  const unsigned minPoints(argh.optionArgument('q',0,"Minimum points/layer for displayed events"));
  const unsigned maxTDelta(argh.optionArgument('t',2,"Maximum timestamp delta for displayed events"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));
  const std::string rpcGeom(argh.optionArgument('g',"Cuboid","Geometry (Slice|Cubic|CubicTC)"));
  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const int angle(argh.optionArgument('a',0,"Rotation angle (degrees) of DHCal"));

  if(argh.help()) return 0;

  gROOT->SetStyle();
  gErrorIgnoreLevel=kWarning;
  gStyle->SetCanvasPreferGL(true);

  TApplication _application("Event Display Application",0,0);

  EDispFrame fEd(width, height);

  // set parameters
  fEd.runNumber(runNumber);
  fEd.runPath(runPath);
  fEd.minLayers(minLayers);
  fEd.maxEvents(maxEvents);
  fEd.maxPoints(maxPoints);
  fEd.minPoints(minPoints);
  fEd.maxTDelta(maxTDelta);
  fEd.rpcGeom(rpcGeom);
  fEd.bBox(55);
  fEd.wAngle((double)angle);

  fEd.initialize();
  fEd.run();

}
