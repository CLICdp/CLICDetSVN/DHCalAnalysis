#include <iostream>
#include <sstream>
#include <fstream.h>

#include "RcdArena.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

#include "UtlArguments.hh"

#include "DhcFeHitData.hh"

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  //unsigned slot(argh.optionArgument('s',3,"Slot"));
  const std::string runList(argh.optionArgument('l',"","file containing list of runs to study"));

  if(argh.help()) return 0;

  //TApplication _application("empty events plots",0,0);
  //gROOT->Reset();

  std::stringstream sin;
  sin << runList;

  ifstream in;
  in.open(sin.str().c_str());

  unsigned runNumber;
  std::string runPath;

  std::vector<double> percEmpty;
  std::vector<unsigned> runNo;

  while (1) {

  in >> runNumber;

  if (runNumber < 600224) 
    runPath = "/srv/data02/data/fnalCombinedOct10/run";
  else {
    std::cout<<std::endl<<"run number out of range, check runList file";
    break;
  }

  unsigned countEmptyEvents = 0;
  unsigned countSubRecordsEvents = 0;

  if (!in.good()) break;

  RcdArena* arena(new RcdArena);
  RunReader reader;

  reader.directory(runPath);
  assert(reader.open(runNumber));
  while(reader.read(*arena)) {
    // use EmptyEvent class methods here
      switch (arena->recordType()) {
        case RcdHeader::event: {

          countSubRecordsEvents++;

          SubAccessor accessor(*arena);
          std::vector<const DhcLocationData<DhcEventData>*>
            v(accessor.access<DhcLocationData<DhcEventData> >());

          unsigned nHitPacks = 0;

          for (unsigned s(0); s<v.size(); s++) {
            if ((v[s]->data()->numberOfWords())>0) {
              for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
                const DhcFeHitData* hits(v[s]->data()->feData(i));
                if (!hits->trg()) {
                  nHitPacks++;
                }
              }
            }
          }
          if (nHitPacks == 0) countEmptyEvents++;

          break;
        }
        default: {break;}
      };
  }
  reader.close();
  delete arena;

  std::cout<<std::endl;
  std::cout<<"Run "<<runNumber;
  std::cout<<" Percentage Empty: "
           <<((double)countEmptyEvents)/((double)countSubRecordsEvents)*100;
  std::cout<<std::endl<<std::endl;

  percEmpty.push_back(((double)countEmptyEvents)/((double)countSubRecordsEvents)*100);
  runNo.push_back(runNumber);

  } // done processing all run in runList

  in.close();

  std::cout<<std::endl<<"recall"<<std::endl;
  std::cout<<std::endl<<"Run Number    "<<"Percent Empty %";
  for (int i = 0; i < runNo.size(); i++) {
  std::cout<<std::endl;
  std::cout<<runNo.at(i);
  std::cout<<"        "<<percEmpty.at(i);
  }

  std::cout<<std::endl<<std::endl;
  //_application.Run();

}
