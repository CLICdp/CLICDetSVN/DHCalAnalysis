#include <iostream>
#include <vector>
#include <map>
#include <cassert>

#include "RcdArena.hh"
#include "RunReader.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"

#include "SubAccessor.hh"

#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "DhcBeConfigurationData.hh"
#include "DhcFeHitData.hh"

#include "DhcCableMap.hh"


class ChkDhcData : public RcdUserRO {

public:
  ChkDhcData(const unsigned& runNumber);
  virtual ~ChkDhcData() {}

  bool record(const RcdRecord &r);
  void report();

private:
  std::map<unsigned, unsigned> _dcolmap;

  DhcCableMap* _cmap;
  bool _configurationEnded;

};

ChkDhcData::ChkDhcData(const unsigned& runNumber)
  : RcdUserRO(9)
{
  // connect to SQL db.
  _cmap = new DhcCableMap(runNumber);
}

bool ChkDhcData::record(const RcdRecord &r) {

  SubAccessor accessor(r);
  
  switch(r.recordType()) {

  case RcdHeader::configurationStart: {

    _configurationEnded = false;

    std::vector<const DaqConfigurationStart*>
      v(accessor.extract<DaqConfigurationStart>());
    assert(v.size()==1);

    std::cout << std::endl << r.recordTime()
	      << " Dhc BE configuration number " << v[0]->configurationNumberInRun()
	      << " in run." << std::endl;

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    assert(be.size()>0);

    std::vector<const DhcLocationData<DhcBeConfigurationData>*>::iterator bit;
    for (bit=be.begin(); bit!=be.end(); bit++) {
      if (!(*bit)->write()) {

	UtlPack loc(0);
	loc.byte(3, ((*bit)->crateNumber()));
	loc.byte(2, ((*bit)->slotNumber()));
	_dcolmap[loc.word()] = 0;

	if (!(*bit)->data()->locked()) {

	  UtlPack dconEnabled(0);
	  dconEnabled.word(((*bit)->data()->dconEnable()));

	  for (unsigned i(0); i<12; i++) 
	    if (dconEnabled.bit(i) != dconEnabled.bit(i+16))
	      std::cout << " Side "
			<< _cmap->label(loc.byte(3), loc.byte(2), i)
			<< " Crate " << static_cast<unsigned int>(loc.byte(3))
			<< " Slot " << static_cast<unsigned int>(loc.byte(2))
			<< " Data Collector input " << i << " not locked"
			<< std::endl;
	}
      }
    }

    std::vector<const DhcReadoutConfigurationData*>
      ro(accessor.access<DhcReadoutConfigurationData>());

    assert(ro.size()>0);

    std::vector<const DhcReadoutConfigurationData*>::const_iterator rt;
    for (rt=ro.begin(); rt!=ro.end(); rt++) {
      unsigned c(static_cast<unsigned int>((*rt)->crateNumber()));

      // loop over DCOLS
      for (unsigned s(4); s<20; s++) {
	if ((*rt)->slotEnable(s)) {
	  UtlPack loc(0);
	  loc.byte(3, c);
	  loc.byte(2, s);

	  if (_dcolmap.find(loc.word()) == _dcolmap.end()) {
	    std::cout << " Crate " << static_cast<unsigned int>(loc.byte(3))
		      << " Slot " << static_cast<unsigned int>(loc.byte(2))
		      << " Data Collector enaabled, but not readable" << std::endl;
	  }
	}
      }
    }
    _dcolmap.clear();

    break;
  }

  case RcdHeader::event: {

    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());

    for (unsigned s(0); s<v.size(); s++) {
      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
	const DhcFeHitData* hits(v[s]->data()->feData(i));

	UtlPack loc(0);
	loc.byte(3, (v[s]->crateNumber()));
	loc.byte(2, (v[s]->slotNumber()));
	loc.byte(1, hits->dconad());

	if (!hits->verify()) {
	  loc.byte(0, 0);
	  _dcolmap[loc.word()]++;
	}
	if (hits->err()) {
	  loc.byte(0, 1);
	  _dcolmap[loc.word()]++;
	}
      }
    }
    break;
  }

  case RcdHeader::configurationEnd: {

    report();

    _configurationEnded = true;
    break;
  }

  default:
    break;
  }
  return true;
}

void ChkDhcData::report()
{
  if (_configurationEnded)
    return;

  std::cout << std::endl;

  if (_dcolmap.empty())
    std::cout << " No Data Collector reports chksum or data errors." << std::endl;
  else {
    std::map<unsigned, unsigned>::const_iterator it;
    for (it=_dcolmap.begin(); it!=_dcolmap.end(); it++) {

      UtlPack loc((*it).first);
      std::string outOfRange;
      if (loc.byte(1)>11)
	outOfRange = " (out of range (0-11))";

      std::cout << " Side "
		<< _cmap->label(loc.byte(3), loc.byte(2), loc.byte(1))
		<< " Crate " << static_cast<unsigned int>(loc.byte(3))
		<< " Slot " << static_cast<unsigned int>(loc.byte(2))
		<< " Data Collector input" << static_cast<unsigned int>(loc.byte(1))
		<< outOfRange;
      if (loc.byte(0) == 0)
	std::cout << " reports " << (*it).second << " chksum errors.";
      else if (loc.byte(0) == 1)
	std::cout << " reports " << (*it).second << " data errors.";
      std::cout << std::endl;
    }
  }
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const std::string runPath(argh.optionArgument('f',"data/run","Path where run data is located"));
  const unsigned runNumber(argh.optionArgument('r',999999,"Run number"));

  if(argh.help()) return 0;

  ChkDhcData* cdd = new ChkDhcData(runNumber);

  RcdArena* arena(new RcdArena);
  RunReader reader;
  reader.directory(runPath);
  assert(reader.open(runNumber));
  
  while(reader.read(*arena)) {

    if (arena->totalNumberOfBytes() > sizeof(*arena)/2)
      continue;

    if(cdd!=0) cdd->record(*arena);
  }
  cdd->report();
  reader.close();

  delete cdd;
  delete arena;
}
