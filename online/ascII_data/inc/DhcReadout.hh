//
// $Id: DhcReadout.hh,v 1.8 2008/02/18 15:04:49 jls Exp $
//

#ifndef DhcReadout_HH
#define DhcReadout_HH

#include <algorithm>
#include <bitset>
#include <deque>

#include "VMEAddressTable.hh"
#include "VMEAddressTableASCIIReader.hh"
#include "DaqBusAdapter.hh"
#include "HardwareAccessException.hh"

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"
#include "SubModifier.hh"

#include "DhcVmeDevice.hh"
#include "DhcEventData.hh"
#include "DhcFeHitData.hh"

#include "DhcReadoutConfigurationData.hh"

const uint32_t MAXIMUM_SUBRECORD_SIZE = 0XFF00;
const uint32_t DEVICE_BUFFER_SIZE = 1024*1024;
const uint32_t MINIMUM_TRIGGER_DELTA_TIME = 100; // 10 usec
const uint32_t MAXIMUM_TRIGGER_DELTA_TIME = 100000; // 10 msec


class DhcReadout : public RcdUserRW {

public:
  DhcReadout(unsigned b, unsigned char c) : 
    _interface(b), _location(c,0,0,0), _crateNumber(c), _busAdapter(0),
    _addressTableReader("online/hal/DhcDcolVmeAddress.hal"),
    _addressTable("DCOL VME Address Table",_addressTableReader) {

    for(unsigned i(0);i<=15;i++) _device[i]=0;

    // Catch exceptions locally here in case PCI card not installed
    try {
      _busAdapter=new DaqBusAdapter(HAL::CAENLinuxBusAdapter::V2718,
				    _interface.byte(1), _interface.byte(0));
    } catch ( std::exception e ) {
      std::ostringstream message;
      message << " DhcReadout::ctor() "
	      << " PCI card " << (unsigned)_interface.byte(1)
	      << " chain " << (unsigned)_interface.byte(0)
	      << " NOT found ";
      std::cerr << message.str() << std::endl;

      _busAdapter=0;
    }

    // Set crate number of readout configuration
    _config.crateNumber(c);

  }

  virtual ~DhcReadout() {

    for(unsigned i(0);i<=15;i++)
      if(_device[i]!=0) {

	delete [] _buffers[i];
	_device[i]=0;
      }

    if(_busAdapter!=0) delete _busAdapter;
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "DhcReadout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    // Catch exceptions from HAL code
    try {

      // Check record type
      switch (r.recordType()) {

      case RcdHeader::startUp: {
	if(_busAdapter!=0) {
	  for(unsigned s(0);s<=15;s++) {
	    _device[s]=new DhcVmeDevice(_addressTable,*_busAdapter,s+4);
	    std::ostringstream message;
	    message << "DhcReadout::record() "
		    << " PCI card " << (unsigned)_interface.byte(1)
		    << " chain " << (unsigned)_interface.byte(0)
		    << " crate 0x" << std::hex << (unsigned)_crateNumber
		    << std::dec
		    << " slot " << std::setw(2) << s+4;
	    if(_device[s]->alive()) {
	      message << " found alive";
	      std::cout << message.str() << std::endl;

	      // 4 Mbytes / Data Collector
	      _buffers[s] = new unsigned[DEVICE_BUFFER_SIZE];

	      // put in known state
	      assert(_device[s]->standby());

	    } else {
	      message << " NOT found alive";
	      std::cerr << message.str() << std::endl;

	      delete _device[s];
	      _device[s]=0;
	    }
	  }
	}
	break;
      }

      case RcdHeader::runStart: {
	// Access the DaqRunStart to get print level 
	SubAccessor accessor(r);
	std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
	if(v.size()==1) {
	  _printLevel=v[0]->runType().printLevel();
	  _bufferRun=v[0]->runType().bufferRun();
	  _beamData=(v[0]->runType().type() == DaqRunType::beamData);
	  _runType=v[0]->runType();
	}

	assert(reset());
	assert(readRunData(r));
	assert(readSlowRunData(r));

	_nXfers=0;
	break;
      }

      case RcdHeader::runEnd:
      case RcdHeader::runStop: {
	assert(readRunData(r));
	assert(readSlowRunData(r));
	break;
      }

	// Configuration start is used to set up system
      case RcdHeader::configurationStart: {
	assert(writeSlowConfigurationData(r));
	assert(readSlowConfigurationData(r));

	assert(writeConfigurationData(r));
	assert(readConfigurationData(r));

	break;
      }

      case RcdHeader::acquisitionStart: {
	// Flush out any data
	assert(flushEventData());

	_nEvents=0;
	_nTrgs=0;
	break;
      }

      case RcdHeader::acquisitionEnd:
      case RcdHeader::acquisitionStop: {
	_nXfers++;
	break;
      }

      case RcdHeader::spillStart: {
	break;
      }

      case RcdHeader::spill: {
	break;
      }

      case RcdHeader::spillEnd:
      case RcdHeader::transferStart: {
	assert(transferEventData());
	break;
      }

      case RcdHeader::transferEnd: {
	break;
      }
      
      case RcdHeader::trigger: {
	_nTrgs++;
	break;
      }

      case RcdHeader::event: {
	_nEvents++;
	beWait(r);
	if (_nEvents > _nXferred)
	  assert(transferEventData());

	assert(readEventData(r));
	break;
      }

      case RcdHeader::shutDown: {
	break;
      }

      case RcdHeader::slowReadout: {
	assert(readSlowReadoutData(r));
	break;
      }

      default: {
	break;
      }
      };

    } catch ( HAL::HardwareAccessException& e ) {
      reportError(e.what());
      
    } catch ( std::runtime_error e ) {
      reportError(e.what());

    } catch ( std::exception e ) {
      std::cout << "*** Unknown exception occurred" << std::endl;
      std::cerr << "*** Unknown exception occurred" << std::endl;
    }

    return true;
  }

  void reportError(const std::string& s) {
    std::ostringstream message;
    UtlTime mt(true);
    message << "*** Exception occurred : " << mt <<std::endl
	    << s;

    std::cout << message.str() << std::endl;
    std::cerr << message.str() << std::endl;
  }

  bool reset() {
    for(unsigned i(0);i<=15;i++) {
      if(_device[i]!=0) {
	assert(_device[i]->reset());
      }
    }
    return true;
  }

  bool beWait(RcdRecord &r) {

    // no need to wait in combined running or buffered data run
    if (_beamData || _bufferRun)
      return true;

    UtlTime trgTime(r.recordTime());
    UtlTime now(true);
    while ((now - trgTime) < _config.beWaitInterval())
      now.update();

    return true;
  }

  bool flushEventData() {

    // Flush out any data
    for(unsigned i(0);i<=15;i++) {
      if(_device[i]!=0) {
	_device[i]->bufferReset();
	_bedata[i] = _endata[i] = _buffers[i];

	_hitdata[i] = _bedata[i];
	_ttsdata[i].clear();
      }
    }
    // number of events transferred
    _nXferred = 0xffffffff;;

    return true;
  }

  bool transferEventData() {

    for(unsigned i(0);i<=15;i++) {
      if(_device[i]!=0) {

	// transfer event data to _buffers
	_endata[i] = _bedata[i] +
	  _device[i]->bufferRead(_bedata[i], 4*DEVICE_BUFFER_SIZE);

	// update trigger timestamp, begin data pointers
	updateEventData(i);
	_bedata[i] = _endata[i];
      }
    }
    // number of events transferred
    _nXferred = _nTrgs;

    return true;
  }

  bool updateEventData(const unsigned& key) {

    DhcFeHitData* hits;
    DhcFeHitData* endata = (DhcFeHitData*)(_endata[key]);

    if (_ttsdata[key].empty()) {

      // find first trigger timestamp
      hits = (DhcFeHitData*)(_bedata[key]);
      while (hits < endata) {
	if (hits->trg()) {
	  _ttsdata[key].push_back((unsigned*)hits);
	  break;
	}
	hits++;
      }
    }

    if (!_ttsdata[key].empty()) {

      hits = (DhcFeHitData*)(_ttsdata[key].back());
      unsigned thisevent = hits->timestamp();

      while (hits < endata) {
	if (hits->trg())
	  if (abs(hits->timestamp() - thisevent) > MINIMUM_TRIGGER_DELTA_TIME) {
	    thisevent = hits->timestamp();
	    _ttsdata[key].push_back((unsigned*)hits);
	  }
	hits++;
      }
    }
    return true;
  }

  bool parseEventData(const unsigned& key, DhcLocationData<DhcEventData>* p) {

    if (_ttsdata[key].empty())
      return false;

    DhcFeHitData* substart((DhcFeHitData*)(p->data()->data()));
    DhcFeHitData* subdata(substart);

    DhcFeHitData* begindata((DhcFeHitData*)(_hitdata[key]));
    DhcFeHitData* endata((DhcFeHitData*)(_endata[key]));
    DhcFeHitData* hits((DhcFeHitData*)(_ttsdata[key].front()));

    unsigned thisevent = hits->timestamp();
    _ttsdata[key].pop_front();

    while (hits < endata) {
      unsigned timestamp(hits->timestamp());
      if (hits->trg()) {
	if (abs(thisevent - timestamp) < MINIMUM_TRIGGER_DELTA_TIME)
	  memcpy(subdata++, hits, sizeof(DhcFeHitData));
	else
	  if (abs(thisevent - timestamp) > MAXIMUM_TRIGGER_DELTA_TIME)
	    break;
      }
      else {
	// don't re-scan out of order data
	if (begindata <= hits) {
	  if (abs(thisevent - timestamp) < MINIMUM_TRIGGER_DELTA_TIME)
	    memcpy(subdata++, hits, sizeof(DhcFeHitData));
	  else {
	    _hitdata[key] = (unsigned*)hits;
	    break;
	  }
	}
      }
      hits++;
    }

    // byte count of hit data,
    uint32_t byteCount((subdata - substart)*sizeof(DhcFeHitData));

    //   limit size of sub-record created to <64k Kbytes
    byteCount = std::min(byteCount, MAXIMUM_SUBRECORD_SIZE);

    // set number of words in record
    p->data()->numberOfWords(byteCount/4);

    return true;
  }

  bool readEventData(RcdRecord &r) {
    // Single event readout
    SubInserter inserter(r);

    for(unsigned i(0);i<=15;) {
      if(_device[i]!=0 && _config.slotEnable(i+4)) {

	_location.slotNumber(_device[i]->slot());
	_location.label(0);
	    
	// Make the object in the record
	DhcLocationData<DhcEventData>
	  *p(inserter.insert< DhcLocationData<DhcEventData> >());
	p->location(_location);

	// Do the copy
	if (_bufferRun) {

	  if (CALICE_DAQ_SIZE < r.totalNumberOfBytes()) {
	    // throw exception
	    throw std::runtime_error("Record size exceeds CALICE_DAQ_SIZE");
	  }
	  // find event boundaries from timestamps
	  if (!parseEventData(i++, p))
	    p->data()->numberOfWords(0);
	}
	else {
	  if (CALICE_DAQ_SIZE < (r.totalNumberOfBytes() + _device[i]->bufferCount())) {
	    // flush all data
	    assert(_device[i]->bufferReset());
	    // throw exception
	    throw std::runtime_error("Record size exceeds CALICE_DAQ_SIZE");
	  }
	  // limit size of sub-record created to <64k Kbytes
	  assert(_device[i]->readEventData(*(p->data()), MAXIMUM_SUBRECORD_SIZE));

	  // only increment slot number if readout was't size limited 
	  if (p->data()->numberOfWords() < MAXIMUM_SUBRECORD_SIZE/4)
	    i++;
	}
	// extend the record
	inserter.extend(p->data()->numberOfWords()*4);

	if(doPrint(r.recordType(),1)) p->print(std::cout) << std::endl;
	if(doPrint(r.recordType(),2)) p->data()->print(std::cout,"",true);

	// Check data structure, attempt device reset if errors
	if (!checkEventData(p))
	  assert(_device[i]->reset());

	if(doPrint(r.recordType(),1)) {
	  if (!p->data()->verify())
	    p->print(std::cout,"NOT VERIFIED ") << std::endl;
	}
      }
      else
	i++;
    }
    return true;
  }

  bool checkEventData(DhcLocationData<DhcEventData>* p) {
    // Check only first few events in run
    if (_nXfers > 0 || _nEvents > 2)
      return true;

    if (p->data()->verify())
      return true;

    std::ostringstream message;
    UtlTime mt(true);
    message << "Dhc Data Collector Error : " << mt <<std::endl
	    << " Crate:" << static_cast<unsigned int>(p->location().crateNumber())
	    << " Slot: " << static_cast<unsigned int>(p->location().slotNumber())
	    << std::endl;

    std::cout << message.str() << std::endl;
    std::cerr << message.str() << std::endl;

    return false;
  }

  bool writeConfigurationData(RcdRecord &r) {
    SubAccessor accessor(r);

    std::vector<const DhcReadoutConfigurationData*>
      bc(accessor.access< DhcReadoutConfigurationData>());

    if(bc.size()>0) {
      for(unsigned j(0);j<bc.size();j++) {
	if(bc[j]->crateNumber()==_crateNumber) {
    	  _config=*(bc[j]);
	  if(doPrint(r.recordType(),1)) _config.print(std::cout) << std::endl;

	  // select FE revision level for enabled slots
	  for(unsigned i(0);i<=15;i++) {
	    if(_device[i]!=0 && _config.slotEnable(i+4)) {
	      _device[i]->feRevision(_config.slotFeRevision(i+4));
	    }
	  }
	}
      }
    }

    // Handle BE
    std::vector<const DhcLocationData<DhcBeConfigurationData>*>
      be(accessor.access< DhcLocationData<DhcBeConfigurationData> >());

    // Check for consistency and store values for this crate
    std::vector<const DhcLocationData<DhcBeConfigurationData>*> bCrate;

    for(unsigned j(0);j<be.size();j++) {
      assert(be[j]->slotBroadcast() || be[j]->slotNumber()<=19);
      assert(be[j]->dhcComponent()==DhcLocation::be);

      if(be[j]->crateNumber()==_location.crateNumber())
	bCrate.push_back(be[j]);
    }

    if(doPrint(r.recordType(),1)) {
      std::cout << " Number of DhcBeConfigurationData write subrecords accessed for crate = "
		<< bCrate.size() << " (of " << be.size() << " total)" << std::endl;

      for(unsigned i(0);i<bCrate.size();i++) {
	bCrate[i]->print(std::cout,"  ") << std::endl;
      }
    }

    // Look for configuration for all slots first
    for(unsigned j(0);j<bCrate.size();j++) {
      if(bCrate[j]->slotBroadcast()) {
	  
	// Loop over all the DCOLs
	for(unsigned i(0);i<=15;i++) {
	  if(_device[i]!=0 && _config.slotEnable(i+4)) {

	    DhcBeConfigurationData cbcd(*bCrate[j]->data());

	    if(doPrint(r.recordType(),1)) {
	      std::cout << " Writing to slot " << std::setw(2) << i+4 << " ";
	      cbcd.print(std::cout,"  ") << std::endl;
	    } 
	    assert(_device[i]->writeBeConfigurationData(cbcd));
	  }
	}
      }
    }

    // Now look for individual slot configurations to override the broadcast
    for(unsigned j(0);j<bCrate.size();j++) {
      if(!bCrate[j]->slotBroadcast()) {
	  
	unsigned i(bCrate[j]->slotNumber());
	if(_device[i-4]!=0 && _config.slotEnable(i)) {

	  DhcBeConfigurationData cbcd(*bCrate[j]->data());

	  if(doPrint(r.recordType(),1)) {
	    std::cout << " Writing to slot " << std::setw(2) << i << " ";
	    cbcd.print(std::cout,"  ") << std::endl;
	  } 
	  assert(_device[i-4]->writeBeConfigurationData(cbcd));
	}
      }
    }

    // pause for counter reset
    sleep(1);

    // Handle FE (Concentrators)
    std::vector<const DhcLocationData<DhcDcConfigurationData>*>
      dc(accessor.access< DhcLocationData<DhcDcConfigurationData> >());

    if(doPrint(r.recordType(),1))
      std::cout << " Number of DhcDcConfigurationData write subrecords accessed = "
		<< dc.size() << std::endl << std::endl;

    // First do slot broadcast
    for(unsigned j(0);j<dc.size();j++) {
      if(dc[j]->crateNumber()==_crateNumber && dc[j]->slotBroadcast()) {
	if(doPrint(r.recordType(),1)) dc[j]->print(std::cout) << std::endl;

	// Loop over all the DCOLs
	for(unsigned i(0);i<=15;i++) {
	  if(_device[i]!=0) {
	    if(_config.slotEnable(i+4)) {
	      if(dc[j]->dhcComponent()==DhcLocation::feBroadcast) {
		for(unsigned f(0);f<12;f++) {
		  if(_config.slotFeEnable(i+4,f)) {
		    if (!_device[i]
			->writeDcConfigurationData((DhcLocation::DhcComponent)f,*dc[j]->data()))
		      reportFeError(DhcLocation(_crateNumber, i+4, f, 1), 31);
		  }
		}
	      } else {
		if (!_device[i]
		    ->writeDcConfigurationData(dc[j]->dhcComponent(),*dc[j]->data()))
		  reportFeError(DhcLocation(_crateNumber, i+4, dc[j]->dhcComponent(), 1), 31);
	      }
	    }
	  }
	}
      }
    }

    // Next do FE broadcast 
    for(unsigned j(0);j<dc.size();j++) {
      if(dc[j]->crateNumber()==_crateNumber && !dc[j]->slotBroadcast()) {
	assert(dc[j]->slotNumber()<=19);
	
	if(dc[j]->dhcComponent()==DhcLocation::feBroadcast) {
	  
	  if(_device[dc[j]->slotNumber()-4]!=0) {
	    if(doPrint(r.recordType(),1)) dc[j]->print(std::cout) << std::endl;
	    for(unsigned f(0);f<12;f++) {
	      if(_config.slotFeEnable(dc[j]->slotNumber(),f)) {
		if (!_device[dc[j]->slotNumber()-4]
		    ->writeDcConfigurationData((DhcLocation::DhcComponent)f,*dc[j]->data()))
		  reportFeError(DhcLocation(_crateNumber, dc[j]->slotNumber(), f, 1), 31);
	      }
	    }
	  }
	}
      }
    }

    // Finally do individual DC configurations
    for(unsigned j(0);j<dc.size();j++) {
      if(dc[j]->crateNumber()==_crateNumber) {

	if(!dc[j]->slotBroadcast() && dc[j]->dhcComponent()!=DhcLocation::feBroadcast) {

	  assert(dc[j]->slotNumber()<=19);
	  if(_device[dc[j]->slotNumber()-4]!=0) {

	    if(_config.slotFeEnable(dc[j]->slotNumber(),dc[j]->dhcComponent())) {
	      
	      if(doPrint(r.recordType(),1)) dc[j]->print(std::cout) << std::endl;
	      if (!_device[dc[j]->slotNumber()-4]
		  ->writeDcConfigurationData(dc[j]->dhcComponent(),*dc[j]->data()))
		reportFeError(DhcLocation(_crateNumber, dc[j]->slotNumber(), dc[j]->dhcComponent(), 1),
			      31);
	    }
	  }
	}
      }
    }

    // Handle FE (ASICs)
    std::vector<const DhcLocationData<DhcFeConfigurationData>*>
      fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

    if(doPrint(r.recordType(),1))
      std::cout << " Number of DhcFeConfigurationData write subrecords accessed = "
		<< fe.size() << std::endl << std::endl;

    // First do slot broadcast
    for(unsigned j(0);j<fe.size();j++) {
      if(fe[j]->crateNumber()==_crateNumber && fe[j]->slotBroadcast()) {
	if(doPrint(r.recordType(),1)) fe[j]->print(std::cout) << std::endl;

	// Loop over all the DCOLs
	for(unsigned i(0);i<=15;i++) {
	  if(_device[i]!=0) {
	    if(_config.slotEnable(i+4)) {
	      if(fe[j]->dhcComponent()==DhcLocation::feBroadcast) {
		for(unsigned f(0);f<12;f++) {
		  if(_config.slotFeEnable(i+4,f)) {
		    if (!_device[i]
			->writeFeConfigurationData((DhcLocation::DhcComponent)f,*fe[j]->data()))
		      reportFeError(DhcLocation(_crateNumber, i+4, f, 1), *fe[j]->data()->chipid());
		  }
		}
	      } else {
		if (!_device[i]
		    ->writeFeConfigurationData(fe[j]->dhcComponent(),*fe[j]->data()))
		  reportFeError(DhcLocation(_crateNumber, i+4, fe[j]->dhcComponent(), 1),
				*fe[j]->data()->chipid());
	      }
	    }
	  }
	}
      }
    }

    // Next do FE broadcast 
    for(unsigned j(0);j<fe.size();j++) {
      if(fe[j]->crateNumber()==_crateNumber && !fe[j]->slotBroadcast()) {
	assert(fe[j]->slotNumber()<=19);
	
	if(fe[j]->dhcComponent()==DhcLocation::feBroadcast) {
	  
	  if(_device[fe[j]->slotNumber()-4]!=0) {
	    if(doPrint(r.recordType(),1)) fe[j]->print(std::cout) << std::endl;
	    for(unsigned f(0);f<12;f++) {
	      if(_config.slotFeEnable(fe[j]->slotNumber(),f)) {
		if (!_device[fe[j]->slotNumber()-4]
		    ->writeFeConfigurationData((DhcLocation::DhcComponent)f,*fe[j]->data()))
		  reportFeError(DhcLocation(_crateNumber, fe[j]->slotNumber(), f, 1),
				*fe[j]->data()->chipid());
	      }
	    }
	  }
	}
      }
    }


    // Finally do individual FE configurations
    for(unsigned j(0);j<fe.size();j++) {
      if(fe[j]->crateNumber()==_crateNumber) {

	if(!fe[j]->slotBroadcast() && fe[j]->dhcComponent()!=DhcLocation::feBroadcast) {

	  assert(fe[j]->slotNumber()<=19);
	  if(_device[fe[j]->slotNumber()-4]!=0) {

	    if(_config.slotFeEnable(fe[j]->slotNumber(),fe[j]->dhcComponent())) {
	      
	    if(doPrint(r.recordType(),1)) fe[j]->print(std::cout) << std::endl;
	    if (!_device[fe[j]->slotNumber()-4]
		->writeFeConfigurationData(fe[j]->dhcComponent(),*fe[j]->data()))
		reportFeError(DhcLocation(_crateNumber, fe[j]->slotNumber(), fe[j]->dhcComponent(), 1),
			      *fe[j]->data()->chipid());
	    }
	  }
	}
      }
    }

    return true;
  }

  bool readConfigurationData(RcdRecord &r) {
    SubInserter inserter(r);
    
    for(unsigned i(0);i<=15;i++) {
      if(_device[i]!=0 && _config.slotEnable(i+4)) {
	DhcLocation cl(_crateNumber,_device[i]->slot(),DhcLocation::be,0);

	DhcLocationData<DhcBeConfigurationData>
	  *b(inserter.insert< DhcLocationData<DhcBeConfigurationData> >());
	b->location(cl);
	assert(_device[i]->readBeConfigurationData(*b->data()));
	if(doPrint(r.recordType(),1)) b->print(std::cout,"  ") << std::endl;
	if (!b->data()->locked()) b->print(std::cerr,"  ") << std::endl;

	std::bitset<32> dconEnabled(b->data()->dconEnable());

	for(unsigned f(0);f<12;f++) {
	  if(_config.slotFeEnable(i+4,f) && dconEnabled.test(f+16)) {
	    cl.dhcComponent((DhcLocation::DhcComponent)f);
	    DhcLocationData<DhcDcConfigurationData>
	      *d(inserter.insert< DhcLocationData<DhcDcConfigurationData> >());

	    d->location(cl);
	    d->data()->chipid(31);
	    if (!_device[i]->readDcConfigurationData(cl.dhcComponent(),*d->data()))
	      reportFeError(cl, 31);
	    if(doPrint(r.recordType(),1)) d->print(std::cout) << std::endl;

	    unsigned maxChip = (_config.slotFeRevision(i+4) == 2) ? 4 : 24;
	    for (unsigned c(0);c<maxChip;c++) {
	      DhcLocationData<DhcFeConfigurationData>
		*e(inserter.insert< DhcLocationData<DhcFeConfigurationData> >());

	      e->location(cl);
	      e->data()->chipid(c);
	      if (!_device[i]->readFeConfigurationData(cl.dhcComponent(),*e->data()))
		reportFeError(cl, c);
	      if(doPrint(r.recordType(),1)) e->print(std::cout) << std::endl;
	    }
	  }
	}
      }
    }
    
    return true;
  }

  bool readRunData(RcdRecord &r) {
    SubInserter inserter(r);
    
    for(unsigned i(0);i<=15;i++) {
      if(_device[i]!=0 && _config.slotEnable(i+4)) {
	DhcLocation cl(_crateNumber,_device[i]->slot(),DhcLocation::feBroadcast,0);

	for(unsigned f(0);f<12;f++) {
	  if(_config.slotFeEnable(i+4,f)) {
	    cl.dhcComponent((DhcLocation::DhcComponent)f);
	    DhcLocationData<DhcDcRunData>
	      *d(inserter.insert< DhcLocationData<DhcDcRunData> >());

	    d->location(cl);
	    d->data()->chipid(31);
	    if (!_device[i]->readDcRunData(cl.dhcComponent(),*d->data()))
	      reportFeError(cl, 31);

	    if(doPrint(r.recordType(),12)) d->print(std::cout) << std::endl;

	    if (!_device[i]->resetDcRunData(cl.dhcComponent(),*d->data()))
	      reportFeError(cl, 31);
	  }
	}
      }
    }
    return true;
  }

  void reportFeError(DhcLocation location, unsigned chip) {

    std::ostringstream message;
    UtlTime mt(true);
    message << "Dhc FE Slow Control Error : " << mt <<std::endl
	    << " Crate:" << static_cast<unsigned int>(location.crateNumber())
	    << " Slot: " << static_cast<unsigned int>(location.slotNumber())
	    << " Link: " << static_cast<unsigned int>(location.dhcComponent())
	    << " ChipID: " << chip
	    << std::endl;

    std::cout << message.str() << std::endl;
    std::cerr << message.str() << std::endl;
  }

  bool readSlowRunData(RcdRecord &r) {

    return true;
  }

  bool readSlowReadoutData(RcdRecord &r) {

    return true;
  }

  bool writeSlowConfigurationData(RcdRecord &r) {

    return true;
  }

  bool readSlowConfigurationData(RcdRecord &r) {

    return true;
  }


protected:
  UtlPack _interface;
  DhcLocation _location;
  unsigned char _crateNumber;
  DaqBusAdapter *_busAdapter;
  HAL::VMEAddressTableASCIIReader _addressTableReader;
  HAL::VMEAddressTable _addressTable;

  DhcVmeDevice *_device[16];
  DhcReadoutConfigurationData _config;

  unsigned _nEvents;
  unsigned _nTrgs;
  unsigned _nXferred;
  unsigned _nXfers;
  bool _bufferRun;
  bool _beamData;
  DaqRunType _runType;

  unsigned* _buffers[16];
  unsigned* _bedata[16];
  unsigned* _endata[16];
  unsigned* _hitdata[16];
  std::deque<unsigned*> _ttsdata[16];
};

#endif // DhcReadout_HH
