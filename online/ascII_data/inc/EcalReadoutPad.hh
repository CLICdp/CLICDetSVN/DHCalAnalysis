//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalReadoutPad_HH
#define EcalReadoutPad_HH

#include <iostream>
#include <fstream>
#include <string>

//////////////////////////////////////////////////////////////////////////////

/// Class to hold channel id as seen by DAQ.
/** There are in total 22 slots, each slot has 8 frontend connectors to 
    read 12 chips of 18 channels each. Each DAQ channel is uniquely 
    identified by these 4 integers.
*/
class EcalReadoutPad 
{

   public:

//............................................................................	
   /// Constructor.
   /**
   */
   EcalReadoutPad() : _value(0xffff) 
   {
   }
//............................................................................
   /// Constructor, see operator++. 
   /**
   */
   EcalReadoutPad(unsigned short v) : _value(v) 
   {	
      assert(isValid());
   }
//............................................................................
   /// Constructor with attributes.
   /**
   */
   EcalReadoutPad(unsigned slot, unsigned frontend, unsigned chip, unsigned channel) 
   : _value(18*(12*(8*slot+frontend)+chip)+channel) 
   {
      assert(slot<22 && frontend<8 && chip<12 && channel<18);
   }
//............................................................................   
   /// Destructor.
   /**
   */
   ~EcalReadoutPad() 
   {
   }
//............................................................................
   /// Return packed id.
   /**
   */
   unsigned short value() const 
   {
      return _value;
   }
//............................................................................
/*
   void value(unsigned short v) 
   {
      _value=v;
      assert(this->isValid());
   }
*/
//............................................................................
   /// Return slot number.
   /**
   */
   unsigned slot() const 
   {
      return _value/(8*12*18);
   }
//............................................................................
   /// Return frontend number.
   /**
   */
   unsigned frontend() const 
   {
      return (_value/(12*18))%8;
   }
//............................................................................
   /// Return chip number.
   /**
   */
   unsigned chip() const 
   {
      return (_value/18)%12;
   }
//............................................................................
   /// Return channel number.
   /**
   */
   unsigned channel() const 
   {
      return _value%18;
   }
//............................................................................
   /// Print out channel id.
   /**
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalReadoutPad::print()" << std::endl;
      out << " Value    = " << _value << std::endl;
      out << " Slot     = " << std::setw(2) << slot() << std::endl;
      out << " Frontend = " << std::setw(2) << frontend() << std::endl;
      out << " Chip     = " << std::setw(2) << chip() << std::endl;
      out << " Channel  = " << std::setw(2) << channel() << std::endl;
      return out;
   }
//............................................................................
   /// Check if channel has id within valid range.
   /**
   */
   bool isValid()
   {
      bool flag = false;
      //if(_value >= _begin && _value < _end) flag = true;
      if(_value < _end) flag = true;
      return flag; 
   }
//............................................................................
   /// Convenient increment operator. 
   /** To be used along with isValid() in for-loops to access all channels.
       I.e. "for(EcalReadoutPad rp(0);rp.isValid();rp++)". 
   */
   void operator ++ (int)
   {
      _value++;
   } 
//............................................................................

   private:
	
   //static const unsigned short _begin=0;
   
   /// Total DAQ channels.
   /**
   */
   static const unsigned short _end=22*8*12*18;
   
   /// Packed channel id.
   /**
   */
   unsigned short _value;

};//end class EcalReadoutPad

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
