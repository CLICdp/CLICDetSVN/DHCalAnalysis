//
// $Id: EbDhcErrorLog.hh,v 1.2 2009/19/09 14:55:00 jacobsmith Exp $
//

#ifndef EbDhcErrorLog_HH
#define EbDhcErrorLog_HH

// c++
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <math.h>
#include <fstream.h>

// daq
#include "RcdRecord.hh"
#include "DaqEvent.hh"
#include "SubAccessor.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"

// dhc
#include "DhcReadoutConfigurationData.hh"
#include "DhcFeHitData.hh"

// crc
#include "CrcBeTrgHistoryHit.hh"
#include "CrcVlinkTrgData.hh"

// dhc event builder
#include "EbDhcEventTrig.hh"
#include "EbDhcDTCalculator.hh"
#include "EbDhcGeom.hh"

// root
#include "TROOT.h"
#include "TH1.h"
#include "TFile.h"
#include "TCanvas.h"

class EbDhcErrorLog {

public:
  //EbDhcErrorLog() 
  EbDhcErrorLog(const unsigned &r, const std::string& w, unsigned &t)
  {
    /*_aout << w << "run" << r << ".beam.txt";
    std::cout<<std::endl << "ascii file name: " << _aout.str().c_str();
    std::ofstream tfile;
    tfile.open(_aout.str().c_str(),ofstream::trunc);
    tfile.close();*/
    
    _rout << w << "run" << r << "errors.root";
    std::cout<<std::endl << "root file name: " << _rout.str().c_str() <<std::endl;
    std::cout<<std::endl; 

  _rfile = new TFile(_rout.str().c_str(), "recreate");
//  _cdhts = _rfile->mkdir("hts");

    _hNTrgPacks = new TH1I("hNTrgPacks","No. Trg Packs All Events",300,0,300);
    _hCorruptLog = new TH1I("hCorruptLog","Corruption Log",20,0,20);
    _hErrLog = new TH1I("hErrLog","Error Log",16,0,16);
    _hErrLogAd = new TH1I("hErrLogAd","Error Log Address",11520,0.,11520.);

    _timestampRange = t;
    _eventRecordCount = 0;
    _NTotalEventSubRecords = 0;
    for (int i(0);i<20;i++) { _corruptLog[i]=0;}
    for (int i(0);i<11520;i++) { _errorLog[i]=0;}

// data for tcmt ::event
    _configCounter = -1;

  /*// histograms
  _canvas = new TCanvas("canvas","canvas",1);
  _canvas->Divide(3,3);
  _canvasPost = new TCanvas("canvasPost","canvasPost",1);
  _canvasPost->Divide(3,3);
  _cDcolData = new TCanvas("_cDcolData","Data Pop (dcol)",1);
  _cDcolData->Divide(1,2);
  _cCrate = new TCanvas("cCrate","cCrate",1);
  _cCrate->Divide(4,4);

  std::stringstream tempT;
  std::stringstream tempN;
  for (unsigned i(0); i<9; i++) {
    _hist[i] = new TH1F();
    tempN.str("");
    tempN << "h_" <<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    tempT.str("");
    tempT << "h_"<<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    _hist[i]->SetNameTitle(tempN.str().c_str(),tempT.str().c_str());
    _hist[i]->SetBins(96,0.,96.);

    _histP[i] = new TH1F();
    tempN.str("");
    tempN << "hP_" <<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    tempT.str("");
    tempT << "hP_"<<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    _histP[i]->SetNameTitle(tempN.str().c_str(),tempT.str().c_str());
    _histP[i]->SetBins(96,0.,96.);
       
  }

  for (unsigned i(0); i<13; i++) {
    _hCrate[i] = new TH1F();
    tempN.str("");
    tempN << "hCrate_" <<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    tempT.str("");
    tempT << "hCrate_"<<std::setfill('0')<<std::hex<<std::setw(2)<<i;
    _hCrate[i]->SetNameTitle(tempN.str().c_str(),tempT.str().c_str());
    _hCrate[i]->SetBins(10,215.,225.);
  }

  _hDcolData = new TH1F("_hDcolData","Data Pop (dcol)",40,0.,40.);
  _hDconTrgPckCnt = new TH1F("_hDconTrgPckCnt","dcon trg pck cnt",500,0.,500.);
*/
  }

  /*void update() {
    for (unsigned i(0);i<9;i++) {
      _canvas->cd(i+1)->SetLogy(1);
      _hist[i]->Draw();
      _canvasPost->cd(i+1)->SetLogy(1);
      _histP[i]->Draw();
    }
    for (unsigned i(0);i<16;i++) {
      _cCrate->cd(i+1)->SetLogy(1);
      if (i<13) _hCrate[i]->Draw();
    }
    _canvas->Update();
    _canvasPost->Update();
    _cCrate->Update();

    _cDcolData->cd(1);
    _hDcolData->Draw();
    _cDcolData->cd(2);
    _hDconTrgPckCnt->Draw();
    _cDcolData->Update();
  }*/

  virtual ~EbDhcErrorLog() {
  }

  void writeRoot();

  void report() { // change variable names probably
  
    for (unsigned i(0); i < 11520; i++)
      if (_errorLog[i]!=0)
        std::cout<<std::endl<<"chip: "<<i<<", reports error: "<<_errorLog[i];
    
    std::cout<<std::endl;

  }

  void event(RcdRecord &r);

private:

/*// data for histograms
  TCanvas* _cCrate;
  TCanvas* _canvas;
  TCanvas* _canvasPost;
  TCanvas* _cDcolData;
  TH1F* _hCrate[13];
  TH1F* _hist[9];
  TH1F* _histP[9];
  TH1F* _hDcolData;
  TH1F* _hDconTrgPckCnt;
  DhcFeHitData* _geomcopy;*/

// data for data checking
  unsigned _nDcon;
  unsigned _nCrates;
  unsigned* _vmeadEnable;
  unsigned** _dcoladEnable;
  unsigned*** _dconadEnable;

// data for tcmt ::event
  int _configCounter;

// data for dhcal 
  TFile* _rfile;

  TH1I* _hNTrgPacks;
  TH1I* _hCorruptLog;
  TH1I* _hErrLog;
  TH1I* _hErrLogAd;
//  TDirectory* _cdhts;

  std::ostringstream _rout;
  std::ofstream _afile;

  unsigned _timestampRange;
  unsigned _runNumber;
  unsigned _NTotalEventSubRecords;
  unsigned _eventRecordCount; // counts event subrecords
  unsigned _corruptLog[20];
  short unsigned _errorLog[11520]; //size = no. of unique chip addresses


};

#ifdef CALICE_DAQ_ICC

void EbDhcErrorLog::event(RcdRecord &r) {
    
    SubAccessor accessor(r);
   
    switch (r.recordType()) { // check record type

    // contains dhcal hit data and trg timestamps
    // also contains tcmt data (including crc trigger signals)
    case RcdHeader::configurationStart: {

     if (_eventRecordCount == 0) {
      std::vector<const DhcReadoutConfigurationData*>
        vcd(accessor.access<DhcReadoutConfigurationData>());
    
      // vcd size is number of crates

  _nCrates = vcd.size();
  _vmeadEnable = new unsigned[vcd.size()];
  _dcoladEnable = new unsigned*[vcd.size()];
  _dconadEnable = new unsigned**[vcd.size()];
  for (unsigned i = 0; i<vcd.size(); i++) {
    _vmeadEnable[i] = 0;
    _dcoladEnable[i] = new unsigned[20];
    _dconadEnable[i] = new unsigned*[20];
    for (unsigned j = 0; j<20; j++) {
      _dcoladEnable[i][j] = 0;
      _dconadEnable[i][j] = new unsigned[12];
      for (unsigned k = 0; k<12; k++) {
        _dconadEnable[i][j][k] = 0;
      }
    }
  }
 
      for(unsigned i(0);i<vcd.size();i++) { // one for each crate
        _vmeadEnable[i] = 1;
        for(unsigned s(2);s<=21;s++) { // one for each dcol in a crate
          if (s>3) {
            if (vcd[i]->slotEnable(s)){
              _dcoladEnable[i][s-4] = 1;

              for (unsigned f(0);f<12;f++) {
                if (vcd[i]->slotFeEnable(s,f)) {
                  _nDcon+=1;
                  _dconadEnable[i][s-4][f] = 1;
                } 
              }
            }
          }
        } // end slot (dcol)loop
      } // end vcd loop
      } // end condition for eventRecordCount==0

      _configCounter++;
      break;
    }
    case RcdHeader::event: {

    _eventRecordCount+=1;


/*------------------------------------------------------------
------------ DHCAL ----------- DATA --------------------------
---------------------BELOW----------------------------------*/


    std::vector<const DhcLocationData<DhcEventData>*>
	v(accessor.access<DhcLocationData<DhcEventData> >());

    //std::cout<<std::endl<<"started event "<<_eventRecordCount;
    EbDhcDTCalculator* calc = new EbDhcDTCalculator();
    bool firsteventhit = true;
    bool firsteventtrg = true;
    bool trgswitch = false;
    bool  hitswitch = false; // switches to find misplaced hit packs
                            // a hit pack is seen
    unsigned firsthitts = 0;
    unsigned firsttrgts = 0;
    bool corrupt = false;
    unsigned trgPackCount = 0;
    unsigned hitOutOfTSRangeCount = 0;
    bool hitTSrange = true;
    unsigned vmead = 0;
    
//    _cdhts->cd();

    unsigned dconTrgPckCnt = 0; // count dcon's trg packs
    for (unsigned s(0); s<v.size(); s++) { //v size is number of DCOLs

      trgswitch = false;
      hitswitch = false;

      if ((v[s]->data()->numberOfWords())>0) { // checks if dcol has data
        //_hDcolData->Fill(s);
        for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) { 
        //numberOfWords/4 is total number of data packages in a DCOL

          // reset counts and package status
          corrupt = false;

          // set vmead ad
          vmead = ((unsigned)v[s]->location().crateNumber()) - 220;

          // extracts data from arena 
          const DhcFeHitData* hits(v[s]->data()->feData(i));
          hitTSrange = true;

          if (hits->verify()) {
            if (hits->err() > 0) { // log error bit information 
                _hErrLog->Fill(hits->err());
                if (!hits->trg()             &&
                    hits->dcolad() < 20      && 
                    hits->dconad() < 12      && 
                    vmead < _nCrates &&
                    //hits->vmead() < _nCrates &&
                    hits->dcalad() < 24 ) {
                  // decode formula
                  // value = input for filling hErrLogAd
                  // 2*12*20*24 = 11520 unique chip addresses
                  // 5760 = 12*20*24 --> unique chip addresses in one crate
                  // 288 = 12*24 --> unique chip addresses in one slot (dcol)
                  // 24 --> unique chip addresses in one dcol input (dcon) (feb)
                  // vmead = value / 5760
                  // dcolad = (value % 5760) / 288
                  // dconad = (value % 288) / 24
                  // dcalad = (value % 288) % 24
                  _hErrLogAd->Fill((vmead*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad()));
                  //_hErrLogAd->Fill((hits->vmead()*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad()));
                  _errorLog[(vmead*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad())] = 
                  //_errorLog[(hits->vmead()*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad())] = 
                    (short unsigned)(hits->err());
                  _hCorruptLog->Fill(9); //error on hit pack
                }
                if (hits->trg()) {
                  _hCorruptLog->Fill(10); //error on trg pack
                }
              corrupt = true; 
              _hCorruptLog->Fill(1);
            } // errors
            if (vmead > (_nCrates-1)) { 
            //if (hits->vmead() > (_nCrates-1)) { 
              corrupt = true; 
              _hCorruptLog->Fill(2);
            }// vmead
            if (vmead < _nCrates) {
            //if (hits->vmead() < _nCrates) {
              if (hits->dcolad() < 20) {
                if (_dcoladEnable[vmead][hits->dcolad()] == 0) {
                //if (_dcoladEnable[hits->vmead()][hits->dcolad()] == 0) {
                  corrupt = true;// dcolad 
                  _hCorruptLog->Fill(3);
                }

                if (hits->dconad() < 12) {
                  if (_dconadEnable[vmead][hits->dcolad()][hits->dconad()]==0) {
                  //if (_dconadEnable[hits->vmead()][hits->dcolad()][hits->dconad()]==0) {
                    corrupt = true;// dconad
                    _hCorruptLog->Fill(4);
                  }
                }
                else {
                }
              }
              else {
              }
            }

          if (hits->trg()) dconTrgPckCnt+=1;

          // check for data corruption
          // corruption is unrealistic addressess for trg and hit 
          // hits
          if (!hits->trg()) {
            
            /*_hCrate[s%13]->Fill((unsigned)v[s]->location().crateNumber());
            // first just look at hits
            EbDhcGeom* geom = new EbDhcGeom();
            _geomcopy = new DhcFeHitData;
            memmove(_geomcopy,hits,sizeof(DhcFeHitData));
            geom->init(_geomcopy,(unsigned)v[s]->location().crateNumber() );
            //_hCrate[s%13]->Fill((unsigned)v[s]->location().crateNumber());
            if (geom->layerNumber() < 9) {
            unsigned hitXsize = geom->hitXvec().size();
            for (unsigned i(0);i<hitXsize;i++) {
              _hist[geom->layerNumber()]->Fill(geom->hitXvec().at(i));
            }
            }

            delete geom;*/

            hitswitch = true;
            if (hits->dcalad() > 23) { 
              corrupt = true; 
              _hCorruptLog->Fill(5);
            }

          }

          // trgs
          // trg dcalad default is 31
          if (hits->trg() && hits->dcalad() != 31) {
            corrupt = true; // trg pack wrong dcalad
            _hCorruptLog->Fill(6);
          }

          if (hits->trg()) {
            trgPackCount++;
            trgswitch = true;
              if (firsteventtrg) { // event starts with this as true
                firsttrgts = hits->timestamp();
                firsteventtrg = false;
              }
              // calculates ts difference between trg ts only
              // this event is set as bad if trg ts go out of range
              // uses same range as hit packs
              if (fabs(calc->dt( firsttrgts , hits->timestamp()) ) > 0) {

                _hCorruptLog->Fill(8);
              }
            //} // oldway, now dont care only if corrupt data
          }

          // check hit data for consistency
          // only if data not corrupt 
          // AND only if trg ts not out of sync
          if (!hits->trg()) { // check hit data for consistency
            trgswitch = false;

            if (firsteventhit) {
              firsthitts = hits->timestamp();
              firsteventhit = false;
            }



            // check timestamp range
            // based on first trg timestamp, if error from trg ts change this
            if ( (fabs(calc->dt( firsttrgts, hits->timestamp()+18 ))) 
                                 > _timestampRange) {

             
              hitTSrange = false;
              }
          }

          if (!hits->trg()  && hitTSrange) { //accept hits 

          } // done filling event with hit data
 
          } // end condition data contains no checksum

          else { // checksum error 
            corrupt = true;
            _hCorruptLog->Fill(0);
          }

        } // finished going through data packages in this dcol
      } // end conditions if dcol has data
    } // finished going through dcols

    if (hitOutOfTSRangeCount > 0) {
    }


      _hNTrgPacks->Fill(trgPackCount); 
      if (trgPackCount != _nDcon) { // missing trg packs
            _hCorruptLog->Fill(7);
      }


    delete calc;

/*------------------ABOVE------------------------------------
------------ DHCAL ----------- DATA --------------------------
------------------------------------------------------------*/

    break;
    }  // end case

    }; //end switch

} //end void event (RcdRecord &r)



void EbDhcErrorLog::writeRoot() {
 
  _rfile->Write();

}

#endif // CALICE_DAQ_ICC
#endif // EbDhcErrorLog_HH
