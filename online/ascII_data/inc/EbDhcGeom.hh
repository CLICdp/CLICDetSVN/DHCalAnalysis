//
// $Id: EbDhcGeom.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcGeom_HH
#define EbDhcGeom_HH

#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <math.h>

#include "EbDhcDTCalculator.hh"
#include "DhcFeHitData.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"

//declare class
class EbDhcGeom {

//declare objects and data in public
public:

//examples
/*
  static double cmax;
  EbDhcGeom();
  unsigned vmead() const; 
*/
  EbDhcGeom();
//  ~EbDhcGeom(){};

  void init(DhcFeHitData* hitP, unsigned crateNumber);
  void removeHitPack(unsigned kPack);
  unsigned layerNumber();
  //unsigned posX(unsigned dcolad, unsigned tempX); // anl cassette test stand way
  //unsigned hitX();
  unsigned posY(unsigned dconad, unsigned posX, unsigned tempY);

  std::vector<DhcFeHitData*> hitPacks();
  std::vector<unsigned> hitXvec();

private:

//example
//  UtlPack _data[4];

  std::vector<unsigned> _hitXvec;
  int _hitTime;
  std::vector<DhcFeHitData*> _hitPacks;

};  // end of class

#ifdef CALICE_DAQ_ICC

// define methods, data, strings, pairs
// see DhcFeHitData.hh for examples

//example
/*
EbDhcGeom::EbDhcGeom() {
}
unsigned EbDhcGeom::vmead() const {
  return _data[0].bits(29,30);
}
*/

EbDhcGeom::EbDhcGeom() {
  _hitTime = -1;
                    //  bit set at the same position means repaire

/*
vectors are filled with add_____Pack method
   _triggerPacks;
   _hitPacks;
*/
}

void EbDhcGeom::init(DhcFeHitData* hitP, unsigned crateNumber) {
    // first write vmead to the hit package
  switch (crateNumber) {
    case 220: {
      hitP->vmead(0);
    break;
    }
    case 221: {
      hitP->vmead(1);
    break;
    }
    default:{break;}
  };
    _hitPacks.push_back(hitP);
    _hitTime = -1;
}

// WARNING: NEEDS TO CHANGE FOR MULTIPLE VME USE
unsigned EbDhcGeom::layerNumber() {

  std::vector<DhcFeHitData*>::iterator fehdi;

  unsigned layer = 100;
  for (fehdi = _hitPacks.begin(); fehdi != _hitPacks.end(); fehdi++) {
  DhcFeHitData* ahpkg = *fehdi;

  unsigned dcolad = ahpkg->dcolad();
  unsigned dconad = ahpkg->dconad();

  // cubic meter
  // dcolad 0-9 == slot 4-13
  if (dcolad < 10) {
  layer = 37-((dcolad*4) + (dconad/3));
    if (ahpkg->vmead()==0) { // for tcmt rpcs using m^3 dcols
      if (dcolad == 9) {
        if (dconad == 8) layer = 40;
        if (dconad > 8) layer = 41;
      }
    }
  }
  else { // rpc-tcmt starts in dcolad=10
    //layer = 38 + (dconad/3);
    layer = 38 + ((dcolad-10)*4) + (dconad/3);
  }
  }

  //std::cout<<std::endl<<" layer: "<<layer;
  return layer;


}

// returns x position
// WARNING: THIS NEEDS TO CHANGE FOR MULTIPLE VME USE
//unsigned EbDhcGeom::hitX(unsigned dcolad, unsigned tempX) { // anl cassette test stand way
//unsigned EbDhcGeom::hitX() {
std::vector<unsigned> EbDhcGeom::hitXvec() {
  // size of left is number of dcols with left sided FEBs
  unsigned x=0;

    const unsigned MAX_CHAN = 64;
    unsigned g[8][8] = {
            {44,40,36,32,31,27,23,19},
            {45,41,37,33,30,26,22,18},
            {46,42,38,34,29,25,21,17},
            {47,43,39,35,28,24,20,16},
            {48,52,56,60,3,7,11,15},
            {49,53,57,61,2,6,10,14},
            {50,54,58,62,1,5,9,13},
            {51,55,59,63,0,4,8,12},
            };
      unsigned posY, posX, posZ;
      unsigned tempY, tempX;
      unsigned dcalX, dcalY;

  std::vector<DhcFeHitData*>::iterator fehdi;

  for (fehdi = _hitPacks.begin(); fehdi != _hitPacks.end(); fehdi++) {
      DhcFeHitData* ahpkg = *fehdi;

      dcalX = (ahpkg->dcalad())%6;
      dcalY = (ahpkg->dcalad())/6;

     UtlPack hitsHi(ahpkg->hitsHi());
      UtlPack hitsLo(ahpkg->hitsLo());

      //std::cout<<std::endl<<"RHS condition";
      for (unsigned c(0); c<MAX_CHAN; c++) {
          double hit = c<32 ?
            (hitsLo.bit(c%32) ? 1.0 : 0.0) :
            (hitsHi.bit(c%32) ? 1.0 : 0.0);
          if (hit > 0) { // now find its position
          for (unsigned x = 0; x < 8; x++) {
             for (unsigned y = 0; y < 8; y++) {
                if (c == g[x][y]) { // found a hit!
                   tempX = dcalX*8 + x; // position from IC address 01
                   tempY = dcalY*8 + y; // position from IC address 01

                   // determines x,y for cubic meter
                   // currently, as of 9-17-2010, for ANL temp cubic meter
                   // update for test beam wiring scheme
                   // for test beam posX needs crate# (ie vmead)
                   //posX = this->hitX(ahpkg->dcolad(),tempX); // cassete test stand way

  switch (ahpkg->vmead()) {
    case 0: { // left side crate 220
      posX = (95-tempX);
      break;
    }
    case 1: { // right side crate 221
      posX = tempX;
      break;
    }
    default: {break;}
  };
  _hitXvec.push_back(posX);


                   //posY = this->posY(ahpkg->dconad(),posX,tempY);
                   // end of cubic meter configuration

                  }
             }
          } //end for x,y
          } // end hit cond
      } // end channels(64) loop (i.e. found x,y)
    //delete hits in vector
    //delete ahpkg;
  }


  return _hitXvec;
  //return posX;
}

// returns y position
// for cubic meter
// large of 3 is bottom chamber
// 0,3,6,9 top chamber
// 1,4,7,10 middle chamber
// 2,5,8,11 bottom chamber
unsigned EbDhcGeom::posY(unsigned dconad, unsigned posX, unsigned tempY) {
  unsigned y = 0;
  // lists to associate dcol channel numbers to FEB position in a layer
  unsigned top[4] = {0,3,6,9};
  unsigned middle[4] = {1,4,7,10};
  unsigned bottom[4] = {2,5,8,11};
  for (unsigned i = 0; i < 4; i++) {
    if (dconad == top[i]) {
      // right top
      if (posX < 48) y = (95 - tempY);
      // left top
      else y = (tempY + 64);
     break;
    }
    if (dconad == middle[i]) {
      // right middle
      if (posX < 48) y = (63 - tempY);
      // left middle
      else y = (tempY + 32);
     break;
    }
    if (dconad == bottom[i]) {
      // right bottom
      if (posX < 48) y = (31 - tempY);
      // left bottom
      else y = (tempY);
     break;
    }
  }
  return y;
}

void EbDhcGeom::removeHitPack(unsigned kPack) {
}

std::vector<DhcFeHitData*> EbDhcGeom::hitPacks() {
  return _hitPacks;
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcGeom_HH
