//
// $Id: EbDhcEvent.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcEvent_HH
#define EbDhcEvent_HH

#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <math.h>

#include "EbDhcDTCalculator.hh"
#include "DhcFeHitData.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"

//declare class
class EbDhcEvent {

//declare objects and data in public
public:

//examples
/*
  static double cmax;
  EbDhcEvent();
  unsigned vmead() const; 
*/
  EbDhcEvent();
//  ~EbDhcEvent(){};

  bool status();
  void write(std::ofstream &aout);
  void clean();
  void order();
  void addHitPack(DhcFeHitData* hitP);
  void removeHitPack(unsigned kPack);
  unsigned layerNumber(DhcFeHitData* &data);
  //unsigned posX(unsigned dcolad, unsigned tempX); // anl cassette test stand way
  unsigned posX(unsigned vmead, unsigned tempX);
  unsigned posY(unsigned dconad, unsigned posX, unsigned tempY);

  std::vector<DhcFeHitData*> hitPacks();

private:

//example
//  UtlPack _data[4];

  int _hitTime;
  std::vector<DhcFeHitData*> _hitPacks;

};  // end of class

#ifdef CALICE_DAQ_ICC

// define methods, data, strings, pairs
// see DhcFeHitData.hh for examples

//example
/*
EbDhcEvent::EbDhcEvent() {
}
unsigned EbDhcEvent::vmead() const {
  return _data[0].bits(29,30);
}
*/

EbDhcEvent::EbDhcEvent() {
  _hitTime = -1;
                    //  bit set at the same position means repaire

/*
vectors are filled with add_____Pack method
   _triggerPacks;
   _hitPacks;
*/
}

void EbDhcEvent::addHitPack(DhcFeHitData* hitP) {
    _hitPacks.push_back(hitP);
    _hitTime = -1;
}
 
bool EbDhcEvent::status() { //returns true if event is a cosmic
			    // cosmic cuts:
			    // 1. at least 3 layers
                            // 2. total TS span less than 5
  bool stat = false;
  unsigned timestampWidth = 4;
  unsigned layerNumberCut = 2;
  if (_hitPacks.size() > layerNumberCut) {

  // check number of hit layers
  unsigned* layerlist = new unsigned[40];
  for (unsigned i = 0; i < 40; i++) {
    layerlist[i] =  0;
  }

  std::vector<DhcFeHitData*>::iterator hitit;
  for (hitit=_hitPacks.begin();hitit!=_hitPacks.end();hitit++) {
    unsigned layer = this->layerNumber(*hitit);
    layerlist[layer]++;
  }
  unsigned layercount = 0;
  for (unsigned j = 0; j< 40; j++) {
    if (layerlist[j] > 0) layercount++;
    if (layercount > layerNumberCut) {
      stat = true;
      break;
    }
  }
  delete[] layerlist;
    //std::cout<<std::endl<<"end of a while loop ";
/* 
  if (count > layerNumberCut) {
    //std::cout<<std::endl<<"count " << count;
    stat = true;
    //std::cout<<std::endl<<"status set to true by layernumber" ;
  }
*/
 
  // check ts width
  if (stat) {
    //std::cout<<std::endl<<"status is true" ;
    //std::cout<<std::endl<< " pack size "<<_hitPacks.size();
    unsigned tsA, tsB;
    tsA = (*_hitPacks.begin())->timestamp();
    //std::cout<<std::endl<<"tsA "<<tsA;
    tsB = (*(_hitPacks.begin()+(_hitPacks.size()-1)))->timestamp();
    //std::cout<<std::endl<<"tsB "<<tsB;

    EbDhcDTCalculator* calc = new EbDhcDTCalculator();

    //std::cout<<std::endl<<"diff "<< fabs(calc->dt( tsA , tsB ));

    if (fabs(calc->dt( tsA , tsB )) < timestampWidth) {
      stat = true;
    //std::cout<<std::endl<<"status is true for ts width";
    }
    else {
      stat = false;
    }
/*
    if ( fabs( ((*_hitPacks.begin())->timestamp()) - 
		 ((*_hitPacks.end())->timestamp()) ) < timestampWidth ) {
      stat = true;
    std::cout<<std::endl<<"status is true for ts width";
    std::cout<<std::endl<<"status is true for ts width";
    }
*/

    delete calc;
  }
/*
  if (stat) {
    std::cout<<std::endl<<"status is true";
    std::cout<<std::endl<<"status is true";
    std::cout<<std::endl<<"status is true";
    std::cout<<std::endl<<"status is true";
  }
*/
  }
  return stat;
}

// WARNING: NEEDS TO CHANGE FOR MULTIPLE VME USE
unsigned EbDhcEvent::layerNumber(DhcFeHitData* &data) {

  unsigned dcolad = data->dcolad();
  unsigned dconad = data->dconad();
  unsigned layer = 100;

  // cubic meter
  // dcolad 0-9 == slot 4-13
  if (dcolad < 10) {
  layer = 37-((dcolad*4) + (dconad/3));
    if (data->vmead()==0) { // for tcmt rpcs using m^3 dcols
      if (dcolad == 9) {
        if (dconad == 8) layer = 40;
        if (dconad > 8) layer = 41;
      }
    }
  }
  else { // rpc-tcmt starts in dcolad=10
    //layer = 38 + (dconad/3);
    layer = 38 + ((dcolad-10)*4) + (dconad/3);
  }


  return layer;


}

// returns x position
// WARNING: THIS NEEDS TO CHANGE FOR MULTIPLE VME USE
//unsigned EbDhcEvent::posX(unsigned dcolad, unsigned tempX) { // anl cassette test stand way
unsigned EbDhcEvent::posX(unsigned vmead, unsigned tempX) {
  // size of left is number of dcols with left sided FEBs
  unsigned x=0;

/*
  // lists to associate dcols with side
  unsigned left[4] = {0,1,2,3};
  unsigned right[4] = {4,5,6,7};
  // loop 4 times in order to identify which side the FEB is on
  for (unsigned i = 0; i < 4; i++) {
    if (dcolad == left[i]){
      x = (95-tempX);
      break;
    }
    if (dcolad == right[i]) {
      x = tempX;
      break;
    }
  }
*/

  // USE THIS FOR MULTIPLE VME
  // CHANGE ARGUMENT FOR THIS METHOD
  /*
  // check which vme is connected to left
  if (vmead == 0) x = (95-tempX); // left
  else x = tempX; // right
  */
  switch (vmead) {
    case 0: { // left side crate 220
      x = (95-tempX);
      break;
    }
    case 1: { // right side crate 221
      x = tempX;
      break;
    }
    default: {break;}
  };

  return x;
}

// returns y position
// for cubic meter
// large of 3 is bottom chamber
// 0,3,6,9 top chamber
// 1,4,7,10 middle chamber
// 2,5,8,11 bottom chamber
unsigned EbDhcEvent::posY(unsigned dconad, unsigned posX, unsigned tempY) {
  unsigned y = 0;
  // lists to associate dcol channel numbers to FEB position in a layer
  unsigned top[4] = {0,3,6,9};
  unsigned middle[4] = {1,4,7,10};
  unsigned bottom[4] = {2,5,8,11};
  for (unsigned i = 0; i < 4; i++) {
    if (dconad == top[i]) {
      // right top
      if (posX < 48) y = (95 - tempY);
      // left top
      else y = (tempY + 64);
     break;
    }
    if (dconad == middle[i]) {
      // right middle
      if (posX < 48) y = (63 - tempY);
      // left middle
      else y = (tempY + 32);
     break;
    }
    if (dconad == bottom[i]) {
      // right bottom
      if (posX < 48) y = (31 - tempY);
      // left bottom
      else y = (tempY);
     break;
    }
  }
  return y;
}

void EbDhcEvent::order() {

  bool swapping;
  EbDhcDTCalculator* calc = new EbDhcDTCalculator();

  while (1) {
    swapping = false;
    std::vector<DhcFeHitData*>::iterator fehdi;
    for (fehdi = _hitPacks.begin(); fehdi != (_hitPacks.end()-1); fehdi++) {
      if (calc->dt( (*fehdi)->timestamp() , (*(fehdi+1))->timestamp() ) < 0) {
        iter_swap( fehdi, fehdi+1);
        swapping = true;
      }
    }
    if (!swapping) break;
  }

}

void EbDhcEvent::clean() {
  std::vector<DhcFeHitData*>::iterator fehdi;

  for (fehdi = _hitPacks.begin(); fehdi != _hitPacks.end(); fehdi++) {
    delete *fehdi;
  }
  _hitPacks.clear();
}

void EbDhcEvent::write(std::ofstream &aout) {
    
    //this->order(); // old way

    //std::cout<<std::endl<<"ready to write";
    //std::cout<<std::endl<<"ready to write";

    const unsigned MAX_CHAN = 64;
    unsigned g[8][8] = {
            {44,40,36,32,31,27,23,19},
            {45,41,37,33,30,26,22,18},
            {46,42,38,34,29,25,21,17},
            {47,43,39,35,28,24,20,16},
            {48,52,56,60,3,7,11,15},
            {49,53,57,61,2,6,10,14},
            {50,54,58,62,1,5,9,13},
            {51,55,59,63,0,4,8,12},
            };
      unsigned posY, posX, posZ;
      unsigned tempY, tempX;
      unsigned dcalX, dcalY;

  std::vector<DhcFeHitData*>::iterator fehdi;

  // aout<<std::dec<< (*_hitPacks.begin())->timestamp() <<" -1 "<<" -1 "<<" -1 "<<std::endl; //oldway

  for (fehdi = _hitPacks.begin(); fehdi != _hitPacks.end(); fehdi++) {
      DhcFeHitData* ahpkg = *fehdi;

      dcalX = (ahpkg->dcalad())%6;
      dcalY = (ahpkg->dcalad())/6;
      posZ = this->layerNumber(ahpkg);
      //std::cout<<std::endl<<"posZ "<<posZ;
      //std::cout<<std::endl<<"posZ "<<posZ;
      UtlPack hitsHi(ahpkg->hitsHi());
      UtlPack hitsLo(ahpkg->hitsLo());

      //std::cout<<std::endl<<"RHS condition";
      for (unsigned c(0); c<MAX_CHAN; c++) {
          double hit = c<32 ?
            (hitsLo.bit(c%32) ? 1.0 : 0.0) :
            (hitsHi.bit(c%32) ? 1.0 : 0.0);
          if (hit > 0) { // now find its position
          for (unsigned x = 0; x < 8; x++) {
             for (unsigned y = 0; y < 8; y++) {
                if (c == g[x][y]) { // found a hit! 
                   tempX = dcalX*8 + x; // position from IC address 01
                   tempY = dcalY*8 + y; // position from IC address 01
                   
                   // determines x,y for cubic meter
                   // currently, as of 9-17-2010, for ANL temp cubic meter
                   // update for test beam wiring scheme
                   // for test beam posX needs crate# (ie vmead)
                   //posX = this->posX(ahpkg->dcolad(),tempX); // cassete test stand way
                   posX = this->posX(ahpkg->vmead(),tempX);
                   posY = this->posY(ahpkg->dconad(),posX,tempY);
                   // end of cubic meter configuration

                   // for CRTS only!!!
/*
                   // can put this part into two methods, ie. posX(),posY()
                   if (ahpkg->vmead() == 0 ) { // dcon RHS
                   posX = tempX; // flip since feb is flipped
                                                // tempX*2 is since there are 2 feb
                   //posY = 31-tempY; // flip since feb is flipped
                   posY = tempY; // flip since feb is flipped
                   // for CRTS only 1 set of feb in y
                   // (for test beam there are 3 sets of feb in y
                   }
                   else { // LHS
                   //if (this->layerNumber(ahpkg) == 4) posZ = 5;
                   //if (this->layerNumber(ahpkg) == 5) posZ = 4;
                   posX = 95-tempX;
                   posY = 31-tempY;
                   }
                   // end of CRTS configuration
*/
/*
                   std::cout<<std::endl<<"timestamp "<<ahpkg->timestamp();
                   std::cout<<std::endl<<"posX "<<posX;
                   std::cout<<std::endl<<"posY "<<posY;
                   std::cout<<std::endl<<"posZ "<<posZ;
*/
                   // write hit info to ascii
                   aout<<std::dec<<ahpkg->timestamp()<<" "
                     <<posX<<" "<<posY<<" "<<posZ<<std::endl;
                }
             }
          } //end for x,y
          } // end hit cond
      } // end channels(64) loop (i.e. found x,y)
    //delete hits in vector
    //delete ahpkg;
  }

}

void EbDhcEvent::removeHitPack(unsigned kPack) {
}

std::vector<DhcFeHitData*> EbDhcEvent::hitPacks() {
  return _hitPacks;
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcEvent_HH
