//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalPcbPad_HH
#define EcalPcbPad_HH

#include <iostream>
#include <fstream>
#include <string>

//////////////////////////////////////////////////////////////////////////////

/// Class to hold ECAL hardware channel id.
/** ECAL is build of in total 60 pcbs, 30 of which are of type
    "FULL" (i.e. have 6 wafers) and 30 of type "HALF" (i.e. have 3 wafers).
    Each "FULL" type is read by 12 chips of 18 channels per chip.
    Each "HALF" type is read by 6 chips of 18 channels per chip.
    So in terms of hardware each channel is uniquely identified by 4 indices,
    pcb, pcbtype, chip, channel.
*/
class EcalPcbPad 
{

   public:
   
//............................................................................
   /// Constructor.
   /**
   */
   EcalPcbPad() : _value(0xffff) 
   {
   }
//............................................................................
   /// Constructor, see operator++.
   /**
   */
   EcalPcbPad(unsigned short v) : _value(v) 
   {  
      assert(isValid());
   }
//............................................................................
   /// Constructor with attributes.
   /**
   */
   EcalPcbPad(unsigned pcb, unsigned pcbtype, unsigned chip, unsigned channel) 
   : _value(18*(12*(2*pcb+pcbtype)+chip)+channel) 
   {
      assert(pcb<40 && pcbtype<2 && chip<12 && channel<18);
   }
//............................................................................
   /// Destructor.
   /**
   */
   ~EcalPcbPad()
   {
   }   
//............................................................................
   /// Return packed id.
   /**
   */
   unsigned short value() const 
   {
      return _value;
   }
//............................................................................
/*
   void value(unsigned short v) 
   {
      _value=v;
      assert(this->isValid());
   }
*/
//............................................................................
   /// Return pcb index.
   /**
   */
   unsigned pcb() const 
   {
      return _value/(2*12*18);
   }
//............................................................................
   /// Return pcbtype index.
   /**
   */
   unsigned pcbtype() const 
   {
      return (_value/(12*18))%2;
   }
//............................................................................
   /// Return chip index.
   /**
   */
   unsigned chip() const 
   {
      return (_value/18)%12;
   }
//............................................................................
   /// Return channel index.
   /**
   */
   unsigned channel() const 
   {
      return _value%18;
   }
//............................................................................
   /// Print out channel id.
   /**
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalPcbPad::print()" << std::endl;
      out << " Pcb    = " << pcb() << std::endl;
      out << " PcbType= " << std::setw(2) << pcbtype() << std::endl;
      out << " Chip   = " << std::setw(2) << chip() << std::endl;
      out << " Channel= " << std::setw(2) << channel() << std::endl;
      return out;
   }
//............................................................................
   /// Check if channel has id within valid range.
   /**
   */
   bool isValid()
   {
      bool flag = false;
      //if(_value >= _begin && _value < _end) flag = true;
      if(_value < _end) flag = true;
      return flag; 
   }
//............................................................................
   /// Convenient increment operator. 
   /** To be used along with isValid() in for-loops to access all channels.
       I.e. "for(EcalPcbPad pp(0);pp.isValid();pp++)". 
   */
   void operator ++ (int)
   {
      _value++;
   } 
//............................................................................

   private:
	
   //static const unsigned short _begin=0;
   
   /// ECAL hardware range.
   /**
   */                                     
   static const unsigned short _end=40*2*12*18;//Pcb PcbType Chip Channel
   
   /// Packed channel id.
   /**
   */     
   unsigned short _value;

};//end class EcalPcbPad

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
