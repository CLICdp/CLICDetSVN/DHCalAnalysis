//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalCalibration_HH
#define EcalCalibration_HH

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "EcalPhysicalPad.hh"
#include "EcalMap.hh"

using namespace std;

//////////////////////////////////////////////////////////////////////////////

/// Class to handle the calibration of the ECAL channels.
/**
*/
class EcalCalibration 
{

   public:

//............................................................................   
   /// Constructor, an EcalMap is required.
   /**
   */
   EcalCalibration(const EcalMap &m) : _map(m)
   {
      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
	 _pedestal[pp.value()]=2.5-1.0-14.5;
	 _pedestalRMS[pp.value()]=0.;
	 
	 _linear[pp.value()]=0.004;
	 _quadratic[pp.value()]=0.0;
	 _threshold[pp.value()]=(short int)(_pedestal[pp.value()]+15.0);
	 
	 
	 _mip[pp.value()] = -999;
	 _gain[pp.value()] = -999;
	 
      }
   }
//............................................................................   
   /// Destructor.
   /**
   */
   virtual ~EcalCalibration()
   {
   }
//............................................................................
   /// Return the pedestal of given channel. 
   /**
   */
   double pedestal(EcalPhysicalPad p) const 
   {
      return _pedestal[p.value()];
   }
//............................................................................
   /// Set the pedestal of given channel.
   /**
   */
   void pedestal(EcalPhysicalPad p, double v) 
   {
      _pedestal[p.value()]=v;
   }
//............................................................................
   double pedestalRMS(EcalPhysicalPad p) const 
   {
      return _pedestalRMS[p.value()];
   }
//............................................................................
   void pedestalRMS(EcalPhysicalPad p, double v) 
   {
      _pedestalRMS[p.value()]=v;
   }
//............................................................................
   /// not used
   /**
   */
   double linear(EcalPhysicalPad p) const 
   {
      return _linear[p.value()];
   }
//............................................................................
   /// not used
   /**
   */
   void linear(EcalPhysicalPad p, double v) 
   {
      _linear[p.value()]=v;
   }
//............................................................................
   /// not used
   /**
   */
   double quadratic(EcalPhysicalPad p) const 
   {
      return _quadratic[p.value()];
   }
//............................................................................
   /// not used
   /**
   */
   void quadratic(EcalPhysicalPad p, double v) 
   {
      _quadratic[p.value()]=v;
   }
//............................................................................
   /// not used
   /**
   */
   short int threshold(EcalPhysicalPad p) const 
   {
      return _threshold[p.value()];
   }
//............................................................................
   /// not used
   /**
   */
   void threshold(EcalPhysicalPad p, short int a) 
   {
      _threshold[p.value()]=a;
   }
//............................................................................
   /// not used
   /**
   */
   void threshold(EcalPhysicalPad p, double a) 
   {
      if(a<0.0) a-=1.0;
      _threshold[p.value()]=(short int)a;
   }
//............................................................................
   /// not used
   /**
   */
   bool aboveThreshold(EcalPhysicalPad p, short int a) const 
   {
      return a>_threshold[p.value()];
   }
//............................................................................
   /// Return the energy (in mip units) of given ECAL channel.
   /**
   */
   double energy(EcalPhysicalPad p, short int adc) const 
   {
      double signal(adc-_pedestal[p.value()]);
      return _gain[p.value()]*signal*(_linear[p.value()]+signal*_quadratic[p.value()]);
   }
//............................................................................
   /// not used
   /**
   */
   short int adc(EcalPhysicalPad p, double v) const 
   {
      double x(-v*_quadratic[p.value()]/_linear[p.value()]);
      double a(_pedestal[p.value()]+v*(1.0+x*(1.0+2.0*x))/_linear[p.value()]);

      if(a>32767.0) 
      {
      	 return 32767;
      }
      if(a<-32768.0) 
      {
     	 return -32768;
      }
      if(a<0.0) a-=1.0;
      return (short int)a;
   }
//............................................................................
/*
   std::ostream& print(std::ostream &o, std::string s="") const 
   {
      o << s << "EcalCalibration::print()" << std::endl;

      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
	 o << s << " X,Y,Z " << std::setw(3) << pp.horizontal()
		<< std::setw(3) << pp.vertical()
		<< std::setw(3) << pp.layer()
	    	<< " = P,L,Q "
	    	<< std::setw(12) << _pedestal[pp.value()]
	    	<< std::setw(12) << _linear[pp.value()]
	    	<< std::setw(12) << _quadratic[pp.value()]
	    	<< std::setw(8) << _threshold[pp.value()] << std::endl;
      }

      return o;
   }
*/   
//............................................................................
   /// Write out calibration constants.
   /**
   */
   bool write(std::string fileName) const 
   {
      std::cout << "EcalCalibration::write()  Opening " << fileName << std::endl;
      std::ofstream o(fileName.c_str());

      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
       	 unsigned i=pp.value();
      	 o << std::setw(4) << i << " "
			<< _pedestal[i] << " "
			<< _linear[i] << " "
			<< _quadratic[i] << " "
			<< _threshold[i] << " " << std::endl;
      }
      return true;
   }
//............................................................................
   /// Read calibration constants from dat file.
   /** File contains data in format: pcb type chip channel mip gain.
       The method computes the average mip of good channels and assigns it
       to non calibrated channels.  
   */
   bool read(std::string fileName) 
   {
      std::cout << "EcalCalibration::read() " << fileName << std::endl;
      std::ifstream input(fileName.c_str(),std::ios::in);
      if(!input) return false;

      /*
      //read the comments first, zzz strings are expected
      string comment;
      for(int i=0;i<10;i++)
      {
         input >> comment;
         cout << comment << endl;
      }
     */
     
      unsigned pcb, type, chip, chan;
      double mip, gain;
      
      double summip =0;
      int goodchannels=0;


      while(input >> pcb >> type >> chip >> chan >> mip >> gain) 
      {
      	 EcalPhysicalPad physP = _map.getPhysPad(EcalPcbPad(pcb,type,chip,chan)); 
	 
	 //DO NOT assert 
	 //assert(physP.isValid());
      	 // but instead
	 //get calibration constants only for the channels that are connected 
	 if(physP.isValid())
	 {
	    _mip[physP.value()] = mip;
      	 
	    _gain[physP.value()] = gain;
      
            if(gain>0)
	    {
	         summip += mip;
		 goodchannels++;
	    }
	 }
      }

  
      //take average
      //cout << "--------" << goodchannels << " " <<summip/goodchannels << endl; 
      
      //assign average to non calibrated channels
      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
       	 if(_gain[pp.value()] < 0)
         {
	    _mip[pp.value()]  = summip/goodchannels;
	    _gain[pp.value()] = 1./_mip[pp.value()];
	 }  
      }


      return true;
   }
//...................................................................... 
   /// Return mip value of given channel.
   /**
   */
   double mip(EcalPhysicalPad p) const 
   {
      return _mip[p.value()];
   }
//...................................................................... 
   /// Return gain (=1/mip) of given channel.
   /**
   */
   double gain(EcalPhysicalPad p) const 
   {
      return _gain[p.value()];
   }

//............................................................................
   /// Print out mip and gain of the ECAL channels.
   /** For each channel both physical position indices and
       hardware id (= pcb type chip channel) are given. 
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << "EcalCalibration::print()" << std::endl;

      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
         if(_map.readout(pp))
	 {
	    out << " " 
	    << " X,Y,Z: " 
	    << std::setw(3)  << pp.horizontal()
            << std::setw(3)  << pp.vertical()
            << std::setw(3)  << pp.layer()
	    << " mip: "   
	    << std::setw(12) << _mip[pp.value()]
	    << " gain: "  
	    << std::setw(12) << _gain[pp.value()]
	    << " = Pcb,Type,Chip,Channel "
	    << std::setw(3) << _map.getPcbPad(pp).pcb()
	    << std::setw(3) << _map.getPcbPad(pp).pcbtype()
	    << std::setw(3) << _map.getPcbPad(pp).chip()
	    << std::setw(3) << _map.getPcbPad(pp).channel() 
	    << std::endl;
	 }
      }

      return out;
   }
//............................................................................

   private:

   double _pedestal[18*18*30];
   double _pedestalRMS[18*18*30];
   
   double _linear[18*18*30];
   double _quadratic[18*18*30];
   short int _threshold[18*18*30];
   
   
   double _mip[18*18*30];
   double _gain[18*18*30];
   const EcalMap &_map;

};//end class EcalCalibration

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
