//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
// modified for quicky dhcal+ecal event building by JacobSmith
#ifndef EbEcalRawAdcExtractor_HH
#define EbEcalRawAdcExtractor_HH

#include <iostream>
#include <sstream>


#include "EcalMap.hh"
#include "EcalAdc.hh"


//for reading daq raw files
#ifdef RAWDAQ
#include "RcdRecord.hh"
#include "SubAccessor.hh"
#include "RcdHeader.hh"
#include "CrcVmeRunData.hh"
#include "CrcBeRunData.hh"
#include "CrcBeTrgRunData.hh"
#include "CrcFeRunData.hh"
#include "CrcVlinkTrgData.hh"

#endif



using namespace std ;

//////////////////////////////////////////////////////////////////////////////

/// Class to extract the ECAL adc data. 
/**
*/
class EbEcalRawAdcExtractor 
{

   public:
   
//............................................................................   
   /// Constructor, an EcalMap is required.
   /**
   */
   EbEcalRawAdcExtractor(const EcalMap &m) : _map(m) 
   {
   }
//............................................................................   
   /// Destructor.
   /**
   */
   virtual ~EbEcalRawAdcExtractor() 
   {
   }
//............................................................................      
//............................................................................      
//for reading daq raw files
#ifdef RAWDAQ

   /// Extract the ecal adc data.
   /** Check if bad adc readout occured. 
       To be used when reading raw data stored in daq format.
   */
   bool extract(RcdRecord &r, EcalAdc &a) 
   {
      if(r.recordType()!=RcdHeader::event) return true;
      
      ostringstream log;//log problems
      
      //extract event numbers 
      SubAccessor extracter(r);
      std::ostringstream daqev;
      std::vector<const DaqEvent*> w(extracter.extract<DaqEvent>());
      if(w.size()>0) w[0]->print(daqev);
      
      
      // In case of readout failure, set all ADC values initially to -32768
      a.reset();

      // Get list of data packets from each CRC board
      SubAccessor accessor(r);
      std::vector<const CrcLocationData<CrcVlinkEventData>*>
      v(accessor.extract< CrcLocationData<CrcVlinkEventData> >());
      
      // Loop over boards = VME slots
      for(unsigned i(0);i<v.size();i++) 
      {
         const unsigned slot(v[i]->slotNumber());
	 int crate(v[i]->crateNumber());

/*
//////////////////////
      	unsigned nBeTrgHistory(0);
	
	
	//temporary
	unsigned line(14), trigslot(12);

        CrcBeTrgHistoryHit window;
        window.startTime(215);
	window.endTime(260);


       
	const CrcVlinkTrgData *td(v[i]->data()->trgData());
	if(td!=0) 
	{
	    nBeTrgHistory++;
	    //v[i]->print(std::cout," ") <<std::endl;
      	    std::vector<CrcBeTrgHistoryHit> w(td->lineHistory(line));



      	    for(unsigned j(0);j<w.size();j++) 
	    {
      	       if(slot==trigslot) 
	       {if(w[j].overlap(window)) cher = true;
      	       }

      	       //w[j].print(std::cout);
	    }

      	}
	

//////////////////////////

*/


	 if(crate != 236 ) continue;



         // Loop over front ends = connectors and get pointer to data from this front end
	 unsigned int thischannel(0);
         for(unsigned fe(0);fe<8;fe++) 
	   {
	     
	     const CrcVlinkFeData *fd(v[i]->data()->feData(fe));
	     
	     if(fd==0 && _map.isConnected(slot,fe))
	       {  
		 log << "EbEcalRawAdcExtractor::extract() Problem with "
		     << "crate " << crate
		     << " slot " << slot 
		     << " fe "   << fe << endl;
		 
	       }
	     if(fd!=0 && _map.isConnected(slot,fe)) 
	       {
		 int samples(v[i]->data()->feNumberOfAdcSamples(fe));
	       if(samples < 18)
		 {
		   log << "EbEcalRawAdcExtractor::extract() Problem with "
		       << "crate " << crate
		       << " slot " << slot 
		       << " fe "   << fe 
		       << " --- time samples read, expected:"
		       << v[i]->data()->feNumberOfAdcSamples(fe) 
		       << " "<< 18 << endl;
		 }
	       
	       
	       // Loop over the 18 channels per VFE chip and get pointer to time sample
	       for(unsigned chan(0);chan<v[i]->data()->feNumberOfAdcSamples(fe) && chan<18;chan++) 
		 {
		   const CrcVlinkAdcSample *as(fd->adcSample(chan));
		   if(as==0 && _map.isConnected(slot,fe))
		     {  
		       log << "EbEcalRawAdcExtractor::extract() Problem with "
			   << " crate " << crate  
			   << " slot "  << slot 
			   << " fe "    << fe 
			   << " chan "  << chan << endl;
		     }
		   if(as!=0) 
		     {
		       // Loop over the 12 VFE chips per time sample
		       for(int chip=0;chip<12;chip++)
			 {
			   //log << "slot "  << slot 
			   //     << " fe "   << fe 
			   //     << " chip " << chip 
			   //     << " chan " << chan 
			   //     << " adc "  << as->adc(chip) 
			   //     << endl;
			   
			   // Check if this readout channel is connected to a physical pad
			   EcalReadoutPad rp(slot,fe,chip,chan);
			   
			   
		        if(_map.physical(rp) && a.adc(_map.map(rp))==-32768) 
			  {
			    
			    // Find physical pad for this readout channel and get ADC value
			    a.adc(_map.map(rp),as->adc(chip));
			  }
			 } 
		     }
		 }
	      }
	   }	
      }   
   

      bool ok(channelsRead == _map.numberOfChannels());

      //return channelsRead == _map.numberOfChannels();
      return ok;
      }
#endif   
//............................................................................   

   private:
   
   
   const EcalMap &_map;

};//class EbEcalRawAdcExtractor

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
