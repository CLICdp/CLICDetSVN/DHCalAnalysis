//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalPedestalCalculator_HH
#define EcalPedestalCalculator_HH

#include <iostream>

#include "UtlRollingAverage.hh"


#include "EcalCalibration.hh"
#include "EcalAdc.hh"
#include "EcalPhysicalPad.hh"
#include "Average.hh"


//////////////////////////////////////////////////////////////////////////////

/// Class to calculate the pedestal values of the ECAL channels.
/**
*/
class EcalPedestalCalculator 
{

   public:
   
//............................................................................   
   /// Constructor, an EcalCalibration is required. 
   /**
   */
   EcalPedestalCalculator(EcalCalibration &c) : _cal(c), _state(false) 
   {
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
	 _cal.linear(pp,1.0);
	 _cal.quadratic(pp,0.0);
      }
   }
//............................................................................
   /// Destructor.
   /**
   */
   virtual ~EcalPedestalCalculator() 
   {
   }
//............................................................................
   /// stupid flipflop status flags. To get rid of them soon.
   /**
   */
   bool isOFF()
   {  return !isON();   
   }
   /// stupid flipflop status flags. To get rid of them soon.
   /**
   */
   bool isON()
   {  return _state;
   }
   /// stupid flipflop status flags. To get rid of them soon.
   /**
   */
   void setOFF()
   {  _state = false;
   }
   /// stupid flipflop status flags. To get rid of them soon.
   /**
   */
   void setON()
   {  _state = true;
   }
//............................................................................
   /// Reset the pedestals.
   /** Reseting should be done at the first event of each 100 pedestal 
       events sequence.
   */
   void reset()
   {
      
      //with UtlRollingAverage
      /*
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
	 _average[pp.value()].reset();
      }
      */
      
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
      	 _average[pp.value()].reset();
      }
   }
//............................................................................
   /// Calculate the pedestals.
   /** It always passes the calculated pedestals to calibration.
   */
   bool calculate(const EcalAdc &a) 
   {
      /*
      //this causes problem when there is bad readout 
      //and the buffer does not contain 100 events and 
      //so the calibration is not updated 
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
	 if(a.adc(pp)>-32768) 
	 {
	    _average[pp.value()]+=a.adc(pp);
	    
	    if(_average[pp.value()].totalNumber()<100) 
	    {
	       _cal.pedestal(pp,32768.0);
	    } 
	    else 
	    {
	       _cal.pedestal(pp,_average[pp.value()].average());
	       _average[pp.value()].reset();
	    }
	 }
      }
      */
      
      //use this and reset the pedestals externally
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
	 if(a.adc(pp)>-32768) 
	 {
	    _average[pp.value()]+=a.adc(pp);
	    
	    //always pass the calculated pedestals to calibration
	    _cal.pedestal(pp,_average[pp.value()].average());
	    _cal.pedestalRMS(pp,_average[pp.value()].sigma());
	    
	 }
      }
      
      
      
      return true;
   }
//............................................................................

   private:

   EcalCalibration &_cal;
   bool _state;
   //UtlRollingAverage _average[18*18*30];
   Average _average[18*18*30];
};

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
