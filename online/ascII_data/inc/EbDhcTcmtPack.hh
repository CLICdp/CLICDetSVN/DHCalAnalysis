//
// $Id: EbDhcTcmtPack.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcTcmtPack_HH
#define EbDhcTcmtPack_HH

#include <iostream>

class EbDhcTcmtPack {

public:

  EbDhcTcmtPack(int tl);
  ~EbDhcTcmtPack(){};

  void energy(double te);
  double energy();

  void strip(int ts);
  int xstrip();
  int ystrip();

  void layer(int tl);
  int layer();


private:

  double _energy;
  int _xstrip; 
  int _ystrip;
  int _layer;

};  // end of class

#ifdef CALICE_DAQ_ICC


EbDhcTcmtPack::EbDhcTcmtPack(int tl) {
  _layer = tl;
}

void EbDhcTcmtPack::layer(int tl) {
  _layer = tl;
}
int EbDhcTcmtPack::layer() {
  return _layer;
}

void EbDhcTcmtPack::energy(double te) {
  //if (_layer > 8) _energy = -1*4.8*te; // condition based on first layer =1
  if (_layer > 7) _energy = -1*4.8*te; // based on first layer = 0
  else _energy = -1*te;
}
double EbDhcTcmtPack::energy() {
  return _energy;
}

void EbDhcTcmtPack::strip(int ts) {
  if (_layer%2==1) {
    _xstrip = ts;
    _ystrip = -1;
  }
  else {
    _ystrip = ts;
    _xstrip = -1;
  }
}
int EbDhcTcmtPack::xstrip() {
  return _xstrip;
}
int EbDhcTcmtPack::ystrip() {
  return _ystrip;
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcTcmtPack_HH
