#!/usr/local/bin/perl -w

#@INC=(/usr/local/lib/perl5/5.8.8)
#use strict;
use English;
use Tk;


#global variables

#Commands to start
my $path="/home/caliceon/online/";
my $path1="/online/";
my $RunTypes=$path."knownRunTypes.txt";
my $RunMonitor=$path."bin/runMonitor -v";
my $startUp=$path."daquser/bin/startUp";
my $shutDown=$path."bin/shutDown";
my $paniclocal="./daquser/bin/panic";
my $panicremote='./daquser/bin/panic';
my $runStart=$path."bin/runStart";
my $runEnd=$path."bin/runEnd";
my $sequenceEnd=$path."bin/sequenceEnd";
my $currentRun=$path."bin/currentRun";


#Remote Communication

#trigger Socket
my $triggercomputer='icalice02';
my $trigger_start_command='daquser/bin/startSkt';

#ecal Socket
my $ecalcomputer='icalice01';
my $ecal_start_command='daquser/bin/startSkt';



# the run command to start
my $CommandLine;

#the logfile to tail
my $LogFile;

#have we started up ?
my $IsRunning=0;

#control sequence
my $control_running="ps -au caliceon|grep runner";

#list of simple run, basic mode 
my @RunTypeSimple=("crcNoise","emcNoise","ahcCmNoise",
		   "ahcPmNoise","ahcCmLedVcalibScan","ahcPmLedVcalibScan",
		   "ahcGain","beamData");

#currently displayed list
my @run=@RunTypeSimple;

#to kill clean
my $Pid_RunMonitor;
my $Pid_Tail;
my $Pid_Panic;
my $Pid_currentRun;

# option field value
my $options;

# trigger field value
my $triggers;

#run field value
my $runs_number;

#event field value
my $events_number;

#Write switch
my $Write;

#no histogramme
my $histo;

#Expert mode switch
my $Expert;

#sockets status
my $trigger=0;
my $ecal=0;
my $hcal=0;


#
#Beginning of geometry drawing
#
#

#Main Window
my $Window=MainWindow->new(-height=>600,-width=>600);

#Window Geometry Disposition
my $LeftFrame=$Window->Frame(-relief=>"raised")->pack(-side=>"left",-expand=>1,-fill=>"both");
my $RightFrame=$Window->Frame(-relief=>"raised")->pack(-side=>"right",-expand=>1,-fill=>"both");
#my $LowerFrame=$Window->Frame(-relief=>"raised")->pack(-side=>"bottom",-expand=>1,-pady=>10,-fill=>"both");

#Content of left Frame from top top bottom
my $StartFrame=$LeftFrame->Frame(-relief=>"raised")->pack(-side=>"top",-fill=>"x");
my $RunFrame=$LeftFrame->Frame(-relief=>"ridge")->pack(-side=>"top",-expand=>1,-fill=>"both");
my $ShutDownFrame=$LeftFrame->Frame(-relief=>"raised")->pack(-side=>"bottom",-fill=>"x");



#Content of lower Frame 
#my $StatusFrame=$LowerFrame->Frame(-relief=>"ridge")->pack(-side=>"bottom",-expand=>1,-fill=>"both");



#StartUp Frame, 1 button && 1 text
my $StartUpButton=$StartFrame->Button(-text=>"DAQ StartUp",-command=>sub{startup_prepare()},-activebackground=>"#4682b4")->pack(-side=>"left");
my $StartUpText=$StartFrame->Label(-text=>"Load SiPM bias Settings.\nLow Voltage MUST be on.")->pack(-side=>"right");


#Shutdown Frame, 1 button && 1status text
my $ShutDownButton=$ShutDownFrame->Button(-text=>"DAQ ShutDown",-command=>sub{shutdown_prepare()},-activebackground=>"#4682b4")->pack(-side=>"left");
my $ShutDownText=$ShutDownFrame->Label(-text=>"   DAQ is not running   ",
					-background=>'grey',
					-foreground=>"black")
				->pack(-side=>"right");
my $PanicButton=$ShutDownFrame->Button(-text=>"PANIC",-foreground=>'red',-command=>sub{panic()},-activebackground=>"#4682b4")->pack(-side=>"bottom",-expand=>"1",-fill=>"x");


#RunControl Frame

#Header
my $RunIntro=$RunFrame->Label(-text=>"---\tRun Control\t---")->pack();

#RunType List
#list of possible runs with scroll bar
my $ListFrame=$RunFrame->Frame()->pack(-fill=>"both",-expand=>1);
my $RunTypeList=$ListFrame->Listbox(-selectmode=>"browse")->pack(-side=>"left",-fill=>"both",-expand=>1);
my $Scroll=$ListFrame->Scrollbar(-orient=>"v",-command=>[yview=>$RunTypeList]);
$RunTypeList->configure(-yscrollcommand=>['set',$Scroll]);
$Scroll->pack(-side=>"right",-fill=>"y",-expand=>1);


#
#prepare first Run list
#

foreach $type (@RunTypeSimple) {
	$RunTypeList->insert('end',$type);
}

#my $ExpertMode=$RunFrame->Checkbutton(-text=>"Expert mode RunType List", -variable=>\$Expert, -command=>\&change_run_list)->pack(-side => 'left', -fill => 'x');
my $ExpertMode=$RunFrame->Checkbutton(-text=>"Expert mode RunType List",-variable=>\$Expert,-command=>\&change_run_list);
#$ExpertMode->pack(-side => 'left', -fill=>'x',-expand=>1);
#$ExpertMode->place(-x=>0, -y=>410);
$ExpertMode->pack(-anchor => 'w');

#Spill Sync button
my $SpillSyncEnable=$RunFrame->Checkbutton(-text=>"Enable spill sync (-m)",-variable=>\$spillsync);
#$SpillSyncEnable->pack(-side => 'left', -fill=>'x',-expand=>1);
#$SpillSyncEnable->place(-x=>0, -y=>430);
$SpillSyncEnable->pack(-anchor => 'w');
$SpillSyncEnable->select();

#CERN db readout button
#my $CERNdbEnable=$RunFrame->Checkbutton(-text=>"Enable CERN db readout",-variable=>\$readCERNdb);
#$CERNdbEnable->pack(-anchor => 'w');
#$CERNdbEnable->select();

#Write Enable button
my $WriteEnable=$RunFrame->Checkbutton(-text=>"Save data (+w)",-variable=>\$Write);
#$WriteEnable->pack(-side => 'left', -fill=>'x',-expand=>1);
#$WriteEnable->place(-x=>0, -y=>450);
$WriteEnable->pack(-anchor => 'w');
$WriteEnable->select();

#No histogram
my $HistoEnable=$RunFrame->Checkbutton(-text=>"Enable histogram output",-variable=>\$histo);
#$HistoEnable->pack(-side => 'left', -fill=>'x',-expand=>1);
#$HistoEnable->place(-x=>0, -y=>470);
$HistoEnable->pack(-anchor => 'w');
$HistoEnable->deselect();

#split run
my $SplitRunEnable=$RunFrame->Checkbutton(-text=>"Split beamRun in 250K events",-variable=>\$splitrun);
$SplitRunEnable->pack(-anchor => 'w');
$SplitRunEnable->deselect();


#
#Run Options
#
#my $OptionFrame=$RunFrame->Frame()->pack(-side=>"left", -fill=>'x',-expand=>1);
my $OptionFrame=$RunFrame->Frame()->pack(-anchor => 'w');


#Data taking in a certain amount of runs Field
#my $RunsText=$OptionFrame->Label(-text=>"Number of Runs (-n)")->pack(-side=>"left");
#my $RunsField=$OptionFrame->Entry(-width=>6)->pack(-side=>"left", -fill => 'x');

#Events number Field
my $EventsText=$OptionFrame->Label(-text=>"   Number of Events per Run(-e)")->pack(-side=>"left", -fill => 'x');
my $EventsField=$OptionFrame->Entry(-width=>6)->pack(-side=>"left", -fill => 'x');

#Option field
my $OptionText=$OptionFrame->Label(-text=>"   Options (-v)")->pack(-side=>"left", -fill => 'x');
my $OptionField=$OptionFrame->Entry(-width=>3)->pack(-side=>"left", -fill => 'x');

my $TriggerText=$OptionFrame->Label(-text=>"Trigger")->pack(-side=>"left", -fill => 'x');
my $TriggerField = $OptionFrame->Frame(-borderwidth => 3)->pack();
#$TriggerField->place(-x=>0 ,-y=>200 );
my @elemente = qw(osci 3x3 10x10 20x20 100x100);

for my $i (0..$#elemente) {
    $TriggerField->Radiobutton(-text     => $elemente[$i],
                     -variable => \$triggers,
                     -value    => $i,
			       )
	->pack(-anchor   => 'ne');
}


#
# Start/Stop Run
#
#my $ButtonFrame=$RunFrame->Frame()->pack(-side=>"bottom",-expand=>1,-fill=>"x");
my $ButtonFrame=$RunFrame->Frame()->pack(-anchor   => 'w');

#Reload DAQ settings button
my $ResetButton=$ButtonFrame->Button(-text=>"RELOAD HCAL DAC SETTINGS",
				  -command=>sub{reload()},
				  -foreground=>"red",-activebackground=>"#4682b4");
#$ResetButton->pack(-side=> 'left');
$ResetButton->pack();

#ECAL safety run button
my $EcalSafetyButton=$ButtonFrame->Button(-text=>"START ECAL SAFETY RUN",
				  -command=>sub{ecalsafety()},
				  -foreground=>"red",-activebackground=>"#4682b4");
#$EcalSafetyButton->pack(-side=> 'left');
$EcalSafetyButton->pack();

my $StartRunButton=$ButtonFrame->Button(-text=>"Start Run",-command=>sub{run_start()},-activebackground=>"#4682b4");
#$StartRunButton->pack(-side=>"left");
$StartRunButton->pack();
my $StopRunButton=$ButtonFrame->Button(-text=>"Stop Run",-command=>sub{run_stop()},-activebackground=>"#4682b4");
#$StopRunButton->pack(-side=>"left");
$StopRunButton->pack();
#my $runstarted=$ButtonFrame->Label(-text=>"No run at the moment\n");
#$runstarted->pack(-side=>"bottom", -padx=>"0m", -pady =>"20m", -fill=>"x");
my $runstarted=$ButtonFrame->Label(-text=>"No run at the moment\n",-background=>'grey')->pack();
my $currentRunButton=$ButtonFrame->Button(-text=>"current run",-command=>sub{get_currentrun()},-activebackground=>"#4682b4");
$currentRunButton->pack();

#
#RightFrame


#RunMonitor check

my $CheckFrame=$RightFrame->Frame()->pack(-expand=>1,-fill=>"both");
my $t;

#Log Frame

my $LogFrame=$RightFrame->Frame()->pack(-side=>"bottom",-expand=>1,-fill=>"both");
my $t2;

#Run Number Frame

my $RunNumberFrame=$ButtonFrame->Label(-text=>"No run number\n",-background=>'grey')->pack();




#
#Update RunMonitor

$Pid_RunMonitor=open(H2, "$RunMonitor|") or die "Nope: $OS_ERROR";
$t2 = $CheckFrame->Text(-width => 50, -height => 12, -wrap => 'none',-background=>"white",-font=>[-size=>12]);
$t2->pack(-expand => 1);
$CheckFrame->fileevent(H2, 'readable', [\&fill_check_widget, $t2]);






MainLoop;


open(EXIT, "kill $Pid_RunMonitor |") or die "Nope: $OS_ERROR";
if(defined($Pid_Tail)){
    	open(EXIT, "kill $Pid_Tail|") or die "Nope: $OS_ERROR";
}
close(EXIT);
close(TRIGGER);
close(ECAL);
close(CURRENTRUN);
close(H);
close(H2);
close(H3);
close(RUNTYPES);
close(STARTUP);
close(SHUTDOWN);
#close(STARTRUN);
close(STOPRUN);
close(CONTROL);


#
#
#Subroutines
#button press
#
#



#StartUp button behaviour

sub startup_prepare{

	if(!$IsRunning){
	        panic(); 
		trigger_socket();
		ecal_socket();
		sleep(10);
		my $Pid_startup = open(STARTUP, "$startUp|") or die "Nope: $OS_ERROR";
		#waitpid($Pid_startup,0);
		#sleep(3);
		my $line1 = 0;
		do{
		   #print $line1;
		    $line1=<STARTUP>;	
		    #print $line1;
		}
		while($line1 !~ m{data/log/Log(\d+).out_calice\d+$});
		close(STARTUP);		
		waitpid($Pid_startup,0);
		$LogFile=$path.$&;
		print $LogFile,"\t".$&."\n";
		$ShutDownText->configure(-text=>"DAQ IS running",-background=>"green",-foreground=>"black");
	
		$Pid_Tail=open(H, "tail -f $LogFile|") or die "Nope: $OS_ERROR";
		$t = $LogFrame->Text(-width => 90, -height => 40, -wrap => 'none',-background=>'white');
		$t->pack(-expand => 1);
		$LogFrame->fileevent(H, 'readable', [\&fill_log_widget, $t]);
		$IsRunning=1;
	}
	
	
}

#Shutdown button behaviour
sub shutdown_prepare{
	if($IsRunning){
		$ShutDownText->configure(-text=>"DAQ is shutting down",-background=>"yellow",-foreground=>"black");
		#sleep(1);
		my $Pid_shutdown = open(SHUTDOWN, "$shutDown|") or die "Nope: $OS_ERROR";
		#close(SHUTDOWN);
		waitpid($Pid_shutdown,0);
		my $line1=<SHUTDOWN>;
		#print $line1."\n" ;
		#sleep(2);

		my $Pid_control = open(CONTROL, "$control_running|") or die "Nope: $OS_ERROR";
	
	#	waitpid($Pid_control,0);
		$line1=<CONTROL>;
		#print $line1."\n"  ;
		if(!defined($line1) or $line1!~m/runner/){
			$LogFrame->fileevent(H, 'readable'=>'');
			#$t->destroy();
			$t->packForget();
			my $Pid_kill = open(EXIT, "kill $Pid_Tail |") or die "Nope: $OS_ERROR";
			close(EXIT);
			waitpid($Pid_kill,0);
			$Pid_Tail=undef;
			$ShutDownText->configure(-text=>"DAQ is not running",
						 -background=>"grey",
						 -foreground=>"black");
			$IsRunning=0;
			return;
		}
#		close(CONTROL);
		waitpid($Pid_control,0);
		$ShutDownText->configure(-text=>"DAQ is STILL  running",
						 -background=>"red",
						 -foreground=>"white");

	
		my $Pid_Kill = open(KILL, "kill $Pid_RunMonitor |") or die "Nope: $OS_ERROR";
		close(KILL);
		waitpid($Pid_Kill,0);
		if(defined($Pid_Tail)){
		    $Pid_Kill = open(KILL, "kill $Pid_Tail|") or die "Nope: $OS_ERROR";
		}
		close(KILL);
		waitpid($Pid_Kill,0);
		panic();
# commented out on request of Paul

		exit();
		return;
	}
}

# emergency case# emergency case# emergency case
sub panic{
        $ShutDownText->configure(-text=>"DAQ is shutting down",-background=>"yellow",-foreground=>"black");
	my $Pid_Panic = open(PANIC, "$paniclocal|" ) or die "Nope: $OS_ERROR";
	close(PANIC);
	waitpid($Pid_Panic,0);
	$Pid_Panic = undef;
#	sleep(5);

	my $Pid_ssh = open(TRIGGER,"ssh ".$triggercomputer." ".$panicremote ." |") or die "Nope: $OS_ERROR";
	close(TRIGGER);
	waitpid($Pid_ssh,0);
	$Pid_ssh=undef;
	
	$Pid_ssh = open(ECAL,"ssh ".$ecalcomputer." ".$panicremote ." |") or die "Nope: $OS_ERROR";
	close(ECAL);
	waitpid($Pid_ssh,0);
	
	$Pid_Panic = open(PANIC, "$paniclocal|" ) or die "Nope: $OS_ERROR";
	close(PANIC);
	#my $line1=<PANIC>;
	waitpid($Pid_Panic,0);
	#sleep(5);
	$ShutDownText->configure(-text=>"DAQ is not running",
						 -background=>"grey",
						 -foreground=>"black");
	$IsRunning=0;

	#open(EXIT, "kill $Pid_Panic |") or die "Nope: $OS_ERROR";
	#wait();
	$Pid_Panic=undef;
	
	
#	exit();
       	return;
}






#Expert mode switch behaviour
sub change_run_list{
	$RunTypeList->delete(0,'end');
	@run=();
	if($Expert){
		$Pid_runtypes = open(RUNTYPES, "<$RunTypes") or die "Nope: $OS_ERROR";
		while(defined($line1=<RUNTYPES>)){
			$line1=~ m/\s+Type\s+=\s+\d+\s+=\s+(\w+),/;
			$RunTypeList->insert('end',$1);
			push @run,$1;
		}
	}
	else{
		foreach $type (@RunTypeSimple){
			$RunTypeList->insert('end',$type);
		}
		@run=@RunTypeSimple;
	    }
	waitpid($Pid_runtypes,0);

}


#RunStart button behaviour
sub run_start{
    my $Pid_open;
	if(!$IsRunning){
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
	}else{
		$CommandLine=$runStart." ";
		if(!$Write){
			$CommandLine=$CommandLine."-w ";
		}
		if(!$histo){
			$CommandLine=$CommandLine."-d ";
		}
		if($spillsync){
			$CommandLine=$CommandLine."-m 1 ";
		}
		#if(!$readCERNdb){
		#    if($options=$OptionField->get()){
			
		#	$options = $options + 128;
		#	$CommandLine=$CommandLine."-v $options ";
		#    }
		#}
		#else
		#{
		#    if($options=$OptionField->get()){
		#	$CommandLine=$CommandLine."-v $options ";
		#    }
		#}
		if($options=$OptionField->get()){
			$CommandLine=$CommandLine."-v $options ";
		}

		my ($runtype)=$RunTypeList->curselection();

		#if($splitrun)
		if($splitrun && ($run[$runtype]=~/Beam/ || $run[$runtype]=~/beam/))
		{
		    $CommandLine=$CommandLine."-n 0 -e 250000 ";
		}
                # if($runs_number=$RunsField->get()){
		#		$CommandLine=$CommandLine."-n $runs_number ";
		#}
		
		#my ($runtype)=$RunTypeList->curselection();
		
		if(!$splitrun){
		    if($events_number=$EventsField->get()){
			$CommandLine=$CommandLine."-e $events_number ";
		    }
		}
		my ($runtype)=$RunTypeList->curselection();
		if(!defined($runtype)){
			return;
		}
#		if($run[$runtype] eq "beamData" || $run[$runtype] eq "ahcBeam" || $run[$runtype] eq "emcBeam")
		if($run[$runtype]=~/Beam/ || $run[$runtype]=~/beam/)
#		if($runtype==14)
		{
		   # if($triggers=$TriggerField->getSelection()){
		    my $triggersel;
		    if($triggersel = $triggers)
		    {
			#if($readCERNdb)
			#{	
			    if ($triggersel == 1) {$CommandLine=$CommandLine."-v 28 ";}
			    if ($triggersel == 2) {$CommandLine=$CommandLine."-v 28 ";}
			    if ($triggersel == 3) {$CommandLine=$CommandLine."-v 1 ";}
			    if ($triggersel == 4) {$CommandLine=$CommandLine."-v 26 ";}
			#}
			#else
			#{
			#    if ($triggersel == 1) {$CommandLine=$CommandLine."-v 92 ";}
			#    if ($triggersel == 2) {$CommandLine=$CommandLine."-v 90 ";}
			#    if ($triggersel == 3) {$CommandLine=$CommandLine."-v 66 ";}
			#    if ($triggersel == 4) {$CommandLine=$CommandLine."-v 83 ";}
			

			#}
			}
		}

		$CommandLine=$CommandLine."-t $run[$runtype] ";
		$Pid_open = open(STARTRUN, "$CommandLine|") or die "Nope: $OS_ERROR";
		#close(STARTRUN);
		waitpid($Pid_open,0);
		$runstarted->configure(-text=>"The following run has started\n$CommandLine",-background=>'green');
 
                #$currentRunButton->after(30000);
		#$currentRunButton->repeat(30000,callback);
		
	    }
    
   # close(STARTRUN);
   # waitpid($Pid_open,0);
}

#RunStop button behaviour
sub run_stop{
    my $Pid_stoprun;
	if(!$IsRunning){
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
	}else{
		my $Pid_stoprun = open(STOPRUN, "$runEnd|") or die "Nope: $OS_ERROR";
	#	close(STOPRUN);
		waitpid($Pid_stoprun,0);
	
		$runstarted->configure(-text=>"No run at the moment\n",-background=>'gray50');
		$RunNumberFrame->configure(-text=>"klick 'current run' ",-background=>'grey');
		
	}
#    close(STOPRUN);
#    waitpid($Pid_stoprun,0);
    
}

#Reload settings button behaviour
sub reload{
    my $Pid_startrun;
	if(!$IsRunning){
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
	}else{
#my change	open(RUNTYPES, "$runStart -t ahcDacScan") or die "Nope: $OS_ERROR";
		$CommandLine=$runStart." ";
		if(!$Write){
			$CommandLine=$CommandLine."-w ";
		}
		$CommandLine=$CommandLine."-t ahcDacScan";
		$Pid_startrun = open(STARTRUN, "$CommandLine|") or die "Nope: $OS_ERROR";
		waitpid($Pid_startrun,0);
		$runstarted->configure(-text=>"The following run has started\n$CommandLine",-background=>'green');
	}

#    close(STARTRUN);
#    waitpid($Pid_startrun,0);
}

#ECAL safety button
sub ecalsafety{
    
    my $Pid_startrun;
	if(!$IsRunning){
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
		sleep(1);
		$ShutDownText->configure(-background=>"red");
		sleep(1);
		$ShutDownText->configure(-background=>"white");
	}else{
	        $CommandLine=$runStart." ";
		$CommandLine=$CommandLine."-t crcNoise -w";
		$Pid_startrun = open(STARTRUN, "$CommandLine|") or die "Nope: $OS_ERROR";
		waitpid($Pid_startrun,0);
	
		$runstarted->configure(-text=>"The following run has started\n$CommandLine",-background=>'green');
	}
#    close(STARTRUN);
   # waitpid($Pid_startrun,0);
    
}

# current run info
sub get_currentrun{
   # if($IsRunning){
	
    my $Pid_currentrun = open (CURRENTRUN, $currentRun." | grep \"Run \" |  " );
 
    $mygrep = <CURRENTRUN>;
    $RunNumberFrame->configure(-text=>"$mygrep",-background=>'grey');

    close(CURRENTRUN);
    waitpid($Pid_currentrun,0);


}



#Open Trigger socket  button


sub trigger_socket {
                my $Pid_trigger = open(TRIGGER,"ssh ".$triggercomputer." ".$panicremote ." |") or die "Nope: $OS_ERROR";
		close(TRIGGER);
		waitpid($Pid_trigger,0);
		$Pid_trigger = undef;
                #sleep(5);
		
		$Pid_trigger = open(TRIGGER,"ssh ".$triggercomputer." ".$trigger_start_command ." |") or die "Nope: $OS_ERROR";
		close(TRIGGER);
		waitpid($Pid_trigger,0);
		
		$trigger = 1;
		$hcal = 1;
}

sub ecal_socket {
                my $Pid_ecal = open(ECAL,"ssh ".$ecalcomputer." ".$panicremote ." |") or die "Nope: $OS_ERROR";
		close(ECAL);
		waitpid($Pid_ecal,0);
		$Pid_ecal = undef;
                #sleep(5);
		
		$Pid_ecal = open(ECAL,"ssh ".$ecalcomputer." ".$ecal_start_command ." |") or die "Nope: $OS_ERROR";
		close(ECAL);
		waitpid($Pid_ecal,0);
		$ecal = 1;
}




#Update log tail frame
sub fill_log_widget {

    my($widget) = @ARG;

    $ARG = <H>;
    $widget->insert('end', $ARG);
    $widget->yview('end');
} # end fill_text_widget 



#Update RunMonitor frame
sub fill_check_widget {

    my($widget) = @ARG;

    $ARG = <H2>;
    $widget->insert('end', $ARG);
    $widget->yview('end');
} # end fill_text_widget 


#Update currentRun frame
sub fill_currentRun_widget {

    my($widget) = @ARG;

    $ARG = <H3>;
    $widget->insert('end', $ARG);
    $widget->yview('end');
} # end fill_text_widget 
