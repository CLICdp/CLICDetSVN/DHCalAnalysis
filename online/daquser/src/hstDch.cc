//#define HST_MAP

#include <iostream>
#include <fstream>
#include <sstream>

// dual/inc/utl
#include "UtlArguments.hh"

// dual/inc/rcd
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

// dual/inc/chk
#include "FixEmc.hh"
#include "FixAhc.hh"
#include "ChkCount.hh"
#include "ChkPrint.hh"
#include "HstGeneric.hh"
#include "HstTFile.hh"
#include "HstDch.hh"
//#include "HstGeneOff.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned inputTrg(argh.optionArgument('i',10,"Input line for trigger"));
  const unsigned printLevel(argh.optionArgument('p',0,"Print level"));
  const unsigned numberOfRecords(argh.optionArgument('r',0xffffffff,
						     "Number of records"));
  const unsigned runNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  cout << "Input trigger line set to " << inputTrg << endl;
  cout << "Print level set to " << printLevel << endl;
  cout << "Number of records set to " << numberOfRecords << endl;
  cout << "Run number set to " << runNumber << endl;
  cout << endl;

  RunReader theReader;
  RunReader *reader(&theReader);

  ChkCount hn;
  hn.printLevel(printLevel);

  /*
  ChkPrint hp;
  hp.printLevel(printLevel);
  hp.enable(subRecordType< CrcLocationData<CrcBeTrgConfigurationData> >(),true);
  hp.enable(subRecordType<DaqConfigurationStart>(),true);
  hp.enable(subRecordType<DaqConfigurationEnd>(),true);
  */

  HstTFile amm(argh.processName()+".root");
  HstDch ht(inputTrg);
  ht.printLevel(printLevel);

  RcdArena &arena(*(new RcdArena));
  unsigned iRecords(0);

  assert(reader->open(runNumber));

  //while(true) {
  while(reader->read(arena) && iRecords<numberOfRecords) {
    iRecords++;
    assert(hn.record(arena));
    assert(ht.record(arena));
    //assert(hp.record(arena));
    assert(amm.record(arena));
  }
  
  assert(reader->close());

  if(arena.recordType()!=RcdHeader::runEnd) amm.write();

  return 0;
}
