#include "runnerDefine.icc"

#include <iostream>

#include "RcdArena.hh"
#include "SubInserter.hh"
#include "UtlArguments.hh"



#include "AhcSlowReadout.hh"

using namespace std;


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const bool moveStage(argh.option('s',false,"Move stage"));
  const bool beamData(argh.option('b',false,"Request beam data"));
  if(argh.help()) return 0;

  AhcSlowReadout asr(AHC_STAGE_SKT);
  asr.printLevel(255);

  // Define record memory
  RcdArena &arena(*(new RcdArena));
  SubInserter inserter(arena);

  arena.initialise(RcdHeader::startUp);
  asr.record(arena);
  //      sleep(10);
    
  arena.initialise(RcdHeader::runStart);

  DaqRunStart *d(inserter.insert<DaqRunStart>(true));
  DaqRunType rt(DaqRunType::daqTest);
  if(beamData) rt.type(DaqRunType::beamData);
  d->runType(rt);

  asr.record(arena);
  //  sleep(10);

  for(unsigned i(0);i<4;i++) {
    arena.initialise(RcdHeader::configurationStart);

    if(moveStage) {
      AhcSlowConfigurationData *d(inserter.insert<AhcSlowConfigurationData>(true));
      d->xPosition((i%2)*1000);
      d->yPosition(((i/2)%2)*2000);
    }

    asr.record(arena);
    
    for(unsigned j(0);j<2;j++) {
      DaqTwoTimer ts;
      arena.initialise(RcdHeader::acquisitionStart);
      asr.record(arena);
      ts.setEndTime();
      ts.print() << std::endl;
      sleep(1);

      DaqTwoTimer te;
      arena.initialise(RcdHeader::acquisitionEnd);
      asr.record(arena);
      te.setEndTime();
      te.print() << std::endl;
      sleep(1);
    }

    arena.initialise(RcdHeader::slowReadout);
    asr.record(arena);
    sleep(1);

    arena.initialise(RcdHeader::configurationEnd);
    asr.record(arena);
  }

  arena.initialise(RcdHeader::runEnd);
  asr.record(arena);

  return 0;
}
