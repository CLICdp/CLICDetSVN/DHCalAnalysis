#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "DaqDbConfigurationData.hh"
#include "EmcCercBeConfigurationData.hh"
#include "EmcCercFeConfigurationData.hh"
#include "CercFeFakeEventData.hh"

#include "DaqRunStart.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"

#include "SubExtracter.hh"

#include "HstCosmics.hh"

using namespace std;

int main(int argc, const char **argv) {
  RcdArena &arena(*(new RcdArena));
  HstBase *hb(new HstCosmics(0xfff,-11.91,-14.09,false));

  RcdReaderAsc reader;
  //RcdReaderBin reader;
  reader.open("dat/Run1087597488");

  while(reader.read(arena)) {
    if(hb!=0) hb->record(arena);
  }

  reader.close();

  if(hb!=0) {
    hb->postscript("dps/Run1087597488.ps");
    delete hb;
  }
}
