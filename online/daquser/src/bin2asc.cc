#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RcdWriterAsc.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  argh.print(cout);
  const unsigned runNumber(argh.lastArgument(999999));
  
  RcdArena &arena(*(new RcdArena));
  RcdReaderBin reader;
  RcdWriterAsc writer;

  ostringstream sout;
  sout << "data/dat/Run" << runNumber;

  reader.open(sout.str());
  writer.open(sout.str());

  while(reader.read(arena) && writer.write(arena));

  reader.close();
  writer.close();
}
