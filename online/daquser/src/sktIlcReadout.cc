#include "runnerDefine.icc"

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdIoSkt.hh"

#include "SubRecordType.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "RcdMultiUserRW.hh"
#include "RcdMultiUserRO.hh"
#include "RunMonitor.hh"

#include "RunLock.hh"

#include "MpsReadout.hh"

//efine SKT_READ_AHEAD

using namespace std;


int main(int argc, const char **argv) {
  
  time_t eTime(CALICE_DAQ_TIME);
  cout << argv[0] << " compiled at " << ctime((const time_t*)&eTime);
  
  UtlArguments argh(argc,argv);
  
  const unsigned printLevel(argh.optionArgument('p',9,"Print level"));
  const unsigned socketPort(argh.optionArgument('s',1124,"Socket Port"));
  const unsigned numberOfUsbDaqs(argh.optionArgument('n',8,"Number of USB_DAQs"));
  
  if(argh.help()) return 0;
  
  RunLock lock(argv[0]);
  
  RcdIoSkt s;
  assert(s.open(socketPort));

  // Set up list of RW modules
  RcdMultiUserRW vRrw;

  MpsReadout er(MPS_LOCATION,numberOfUsbDaqs);
  vRrw.addUser(er);

  // Set up list of RO modules
  RcdMultiUserRO vRro;

  // Add display module
  //RunMonitor rm;
  //vRro.addUser(rm);

  // Define memory for records
  RcdArena &r(*(new RcdArena()));
  r.initialise(RcdHeader::startUp);
  
  // Set initial print level;
  vRrw.printLevel(printLevel);
  vRro.printLevel(printLevel);

#ifdef SKT_READ_AHEAD

  RcdArena &ahead(*(new RcdArena));
  ahead.initialise(RcdHeader::event);
  bool inSpill(false),eventAhead(false);
  unsigned nTrg(0),nEvt(0);

#endif

  // Define the id label for the timer
  UtlPack tid;
  tid.halfWord(1,SubHeader::mps);
  tid.byte(2,0xff);
 
  while(r.recordType()!=RcdHeader::shutDown) {
    
    // Read next record from socket while timing the wait
    DaqTwoTimer tt;
    tt.timerId(tid.word());

    assert(s.read(r));

    tt.setEndTime();
    SubInserter inserter(r);
    inserter.insert<DaqTwoTimer>(tt);
    
    // Check for startUp record
    if(r.recordType()==RcdHeader::startUp) {
      
      // Put in software information
      SubInserter inserter(r);
      DaqSoftware *dsw(inserter.insert<DaqSoftware>(true));
      dsw->message(argh.command());
      dsw->setVersions();
      dsw->print(std::cout);
    }
    
    // Check for shutDown record
    if(r.recordType()==RcdHeader::shutDown) {

      // Reset print level
      vRrw.printLevel(printLevel);
      vRro.printLevel(printLevel);
    }

    // Check for runStart record
    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level
      SubAccessor accessor(r);
      std::vector<const IlcRunStart*> v(accessor.extract<IlcRunStart>());
      if(v.size()>0) {
	vRrw.printLevel(v[0]->runType().printLevel());
	vRro.printLevel(v[0]->runType().printLevel());
      }
    }

#ifndef SKT_READ_AHEAD

    // Process record and send back
    assert(vRrw.record(r));
    assert(vRro.record(r));
    assert(s.write(r));

#else

    // Zero trigger and event counters in acquisition start
    if(r.recordType()==RcdHeader::acquisitionStart) {
      nTrg=0;
      nEvt=0;
    }

    // Check for mismatch of counters
    if(r.recordType()==RcdHeader::acquisitionEnd) {
      if(nTrg!=nEvt) {
	std::cerr << "sktReadout counter mismatch, nTrg = "
		  << nTrg << ", nEvt = " << nEvt << std::endl;
	r.RcdHeader::print(std::cerr," SKT ") << std::endl;
      }
    }

    // Check for spillStart/End records
    if(r.recordType()==RcdHeader::spillStart) inSpill=true;
    if(r.recordType()==RcdHeader::spillEnd)   inSpill=false;

    // Increment counter
    if(r.recordType()==RcdHeader::trigger) nTrg++;

    if(r.recordType()==RcdHeader::triggerBurst) {
      SubAccessor accessor(r);
      std::vector<const DaqBurstEnd*> v(accessor.extract<DaqBurstEnd>());
      if(v.size()>0) {
	nTrg+=v[0]->actualNumberOfTriggersInBurst();
      }
    }

    // Process record and send back
    if(r.recordType()==RcdHeader::event && eventAhead) {
      eventAhead=false;
      r.extend(ahead.numberOfWords(),ahead.data());
    } else {
      if(r.recordType()==RcdHeader::event) nEvt++;
      assert(vRrw.record(r));
      assert(vRro.record(r));
    }
    assert(s.write(r));

    // Check for possibility of readahead
    if(!eventAhead && !inSpill && nTrg>nEvt) {
      eventAhead=true;
      nEvt++;
      ahead.initialise(RcdHeader::event);
      assert(vRrw.record(ahead));
      assert(vRro.record(ahead));
    }

#endif

  }

  assert(s.close());

  return 0;
}
