#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>

#include <iostream>

#include "TSystem.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TText.h"
#include "TTimer.h"

#include "UtlArguments.hh"

class LogMonitor : public TTimer {

public:
  LogMonitor(std::string f, int d=24, int s=4)
    : _logFile(f), _displayLines(d), _sleepSecs(s) {

    _canvas = new TCanvas(_logFile.c_str(),_logFile.c_str(),
			  20,40,600,600);
    _canvas->Range(0,0,20,24);

    _text = new TText (0,0,"a");
    _text->SetTextFont(62);
    _text->SetTextSize(0.025);
    _text->SetTextAlign(12);
    
    SetTime(_sleepSecs*1000);
    gSystem->AddTimer(this);
  }

  virtual ~LogMonitor() {
  }

  Bool_t Notify() {

    // Actions after timer time-out

    struct stat fs;
    if (stat(_logFile.c_str(),&fs) <0) {
      std::string error = _logFile + " does not exist";
      _text->DrawText(1,23, error.c_str());
      _canvas->Update();

      TTimer::TurnOff();
      return kFALSE;
    }

    if ((time(NULL) - fs.st_mtime ) > _sleepSecs) {
      TTimer::Reset();
      return kFALSE;
    }

    FILE* fp = fopen(_logFile.c_str(), "r");

    // count lines in file
    char s[256];
    int count(0);
    while (fgets(s, sizeof(s), fp))
      count++;

    fclose(fp);

    // reopen file and process stream
    fp = fopen(_logFile.c_str(), "r");

    _canvas->Clear();
    _canvas->RaiseWindow();

    int lp = _displayLines;
    while (fgets(s, sizeof(s), fp)) {
      if (count-- < _displayLines && lp-- > 0)
	_text->DrawText(1,lp,s);
    }

    fclose(fp);

    // try  to sound alarm
    gSystem->Exec("play /usr/share/sounds/error.wav &>/dev/null");

    _canvas->Update();
 
    TTimer::Reset();
    return kFALSE;
  }

private:
  const std::string _logFile;
  const int _displayLines;
  const int _sleepSecs;

  TCanvas* _canvas;
  TText* _text;

};


int main(int argc, const char *argv[]) {

  UtlArguments argh(argc,argv);
  const std::string logFile(argh.optionArgument('f',"ActiveLog.err","File to monitor"));
  const int displayLines(argh.optionArgument('l',24,"Lines of text to display"));
  const int sleepSecs(argh.optionArgument('s',4,"Sleep period (secs), 0 = do once"));
  if(argh.help()) return 0;


  TApplication _application("logMonitor Application",0,0);

  LogMonitor* lm = new LogMonitor(logFile, displayLines, sleepSecs);

  _application.Run(1);

  delete lm;
}
