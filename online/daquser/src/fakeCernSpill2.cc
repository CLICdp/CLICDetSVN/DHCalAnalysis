#include <unistd.h>
#include <iostream>

#include "CrcFakeCernSpill.hh"
#include "UtlTime.hh"
#include "ShmObject.hh"

int main() {
  
  ShmObject<CrcFakeCernSpill> _shmCrcFakeCernSpill(CrcFakeCernSpill::shmKey);
  CrcFakeCernSpill *_pShm(_shmCrcFakeCernSpill.payload());
  assert(_pShm!=0);

  _pShm->_invert=!_pShm->_invert;

  if(_pShm->_invert) std::cout << "Invert set true" << std::endl;
  else               std::cout << "Invert set false" << std::endl;

  return 0;
}
