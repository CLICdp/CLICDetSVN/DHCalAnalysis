#include "runnerDefine.icc"

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "DaqRunType.hh"
#include "IlcRunType.hh"

using namespace std;

int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);

#ifndef DAQ_ILC_TIMING
  const std::string type(argh.lastArgument("crcNoise","Run type"));
#else
  const std::string type(argh.lastArgument("mpsNoise","Run type"));
#endif

  if(argh.help()) return 0;

  std::cout << std::endl
#ifndef DAQ_ILC_TIMING
	    << DaqRunType::typeComment(DaqRunType::typeNumber(type))
#else
	    << IlcRunType::typeComment(IlcRunType::typeNumber(type))
#endif
	    << std::endl << std::endl;

  return 0;
}
