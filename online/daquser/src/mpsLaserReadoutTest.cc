#include "runnerDefine.icc"

#include <iostream>

#include "RcdArena.hh"
#include "SubInserter.hh"
#include "UtlArguments.hh"

#include "MpsLaserReadout.hh"

using namespace std;


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const bool moveStage(argh.option('s',false,"Move stage"));
  const bool fireLaser(argh.option('f',false,"Fire laser"));
  const unsigned nCfg(argh.optionArgument('c',4,"Number of configurations"));
  const unsigned nBnt(argh.optionArgument('b',10,"Number of bunch trains"));
  if(argh.help()) return 0;

  MpsLaserReadout asr(MPS_LASER_SKT);
  asr.printLevel(255);

  // Define record memory
  RcdArena &arena(*(new RcdArena));
  SubInserter inserter(arena);

  arena.initialise(RcdHeader::startUp);
  asr.record(arena);
    
  arena.initialise(RcdHeader::runStart);
  asr.record(arena);

  for(unsigned i(0);i<nCfg;i++) {
    arena.initialise(RcdHeader::configurationStart);

    MpsLaserConfigurationData *d(inserter.insert<MpsLaserConfigurationData>(true));
    d->numberOfPulses(10);

    if(moveStage) {
      d->stageX((i%2)*1000);
      d->stageY(((i/2)%2)*1000);
    }		
    
    if(fireLaser) {
      d->triggerMode(1);
    }

    asr.record(arena);
    sleep(1);

    for(unsigned j(0);j<nBnt;j++) {
      arena.initialise(RcdHeader::bunchTrain);
      asr.record(arena);
    }

    arena.initialise(RcdHeader::configurationEnd);
    asr.record(arena);
  }
  
  arena.initialise(RcdHeader::runEnd);
  asr.record(arena);
  
  arena.initialise(RcdHeader::shutDown);
  asr.record(arena);
  
  return 0;
}
