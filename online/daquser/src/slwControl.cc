#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "DaqSlwControl.hh"
#include "ShmObject.hh"

using namespace std;

int main(int argc, const char **argv) {

  time_t eTime(CALICE_DAQ_TIME);
  cout << argv[0] << " compiled at " << ctime((const time_t*)&eTime) << endl;
 
  UtlArguments argh(argc,argv);
  unsigned vnum(argh.lastArgument(999999)); 
   
  //if(argh.help()) return 0;
   
  ShmObject<DaqSlwControl> shmSlwControl(DaqSlwControl::shmKey);
  DaqSlwControl *p(shmSlwControl.payload());
  if(p==0) return 1;

  if(vnum>=DaqSlwControl::endOfStateEnum) vnum=DaqSlwControl::running;

  p->command(0,(DaqSlwControl::State)vnum);
  while(p->status()>(DaqSlwControl::State)vnum) sleep(1);

  if(vnum==0) p->command(0,DaqSlwControl::running);

  p->print(cout);
  
  return 0;
}
