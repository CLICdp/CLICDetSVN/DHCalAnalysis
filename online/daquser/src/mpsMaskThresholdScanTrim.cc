#include <cmath>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"

#include "UtlArguments.hh"
#include "MpsSensorV11ConfigurationData.hh"

class HitCount {
public:
  int _threshold;
  double _sgCount[168][168];
  double _bgCount[168][168];

  void reset() {
    for(unsigned x(0);x<168;x++) {
      for(unsigned y(0);y<168;y++) {
	_sgCount[x][y]=0;
	_bgCount[x][y]=0;
      }
    }
  }
};


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  const std::string sensor(argh.lastArgument("Sensor21"));

  if(argh.help()) return 0;

  TFile* _rootFile;
  _rootFile = new TFile((sensor+".root").c_str(),"RECREATE");

  std::vector<std::string> v;
  v.push_back("Trim0");
  v.push_back("Bit0");
  v.push_back("Bit1");
  v.push_back("Bit2");
  v.push_back("Bit3");
  v.push_back("Bit4");
  v.push_back("Bit5");
  v.push_back("TrimV0");

  unsigned trim[8][168][168];
  unsigned entries[8][168][168];
  double mean[8][168][168];
  double sigma[8][168][168];

  unsigned vFiles(0);
  for(unsigned f(0);f<v.size() && vFiles==0;f++) {
    std::ifstream fin;
    std::cout << "Opening " << (sensor+v[f]+".txt").c_str() << std::endl;
    fin.open((sensor+v[f]+".txt").c_str());
    if(!fin) {
      std::cout << "Open failed" << std::endl;
      vFiles=f;
    } else {

      for(unsigned x(0);x<168;x++) {
	for(unsigned y(0);y<168;y++) {
	  unsigned a,b;
	  fin >> a >> b;
	  assert(a==x);
	  assert(b==y);
	  fin >> trim[f][x][y] >> entries[f][x][y]
	      >> mean[f][x][y] >> sigma[f][x][y];
	  
	  if(sigma[f][x][y]>=4.0 && sigma[f][x][y]<12.0) {
	  } else {
	    std::cout << "File " << f << ", x,y = "
		      << x << ", " << y
		      << ", RMS = " << sigma[f][x][y] << std::endl;
	  }
	}
      }
    }
  }
  if(vFiles==0) vFiles=v.size();

  std::vector<std::string> w;
  w.push_back("Quad0");
  w.push_back("Quad1");
  w.push_back("Quad2");
  w.push_back("Quad3");
  w.push_back("All");

  std::vector<TH1F*> hTrims[5];
  std::vector<TH1F*> hEntries[5];
  std::vector<TH1F*> hMeans[5];
  std::vector<TH1F*> hSigmas[5];
  std::vector<TH2F*> hTrimSigma[5];
  std::vector<TH2F*> hMeanSigma[5];
  std::vector<TH2F*> hTrimShift[5];
  std::vector<TH2F*> hShiftTrim[5];
  std::vector<TProfile*> pShiftTrim[5];
  std::vector<TH2F*> h2Means[5];
  std::vector<TH2F*> h2Sigmas[5];
  
  std::vector<TH2F*> hMeanT[5];
  std::vector<TH2F*> hSigmaT[5];

  //unsigned trimmed(999);
  for(unsigned f(0);f<vFiles;f++) {
    /*
    if(v[f]=="Paul") {
      std::cout << "Found trimmed file in list = " << f << std::endl;
      trimmed=f;
    }
    */

    for(unsigned q(0);q<5;q++) {
      hTrims[q].push_back(new TH1F((v[f]+w[q]+"Trims").c_str(),
				   (v[f]+" "+w[q]+" Trims").c_str(),
				   64,0.0,64.0));
      hEntries[q].push_back(new TH1F((v[f]+w[q]+"Entries").c_str(),
				     (v[f]+" "+w[q]+" Entries").c_str(),
				     100,0.0,20000.0));
      hMeans[q].push_back(new TH1F((v[f]+w[q]+"Means").c_str(),
				   (v[f]+" "+w[q]+" Means").c_str(),
				   150,-100.0,200.0));
      hSigmas[q].push_back(new TH1F((v[f]+w[q]+"Sigmas").c_str(),
				    (v[f]+" "+w[q]+" Sigmas").c_str(),
				    100,0.0,20.0));
      if(q<4) {
	h2Means[q].push_back(new TH2F((v[f]+w[q]+"2DMeans").c_str(),
				       (v[f]+" "+w[q]+" Mean vs x,y").c_str(),
				       84,(q/2)*84.0,(1+q/2)*84.0,84,(q%2)*84.0,(1+(q%2))*84.0));
	h2Sigmas[q].push_back(new TH2F((v[f]+w[q]+"2DSigmas").c_str(),
				       (v[f]+" "+w[q]+" Sigma vs x,y").c_str(),
				       84,(q/2)*84.0,(1+q/2)*84.0,84,(q%2)*84.0,(1+(q%2))*84.0));
      } else {
	h2Means[q].push_back(new TH2F((v[f]+w[q]+"2DMeans").c_str(),
				       (v[f]+" "+w[q]+" Mean vs x,y").c_str(),
				       168,0.0,168.0,168,0.0,168.0));
	h2Sigmas[q].push_back(new TH2F((v[f]+w[q]+"2DSigmas").c_str(),
				       (v[f]+" "+w[q]+" Sigma vs x,y").c_str(),
				       168,0.0,168.0,168,0.0,168.0));
      }

      hTrimSigma[q].push_back(new TH2F((v[f]+w[q]+"TrimSigma").c_str(),
				       (v[f]+" "+w[q]+" Sigma vs trim").c_str(),
				       64,0.0,64.0,100,0.0,20.0));
      hMeanSigma[q].push_back(new TH2F((v[f]+w[q]+"MeanSigma").c_str(),
				       (v[f]+" "+w[q]+" Sigma vs mean").c_str(),
				       150,-100.0,200.0,100,0.0,20.0));
      hTrimShift[q].push_back(new TH2F((v[f]+w[q]+"TrimShift").c_str(),
				       (v[f]+" "+w[q]+" Shift vs trim").c_str(),
				       64,0.0,64.0,100,0.0,200.0));
      hShiftTrim[q].push_back(new TH2F((v[f]+w[q]+"ShiftTrim").c_str(),
				       (v[f]+" "+w[q]+" Trim vs shift").c_str(),
				       100,0.0,200.0,64,0.0,64.0));
      pShiftTrim[q].push_back(new TProfile((v[f]+w[q]+"ShiftTrimP").c_str(),
				       (v[f]+" "+w[q]+" Trim vs shift").c_str(),
				       100,0.0,200.0,0.0,64.0));

      for(unsigned g(f+1);g<vFiles;g++) {
	hMeanT[q].push_back(new TH2F((v[f]+v[g]+w[q]+"MeanT").c_str(),
				     (v[g]+" vs "+v[f]+" "+w[q]+" Means").c_str(),
				     150,-100.0,200.0,150,-100.0,200.0));
	hSigmaT[q].push_back(new TH2F((v[f]+v[g]+w[q]+"SigmaT").c_str(),
				      (v[g]+" vs "+v[f]+" "+w[q]+" Sigmas").c_str(),
				      100,0.0,20.0,100,0.0,20.0));
      }
    }
  }
  
  /*
  TH1F *hDiff80=new TH1F("hDiff80","Trim 8-0 Means",
		       100,0.0,100.0);
  TH2F *hNoise80=new TH2F("hNoise80","Trim 8 Sigma vs trim 0 sigma",
		       100,0.0,20.0,100,0.0,20.0);
  TH1F *hDiffF0=new TH1F("hDiffF0","Trim 15-0 Means",
		       100,0.0,200.0);
  TH2F *hNoiseF0=new TH2F("hNoiseF0","Trim 15 Sigma vs trim 0 sigma",
		       100,0.0,20.0,100,0.0,20.0);
  */
  /*
  TProfile *hEff=new TProfile("hEff","Relative efficiency vs real threshold",
			      60,0.0,300.0);
  TProfile *hEfs=new TProfile("hEfs","Relative efficiency vs real threshold/sigma",
			      60,0.0,60.0);

  std::vector<HitCount> vHitCount;
  std::vector<TH1F*> vThreshold;
  std::vector<TH1F*> vToverS;
  std::vector<TH1F*> vThresholdBg;
  std::vector<TH1F*> vToverSBg;
  std::vector<TH1F*> vThresholdSg;
  std::vector<TH1F*> vToverSSg;
  std::vector<TH1F*> vThresholdSB;
  std::vector<TH1F*> vToverSSB;
  */

  for(unsigned x(0);x<168;x++) {
    for(unsigned y(0);y<168;y++) {
      unsigned q(2*(x/84)+(y/84));
      
      unsigned fTrim0(999);
      for(unsigned f(0);f<vFiles;f++) {
	if(trim[f][x][y]==0) fTrim0=f;
      }
      
      unsigned n(0);
      for(unsigned f(0);f<vFiles;f++) {
	
	hTrims[q][f]->Fill(trim[f][x][y]);
	hTrims[4][f]->Fill(trim[f][x][y]);
	
	hEntries[q][f]->Fill(entries[f][x][y]);
	hEntries[4][f]->Fill(entries[f][x][y]);
	
	if(entries[f][x][y]>0) {
	  hMeans[q][f]->Fill(mean[f][x][y]);
	  hSigmas[q][f]->Fill(sigma[f][x][y]);
	  h2Means[q][f]->Fill(x,y,mean[f][x][y]);
	  h2Sigmas[q][f]->Fill(x,y,sigma[f][x][y]);
	  hTrimSigma[q][f]->Fill(trim[f][x][y],sigma[f][x][y]);
	  hMeanSigma[q][f]->Fill(mean[f][x][y],sigma[f][x][y]);
	  
	  if(fTrim0<999) {
	    hTrimShift[q][f]->Fill(trim[f][x][y],mean[f][x][y]-mean[fTrim0][x][y]);
	    hShiftTrim[q][f]->Fill(mean[f][x][y]-mean[fTrim0][x][y],trim[f][x][y]);
	    pShiftTrim[q][f]->Fill(mean[f][x][y]-mean[fTrim0][x][y],trim[f][x][y]);
	  }
	  
	  hMeans[4][f]->Fill(mean[f][x][y]);
	  hSigmas[4][f]->Fill(sigma[f][x][y]);
	  h2Means[4][f]->Fill(x,y,mean[f][x][y]);
	  h2Sigmas[4][f]->Fill(x,y,sigma[f][x][y]);
	  hTrimSigma[4][f]->Fill(trim[f][x][y],sigma[f][x][y]);
	  hMeanSigma[4][f]->Fill(mean[f][x][y],sigma[f][x][y]);

	  if(fTrim0<999) {
	    hTrimShift[4][f]->Fill(trim[f][x][y],mean[f][x][y]-mean[fTrim0][x][y]);
	    hShiftTrim[4][f]->Fill(mean[f][x][y]-mean[fTrim0][x][y],trim[f][x][y]);
	    pShiftTrim[4][f]->Fill(mean[f][x][y]-mean[fTrim0][x][y],trim[f][x][y]);
	  }

	  for(unsigned g(f+1);g<vFiles;g++) {
	    if(entries[g][x][y]>0) {
	      hMeanT[q][n]->Fill(mean[f][x][y],mean[g][x][y]);
	      hSigmaT[q][n]->Fill(sigma[f][x][y],sigma[g][x][y]);

	      hMeanT[4][n]->Fill(mean[f][x][y],mean[g][x][y]);
	      hSigmaT[4][n]->Fill(sigma[f][x][y],sigma[g][x][y]);
	    }
	    n++;
	  }
	}
      }
    }
  }

  MpsSensorV11ConfigurationData cfgData;
  //cfgData.readFile(std::string("cfg/MpsSensor1ConfigurationData")+sensor+".cfg");
  cfgData.maskSensor(false);

  double target(100.0);
  for(unsigned x(0);x<168;x++) {
    for(unsigned y(0);y<168;y++) {

      /*
      unsigned fClosest(0);
      double mClosest(1.0e6);
      for(unsigned f(0);f<vFiles;f++) {
	if(entries[f][x][y]==0) {
	  //if(f==0) trim[f][x][y]=0;
	  mean[f][x][y]=500.0;
	}

	if(fabs(target-mean[f][x][y])<mClosest) {
	  mClosest=fabs(target-mean[f][x][y]);
	  fClosest=f;
	}
      }

      assert(trim[0][x][y]==0);

      double tLimit(36.3);
      if(fClosest!=0) {
	tLimit=trim[fClosest][x][y]/(1.0-exp(-0.0121*(mean[fClosest][x][y]-mean[0][x][y])));
      }

      double t0(tLimit*(1.0-exp(-0.0121*(target-mean[0][x][y]))));
      if(t0>32.0) t0=32.0;

      int it0((int)(t0+0.5));
      if(it0< 0) it0= 0;
      if(it0>63) it0=63;

      unsigned it((unsigned)it0);
      */

      unsigned it(0),bt(0);
      double btd(1.0e6);
      for(unsigned f(0);f<vFiles;f++) {
	if(entries[f][x][y]>0) {
	  if(fabs(mean[f][x][y]-target)<btd) {
	    btd=fabs(mean[f][x][y]-target);
	    bt=trim[f][x][y];
	  }
	}

	if(f==0) {
	  if(entries[f][x][y]>0 && mean[f][x][y]<target) it=32;
	}
	if(f>=6) {
	  //if(entries[f][x][y]>0 && mean[f][x][y]<target) it+=0;
	  //else                                           it-=1;
	  it=bt;

	  if(mean[vFiles-1][x][y]<85.0 || mean[vFiles-1][x][y]>=115.0) it=0;
	}
	if(f>=1 && f<=5) {
	  if(entries[f][x][y]>0 && mean[f][x][y]<target) it+=(1<<(5-f));
	  else                                           it-=(1<<(5-f));
	}
      }

      if(it>63) it=32;

      

      if(it!=trim[vFiles-1][x][y]) {
	std::cout << x << ", " << y
		  << ", old mean " << mean[vFiles-1][x][y]
		  << " old,new trims = "
		  << trim[vFiles-1][x][y] << ", " << it << std::endl;
      }
      cfgData.trim(x,y,it);
    }
  }


  /*
      hDiffF0->Fill(mean[2][x][y]-mean[0][x][y]);
      hNoiseF0->Fill(sigma[0][x][y],sigma[2][x][y]);

      hEntriesT->Fill(entries[3][x][y]);
      hMeanT->Fill(mean[3][x][y]);
      hSigmaT->Fill(sigma[3][x][y]);


      unsigned fiff(4);
      double diff(1.0e6);
      for(unsigned f(0);f<4;f++) {
	double d(fabs(target-mean[f][x][y]));
	if(d<diff) {
	  fiff=f;
	  diff=d;
	}
      }
      if(fiff!=3) {
	  std::cout << "x " << x << ", y " << y
		    << ", means " << mean[0][x][y] << " " << mean[1][x][y] << " " << mean[2][x][y] << " " << mean[3][x][y]
		    << ", fiff = " << fiff << std::endl;
      }


      unsigned trim(0);
      if(mean[2][x][y]!=0.0) {
	double a((mean[2][x][y]/(7.0*15.0))-(mean[1][x][y]/(7.0*8.0))+(mean[0][x][y]/(15.0*8.0)));
	double b(-(8.0*mean[2][x][y]/(7.0*15.0))+(15.0*mean[1][x][y]/(7.0*8.0))-(23.0*mean[0][x][y]/(15.0*8.0)));
	double c(mean[0][x][y]-target);
	double t(0.5*(-b+sqrt(b*b-4.0*a*c))/a);

	trim=(unsigned)(t+0.5);
	if(t< 0.0) trim=0;
	if(t>15.0) trim=15;

	  std::cout << "x " << x << ", y " << y
		    << ", means " << mean[0][x][y] << " " << mean[1][x][y] << " " << mean[2][x][y]
		    << ", trim = " << trim << std::endl;

      } else {

	double t2(8.0*(target-mean[0][x][y])/(mean[1][x][y]-mean[0][x][y]));
	unsigned trim2=(unsigned)(t2+0.5);
	if(t2< 0.0) trim2=0;
	if(t2>15.0) trim2=15;
	
	//if(trim2>(trim+2) || trim>(trim2+2)) {
	  std::cout << "x " << x << ", y " << y
		    << ", means " << mean[0][x][y] << " " << mean[1][x][y] << " " << mean[2][x][y]
		    << ", trim and trim2 = " << trim << ", " << trim2 << std::endl;
	  //}

	trim=trim2;
      }

      cfgData.trim(x,y,trim);
    }
  }
  */

  cfgData.writeFile(sensor+".cfg");
  //cfgData.print();


  if(_rootFile!=0) {
    _rootFile->Write();
    _rootFile->Close();
    delete _rootFile;
    _rootFile=0;
  }
}
