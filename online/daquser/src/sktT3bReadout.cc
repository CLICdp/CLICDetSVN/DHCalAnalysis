#include "runnerDefine.icc"

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <list>
//#include <map>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdIoSkt.hh"

#include "SubRecordType.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "RcdMultiUserRW.hh"
#include "RcdMultiUserRO.hh"
#include "RcdCount.hh"
#include "RunLock.hh"
#include "RunMonitor.hh"
#include "TrgConfiguration.hh"

#include <time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <float.h>

using namespace std;

struct SpillInfo
{
	unsigned		runNumber;
	unsigned		internalSpillNumber;
	unsigned		ignoredBeamConfigurations;

	struct timeval	acquisitionTimeStart;
	struct timeval	acquisitionTimeStop;
	struct timeval	firstEventTime;
	struct timeval	lastEventTime;

	unsigned		nrEventsInAcquisition;
	unsigned		nrEventsInSpill;
	unsigned		nrEventsInSpillNoFrontendLine;
	unsigned		nrEventsInSpillScintCoincidence;
	unsigned		nrEventsInSpillOscillatorOrSoftware;
	
};

void* listenT3B(void*);
double timediff(struct timeval* a, struct timeval* b);

class RcdT3B
{
public:
	RcdT3B();

	int listenCalice();
	SpillInfo* getSpillInfoForTime(struct timeval* time);
	unsigned getT3BListenPort() { return _sktT3BDaqQuery; }

	void setVerbose(bool v) { _verbose = v; }
protected:
	// functions
	void loadValues();

	bool rcdCheckIfRunStarted();
	bool rcdCheckIfWithinConfiguration();
	bool rcdCheckIfWithinAcquisition();
	void rcdCheckForEvents();

	bool isInTime(const CrcVlinkTrgData* triggerData, int lineNumber, bool strictInTime=true);
	void fillCaliceEventTimes(std::vector<const CrcLocationData<CrcVlinkEventData>*>&);

private:
	// global settings
	int _channelSpillTrigger;
	int _channelExtScintillator10x10Trigger;
	int _channelExtScintillator80x50Trigger;
	int _channelIntScintillatorTrigger;
	int _channelFrontendTrigger;
	int _channelOscillator;
	int _channelSoftwareTrigger;

	double 		_spillTimeAccuracy;
	unsigned int 	_timeWindowAccuracy;
	unsigned int 	_sktCaliceDaq;
	unsigned int 	_sktT3BDaqQuery;
	unsigned int	_waitForFutureSpills;

	// status of the CALICE DAQ
	bool _runStarted;
	bool _beamConfiguration;
	unsigned _nrIgnoredConfigurations;
	bool _beamAcquisition;
	bool _beamSpill;
	int  _runNumber;
	int	 _internalSpillNumber;

	// verbosity for debugging
	bool _verbose;
	void printRcdType();
	
	// for each event: saves the start/stoptime of when a real calice trigger (_channelGeneralTrigger) occured
	list < pair< unsigned, unsigned > > _caliceEventTimes;

	// threading access to the spillInfo list
	pthread_mutex_t		_mutex;
	list < SpillInfo* >	_spillInfoList;
	//map < unsigned, SpillInfo* >	_spillInfoList;
	SpillInfo*			_curSpillInfo;
	unsigned int		_maxHistoryListSize;

	// Define memory for records
	RcdArena r;
	SubAccessor accessor;

};

// for date/time timestamp @ log
const unsigned DATE_SIZE = 256;
char dataBuffer[DATE_SIZE];

const char* now()
{
	time_t t;
	time(&t);
	strftime(dataBuffer, DATE_SIZE, "[%Y-%m-%d %H:%M:%S] ", gmtime(&t));
	return dataBuffer;
}

// helper functions
void serialize(unsigned x, char* a)
{
	for (int i=sizeof(x)-1; i>=0; i--)
	{
		a[i] = (char)(x % 256);
		x /= 256;
	}
}

unsigned deserializeU(char* a)
{
	unsigned x = 0;
	for (unsigned i=0; i<sizeof(x); i++)
	{
		x *= 256;
		x += (unsigned char)a[i];
	}
	return x;
}

// lock file, so that we cannot be started twice
RunLock* lock;

// implementation
int main(int argc, const char **argv)
{
  int retVal(0);

#ifndef T3B_SKT
  std::cout << now() <<argv[0] << " not compiled for use; exiting" << std::endl;

#else
  lock = new RunLock(argv[0]);

	  // Any arguments needed
  UtlArguments argh(argc,argv);

  const bool verbose(argh.option('v',false,"Turn on printout"));

  if(argh.help()) 
		return 0;

  RcdT3B* t;
  t = new RcdT3B();
	t->setVerbose(verbose);
  retVal = t->listenCalice();
  delete t;
#endif
  delete lock;
  return retVal;
}


RcdT3B::RcdT3B()
	: r(*(new RcdArena)), accessor(r)
{
	_curSpillInfo = NULL;
//	_mutex(PTHREAD_MUTEX_INITIALIZER);
	pthread_mutex_init(&_mutex, NULL);

	// load values from settings file
	loadValues();

	// start the udp listener thread for the T3B queries
	pthread_t udpListenerT3BThread;
	pthread_create( &udpListenerT3BThread, NULL, listenT3B, this );
}

void RcdT3B::loadValues()
{
	// TODO: read this from settings file!

	// the input lines
	_channelSpillTrigger = 5;
	_channelExtScintillator10x10Trigger = 9;
	_channelExtScintillator80x50Trigger = 8;
	_channelIntScintillatorTrigger = 25;
	_channelFrontendTrigger = 19;
	_channelOscillator = 24;
	_channelSoftwareTrigger = 22;

	// store this many history events
	_maxHistoryListSize = 1000;

	// if 2 events are within this many <time window of size 25ns>, we define this as coincidence
	_timeWindowAccuracy = 5;
	// similar, but this time when T3B ask for a certain time, how accurate (in sec) do we have to be?
	_spillTimeAccuracy = 3;

	// if a request lies less than this many seconds in the future: wait for it to happen
	_waitForFutureSpills = 60;

	// the sockets used for communication with the DAQs
	_sktCaliceDaq = 1128;
	_sktT3BDaqQuery = 1129;
}

int RcdT3B::listenCalice()
{
	_runStarted = false;
	_beamAcquisition = false;
	_beamConfiguration = false;
	_beamSpill = false;

	// outer loop: reestablish socket connection even after a disconnect
	while (true)
	{
		cout << now() <<"Opening CALICE DAQ listener on port " << _sktCaliceDaq << endl;
		
		// Open listening socket
		RcdIoSkt s;

		if (!s.open(_sktCaliceDaq))
		{
			cerr << "ERROR: Couldn't bind to port " << _sktCaliceDaq << endl;
		}
		else
		{	// ok,socket is open. now start reading into the RcdArena permanently
			
			// inner loop: permanently read next record from the current
			while (true)
			{
				if (!s.read(r))
				{
					cerr << "ERROR: Couldn't read from RcdIoSkt! Will retry." << endl;
					break;	// ... out of inner loop, next iteration of outer loop will reestablish socket connection
				}
				else
				{
					// if we are here, then we successfully recieved a packet
					// now check what kind of packet

					// first: print the rcd type to the console (only in verbose mode)
					printRcdType();
					
					// On a Shutdown we cancel the connection
					if (r.recordType() == RcdHeader::shutDown)
					{
						cout << now() <<"Shutdown recieved. Closing RcdIoSkt" << endl;
                        delete lock;
						exit(0);
						break;
					}
				
					
					// check if we have a started run
					if (!rcdCheckIfRunStarted())
						continue;

						
					// check if we are within a real beam configuration (i.e. no LED/PED runs)
					if (!rcdCheckIfWithinConfiguration())
						continue;

					
					// check if we are within the spill
					if (!rcdCheckIfWithinAcquisition())
						continue;
					
						
					// do the counting
					rcdCheckForEvents();

				}
			} // end inner loop
		}
	} // end outer loop
}

void RcdT3B::printRcdType()
{
	if (!_verbose)
		return;
		
	if (r.recordType() == RcdHeader::runStart) cout << " [V] RcdHeader::runStart" << endl;
	else if (r.recordType() == RcdHeader::runStart) cout 	<< " [V] RcdHeader::runStart" << endl;
	else if (r.recordType() == RcdHeader::runStop) cout 	<< " [V] RcdHeader::runStop" << endl;
	else if (r.recordType() == RcdHeader::runEnd) cout 		<< " [V] RcdHeader::runEnd" << endl;
	else if (r.recordType() == RcdHeader::configurationStart) cout 	<< " [V] RcdHeader::configurationStart" << endl;
	else if (r.recordType() == RcdHeader::configurationEnd) cout << " [V] RcdHeader::configurationEnd" << endl;
	else if (r.recordType() == RcdHeader::acquisitionStart) cout << " [V] RcdHeader::acquisitionStart" << endl;
	else if (r.recordType() == RcdHeader::acquisitionEnd) cout << " [V] RcdHeader::acquisitionEnd" << endl;
	else if (r.recordType() == RcdHeader::event) cout << " [V] RcdHeader::event" << endl;
	else if (r.recordType() == RcdHeader::trigger) cout << " [V] RcdHeader::trigger" << endl;
}

bool RcdT3B::rcdCheckIfRunStarted()
{
	if (r.recordType() == RcdHeader::runStart)
	{
		// Access the DaqRunStart
		std::vector<const DaqRunStart*> v(accessor.access<DaqRunStart> ());

		if (v.size() > 0)
		{
			_runNumber = v[0]->runNumber();
		}
		else
		{
			std::cerr << "ERROR: DaqRunStart not found!!!" << std::endl;
			_runNumber = -1;
		}

		cout << now() <<"Run with number " << _runNumber << " started" << endl;
		_internalSpillNumber = 0;
		_nrIgnoredConfigurations = 0;
		_runStarted = true;
	}

	if (r.recordType() == RcdHeader::runStop ||
		r.recordType() == RcdHeader::runEnd)
	{
		_runStarted = false;
		cout << now() <<"Run with number " << _runNumber << " stopped (found " << _internalSpillNumber << " spills)" << endl;
		_runNumber = -1;
	}

	return _runStarted;
}

bool RcdT3B::rcdCheckIfWithinConfiguration()
{
	
	if (r.recordType() == RcdHeader::configurationStart)
	{
		// Access the DaqConfigurationStart
		std::vector<const DaqConfigurationStart*> v(accessor.access<DaqConfigurationStart> ());

		if (v.size() > 0)
		{
			// HACK HACK HACK!!!
			// in the DAQ the LED and Pedestal event maximum Number is hardcoded to 500 events,
			// except for an old DESY settings. For the actual beam this number was always higher,
			// so I use this to distinguish whether the 'spill' we take contains beamData or calibration events
			if (v[0]->maximumNumberOfEventsInConfiguration() != 500)
			{
				_beamConfiguration = true;
				cout << now() <<" next beam configuration" << endl;
			}
			else
			{
				_beamConfiguration = false;
			}
		}
		else
		{
			std::cerr << "ERROR: DaqConfigurationStart not found!!!"
					  << std::endl;
		}

	}

	if (r.recordType() == RcdHeader::configurationEnd)
	{
		if (_beamConfiguration)
		{
			cout << now() <<" beam configuration end" << endl;
			// reset the number of ignored conficurations
			_nrIgnoredConfigurations = 0;
		}
		else
		{	// this was a PED/LED configuration --> we ignored that
			_nrIgnoredConfigurations++;
		}
		_beamConfiguration = false;
	}

	return _beamConfiguration;
}


bool RcdT3B::rcdCheckIfWithinAcquisition()
{
	if (r.recordType() == RcdHeader::acquisitionStart)
	{


		// this should always be NULL, otherwise we had some interruption in communication and hence we don't trust our data any more
		if (_curSpillInfo || _beamAcquisition)
		{
			cerr << "ERROR! The last Spill (acquisition) was not ended properly! Will ignore that 'spill'" << endl;
			cout << now() <<"ERROR! The last Spill (acquisition) was not ended properly! Will ignore that 'spill'" << endl;
			delete _curSpillInfo;
		}

		_beamAcquisition = true;

		cout << now() << "  spill started: "<< setw(5) << _internalSpillNumber << endl;

		// initliaze the spillInfo
		_curSpillInfo = new SpillInfo();
		_curSpillInfo->runNumber = _runNumber;
		_curSpillInfo->internalSpillNumber = _internalSpillNumber;
		_curSpillInfo->nrEventsInAcquisition = 0;
		_curSpillInfo->nrEventsInSpill = 0;
		_curSpillInfo->nrEventsInSpillNoFrontendLine = 0;
		_curSpillInfo->nrEventsInSpillScintCoincidence = 0;
		_curSpillInfo->nrEventsInSpillOscillatorOrSoftware = 0;
		_curSpillInfo->acquisitionTimeStart.tv_sec =  r.recordTime().seconds();
		_curSpillInfo->acquisitionTimeStart.tv_usec =  r.recordTime().microseconds();

	}

	// spill end
	if (r.recordType() == RcdHeader::acquisitionEnd)
	{
		_beamAcquisition = false;

		cout << now() << setfill(' ') <<	"  spill ended:   " << setw(5) << _internalSpillNumber++;
		
		// don't count spill which are essentially empty.
		if (_curSpillInfo->nrEventsInAcquisition > 0)
		{
			_curSpillInfo->ignoredBeamConfigurations = _nrIgnoredConfigurations;
			_curSpillInfo->acquisitionTimeStop.tv_sec =  r.recordTime().seconds();
			_curSpillInfo->acquisitionTimeStop.tv_usec =  r.recordTime().microseconds();
			const unsigned DATEBUFLEN = 128;
			char timeStart[DATEBUFLEN];
			char timeStop[DATEBUFLEN];

			strftime(timeStart, DATEBUFLEN, "%Y-%m-%d %H:%M:%S", gmtime(&_curSpillInfo->firstEventTime.tv_sec));
			strftime(timeStop,  DATEBUFLEN, "%Y-%m-%d %H:%M:%S", gmtime(&_curSpillInfo->lastEventTime.tv_sec));

			cout << setfill(' ') <<
					" - Events (acq/Spill/SpillScint/SpillNoFE/SpillOsciOrSoft): " <<
					setw(5) << _curSpillInfo->nrEventsInAcquisition <<
					setw(5) << _curSpillInfo->nrEventsInSpill <<
					setw(5) << _curSpillInfo->nrEventsInSpillScintCoincidence <<
					setw(5) << _curSpillInfo->nrEventsInSpillNoFrontendLine <<
					setw(5) << _curSpillInfo->nrEventsInSpillOscillatorOrSoftware;
			cout <<	" - Time:  " <<
					timeStart << ":" << setfill('0') << setw(6) << _curSpillInfo->firstEventTime.tv_usec <<
					" ==> " <<
					timeStop << ":"  << setfill('0') << setw(6) << _curSpillInfo->lastEventTime.tv_usec <<
					setfill(' ') << endl;

			////////////
			// save the spill information to the spillList. This requires a lock on the mutex for the list
			pthread_mutex_lock(&_mutex);

			_spillInfoList.push_front(_curSpillInfo);

			// make sure we don't use to much memory
			while (_spillInfoList.size() > _maxHistoryListSize)
			{
				delete _spillInfoList.back();
				_spillInfoList.pop_back();
			}
			// unlock the mutex, so that the other thread can run again
			pthread_mutex_unlock(&_mutex);
		}
		else
		{
			cout << endl << now() << "WARNING: The current spill Information doesn't contain any entries!"  << endl;
			cerr << "WARNING: The current spill Information doesn't contain any entries!"  << endl;
		}

		// we used the current spill info
		_curSpillInfo = NULL;
	}

	return _beamAcquisition;
}



void RcdT3B::rcdCheckForEvents()
{
	// if this is an event (we are sure to be within spill and having the right config, i.e. no LED/Ped evts):
	//  check the trigger data
	if (r.recordType() == RcdHeader::event)
	{
		if (_verbose)
			cout << now() << "    event" << endl;
		std::vector<const CrcLocationData<CrcVlinkEventData>*>
			vecCrcLoc(accessor.access<CrcLocationData<CrcVlinkEventData> > ());

		// first we check if there is an event triggered by calice, and at which time it was
		_caliceEventTimes.clear();
		fillCaliceEventTimes(vecCrcLoc);

		//if (_caliceEventTimes.size() < 0)
		//	return;
		

		// now: check the different lines to see if we are inSpill, withinScintillator ...
		bool inSpill = false;
		bool in10x10Scintillator = false;
		bool inOscillator = false;
		bool inSoftwareTrigger = false;
		bool inFrontendTrigger = _caliceEventTimes.size() > 0;

		for (unsigned i(0); i < vecCrcLoc.size(); i++)
		{
			const CrcVlinkTrgData *td(vecCrcLoc[i]->data()->trgData());

			if (td)
			{
				// Check the triggers
				inSpill 			|= isInTime(td, _channelSpillTrigger, false);
				in10x10Scintillator |= isInTime(td, _channelExtScintillator10x10Trigger, false);
				inOscillator		|= isInTime(td, _channelOscillator);
				inSoftwareTrigger	|= isInTime(td, _channelSoftwareTrigger);

			}
		}

		// do the math
		_curSpillInfo->nrEventsInAcquisition++;

		if (inSpill)
		{
			if (_curSpillInfo->nrEventsInSpill == 0)
			{
				_curSpillInfo->firstEventTime.tv_sec =  r.recordTime().seconds();
				_curSpillInfo->firstEventTime.tv_usec =  r.recordTime().microseconds();
			}
			_curSpillInfo->lastEventTime.tv_sec =  r.recordTime().seconds();
			_curSpillInfo->lastEventTime.tv_usec =  r.recordTime().microseconds();
			_curSpillInfo->nrEventsInSpill++;

			if (!inFrontendTrigger)
				_curSpillInfo->nrEventsInSpillNoFrontendLine++;

			if (in10x10Scintillator)
				_curSpillInfo->nrEventsInSpillScintCoincidence++;

			if (inOscillator || inSoftwareTrigger)
				_curSpillInfo->nrEventsInSpillOscillatorOrSoftware++;
		}
	}
}

void RcdT3B::fillCaliceEventTimes(std::vector<const CrcLocationData<CrcVlinkEventData>*>& vecCrcLoc)
{
	// loop through the entire trigger history of this event, and check channel _channelGeneralTrigger (usually 19)
	for (unsigned i(0); i < vecCrcLoc.size(); i++)
	{
		const CrcVlinkTrgData *td(vecCrcLoc[i]->data()->trgData());

		if (td)
		{
			std::vector < CrcBeTrgHistoryHit > vecTriggerLineInfo(td->lineHistory(_channelFrontendTrigger));

			for (unsigned j(0); j < vecTriggerLineInfo.size(); j++)
			{
				_caliceEventTimes.push_back( pair<unsigned,unsigned>() );
				_caliceEventTimes.back().first = vecTriggerLineInfo[j].startTime();
				_caliceEventTimes.back().second = vecTriggerLineInfo[j].endTime();
			}
		}
	}
}

bool RcdT3B::isInTime(const CrcVlinkTrgData* triggerData, int lineNumber, bool strictInTime)
{
	// get the trigger info for this lineNumber
	std::vector < CrcBeTrgHistoryHit > vecTriggerLineInfo(triggerData->lineHistory(lineNumber));


	// loop over all trigger events of this channel, and over all calice frontend trigger events
	list < pair < unsigned, unsigned > >::iterator it;
	for (unsigned j(0); j < vecTriggerLineInfo.size(); j++)
	{

		// strict = it really has to be close (i.e. < _timeWindowAcc) to the caliceFrontendTrigger
		if (strictInTime)
		{
			for (it = _caliceEventTimes.begin(); it != _caliceEventTimes.end(); it++)
			{
				if (abs((int)vecTriggerLineInfo[j].startTime() - (int)(*it).first) < (int)_timeWindowAccuracy &&
					abs((int)vecTriggerLineInfo[j].endTime()  - (int)(*it).second) < (int)_timeWindowAccuracy)
					return true;
			}
		}
		else	// not so strict: this trigger channel has to start before the global trigger window ends and vice versa
		{
			if (vecTriggerLineInfo[j].startTime() < 255 &&
				vecTriggerLineInfo[j].endTime()   > 0)
				return true;
		}
	}

	return false;

}

SpillInfo* RcdT3B::getSpillInfoForTime(struct timeval* time)
{
	// the iterator for our history of spillInfos
	list < SpillInfo* >::iterator itSI;

	// _spillInfoList is sorted by time (as the events will come one after another, we rely on this)
	// so the first entry is the newest. if the request is newer than the newest request: wait (i.e. sleep), but only if
	// we wouldn't wait longer than _waitForFutureSpills seconds, otherwise we just don't have data.

	pthread_mutex_lock(&_mutex);
	
	// sanity check: if there is nothing in the list, we can do a shortcut here, skip the searching and exit directly
	if (_spillInfoList.size() == 0)
	{
		pthread_mutex_unlock(&_mutex);
		return NULL;
	}
	
	// check first entry: while the passed time is newer (i.e. has higher value) than the newest entry in the spillList: 
    //if ( (timediff(time, &_spillInfoList.front()->firstEventTime) > _waitForFutureSpills) )
	if ( (timediff(time, &_spillInfoList.front()->acquisitionTimeStart) > _waitForFutureSpills) )
	{
		cout << now() << "Request way in the future --> Ignoring" << endl;
		pthread_mutex_unlock(&_mutex);
		return NULL;
	}

	
	
	//if (_spillInfoList.size() == 0 || timediff(time, &_spillInfoList.front()->firstEventTime) > 0)
	if (_spillInfoList.size() == 0 || timediff(time, &_spillInfoList.front()->acquisitionTimeStart) > 0)
            cout << now() << "Request in the future. Will wait" << endl;
			
	//while (_spillInfoList.size() == 0 || timediff(time, &_spillInfoList.front()->firstEventTime) > 0)
	while (_spillInfoList.size() == 0 || timediff(time, &_spillInfoList.front()->acquisitionTimeStart) > 0)
	{
		pthread_mutex_unlock(&_mutex);	// unlock the list access and sleep for 100ms, so that the other thread is able to fill _spillInfoList with newer values
		usleep(100000);
		pthread_mutex_lock(&_mutex);
	}


    // a list of possible answers to this time request
    list < SpillInfo* > requestCandidates;
    
	// no we know for sure that the requested spill is available in the list (unless the request is too old)
	SpillInfo* requestedSpillInfo = NULL;
	for (itSI = _spillInfoList.begin(); itSI != _spillInfoList.end(); itSI++)
	{
		/*      1stEvt      LastEvt
		           _________
		__________|         |________
		    |<--->|         |<--->|
		     TmAcc           TmAcc		--> TmAcc == _spillTimeAccuracy
		    |<------------------->|
		   The request should be here
		*/
		//double diffTimeStart = timediff(time, &(*itSI)->firstEventTime) + _spillTimeAccuracy; 	// time - (firstEventTime - _spillTimeAccuracy)
		//double diffTimeStop  = timediff(time, &(*itSI)->lastEventTime) - _spillTimeAccuracy;	// time - (lastEventTime + _spillTimeAccuracy)
		
		double diffTimeStart = timediff(time, &(*itSI)->acquisitionTimeStart) + _spillTimeAccuracy; 	// time - (firstEventTime - _spillTimeAccuracy)
		double diffTimeStop  = timediff(time, &(*itSI)->acquisitionTimeStop) - _spillTimeAccuracy;	// time - (lastEventTime + _spillTimeAccuracy)

		if (diffTimeStart > 0 && 0 > diffTimeStop)
		{
            cout << "For time " << time->tv_sec << " we found " << (*itSI)->acquisitionTimeStart.tv_sec << " -> " << (*itSI)->acquisitionTimeStop.tv_sec << endl;
			//requestedSpillInfo = (*itSI);
			//break;
            requestCandidates.push_back(*itSI);
		}
	}
    
	pthread_mutex_unlock(&_mutex);

    // loop over the candidates and choose the best one, depending on the time distance to the lastEventTime (squared)
    double minDiff = DBL_MAX;
    for (itSI = requestCandidates.begin(); itSI != requestCandidates.end(); itSI++)
    {
        double diff = timediff(time, &(*itSI)->lastEventTime);
        diff *= diff;
        
        if (diff < minDiff)
        {
            minDiff = diff;
            requestedSpillInfo = (*itSI);
        }
    }
    
    if (requestedSpillInfo)
        cout << "Using " 
             << requestedSpillInfo->acquisitionTimeStart.tv_sec << " -> " << requestedSpillInfo->firstEventTime.tv_sec << " -> "
             << requestedSpillInfo->lastEventTime.tv_sec << " -> " << requestedSpillInfo->acquisitionTimeStop.tv_sec 
             << endl;
    
	return requestedSpillInfo;
}


//void* RcdT3B::listenT3B(void * c) 
void* listenT3B(void * c) 
{
	// use the RcdT3B class from above
	RcdT3B* t = (RcdT3B*) c;
	
	int portNr = t->getT3BListenPort();

	// this is C! Nice, huh? I'd love if I could use something like Qt ...
	// the ip address of me and the sender
	struct sockaddr_in addrMe, addrOther;
	// the handle to our local socket
	int sockHandle = 0;
	// helper variable
//	int slen = sizeof(addrOther);
	socklen_t slen = sizeof(addrOther);
	// the buffer which will be used for recieving
	const unsigned BUF_SIZE(512);
	char buf[BUF_SIZE];

	// initialize my IP address with the specified port, but bind to any ip address
	memset((char *) &addrMe, 0, sizeof(addrMe));
	addrMe.sin_family = AF_INET;
	addrMe.sin_port = htons(portNr);
	addrMe.sin_addr.s_addr = htonl(INADDR_ANY);

	while (true)
	{	
		// clean up the renmants from the last round if necessary
		if (sockHandle)
		{
			cout << now() << "Closing socket from last try" << endl;
			close(sockHandle);
		}

		// create the socket
		cout << now() << "Creating UDP socket" << endl;
		if ((sockHandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
		{
			cerr << "ERROR while creating socket. Will try again." << endl;
			continue;
		}

		// bind it to local port
		cout << now() << "Binding socket to UDP Port " << portNr << endl;
		if (bind(sockHandle, (struct sockaddr *)&addrMe, sizeof(addrMe)) == -1)
		{
			cerr << "ERROR while binding socket. Will try again." << endl;
			continue;			
		}

		// the reciever loop. If something fails: break out of it, and reopen udp port
		while (true)
		{
			// recieve the packet
			int bufSize = recvfrom(sockHandle, buf, BUF_SIZE, 0, (struct sockaddr *)&addrOther, &slen);

			if (bufSize == -1)
			{
				cerr << "ERROR while recieving UDP packet" << endl;
				break;
			}
			else
			{
				// deserialize the  packets. it should be
				// 4 bytes -> requestID
				// 4 bytes -> seconds (linux time_t)
				// 4 bytes -> microseconds
				unsigned requestID = deserializeU(&buf[0]);

				struct timeval requestedTime;
				requestedTime.tv_sec = deserializeU(&buf[sizeof(unsigned)]);
				if (bufSize >= 3*(int)sizeof(unsigned))
				{
					requestedTime.tv_usec = deserializeU(&buf[2*sizeof(unsigned)]);
				}
				else
				{
					requestedTime.tv_usec = 0;
				}

				const unsigned DATEBUFLEN = 128;
				char timeStr[DATEBUFLEN];

				strftime(timeStr, DATEBUFLEN, "%Y-%m-%d %X", gmtime(&requestedTime.tv_sec));
				
				char senderIP[DATEBUFLEN];
				inet_ntop(AF_INET, &(addrOther.sin_addr), senderIP, DATEBUFLEN);


				cout << now() << "Recieved request " << requestID << 
						" from " << senderIP << ":" << ntohs(addrOther.sin_port) <<
						" for time (s/us): " << requestedTime.tv_sec << " / " << requestedTime.tv_usec <<
						" (" << timeStr << ")" <<
						endl;

				// search for this request
				SpillInfo* s = t->getSpillInfoForTime(&requestedTime);
				int size = 0;

				if (s == NULL)
				{
					cerr << "WARNING: No data found. " << endl;
					cout << now() << "WARNING: No data found. " << endl;
					size = sizeof(unsigned);
				}
				else
				{
					//struct SpillInfo
					//{
					//	unsigned		runNumber;
					//	unsigned		internalSpillNumber;
					//	unsigned		ignoredBeamConfigurations;

					//	struct timeval	acquisitionTimeStart;
					//	struct timeval	acquisitionTimeStop;
					//	struct timeval	firstEventTime;
					//	struct timeval	lastEventTime;

					//	unsigned		nrEventsInAcquisition;
					//	unsigned		nrEventsInSpill;
					//	unsigned		nrEventsInSpillNoFrontendLine;
					//	unsigned		nrEventsInSpillScintCoincidence;
					//	unsigned		nrEventsInSpillOscillatorOrSoftware;
					//	
					//};
					serialize(s->runNumber								, &buf[size += sizeof(unsigned)]);
					serialize(s->internalSpillNumber					, &buf[size += sizeof(unsigned)]);
					serialize(s->ignoredBeamConfigurations				, &buf[size += sizeof(unsigned)]);
					serialize(s->acquisitionTimeStart.tv_sec			, &buf[size += sizeof(unsigned)]);
					serialize(s->acquisitionTimeStart.tv_usec			, &buf[size += sizeof(unsigned)]);
					serialize(s->acquisitionTimeStop.tv_sec				, &buf[size += sizeof(unsigned)]);
					serialize(s->acquisitionTimeStop.tv_usec			, &buf[size += sizeof(unsigned)]);
					serialize(s->firstEventTime.tv_sec					, &buf[size += sizeof(unsigned)]);
					serialize(s->firstEventTime.tv_usec					, &buf[size += sizeof(unsigned)]);
					serialize(s->lastEventTime.tv_sec					, &buf[size += sizeof(unsigned)]);
					serialize(s->lastEventTime.tv_usec					, &buf[size += sizeof(unsigned)]);
					serialize(s->nrEventsInAcquisition					, &buf[size += sizeof(unsigned)]);
					serialize(s->nrEventsInSpill						, &buf[size += sizeof(unsigned)]);
					serialize(s->nrEventsInSpillNoFrontendLine			, &buf[size += sizeof(unsigned)]);
					serialize(s->nrEventsInSpillScintCoincidence		, &buf[size += sizeof(unsigned)]);
					serialize(s->nrEventsInSpillOscillatorOrSoftware	, &buf[size += sizeof(unsigned)]);
					size += sizeof(unsigned);

				}

				if (sendto(sockHandle, buf, size, 0, (struct sockaddr *)&addrOther, slen) == -1)
				{
					cerr << "ERROR while processing request " << requestID << endl;
					break;
				}
				else
				{
					cout << now() << "Processing of request " << requestID << " complete" << endl;
				}
			}
		}	
	}

	close(sockHandle);

	return NULL;
}




double timediff(struct timeval* a, struct timeval* b)
{
	double secDiff = difftime(a->tv_sec, b->tv_sec);
	double uSecDiff = difftime(a->tv_usec, b->tv_usec);

	while (uSecDiff < 0)
	{
		secDiff--;
		uSecDiff += 1e6;
	}

	return  secDiff + uSecDiff/1e6;
}
