#include <iostream>
#include <fstream>
#include <sstream>

#include "TApplication.h"
#include "TSystem.h"
#include "TCanvas.h"

// dual/inc/utl
#include "UtlArguments.hh"

// dual/inc/rcd
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

// dual/inc/chk
#include "FixEmc.hh"
#include "FixAhc.hh"
#include "ChkCount.hh"
#include "HstGeneric.hh"
#include "HstSingleChannel.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const bool useReadAsc(argh.option('a',"Ascii input file"));
  const bool slwRead(argh.option('s',"Slow data input file"));
  const unsigned printLevel(argh.optionArgument('p',0,"Print level"));
  const unsigned numberOfRecords(argh.optionArgument('r',0xffffffff,
						     "Number of records"));
  const unsigned runNumber(argh.lastArgument(999999));

  const unsigned sleepMs(1000);

  if(argh.help()) return 0;

  if(useReadAsc) cout << "Ascii input file selected" << endl;
  else           cout << "Binary input file selected" << endl;
  if(slwRead) cout << "Normal data file selected" << endl;
  else        cout << "Slow data file selected" << endl;
  cout << "Print level set to " << printLevel << endl;
  cout << "Number of records set to " << numberOfRecords << endl;
  cout << "Run number set to " << runNumber << endl;
  cout << endl;

  RunReader reader;
  assert(reader.open(runNumber));

  TApplication application("HstOfflineApplication",0,0);
  gROOT->Reset();

  TCanvas hstHistCanvas("HstCrcSignalCanvas","HstCrcSignal",10,10,710,510);

  ChkCount hn;
  HstSingleChannel hp(1,12,3,5,6);

  RcdArena &arena(*(new RcdArena));
  unsigned iRecords(0);
  while(reader.read(arena) && iRecords<numberOfRecords) {
    iRecords++;

    assert(hn.record(arena));
    assert(hp.record(arena));
  }
  
  assert(reader.close());

  while(!gSystem->ProcessEvents()) gSystem->Sleep(sleepMs);

  return 0;
}
