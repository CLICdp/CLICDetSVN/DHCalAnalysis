#include <sstream>

#include "TH1F.h"

#include "HstRecordStore.hh"
#include "HstCrcNoiseStore.hh"
#include "HstCaen767Store.hh"
#include "HstBeTrgHistoryStore.hh"
#include "HstCrcChannelStore.hh"
#include "ShmObject.hh"
#include "UtlArguments.hh"

using namespace std;


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);

  const std::string shmName(argh.optionArgument('n',"HstCrcNoise","Shared memory name"));

  unsigned shmKey(0);
  if(shmName=="HstRecord") shmKey=HstRecordStore::shmKey;
  if(shmName=="HstCrcNoise") shmKey=HstCrcNoiseStore::shmKey;
  if(shmName=="HstBeTrgHistory") shmKey=HstBeTrgHistoryStore::shmKey;
  if(shmName=="HstCaen767") shmKey=HstCaen767Store::shmKey;
  if(shmName=="HstCrcChannel") shmKey=HstCrcChannelStore::shmKey;

  if(shmKey==0) {
    std::cerr << "Shared memory name unrecognised" << std::endl;
    return 1;
  }

  ShmObject<HstFlags> _shmHstFlags(shmKey);
  HstFlags *_pShm(_shmHstFlags.payload());

  if(_pShm==0) {
    std::cerr << "Cannot connect to shared memory" << std::endl;
    return 2;
  }

  std::ostringstream sout;
  sout << "Reset level ("
       << HstFlags::job << "=never, "
       << HstFlags::run << "=run, "
       << HstFlags::configuration << "=cfg, "
       << HstFlags::acquisition << "=acq, "
       << HstFlags::event << "=evt)";
    
  const unsigned resetLevel(argh.optionArgument('l',_pShm->_resetLevel,sout.str()));
  const unsigned resetNow(argh.option('r',false,"Reset now"));
			  
  if(argh.help()) return 0;

  _pShm->_resetLevel=(HstFlags::Level)resetLevel;
  _pShm->_resetNow=resetNow;

  return 0;
}
