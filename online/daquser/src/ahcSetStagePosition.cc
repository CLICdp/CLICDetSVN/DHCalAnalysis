#define AHC_SLOW_DISABLED

#include "runnerDefine.icc"

#include <iostream>

#include "RcdArena.hh"
#include "SubInserter.hh"

#include "AhcSlowReadout.hh"

using namespace std;

int main(int argc, const char **argv) {
  if(argc!=3) {
    cout << "Usage: " << argv[0] << " <x in mm> <y in mm>" << endl;
    return 1;
  }

  AhcSlowReadout asr(AHC_STAGE_SKT);
  asr.printLevel(99);

  // Define record memory
  RcdArena &arena(*(new RcdArena));
  arena.initialise(RcdHeader::configurationStart);

  SubInserter inserter(arena);
  //AhcSlowControlData *d(inserter.insert<AhcSlowControlData>(true));
  AhcSlowConfigurationData *d(inserter.insert<AhcSlowConfigurationData>(true));

  std::istringstream sinx(argv[1]);
  std::istringstream siny(argv[2]);
  double x,y;
  sinx >> x;
  siny >> y;

  d->mmXPosition(x);
  d->mmYPosition(y);
  d->print(std::cout);

  asr.record(arena);

  return 0;
}
