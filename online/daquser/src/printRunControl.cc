#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "DaqRunType.hh"
#include "RunControl.hh"
#include "ShmObject.hh"

using namespace std;

int main(int argc, const char **argv) {

  ShmObject<RunControl> shmRunControl(RunControl::shmKey);
  RunControl *pRc(shmRunControl.payload());
  if(pRc==0) return 1;

  pRc->print(std::cout);

  return 0;
}
