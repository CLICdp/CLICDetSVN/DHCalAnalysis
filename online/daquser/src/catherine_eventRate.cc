#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlArguments.hh"

#include "DaqRunStart.hh"
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"

#include "HstEventRate.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const bool batch(argh.option('b',"Select batch mode for ROOT"));
  const bool useReadAsc(argh.option('a',"Ascii input file"));

  const unsigned vnum(argh.lastArgument(999999));
  std::ostringstream run;
  run << vnum;


  if(argh.help()) return 0;

  if(batch) cout << "ROOT batch mode selected" << endl;
  else      cout << "ROOT interactive mode selected" << endl;

  RcdArena &arena(*(new RcdArena));
  RcdReader *reader(0);
  if(useReadAsc){
    reader=new RcdReaderAsc();
    cout << "Reading an ascii file" << endl;
  }
  else{
    reader=new RcdReaderBin();
    cout << "Reading a binary file" << endl;
  }

  HstEventRate *hr=new HstEventRate(!batch);
  
  std::ostringstream sout;
  sout << "./data/sum/Run" << run.str() << "_eventRate.ps";
 
  std::ostringstream sout2;
  sout2 << std::string("./data/dat/Run")+run.str();
 
  unsigned count(0);
  reader->open(sout2.str());  
  while(reader->read(arena)) {
    if(hr!=0) hr->record(arena);
    count++;
  }
  reader->close();

  hr->update();
  if(hr!=0) {
    hr->postscript(sout.str());
    delete hr;
  }
  
  delete reader;

}
