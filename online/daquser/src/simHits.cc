#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>

#include "UtlTime.hh"
#include "UtlArguments.hh"

#include "EmcMap.hh"
#include "EmcCalibration.hh"
#include "EmcAlignment.hh"
#include "EmcEventEnergy.hh"
#include "EmcEventAdc.hh"

#include "SimEmcHitsToEnergy.hh"
#include "SimEmcEnergyToAdc.hh"
#include "LcoEmcConverter.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const unsigned rn(1234567890);
  const unsigned me(10);
  
  // Output file; should be LCIO!!!
  std::ostringstream sout;
  sout << "dat/Run" << rn << ".lcio";
  std::ofstream fout(sout.str().c_str());
  
  bool noiseOnly(rn==1234567890);
  
  EmcAlignment al;
  EmcCalibration cl;
  std::ostringstream sout4;
  sout4 << "cal/Cal" << rn << ".txt";
  assert(cl.read(sout4.str()));
  //cl.print(cout);
  
  EmcEventEnergy energy;
  EmcEventAdc adc;
  SimEmcHitsToEnergy h2e;
  h2e.hitsDirectory("/vols/calice/calice/simData/10000_TestBeam_30_0_30_20GeV_electrons");
  SimEmcEnergyToAdc e2a(cl);
  LcoEmcConverter a2l(cl,al);
  
  for(unsigned event(0);event<me;event++) {
    h2e.event(event,energy);
    e2a.event(energy,adc);
    fout << "Run " << rn << " Event " << event << std::endl;
    a2l.event(adc,fout);
  }
}
