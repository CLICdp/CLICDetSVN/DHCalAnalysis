#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdWriterAsc.hh"

#include "DaqRunStart.hh"
#include "RcdReaderAsc.hh"

#include "SubAccessor.hh"

#include "HstExtDac.hh"

using namespace std;

bool continueJob=true;

void signalHandler(int signal) {
  std::cout << "Process " << getpid() << " received signal "
	    << signal << std::endl;
  continueJob=false;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  bool doGraphics(true);
  //include "arguments.icc"

  const unsigned vnum(argh.lastArgument(999999));
  ostringstream sout;
  sout << vnum;

  RcdArena &arena(*(new RcdArena));
  RcdReaderAsc reader;

  unsigned slot(18); // SER003
  //unsigned slot(14); // SER004
  HstExtDac *hn(0);
  if(doGraphics) hn=new HstExtDac(slot);

  //reader.open("dat/Run1099819725"); // SER003
  //reader.open("dat/Run1099840335"); // SER001
  //reader.open("dat/Run1099915737"); // SER004

  //reader.open(std::string("dat/Run")+sout.str());

  unsigned x(0);
  for(unsigned i(0);i<24;i++) {
    // SER003

    if(i== 0) reader.open("dat/Run1100853277");
    if(i== 1) reader.open("dat/Run1100852944");
    if(i== 2) reader.open("dat/Run1100853509");
    if(i== 3) reader.open("dat/Run1100853863");
    if(i== 4) reader.open("dat/Run1100853993");
    if(i== 5) reader.open("dat/Run1100855071");
    if(i== 6) reader.open("dat/Run1100855332");
    if(i== 7) reader.open("dat/Run1100855893");
    if(i== 8) reader.open("dat/Run1100856126");
    if(i== 9) reader.open("dat/Run1100856685");
    if(i==10) reader.open("dat/Run1100856812");
    if(i==11) reader.open("dat/Run1100857157");
    if(i==12) reader.open("dat/Run1100853163");
    if(i==13) reader.open("dat/Run1100853055");
    if(i==14) reader.open("dat/Run1100853616");
    if(i==15) reader.open("dat/Run1100853722");
    if(i==16) reader.open("dat/Run1100854116");
    if(i==17) reader.open("dat/Run1100854765");
    if(i==18) reader.open("dat/Run1100855529");
    if(i==19) reader.open("dat/Run1100855711");
    if(i==20) reader.open("dat/Run1100856271");
    if(i==21) reader.open("dat/Run1100856477");
    if(i==22) reader.open("dat/Run1100856909");
    if(i==23) reader.open("dat/Run1100857038");

    // SER004
    /*
    if(i== 0) reader.open("dat/Run1100790747");
    if(i== 1) reader.open("dat/Run1100790950");
    if(i== 2) reader.open("dat/Run1100791141");
    if(i== 3) reader.open("dat/Run1100791313");
    if(i== 4) reader.open("dat/Run1100791540");
    if(i== 5) reader.open("dat/Run1100791641");
    if(i== 6) reader.open("dat/Run1100791801");
    if(i== 7) reader.open("dat/Run1100791936");
    if(i== 8) reader.open("dat/Run1100792110");
    if(i== 9) reader.open("dat/Run1100792330");
    if(i==10) reader.open("dat/Run1100792496");
    if(i==11) reader.open("dat/Run1100792645");
    if(i==12) reader.open("dat/Run1100792853");
    if(i==13) reader.open("dat/Run1100793038");
    if(i==14) reader.open("dat/Run1100793274");
    if(i==15) reader.open("dat/Run1100793483");
    if(i==16) reader.open("dat/Run1100793687");
    if(i==17) reader.open("dat/Run1100793860");
    if(i==18) reader.open("dat/Run1100794027");
    if(i==19) reader.open("dat/Run1100794225");
    if(i==20) reader.open("dat/Run1100794590");
    if(i==21) reader.open("dat/Run1100794796");
    if(i==22) reader.open("dat/Run1100795017");
    if(i==23) reader.open("dat/Run1100795180");
    */

    while(reader.read(arena)) {
      if((x%100)==0) std::cout << "Record " << x << std::endl;
      x++;
      if(hn!=0) hn->event(arena);
    }
    
    
    reader.close();
  }

  //if(hn!=0) hn->postscript("dps/Run1099819725.ps");
  //if(hn!=0) hn->postscript("dps/Run1099840335.ps");
  //if(hn!=0) hn->postscript("dps/Run1099915737.ps");

  sout << "Slot" << slot << ".ps";
  //if(hn!=0) hn->postscript(std::string("dps/Run")+sout.str());
  if(hn!=0) hn->update(true);
  if(hn!=0) hn->postscript("dps/extDac.ps");
  if(hn!=0) delete hn;
}
