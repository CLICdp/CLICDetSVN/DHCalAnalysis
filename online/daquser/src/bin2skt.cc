#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RunReader.hh"
#include "RcdIoSkt.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const std::string ip(argh.optionArgument('i',"localhost","IP address"));
  const unsigned runNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  RcdArena &arena(*(new RcdArena));
  RunReader reader;
  RcdIoSkt writer;

  reader.open(runNumber);
  writer.open(ip,1124,100);

  while(reader.read(arena) && writer.write(arena));

  arena.initialise(RcdHeader::shutDown);
  writer.write(arena);

  reader.close();
  writer.close();
}
