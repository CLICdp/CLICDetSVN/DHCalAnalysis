#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <fstream>
#include <iostream>

#include "UtlTime.hh"
#include "UtlArguments.hh"

bool continueJob=true;

void signalHandler(int signal) {
  std::cerr << "Process " << getpid() << " received signal "
            << signal << std::endl;
  continueJob=false;
}

int main(int argc, const char *argv[]) {
  UtlArguments argh(argc,argv);
  const unsigned sleepSecs(argh.optionArgument('s',0,"Sleep period (secs), 0 = do once"));
  if(argh.help()) return 0;

  // Set up signal handling
  signal(SIGINT,signalHandler);
  signal(SIGTERM,signalHandler);

  // Loop until interrupted
  while(continueJob) {

    std::ifstream fin("data/eudetRunNumber.txt");
    if(!fin) {
      std::cerr << "Cannot open file data/eudetRunNumber.txt" << std::endl;
      return 1;
    }
    
    unsigned n;
    int t,u;
    fin >> n >> t >> u;
    if(!fin) {
      std::cerr << "Cannot read file data/eudetRunNumber.txt" << std::endl;
      return 2;
    }

    UtlTime rTime(t,u);
    std::cout << "EUDET run number = " << n 
	      << ", started at " << rTime << std::endl;
    
    if(sleepSecs>0) sleep(sleepSecs);
    else continueJob=false;
  }

  return 0;
}
