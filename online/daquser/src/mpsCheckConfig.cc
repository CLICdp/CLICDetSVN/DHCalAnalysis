#include "runnerDefine.icc"

#include <iostream>
#include <sstream>
#include <vector>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdCount.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

using namespace std;


int main(int argc, const char **argv) {

  // Get run number as last (=only) argument
  UtlArguments argh(argc,argv);
  const unsigned numberOfRuns(argh.optionArgument('n',1,"Number of runs"));
  const unsigned runnum(argh.lastArgument(999999,"Run number"));

  if(argh.help()) return 0;

  // Define memory space for records
  RcdArena &arena(*(new RcdArena));
  RcdHeader lastHeader;

  // This can handle both single-file and multi-file runs
  RunReader reader;
  bool runExists(true);

  IlcRunStart runStart;

  // Storage for configs
  std::vector< MpsLocationData<MpsSensorV12ConfigurationData> > vConfig;

  for(unsigned n(0);n<numberOfRuns && runExists;n++) {

    // Open run file using reader
    runExists=reader.open(runnum+n);
    if(runExists) {

      // Check for run end
      bool runEndSeen(false);

      // Open output file
      std::ostringstream sout;
      sout << "configCheck/Run" << std::setfill('0') << std::setw(6) << runnum+n << ".txt";
      std::ofstream fout(sout.str().c_str());
      assert(fout);

      // Create simple record type counter
      RcdCount counter;
      
      // Loop over all records until end-of-file
      while(reader.read(arena)) {
	
	// Increment counter
	counter+=arena;
	
	// Select to look at run start
	if(arena.recordType()==RcdHeader::runStart) {
	  arena.RcdHeader::print(fout) << endl;

	  SubAccessor accessor(arena);    
	  
	  std::vector<const IlcRunStart*>
	    vs(accessor.access<IlcRunStart>());
	  assert(vs.size()==1);
	  runStart=*(vs[0]);
	  runStart.print(fout) << std::endl;
	  
	  std::vector<const MpsLocationData<MpsUsbDaqRunData>*>
	    v(accessor.access<MpsLocationData<MpsUsbDaqRunData> >());
	  
	  vConfig.clear();
	  for(unsigned i(0);i<v.size();i++) {
	    if(!v[i]->usbDaqMasterFirmware()) {
	      v[i]->print(fout) << std::endl;

	      MpsLocationData<MpsSensorV12ConfigurationData> d;
	      d.location(v[i]->location());
	      d.data()->maskSensor(false);
	      d.data()->trimSensor(0);
	      vConfig.push_back(d);
	    }
	  }
	  
	  for(unsigned i(0);i<vConfig.size();i++) {
	    //vConfig[i].print() << std::endl;
	  }
	}
	
	if(arena.recordType()==RcdHeader::runEnd) {
	  arena.RcdHeader::print(fout) << endl;
	  runEndSeen=true;

	  SubAccessor accessor(arena);    
	  std::vector<const MpsLocationData<MpsSensorV12ConfigurationData>*>
	    v(accessor.access<MpsLocationData<MpsSensorV12ConfigurationData> >());

	  for(unsigned i(0);i<v.size();i++) {
	    //v[i]->print(std::cout) << std::endl;
	    assert(!v[i]->write());

	    unsigned j(999);
	    for(unsigned k(0);k<vConfig.size();k++) {
	      if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
	    }
	    assert(j!=999);

	    unsigned nError(0);
	    for(unsigned x(0);x<168;x++) {
	      for(unsigned y(0);y<168;y++) {

		// Exclude known bad configuration columns
		if((v[i]->sensorId()==26 && x==134) ||
		   (v[i]->sensorId()==26 && x==137) ||
		   (v[i]->sensorId()==26 && x==139) ||
		   (v[i]->sensorId()==26 && x==148) ||
		   (v[i]->sensorId()==26 && x==163) ||
		   (v[i]->sensorId()==32 && x==128) ||
		   (v[i]->sensorId()==39 && x==129) ||
		   (v[i]->sensorId()==41 && x==117) ||
		   (v[i]->sensorId()==41 && x==129) ||
		   (v[i]->sensorId()==41 && x==136) ||
		   (v[i]->sensorId()==41 && x==154)) {

		} else {
		  if(vConfig[j].data()->trim(x,y)!=v[i]->data()->trim(x,y) ||
		     vConfig[j].data()->mask(x,y)!=v[i]->data()->mask(x,y)) nError++;
		}
	      }
	    }

	    if(nError>0) {
	      cerr << std::endl
		   << "ERROR Run " << runStart.runNumber()
		   << " sensor " << (unsigned)v[i]->sensorId()
		   << " runEnd number of errors " << nError << std::endl;
	      fout << std::endl
		   << "ERROR Run " << runStart.runNumber()
		   << " sensor " << (unsigned)v[i]->sensorId()
		   << " runEnd number of errors " << nError << std::endl;
	      vConfig[j].print(fout,"WRITE ") << std::endl;
	      v[i]->print(fout,"READ  ") << std::endl;
	      v[i]->data()->diff(*(vConfig[j].data()),fout);
	    }
	  }
	}
	
	if(arena.recordType()==RcdHeader::configurationStart) {
	  arena.RcdHeader::print(fout) << endl;

	  SubAccessor accessor(arena);    
	  std::vector<const MpsLocationData<MpsSensorV12ConfigurationData>*>
	    v(accessor.access<MpsLocationData<MpsSensorV12ConfigurationData> >());

	  for(unsigned i(0);i<v.size();i++) {
	    //v[i]->print(std::cout) << std::endl;

	    if(!v[i]->write()) {
	      unsigned j(999);
	      for(unsigned k(0);k<vConfig.size();k++) {
		if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
	      }
	      assert(j!=999);

	      unsigned nError(0);
	      for(unsigned x(0);x<168;x++) {
		for(unsigned y(0);y<168;y++) {

		  // Exclude known bad configuration columns
		  if((v[i]->sensorId()==26 && x==134) ||
		     (v[i]->sensorId()==32 && x==128) ||
		     (v[i]->sensorId()==41 && x==117) ||
		     (v[i]->sensorId()==41 && x==129)) {
		    
		  } else {
		    if(vConfig[j].data()->trim(x,y)!=v[i]->data()->trim(x,y)) nError++;
		  }
		}
	      }

	      if(nError>0) {
		cerr << std::endl
		     << "ERROR Run " << runStart.runNumber()
		     << " sensor " << (unsigned)v[i]->sensorId()
		     << " configurationStart number of errors " << nError << std::endl;
		fout << std::endl
		     << "ERROR Run " << runStart.runNumber()
		     << " sensor " << (unsigned)v[i]->sensorId()
		     << " configurationStart number of errors " << nError << std::endl;
		vConfig[j].print(fout,"WRITE ") << std::endl;
		v[i]->print(fout,"READ  ") << std::endl;
	      }
	    }
	  }

	  for(unsigned i(0);i<v.size();i++) {
	    if(v[i]->write()) {
	      if(v[i]->sensorBroadcast()) {
		for(unsigned k(0);k<vConfig.size();k++) {
		  vConfig[k].data(*(v[i]->data()));
		}
	      }
	    }
	  }
	  for(unsigned i(0);i<v.size();i++) {
	    if(v[i]->write()) {
	      if(!v[i]->sensorBroadcast()) {

		unsigned j(999);
		for(unsigned k(0);k<vConfig.size();k++) {
		  if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
		}
		assert(j!=999);
		vConfig[j].data(*(v[i]->data()));
	      }
	    }
	  }

	  for(unsigned i(0);i<vConfig.size();i++) {
	    //vConfig[i].print() << std::endl;
	  }
	}
      }

      if(!runEndSeen) {
	std::cerr << std::endl
		  << "ERROR Run " << runStart.runNumber()
		  << " no end of run config check" << std::endl;
	fout << std::endl
	     << "ERROR Run " << runStart.runNumber()
	     << " no end of run config check" << std::endl;
      }
      
      // Print total of counts
      cout << endl;
      counter.print(fout);
      
      // Close run file
      assert(reader.close());
    }
  }

  return 0;
}
