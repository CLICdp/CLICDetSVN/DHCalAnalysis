#include <signal.h>

#include <iostream>
#include <sstream>
#include <vector>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdCount.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

#include "SimClustering.hh"
#include "SimAnalysis.hh"

using namespace std;


bool continueJob=true;

void signalHandler(int signal) {
  std::cerr << "Process " << getpid() << " received signal "
            << signal << std::endl;
  continueJob=false;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned numberOfEvents(argh.optionArgument('e',1000000000,"Number of events"));
  const unsigned numberOfRuns(argh.optionArgument('n',1,"Number of runs"));
  const unsigned   printLevel(argh.optionArgument('p',9,"Print level"));
  const unsigned analysisBits(argh.optionArgument('a',SimAnalysis::defaultSimAnalysisBits,"SimAnalysis bits"));

  // Get run number as last argument
  const unsigned runnum(argh.lastArgument(999999,"Run number"));

  if(argh.help()) return 0;

  time_t eTime(CALICE_DAQ_TIME);
  cout << argv[0] << " compiled at   CALICE_DAQ_TIME = " << ctime(&eTime);
  cout << argv[0] << " compiled with CALICE_DAQ_SIZE = " << CALICE_DAQ_SIZE <<std::endl;

  // Allow interrupts
  signal(SIGINT,signalHandler);
  signal(SIGTERM,signalHandler);

  // Define memory space for records
  RcdArena &arena(*(new RcdArena));

  // Create simple record type counter and configuration counter
  RcdCount counter;
  unsigned nEvt(0);

  // Create modules
  //SimClustering clustering;
  //clustering.printLevel(printLevel);

  SimAnalysis analysis(analysisBits);
  analysis.printLevel(printLevel);
  
  // This can handle both single-file and multi-file runs
  RunReader reader;

  for(unsigned run(0);run<numberOfRuns;run++) {

    // Open run file using reader
    assert(reader.open(runnum+run));

    // Loop over all records until end-of-file
    while(reader.read(arena) && continueJob && nEvt<numberOfEvents) {

      // Increment counters
      counter+=arena;
      if(arena.recordType()==RcdHeader::event ||
	 arena.recordType()==RcdHeader::bunchTrain) nEvt++;

      // Run clustering
      //clustering.record(arena);

      // Run analysis
      analysis.record(arena);
    }
    
    // Close run file
    assert(reader.close());
  }
    
  // Print total of counts
  cout << endl;
  counter.print(cout);

  return 0;
}
