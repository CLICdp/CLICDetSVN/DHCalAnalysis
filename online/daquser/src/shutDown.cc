#include "runnerDefine.icc"

#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "DaqRunType.hh"
#include "RunControl.hh"
#include "ShmObject.hh"

using namespace std;

int main(int argc, const char **argv) {

  //unsigned eTime(CALICE_DAQ_TIME);
  //cout << argv[0] << " compiled at " << ctime((const time_t*)&eTime);
 
  //UtlArguments argh(argc,argv);
  //if(argh.help()) return 0;
   
  ShmObject<RunControl> shmRunControl(RunControl::shmKey);
  RunControl *pRc(shmRunControl.payload());
  if(pRc==0) return 1;

  pRc->flag(RunControl::shutDown);
  //pRc->runInterrupt(SIGTERM);

  return 0;
}
