//#include <sys/types.h>
//#include <sys/stat.h>
//#include <signal.h>
//#include <fcntl.h>
//#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <map>
//#include <cstdio>

//#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

#include "mappingFromFile.hh"




using namespace std;


RcdHeader runHeader;
AhcMapping mapping;

std::string _printmode;
std::ostringstream _output;
bool _extended;

void print(std::string header, std::string text, bool resetLine=false) {
  if (header != _printmode) std::cout << std::endl << header << " ";
  _printmode = header;
  if (resetLine) std::cout << "\r"  << header << " ";
  std::cout << text << " " << std::flush;
}

void print(std::string header, long number, bool resetLine=false) {
  std::ostringstream stream;
  stream << number;
  if (!resetLine) stream << ",";
  print(header,stream.str(),resetLine);
}


void processData(const RcdRecord &arena) {

  if(arena.recordType()==RcdHeader::runStart) {
    runHeader=arena;
    //    runHeader.print(std::cout) << std::endl;
  }

  SubAccessor accessor(arena);


  std::vector<const DaqRunStart*>
    v(accessor.access<DaqRunStart >());
  
  if(v.size()>0) {
    //    arena.RcdHeader::print(std::cout) << std::endl;
    for(unsigned i(0);i<v.size();i++) {
      //v[i]->print(std::cout," ") << std::endl;
      DaqRunType type = v[i]->runType();
      _output << "|"<<v[i]->runNumber() << "|" <<  type.typeName() << "|" << (unsigned)(type.version());
      if (!_extended) {
	std::cout << _output.str() << std::endl;
	exit(type.type()); 
      }
    }

  }

  if (_extended) {
    std::vector<const DaqRunEnd*>
      vE(accessor.access<DaqRunEnd >());
    if (vE.size() > 0) {
      for (unsigned i=0;i<vE.size();i++) {
	std::cout << _output.str() << "|" << vE[i]->actualNumberOfConfigurationsInRun() << "|" << vE[i]->actualNumberOfEventsInRun() << "|" <<std::setprecision(3) <<vE[i]->actualNumberOfEventsInRun()/(vE[i]->actualTimeOfRun().deltaTime()) << " Hz" << std::endl;
	exit(vE[i]->runType().type());
      }
    }
  }
}

int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const bool extended(argh.optionArgument('e',false,"extended info, reads run summary info but needs longer"));
  const unsigned vnum(argh.lastArgument(999999));

  _printmode = "";
  _extended = extended;
  RcdArena arena;

  RunReader reader;
  assert(reader.open(vnum));

  while(reader.read(arena)) processData(arena);
  assert(reader.close());
  
  std::cout <<_output.str()<< "|broken end of run" <<std::endl;

  return 0;
}
