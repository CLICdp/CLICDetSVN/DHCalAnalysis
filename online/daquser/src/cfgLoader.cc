#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlArguments.hh"
#include "UsbDaqDevice.hh"

using namespace std;

int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const unsigned sensorId(argh.lastArgument(0));
  if(argh.help()) return 0;

  const bool sensorV11(sensorId>=21);

  std::ostringstream sid;
  sid << std::setw(2) << std::setfill('0') << sensorId;

  std::ifstream fin((std::string("sensor")+sid.str()+".txt").c_str());
  assert(fin);

  std::string text;
  unsigned x,y;
  double n;

  fin >> text >> x;
  std::cout << text << std::endl;
  std::cout << x << std::endl;
  assert(x==sensorId);

  if(!sensorV11) {
    MpsSensorV10ConfigurationData d;
    assert(d.readFile(std::string("cfg/MpsSensor1ConfigurationDataSensor")+sid.str()+".cfg"));

    unsigned m(0);
    fin >> x;
    while(fin) {
      m++;
      fin >> y >> n;
      if(d.mask(x,y)) {
	std::cout << x << " " << y << " " << n << " already masked" << std::endl;
      }
      d.mask(x,y,true);
      
      fin >> x;
    }
    d.print();
    std::cout << "Total number of new masks = " << m << std::endl;
    
    d.writeFile(std::string("Sensor")+sid.str()+".cfg");

  } else {
    MpsSensorV11ConfigurationData d;
    assert(d.readFile(std::string("cfg/MpsSensor1ConfigurationDataSensor")+sid.str()+".cfg"));

    unsigned m(0);
    fin >> x;
    while(fin) {
      m++;
      fin >> y >> n;
      if(d.mask(x,y)) {
	std::cout << x << " " << y << " " << n << " already masked" << std::endl;
      }
      d.mask(x,y,true);
      
      fin >> x;
    }
    d.print();
    std::cout << "Total number of new masks = " << m << std::endl;
    
    d.writeFile(std::string("Sensor")+sid.str()+".cfg");
  }
  
  return 0;
}
