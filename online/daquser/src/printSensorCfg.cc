#include <cstdlib>
#include <iostream>

#include "UtlArguments.hh"
#include "MpsSensor1ConfigurationData.hh"

using namespace std;


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const std::string fileName(argh.lastArgument("dummy.cfg","File name"));
  if(argh.help()) return 0;

  MpsSensor1ConfigurationData d;
  assert(d.readFile(fileName));
  d.print() << std::endl;

  return 0;
}
