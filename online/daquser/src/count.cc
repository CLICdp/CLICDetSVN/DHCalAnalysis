#include "runnerDefine.icc"

#include <iostream>
#include <sstream>

// dual/inc/utl
#include "UtlArguments.hh"

// dual/inc/rcd
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

// dual/inc/chk
#include "ChkCount.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const bool useReadAsc(argh.option('a',"Ascii input file"));
  const bool slwRead(argh.option('s',"Slow data input file"));
  const unsigned printLevel(argh.optionArgument('p',0,"Print level"));
  const unsigned numberOfRecords(argh.optionArgument('r',0xffffffff,
						     "Number of records"));
  const unsigned runNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  if(useReadAsc) cout << "Ascii input file selected" << endl;
  else           cout << "Binary input file selected" << endl;
  if(slwRead) cout << "Slow data file selected" << endl;
  else        cout << "Normal data file selected" << endl;
  cout << "Print level set to " << printLevel << endl;
  cout << "Number of records set to " << numberOfRecords << endl;
  cout << "Run number set to " << runNumber << endl;
  cout << endl;

#ifdef OLD_READER
  RcdReader *reader(0);
  if(useReadAsc) reader=new RcdReaderAsc();
  else           reader=new RcdReaderBin();
 
  ostringstream sout;
  sout << runNumber;
  if(slwRead) {
    assert(reader->open(std::string("data/slw/Slw")+sout.str()));
  } else {
    if(runNumber<100000) assert(reader->open(std::string("data/dat/Run0")+sout.str()));
    else                 assert(reader->open(std::string("data/dat/Run" )+sout.str()));
  }
#else
  assert(!slwRead);
  RunReader theReader;
  RunReader *reader(&theReader);
  assert(reader->open(runNumber,useReadAsc));
#endif

  ChkCount hn;
  hn.printLevel(printLevel);

  RcdArena &arena(*(new RcdArena));
  unsigned iRecords(0);
  while(reader->read(arena) && iRecords<numberOfRecords) {
    iRecords++;
    assert(hn.record(arena));
  }

  assert(reader->close());
  return 0;
}
