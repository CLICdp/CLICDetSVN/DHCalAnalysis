#include <unistd.h>
#include <iostream>

#include "CrcFakeCernSpill.hh"
#include "UtlTime.hh"
#include "ShmObject.hh"

int main() {
  
  ShmObject<CrcFakeCernSpill> _shmCrcFakeCernSpill(CrcFakeCernSpill::shmKey);
  CrcFakeCernSpill *_pShm(_shmCrcFakeCernSpill.payload());
  assert(_pShm!=0);

  UtlTime t[2];
  t[0].update();
  t[1].update();

  bool level[2];
  level[0]=_pShm->_level;
  level[1]=_pShm->_level;

  while(true) {
    t[1].update();
    unsigned tm((unsigned)(10.0*(t[1].time()-100000000.0)));
    if(_pShm->_invert) level[1]=(tm%168)>47;
    else               level[1]=(tm%168)<48;

    if(level[1]!=level[0]) {
      if(_pShm->_invert) std::cout << "Level inverted: changed from ";
      else               std::cout << "Level normal:   changed from ";
      if(level[0]) std::cout << "high to low";
      else         std::cout << "low to high";
      std::cout << " after " << std::setw(10) << t[1]-t[0] 
		<< " secs" << std::endl;

      t[0]=t[1];
      level[0]=level[1];
    }
  
    usleep(10000);
  }

  _pShm->print(std::cout);
  return 0;
}
