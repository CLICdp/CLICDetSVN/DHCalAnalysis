#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdWriterAsc.hh"

#include "DaqRunStart.hh"
#include "RunReader.hh"

#include "HstVfeDac.hh"
#include "ChkPrint.hh"

using namespace std;

bool continueJob=true;

void signalHandler(int signal) {
  std::cout << "Process " << getpid() << " received signal "
	    << signal << std::endl;
  continueJob=false;
}

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const bool batch(argh.option('b',"Select batch mode for ROOT"));

  if(argh.help()) return 0;

  if(batch) cout << "ROOT batch mode selected" << endl;

  RcdArena &arena(*(new RcdArena));
  RunReader reader;
  HstVfeDac *hn(0);
  ChkPrint cp;
  cp.enableType(RcdHeader::configurationStart,true);

  //const unsigned nFiles(2);
  //unsigned file[nFiles]={1086950970,1086959320};

  //const unsigned nFiles(3);
  //unsigned file[nFiles]={1086950970,1086959320,1087221855};

  // FE V3.7_2!!!
  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1086950970};
  //hn=new HstVfeDac(CercLocationData(0,7,0),CercFeConfigurationData::bot,EmcCercFeRunData::right,35,false,!batch);

  // FE V3.7_2!!!
  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1086959320};
  //hn=new HstVfeDac(CercLocationData(0,7,0),CercFeConfigurationData::bot,EmcCercFeRunData::right,4000,true,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1087390770};

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1087397726};

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1087402796};
  //hn=new HstVfeDac(CercLocationData(0,7,0),CercFeConfigurationData::bot,EmcCercFeRunData::left,35,false,!batch);
  //hn=new HstVfeDac(CercLocationData(0,7,0),CercFeConfigurationData::bot,EmcCercFeRunData::left,4000,true,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1087552161};
  //hn=new HstVfeDac(CercLocationData(0,7,0),CercFeConfigurationData::bot,EmcCercFeRunData::left,35,false,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1088701270};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,4000,true,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1088704830};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,35,false,!batch);

  // Gain x10
  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1088708673};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,35,false,!batch);

  // Gain x10
  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1088711351};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,1600,true,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1088716291};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,35,false,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1089043531};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,35,false,!batch);

  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1089095125};
  //hn=new HstVfeDac(CercLocationData(0,7,1),CercFeConfigurationData::bot,EmcCercFeRunData::ecalFull,30,false,!batch);



  // Production
  //const unsigned nFiles(1);
  //unsigned file[nFiles]={1102069034};
  //hn=new HstVfeDac(CrcLocation(0xec,12,2),CrcFeConfigurationData::bot,CrcFeRunData::ecalFull,30,false,!batch);

  const unsigned nFiles(1);
  unsigned file[nFiles]={510015};
  hn=new HstVfeDac(CrcLocation(0xec,9,1),CrcFeConfigurationData::bot,CrcFeRunData::ecalFull,16384,true,!batch);


  bool continueJob(true);
  for(unsigned i(0);i<nFiles;i++) {

    //ostringstream sout;
    //sout << "dat/Run" << file[i];
    //reader.open(sout.str());
    reader.open(file[i]);

    unsigned nev(0);
    while(reader.read(arena) && continueJob) { // && nev<10000) {
      if(hn!=0) continueJob=hn->record(arena);
      cp.record(arena);

      nev++;
      if((nev%1000)==0) cout << "Record number " << nev << endl;
    }

    reader.close();
  }

  if(hn!=0) hn->postscript("dps/vfeDac.ps");
  if(hn!=0) delete hn;
}
