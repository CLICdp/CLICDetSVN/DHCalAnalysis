#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RcdWriterBin.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  argh.print(cout);
  const unsigned runNumber(argh.lastArgument(999999));
  
  RcdArena &arena(*(new RcdArena));
  RcdReaderBin reader;
  RcdWriterBin writer;

  ostringstream sout;
  sout << "data/dat/Run" << runNumber;

  reader.open(sout.str());
  writer.open("data/dat/temp");

  while(reader.read(arena) && writer.write(arena) && writer.numberOfBytes()<1800000000);

  reader.close();
  writer.close();
}
