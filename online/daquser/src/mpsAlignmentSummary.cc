#include <cmath>
#include <cassert>
#include <fstream>
#include <sstream>
#include <iomanip>

#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraph.h"
#include "TProfile.h"

#include "UtlArguments.hh"
#include "MpsAlignmentRuns.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  const unsigned numberOfRuns(argh.optionArgument('n',100,"Number of runs"));
  const unsigned runNumberLo(argh.lastArgument(447600));

  if(argh.help()) return 0;
  
  const unsigned runNumberHi(runNumberLo+numberOfRuns);

  TFile* _rootFile;
  _rootFile = new TFile("MpsAlignmentSummary.root","RECREATE");
  
  TH2F *sr,*mp;
  TH1F *hX[6],*hY[6],*hA[6];
  TH1F *aX[6],*aY[6],*aA[6];
  TH2F *rX[6],*rY[6],*rA[6];
  //TGraph *rX[6],*rY[6],*rA[6];
  
  sr=new TH2F("SpillRatioRun",";Run number - 447000;Spill/total duty cycle",
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000,100,0.0,1.0);
  mp=new TH2F("MeanPmtRun",";Run number - 447000;Mean PMT hits/spill",
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000,100,0.0,1.0);

  for(unsigned l(0);l<6;l++) {
    std::ostringstream sout;
    sout << l;
    
    hX[l]=new TH1F((std::string("Layer")+sout.str()+"X").c_str(),
		   (std::string("Layer ")+sout.str()+";Delta x (mm)").c_str(),
		   3000,-1.5,1.5);
    hY[l]=new TH1F((std::string("Layer")+sout.str()+"Y").c_str(),
		   (std::string("Layer ")+sout.str()+";Delta y (mm)").c_str(),
		   3000,-1.5,1.5);
    hA[l]=new TH1F((std::string("Layer")+sout.str()+"A").c_str(),
		   (std::string("Layer ")+sout.str()+";Delta angle (rad)").c_str(),
		   1000,-0.05,0.05);

    aX[l]=new TH1F((std::string("Layer")+sout.str()+"AlnX").c_str(),
		   (std::string("Layer ")+sout.str()+" alignment;Run number - 447000;Delta x (mm)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000);
    aY[l]=new TH1F((std::string("Layer")+sout.str()+"AlnY").c_str(),
		   (std::string("Layer ")+sout.str()+" alignment;Run number - 447000;Delta y (mm)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000);
    aA[l]=new TH1F((std::string("Layer")+sout.str()+"AlnA").c_str(),
		   (std::string("Layer ")+sout.str()+" alignment;Run number - 447000;Delta angle (rad)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000);

    rX[l]=new TH2F((std::string("Layer")+sout.str()+"RunX").c_str(),
		   (std::string("Layer ")+sout.str()+";Run number - 447000;Delta x (mm)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000,3000,-1.5,1.5);
    rY[l]=new TH2F((std::string("Layer")+sout.str()+"RunY").c_str(),
		   (std::string("Layer ")+sout.str()+";Run number - 447000;Delta y (mm)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000,3000,-1.5,1.5);
    rA[l]=new TH2F((std::string("Layer")+sout.str()+"RunA").c_str(),
		   (std::string("Layer ")+sout.str()+";Run number - 447000;Delta angle (rad)").c_str(),
		   runNumberHi-runNumberLo,runNumberLo-447000,runNumberHi-447000,1000,-0.05,0.05);
    /*    
    rX[l]=new TGraph(0);
    rX[l]->SetTitle((std::string("Layer ")+sout.str()+";Run number - 447000;Delta x (mm)").c_str());
    rY[l]=new TGraph(0);
    rY[l]->SetTitle((std::string("Layer ")+sout.str()+";Run number - 447000;Delta y (mm)").c_str());
    rA[l]=new TGraph(0);
    rA[l]->SetTitle((std::string("Layer ")+sout.str()+";Run number - 447000;Delta angle (rad)").c_str());
    */
  }
  
  
  //MpsAlignmentRuns mar;
  //mar.print();

  MpsAlignment ma;
  int np(0);

  for(unsigned r(runNumberLo);r<runNumberHi;r++) {
    //MpsAlignment aa(mar.alignment(r));
    MpsAlignment aa;
    aa.readRun(r);
    for(unsigned l(0);l<6;l++) {
      MpsAlignmentSensor mas(aa.sensor(l));
      aX[l]->Fill(r-447000,mas.xDelta());
      aY[l]->Fill(r-447000,mas.yDelta());
      aA[l]->Fill(r-447000,mas.angle());
    }	  

    std::ostringstream sout;
    sout << std::setw(6) << std::setfill('0') << r;
    std::ifstream fin((std::string("align/MpsAnalysisBeam")+sout.str()+".txt").c_str());
    if(fin) {
      unsigned n;
      double d;
      fin >> n;
      assert(n==r);
      fin >> n >> n >> d;
      if(n>100) {
	sr->Fill(r-447000,d);
	fin >> d;
	mp->Fill(r-447000,d);
      }
      fin.close();
    }

    if(ma.readFile(std::string("align/MpsAnalysisBeam")+sout.str()+".aln")) {

      if(ma.sensor(1).xDelta()>-0.03401 && ma.sensor(1).xDelta()<-0.03400) {
	std::cout << "Run " << sout.str() << " rejected" << std::endl;
	//ma.print();
      } else {
	
	np++;
	for(unsigned l(0);l<6;l++) {
	  MpsAlignmentSensor mas(ma.sensor(l));
	  hX[l]->Fill(mas.xDelta());
	  hY[l]->Fill(mas.yDelta());
	  hA[l]->Fill(mas.angle());
	  
	  rX[l]->Fill(r-447000,mas.xDelta());
	  rY[l]->Fill(r-447000,mas.yDelta());
	  rA[l]->Fill(r-447000,mas.angle());
	  /*
	    rX[l]->Set(np);
	    rX[l]->SetPoint(np-1,r,mas.xDelta());
	    rY[l]->Set(np);
	    rY[l]->SetPoint(np-1,r,mas.yDelta());
	    rA[l]->Set(np);
	    rA[l]->SetPoint(np-1,r,mas.angle());
	  */
	}
      }
    }
  }

  if(_rootFile!=0) {
    _rootFile->Write();
    _rootFile->Close();
    delete _rootFile;
    _rootFile=0;
  }
}
