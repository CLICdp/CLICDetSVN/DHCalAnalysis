#include <iostream>

#include "RcdArena.hh"
#include "AhcSlowReadout.hh"

using namespace std;

int main(int argc, const char **argv) {
  AhcSlowReadout asr;
  asr.printLevel(8);

  RcdArena &arena(*(new RcdArena));
  arena.initialise(RcdHeader::startUp);
  asr.record(arena);

  return 0;
}
