#include "runnerDefine.icc"

#include <iostream>

#include "RcdArena.hh"
#include "SubInserter.hh"
#include "UtlArguments.hh"



#include "BmlLalHodoscopeReadout.hh"

using namespace std;


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const unsigned nCfgs(argh.optionArgument('c',1,"Number of configurations"));
  const unsigned nAcqs(argh.optionArgument('a',2,"Number of acquisitions"));
  if(argh.help()) return 0;

  //BmlLalHodoscopeReadout asr(BML_LALHODO_SKT);
  //BmlLalHodoscopeReadout asr("192.168.1.171",49153);
  //BmlLalHodoscopeReadout asr("128.141.61.37",49153);
  //BmlLalHodoscopeReadout asr("128.141.61.20",49153);
  BmlLalHodoscopeReadout asr("128.141.61.49");//,49153);
  asr.printLevel(255);

  // Define record memory
  RcdArena &arena(*(new RcdArena));
  SubInserter inserter(arena);

  arena.initialise(RcdHeader::startUp);
  asr.record(arena);
    
  arena.initialise(RcdHeader::runStart);
  asr.record(arena);

  for(unsigned i(0);i<nCfgs;i++) {
    arena.initialise(RcdHeader::configurationStart);

    BmlLalHodoscopeConfigurationData
      *d(inserter.insert<BmlLalHodoscopeConfigurationData>(true));
    d->acquisitionMode(0);
    asr.record(arena);
    
    for(unsigned j(0);j<nAcqs;j++) {
      arena.initialise(RcdHeader::acquisitionStart);
      asr.record(arena);
      sleep(1);

      arena.initialise(RcdHeader::trigger);
      asr.record(arena);
      sleep(1);

      arena.initialise(RcdHeader::event);
      asr.record(arena);
      sleep(1);

      arena.initialise(RcdHeader::acquisitionEnd);
      asr.record(arena);
    }

    arena.initialise(RcdHeader::configurationEnd);
    asr.record(arena);
  }

  arena.initialise(RcdHeader::runEnd);
  asr.record(arena);

  arena.initialise(RcdHeader::shutDown);
  asr.record(arena);

  return 0;
}
