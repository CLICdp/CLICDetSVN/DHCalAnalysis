#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <vector>
#include <cstdio>

#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "DaqDbConfigurationData.hh"
#include "EmcCercBeConfigurationData.hh"
#include "EmcCercFeConfigurationData.hh"
#include "CercFeFakeEventData.hh"

#include "DaqRunStart.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"

#include "SubExtracter.hh"

#include "HstSlwMonLm82.hh"
#include "HstSlwMonAdm1025.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  //argh.print(cout);

  const bool batch(argh.option('b',"Select batch mode for ROOT"));
  const unsigned printLevel(argh.optionArgument('p',1,"Print level"));
  const unsigned slwNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  if(batch) cout << "ROOT batch mode selected" << endl;
  else      cout << "ROOT interactive mode selected" << endl;
  cout << "Print level set to " << printLevel << endl;
  cout << "Slow controls file number set to " << slwNumber << endl;

  RcdArena &arena(*(new RcdArena));
  RcdReaderAsc reader;
  CercLocationData d(0,7,CercLocationData::vme);

  HstBase *hb[2];
  hb[0]=new HstSlwMonLm82(d,!batch);
  hb[1]=new HstSlwMonAdm1025(d,false);

  for(unsigned i(0);i<2;i++) {
    hb[i]->printLevel(printLevel);
  }

  ostringstream sout;
  sout << "slw/Slw" << slwNumber;
  reader.open(sout.str());

  while(reader.read(arena)) {
    for(unsigned i(0);i<2;i++) {
      if(hb[i]!=0) hb[i]->record(arena);
    }
  }

  reader.close();

  for(unsigned i(0);i<2;i++) {
    if(hb[i]!=0) {
      ostringstream sout;
      sout << "dps/Slw" << slwNumber << "_" << i << ".ps";
      hb[i]->postscript(sout.str());
      delete hb[i];
    }
  }
}
