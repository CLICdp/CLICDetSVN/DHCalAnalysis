// g++-2.95.3 -Wall `$ROOTSYS/bin/root-config --cflags --glibs` -I daq/dual/inc/utl -I daq/dual/inc/rcd -I daq/dual/inc/emc -I ../../../daq/dual/inc/daq -I daq/dual/inc/sub main.cc

#include <unistd.h>

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include "RcdArena.hh"
#include "RcdReader1082131336.hh"
#include "DspCosmics.hh"

using namespace std;


int main(int argc, const char **argv) {
  RcdReader1082131336 reader;
  if(!reader.open("dat/run1082131336")) return 0;

  RcdArena &r(*(new RcdArena));

  DspCosmics display;

  unsigned count(0);
  while(reader.read(r)) {
    cout << "Counter = " << count << endl;
    if(count>2250) {
      if(count==2352) {//2003
	display.event(r,true);
	sleep(20);
    }else           display.event(r);
    }
    //sleep(1);
    count++;
  }

  reader.close();
  return 0;
}
