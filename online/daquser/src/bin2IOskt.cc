#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RcdIoSkt.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  argh.print(cout);
  const unsigned runNumber(argh.lastArgument(999999));
  
  RcdArena &arena(*(new RcdArena));
  RcdArena &arena2(*(new RcdArena));
  RcdReaderBin reader;
  RcdIoSkt writer;

  ostringstream sout;
  sout << "data/dat/Run" << runNumber;

  reader.open(sout.str());
  writer.open("localhost",1124,100);

  while(reader.read(arena)) {
    if(writer.write(arena) && writer.read(arena2)) {
      arena2.RcdHeader::print(std::cout);
    }
  }
  
  arena.initialise(RcdHeader::shutDown);
  if(writer.write(arena) && writer.read(arena2)) {
    arena2.RcdHeader::print(std::cout);
  }

  reader.close();
  sleep(10);
  writer.close();
}
