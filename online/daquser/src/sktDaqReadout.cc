#include "runnerDefine.icc"

#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include <iostream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdIoSkt.hh"

#include "SubRecordType.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "RcdMultiUserRW.hh"
#include "RcdMultiUserRO.hh"
#include "RunMonitor.hh"

#include "RunLock.hh"

#define SKT_READ_AHEAD

#ifdef EMC_PCI
#include "EmcReadout.hh"
#else // EMC_PCI
#ifdef AHC_PCI
#include "AhcConfiguration.hh"
#include "AhcReadout.hh"
#endif // AHC_PCI
#endif // EMC_PCI

#ifdef BML_LC1176_PCI
#include "BmlLc1176Readout.hh"
#endif // BML_LC1176_PCI
#ifdef BML_CAEN767_CRATE
#include "BmlCaen767Configuration.hh"
#endif
#ifdef BML_CAEN767_PCI
#include "BmlCaen767Readout.hh"
#endif // BML_CAEN767_PCI

#ifdef DHE_DIF
#include "DheDifConfiguration.hh"
#include "DheDifReadout.hh"
#undef SKT_READ_AHEAD
#endif // DHE_DIF

#ifdef DHC_PCI
#include "TtmReadout.hh"
#include "DhcConfiguration.hh"
#include "DhcReadout.hh"
#undef SKT_READ_AHEAD
#endif // DHC_PCI

#ifdef AHC_STAGE_SKT
#include "AhcSlowReadout.hh"
#endif

#undef SKT_READ_AHEAD

using namespace std;

int main(int argc, const char **argv) {
  
  time_t eTime(CALICE_DAQ_TIME);
  cout << argv[0] << " compiled at " << ctime((const time_t*)&eTime);
  cout << argv[0] << " compiled with CALICE_DAQ_SIZE = " << CALICE_DAQ_SIZE <<std::endl;
  
  UtlArguments argh(argc,argv);
  
  const unsigned pciCard(argh.optionArgument('n',0,"PCI card"));
  const unsigned printLevel(argh.optionArgument('p',9,"Print level"));
  
  if(argh.help()) return 0;
  
  RunLock lock(argv[0]);

  cout << "PCI card set to " << pciCard << endl;
  
  RcdIoSkt s;
  //assert(s.open(1124+pciCard));
  assert(s.open(1124));

  // Set up list of RW modules
  RcdMultiUserRW vRrw;

  // Add ECAL and AHCAL readout modules
#ifdef EMC_PCI
  EmcReadout er(EMC_PCI);
  vRrw.addUser(er);
#else

#ifdef AHC_PCI
  AhcConfiguration ac;
#ifdef TRG_CRATE
#ifdef TRG_SLOT
  if(TRG_CRATE==0xac && TRG_SLOT!=0) ac.trgSlot(TRG_SLOT);
#endif
#endif
  vRrw.addUser(ac);

  AhcReadout ar(AHC_PCI);
  vRrw.addUser(ar);
#endif
#endif

  // Add TDC modules
#ifdef BML_LC1176_PCI
  BmlLc1176Readout bl(BML_LC1176_PCI,BML_LC1176_ADDRESS);
  vRrw.addUser(bl);
#endif

#ifdef BML_CAEN767_PCI
  BmlCaen767Configuration bc(BML_CAEN767_CRATE);
  vRrw.addUser(bc);
  BmlCaen767Readout cl(BML_CAEN767_PCI,BML_CAEN767_CRATE,
		       BML_CAEN767_ADDRESS0,BML_CAEN767_ADDRESS1);
  vRrw.addUser(cl);
#endif

#ifdef DHE_DIF
  DheDifConfiguration dc;
  vRrw.addUser(dc);
  DheDifReadout dd;
  vRrw.addUser(dd);
#endif

  // Add DHCAL modules
#ifdef DHC_PCI
  DhcConfiguration hc;
  vRrw.addUser(hc);
#ifdef TTM_SLOT
  TtmReadout tr(DHC_PCI,0xdc,TTM_SLOT);
  vRrw.addUser(tr);
#endif
  DhcReadout hr(DHC_PCI,0xdc);
  vRrw.addUser(hr);
#ifdef DHC_PCID
  DhcReadout hrd(DHC_PCID,0xdd);
  vRrw.addUser(hrd);
#endif
#endif

  // Add stage position readout
#ifdef AHC_STAGE_SKT
  AhcSlowReadout asr(AHC_STAGE_SKT); 
  vRrw.addUser(asr); 
#endif

  // Set up list of RO modules
  RcdMultiUserRO vRro;

  // Add display module
  //RunMonitor rm;
  //vRro.addUser(rm);

  // Define memory for records
  RcdArena &r(*(new RcdArena));
  r.initialise(RcdHeader::startUp);
  
  // Set initial print level;
  vRrw.printLevel(printLevel);
  vRro.printLevel(printLevel);

#ifdef SKT_READ_AHEAD

  RcdArena &ahead(*(new RcdArena));
  ahead.initialise(RcdHeader::event);
  bool inSpill(false),eventAhead(false);
  unsigned nTrg(0),nEvt(0);

#endif

  // Define the id label for the timer
  UtlPack tid;
  tid.halfWord(1,SubHeader::crc);
#ifdef EMC_PCI
  tid.halfWord(1,SubHeader::emc);
#endif
#ifdef AHC_PCI
  tid.halfWord(1,SubHeader::ahc);
#endif
  tid.byte(2,0xff);
 
  while(r.recordType()!=RcdHeader::shutDown) {
    
    // Read next record from socket while timing the wait
    DaqTwoTimer tt;
    tt.timerId(tid.word());

    assert(s.read(r));

    tt.setEndTime();
    SubInserter inserter(r);
    inserter.insert<DaqTwoTimer>(tt);
    
    // Check for startUp record
    if(r.recordType()==RcdHeader::startUp) {
      
      // Put in software information
      SubInserter inserter(r);
      DaqSoftware *dsw(inserter.insert<DaqSoftware>(true));
      dsw->message(argh.command());
      dsw->setVersions();
      dsw->print(std::cout);
    }
    
    // Check for shutDown record
    if(r.recordType()==RcdHeader::shutDown) {

      // Reset print level
      vRrw.printLevel(printLevel);
      vRro.printLevel(printLevel);
    }

    // Check for runStart record
    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
      if(v.size()>0) {
	vRrw.printLevel(v[0]->runType().printLevel());
	vRro.printLevel(v[0]->runType().printLevel());
      }
    }

#ifndef SKT_READ_AHEAD

    // Process record and send back
    assert(vRrw.record(r));
    assert(vRro.record(r));
    assert(s.write(r));

#else

    // Zero trigger and event counters in acquisition start
    if(r.recordType()==RcdHeader::acquisitionStart) {
      nTrg=0;
      nEvt=0;
    }

    // Check for mismatch of counters
    if(r.recordType()==RcdHeader::acquisitionEnd) {
      if(nTrg!=nEvt) {
	std::cerr << "sktReadout counter mismatch, nTrg = "
		  << nTrg << ", nEvt = " << nEvt << std::endl;
	r.RcdHeader::print(std::cerr," SKT ") << std::endl;
      }
    }

    // Check for spillStart/End records
    if(r.recordType()==RcdHeader::spillStart) inSpill=true;
    if(r.recordType()==RcdHeader::spillEnd)   inSpill=false;

    // Increment counter
    if(r.recordType()==RcdHeader::trigger) nTrg++;

    if(r.recordType()==RcdHeader::triggerBurst) {
      SubAccessor accessor(r);
      std::vector<const DaqBurstEnd*> v(accessor.extract<DaqBurstEnd>());
      if(v.size()>0) {
	nTrg+=v[0]->actualNumberOfTriggersInBurst();
      }
    }

    // Process record and send back
    if(r.recordType()==RcdHeader::event && eventAhead) {
      eventAhead=false;
      r.extend(ahead.numberOfWords(),ahead.data());
    } else {
      if(r.recordType()==RcdHeader::event) nEvt++;
      assert(vRrw.record(r));
      assert(vRro.record(r));
    }
    assert(s.write(r));

    // Check for possibility of readahead
    if(!eventAhead && !inSpill && nTrg>nEvt) {
      eventAhead=true;
      nEvt++;
      ahead.initialise(RcdHeader::event);
      assert(vRrw.record(ahead));
      assert(vRro.record(ahead));
    }

#endif

  }

  assert(s.close());

  return 0;
}
