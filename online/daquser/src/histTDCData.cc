//////////////////////////////////////////////////////////////////////////////
// d050131 : GM  : TDC data histogramming, see class dual/inc/HstTDCData.hh
//////////////////////////////////////////////////////////////////////////////


#include <cstring>
#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"

#include "HstTDCData.hh"

using namespace std;


int main(int argc, const char **argv) 
{
   UtlArguments argh(argc,argv);
   const unsigned runNumber(argh.lastArgument(999999));

   // Array used for storing raw data records
   RcdArena &arena(*(new RcdArena));

   // Open the raw data input file
   RcdReaderBin reader;
   
   ostringstream sout;
   sout << "data/dat/Run" << runNumber;
   reader.open(sout.str());

   //TDC data histogramming
   HstTDCData histoTDC(runNumber);
  
   // Loop over records from input file
   unsigned iEvent(0);
   while(reader.read(arena)) 
   {
      histoTDC.event(arena);//fill histos per event 
    
      if(arena.recordType()==RcdHeader::event) iEvent++;     
    
   }
   reader.close();
   
   histoTDC.Close();//dump histos in .root and .ps

   cout << "Finished after " << iEvent << " events" << endl;
   return 0;
}
//////////////////////////////////////////////////////////////////////////////
