//#define HST_MAP

#include <iostream>
#include <fstream>
#include <sstream>

// dual/inc/utl
#include "UtlArguments.hh"

// dual/inc/rcd
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"

#include "ChkCount.hh"
#include "ChkCaen767.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  //const bool ignoreNoHitError(argh.option('n',false,"Ignore no-hit error"));

  const unsigned printLevel(argh.optionArgument('p',0,"Print level"));
  const unsigned numberOfRecords(argh.optionArgument('r',0xffffffff,
						     "Number of records"));
  const unsigned runNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  cout << "Print level set to " << printLevel << endl;
  cout << "Number of records set to " << numberOfRecords << endl;
  cout << "Run number set to " << runNumber << endl;
  cout << endl;

  RunReader theReader;
  RunReader *reader(&theReader);

  ChkCount hc;
  hc.printLevel(printLevel);
  //ChkCaen767 hn(0,ignoreNoHitError);
  ChkCaen767 hn(0);
  hn.printLevel(printLevel);

  RcdArena &arena(*(new RcdArena));
  unsigned iRecords(0);

  assert(reader->open(runNumber));
  while(reader->read(arena) && iRecords<numberOfRecords) {
    iRecords++;
    assert(hc.record(arena));
    assert(hn.record(arena));
  }
    
  assert(reader->close());
  return 0;
}
