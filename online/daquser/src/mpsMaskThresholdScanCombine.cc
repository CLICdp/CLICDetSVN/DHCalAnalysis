#include <cassert>
#include <vector>
#include <fstream>
#include <iostream>

#include "UtlArguments.hh"


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  const std::string sensor(argh.lastArgument("Sensor45Trim0"));
  
  if(argh.help()) return 0;

  std::vector<unsigned> vRun;

  bool v11(false);

  //if(sensor=="Unknown") for(unsigned i(0);i<20;i++) vRun.push_back(452808+i);
  //if(sensor=="Sensor07") for(unsigned i(0);i<168;i++) vRun.push_back(480392+i);
  //if(sensor=="Sensor10Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(475000+i);
  //if(sensor=="Sensor10Trim8") for(unsigned i(0);i<168;i++) vRun.push_back(475168+i);
  //if(sensor=="Sensor10Trim4") for(unsigned i(0);i<168;i++) vRun.push_back(475336+i);
  //if(sensor=="Sensor10TrimC") for(unsigned i(0);i<168;i++) vRun.push_back(475504+i);
  //if(sensor=="Sensor10Trimed") for(unsigned i(0);i<168;i++) vRun.push_back(475672+i);
  //if(sensor=="Sensor11Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(475845+i);
  //if(sensor=="Sensor11TrimF") for(unsigned i(0);i<168;i++) vRun.push_back(476013+i);
  //if(sensor=="Sensor11Trim8") for(unsigned i(0);i<168;i++) vRun.push_back(476181+i);
  //if(sensor=="Sensor11Trimed") for(unsigned i(0);i<168;i++) vRun.push_back(476521+i);
  //if(sensor=="Sensor11Standard") for(unsigned i(0);i<5;i++) vRun.push_back(476693+i);
  //if(sensor=="Sensor11Light") for(unsigned i(0);i<5;i++) vRun.push_back(476698+i);
  //if(sensor=="Sensor11J24Swap") for(unsigned i(0);i<5;i++) vRun.push_back(476703+i);
  //if(sensor=="Sensor11J24Off") for(unsigned i(0);i<5;i++) vRun.push_back(476708+i);
  //if(sensor=="Sensor11J23Swap") for(unsigned i(0);i<5;i++) vRun.push_back(476714+i);
  //if(sensor=="Sensor11J23Off") for(unsigned i(0);i<5;i++) vRun.push_back(476719+i);
  //if(sensor=="Sensor11Std2") for(unsigned i(0);i<5;i++) vRun.push_back(476724+i);
  //if(sensor=="Sensor11J5Off") for(unsigned i(0);i<5;i++) vRun.push_back(476729+i);
  //if(sensor=="Sensor11J8Off") for(unsigned i(0);i<5;i++) vRun.push_back(476734+i);
  //if(sensor=="Sensor11PAB4000") for(unsigned i(0);i<5;i++) vRun.push_back(476739+i);
  //if(sensor=="Sensor11Std3") for(unsigned i(0);i<5;i++) vRun.push_back(476744+i);
  //if(sensor=="Sensor13Owen") for(unsigned i(0);i<168;i++) vRun.push_back(465582+i);
  //if(sensor=="Sensor13Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(465750+i);
  //if(sensor=="Sensor13Paul") for(unsigned i(0);i<168;i++) vRun.push_back(465918+i);
  //if(sensor=="Sensor13Paul") for(unsigned i(0);i<168;i++) vRun.push_back(466086+i);
  //if(sensor=="Sensor04Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(476752+i);
  //if(sensor=="Sensor26Trim0";v11=true;
  //for(unsigned i(0);i<168;i++) vRun.push_back(477111+i);
  //if(sensor=="Sensor26Trim4";v11=true;
  //for(unsigned i(0);i<168;i++) vRun.push_back(477279+i);
  //if(sensor=="Sensor29Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(491179+i);
  //if(sensor=="Sensor29Trim8") for(unsigned i(0);i<168;i++) vRun.push_back(491347+i);
  //if(sensor=="Sensor29Paul1") for(unsigned i(0);i<168;i++) vRun.push_back(491515+i);
  //if(sensor=="Sensor29Paul2") for(unsigned i(0);i<168;i++) vRun.push_back(491683+i);
  //if(sensor=="Sensor29Paul3") for(unsigned i(0);i<168;i++) vRun.push_back(491851+i);
  //if(sensor=="Sensor29Paul4") for(unsigned i(0);i<168;i++) vRun.push_back(492020+i);
  //if(sensor=="Sensor44Trim0") for(unsigned i(0);i<168;i++) vRun.push_back(492189+i);
  //if(sensor=="Sensor44Paul1") for(unsigned i(0);i<168;i++) vRun.push_back(492357+i);
  //if(sensor=="Sensor44Bit0") for(unsigned i(0);i<1;i++) vRun.push_back(492525+i);
  //if(sensor=="Sensor44Bit1") for(unsigned i(0);i<1;i++) vRun.push_back(492526+i);
  //if(sensor=="Sensor44Bit2") for(unsigned i(0);i<1;i++) vRun.push_back(492527+i);
  //if(sensor=="Sensor44Bit3") for(unsigned i(0);i<1;i++) vRun.push_back(492528+i);
  //if(sensor=="Sensor44Bit4") for(unsigned i(0);i<1;i++) vRun.push_back(492529+i);
  //if(sensor=="Sensor44Bit5") for(unsigned i(0);i<1;i++) vRun.push_back(492530+i);
  //if(sensor=="Sensor44TrimV0") for(unsigned i(0);i<1;i++) vRun.push_back(492531+i);
  //if(sensor=="Sensor44Trim0") for(unsigned i(0);i<1;i++) vRun.push_back(492532+i);
  //if(sensor=="Sensor44Trim0") for(unsigned i(0);i<1;i++) vRun.push_back(492533+i);
  //if(sensor=="Sensor44Bit0") for(unsigned i(0);i<1;i++) vRun.push_back(492534+i);
  //if(sensor=="Sensor44Bit1") for(unsigned i(0);i<1;i++) vRun.push_back(492535+i);
  //if(sensor=="Sensor44Bit2") for(unsigned i(0);i<1;i++) vRun.push_back(492536+i);
  //if(sensor=="Sensor44Bit3") for(unsigned i(0);i<1;i++) vRun.push_back(492539+i);
  //if(sensor=="Sensor44Bit4") for(unsigned i(0);i<1;i++) vRun.push_back(492540+i);
  //if(sensor=="Sensor44Bit5") for(unsigned i(0);i<1;i++) vRun.push_back(492541+i);
  //if(sensor=="Sensor44TrimV0") for(unsigned i(0);i<1;i++) vRun.push_back(492542+i);

  // Sensor #48
  if(sensor=="Sensor48P42"   ) for(unsigned i(0);i<1;i++) vRun.push_back(492543+i);
  if(sensor=="Sensor48P84"   ) for(unsigned i(0);i<1;i++) vRun.push_back(492544+i);
  if(sensor=="Sensor48Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(492545+i);
  if(sensor=="Sensor48Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(492713+i);
  if(sensor=="Sensor48Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(492881+i);
  if(sensor=="Sensor48Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(493049+i);
  if(sensor=="Sensor48Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(493217+i);
  if(sensor=="Sensor48Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(493385+i);
  if(sensor=="Sensor48Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(493553+i);
  if(sensor=="Sensor48TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(493721+i);
  
  // Sensor #45
  if(sensor=="Sensor45Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(493892+i);
  if(sensor=="Sensor45Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(494060+i);
  if(sensor=="Sensor45Bit1"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(494228+i);
    vRun[92]=494396; // Original corrupted
    vRun[96]=494397; // Original corrupted
  }
  if(sensor=="Sensor45Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(494398+i);
  if(sensor=="Sensor45Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(494566+i);
  if(sensor=="Sensor45Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(494734+i);
  if(sensor=="Sensor45Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(494902+i);
  if(sensor=="Sensor45TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(495070+i);

  // Sensor #46
  if(sensor=="Sensor46Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(495240+i);
  if(sensor=="Sensor46Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(495408+i);
  if(sensor=="Sensor46Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(495576+i);
  if(sensor=="Sensor46Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(495744+i);
  if(sensor=="Sensor46Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(495912+i);
  if(sensor=="Sensor46Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(496080+i);
  if(sensor=="Sensor46Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(496248+i);
  if(sensor=="Sensor46TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(496416+i);

  // Sensor #47
  if(sensor=="Sensor47Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(496585+i);
  if(sensor=="Sensor47Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(496753+i);
  if(sensor=="Sensor47Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(496921+i);
  if(sensor=="Sensor47Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(497089+i);
  if(sensor=="Sensor47Bit3"  ) {
    for(unsigned i(0);i<161;i++) vRun.push_back(497257+i);
    for(unsigned i(0);i<  7;i++) vRun.push_back(497419+i); // USB disconnect; restarted
  }
  if(sensor=="Sensor47Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(497426+i);
  if(sensor=="Sensor47Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(497594+i);
  if(sensor=="Sensor47TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(497762+i);

  // Sensor #42
  /*
  if(sensor=="Sensor42Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(498099+i);
  if(sensor=="Sensor42Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(498267+i);
  if(sensor=="Sensor42Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(498435+i);
  if(sensor=="Sensor42Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(498603+i);
  if(sensor=="Sensor42Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(498771+i);
  if(sensor=="Sensor42Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(498939+i);
  if(sensor=="Sensor42Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(499107+i);
  if(sensor=="Sensor42TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(499275+i);
  */

  /*
  // Sensor #50
  if(sensor=="Sensor50Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(461280+i);
  if(sensor=="Sensor50Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(461448+i);
  if(sensor=="Sensor50Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(461616+i);
  if(sensor=="Sensor50Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(461784+i);
  if(sensor=="Sensor50Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(461952+i);
  // if(sensor=="Sensor50Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(468000+i);
  if(sensor=="Sensor50Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(469542+i);
  // if(sensor=="Sensor50Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(468168+i);
  if(sensor=="Sensor50Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(469710+i);
  // if(sensor=="Sensor50TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(468336+i);
  if(sensor=="Sensor50TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(469877+i);
  */

  // Sensor #36
  /*
  if(sensor=="Sensor36Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(460608+i);
  if(sensor=="Sensor36Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(460776+i);
  if(sensor=="Sensor36Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(460944+i);
  if(sensor=="Sensor36Bit2"  ) 
  {
	for(unsigned i(0);i<168;i++) vRun.push_back(461112+i);
	vRun[145] = 468621;
  }
  if(sensor=="Sensor36Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(468622+i);
  if(sensor=="Sensor36Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(468790+i);
  if(sensor=="Sensor36Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(468958+i);
  if(sensor=="Sensor36TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(469156+i);
  */

  // Sensor #32
  if(sensor=="Sensor32Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(454287+i);
  if(sensor=="Sensor32Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(454455+i);
  if(sensor=="Sensor32Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(454623+i);
  if(sensor=="Sensor32Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(454791+i);
  if(sensor=="Sensor32Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(454959+i);
  if(sensor=="Sensor32Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(455127+i);
  if(sensor=="Sensor32Bit5"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(455295+i);
    vRun[44]=455463;
  }
  if(sensor=="Sensor32TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(455464+i);

  // Sensor #33
  if(sensor=="Sensor33Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(499444+i);
  if(sensor=="Sensor33Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(499612+i);
  if(sensor=="Sensor33Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(499780+i);
  if(sensor=="Sensor33Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(420000+i);
  if(sensor=="Sensor33Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(420168+i);
  if(sensor=="Sensor33Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(420336+i);
  if(sensor=="Sensor33Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(420504+i);
  if(sensor=="Sensor33TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(420672+i);

  // Sensor #43
  if(sensor=="Sensor43Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(420852+i);
  if(sensor=="Sensor43Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(421020+i);
  if(sensor=="Sensor43Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(421188+i);
  if(sensor=="Sensor43Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(421356+i);
  if(sensor=="Sensor43Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(421524+i);
  if(sensor=="Sensor43Bit4"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(421692+i);
    vRun[116]=421860; // Original corrupted
    vRun[135]=421861; // Original corrupted
  }
  if(sensor=="Sensor43Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(421862+i);
  if(sensor=="Sensor43TrimV0") {
    for(unsigned i(0);i<168;i++) vRun.push_back(422030+i);
    vRun[104]=422198; // Original corrupted
    vRun[166]=422199; // Original corrupted
  }

  // Sensor #26
  if(sensor=="Sensor26Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(422201+i);
  if(sensor=="Sensor26Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(422369+i);
  if(sensor=="Sensor26Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(422537+i);
  if(sensor=="Sensor26Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(422705+i);
  if(sensor=="Sensor26Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(422873+i);
  if(sensor=="Sensor26Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(423041+i);
  if(sensor=="Sensor26Bit5"  ) {
    for(unsigned i(0);i<105;i++) vRun.push_back(423209+i);
    for(unsigned i(0);i< 63;i++) vRun.push_back(423315+i);
  }
  if(sensor=="Sensor26TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(423379+i);

  // Sensor #36

/*
  if(sensor=="Sensor36Trim0" ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(424293+i);
    vRun[ 88]=424461; // Original corrupted
    vRun[147]=424462; // Original corrupted
  }
  if(sensor=="Sensor36Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(424465+i);
  if(sensor=="Sensor36Bit1"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(424633+i);
    vRun[ 76]=424801; // Original corrupted
  }

  if(sensor=="Sensor36Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(424802+i);
  if(sensor=="Sensor36Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(424970+i);
  if(sensor=="Sensor36Bit4"  ) {
    for(unsigned i(0);i<124;i++) vRun.push_back(425138+i);
    for(unsigned i(0);i< 44;i++) vRun.push_back(425263+i);
    vRun[  7]=425307; // Original corrupted
    vRun[ 33]=425308; // Original corrupted
  }
  if(sensor=="Sensor36Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(425309+i);
  if(sensor=="Sensor36TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(425477+i);
*/


  // Sensor #21
  if(sensor=="Sensor21Trim0" ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(425646+i);
    vRun[ 23]=425814; // Original corrupted
    vRun[ 94]=425815; // Original corrupted
  }
  if(sensor=="Sensor21Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(425819+i);
  if(sensor=="Sensor21Bit1"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(425987+i);
    vRun[ 60]=426155; // Original corrupted
    vRun[162]=426156; // Original corrupted
  }
  if(sensor=="Sensor21Bit2"  ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(426157+i);
    vRun[ 28]=426325; // Original corrupted
  }
  if(sensor=="Sensor21Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(426326+i);
  if(sensor=="Sensor21Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(426494+i);
  if(sensor=="Sensor21Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(426662+i);
  if(sensor=="Sensor21TrimV0") {
    for(unsigned i(0);i<168;i++) vRun.push_back(426830+i);
    vRun[ 34]=426998; // Original corrupted
    vRun[ 63]=426999; // Original corrupted
  }



  // JC Sensor #37
  if(sensor=="Sensor37Trim0" ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(427000+i);
    vRun[ 3]=427168; // Original corrupted
  }
  if(sensor=="Sensor37Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(427169+i);
  if(sensor=="Sensor37Bit1" ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(427337+i);
    vRun[143]=427505; // Original corrupted
  }
  if(sensor=="Sensor37Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(427506+i);
  if(sensor=="Sensor37Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(427674+i);
  if(sensor=="Sensor37Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(427842+i);
  if(sensor=="Sensor37Bit5" ) {
    for(unsigned i(0);i<168;i++) vRun.push_back(428010+i);
    vRun[44]=428178; // Original corrupted
    vRun[55]=428179; // Original corrupted    
  }
  if(sensor=="Sensor37TrimV0" ) for(unsigned i(0);i<168;i++) vRun.push_back(428184+i);


  // JC Sensor #39
  if(sensor=="Sensor39Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(459374+i);
  if(sensor=="Sensor39Bit0" )  {
    for(unsigned i(0);i<168;i++) vRun.push_back(428372+i);
    vRun[29]=428540; // Original corrupted
  }
  if(sensor=="Sensor39Bit1" )  for(unsigned i(0);i<168;i++) vRun.push_back(428542+i);
  if(sensor=="Sensor39Bit2" )  for(unsigned i(0);i<168;i++) vRun.push_back(428710+i);
  if(sensor=="Sensor39Bit3" )  {
    for(unsigned i(0);i<168;i++) vRun.push_back(428878+i);
    vRun[110]=459908; // Original corrupted (re-taken on calicedaq2)
  }
  if(sensor=="Sensor39Bit4" )  for(unsigned i(0);i<168;i++) vRun.push_back(459909+i);
  if(sensor=="Sensor39Bit5" )  for(unsigned i(0);i<168;i++) vRun.push_back(460077+i);
  if(sensor=="Sensor39TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(460245+i);

  // Sensor #36
  if(sensor=="Sensor36Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(470657+i);
  if(sensor=="Sensor36Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(470825+i);
  if(sensor=="Sensor36Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(470993+i);
  if(sensor=="Sensor36Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(471161+i);
  if(sensor=="Sensor36Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(471329+i);
  if(sensor=="Sensor36Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(471497+i);
  if(sensor=="Sensor36Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(471665+i);
  if(sensor=="Sensor36TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(471833+i);

  // Sensor #38
  if(sensor=="Sensor38Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(472001+i);
  if(sensor=="Sensor38Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(472169+i);
  if(sensor=="Sensor38Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(472337+i);
  if(sensor=="Sensor38Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(472505+i);
  if(sensor=="Sensor38Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(472673+i);
  if(sensor=="Sensor38Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(472841+i);
  if(sensor=="Sensor38Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(473009+i);
  if(sensor=="Sensor38TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(473177+i);

  // Sensor #42
  //if(sensor=="Sensor42Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(473345+i);
  if(sensor=="Sensor42Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(475025+i);
  if(sensor=="Sensor42Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(475193+i);
  if(sensor=="Sensor42Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(475361+i);
  if(sensor=="Sensor42Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(475529+i);
  if(sensor=="Sensor42Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(475697+i);
  if(sensor=="Sensor42Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(475865+i);
  if(sensor=="Sensor42Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(476033+i);
  if(sensor=="Sensor42TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(476201+i);

  // Sensor #44
  if(sensor=="Sensor44Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(473513+i);
  if(sensor=="Sensor44Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(473681+i);
  if(sensor=="Sensor44Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(473849+i);
  if(sensor=="Sensor44Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(474017+i);
  if(sensor=="Sensor44Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(474185+i);
  if(sensor=="Sensor44Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(474353+i);
  if(sensor=="Sensor44Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(474521+i);
  if(sensor=="Sensor44TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(474689+i);

  // Sensor #50
  //if(sensor=="Sensor50Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(474857+i);
  if(sensor=="Sensor50Trim0" ) for(unsigned i(0);i<168;i++) vRun.push_back(476369+i);
  if(sensor=="Sensor50Bit0"  ) for(unsigned i(0);i<168;i++) vRun.push_back(476537+i);
  if(sensor=="Sensor50Bit1"  ) for(unsigned i(0);i<168;i++) vRun.push_back(476705+i);
  if(sensor=="Sensor50Bit2"  ) for(unsigned i(0);i<168;i++) vRun.push_back(476873+i);
  if(sensor=="Sensor50Bit3"  ) for(unsigned i(0);i<168;i++) vRun.push_back(477041+i);
  if(sensor=="Sensor50Bit4"  ) for(unsigned i(0);i<168;i++) vRun.push_back(477209+i);
  if(sensor=="Sensor50Bit5"  ) for(unsigned i(0);i<168;i++) vRun.push_back(477377+i);
  if(sensor=="Sensor50TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(477545+i);

  // Sensor #29
  if(sensor=="Sensor29TrimV0") for(unsigned i(0);i<168;i++) vRun.push_back(491010+i);

  if(vRun.size()==0) {
    std::cout << "Error " << sensor << " has no runs" << std::endl;
    return 1;
  }

  bool good[168][168];
  unsigned trim[168][168];
  unsigned entries[168][168];
  double mean[168][168];
  double sigma[168][168];

  for(unsigned x(0);x<168;x++) {
    for(unsigned y(0);y<168;y++) {
      good[x][y]=false;
      trim[x][y]=64;
      entries[x][y]=0;
      mean[x][y]=0.0;
      sigma[x][y]=0.0;
    }
  }

  for(unsigned i(0);i<vRun.size();i++) {
    std::ostringstream sout;
    sout << "MpsAnalysisMaskThresholdScan" << vRun[i] << ".txt";
    std::cout << "Opened file " << sout.str() << std::endl;
    std::ifstream fin(sout.str().c_str());
    assert(fin);

    unsigned x,y,t,e;
    double m,s;

    fin >> x;
    while(fin) {
      fin >> y >> t >> e >> m >> s;
      if(v11 && i>=84) y+=84;

      if(x<168 && y<168) {
	trim[x][y]=t;
	if(t>63) {
	  std::cout << "Error " << x << ", " << y << " trim = " << t << std::endl;
	}
	if(e>10.5) {
	  if(!good[x][y]) {
	    good[x][y]=true;
	    entries[x][y]=e;
	    mean[x][y]=m;
	    sigma[x][y]=s;
	  } else {
	    std::cout << "Error " << x << ", " << y << " already good" << std::endl;
	  }
	} else {
	  std::cout << "Warning " << x << ", " << y << " entries = " << e << std::endl;
	}
      } else {
	std::cout << "Error " << x << ", " << y << " bad values" << std::endl;
      }

      fin >> x;
    }
  }

  std::ofstream fout((sensor+".txt").c_str());
  for(unsigned x(0);x<168;x++) {
    for(unsigned y(0);y<168;y++) {
      if(good[x][y]) {
	fout << x << " " << y
	     << " " << trim[x][y]
	     << " " << entries[x][y]
	     << " " << mean[x][y]
	     << " " << sigma[x][y] << std::endl;
      } else {
	std::cout << "Error " << x << ", " << y << " missing" << std::endl;
	fout << x << " " << y << " " << trim[x][y] << " 0 0 0" << std::endl;
      }
    }
  }
  fout.close();

  return 0;
}
