//#include <sys/types.h>
//#include <sys/stat.h>
//#include <signal.h>
//#include <fcntl.h>
//#include <unistd.h>

#include <string>
#include <iostream>
#include <sstream>
#include <vector>
#include <map>
//#include <cstdio>

//#include "UtlTime.hh"
#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

#include "mappingFromFile.hh"


//ROOT includes
#include "TROOT.h"
//#include "TH1I.h"
//#include "TFile.h"


//using namespace std;


bool newRun, mapped, extern_mapping, _beamRun;//, _pedestal;
unsigned _runNumber, _events;

RcdHeader runHeader;
AhcMapping mapping;


const unsigned MAXMODULE = 41;  // 38 HCAL + 2 tcmt + 1 PMT
const unsigned MAXCHIP = 12;
const unsigned MAXCHAN = 18;

float _sum[MAXMODULE+1][MAXCHIP][MAXCHAN];
unsigned _counter[MAXMODULE+1][MAXCHIP][MAXCHAN];
float _pedestal[MAXMODULE+1][MAXCHIP][MAXCHAN];
unsigned _pedestalCounter[MAXMODULE+1][MAXCHIP][MAXCHAN];
unsigned _hold[MAXMODULE+1];
bool _newHold[MAXMODULE+1];


//std::string _printmode;
std::string _lastMessage;
unsigned int _configurationNumber;
unsigned int _lastPedConfig;

void printOut(const std::string title,const std::string mytext, bool resetLine=false) {
  if (title != _lastMessage) std::cout << std::endl << title << " ";
  _lastMessage = title;
  if (resetLine) std::cout << "\r"  << title << " ";
  std::cout << mytext << " " << std::flush;
}

void printOut(const std::string title, const long int number,const bool resetLine=false) {
  std::stringstream mystream;
  mystream << number;
  if (!resetLine) mystream << ",";
  printOut(title,std::string(mystream.str()),resetLine);
}


void resetPedestal(unsigned module) {
  for (unsigned chip=0;chip<MAXCHIP;chip++)
    for (unsigned chan=0;chan<MAXCHAN;chan++) {
      _pedestal[module][chip][chan] = 0;
      _pedestalCounter[module][chip][chan] = 0;
    }
}

void resetPedestal() {
  for (unsigned mod=1;mod<=MAXMODULE;mod++) resetPedestal(mod);
  _lastPedConfig = _configurationNumber;
}

void resetValues(unsigned module) {
  for (unsigned chip=0;chip<MAXCHIP;chip++)
    for (unsigned chan=0;chan<MAXCHAN;chan++) {
      _sum[module][chip][chan] = 0;
      _counter[module][chip][chan] = 0;
    }
}

void resetValues() {
  //  std::cout << "resetting values" << std::endl;
  for (unsigned mod=1;mod<=MAXMODULE;mod++) resetValues(mod);
}

void resetHold() {
  for (unsigned mod=1;mod<=MAXMODULE;mod++) _hold[mod]=0xFFFFFFFF;
}


void calculatePedestal(unsigned module,unsigned chip, unsigned chan, int value) {
  if (_lastPedConfig != _configurationNumber) resetPedestal();
  //  std::cout << "pedestal: mod " << module << " chip " << chip << " chan " << chan << " value " << value << std::endl;
  (_pedestal[module][chip][chan]) += value;
  (_pedestalCounter[module][chip][chan])++;
}

float pedestal(unsigned module,unsigned chip, unsigned chan) {
  float thisPedestal =  _pedestal[module][chip][chan]/(float)_pedestalCounter[module][chip][chan];
  //  std::cout << "asked for pedestal: mod " << module << " chip " << chip << " chan " << chan << " value " << thisPedestal << std::endl;
  return thisPedestal;
}

void calculate(unsigned module,unsigned chip, unsigned chan, int value) {

  //  std::cout << "mod " << module << " chip " << chip << " chan " << chan << " value " << value << std::endl;
  if ( ((float)value ) > 150.) {
    (_sum[module][chip][chan]) += value;
    (_counter[module][chip][chan])++;
  }
}

void resetFile(unsigned module) {
  std::ofstream holdfile;
  std::ostringstream filename;
  filename << "hold_module" << module << ".dat";
  holdfile.open(filename.str().c_str());
  holdfile << "#hold chan0 fakerms0 chan1 fakerms1 .. .. " << std::endl;
}

void resetFile() {
  for (unsigned mod=1;mod<=MAXMODULE;mod++) resetFile(mod);
}

void writePointToFile(unsigned module, unsigned hold) {
  std::ofstream holdfile;
  std::ostringstream filename;
  filename << "hold_module" << module << ".dat";
  holdfile.open(filename.str().c_str(),std::ios::app);

  holdfile << hold << " ";
  for (unsigned chip=0;chip<MAXCHIP;chip++)
    for (unsigned chan=0;chan<MAXCHAN;chan++) {
      //      std::cout << "sum " << (float)(_sum[module][chip][chan]) << " counter " << _counter[module][chip][chan] << std::endl;
      if (_counter[module][chip][chan] == 0) {
	holdfile << -99999 << " 0 ";
      } else {
	holdfile << (float)(_sum[module][chip][chan])/(float)(_counter[module][chip][chan]) << " 0 ";
      }
    }
  holdfile << std::endl;
  holdfile.close();
  resetValues(module);
}

void writePointToFile(unsigned* hold) {
  for (unsigned mod=1;mod<=MAXMODULE;mod++) writePointToFile(mod,hold[mod]);
}

void processData(const RcdRecord &arena) {

  SubAccessor accessor(arena);

  if(arena.recordType()==RcdHeader::runStart) {
    _beamRun = false;
    newRun=true;
    mapped=false;
    runHeader=arena;
    runHeader.print(std::cout) << std::endl;
    resetValues();
    resetHold();

    std::vector<const DaqRunStart*>
      v(accessor.access<DaqRunStart >());
    
    if(v.size()>0) {
      //    arena.RcdHeader::print(std::cout) << std::endl;
      for(unsigned i(0);i<v.size();i++) {
	//v[i]->print(std::cout," ") << std::endl;
	DaqRunType type = v[i]->runType();
	if (type.beamType()) {
	  std::cout << "This run is a beam run, assuming 3 type configuration" << std::endl;
	  _beamRun=true;
	}
      }

      
    }
  }


  if (newRun) {
    if (extern_mapping) {
      arena.RcdHeader::print(std::cout) << std::endl;
      mapping.print(std::cout," ") << std::endl;
      mapped = true;
      newRun = false;
      for (unsigned mod=1;mod<=MAXMODULE;mod++) _newHold[mod] = true;
      _events = 0;
    }
    else {
      std::vector<const AhcMapping*>
	v(accessor.access<AhcMapping >());
      
      if(v.size()>0) {
	newRun=false;
	mapped=true;
	for (unsigned mod=1;mod<=MAXMODULE;mod++) _newHold[mod] = true;
	std::ostringstream warnText;
	warnText << "found "<< v.size() <<" mappings, using last:" << std::endl;
	if (v.size()>1) printOut(std::string("WARNING:"),warnText.str());
	arena.RcdHeader::print(std::cout) << std::endl;
	for(unsigned i(0);i<v.size();i++) 
	  v[i]->print(std::cout," ") << std::endl;
	mapping = *(v[v.size()-1]);
      }
    }
  }

  /*
  if (arena.recordType()==RcdHeader::configurationStart) {
    configCounter++;
  }
  */

  if (mapped) {

    if (arena.recordType()==RcdHeader::configurationStart ){

      std::vector<const DaqConfigurationStart*>
	vConf(accessor.access< DaqConfigurationStart >());
    
      for(unsigned i(0);i<vConf.size();i++) {
	_configurationNumber = vConf[i]->configurationNumberInRun();
	//	std::cout << _configurationNumber << std::endl;
	printOut(std::string("config"),_configurationNumber,true);
      }

      std::vector<const CrcLocationData<CrcFeConfigurationData>*>
	vc(accessor.access< CrcLocationData<CrcFeConfigurationData> >());      
      
      //      printOut("size of feConfig vector",vc.size(),true);

      for(unsigned i(0);i<vc.size();i++) {

	if (vc[i]->crateNumber()!=0xac) continue; // ignore ECAL

	unsigned slot = vc[i]->slotNumber();
	unsigned fe = vc[i]->crcComponent();

	if (vc[i]->label()!=0) {
	  //	  print("skipped write Fe configuration record, accept only read records   x",i,true);
	  //	  arena.RcdHeader::print(std::cout) << std::endl;
	  //       vc[i]->print(std::cout," ") << std::endl;
	  continue;
	}

	//	if (_events%1001==0) printOut(std::string("hold"),vc[i]->data()->holdStart(),true);

	unsigned module;

	if (slot == 7 && (fe == 4 || fe == 6)) {   //TCMT hack
	  if (fe == 4) module = MAXMODULE-1;
	  else module = MAXMODULE;
	}
	else if (slot == 7 && fe == 2) {
	  module = MAXMODULE-2;
	}
	else  module = mapping.module(slot,fe);

	if (module!=999) {
	  if (_hold[module]!= vc[i]->data()->holdStart() && !_newHold[module]) {
	    printOut("hold changed for module",module,false);
	    if ( (_beamRun && _configurationNumber%3==0) || !_beamRun) {
	      //std::cout << "Writing point. Configuration: " << _configurationNumber << ", module: " << module << ", hold value: " << _hold[module] << std::endl; 
	      writePointToFile(module,_hold[module]);
	    }
	  }
	  _newHold[module]=false;
	  _hold[module] = vc[i]->data()->holdStart();

	}
      }
      
    }


    if (arena.recordType()==RcdHeader::event){
      _events++;
      if (_events%500==0) printOut(std::string("event"),_events,true);
      std::vector<const CrcLocationData<CrcVlinkEventData>* > v(accessor.extract< CrcLocationData<CrcVlinkEventData> >());


      for(unsigned i(0);i<v.size();i++) {
	//	v[i]->print(std::cout," ") << std::endl;

	unsigned slot = v[i]->slotNumber();
	if (v[i]->crateNumber()!=0xac) continue;
	for(unsigned fe(0);fe<8;fe++) {

	  unsigned module;
	  if (slot == 7 && (fe == 4 || fe == 6)) {   //TCMT hack
	    if (fe == 4) module = MAXMODULE-1;
	    else module = MAXMODULE;
	  }
	  else if (slot == 7 && fe == 2) {
	    module = MAXMODULE-2;
	  }
	  else  module = mapping.module(slot,fe);

	  if (module!=999) {
	    const CrcVlinkFeData *fd(v[i]->data()->feData(fe));
	    if(fd!=0) {
	      for(unsigned chan(0);chan<v[i]->data()->feNumberOfAdcSamples(fe) && chan<18;chan++) {
		const CrcVlinkAdcSample *as(fd->adcSample(chan));
		if(as!=0) {
		  for(unsigned chip(0);chip<12;chip++) {
		    if (_beamRun) {
		      if ( _configurationNumber%3==0) {
			//			std::cout << "getting pedestal for fe "<<fe<<" chip " << chip << " chan " << chan << " with " << as->adc(chip) << std::endl; 
			calculatePedestal(module,chip,chan,as->adc(chip));
		      }
		      else if ( _configurationNumber%3==2) {
			//			std::cout << "getting beam for fe "<<fe<<" chip " << chip << " chan " << chan << " with " << as->adc(chip) << std::endl; 
			calculate(module,chip,chan,as->adc(chip) - (int)pedestal(module,chip,chan));
		      }
		    }
		    else {
		      //		      std::cout << "not beam type" << std::endl;
		      calculate(module,chip,chan,as->adc(chip));
		    }
		  }
		}
	      }
	    }
	  }
	  
	}
      }
     }

  }

  if(arena.recordType()==RcdHeader::runEnd) {
    newRun=false;
    writePointToFile(_hold);
    printOut("End of run\n",_runNumber,true);
  }    
}

int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);
  const unsigned vnum(argh.lastArgument(999999));
  const std::string mappingFile(argh.optionArgument('m',"","index file containing all mapping files to be searched"));

  //  _printmode = "";
  _lastMessage = std::string("keine message"); 
  RcdArena arena;
  extern_mapping = false;
  newRun=false;
  mapped=false;

  resetFile();

  _lastPedConfig = 1; // forces first reset as pedestal should never be 1 ( conf%3==0 is pedestal)

  _runNumber = vnum;
  _beamRun = false;
  RunReader reader;
  assert(reader.open(vnum));

  if (mappingFile != "") {
    if (reader.read(arena)) {
      time_t time = arena.recordTime().seconds();

      std::cout << "timestamp of runfile: " << time << std::endl;

      mappingFromFile *extMappingReader = new mappingFromFile();
      extMappingReader->readMapping(mappingFile);

      assert(extMappingReader->getMapping(time,&mapping));
      extern_mapping = true;
     
    
      delete extMappingReader;
      
      processData(arena);
    } 
    else {
      std::cout << "error reading file or empty file" << std::cout;
      assert(false);
    }


  }
  

  while(reader.read(arena)) processData(arena);
  assert(reader.close());


  return 0;
}
