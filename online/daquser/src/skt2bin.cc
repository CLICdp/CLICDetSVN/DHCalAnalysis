#include <iostream>
#include <sstream>

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdReaderSkt.hh"
#include "RcdWriterBin.hh"

using namespace std;

int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);
  argh.print(cout);
  const unsigned runNumber(argh.lastArgument(999999));
  
  RcdArena &arena(*(new RcdArena));
  RcdReaderSkt reader;
  RcdWriterBin writer;

  ostringstream sout;
  sout << "data/dat/Run" << runNumber;

  //reader.open("ecal-daq.desy.de");
  reader.open("131.169.61.122");
  writer.open(sout.str());

  while(reader.read(arena) && writer.write(arena));

  reader.close();
  writer.close();
}
