#include "runnerDefine.icc"

#include <iostream>
#include <fstream>
#include <sstream>

#include "UtlArguments.hh"

#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "RunReader.hh"
#include "RunMonitor.hh"

#include "ChkCount.hh"
#include "HstGeneric.hh"

using namespace std;


int main(int argc, const char **argv) {
  UtlArguments argh(argc,argv);

  const unsigned hstBits(argh.optionArgument('b',HstGeneric::defaultHstGenericBits,
					     "Histogram selection bits"));
  const unsigned printLevel(argh.optionArgument('p',0,"Print level"));
  const unsigned numberOfRecords(argh.optionArgument('r',0xffffffff,
						     "Number of records"));
  const unsigned runNumber(argh.lastArgument(999999));

  if(argh.help()) return 0;

  cout << "Histogram selection bits set to " << printHex(hstBits) << endl;
  cout << "Print level set to " << printLevel << endl;
  cout << "Number of records set to " << numberOfRecords << endl;
  cout << "Run number set to " << runNumber << endl;
  cout << endl;

  RunReader theReader;
  RunReader *reader(&theReader);

  ChkCount hn;
  hn.printLevel(printLevel);
  RunMonitor rm;
  rm.printLevel(printLevel);

  HstGeneric hp(hstBits);
  hp.printLevel(printLevel);
  hp.ignorRunType(true);
  hp.updatePeriod(10);

  RcdArena &arena(*(new RcdArena));
  unsigned iRecords(0);

  assert(reader->open(runNumber));

  while(reader->read(arena) && iRecords<numberOfRecords) {
    iRecords++;
    assert(rm.record(arena));
    assert(hn.record(arena));
    assert(hp.record(arena));
  }
  
  assert(reader->close());

  //arena.initialise(RcdHeader::shutDown);
  //assert(hn.record(arena));
  //assert(hp.record(arena));

  return 0;
}
