#include <cstdlib>
#include <iostream>

#include "UtlArguments.hh"
#include "MpsLaserCoordinates.hh"


int main(int argc, const char **argv) {

  UtlArguments argh(argc,argv);

  const bool stageCoordinates(argh.option('s',"Stage coordinates"));
  const bool pixelCoordinates(argh.option('p',"Pixel coordinates"));
  const bool localCoordinates(argh.option('l',"Local coordinates"));

  if(argh.help()) return 0;
  if(argh.numberOfArguments()!=3) return 1;

  MpsLaserCoordinates c;
  c.print() << std::endl;

  if(stageCoordinates) {
    const int stageX(argh.argument(1,0));
    const int stageY(argh.argument(2,0));

    const double localX(c.localX(stageX,stageY));
    const double localY(c.localY(stageX,stageY));
    
    const unsigned pixelX(c.pixelX(stageX,stageY));
    const unsigned pixelY(c.pixelY(stageX,stageY));

    std::cout << "Stage coordinates " << stageX << ", " << stageY << std::endl;
    std::cout << "Local coordinates " << localX << ", " << localY << std::endl;
    std::cout << "Pixel coordinates " << pixelX << ", " << pixelY << std::endl;
  }

  if(localCoordinates) {
    const double localX(argh.argument(1,0.0));
    const double localY(argh.argument(2,0.0));
    
    const int stageX(c.stageX(localX,localY));
    const int stageY(c.stageY(localX,localY));

    const unsigned pixelX(c.pixelX(localX));
    const unsigned pixelY(c.pixelY(localY));

    std::cout << "Stage coordinates " << stageX << ", " << stageY << std::endl;
    std::cout << "Local coordinates " << localX << ", " << localY << std::endl;
    std::cout << "Pixel coordinates " << pixelX << ", " << pixelY << std::endl;
  }

  if(pixelCoordinates) {
    const unsigned pixelX(argh.argument(1,0));
    const unsigned pixelY(argh.argument(2,0));

    const double localX(c.localX(pixelX));
    const double localY(c.localY(pixelY));

    const int stageX(c.stageX(pixelX,pixelY));
    const int stageY(c.stageY(pixelX,pixelY));

    std::cout << "Stage coordinates " << stageX << ", " << stageY << std::endl;
    std::cout << "Local coordinates " << localX << ", " << localY << std::endl;
    std::cout << "Pixel coordinates " << pixelX << ", " << pixelY << std::endl;
  }

  return 0;
}
