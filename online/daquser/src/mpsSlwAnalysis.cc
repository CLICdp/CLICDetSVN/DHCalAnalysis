#include "runnerDefine.icc"

#include <iostream>
#include <sstream>
#include <vector>

#include "TFile.h"
#include "TH2F.h"
#include "TProfile.h"

#include "UtlArguments.hh"
#include "RcdArena.hh"
#include "RcdCount.hh"
#include "RunReader.hh"
#include "SubAccessor.hh"

using namespace std;


int main(int argc, const char **argv) {
  bool cern(false);

  // Get run number as last (=only) argument
  /*
    UtlArguments argh(argc,argv);
    const unsigned numberOfRuns(argh.optionArgument('n',1,"Number of runs"));
    const unsigned runnum(argh.lastArgument(999999,"Run number"));

    if(argh.help()) return 0;
  */
  int st,nd;
  double tMax;

  // CERN Aug 2009
  if(cern) {
    st=1250118000;
    nd=15;
    tMax=45.0;

    // DESY Mar 2010
  } else {
    //st=1265328000; // RAL
    //nd=30;
    st=1268175600; // DESY
    nd=22;
    tMax=30.0;
  }

  time_t tst(st);
  UtlTime startTime(st,0);
  startTime.print(cout);
  /*
    for(int f(1);f<argc;f++) {
    std::cout << "File " << f << " = " << argv[f] << std::endl;
    }
  */

  TFile* _rootFile;
  _rootFile = new TFile("MpsSlwAnalysis.root","RECREATE");

  // Define memory space for records
  RcdArena &arena(*(new RcdArena));

  // This can handle slw files
  RcdReaderBin reader;

  // Create simple record type counter
  RcdCount counter;

  //std::ofstream fout("RunSummary.txt");
  unsigned nRun, nBnt, nPmt;

  std::vector<unsigned> vSens;
  std::vector<TH2F*> vTH2F;
  std::vector<TProfile*> vProf;

  std::string title;
  title=std::string(";Time since ")+ctime(&tst)+" (days);Bunch train rate (Hz)";
  TH1F *hLive=new TH1F("hLive",title.c_str(),nd*24,0.0,nd);

  std::string title2;
  title2=std::string(";Time since ")+ctime(&tst)+" (days);Number of all bunch trains";
  TH1F *hLiv2=new TH1F("hLiv2",title2.c_str(),nd*24,0.0,nd);

  std::string title3;
  title3=std::string(";Time since ")+ctime(&tst)+" (days);Number of run-active bunch trains";
  TH1F *hLiv3=new TH1F("hLiv3",title3.c_str(),nd*24,0.0,nd);

  std::string tipmt;
  tipmt=std::string(";Time since ")+ctime(&tst)+" (days);PMT coincidence rate (Hz)";
  TH1F *hPMT1=new TH1F("hPMT1",tipmt.c_str(),nd*24,0.0,nd);

  std::string tipmt2;
  tipmt2=std::string(";Time since ")+ctime(&tst)+" (days);Number of PMT coincidences";
  TH1F *hPMT2=new TH1F("hPMT2",tipmt2.c_str(),nd*24,0.0,nd);

  std::string tipmt3;
  tipmt3=std::string(";Time since ")+ctime(&tst)+" (days);Number of PMT coincidences to disk";
  TH1F *hPMT3=new TH1F("hPMT3",tipmt3.c_str(),nd*24,0.0,nd);

  bool toDisk(false);
  bool runEndSeen(true);

  for(int n(1);n<argc;n++) {
    std::string file(argv[n]);
    file.replace(file.length()-4,file.length(),"");

    // Open run file using reader
    if(reader.open(file)) {

      
      // Loop over all records until end-of-file
      while(reader.read(arena)) {
	
	// Increment counter
	counter+=arena;
	
	if(arena.recordType()==RcdHeader::runStart) {
	  //double dt((arena.recordTime()-startTime).deltaTime()/(24.0*3600.0)); // in days

	  //	  if(!runEndSeen && toDisk) fout << nRun << " " << std::setw(10) << nBnt << " " << std::setw(10) << nPmt << std::endl;
	  runEndSeen=false;

	  SubAccessor accessor(arena);    
	  
	  std::vector<const IlcRunStart*>
	    v(accessor.access<IlcRunStart>());
	  //assert(v.size()==1);

	  if(v.size()==1) {
	    cout << "Run " << v[0]->runNumber() << std::endl;
	  	  
	    toDisk=v[0]->runType().writeRun();
	    nRun=v[0]->runNumber();
	    nBnt=0;
	    nPmt=0;
	    
	  } else {
	    arena.RcdHeader::print(cout) << endl;
	  }
	}

	if(arena.recordType()==RcdHeader::configurationEnd) {
	  double dt((arena.recordTime()-startTime).deltaTime()/(24.0*3600.0)); // in days
	  if(dt>0.0) {
	    SubAccessor accessor(arena);    
	    
	    std::vector<const IlcConfigurationEnd*>
	      v(accessor.access<IlcConfigurationEnd>());
	    //assert(v.size()==1);

	    if(v.size()==1) {
	      hLive->Fill(dt,v[0]->actualNumberOfBunchTrainsInConfiguration()/3600.0);
	      int lo(hLiv2->FindBin(dt));
	      for(int i(lo);i<=nd*24;i++) {
		hLiv2->Fill(hLiv2->GetBinCenter(i),v[0]->actualNumberOfBunchTrainsInConfiguration());
		if(toDisk) hLiv3->Fill(hLiv3->GetBinCenter(i),v[0]->actualNumberOfBunchTrainsInConfiguration());
	      }

	    } else {
	      arena.RcdHeader::print(cout) << endl;
	    }
	  }
	}

	if(arena.recordType()==RcdHeader::slowReadout) {
	  //arena.RcdHeader::print(cout) << endl;
	  double dt((arena.recordTime()-startTime).deltaTime()/(24.0*3600.0)); // in days
	  
	  SubAccessor accessor(arena);    
	  
	  std::vector<const MpsLocationData<MpsPcb1SlowReadoutData>*>
	    v(accessor.access<MpsLocationData<MpsPcb1SlowReadoutData> >());

	  for(unsigned i(0);i<v.size();i++) {
	    //v[i]->print(cout) << std::endl;
	    
	    unsigned k(999);
	    for(unsigned j(0);j<vSens.size() && k==999;j++) {
	      if(vSens[j]==v[i]->sensorId()) k=j;
	    }
	    if(k==999) {
	      std::cout << "New sensor found = " << (unsigned)v[i]->sensorId() << std::endl;
	      k=vSens.size();
	      vSens.push_back(v[i]->sensorId());

	      std::ostringstream sout;
	      sout << (unsigned)v[i]->sensorId();

	      std::string title;
	      title="Sensor "+sout.str()+"; Time since "+ctime(&tst)+" (days);Temperature (C)";

	      vTH2F.push_back(
			      new TH2F((std::string("hSensor")+sout.str()).c_str(),
				       title.c_str(),
				       nd*24,0.0,nd,100,15.0,tMax));
	      vProf.push_back(
			      new TProfile((std::string("pSensor")+sout.str()).c_str(),
					   title.c_str(),
					   nd*24,0.0,nd));
	    }

	    vTH2F[k]->Fill(dt,v[i]->data()->celcius());
	    vProf[k]->Fill(dt,v[i]->data()->celcius());
	  }	  
	}
	//
	/*
	  
	std::vector<const IlcRunStart*>
	vs(accessor.access<IlcRunStart>());
	assert(vs.size()==1);
	runStart=*(vs[0]);
	//runStart.print(fout) << std::endl;

	vConfig.clear();
	for(unsigned i(0);i<v.size();i++) {
	if(!v[i]->usbDaqMasterFirmware()) {
	//v[i]->print(fout) << std::endl;

	MpsLocationData<MpsSensorV12ConfigurationData> d;
	d.location(v[i]->location());
	d.data()->maskSensor(false);
	d.data()->trimSensor(0);
	vConfig.push_back(d);
	}
	}
	  
	for(unsigned i(0);i<vConfig.size();i++) {
	//vConfig[i].print() << std::endl;
	}
	}
	*/

	if(arena.recordType()==RcdHeader::runEnd) {
	  //arena.RcdHeader::print(fout) << endl;
	  runEndSeen=true;
	  //if(toDisk) fout << nRun << " " << std::setw(10) << nBnt << " " << std::setw(10) << nPmt << std::endl;

	  SubAccessor accessor(arena);    
	  /*
	    std::vector<const MpsLocationData<MpsSensorV12ConfigurationData>*>
	    v(accessor.access<MpsLocationData<MpsSensorV12ConfigurationData> >());

	    for(unsigned i(0);i<v.size();i++) {
	    //v[i]->print(std::cout) << std::endl;
	    assert(!v[i]->write());

	    unsigned j(999);
	    for(unsigned k(0);k<vConfig.size();k++) {
	    if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
	    }
	    assert(j!=999);

	    unsigned nError(0);
	    for(unsigned x(0);x<168;x++) {
	    for(unsigned y(0);y<168;y++) {

	    // Exclude known bad configuration columns
	    if((v[i]->sensorId()==26 && x==134) ||
	    (v[i]->sensorId()==32 && x==128) ||
	    (v[i]->sensorId()==41 && x==117) ||
	    (v[i]->sensorId()==41 && x==129)) {

	    } else {
	    if(vConfig[j].data()->trim(x,y)!=v[i]->data()->trim(x,y)) nError++;
	    }
	    }
	    }

	    if(nError>0) {
	    cerr << std::endl
	    << "ERROR Run " << runStart.runNumber()
	    << " sensor " << (unsigned)v[i]->sensorId()
	    << " runEnd number of errors " << nError << std::endl;
	    fout << std::endl
	    << "ERROR Run " << runStart.runNumber()
	    << " sensor " << (unsigned)v[i]->sensorId()
	    << " runEnd number of errors " << nError << std::endl;
	    vConfig[j].print(fout,"WRITE ") << std::endl;
	    v[i]->print(fout,"READ  ") << std::endl;
	    }
	    }
	  */

	}
	/*	
	  if(arena.recordType()==RcdHeader::configurationStart) {
	  //arena.RcdHeader::print(fout) << endl;

	  SubAccessor accessor(arena);    

	  std::vector<const MpsLocationData<MpsPcb1ConfigurationData>*>
	  v(accessor.access<MpsLocationData<MpsPcb1ConfigurationData> >());
	  for(unsigned i(0);i<v.size();i++) {
	  //fout << std::setw(6) << v[i]->data()->region01ThresholdValue();
	  }



	  std::vector<const MpsLocationData<MpsSensorV12ConfigurationData>*>
	  v(accessor.access<MpsLocationData<MpsSensorV12ConfigurationData> >());

	  for(unsigned i(0);i<v.size();i++) {
	  //v[i]->print(std::cout) << std::endl;

	  if(!v[i]->write()) {
	  unsigned j(999);
	  for(unsigned k(0);k<vConfig.size();k++) {
	  if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
	  }
	  assert(j!=999);

	  unsigned nError(0);
	  for(unsigned x(0);x<168;x++) {
	  for(unsigned y(0);y<168;y++) {

	  // Exclude known bad configuration columns
	  if((v[i]->sensorId()==26 && x==134) ||
	  (v[i]->sensorId()==32 && x==128) ||
	  (v[i]->sensorId()==41 && x==117) ||
	  (v[i]->sensorId()==41 && x==129)) {
		    
	  } else {
	  if(vConfig[j].data()->trim(x,y)!=v[i]->data()->trim(x,y)) nError++;
	  }
	  }
	  }

	  if(nError>0) {
	  cerr << std::endl
	  << "ERROR Run " << runStart.runNumber()
	  << " sensor " << (unsigned)v[i]->sensorId()
	  << " configurationStart number of errors " << nError << std::endl;
	  fout << std::endl
	  << "ERROR Run " << runStart.runNumber()
	  << " sensor " << (unsigned)v[i]->sensorId()
	  << " configurationStart number of errors " << nError << std::endl;
	  vConfig[j].print(fout,"WRITE ") << std::endl;
	  v[i]->print(fout,"READ  ") << std::endl;
	  }
	  }
	  }

	  for(unsigned i(0);i<v.size();i++) {
	  if(v[i]->write()) {
	  if(v[i]->sensorBroadcast()) {
	  for(unsigned k(0);k<vConfig.size();k++) {
	  vConfig[k].data(*(v[i]->data()));
	  }
	  }
	  }
	  }
	  for(unsigned i(0);i<v.size();i++) {
	  if(v[i]->write()) {
	  if(!v[i]->sensorBroadcast()) {

	  unsigned j(999);
	  for(unsigned k(0);k<vConfig.size();k++) {
	  if(vConfig[k].sensorId()==v[i]->sensorId()) j=k;
	  }
	  assert(j!=999);
	  vConfig[j].data(*(v[i]->data()));
	  }
	  }
	  }

	  for(unsigned i(0);i<vConfig.size();i++) {
	  //vConfig[i].print() << std::endl;
	  }
	  }
	  }

	  if(!runEndSeen) {
	  std::cerr << std::endl
	  << "ERROR Run " << runStart.runNumber()
	  << " no end of run config check" << std::endl;
	  fout << std::endl
	  << "ERROR Run " << runStart.runNumber()
	  << " no end of run config check" << std::endl;
	  }
	  }
	*/

	if(arena.recordType()==RcdHeader::bunchTrain) {
	  //arena.RcdHeader::print(cout) << endl;
	  double dt((arena.recordTime()-startTime).deltaTime()/(24.0*3600.0)); // in days
	  if(dt>0.0) {
	  
	    SubAccessor accessor(arena);    
	  
	    std::vector<const MpsLocationData<MpsUsbDaqBunchTrainData>*>
	      v(accessor.access<MpsLocationData<MpsUsbDaqBunchTrainData> >());
	    //assert(v.size()==1);
	    if(v.size()==1) {
	      const MpsUsbDaqBunchTrainDatum *d(v[0]->data()->data());
	      
	      unsigned nc(0);
	      for(unsigned i(0);i<v[0]->data()->numberOfTags();i++) {
		if(d[i].channel(0) && d[i].channel(1)) nc++;
	      }
	      
	      if(nc>0) {
		hPMT1->Fill(dt,nc/3600.0);
		int lo(hPMT2->FindBin(dt));
		for(int i(lo);i<=nd*24;i++) {
		  hPMT2->Fill(hPMT2->GetBinCenter(i),nc);
		  if(toDisk) hPMT3->Fill(hPMT3->GetBinCenter(i),nc);
		}
	      }
	    } else {
	      arena.RcdHeader::print(cout) << endl;
	    }
	  }
	}
      }

      // Close run file
      assert(reader.close());
    }
  }      
  // Print total of counts
  cout << endl;
  counter.print(cout);

  //fout.close();
  
  if(_rootFile!=0) {
    _rootFile->Write();
    _rootFile->Close();
    delete _rootFile;
    _rootFile=0;
  }

  return 0;
}
