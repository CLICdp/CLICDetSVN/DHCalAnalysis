#ifndef ChkPrint_HH
#define ChkPrint_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"
#include "SubRecordType.hh"
#include "RcdUserRO.hh"


class ChkPrint : public RcdUserRO {

public:
  ChkPrint() : RcdUserRO(9) {
    enableSubrecords(false);
    enableType(false);
    enable(false);
  }

  virtual ~ChkPrint() {
  }

  void enableSubrecords(bool e) {
    _selectSubrecords=e;
  }

  void enableType(bool e) {
    for(unsigned i(0);i<RcdHeader::endOfRecordTypeEnum;i++) _selectType[i]=e;
  }

  void enableType(RcdHeader::RecordType t, bool e) {
    _selectType[t]=e;
  }

  void enable(bool e) {
    for(unsigned i(0);i<65536;i++) _select[i]=e;
  }

  void enable(SubHeader::Group g, bool e) {
    for(unsigned i(0);i<4096;i++) _select[g+i]=e;
  }

  void enable(unsigned short n, bool e) {
    _select[n]=e;
  }

  bool record(const RcdRecord &r) {

    SubAccessor accessor(r);

    // Print out header
    if(_selectType[r.recordType()]) r.RcdHeader::print(std::cout) << std::endl;

    // Print out SubHeader information for all subrecords
    if(_selectSubrecords) accessor.print(std::cout) << std::endl;

    // Print out each subrecord individually
    // The template below should make this simpler but does not compile!

    // daq subrecords

    if(_select[subRecordType< DaqRunStart >()]) {
      std::vector<const DaqRunStart*>
	v(accessor.access<DaqRunStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< DaqRunEndV0 >()]) {
      std::vector<const DaqRunEndV0*>
	v(accessor.access<DaqRunEndV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqRunEndV1 >()]) {
      std::vector<const DaqRunEndV1*>
	v(accessor.access<DaqRunEndV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationStartV0 >()]) {
      std::vector<const DaqConfigurationStartV0*>
	v(accessor.access<DaqConfigurationStartV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationStartV1 >()]) {
      std::vector<const DaqConfigurationStartV1*>
	v(accessor.access<DaqConfigurationStartV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationStartV2 >()]) {
      std::vector<const DaqConfigurationStartV2*>
	v(accessor.access<DaqConfigurationStartV2>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationEndV0 >()]) {
      std::vector<const DaqConfigurationEndV0*>
	v(accessor.access<DaqConfigurationEndV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationEndV1 >()]) {
      std::vector<const DaqConfigurationEndV1*>
	v(accessor.access<DaqConfigurationEndV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqAcquisitionStartV0 >()]) {
      std::vector<const DaqAcquisitionStartV0*>
	v(accessor.access<DaqAcquisitionStartV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqAcquisitionStartV1 >()]) {
      std::vector<const DaqAcquisitionStartV1*>
	v(accessor.access<DaqAcquisitionStartV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqAcquisitionEndV0 >()]) {
      std::vector<const DaqAcquisitionEndV0*>
	v(accessor.access<DaqAcquisitionEndV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqAcquisitionEndV1 >()]) {
      std::vector<const DaqAcquisitionEndV1*>
	v(accessor.access<DaqAcquisitionEndV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqEvent >()]) {
      std::vector<const DaqEvent*>
	v(accessor.access<DaqEvent>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSpillStart >()]) {
      std::vector<const DaqSpillStart*>
	v(accessor.access<DaqSpillStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSpillEnd >()]) {
      std::vector<const DaqSpillEnd*>
	v(accessor.access<DaqSpillEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqTransferStart >()]) {
      std::vector<const DaqTransferStart*>
	v(accessor.access<DaqTransferStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqTransferEnd >()]) {
      std::vector<const DaqTransferEnd*>
	v(accessor.access<DaqTransferEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqMessage >()]) {
      std::vector<const DaqMessage*>
	v(accessor.access<DaqMessage>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSoftware >()]) {
      std::vector<const DaqSoftware*>
	v(accessor.access<DaqSoftware>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSlowReadout >()]) {
      std::vector<const DaqSlowReadout*>
	v(accessor.access<DaqSlowReadout>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSequenceStart >()]) {
      std::vector<const DaqSequenceStart*>
	v(accessor.access<DaqSequenceStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqSequenceEnd >()]) {
      std::vector<const DaqSequenceEnd*>
	v(accessor.access<DaqSequenceEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqTwoTimer >()]) {
      std::vector<const DaqTwoTimer*>
	v(accessor.access<DaqTwoTimer>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqMultiTimer >()]) {
      std::vector<const DaqMultiTimer*>
	v(accessor.access<DaqMultiTimer>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqTrigger >()]) {
      std::vector<const DaqTrigger*>
	v(accessor.access<DaqTrigger>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqBurstStart >()]) {
      std::vector<const DaqBurstStart*>
	v(accessor.access<DaqBurstStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqBurstEnd >()]) {
      std::vector<const DaqBurstEnd*>
	v(accessor.access<DaqBurstEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< IlcRunStart >()]) {
      std::vector<const IlcRunStart*>
	v(accessor.access<IlcRunStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< IlcRunEnd >()]) {
      std::vector<const IlcRunEnd*>
	v(accessor.access<IlcRunEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< IlcConfigurationStart >()]) {
      std::vector<const IlcConfigurationStart*>
	v(accessor.access<IlcConfigurationStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< IlcConfigurationEnd >()]) {
      std::vector<const IlcConfigurationEnd*>
	v(accessor.access<IlcConfigurationEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< IlcBunchTrain >()]) {
      std::vector<const IlcBunchTrain*>
	v(accessor.access<IlcBunchTrain>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< IlcSlowReadout >()]) {
      std::vector<const IlcSlowReadout*>
	v(accessor.access<IlcSlowReadout>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }


    // crc subrecords

    if(_select[subRecordType< CrcLocationData<CrcVmeRunData> >()]) {
      std::vector<const CrcLocationData<CrcVmeRunData>*>
	v(accessor.access<CrcLocationData<CrcVmeRunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcVmeConfigurationData> >()]) {
      std::vector<const CrcLocationData<CrcVmeConfigurationData>*>
	v(accessor.access<CrcLocationData<CrcVmeConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcVmeEventData> >()]) {
      std::vector<const CrcLocationData<CrcVmeEventData>*>
	v(accessor.access<CrcLocationData<CrcVmeEventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcBeRunData> >()]) {
      std::vector<const CrcLocationData<CrcBeRunData>*>
	v(accessor.access<CrcLocationData<CrcBeRunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcBeConfigurationDataV0> >()]) {
      std::vector<const CrcLocationData<CrcBeConfigurationDataV0>*>
	v(accessor.access<CrcLocationData<CrcBeConfigurationDataV0> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcBeConfigurationDataV1> >()]) {
      std::vector<const CrcLocationData<CrcBeConfigurationDataV1>*>
	v(accessor.access<CrcLocationData<CrcBeConfigurationDataV1> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcBeEventDataV0> >()]) {
      std::vector<const CrcLocationData<CrcBeEventDataV0>*>
	v(accessor.access<CrcLocationData<CrcBeEventDataV0> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcBeEventDataV1> >()]) {
      std::vector<const CrcLocationData<CrcBeEventDataV1>*>
	v(accessor.access<CrcLocationData<CrcBeEventDataV1> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcFeRunData> >()]) {
      std::vector<const CrcLocationData<CrcFeRunData>*>
	v(accessor.access<CrcLocationData<CrcFeRunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcFeConfigurationData> >()]) {
      std::vector<const CrcLocationData<CrcFeConfigurationData>*>
	v(accessor.access<CrcLocationData<CrcFeConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcFeEventData> >()]) {
      std::vector<const CrcLocationData<CrcFeEventData>*>
	v(accessor.access<CrcLocationData<CrcFeEventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcVlinkEventData> >()]) {
      std::vector<const CrcLocationData<CrcVlinkEventData>*>
	v(accessor.access<CrcLocationData<CrcVlinkEventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
	v[i]->data()->print(std::cout,"",true) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcFeFakeEventData> >()]) {
      std::vector<const CrcLocationData<CrcFeFakeEventData>*>
	v(accessor.access<CrcLocationData<CrcFeFakeEventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcReadoutConfigurationDataV0 >()]) {
      std::vector<const CrcReadoutConfigurationDataV0*>
	v(accessor.access<CrcReadoutConfigurationDataV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcReadoutConfigurationDataV1 >()]) {
      std::vector<const CrcReadoutConfigurationDataV1*>
	v(accessor.access<CrcReadoutConfigurationDataV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    

    // emc subrecords

    if(_select[subRecordType< CrcLocationData<EmcFeConfigurationData> >()]) {
      std::vector<const CrcLocationData<EmcFeConfigurationData>*>
	v(accessor.access<CrcLocationData<EmcFeConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }


    // ahc subrecords

    if(_select[subRecordType< CrcLocationData<AhcFeConfigurationDataV0> >()]) {
      std::vector<const CrcLocationData<AhcFeConfigurationDataV0>*>
	v(accessor.access<CrcLocationData<AhcFeConfigurationDataV0> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<AhcFeConfigurationDataV1> >()]) {
      std::vector<const CrcLocationData<AhcFeConfigurationDataV1>*>
	v(accessor.access<CrcLocationData<AhcFeConfigurationDataV1> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<AhcVfeConfigurationDataFine> >()]) {
      std::vector<const CrcLocationData<AhcVfeConfigurationDataFine>*>
       v(accessor.access<CrcLocationData<AhcVfeConfigurationDataFine> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<AhcVfeStartUpDataFine> >()]) {
      std::vector<const CrcLocationData<AhcVfeStartUpDataFine>*>
       v(accessor.access<CrcLocationData<AhcVfeStartUpDataFine> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowRunDataV0>()]) {
      std::vector<const AhcSlowRunDataV0*>
       v(accessor.access<AhcSlowRunDataV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowRunDataV1>()]) {
      std::vector<const AhcSlowRunDataV1*>
       v(accessor.access<AhcSlowRunDataV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowConfigurationDataV0>()]) {
      std::vector<const AhcSlowConfigurationDataV0*>
       v(accessor.access<AhcSlowConfigurationDataV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowConfigurationDataV1>()]) {
      std::vector<const AhcSlowConfigurationDataV1*>
       v(accessor.access<AhcSlowConfigurationDataV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowReadoutDataV0>()]) {
      std::vector<const AhcSlowReadoutDataV0*>
       v(accessor.access<AhcSlowReadoutDataV0>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<AhcSlowReadoutDataV1>()]) {
      std::vector<const AhcSlowReadoutDataV1*>
       v(accessor.access<AhcSlowReadoutDataV1>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<AhcVfeStartUpDataCoarse> >()]) {
      std::vector<const CrcLocationData<AhcVfeStartUpDataCoarse>*>
       v(accessor.access<CrcLocationData<AhcVfeStartUpDataCoarse> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<AhcVfeConfigurationDataCoarse> >()]) {
      std::vector<const CrcLocationData<AhcVfeConfigurationDataCoarse>*>
       v(accessor.access<CrcLocationData<AhcVfeConfigurationDataCoarse> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< AhcMapping >()]) {
      std::vector<const AhcMapping*>
       v(accessor.access<AhcMapping>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }


    // sce subrecords

    if(_select[subRecordType<SceSlowReadoutData>()]) {
      std::vector<const SceSlowReadoutData*>
       v(accessor.access<SceSlowReadoutData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<SceSlowTemperatureData>()]) {
      std::vector<const SceSlowTemperatureData*>
       v(accessor.access<SceSlowTemperatureData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }


    // bml subrecords

    if(_select[subRecordType< BmlLc1176RunData >()]) {
      std::vector<const BmlLc1176RunData*>
	v(accessor.access<BmlLc1176RunData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLc1176ConfigurationData >()]) {
      std::vector<const BmlLc1176ConfigurationData*>
	v(accessor.access<BmlLc1176ConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLc1176EventData >()]) {
      std::vector<const BmlLc1176EventData*>
	v(accessor.access<BmlLc1176EventData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlHodRunData >()]) {
      std::vector<const BmlHodRunData*>
	v(accessor.access<BmlHodRunData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlHodEventData >()]) {
      std::vector<const BmlHodEventData*>
	v(accessor.access<BmlHodEventData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767RunData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767RunData>*>
	v(accessor.access<BmlLocationData<BmlCaen767RunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767OpcodeData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767OpcodeData>*>
	v(accessor.access<BmlLocationData<BmlCaen767OpcodeData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767ConfigurationData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767ConfigurationData>*>
	v(accessor.access<BmlLocationData<BmlCaen767ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767TriggerData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767TriggerData>*>
	v(accessor.access<BmlLocationData<BmlCaen767TriggerData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767EventData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767EventData>*>
	v(accessor.access<BmlLocationData<BmlCaen767EventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlCaen767ReadoutConfigurationData >()]) {
      std::vector<const BmlCaen767ReadoutConfigurationData*>
	v(accessor.access<BmlCaen767ReadoutConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767TestData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767TestData>*>
	v(accessor.access<BmlLocationData<BmlCaen767TestData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen767TdcErrorData> >()]) {
      std::vector<const BmlLocationData<BmlCaen767TdcErrorData>*>
	v(accessor.access<BmlLocationData<BmlCaen767TdcErrorData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290RunData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290RunData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290RunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290OpcodeData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290OpcodeData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290OpcodeData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290ConfigurationData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290ConfigurationData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290TriggerData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290TriggerData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290TriggerData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290EventData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290EventData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290EventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlCaen1290ReadoutConfigurationData >()]) {
      std::vector<const BmlCaen1290ReadoutConfigurationData*>
	v(accessor.access<BmlCaen1290ReadoutConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290TestData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290TestData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290TestData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLocationData<BmlCaen1290TdcErrorData> >()]) {
      std::vector<const BmlLocationData<BmlCaen1290TdcErrorData>*>
	v(accessor.access<BmlLocationData<BmlCaen1290TdcErrorData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLalHodoscopeRunData >()]) {
      std::vector<const BmlLalHodoscopeRunData*>
	v(accessor.access<BmlLalHodoscopeRunData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLalHodoscopeConfigurationData >()]) {
      std::vector<const BmlLalHodoscopeConfigurationData*>
	v(accessor.access<BmlLalHodoscopeConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLalHodoscopeTriggerData >()]) {
      std::vector<const BmlLalHodoscopeTriggerData*>
	v(accessor.access<BmlLalHodoscopeTriggerData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< BmlLalHodoscopeEventData >()]) {
      std::vector<const BmlLalHodoscopeEventData*>
	v(accessor.access<BmlLalHodoscopeEventData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }


    // trg subrecords

    if(_select[subRecordType< CrcLocationData<CrcBeTrgRunData> >()]) {
      std::vector<const CrcLocationData<CrcBeTrgRunData>*>
	v(accessor.access<CrcLocationData<CrcBeTrgRunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcBeTrgConfigurationData> >()]) {
      std::vector<const CrcLocationData<CrcBeTrgConfigurationData>*>
	v(accessor.access<CrcLocationData<CrcBeTrgConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcBeTrgEventData> >()]) {
      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
	v(accessor.access<CrcLocationData<CrcBeTrgEventData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcBeTrgPollData> >()]) {
      std::vector<const CrcLocationData<CrcBeTrgPollData>*>
	v(accessor.access<CrcLocationData<CrcBeTrgPollData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<TrgSpillPollData> >()]) {
      std::vector<const CrcLocationData<TrgSpillPollData>*>
	v(accessor.access<CrcLocationData<TrgSpillPollData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< TrgReadoutConfigurationData >()]) {
      std::vector<const TrgReadoutConfigurationData*>
	v(accessor.access<TrgReadoutConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    // mps subrecords

    if(_select[subRecordType< MpsLocationData<MpsUsbDaqRunData> >()]) {
      std::vector<const MpsLocationData<MpsUsbDaqRunData>*>
        v(accessor.access<MpsLocationData<MpsUsbDaqRunData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType<MpsReadoutConfigurationData>()]) {
      std::vector<const MpsReadoutConfigurationData*>
	v(accessor.access<MpsReadoutConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsUsbDaqConfigurationData> >()]) {
      std::vector<const MpsLocationData<MpsUsbDaqConfigurationData>*>
	v(accessor.access<MpsLocationData<MpsUsbDaqConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsUsbDaqMasterConfigurationData> >()]) {
      std::vector<const MpsLocationData<MpsUsbDaqMasterConfigurationData>*>
	v(accessor.access<MpsLocationData<MpsUsbDaqMasterConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsPcb1ConfigurationData> >()]) {
      std::vector<const MpsLocationData<MpsPcb1ConfigurationData>*>
	v(accessor.access<MpsLocationData<MpsPcb1ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsSensorV10ConfigurationData> >()]) {
      std::vector<const MpsLocationData<MpsSensorV10ConfigurationData>*>
	v(accessor.access<MpsLocationData<MpsSensorV10ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsSensorV12ConfigurationData> >()]) {
      std::vector<const MpsLocationData<MpsSensorV12ConfigurationData>*>
	v(accessor.access<MpsLocationData<MpsSensorV12ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsPcb1SlowReadoutData> >()]) {
      std::vector<const MpsLocationData<MpsPcb1SlowReadoutData>*>
	v(accessor.access<MpsLocationData<MpsPcb1SlowReadoutData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsUsbDaqSpillPollData> >()]) {
      std::vector<const MpsLocationData<MpsUsbDaqSpillPollData>*>
        v(accessor.access<MpsLocationData<MpsUsbDaqSpillPollData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsUsbDaqBunchTrainData> >()]) {
      std::vector<const MpsLocationData<MpsUsbDaqBunchTrainData>*>
	v(accessor.access<MpsLocationData<MpsUsbDaqBunchTrainData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsSensor1BunchTrainData> >()]) {
      std::vector<const MpsLocationData<MpsSensor1BunchTrainData>*>
	v(accessor.access<MpsLocationData<MpsSensor1BunchTrainData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLaserRunData >()]) {
      std::vector<const MpsLaserRunData*>
        v(accessor.access<MpsLaserRunData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLaserConfigurationData >()]) {
      std::vector<const MpsLaserConfigurationData*>
        v(accessor.access<MpsLaserConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsDigitisationData >()]) {
      std::vector<const MpsDigitisationData*>
        v(accessor.access<MpsDigitisationData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsAlignmentSensor >()]) {
      std::vector<const MpsAlignmentSensor*>
        v(accessor.access<MpsAlignmentSensor>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< MpsLocationData<MpsEudetBunchTrainData> >()]) {
      std::vector<const MpsLocationData<MpsEudetBunchTrainData>*>
	v(accessor.access<MpsLocationData<MpsEudetBunchTrainData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    // slw subrecords

    if(_select[subRecordType< CrcLocationData<CrcLm82RunData> >()]) {
      std::vector<const CrcLocationData<CrcLm82RunData>*>
	v(accessor.access<CrcLocationData<CrcLm82RunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcLm82ConfigurationData> >()]) {
      std::vector<const CrcLocationData<CrcLm82ConfigurationData>*>
	v(accessor.access<CrcLocationData<CrcLm82ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcLm82SlowReadoutData> >()]) {
      std::vector<const CrcLocationData<CrcLm82SlowReadoutData>*>
       v(accessor.access<CrcLocationData<CrcLm82SlowReadoutData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcAdm1025RunData>  >()]) {
      std::vector<const CrcLocationData<CrcAdm1025RunData>*>
	v(accessor.access<CrcLocationData<CrcAdm1025RunData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType< CrcLocationData<CrcAdm1025ConfigurationData> >()]) {
      std::vector<const CrcLocationData<CrcAdm1025ConfigurationData>*>
       v(accessor.access<CrcLocationData<CrcAdm1025ConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< CrcLocationData<CrcAdm1025SlowReadoutData> >()]) {
      std::vector<const CrcLocationData<CrcAdm1025SlowReadoutData>*>
	v(accessor.access<CrcLocationData<CrcAdm1025SlowReadoutData> >());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    
    if(_select[subRecordType<EmcStageRunData>()]) {
      std::vector<const EmcStageRunData*>
	v(accessor.access<EmcStageRunData>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    
    // dhc subrecords
    
    if(_select[subRecordType< DhcReadoutConfigurationData >()]) {
      std::vector<const DhcReadoutConfigurationData*>
        v(accessor.access<DhcReadoutConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcEventData> >()]) {
      std::vector<const DhcLocationData<DhcEventData>*>
        v(accessor.access<DhcLocationData<DhcEventData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
        //v[i]->data()->print(std::cout,"",true) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcTriggerData> >()]) {
      std::vector<const DhcLocationData<DhcTriggerData>*>
        v(accessor.access<DhcLocationData<DhcTriggerData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
        //v[i]->data()->print(std::cout,"",true) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcBeConfigurationData> >()]) {
      std::vector<const DhcLocationData<DhcBeConfigurationData>*>
        v(accessor.access<DhcLocationData<DhcBeConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcFeConfigurationData> >()]) {
      std::vector<const DhcLocationData<DhcFeConfigurationData>*>
        v(accessor.access<DhcLocationData<DhcFeConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcDcConfigurationData> >()]) {
      std::vector<const DhcLocationData<DhcDcConfigurationData>*>
        v(accessor.access<DhcLocationData<DhcDcConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DhcLocationData<DhcDcRunData> >()]) {
      std::vector<const DhcLocationData<DhcDcRunData>*>
        v(accessor.access<DhcLocationData<DhcDcRunData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< TtmLocationData<TtmConfigurationData> >()]) {
      std::vector<const TtmLocationData<TtmConfigurationData>*>
        v(accessor.access<TtmLocationData<TtmConfigurationData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< TtmLocationData<TtmTriggerData> >()]) {
      std::vector<const TtmLocationData<TtmTriggerData>*>
        v(accessor.access<TtmLocationData<TtmTriggerData> >());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
        //v[i]->data()->print(std::cout,"",true) << std::endl;
      }
    }

    if(_select[subRecordType< DhcTriggerConfigurationData >()]) {
      std::vector<const DhcTriggerConfigurationData*>
        v(accessor.access<DhcTriggerConfigurationData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }


    // sim subrecords

    if(_select[subRecordType< SimDetectorData >()]) {
      std::vector<const SimDetectorData*>
        v(accessor.access<SimDetectorData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< SimPrimaryData >()]) {
      std::vector<const SimPrimaryData*>
        v(accessor.access<SimPrimaryData>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< SimDetectorEnergies >()]) {
      std::vector<const SimDetectorEnergies*>
        v(accessor.access<SimDetectorEnergies>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< SimParticle >()]) {
      std::vector<const SimParticle*>
        v(accessor.access<SimParticle>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< SimLayerTrack >()]) {
      std::vector<const SimLayerTrack*>
        v(accessor.access<SimLayerTrack>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< SimLayerHits >()]) {
      std::vector<const SimLayerHits*>
        v(accessor.access<SimLayerHits>());
      for(unsigned i(0);i<v.size();i++) {
        v[i]->print(std::cout) << std::endl;
      }
    }

    return true;
  }

  /*
  template <class Payload> void doPrint(const RcdRecord &r) const {
    if(_select[subRecordType<Payload>()]) {
      SubAccessor accessor(r);
      std::vector<const Payload*> v;
      v=accessor.access<>(); /// DOES NOT COMPILE!!!
      //v=accessor.access();

      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout,"DOPRINT ") << std::endl;
      }
    }
  }
  */

private:
  bool _selectSubrecords;
  bool _selectType[RcdHeader::endOfRecordTypeEnum];
  bool _select[65536];
};

#endif
