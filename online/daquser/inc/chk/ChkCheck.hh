#ifndef ChkCheck_HH
#define ChkCheck_HH

#include <iostream>
#include <fstream>

#include "CrcVmeRunData.hh"
#include "CrcBeRunData.hh"
#include "CrcBeTrgRunData.hh"
#include "CrcFeRunData.hh"

#include "RcdHeader.hh"
#include "RcdUserRO.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"


class ChkCheckCrc {

public:
  ChkCheckCrc(unsigned c, unsigned s) :
    _crate(c), _slot(s) {
  }

private:
  //const
 unsigned _crate;
  //  const
 unsigned _slot;

  unsigned _beTrg;
  unsigned _be;
  unsigned _fe[8];
};

class ChkCheck : public RcdUserRO {

public:
  ChkCheck(unsigned s) : RcdUserRO(4) {

    _crate=0xac;
    _slot=s;

    for(unsigned s(0);s<=21;s++) {
      for(unsigned i(0);i<100;i++) {
	_counts[0][i]=0;
	_counts[1][i]=0;
      }
    }

    _connum=0;
  }

  virtual ~ChkCheck() {
  }

  bool record(const RcdRecord &r) {
    SubAccessor extracter(r);
    //extracter.print(std::cout);

    /*
    if(_select[subRecordType< DaqRunStart >()]) {
      std::vector<const DaqRunStart*>
	v(extracter.extract<DaqRunStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqRunEnd >()]) {
      std::vector<const DaqRunEnd*>
	v(extracter.extract<DaqRunEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationStart >()]) {
      std::vector<const DaqConfigurationStart*>
	v(extracter.extract<DaqConfigurationStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationEnd >()]) {
      std::vector<const DaqConfigurationEnd*>
	v(extracter.extract<DaqConfigurationEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    */

    switch(r.recordType()) {

    case RcdHeader::runStart: {
      std::vector<const CrcLocationData<CrcVmeRunData>*>
       bted(extracter.extract<CrcLocationData<CrcVmeRunData> >());
      assert(bted.size()>0);
      
      _vChkCheckCrc.clear();
      for(unsigned i(0);i<bted.size();i++) {
	_vChkCheckCrc.push_back(ChkCheckCrc(bted[i]->crateNumber(),bted[i]->slotNumber()));
      }

      break;
    }

    case RcdHeader::configurationStart: {
      std::vector<const CrcReadoutConfigurationData*>
       bted(extracter.extract<CrcReadoutConfigurationData>());
      assert(bted.size()>0);
      
      for(unsigned i(0);i<bted.size();i++) {
	//bted[i]->print(std::cout) << std::endl;

	unsigned c(2);
	if(bted[i]->crateNumber()==0xec) c=0;
	if(bted[i]->crateNumber()==0xce) c=0;
	if(bted[i]->crateNumber()==0xac) c=1;
	assert(c<2);

	_readoutConfigurationData[c]=*(bted[i]);
      }

      break;
    }

    case RcdHeader::configurationEnd: {
      _connum++;
      break;
    }

    case RcdHeader::acquisitionStart: {
      _counter=0;
      _beTrgCounter=0;

      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  _beCounter[c][s]=0;
	  
	  for(unsigned i(0);i<8;i++) {
	    _feCounter[c][s][i]=0;
	    _vlinkCounter[c][s][i]=0;
	  }
	}
      }

      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
       bted(extracter.extract<CrcLocationData<CrcBeTrgEventData> >());
      assert(bted.size()<=1);

      for(unsigned i(0);i<bted.size();i++) {
	if(bted[i]->data()->triggerCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Acq CrcBeTrgEventData trigger counter != 0 " << std::endl;
	  bted[i]->print(std::cout," ERROR ");
	}
      }

      std::vector<const CrcLocationData<CrcBeEventData>*>
	bed(extracter.access< CrcLocationData<CrcBeEventData> >());

      for(unsigned i(0);i<bed.size();i++) {
	if(bed[i]->data()->l1aCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Acq CrcBeEventData slot " << (unsigned)bed[i]->slotNumber()
		    << " L1A counter = " << bed[i]->data()->l1aCounter() << " != 0" << std::endl;
	  bed[i]->print(std::cout," ERROR ");
	}
      }

      std::vector<const CrcLocationData<CrcFeEventData>*>
       fed(extracter.extract<CrcLocationData<CrcFeEventData> >());

      for(unsigned i(0);i<fed.size();i++) {
	if(fed[i]->data()->triggerCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Aqc CrcFeEventData FE" << fed[i]->crcComponent() 
		    << " trigger counter != 0" << std::endl;
	  fed[i]->print(std::cout," ERROR ");
	}
      }

      break;
    }

    case RcdHeader::acquisitionEnd: {
      /*
      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
       bted(extracter.extract<CrcLocationData<CrcBeTrgEventData> >());
      assert(bted.size()<=1);

      for(unsigned i(0);i<bted.size();i++) {
	if(bted[i]->data()->triggerCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Acq CrcBeTrgEventData trigger counter != 0 " << std::endl;
	  bted[i]->print(std::cout," ERROR ");
	}
      }

      std::vector<const CrcLocationData<CrcBeEventData>*>
	bed(extracter.access< CrcLocationData<CrcBeEventData> >());

      for(unsigned i(0);i<bed.size();i++) {
	if(bed[i]->data()->l1aCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Acq CrcBeEventData slot " << (unsigned)bed[i]->slotNumber()
		    << " L1A counter = " << bed[i]->data()->l1aCounter() << " != 0" << std::endl;
	  bed[i]->print(std::cout," ERROR ");
	}
      }

      std::vector<const CrcLocationData<CrcFeEventData>*>
       fed(extracter.extract<CrcLocationData<CrcFeEventData> >());

      for(unsigned i(0);i<fed.size();i++) {
	if(fed[i]->data()->triggerCounter()==0) {
	} else {
	  std::cout << "ChkCheck::record()  ERROR Aqc CrcFeEventData FE" << fed[i]->crcComponent() 
		    << " trigger counter != 0" << std::endl;
	  fed[i]->print(std::cout," ERROR ");
	}
      }
      */
      break;
    }

    case RcdHeader::trigger: {

      std::vector<const DaqEvent*> de(extracter.extract<DaqEvent>());
      assert(de.size()==1);

      //assert(de[0]->eventNumberInSpill()==_counter);
      if(de[0]->eventNumberInAcquisition()==_counter) {
	_counts[0][0]++;
      } else {
	_counts[1][0]++;
	if(_counts[1][0]<5) {
	  std::cout << "ChkCheck::record()  ERROR DaqEvent event number in acquisition != counter = " << _counter << std::endl;
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
	_counter=de[0]->eventNumberInAcquisition();
      }
      _counter++;

      //de[0]->print(std::cout) << std::endl;


      _beTrgCounter++;
      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
       bted(extracter.extract<CrcLocationData<CrcBeTrgEventData> >());
      assert(bted.size()<=1);

      for(unsigned i(0);i<bted.size();i++) {
	//bted[i]->print(std::cout," ");

	//assert(bted[i]->data()->triggerCounter()==_beTrgCounter);
	if(bted[i]->data()->triggerCounter()==_beTrgCounter) {
	  _counts[0][1]++;
	} else {
	  _counts[1][1]++;
	  if(_counts[1][1]<5) {
	    std::cout << "ChkCheck::record()  ERROR CrcBeTrgEventData trigger counter != counter = " << _beTrgCounter << std::endl;
	    bted[i]->print(std::cout," ERROR ");

	    std::vector<const CrcLocationData<CrcBeTrgPollData>*>
	      btpd(extracter.extract<CrcLocationData<CrcBeTrgPollData> >());
	    //      assert(btpd.size()==1);
	    if(btpd.size()>0) btpd[0]->print(std::cout," ERROR ");

	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	    de[0]->print(std::cout," ERROR ") << std::endl;
	  }
	  _beTrgCounter=bted[i]->data()->triggerCounter();
	}
      }
      
      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  _beCounter[c][s]++;
	}
      }

      std::vector<const CrcLocationData<CrcBeEventData>*>
	//bed(extracter.locationAccess<CrcBeEventData>(0xec));
	bed(extracter.access< CrcLocationData<CrcBeEventData> >());

      for(unsigned i(0);i<bed.size();i++) {
	/*
	if(de[0]->eventNumberInRun()>=1000 && bed[i]->slotNumber()==_slot) {
	  bed[i]->print(std::cout," ");
	}
	*/

	unsigned c(2);
	if(bed[i]->crateNumber()==0xec) c=0;
	if(bed[i]->crateNumber()==0xce) c=0;
	if(bed[i]->crateNumber()==0xac) c=1;

	if(c<2 && bed[i]->slotNumber()<22) {
	  if(bed[i]->data()->l1aCounter()==_beCounter[c][bed[i]->slotNumber()]) {
	    _counts[0][2]++;
	    //bed[i]->print(std::cout," GOOD ");

	  } else {
	    _counts[1][2]++;
	    if(_counts[1][2]<100) {
	      std::cout << "ChkCheck::record()  ERROR CrcBeEventData slot " << (unsigned)bed[i]->slotNumber()
			<< " L1A counter = " << bed[i]->data()->l1aCounter() << " != counter = "
			<< _beCounter[c][bed[i]->slotNumber()] << std::endl;
	      bed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	      de[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _beCounter[c][bed[i]->slotNumber()]=bed[i]->data()->l1aCounter();
	  }
	} else {
	  std::cout << "ChkCheck::record()  ERROR CrcBeEventData corrupted" << std::endl;
	  bed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
      }

      /*
      std::vector<const CrcLocationData<CrcBeTrgPollData>*>
       (extracter.extract<CrcLocationData<CrcBeTrgPollData> >());
    for(unsigned i(0);i<v.size();i++) {
      v[i]->print(std::cout) << std::endl;
    }}
      */

      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<22;s++) {
	  for(unsigned i(0);i<8;i++) {
	    _feCounter[c][s][i]++;
	  }
	}
      }

      std::vector<const CrcLocationData<CrcFeEventData>*>
       fed(extracter.extract<CrcLocationData<CrcFeEventData> >());

      for(unsigned i(0);i<fed.size();i++) {
	
	unsigned c(2);
	if(fed[i]->crateNumber()==0xec) c=0;
	if(fed[i]->crateNumber()==0xce) c=0;
	if(fed[i]->crateNumber()==0xac) c=1;

	
	unsigned fe((unsigned)fed[i]->crcComponent());

	if(c<2 && fed[i]->slotNumber()<22 && fe<8) {

	  if(fed[i]->data()->triggerCounter()==_feCounter[c][fed[i]->slotNumber()][fe]) {
	    _counts[0][3+fe]++;
	  } else {
	    _counts[1][3+fe]++;
	    if(_counts[1][3+fe]<100) {
	      std::cout << "ChkCheck::record()  ERROR CrcFeEventData FE" << fe 
			<< " trigger counter != counter = " << _feCounter[c][fed[i]->slotNumber()][fe] << std::endl;
	      fed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	      de[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _feCounter[c][fed[i]->slotNumber()][fe]=fed[i]->data()->triggerCounter();
	  }
	} else {
	  std::cout << "ChkCheck::record()  ERROR CrcFeEventData corrupted" << std::endl;
	  fed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
      }
      
      break;
    }

    case RcdHeader::triggerBurst: {

      std::vector<const DaqBurstStart*> dbs(extracter.extract<DaqBurstStart>());
      assert(dbs.size()==1);
      std::vector<const DaqBurstEnd*> dbe(extracter.extract<DaqBurstEnd>());
      assert(dbe.size()==1);

      if(dbs[0]->firstTriggerNumberInAcquisition()==_counter) {
	_counts[0][0]++;
      } else {
	_counts[1][0]++;
	if(_counts[1][0]<5) {
	  std::cout << "ChkCheck::record()  ERROR DaqBurstEnd first trigger number in acquisition != counter = " << _counter << std::endl;
	  dbs[0]->print(std::cout," ERROR ") << std::endl;
	}
	_counter=dbe[0]->lastTriggerNumberInAcquisition();
      }
      _counter+=dbe[0]->actualNumberOfTriggersInBurst();

      //dbs[0]->print(std::cout) << std::endl;
      //dbe[0]->print(std::cout) << std::endl;


      _beTrgCounter+=dbe[0]->actualNumberOfTriggersInBurst();

      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
       bted(extracter.extract<CrcLocationData<CrcBeTrgEventData> >());
      assert(bted.size()==1);

      for(unsigned i(0);i<bted.size();i++) {
	//bted[i]->print(std::cout," ");

	//assert(bted[i]->data()->triggerCounter()==_beTrgCounter);
	if(bted[i]->data()->triggerCounter()==_beTrgCounter) {
	  _counts[0][1]++;
	} else {
	  _counts[1][1]++;
	  if(_counts[1][1]<5) {
	    std::cout << "ChkCheck::record()  ERROR CrcBeTrgEventData trigger counter != counter = " << _beTrgCounter << std::endl;
	    bted[i]->print(std::cout," ERROR ");

	    std::vector<const CrcLocationData<CrcBeTrgPollData>*>
	      btpd(extracter.extract<CrcLocationData<CrcBeTrgPollData> >());
	    //      assert(btpd.size()==1);
	    if(btpd.size()>0) btpd[0]->print(std::cout," ERROR ");
	    
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	    dbe[0]->print(std::cout," ERROR ") << std::endl;
	  }
	  _beTrgCounter=bted[i]->data()->triggerCounter();
	}
      }

      
      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  _beCounter[c][s]+=dbe[0]->actualNumberOfTriggersInBurst();
	}
      }

      std::vector<const CrcLocationData<CrcBeEventData>*>
	//bed(extracter.locationAccess<CrcBeEventData>(0xec));
	bed(extracter.access< CrcLocationData<CrcBeEventData> >());

      for(unsigned i(0);i<bed.size();i++) {
	/*
	if(de[0]->eventNumberInRun()>=1000 && bed[i]->slotNumber()==_slot) {
	  bed[i]->print(std::cout," ");
	}
	*/

	unsigned c(2);
	if(bed[i]->crateNumber()==0xec) c=0;
	if(bed[i]->crateNumber()==0xce) c=0;
	if(bed[i]->crateNumber()==0xac) c=1;

	if(c<2 && bed[i]->slotNumber()<22) {
	  if(bed[i]->data()->l1aCounter()==_beCounter[c][bed[i]->slotNumber()]) {
	    _counts[0][2]++;
	    //bed[i]->print(std::cout," GOOD ");

	  } else {
	    _counts[1][2]++;
	    if(_counts[1][2]<100) {
	      std::cout << "ChkCheck::record()  ERROR CrcBeEventData slot " << (unsigned)bed[i]->slotNumber()
			<< " L1A counter = " << bed[i]->data()->l1aCounter() << " != counter = "
			<< _beCounter[c][bed[i]->slotNumber()] << std::endl;
	      bed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	      dbe[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _beCounter[c][bed[i]->slotNumber()]=bed[i]->data()->l1aCounter();
	  }
	} else {
	  std::cout << "ChkCheck::record()  ERROR CrcBeEventData corrupted" << std::endl;
	  bed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	  dbe[0]->print(std::cout," ERROR ") << std::endl;
	}
      }


      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<22;s++) {
	  for(unsigned i(0);i<8;i++) {
	    _feCounter[c][s][i]+=dbe[0]->actualNumberOfTriggersInBurst();
	  }
	}
      }

      std::vector<const CrcLocationData<CrcFeEventData>*>
       fed(extracter.extract<CrcLocationData<CrcFeEventData> >());

      for(unsigned i(0);i<fed.size();i++) {
	
	unsigned c(2);
	if(fed[i]->crateNumber()==0xec) c=0;
	if(fed[i]->crateNumber()==0xce) c=0;
	if(fed[i]->crateNumber()==0xac) c=1;

	
	unsigned fe((unsigned)fed[i]->crcComponent());

	if(c<2 && fed[i]->slotNumber()<22 && fe<8) {

	  if(fed[i]->data()->triggerCounter()==_feCounter[c][fed[i]->slotNumber()][fe]) {
	    _counts[0][3+fe]++;
	  } else {
	    _counts[1][3+fe]++;
	    if(_counts[1][3+fe]<100) {
	      std::cout << "ChkCheck::record()  ERROR CrcFeEventData FE" << fe 
			<< " trigger counter != counter = " << _feCounter[c][fed[i]->slotNumber()][fe] << std::endl;
	      fed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	      dbe[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _feCounter[c][fed[i]->slotNumber()][fe]=fed[i]->data()->triggerCounter();
	  }
	} else {
	  std::cout << "ChkCheck::record()  ERROR CrcFeEventData corrupted" << std::endl;
	  fed[i]->print(std::cout," ERROR ");
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	  dbe[0]->print(std::cout," ERROR ") << std::endl;
	}
      }
      
      break;
    }

    case RcdHeader::event: {

      std::vector<const DaqEvent*> de(extracter.extract<DaqEvent>());
      assert(de.size()==1);

      //std::cout << std::endl;
      //de[0]->print(std::cout) << std::endl;

      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  for(unsigned i(0);i<8;i++) {
	    _vlinkCounter[c][s][i]++;
	  }
	}
      }

      std::vector<const CrcLocationData<CrcVlinkEventData>*>
	ved(extracter.extract<CrcLocationData<CrcVlinkEventData> >());

      for(unsigned i(0);i<ved.size();i++) {

	unsigned c(2);
	if(ved[i]->crateNumber()==0xec) c=0;
	if(ved[i]->crateNumber()==0xce) c=0;
	if(ved[i]->crateNumber()==0xac) c=1;
	assert(c<2);

	unsigned s(ved[i]->slotNumber());
	assert(s<22);

	// Check it should have been read out
	assert(_readoutConfigurationData[c].slotEnable(s));

	if(!ved[i]->data()->verify()) {
	  _counts[1][27]++;
	  if(_counts[1][27]<10) {
	    std::cout << "ChkCheck::record()  ERROR CrcVlinkEventData not verified" << std::endl;
	    ved[i]->data()->verify(true);
	    ved[i]->print(std::cout," ERROR ");
	    ved[i]->data()->print(std::cout," ERROR ",true);
	    std::cout << " ERROR Configuration number = " << _connum << std::endl;
	    de[0]->print(std::cout," ERROR ") << std::endl;
	  }
	} else {
	  _counts[0][27]++;
	}

	for(unsigned f(0);f<8;f++) {
	  if(_readoutConfigurationData[c].slotFeEnable(s,f)) {
	    //if(ved[i]->slotNumber()!=12 || f!=1) {
	    const CrcVlinkFeData *fd(ved[i]->data()->feData(f));
	    //assert(fd!=0);
	    if(fd!=0) {
	      _counts[0][11+f]++;
	      
	      //assert(fd->triggerCounter()==_vlinkCounter[f]);
	      if(fd->triggerCounter()==_vlinkCounter[c][ved[i]->slotNumber()][f]) {
		_counts[0][19+f]++;
	      } else {
		_counts[1][19+f]++;
		if(_counts[1][19+f]<20) {
		  std::cout << "ChkCheck::record()  ERROR CrcVlinkEventData FE" << f << " counter != counter = "
			    << _vlinkCounter[c][ved[i]->slotNumber()][f] << std::endl;
		  fd->print(std::cout," ERROR ");
		  ved[i]->CrcLocation::print(std::cout," ERROR ");
		  std::cout << " ERROR Configuration number = " << _connum << std::endl;
		  de[0]->print(std::cout," ERROR ") << std::endl;
		}
		_vlinkCounter[c][ved[i]->slotNumber()][f]=fd->triggerCounter();
	      }
	    } else {
	      _counts[1][11+f]++;
	      if(_counts[1][11+f]<20) {
		std::cout << "ChkCheck::record()  ERROR CrcVlinkEventData FE" << f << " has no data" << std::endl;
		ved[i]->CrcLocation::print(std::cout," ERROR ");
		if(f==0) ved[i]->data()->print(std::cout," ERROR ",true);
		std::cout << " ERROR Configuration number = " << _connum << std::endl;
		de[0]->print(std::cout," ERROR ") << std::endl;
	      }
	    }
	  }
	}
      }
      
      break;
    }
      
    default: {  
      break;
    }
    };

    return true;
  }

  std::ostream& print(std::ostream& o, std::string s="") {
    o << s << "ChkCount::print()" << std::endl;
    for(unsigned i(0);i<28;i++) {
      if(i== 0) std::cout << "DaqEvent" << std::endl;
      if(i== 1) std::cout << "BETrg" << std::endl;
      if(i== 2) std::cout << "BE" << std::endl;
      if(i>= 3 && i<=10) std::cout << "FE" << i-3 << std::endl;
      if(i>=11 && i<=18) std::cout << "Vlink FE" << i-11 << " missing" << std::endl;
      if(i>=19 && i<=26) std::cout << "Vlink FE" << i-19 << std::endl;
      if(i==27) std::cout << "Vlink verify" << std::endl;

      o << s << " Count " << std::setw(4) << i 
	<< ", Good = " << std::setw(8) << _counts[0][i]
	<< ", bad = " << std::setw(8) << _counts[1][i]
	<< ", total = " << std::setw(8) << _counts[0][i]+_counts[1][i] << std::endl;
    }
    return o;
  }

private:
  unsigned _connum;

  unsigned _crate;
  unsigned _slot;
  unsigned _counter;
  unsigned _beTrgCounter;
  unsigned _beCounter[2][22];
  unsigned _feCounter[2][22][8];
  unsigned _vlinkCounter[2][22][8];
  unsigned _counts[2][100];

  CrcReadoutConfigurationData _readoutConfigurationData[2];
  std::vector<ChkCheckCrc> _vChkCheckCrc;
};

#endif
