#ifndef ChkCaen767_HH
#define ChkCaen767_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRO.hh"
#include "SubAccessor.hh"


class ChkCaen767 : public RcdUserRO {

public:
  ChkCaen767(unsigned t=0) : RcdUserRO(255), _tdc(t), _location(0,0,0) {
    /*
    for(unsigned s(0);s<=21;s++) {
      for(unsigned i(0);i<100;i++) {
	_counts[0][i]=0;
	_counts[1][i]=0;
      }
    }
    */
  }

  virtual ~ChkCaen767() {
  }

  void location(BmlLocation l) {
    _location=l;
    _location.label(0);
    std::cout << " Setting location" << std::endl;
    _location.print(std::cout,"  ") << std::endl;
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "ChkCaen767::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    SubAccessor accessor(r);

    //accessor.print(std::cout);

    /*
    if(_select[subRecordType< DaqRunStart >()]) {
      std::vector<const DaqRunStart*>
	v(accessor.access<DaqRunStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqRunEnd >()]) {
      std::vector<const DaqRunEnd*>
	v(accessor.access<DaqRunEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationStart >()]) {
      std::vector<const DaqConfigurationStart*>
	v(accessor.access<DaqConfigurationStart>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }

    if(_select[subRecordType< DaqConfigurationEnd >()]) {
      std::vector<const DaqConfigurationEnd*>
	v(accessor.access<DaqConfigurationEnd>());
      for(unsigned i(0);i<v.size();i++) {
	v[i]->print(std::cout) << std::endl;
      }
    }
    */

    switch(r.recordType()) {

    case RcdHeader::runStart: {
      if(_tdc!=0xffffffff) {
	std::vector<const BmlLocationData<BmlCaen767RunData>*>
	  rd(accessor.access< BmlLocationData<BmlCaen767RunData> >());
	
	if(doPrint(r.recordType(),1)) {
	  std::cout << " Number of BmlCaen767RunData subrecords found = "
		    << rd.size() << std::endl << std::endl;
	  for(unsigned i(0);i<rd.size();i++) {
	    rd[i]->print(std::cout," ") << std::endl;
	  }
	}

	if(_tdc<rd.size()) {
	  _location=rd[_tdc]->location();
	  std::cout << " Setting location" << std::endl;
	  _location.print(std::cout,"  ") << std::endl;
	  _tdc=0xffffffff;
	}
      }
      break;
    }

    case RcdHeader::acquisitionStart: {
      _trgCounter=0;

      std::vector<const BmlLocationData<BmlCaen767TriggerData>*>
	td(accessor.access< BmlLocationData<BmlCaen767TriggerData> >());
      
      if(doPrint(r.recordType(),1)) {
	std::cout << " Number of BmlCaen767TriggerData subrecords found = "
		  << td.size() << std::endl << std::endl;
	for(unsigned i(0);i<td.size();i++) {
	  td[i]->print(std::cout," ") << std::endl;
	}
      }

      unsigned nFound(0);

      for(unsigned i(0);i<td.size();i++) {
	if(td[i]->location()==_location) {
	  nFound++;
	  bool error(false);

	  BmlCaen767StatusRegister s(td[i]->data()->statusRegister());
	  if(s.dataReady()) {
	    std::cout << " ERROR  This module has data ready" << std::endl;
	    error=true;
	  }

	  if(s.globalDataReady()) {
	    std::cout << " ERROR  Some modules have data ready" << std::endl;
	    error=true;
	  }

	  if(s.busy()) {
	    std::cout << " ERROR  This module is busy" << std::endl;
	    error=true;
	  }

	  if(s.globalBusy()) {
	    std::cout << " ERROR  Some modules are busy" << std::endl;
	    error=true;
	  }

	  if(!s.bufferEmpty()) {
	    std::cout << " ERROR  Buffer not empty" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferFull()) {
	    std::cout << " ERROR  Buffer full" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferAlmostFull()) {
	    std::cout << " ERROR  Buffer almost full" << std::endl;
	    error=true;
	  }
	  
	  if(s.tdcError()) {
	    std::cout << " ERROR  A TDC has an error" << std::endl;
	    error=true;
	  }
	  
	  for(unsigned j(0);j<4;j++) {
	    if(s.tdcError(j)) {
	      std::cout << " ERROR  TDC " << j << " has an error" << std::endl;
	      error=true;
	    }
	  }
	  
	  if(td[i]->data()->eventCounter()!=0) {
	    std::cout << " ERROR  Event counter = "
		      << td[i]->data()->eventCounter()
		      << " != 0" << std::endl;
	    _trgCounter=td[i]->data()->eventCounter();
	    error=true;
	  }

	  if(error) td[i]->print(std::cout," ERROR Acq ") << std::endl;
	}
      }

      assert(nFound<2);

      break;
    }

    case RcdHeader::acquisitionEnd: {
      std::vector<const DaqAcquisitionEnd*>
	ae(accessor.access<DaqAcquisitionEnd>());

      std::vector<const BmlLocationData<BmlCaen767TriggerData>*>
	td(accessor.access< BmlLocationData<BmlCaen767TriggerData> >());
      
      if(doPrint(r.recordType(),1)) {
	std::cout << " Number of BmlCaen767TriggerData subrecords found = "
		  << td.size() << std::endl << std::endl;
	for(unsigned i(0);i<td.size();i++) {
	  td[i]->print(std::cout," ") << std::endl;
	}
      }

      unsigned nFound(0);

      for(unsigned i(0);i<td.size();i++) {
	if(td[i]->location()==_location) {
	  nFound++;
	  bool error(false);

	  BmlCaen767StatusRegister s(td[i]->data()->statusRegister());
	  if(s.dataReady()) {
	    std::cout << " ERROR  This module has data ready" << std::endl;
	    error=true;
	  }

	  if(s.globalDataReady()) {
	    std::cout << " ERROR  Some modules have data ready" << std::endl;
	    error=true;
	  }

	  if(s.busy()) {
	    std::cout << " ERROR  This module is busy" << std::endl;
	    error=true;
	  }

	  if(s.globalBusy()) {
	    std::cout << " ERROR  Some modules are busy" << std::endl;
	    error=true;
	  }

	  if(!s.bufferEmpty()) {
	    std::cout << " ERROR  Buffer not empty" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferFull()) {
	    std::cout << " ERROR  Buffer full" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferAlmostFull()) {
	    std::cout << " ERROR  Buffer almost full" << std::endl;
	    error=true;
	  }
	  
	  if(s.tdcError()) {
	    std::cout << " ERROR  A TDC has an error" << std::endl;
	    error=true;
	  }
	  
	  for(unsigned j(0);j<4;j++) {
	    if(s.tdcError(j)) {
	      std::cout << " ERROR  TDC " << j << " has an error" << std::endl;
	      error=true;
	    }
	  }
	  
	  if(ae.size()!=1) {
	    std::cout << " ERROR  Number of DaqAcquisitionEnd subrecords found = "
		      << ae.size() << std::endl;
	    error=true;

	  } else {
	    // Event counter is only 10 bits
	    unsigned ec((ae[0]->actualNumberOfEventsInAcquisition())%1024);

	    if(td[i]->data()->eventCounter()!=ec) {
	      std::cout << " ERROR  Event counter = "
			<< td[i]->data()->eventCounter() << " != "
			<< ec << std::endl;
	      ae[0]->print(std::cout," ERROR ");
	      error=true;
	    }
	  }

	  if(error) td[i]->print(std::cout," ERROR Acq ") << std::endl;
	}
      }

      assert(nFound<2);

      std::vector<const BmlLocationData<BmlCaen767TdcErrorData>*>
	te(accessor.access< BmlLocationData<BmlCaen767TdcErrorData> >());
      
      //      if(doPrint(r.recordType(),1)) {
      if(te.size()>0) {
	std::cout << " Number of BmlCaen767TdcErrorData subrecords found = "
		  << te.size() << std::endl << std::endl;
	for(unsigned i(0);i<te.size();i++) {
	  te[i]->print(std::cout," ") << std::endl;
	}
      }

      break;
    }

    case RcdHeader::trigger: {
      std::vector<const DaqEvent*>
	de(accessor.access<DaqEvent>());

      std::vector<const BmlLocationData<BmlCaen767TriggerData>*>
	td(accessor.access< BmlLocationData<BmlCaen767TriggerData> >());
      
      if(doPrint(r.recordType(),1)) {
	std::cout << " Number of BmlCaen767TriggerData subrecords found = "
		  << td.size() << std::endl << std::endl;
	for(unsigned i(0);i<td.size();i++) {
	  td[i]->print(std::cout," ") << std::endl;
	}
      }

      unsigned nFound(0);

      for(unsigned i(0);i<td.size();i++) {
	if(td[i]->location()==_location) {
	  nFound++;
	  bool error(false);

	  BmlCaen767StatusRegister s(td[i]->data()->statusRegister());
	  if(!s.dataReady()) {
	    std::cout << " ERROR  This module has no data ready" << std::endl;
	    error=true;
	  }

	  if(!s.globalDataReady()) {
	    std::cout << " ERROR  All modules have no data ready" << std::endl;
	    error=true;
	  }

	  if(s.busy()) {
	    std::cout << " ERROR  This module is busy" << std::endl;
	    error=true;
	  }

	  if(s.globalBusy()) {
	    std::cout << " ERROR  Some modules are busy" << std::endl;
	    error=true;
	  }

	  if(s.bufferEmpty()) {
	    std::cout << " ERROR  Buffer empty" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferFull()) {
	    std::cout << " ERROR  Buffer full" << std::endl;
	    error=true;
	  }
	  /*	  
	  if(s.bufferAlmostFull()) {
	    std::cout << " ERROR  Buffer almost full" << std::endl;
	    error=true;
	  }
	  */
	  if(s.tdcError()) {
	    std::cout << " ERROR  A TDC has an error" << std::endl;
	    error=true;
	  }
	  
	  for(unsigned j(0);j<4;j++) {
	    if(s.tdcError(j)) {
	      std::cout << " ERROR  TDC " << j << " has an error" << std::endl;
	      error=true;
	    }
	  }
	  
	  if(de.size()!=1) {
	    std::cout << " ERROR  Number of DaqEvent subrecords found = "
		      << de.size() << std::endl;
	    error=true;

	  } else {
	    // Event counter is only 10 bits
	    unsigned ec((de[0]->eventNumberInAcquisition()+1+_trgCounter)%1024);

	    if(td[i]->data()->eventCounter()!=ec) {
	      std::cout << " ERROR  Event counter = "
			<< td[i]->data()->eventCounter() << " != "
			<< ec << std::endl;
	      de[0]->print(std::cout," ERROR ");
	      _trgCounter=td[i]->data()->eventCounter()-ec;
	      error=true;
	    }
	  }

	  if(error) td[i]->print(std::cout," ERROR Trg ") << std::endl;
	}
      }

      assert(nFound<2);

      break;
    }

    case RcdHeader::event: {
      std::vector<const DaqEvent*>
	de(accessor.access<DaqEvent>());

      std::vector<const BmlLocationData<BmlCaen767EventData>*>
	ed(accessor.access< BmlLocationData<BmlCaen767EventData> >());
      
      if(doPrint(r.recordType(),1)) {
	std::cout << " Number of BmlCaen767EventData subrecords found = "
		  << ed.size() << std::endl << std::endl;
	for(unsigned i(0);i<ed.size();i++) {
	  ed[i]->print(std::cout," ") << std::endl;
	}
      }

      unsigned nFound(0);

      for(unsigned i(0);i<ed.size();i++) {
	if(ed[i]->location()==_location) {
	  nFound++;
	  bool error(false);

	  BmlCaen767StatusRegister s(ed[i]->data()->statusRegister());
	  if(!s.dataReady()) {
	    std::cout << " ERROR  This module has no data ready" << std::endl;
	    error=true;
	  }

	  if(!s.globalDataReady()) {
	    std::cout << " ERROR  All modules have no data ready" << std::endl;
	    error=true;
	  }

	  if(s.busy()) {
	    std::cout << " ERROR  This module is busy" << std::endl;
	    error=true;
	  }

	  if(s.globalBusy()) {
	    std::cout << " ERROR  Some modules are busy" << std::endl;
	    error=true;
	  }

	  if(s.bufferEmpty()) {
	    std::cout << " ERROR  Buffer empty" << std::endl;
	    error=true;
	  }
	  
	  if(s.bufferFull()) {
	    std::cout << " ERROR  Buffer full" << std::endl;
	    error=true;
	  }
	  /*	  
	  if(s.bufferAlmostFull()) {
	    std::cout << " ERROR  Buffer almost full" << std::endl;
	    error=true;
	  }
	  */
	  if(s.tdcError()) {
	    std::cout << " ERROR  A TDC has an error" << std::endl;
	    error=true;
	  }
	  
	  for(unsigned j(0);j<4;j++) {
	    if(s.tdcError(j)) {
	      std::cout << " ERROR  TDC " << j << " has an error" << std::endl;
	      error=true;
	    }
	  }
	  

	  //if(ed[i]->data()->numberOfWords()<2) {
	  //if(ed[i]->data()->numberOfWords()!=2) {
	  if(ed[i]->data()->numberOfWords()<4) {
	    std::cout << " ERROR  Number of words = "
		      << ed[i]->data()->numberOfWords()
		      << " < 4" << std::endl;
	    error=true;
	  }

	  const BmlCaen767EventDatum *datum(ed[i]->data()->data());

	  //	  if(datum[0].eventNumber()==548) error=true;





	  unsigned ec(0);
	  if(de.size()!=1) {
	    std::cout << " ERROR  Number of DaqEvent subrecords found = "
		      << de.size() << std::endl;
	    error=true;

	  } else {

	    // Event counter is only 10 bits
	    ec=(de[0]->eventNumberInAcquisition()+1)%1024;
	  }

	  /*
	  if(ed[i]->data()->eventCounter()!=ec) {
	    std::cout << " ERROR  Event counter = "
		      << ed[i]->data()->eventCounter() << " != "
		      << ec << std::endl;
	    de[0]->print(std::cout," ERROR ");
	    error=true;
	  }
	  */
	  
	  if(error) ed[i]->print(std::cout," ERROR Evt ") << std::endl;
	}
      }

      assert(nFound<2);

      break;
    }

      /*
    case RcdHeader::trigger: {

      std::vector<const DaqEvent*> de(accessor.access<DaqEvent>());
      assert(de.size()==1);

      //assert(de[0]->eventNumberInSpill()==_counter);
      if(de[0]->eventNumberInAcquisition()==_counter) {
	_counts[0][0]++;
      } else {
	_counts[1][0]++;
	if(_counts[1][0]<5) {
	  std::cout << "ChkCaen767::record()  ERROR DaqEvent event number in acquisition != counter = " << _counter << std::endl;
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
	_counter=de[0]->eventNumberInAcquisition();
      }
      _counter++;

      //de[0]->print(std::cout) << std::endl;


      _beTrgCounter++;
      std::vector<const CrcLocationData<CrcBeTrgEventData>*>
       bted(accessor.access<CrcLocationData<CrcBeTrgEventData> >());
      assert(bted.size()<=1);

      for(unsigned i(0);i<bted.size();i++) {
	//bted[i]->print(std::cout," ");

	//assert(bted[i]->data()->triggerCounter()==_beTrgCounter);
	if(bted[i]->data()->triggerCounter()==_beTrgCounter) {
	  _counts[0][1]++;
	} else {
	  _counts[1][1]++;
	  if(_counts[1][1]<5) {
	    std::cout << "ChkCaen767::record()  ERROR CrcBeTrgEventData trigger counter != counter = " << _beTrgCounter << std::endl;
	    bted[i]->print(std::cout," ERROR ");

      std::vector<const CrcLocationData<CrcBeTrgPollData>*>
	btpd(accessor.access<CrcLocationData<CrcBeTrgPollData> >());
      //      assert(btpd.size()==1);
      if(btpd.size()>0) btpd[0]->print(std::cout," ERROR ");



	    de[0]->print(std::cout," ERROR ") << std::endl;
	  }
	  _beTrgCounter=bted[i]->data()->triggerCounter();
	}
      }
      
      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  _beCounter[c][s]++;
	}
      }

      std::vector<const CrcLocationData<CrcBeEventData>*>
	//bed(accessor.locationAccess<CrcBeEventData>(0xec));
	bed(accessor.access< CrcLocationData<CrcBeEventData> >());

      for(unsigned i(0);i<bed.size();i++) {
	unsigned c(2);
	if(bed[i]->crateNumber()==0xec) c=0;
	if(bed[i]->crateNumber()==0xac) c=1;

	if(c<2 && bed[i]->slotNumber()<22) {
	  if(bed[i]->data()->l1aCounter()==_beCounter[c][bed[i]->slotNumber()]) {
	    _counts[0][2]++;
	    //bed[i]->print(std::cout," GOOD ");

	  } else {
	    _counts[1][2]++;
	    if(_counts[1][2]<100) {
	      std::cout << "ChkCaen767::record()  ERROR CrcBeEventData slot " << (unsigned)bed[i]->slotNumber()
			<< " L1A counter = " << bed[i]->data()->l1aCounter() << " != counter = "
			<< _beCounter[c][bed[i]->slotNumber()] << std::endl;
	      bed[i]->print(std::cout," ERROR ");
	      de[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _beCounter[c][bed[i]->slotNumber()]=bed[i]->data()->l1aCounter();
	  }
	} else {
	  std::cout << "ChkCaen767::record()  ERROR CrcBeEventData corrupted" << std::endl;
	  bed[i]->print(std::cout," ERROR ");
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
      }
*/

      /*
      std::vector<const CrcLocationData<CrcBeTrgPollData>*>
       (accessor.access<CrcLocationData<CrcBeTrgPollData> >());
    for(unsigned i(0);i<v.size();i++) {
      v[i]->print(std::cout) << std::endl;
    }}
      */
      /*
      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<22;s++) {
	  for(unsigned i(0);i<8;i++) {
	    _feCounter[c][s][i]++;
	  }
	}
      }

      std::vector<const CrcLocationData<CrcFeEventData>*>
       fed(accessor.access<CrcLocationData<CrcFeEventData> >());

      for(unsigned i(0);i<fed.size();i++) {
	
	unsigned c(2);
	if(fed[i]->crateNumber()==0xec) c=0;
	if(fed[i]->crateNumber()==0xac) c=1;

	
	unsigned fe((unsigned)fed[i]->crcComponent());

	if(c<2 && fed[i]->slotNumber()<22 && fe<8) {

	  if(fed[i]->data()->triggerCounter()==_feCounter[c][fed[i]->slotNumber()][fe]) {
	    _counts[0][3+fe]++;
	  } else {
	    _counts[1][3+fe]++;
	    if(_counts[1][3+fe]<100) {
	      std::cout << "ChkCaen767::record()  ERROR CrcFeEventData FE" << fe 
			<< " trigger counter != counter = " << _feCounter[c][fed[i]->slotNumber()][fe] << std::endl;
	      fed[i]->print(std::cout," ERROR ");
	      de[0]->print(std::cout," ERROR ") << std::endl;
	    }
	    _feCounter[c][fed[i]->slotNumber()][fe]=fed[i]->data()->triggerCounter();
	  }
	} else {
	  std::cout << "ChkCaen767::record()  ERROR CrcFeEventData corrupted" << std::endl;
	  fed[i]->print(std::cout," ERROR ");
	  de[0]->print(std::cout," ERROR ") << std::endl;
	}
      }
      
      break;
    }

    case RcdHeader::event: {

      std::vector<const DaqEvent*> de(accessor.access<DaqEvent>());
      assert(de.size()==1);

      //std::cout << std::endl;
      //de[0]->print(std::cout) << std::endl;

      for(unsigned c(0);c<2;c++) {
	for(unsigned s(0);s<=21;s++) {
	  for(unsigned i(0);i<8;i++) {
	    _vlinkCounter[c][s][i]++;
	  }
	}
      }

      std::vector<const CrcLocationData<CrcVlinkEventData>*>
	ved(accessor.access<CrcLocationData<CrcVlinkEventData> >());

      for(unsigned i(0);i<ved.size();i++) {

	unsigned c(2);
	if(ved[i]->crateNumber()==0xec) c=0;
	if(ved[i]->crateNumber()==0xac) c=1;

	if(ved[i]->slotNumber()==_slot) {
	  if(!ved[i]->data()->verify()) {
	    _counts[1][27]++;
	    if(_counts[1][27]<10) {
	      std::cout << "ChkCaen767::record()  ERROR CrcVlinkEventData not verified" << std::endl;
	      ved[i]->data()->verify(true);
	      ved[i]->print(std::cout," ERROR ");
	      de[0]->print(std::cout," ERROR ") << std::endl;
	    }
	  } else {
	    _counts[0][27]++;
	  }

	if(ved[i]->label()==0) {
	for(unsigned f(0);f<8;f++) {
	  //if(ved[i]->slotNumber()!=12 || f!=1) {
	    const CrcVlinkFeData *fd(ved[i]->data()->feData(f));
	    //assert(fd!=0);
	    if(fd!=0) {
	      _counts[0][11+f]++;
	      
	      //assert(fd->triggerCounter()==_vlinkCounter[f]);
	      if(fd->triggerCounter()==_vlinkCounter[c][ved[i]->slotNumber()][f]) {
		_counts[0][19+f]++;
	      } else {
		_counts[1][19+f]++;
		if(_counts[1][19+f]<20) {
		  std::cout << "ChkCaen767::record()  ERROR CrcVlinkEventData FE" << f << " counter != counter = "
			    << _vlinkCounter[c][ved[i]->slotNumber()][f] << std::endl;
		  ved[i]->CrcLocation::print(std::cout," ERROR ");
		  fd->print(std::cout," ERROR ");
		  de[0]->print(std::cout," ERROR ") << std::endl;
		}
		_vlinkCounter[c][ved[i]->slotNumber()][f]=fd->triggerCounter();
	      }
	    } else {
	      _counts[1][11+f]++;
	      if(_counts[1][11+f]<20) {
		std::cout << "ChkCaen767::record()  ERROR CrcVlinkEventData FE" << f << " has no data" << std::endl;
		ved[i]->CrcLocation::print(std::cout," ERROR ");
		de[0]->print(std::cout," ERROR ") << std::endl;
	      }
	    }

	    //}
	  }
	}
	}
      }

      break;
    }
      */
    default: {  
      break;
    }
    };

    return true;
  }

  std::ostream& print(std::ostream& o, std::string s="") {
    o << s << "ChkCaen767::print()" << std::endl;
    /*
    for(unsigned i(0);i<28;i++) {
      if(i== 0) std::cout << "DaqEvent" << std::endl;
      if(i== 1) std::cout << "BETrg" << std::endl;
      if(i== 2) std::cout << "BE" << std::endl;
      if(i>= 3 && i<=10) std::cout << "FE" << i-3 << std::endl;
      if(i>=11 && i<=18) std::cout << "Vlink FE" << i-11 << " missing" << std::endl;
      if(i>=19 && i<=26) std::cout << "Vlink FE" << i-19 << std::endl;
      if(i==27) std::cout << "Vlink verify" << std::endl;

      o << s << " Count " << std::setw(4) << i 
	<< ", Good = " << std::setw(8) << _counts[0][i]
	<< ", bad = " << std::setw(8) << _counts[1][i]
	<< ", total = " << std::setw(8) << _counts[0][i]+_counts[1][i] << std::endl;
    }
    */
    return o;
  }

private:
  unsigned _tdc;
  BmlLocation _location;
  unsigned _trgCounter;

  unsigned _crate;
  unsigned _slot;
  unsigned _counter;
  unsigned _beTrgCounter;
  unsigned _beCounter[2][22];
  unsigned _feCounter[2][22][8];
  unsigned _vlinkCounter[2][22][8];
  unsigned _counts[2][100];
};

#endif
