#ifndef GenConfiguration_HH
#define GenConfiguration_HH

#include <vector>
#include <fstream>
#include <iostream>

#include "RcdUserRW.hh"

#include "SubInserter.hh"
#include "SubAccessor.hh"


class GenConfiguration : public RcdUserRW {

public:
  GenConfiguration() {
  }

  virtual ~GenConfiguration() {
  }

  bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "GenConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    SubInserter inserter(r);

    // Check record type
    switch (r.recordType()) {
      
    case RcdHeader::runStart: {

      SubAccessor accessor(r);
      std::vector<const IlcRunStart*>
	v(accessor.access<IlcRunStart>());
      assert(v.size()==1);
      _runStart=*(v[0]);

      if(_runStart.runType().majorType()==IlcRunType::gen) {
	SubInserter inserter(r);

	SimDetectorData *d(inserter.insert<SimDetectorData>(true));
	d->type(SimDetectorData::ecalVolume);
	d->layer(0);
	d->material(SimDetectorBox::air);

	double boxX0[3]={3.5,0.0,0.0};

	// Fill detector here
	SimDetectorBox box;

	double z(0.0);
	double x0(0.0);

	for(unsigned l(0);l<30;l++) {
	  double dz(2.1);
	  if(l>=20) dz*=2.0;
	  box.xCentre(0.0);
	  box.xWidth(1000.0);
	  box.yCentre(0.0);
	  box.yWidth(1000.0);
	  box.zThickness(dz);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(dz/boxX0[0]);
	  box.type(SimDetectorBoxId::ecalConverter);
	  box.layer(l);
	  box.material(SimDetectorBox::tungsten);
	  d->addBox(box);

	  z+=box.zThickness()+0.1;
	  x0+=dz/boxX0[0];

	  box.xCentre(0.0);
	  box.xWidth(1000.0);
	  box.yCentre(0.0);
	  box.yWidth(1000.0);
	  box.zThickness(1.0);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalPcb);
	  box.layer(l);
	  box.material(SimDetectorBox::pcb);
	  d->addBox(box);

	  z+=box.zThickness()+0.1;

	  box.xCentre(0.0);
	  box.xWidth(1000.0);
	  box.yCentre(0.0);
	  box.yWidth(1000.0);
	  box.zThickness(0.500-0.012);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiBulk);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);

	  z+=box.zThickness();

	  box.xCentre(0.0);
	  box.xWidth(1000.0);
	  box.yCentre(0.0);
	  box.yWidth(1000.0);
	  box.zThickness(0.012);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiEpitaxial);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);

	  if(l<29) z+=box.zThickness()+0.5;
	  else     z+=box.zThickness();
	}

	d->resize();
	d->radiationLength(x0);

	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;


	SimPrimaryData *p(inserter.insert<SimPrimaryData>(true));
	
	if(_runStart.runType().type()==IlcRunType::genPhoton) {
	  p->particleId( 22);
	  p->mass(0.0);
	}
	if(_runStart.runType().type()==IlcRunType::genPositron) {
	  p->particleId(-11);
	  p->mass(0.51099906);
	}
	if(_runStart.runType().type()==IlcRunType::genElectron) {
	  p->particleId( 11);
	  p->mass(0.51099906);
	}
	if(_runStart.runType().type()==IlcRunType::genMuPlus) {
	  p->particleId(-13);
	  p->mass(105.6584);
	}
	if(_runStart.runType().type()==IlcRunType::genMuMinus) {
	  p->particleId( 13);
	  p->mass(105.6584);
	}
	if(_runStart.runType().type()==IlcRunType::genPiPlus) {
	  p->particleId( 211);
	  p->mass(139.5700);
	}
	if(_runStart.runType().type()==IlcRunType::genPiMinus) {
	  p->particleId(-211);
	  p->mass(139.5700);
	}
	if(_runStart.runType().type()==IlcRunType::genPiZero) {
	  p->particleId( 111);
	  p->mass(134.9764);
	}
	if(_runStart.runType().type()==IlcRunType::genProton) {
	  p->particleId( 2212);
	  p->mass(938.27231);
	}
	if(_runStart.runType().type()==IlcRunType::genAntiProton) {
	  p->particleId(-2212);
	  p->mass(938.27231);
	}
	if(_runStart.runType().type()==IlcRunType::genNeutron) {
	  p->particleId( 2112);
	  p->mass(939.56563);
	}
	if(_runStart.runType().type()==IlcRunType::genAntiNeutron) {
	  p->particleId(-2112);
	  p->mass(939.56563);
	}

	/*
	if(_runType.type()==IlcRunType::simMuPlus    ) particleName="mu+";
	if(_runType.type()==IlcRunType::simMuMinus   ) particleName="mu-";
	if(_runType.type()==IlcRunType::simPiPlus    ) particleName="pi+";
	if(_runType.type()==IlcRunType::simPiMinus   ) particleName="pi-";
	if(_runType.type()==IlcRunType::simProton    ) particleName="proton";
	if(_runType.type()==IlcRunType::simAntiProton) particleName="anti_proton";
	*/
	p->x(0.0);
	p->y(0.0);
	p->z(-1.0);

	p->smearMatrix(0,0,25.0);
	p->smearMatrix(1,1,25.0);

	p->px(0.0);
	p->py(0.0);
	p->pz(1000.0*_runStart.runType().version());
	if(_runStart.runType().version()==252) p->pz( 300000.0);
	if(_runStart.runType().version()==253) p->pz( 400000.0);
	if(_runStart.runType().version()==254) p->pz( 600000.0);
	if(_runStart.runType().version()==255) p->pz(1000000.0);
    
	if(doPrint(r.recordType(),1)) p->print(std::cout," ") << std::endl;
      }

      break;
    }
      
    case RcdHeader::configurationStart: {
     
      if(_runStart.runType().majorType()==IlcRunType::gen) {
      }

      break;
    }
      
    default: {
      break;
    }
    };

    return true;
  }

private:
  IlcRunStart _runStart;
};

#endif
