#ifndef DaqSlwUser_HH
#define DaqSlwUser_HH

#include <vector>
#include <fstream>
#include <iostream>

// dual/inc/rcd
#include "RcdUserRO.hh"

// dual/inc/daq
#include "DaqSlwControl.hh"
#include "ShmObject.hh"




class DaqSlwUser : public RcdUserRO {

public:
  DaqSlwUser() : RcdUserRO(), _shmSlwControl(DaqSlwControl::shmKey), _pDsc(0) {
    _pDsc=_shmSlwControl.payload();
    assert(_pDsc!=0);
  
    _pDsc->status(DaqSlwControl::dead);
  }

  virtual ~DaqSlwUser() {
    _pDsc->unregister();
    _pDsc->status(DaqSlwControl::dead);
  }

  bool record(const RcdRecord &r) {

    // Check record type
    switch (r.recordType()) {

    case RcdHeader::startUp: {
      _pDsc->resetCount(false);
      _pDsc->status(DaqSlwControl::waiting,false);
      break;
    }

    case RcdHeader::slowStart: {
      _pDsc->status(DaqSlwControl::initialising,false);
      break;
    }
      
    case RcdHeader::slowControl: {
      _pDsc->status(DaqSlwControl::running,false);
      break;
    }
      
    case RcdHeader::slowReadout: {
      _pDsc->status(DaqSlwControl::running,false);
      break;
    }
/*      
    case RcdHeader::slowUncontrol: { 
      _pDsc->status(DaqSlwControl::initialising,false);
      break;
    }
*/
    case RcdHeader::slowEnd: {
      _pDsc->status(DaqSlwControl::waiting,false);
      break;
    }

    case RcdHeader::shutdown: {
      _pDsc->status(DaqSlwControl::dead,false);
      break;
    }

    default: {
      _pDsc->status(DaqSlwControl::endOfStateEnum,false);
      break;
    }
    };
    
    // This wakes the status processes
    _pDsc->count(r);

    return true;
  }
  
private:
  ShmObject<DaqSlwControl> _shmSlwControl;
  DaqSlwControl *_pDsc;
};

#endif
