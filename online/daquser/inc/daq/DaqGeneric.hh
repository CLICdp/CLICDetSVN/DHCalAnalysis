#ifndef DaqGeneric_HH
#define DaqGeneric_HH

#include <cassert>

#include "RcdMultiUserRO.hh"


class DaqGeneric : public RcdMultiUserRO {

public:

  // Set default bits to enable required modules
  enum {
    //defaultDaqGenericBits=0x1763 // = 0b 00010111 01100011
    defaultDaqGenericBits=0x3107 // = 0b 00110001 00000111
  };


  DaqGeneric(const UtlPack bits=defaultDaqGenericBits) : _ignorRunType(false) {

    // Add modules

    if(bits.bit( 0)) {
      //_vModule.push_back(new SOMEMODULE);
      //addUser(*(_vModule[_vModule.size()-1]));
    } else {
      std::cout << "DaqGeneric::ctor() SOMEMODULE disabled" << std::endl;
    }
  }

  virtual ~DaqGeneric() {
    for(unsigned i(0);i<_vModule.size();i++) delete _vModule[i];
  }

  virtual bool record(const RcdRecord &r) {

    // Possibly disable histogramming
    if(r.recordType()==RcdHeader::runStart) {
      if(!_ignorRunType) {
	SubAccessor accessor(r);
	std::vector<const DaqRunStart*>
	  v(accessor.extract<DaqRunStart>());

        if(v.size()==1) {
          printLevel(v[0]->runType().printLevel());
        }
      }
    }

    if(!RcdMultiUserRO::record(r)) return false;

    return true;
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }


private:
  bool _ignorRunType;
  std::vector<RcdUserRO*> _vModule;
};

#endif
