#include <fstream>

unsigned daqReadRunNumber() {
  std::ifstream fin("data/runNumber.txt",std::ios::in);
  if(!fin) return 0;
  unsigned n(0);
  fin >> n;
  return n;
}

bool daqWriteRunNumber(unsigned n) {
  std::ofstream fout("data/runNumber.txt",std::ios::out);
  if(!fout) return false;
  fout << n << std::endl;
  return true;
}
