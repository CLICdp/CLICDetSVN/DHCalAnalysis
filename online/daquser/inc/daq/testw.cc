#include <iostream>

#include "RcdArena.hh"
#include "RcdWriterAsc.hh"
#include "RcdWriterBin.hh"
#include "DaqMessage.hh"
#include "DaqSubRecordId.hh"

using namespace std;

int main() {
  RcdWriterBin w;
  w.open("test.bin");
  //RcdWriterAsc w;
  //w.open("test.asc");

  RcdArena a;

  // Start Run record
  a.updateRecordTime();
  a.recordType(RcdHeader::startRun);
  a.deleteSubHeaders();

  DaqMessage m("This is a test");
  SubRecord<DaqMessage> r;
  r.payload(&m);
  r.numberOfBytes(m.numberOfBytes());
  a.appendSubHeader(&r);
  a.print(cout);
  w.write(&a);

  w.close();
}
