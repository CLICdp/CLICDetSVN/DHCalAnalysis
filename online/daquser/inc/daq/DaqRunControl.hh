#ifndef DaqRunControl_HH
#define DaqRunControl_HH

#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#include <string>

#include "RcdCount.hh"
#include "DaqControl.hh"


class DaqRunControl : public DaqControl {

public:
  enum {
    shmKey=0x76543211
  };

  enum Command {
    shutdown,
    wait,
    run,
    endOfCommandEnum
  };

  enum Status {
    waiting,
    initialising,
    runStarting,
    configurationStarting,
    spillStarting,
    eventing,
    spillEnding,
    configurationEnding,
    runEnding,
    endOfStatusEnum
  };

  DaqRunControl() {
    wakeSignal(SIGUSR2);
  }

  Command command() const {
    return (Command)DaqControl::command();
  }

  void command(Command s, bool p=true) {
    DaqControl::command(s,p);
  }

  Status status() const {
    return (Status)DaqControl::status();
  }

  void status(Status s, bool p=true) {
    DaqControl::status(s,p);
  }

  RcdCount& count() {
    return _count;
  }

  void count(const RcdRecord &r, bool p=true) {
    _count+=r;
    if(p) statusSignal();
  }

  void resetCount(bool p=true) {
    _count.reset();
    if(p) statusSignal();
  }

  std::string commandName() const {
    return commandName(command());
  }

  std::string commandName(Command s) const {
    if(s<endOfCommandEnum) return _commandName[s];
    else                   return "unknown";
  }

  std::string statusName() const {
    return statusName(status());
  }

  std::string statusName(Status s) const {
    if(s<endOfStatusEnum) return _statusName[s];
    else                  return "unknown";
  }

  std::ostream& print(std::ostream& o, std::string s="") const {
    o << s << "DaqRunControl::print()" << std::endl;

    DaqControl::print(o,s+" ");

    o << s << " Command = " << command() << " = " << commandName() << std::endl;
    o << s << " Status  = " << status() << " = " << statusName() << std::endl;
    
    _count.print(o,s+" ");

    return o;
  }

private:
  RcdCount _count;

  static const std::string _commandName[endOfCommandEnum];
  static const std::string _statusName[endOfStatusEnum];
};

const std::string DaqRunControl::_commandName[]={
  "shutdown",
  "wait",
  "run"
};

const std::string DaqRunControl::_statusName[]={
  "waiting",
  "initialising",
  "runStarting",
  "configurationStarting",
  "spillStarting",
  "eventing",
  "spillEnding",
  "configurationEnding",
  "runEnding"
};

#endif
