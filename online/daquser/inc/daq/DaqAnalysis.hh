#ifndef DaqAnalysis_HH
#define DaqAnalysis_HH

#include <cassert>

#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

#include "RcdUserRO.hh"
#include "SubAccessor.hh"


class DaqAnalysis : public RcdUserRO {

public:

  // Set default bits
  enum {
    defaultDaqAnalysisBits=0xffffffff
  };


  DaqAnalysis(const UtlPack bits=defaultDaqAnalysisBits) :
    _daqAnalysisName("DaqAnalysis"),
    _ignorRunType(true) {
  }

  virtual ~DaqAnalysis() {
    endRoot(true);
  }

  virtual bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << _daqAnalysisName << "::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    if(r.recordType()==RcdHeader::runStart) {
      SubAccessor accessor(r);

      // Possibly set print level
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      if(v.size()>0) {
	if(!_ignorRunType) printLevel(v[0]->runType().printLevel());
	_runStart=*(v[0]);
      }

      // Open root file
      std::ostringstream sFile;
      sFile << _daqAnalysisName << std::setfill('0') << std::setw(6)
	    << _runStart.runNumber() << ".root";
      _rootFileName=sFile.str();
        
      if(doPrint(r.recordType())) {
	std::cout << _daqAnalysisName << "::record()" << std::endl
		  << " Creating ROOT file " << _rootFileName << std::endl << std::endl;
      }
      _rootFile = new TFile(sFile.str().c_str(),"RECREATE");

      // Define histograms here
    }

    if(r.recordType()==RcdHeader::runEnd) {
      endRoot(doPrint(r.recordType()));
    }

    return true;
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }

  virtual void endRoot(bool p) {
    if(_rootFile!=0) {
      if(p) {
	std::cout << _daqAnalysisName << "::endRoot()" << std::endl
		  << " Closing ROOT file " << _rootFileName << std::endl << std::endl;
      }

      _rootFile->cd();
      _rootFile->Write();
      _rootFile->Close();

      delete _rootFile;
      _rootFile=0;
    }
  }

private:
  std::string _daqAnalysisName;
  DaqRunStart _runStart;
  bool _ignorRunType;
  std::string _rootFileName;
  TFile *_rootFile;
};

#endif
