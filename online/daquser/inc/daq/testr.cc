#include <iostream>

#include "RcdArena.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"
#include "DaqMessage.hh"
#include "DaqSubRecordId.hh"

using namespace std;

int main() {
  RcdReaderBin r;
  r.open("test.bin");
  //RcdReaderAsc r;
  //r.open("test.asc");

  RcdArena a;

  while(r.read(&a)) {
    a.print(cout);

    RcdSubHeader *h(a.firstSubHeader());

    DaqMessage *m(SubRecord<DaqMessage>::safePayload(h));
    if(m==0) {
      cerr << "No DaqMessage" << endl;
      return 0;
    }
    m->print(cout);
  }

  r.close();
}
