#ifndef DaqSlwControl_HH
#define DaqSlwControl_HH

#include <sys/types.h>
#include <unistd.h>

#include <string>

#include "ChkCount.hh"


class DaqSlwControl {

public:
  enum Command {
    shutdown,
    wait,
    run,
    endOfCommandEnum
  };

  enum Status {
    waiting,
    initialising,
    running,
    endOfStatusEnum
  };

  DaqSlwControl() : _processId(0),
		    _command(endOfCommandEnum), _status(endOfStatusEnum) {
  }

  void processId() {
    _processId=getpid();
  }

  void processId(pid_t p) {
    _processId=p;
  }

  Command command() const {
    return _command;
  }

  void command(Command s, bool p=true) {
    _command=s;
    if(p && _processId>0) kill(_processId,SIGUSR1);
  }

  Status status() const {
    return _status;
  }

  void status(Status s) {
    _status=s;
  }

  ChkCount* count() {
    return &_count;
  }

  std::string commandName() const {
    return commandName(_command);
  }

  std::string commandName(Command s) const {
    if(s<endOfCommandEnum) return _commandName[s];
    else                   return "unknown";
  }

  std::string statusName() const {
    return statusName(_status);
  }

  std::string statusName(Status s) const {
    if(s<endOfStatusEnum) return _statusName[s];
    else                  return "unknown";
  }

  std::ostream& print(std::ostream& o, std::string s="") const {
    o << s << "DaqSlwControl::print()" << std::endl;
    o << s << " SlwData process id = " << _processId << std::endl;
    o << s << " Command = " << _command << " = " << commandName() << std::endl;
    o << s << " Status  = " << _status << " = " << statusName() << std::endl;
    
    _count.print(o,s+" ");

    return o;
  }

private:
  pid_t _processId;
  Command _command;
  Status _status;
  ChkCount _count;

  static const std::string _commandName[endOfCommandEnum];
  static const std::string _statusName[endOfStatusEnum];
};

const std::string DaqSlwControl::_commandName[]={
  "shutdown",
  "wait",
  "run"
};

const std::string DaqSlwControl::_statusName[]={
  "waiting",
  "initialising",
  "running"
};

#endif
