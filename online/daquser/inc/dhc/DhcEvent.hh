#ifndef EVENT_HH
#define EVENT_HH

#include "TObject.h"
#include "TClonesArray.h"
#include "TBits.h"
#include "TTimeStamp.h"
#include "DhcCluster.hh"
#include "WireChamberHit.hh"

#include <set>
#include <vector>
#include <list>
#include <map>

class DhcEventHeader {

private:
	int eventNumber;
	TTimeStamp timeStamp;

public:
	DhcEventHeader() :
			eventNumber(0), timeStamp(0) {
	}
	virtual ~DhcEventHeader() {
	}
	void set(int evtNumber, TTimeStamp& evtTimeStamp) {
		eventNumber = evtNumber;
		timeStamp = evtTimeStamp;
	}
	int getEventNumber() const {
		return eventNumber;
	}
	TTimeStamp* getEventTimeStamp() {
		return &timeStamp;
	}
ClassDef(DhcEventHeader,1)  //Event Header
};

class DhcEvent: public TObject {
private:
	DhcEventHeader eventHeader;
	std::set<const DhcHit*> hits; // set of all hits
	std::set<const WireChamberHit*> wireChamberHits;
	TBits triggerBits;             //Bits triggered by this event.
	int triggerTimeStamp;

	mutable std::map<int, std::set<const DhcHit*> > layerHits;
	mutable std::map<int, std::vector<DhcCluster> > layerClusters2D;
	mutable std::vector<DhcCluster> clusters3D;
	mutable bool isClustered;
	
	// cached variables
	mutable int nHits;
	mutable int nHitLayers;
	mutable int maxHitsPerLayer;
	mutable int interactionLayer;
	mutable double baryCenterLayer;
	mutable double baryCenterZ;
	mutable double density;
	
	// clustering parameters
	mutable int minClustersForInteractionLayer;
	

public:
	DhcEvent() :
			triggerTimeStamp(0), isClustered(false), minClustersForInteractionLayer(3) {
	}
	;

	virtual ~DhcEvent();

	void Clear(Option_t *option = "");
	DhcHit* addHit(double x, double y, double z, int layer, int timeStamp);
	WireChamberHit* addWireChamberHit(double x, double y, double z, int layer);

	std::map<int, std::set<const DhcHit*> > getLayerHitsMap() const;
	std::set<const DhcHit*> getLayerHits(int layer) const;
	std::vector<DhcCluster> get2DClustersInLayer(int layer) const;
	std::map<int, std::vector<DhcCluster> > get2DClusters(int minHits = 0) const;
	std::vector<DhcCluster> get3DClusters(int minHits = 0) const;

	DhcEventHeader* GetHeader() {
		return &eventHeader;
	}
	int getNHits() const;
	int getNHitLayers() const;
	int getMaxHitsPerLayer() const;
	std::set<const DhcHit*> getHits() const {
		return hits;
	}
	std::set<const WireChamberHit*> getWireChamberHits() const {
		return wireChamberHits;
	}
	TBits& getTriggerBits() {
		return triggerBits;
	}
	int getTriggerTimeStamp() const {
		return triggerTimeStamp;
	}

	void findClusters(double xMargin = 10., double yMarginY = 10., int layerMargin = 1) const;

	int getInteractionLayer(int minClusters = 3);
	double getBaryCenterZ() const;
	double getDensity() const;
	void printClusters() const;
	
	inline void setHeader(int eventNumber, TTimeStamp& timeStamp) {
		eventHeader.set(eventNumber, timeStamp);
	}

	inline void setTriggerBits(const int* f) {
		triggerBits.Set(32, f);
	}

	inline void setTriggerTimeStamp(int timeStamp) {
		triggerTimeStamp = timeStamp;
	}

	ClassDef(DhcEvent,1)  //Event structure
};

#endif
