#ifndef DhcConfigWriter_HH
#define DhcConfigWriter_HH

#include "tinyxml.h"

#include "SubAccessor.hh"
#include "SubRecordType.hh"

#include "DhcLocation.hh"
#include "DhcLocationData.hh"
#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcFeConfigurationData.hh"


class DhcConfigWriter {
public:
  DhcConfigWriter(const char* s);
  virtual ~DhcConfigWriter();

  bool save(const RcdRecord& r);

private:
  const char* _docName;
  TiXmlDocument _doc;
};


DhcConfigWriter::DhcConfigWriter(const char* s)
  : _docName(s)
{
}

DhcConfigWriter::~DhcConfigWriter()
{
}

bool
DhcConfigWriter::save(const RcdRecord& r)
{
  SubAccessor accessor(r);

  std::vector<const DhcLocationData<DhcFeConfigurationData>*>
    fe(accessor.access< DhcLocationData<DhcFeConfigurationData> >());

  for (unsigned f(0); f<fe.size(); f++) {
    TiXmlElement * feData = new TiXmlElement( "FeConfiguration" );  
    _doc.LinkEndChild( feData );  

    TiXmlElement * location;
    location = new TiXmlElement( "location" );  
    feData->LinkEndChild( location );  
    location->SetAttribute("crate", fe[f]->location().crateNumber());
    location->SetAttribute("slot", fe[f]->location().slotNumber());

    TiXmlElement * dcon;
    dcon = new TiXmlElement( "dcon" );  
    feData->LinkEndChild( dcon );  
    dcon->SetAttribute("con", fe[f]->location().dhcComponentName());

    TiXmlElement * dcal;
    dcal = new TiXmlElement( "dcal" );  
    feData->LinkEndChild( dcal );  
    dcal->SetAttribute("chip", *fe[f]->data()->chipid());
    dcal->SetAttribute("plsr", *fe[f]->data()->plsr());
    dcal->SetAttribute("intd", *fe[f]->data()->intd());
    dcal->SetAttribute("shp2", *fe[f]->data()->shp2());
    dcal->SetAttribute("shp1", *fe[f]->data()->shp1());
    dcal->SetAttribute("blrd", *fe[f]->data()->blrd());
    dcal->SetAttribute("vtnd", *fe[f]->data()->vtnd());
    dcal->SetAttribute("vtpd", *fe[f]->data()->vtpd());

    std::ostringstream o;
    o << "0x" << std::setfill('0') << std::setw(2)
      << std::hex << (unsigned)*fe[f]->data()->dcr() << std::dec << std::setfill(' ');
    dcal->SetAttribute("dcr", o.str());
    o.seekp(std::ios_base::beg);

    unsigned long long mask;
    memcpy(&mask, fe[f]->data()->inj(),sizeof(mask));
    o << "0x" << std::setfill('0') << std::setw(16)
      << std::hex << mask << std::dec << std::setfill(' ');
    dcal->SetAttribute("inj", o.str());
    o.seekp(std::ios_base::beg);

    memcpy(&mask, fe[f]->data()->kill(),sizeof(mask));
    o << "0x" << std::setfill('0') << std::setw(16)
      << std::hex << mask << std::dec << std::setfill(' ');
    dcal->SetAttribute("kill", o.str());
  }

  if (!_doc.SaveFile(_docName)) {
    std::cerr << "Error saving configuration file " << std::endl;
    return false;
  }

  return true;
}

#endif // DhcConfigWriter_HH
