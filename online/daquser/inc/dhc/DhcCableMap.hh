//
// $Id$
//

#ifndef DhcCableMap_HH
#define DhcCableMap_HH

#include <map>
#include <string>
#include <cstdio>

#include <iostream>

#include <sqlite3.h>

class DhcPadPts {

public:
  DhcPadPts(float& x, float& y, float& z, unsigned& layer) :
    Px(x), Py(y), Pz(z), Layer(layer), Ts(0)
  {}

  DhcPadPts(float& x, float& y, float& z, unsigned& layer, unsigned& t) :
    Px(x), Py(y), Pz(z), Layer(layer), Ts(t)
  {}

public:
  float Px;
  float Py;
  float Pz;
  unsigned Layer;
  unsigned Ts;
};


class DhcCableMap {

private:
  std::map<unsigned, DhcPadPts*> _cmap;
  std::map<unsigned, std::string> _lmap;
  std::map<unsigned, int> _layermap;
  sqlite3 *db;

  const std::string dbname(const unsigned& runNumber);
  void load(const std::string& tablename);

public:
  DhcCableMap(const unsigned& runNumber);
  ~DhcCableMap();

  DhcPadPts* origin(unsigned c, unsigned s, unsigned d);
  std::string label(unsigned c, unsigned s, unsigned d);
};

#ifdef CALICE_DAQ_ICC

DhcCableMap::DhcCableMap(const unsigned& runNumber)
{
  assert(sqlite3_open("cfg/dhcal/cablemap/cablemaps.db", &db) == 0);

  const std::string tablename(dbname(runNumber));
  assert(!tablename.empty());

  load(tablename);
}

DhcCableMap::~DhcCableMap()
{
  sqlite3_close(db);

  std::map<unsigned, DhcPadPts*>::iterator it;
  for (it=_cmap.begin(); it!=_cmap.end(); it++)
    delete (*it).second;
}

const std::string
DhcCableMap::dbname(const unsigned& runNumber)
{
  char *zErrMsg;
  char **result;
  int nrow,ncol;

  std::string query("SELECT * FROM table_names");

  assert(sqlite3_get_table(db, query.c_str(),&result,&nrow,&ncol,&zErrMsg) == SQLITE_OK);

  char label[18];
  for (int i=1; i<=nrow; i++)
    {
      unsigned runLo, runHi;
      char** row(&result[i*ncol]);

      sscanf(row[0], "%s", label);

      sscanf(row[1], "%d", &runLo);
      sscanf(row[2], "%d", &runHi);

      if (runLo <= runNumber && runNumber <= runHi)
	break;
    }

  sqlite3_free_table(result);
  return std::string(label);
}

void
DhcCableMap::load(const std::string& tablename)
{
  char *zErrMsg;
  char **result;
  int nrow,ncol;

  std::string query("SELECT * FROM "+tablename);

  assert(sqlite3_get_table(db, query.c_str(),&result,&nrow,&ncol,&zErrMsg) == SQLITE_OK);

  for (int i=1; i<=nrow; i++)
    {
      char label[10];
      unsigned crate, slot, dcon, layer;
      float x, y, z;
      char** row(&result[i*ncol]);

      sscanf(row[0], "%s", label);
      
      char layerString[2];
      layerString[0] = row[0][3];
      layerString[1] = row[0][4];
      sscanf(row[0], "%s", label);
      sscanf(layerString, "%d", &layer);
      
      std::string sLabel = std::string(label);

      sscanf(row[1], "%d", &crate);
      sscanf(row[2], "%d", &slot);
      sscanf(row[3], "%d", &dcon);

      sscanf(row[4], "%f", &x);
      sscanf(row[5], "%f", &y);
      sscanf(row[6], "%f", &z);

      _cmap[(crate<<16) | (slot<<8) | (dcon)] = new DhcPadPts(x, y, z, layer);
      _lmap[(crate<<16) | (slot<<8) | (dcon)] = std::string(label);
    }

  sqlite3_free_table(result);
}

DhcPadPts* DhcCableMap::origin(unsigned c,  unsigned s, unsigned d) {
  return (_cmap.find((c<<16) | (s<<8) | d) == _cmap.end())
    ? 0 : _cmap[(c<<16) | (s<<8) | d];
}

std::string DhcCableMap::label(unsigned c,  unsigned s, unsigned d) {
  return (_lmap.find((c<<16) | (s<<8) | d) == _lmap.end())
    ? "" : _lmap[(c<<16) | (s<<8) | d];
}

#endif // CALICE_DAQ_ICC
#endif // DhcCableMap_HH

