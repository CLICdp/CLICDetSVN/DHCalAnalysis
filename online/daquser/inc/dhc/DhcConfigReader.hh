//
// $Id: DhcConfigReader.hh,v 1.4 2008/02/28 13:40:31 jls Exp $
//

#ifndef DhcConfigReader_HH
#define DhcConfigReader_HH

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>

#include "tinyxml.h"

#include "SubModifier.hh"
#include "SubInserter.hh"
#include "SubRecordType.hh"

#include "DhcLocation.hh"
#include "DhcLocationData.hh"
#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcFeConfigurationData.hh"
#include "DhcReadoutConfigurationData.hh"
#include "DhcTriggerConfigurationData.hh"

#include "TtmLocation.hh"
#include "TtmLocationData.hh"
#include "TtmConfigurationData.hh"


class DhcConfigReader {

public:
  enum DhcElement {
    location=0x0,
    dcol,
    dcon,
    dcal,
    dhcal,
    dhtrg,
    ttm,
    trigger,
    pollInterval,
    beWaitInterval,
    enable,
    endOfDhcElementEnum
  };

  enum DhcAttribute {
    id=0x0,
    crate,
    slot,
    dcon_enable,
    csr,
    con,
    chip,
    plsr,
    intd,
    shp2,
    shp1,
    blrd,
    vtnd,
    vtpd,
    dcr,
    inj,
    kill,
    type,
    sec,
    usec,
    feslot,
    ferev,
    trigger_timestamp,
    zero_suppress,
    trigger_width,
    trigger_delay,
    dcal_enable,
    endOfDhcAttributeEnum
  };

  DhcConfigReader(const char* s);
  virtual ~DhcConfigReader();

  bool load(RcdRecord& r);
  void load_objects(TiXmlElement* pElem);

private:
  TiXmlDocument _doc;

  std::map<std::string, unsigned> _elementMap;
  std::map<std::string, unsigned> _attributeMap;
  std::map<std::string, unsigned> _componentMap;

  static const std::string _dhcElementName[endOfDhcElementEnum];
  static const std::string _dhcAttributeName[endOfDhcAttributeEnum];
  static const std::string _dhcComponentName[DhcLocation::endOfDhcComponentEnum];

  DhcLocation _location;
  TtmLocation _trgLocation;
  DhcBeConfigurationData _beData;
  DhcDcConfigurationData _dcData;
  DhcFeConfigurationData _feData;
  DhcReadoutConfigurationData _roData;
  DhcTriggerConfigurationData _trgData;

};

DhcConfigReader::DhcConfigReader(const char* s)
  : _doc(s)
{
  for (unsigned i(0); i<endOfDhcElementEnum; i++ )
  _elementMap[_dhcElementName[i]] = i;

  for (unsigned i(0); i<endOfDhcAttributeEnum; i++ )
  _attributeMap[_dhcAttributeName[i]] = i;

  for (unsigned i(0); i<DhcLocation::endOfDhcComponentEnum; i++ )
  _componentMap[_dhcComponentName[i]] = i;

}

DhcConfigReader::~DhcConfigReader()
{
}

bool
DhcConfigReader::load(RcdRecord& r)
{
  if (!_doc.LoadFile()) {
    std::cerr << "Error loading configuration file " << std::endl;
    return false;
  }

  TiXmlHandle hDoc(&_doc);
  TiXmlElement* pElem;
  TiXmlHandle hRoot(0);

  SubInserter inserter(r);

  // block: name
  {
    pElem=hDoc.FirstChildElement().Element();
    // should always have a valid root but handle gracefully if it does
    if (!pElem) return false;

    // save this for later
    hRoot=TiXmlHandle(pElem);
  }
 
  // block: TrigerConfiguration
  {
    pElem=hRoot.FirstChild( "TriggerConfiguration" ).Element();
    for( ; pElem; pElem=pElem->NextSiblingElement("TriggerConfiguration"))
      {
	_trgLocation.slotNumber(0);
	_trgLocation.write(1);
	load_objects(pElem);

	inserter.insert< DhcTriggerConfigurationData >(_trgData);

	if(_trgLocation.slotNumber())
	  {
	    TtmLocationData<TtmConfigurationData>
	      *t(inserter.insert< TtmLocationData<TtmConfigurationData> >());
	  
	    _trgLocation.crateNumber(_trgData.crateNumber());
	    t->location(_trgLocation);
	  }
      }
  }

  // block: ReadoutConfiguration
  {
    pElem=hRoot.FirstChild( "ReadoutConfiguration" ).Element();
    for( ; pElem; pElem=pElem->NextSiblingElement("ReadoutConfiguration"))
      {
	_trgLocation.slotNumber(0);
	_trgLocation.write(1);
	_roData.reset();

	load_objects(pElem);

	inserter.insert< DhcReadoutConfigurationData >(_roData);

	if(_trgLocation.slotNumber())
	  {
	    TtmLocationData<TtmConfigurationData>
	      *t(inserter.insert< TtmLocationData<TtmConfigurationData> >());
	  
	    _trgLocation.crateNumber(_roData.crateNumber());
	    t->location(_trgLocation);
	  }
      }
  }

  SubModifier modifier(r);
  std::vector< DhcReadoutConfigurationData* >
    vRcd(modifier.access<DhcReadoutConfigurationData>());

  // block: BeConfiguration
  {
    pElem=hRoot.FirstChild( "BeConfiguration" ).Element();
    for( ; pElem; pElem=pElem->NextSiblingElement("BeConfiguration"))
      {
	_location.write(1);
	load_objects(pElem);

	for(unsigned c(0);c<vRcd.size();c++)
	  {
	    if (_location.crateNumber() == vRcd[c]->crateNumber() ||
		_location.crateNumber() == 0xff)
	      {
		for(unsigned s(2);s<=21;s++)
		  if (vRcd[c]->slotEnable(s) &&
		      (s ==_location.slotNumber() || _location.slotBroadcast()))
		    {
		      _beData.dconEnable(vRcd[c]->slotFeEnables(s));

		      DhcLocationData<DhcBeConfigurationData>
			*b(inserter.insert< DhcLocationData<DhcBeConfigurationData> >());
		  
		      b->location(DhcLocation(vRcd[c]->crateNumber(), s, DhcLocation::be, 1));
		      b->data(_beData);
		    }
	      }
	  }
      }
  }

  // block: DcConfiguration
  {
    pElem=hRoot.FirstChild( "DcConfiguration" ).Element();
    for( ; pElem; pElem=pElem->NextSiblingElement("DcConfiguration"))
      {
	_location.write(1);
	load_objects(pElem);

	if (_location.crateNumber() == 0xff) {
	  for(unsigned c(0);c<vRcd.size();c++) {

	    DhcLocationData<DhcDcConfigurationData>
	      *f(inserter.insert< DhcLocationData<DhcDcConfigurationData> >());

	    f->location(DhcLocation(vRcd[c]->crateNumber(),
				    _location.slotNumber(),
				    _location.componentNumber(),
				    1));
	    f->data(_dcData);
	  }
	}
	else
	  {
	    DhcLocationData<DhcDcConfigurationData>
	      *f(inserter.insert< DhcLocationData<DhcDcConfigurationData> >());

	    f->location(_location);
	    f->data(_dcData);
	  }
      }
  }

  // block: FeConfiguration
  {
    pElem=hRoot.FirstChild( "FeConfiguration" ).Element();
    for( ; pElem; pElem=pElem->NextSiblingElement("FeConfiguration"))
      {
	_location.write(1);
	load_objects(pElem);

	if (_location.crateNumber() == 0xff) {
	  for(unsigned c(0);c<vRcd.size();c++) {

	    DhcLocationData<DhcFeConfigurationData>
	      *f(inserter.insert< DhcLocationData<DhcFeConfigurationData> >());

	    f->location(DhcLocation(vRcd[c]->crateNumber(),
				    _location.slotNumber(),
				    _location.componentNumber(),
				    1));
	    f->data(_feData);
	  }
	}
	else
	  {
	    DhcLocationData<DhcFeConfigurationData>
	      *f(inserter.insert< DhcLocationData<DhcFeConfigurationData> >());

	    f->location(_location);
	    f->data(_feData);
	  }
      }
  }

  return true;
}

void
DhcConfigReader::load_objects(TiXmlElement* pElem)
{
  for( pElem=pElem->FirstChildElement(); pElem; pElem=pElem->NextSiblingElement())
    {
      unsigned _element=0xffff;

      std::string pKey(pElem->Value());
      if (_elementMap.find(pKey) != _elementMap.end()) {
	_element = _elementMap[pKey];
      }

      TiXmlAttribute* pAttrib=pElem->FirstAttribute();
      while (pAttrib)
	{
	  unsigned _attribute=0xffff;

	  std::string pAttr(pAttrib->Name());
	  if (_attributeMap.find(pAttr) != _attributeMap.end()) {
	    _attribute = _attributeMap[pAttr];

	  }

	  switch (_element) {
	  case location: {
	    switch (_attribute) {
	    case crate: {
	      const char* s=pAttrib->Value();
	      if (strcmp(s,"broadcast") == 0)
		_location.crateNumber(0xff);
	      else
		_location.crateNumber(strtol(s,0,0));
	      break;
	    }
	    case slot: {
	      const char* s=pAttrib->Value();
	      if (strcmp(s,"broadcast") == 0)
		_location.slotBroadcast(true);
	      else
		_location.slotNumber(strtol(s,0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case dcol: {
	    switch (_attribute) {
	    case dcon_enable: {
	      _beData.dconEnable(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case csr: {
	      _beData.csr(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case dcon: {
	    switch (_attribute) {
	    case con: {
	      if (_componentMap.find(pAttrib->Value()) != _componentMap.end()) {
		unsigned _component = _componentMap[std::string(pAttrib->Value())];
		_location.dhcComponent(static_cast<DhcLocation::DhcComponent>(_component));
	      }
	      break;
	    }

	    case trigger_timestamp: {
	      _dcData.controlTriggerTimestampEnable(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case zero_suppress: {
	      _dcData.controlZeroSuppressionEnable(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case trigger_width: {
	      _dcData.triggerWidth(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case trigger_delay: {
	      _dcData.triggerDelay(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case dcal_enable: {
	      unsigned dcalEnable(strtol(pAttrib->Value(),0,0));
	      _dcData.enable((const unsigned char*)(&dcalEnable));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case dcal: {
	    switch (_attribute) {
	    case chip: {
	      _feData.chipid(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case plsr: {
	      _feData.plsr(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case intd: {
	      _feData.intd(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case shp2: {
	      _feData.shp2(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case shp1: {
	      _feData.shp1(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case blrd: {
	      _feData.blrd(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case vtnd: {
	      _feData.vtnd(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case vtpd: {
	      _feData.vtpd(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case dcr: {
	      _feData.dcr(strtol(pAttrib->Value(),0,0));
	      break;
	    }
	    case inj: {
	      _feData.inj(strtoull(pAttrib->Value(),0,0));
	      break;
	    }
	    case kill: {
	      _feData.kill(strtoull(pAttrib->Value(),0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case dhcal: {
	    switch (_attribute) {
	    case crate: {
	      _roData.crateNumber(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case dhtrg: {
	    switch (_attribute) {
	    case crate: {
	      _trgData.crateNumber(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case ttm: {
	    switch (_attribute) {
	    case slot: {
	      _trgLocation.slotNumber(strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case type: {
	      const char* ttype=pAttrib->Value();
	      if (strcmp(ttype,"slave") == 0)
		_trgLocation.ttmComponent(TtmLocation::slave);
	      else if (strcmp(ttype,"master") == 0)
		_trgLocation.ttmComponent(TtmLocation::master);
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case enable: {
	    unsigned s;
	    switch (_attribute) {
	    case slot: {
	      s=strtol(pAttrib->Value(),0,0);
	      _roData.slotEnable(s,true);
	      break;
	    }

	    case feslot: {
	      _roData.slotFeEnables(s,strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    case ferev: {
	      _roData.slotFeRevision(s,strtol(pAttrib->Value(),0,0));
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case trigger: {
	    switch (_attribute) {

	    case type: {
	      const char* ttype=pAttrib->Value();
	      if (strcmp(ttype,"internal") == 0)
		_trgData.triggerInternally(1);
	      else if (strcmp(ttype,"external") == 0)
		_trgData.triggerExternally(1);
	      else if (strcmp(ttype,"waiting") == 0)
		_trgData.triggerByWaiting(1);
	      break;
	    }

	    default: {
	      break;
	    }
	    };
	    break;
	  }

	  case pollInterval: {
	    int s; int u;
	    switch (_attribute) {
	    case sec: {
	      s=strtol(pAttrib->Value(),0,0);
	      break;
	    }

	    case usec: {
	      u=strtol(pAttrib->Value(),0,0);
	      break;
	    }
	      
	    default: {
	      break;
	    }
	    };
	    _trgData.pollInterval(UtlTimeDifference(s,u));
	    break;
	  }

	  case beWaitInterval: {
	    int s; int u;
	    switch (_attribute) {
	    case sec: {
	      s=strtol(pAttrib->Value(),0,0);
	      break;
	    }

	    case usec: {
	      u=strtol(pAttrib->Value(),0,0);
	      break;
	    }
	      
	    default: {
	      break;
	    }
	    };
	    _roData.beWaitInterval(UtlTimeDifference(s,u));
	    break;
	  }

	  default: {
	    break;
	  }
	  };
	  
	  pAttrib=pAttrib->Next();
	}

      load_objects(pElem);
    }
}

const std::string DhcConfigReader::_dhcElementName[]={
  "location",  "dcol",  "dcon",  "dcal",  "dhcal",  "dhtrg",
  "ttm", "trigger", "pollInterval", "beWaitInterval", "enable"
};

const std::string DhcConfigReader::_dhcAttributeName[]={
  "id",  "crate",  "slot",   "dcon_enable",   "csr",   "con",
  "chip",  "plsr",  "intd",  "shp2", "shp1",
  "blrd",  "vtnd",  "vtpd",  "dcr",  "inj",  "kill",
  "type",  "sec",  "usec",  "feslot","ferev",
  "triggerTimestamp", "zeroSuppress",
  "triggerWidth", "triggerDelay", "dcal_enable"
};

const std::string DhcConfigReader::_dhcComponentName[]={
  "dc0",  "dc1",  "dc2",  "dc3",  "dc4",  "dc5",
  "dc6",  "dc7",  "dc8",  "dc9",  "dc10",  "dc11",
  "be",  "febroadcast"
};

#endif // DhcConfigReader_HH
