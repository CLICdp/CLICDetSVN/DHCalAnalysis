//
// $Id$
//

#ifndef DhcTrack_HH
#define DhcTrack_HH

#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <algorithm>

#include "DhcTree.hh"


class ClusterPt {

public:
    ClusterPt(float f, float s, unsigned n) :
        first(f), second(s), nHits(n)
    {};

    float first;
    float second;
    unsigned nHits;

};

class LineFitter {

public:
    LineFitter() {};
    virtual ~LineFitter();

    void addPoint(ClusterPt* p);
    bool fitPoints();

    double slope;
    double intercept;
    double residual;
    double chi2;

    std::set<ClusterPt*> _points;

};

class DhcCluster {

public:
    DhcCluster(int layer) :
        _layer(layer), _clx(NAN), _cly(NAN), _clz(NAN)
    {}

    void addHit(DhcHit* hit);
    const std::set<DhcHit*>& getHits() const;
    const unsigned getSize() const;
    const double getX();
    const double getY();
    const double getZ();

protected:
    double _layer;
    std::set<DhcHit*>  _hitList;

    double _clx;
    double _cly;
    double _clz;

};

class DhcTrack {

public:
    DhcTrack()  {}
    virtual ~DhcTrack()  {}

    const unsigned getLayerNHits(int Pz) const;
    const unsigned getNLayers() const;
    void clear();
    void addHit(DhcHit* hit);
    const std::set<DhcHit*>& getHits() const;
    std::set<DhcHit*>* getLayerHits(int Pz);
    std::set<DhcCluster*>* getLayerClusters(int Pz);

protected:
    std::set<DhcHit*>  _hitList;

private:
    std::map<double, unsigned> _hitCount;

};

#endif // DhcTrack_HH
        
