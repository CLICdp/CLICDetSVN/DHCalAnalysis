//
// $Id: DhcWriterBin.hh,v 1.1 2008/03/17 14:40:23 jls Exp $
//

#ifndef DhcWriterBin_HH
#define DhcWriterBin_HH

#include <string>
#include <vector>

#include "RcdWriter.hh"

#include "RcdRecord.hh"
#include "DaqEvent.hh"
#include "SubAccessor.hh"

#include "DhcFeHitData.hh"

class DhcWriterBin : public RcdWriter {

public:
  DhcWriterBin();
  virtual ~DhcWriterBin();

  bool open(const std::string &file);
  bool convert(const RcdRecord &r);
  bool convert(const std::vector<DhcFeHitData*> &r);
  bool close();


private:
  void reset();

  int _fileDescriptor;
};

#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <iostream>


DhcWriterBin::DhcWriterBin() : _fileDescriptor(-1) {
}

DhcWriterBin::~DhcWriterBin() {
  close();
}

bool DhcWriterBin::open(const std::string &file) {
  if(_fileDescriptor>=0) close();
  reset();

  _fileName=file+".bin";
  _fileDescriptor=::creat(_fileName.c_str(),S_IRUSR|S_IRGRP|S_IROTH);
  
  if(_fileDescriptor<0) {
    std::cerr << "DhcWriterBin::open()   Error opening file "
	      << _fileName << " " << strerror(errno)
	      << std::endl << std::flush;
    reset();
    return false;
  }
  
  if(_printLevel>0) {
    std::cout << "DhcWriterBin::open()   Opened file "
	      << _fileName << std::endl;
  }

  _isOpen=true;

  return true;
}

bool DhcWriterBin::convert(const std::vector<DhcFeHitData*>& r) {
  if(_fileDescriptor<0) return false;

  std::vector<DhcFeHitData*>::const_iterator hb(r.begin());
  std::vector<DhcFeHitData*>::const_iterator he(r.end());

  for (; hb!=he; hb++) {

    int n(sizeof(DhcFeHitData)),
      m(::write(_fileDescriptor,*hb,n));
    if(m!=n) {
      std::cerr << "DhcWriterBin::write()  Error writing to file;"
		<< "number of bytes written " << m << " < " << n
		<< " " << strerror(errno) << std::endl << std::flush;
      _numberOfBytes+=m;
      return false;
    }
    _numberOfBytes+=sizeof(DhcFeHitData);
  }

  return true;
}

bool DhcWriterBin::convert(const RcdRecord &r) {
  if(_fileDescriptor<0) return false;

  SubAccessor accessor(r);

  // Check record type
  switch (r.recordType()) {

  case RcdHeader::event: {
    std::vector<const DhcLocationData<DhcEventData>*>
      v(accessor.access<DhcLocationData<DhcEventData> >());
      
    for (unsigned s(0); s<v.size(); s++) {
      for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) {
	DhcFeHitData* hits = (DhcFeHitData*)(v[s]->data()->feData(i));
	hits->vmead(v[s]->crateNumber()&3);
      }
      int n(v[s]->data()->numberOfWords()*4),
	m(::write(_fileDescriptor,v[s]->data()->data(),n));
      if(m!=n) {
	std::cerr << "DhcWriterBin::write()  Error writing to file;"
		  << "number of bytes written " << m << " < " << n
		  << " " << strerror(errno) << std::endl << std::flush;
	_numberOfBytes+=m;
	return false;
      }
      _numberOfBytes+=v[s]->data()->numberOfWords()*4;
    }
    break;
  }  
    
  default: {
    break;
  }
  };

  return true;
}

bool DhcWriterBin::close() {
  if(_fileDescriptor<0) {
    reset();
    return false;
  }
  
  if(::close(_fileDescriptor)<0) {
    std::cerr << "DhcWriterBin::close()  Error closing file "
	      << _fileName << " " << strerror(errno)
	      << std::endl << std::flush;
    reset();
    return false;
  }
  
  if(_printLevel>0) {
    std::cout << "DhcWriterBin::close()  Closed file " << _fileName;
    if(_printLevel>1) std::cout << " after writing "
				<< _numberOfBytes << " bytes";
    std::cout << std::endl;
  }

  reset();
  return true;
}

void DhcWriterBin::reset() {
  RcdIoBase::reset();
  _fileDescriptor=-1;
}

#endif // DhcWriterBin_HH
