#ifndef DHCHIT_HH
#define DHCHIT_HH
#include "TVector3.h"

#include <sstream>
#include <string>

class DhcHit: public TVector3 {

private:
	int layer;
	int timeStamp;

public:
	DhcHit() :
			TVector3(), layer(0), timeStamp(0) {
	}

	DhcHit(double x, double y, double z, int layer, int timeStamp) :
			TVector3(x, y, z), layer(layer), timeStamp(timeStamp) {

	}

	virtual ~DhcHit() {
	}

	inline int Layer() const {
		return layer;
	}

	inline int T() const {
		return timeStamp;
	}

	std::string toString() const {
		std::stringstream ss;
		ss << "(" << X() << ", " << Y() << ", " << Z() << ")";
		return ss.str();
	}

ClassDef(DhcHit,3)  //DhcHit structure
};

#endif
