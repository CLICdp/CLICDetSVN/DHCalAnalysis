#ifndef WIRECHAMBERHIT_HH_
#define WIRECHAMBERHIT_HH_

#include "TVector3.h"

class WireChamberHit: public TVector3 {
private:
	int layer;

public:
	WireChamberHit() :
			TVector3(), layer(0) {

	}

	WireChamberHit(double x, double y, double z, int layer) :
			TVector3(x, y, z), layer(layer) {

	}

	virtual ~WireChamberHit() {
	}

	inline int Layer() const {
		return layer;
	}

ClassDef(WireChamberHit, 1) //WireChamberHit structure
};

#endif /* WIRECHAMBERHIT_HH_ */
