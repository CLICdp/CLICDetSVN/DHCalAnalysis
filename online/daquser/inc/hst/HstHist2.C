void HstHist2(const char* const label, unsigned sleepMs=1000) {

  // Histogram consumer script. Create a canvas and 3 pads. Connect
  // to memory mapped file "hsimple.map", that was created by hprod.C.
  // It reads the histograms from shared memory and displays them
  // in the pads (sleeping for 0.1 seconds before starting a new read-out
  // cycle). This script runs in an infinite loop, so use ctrl-c to stop it.
  
  gROOT->Reset();
  
  // Open the memory mapped file "hsimple.map" in "READ" (default) mode.
  mfile = TMapFile::Create("HstGeneric.map");

  // Print status of mapped file
  mfile->Print();
  
  // Create a new canvas
  TCanvas *hstHistCanvas(0);
   
  // Create pointer to the object in shared memory.
  TH2F *hstHist(0);
  hstHist=(TH2F*)mfile->Get(label);

  if(hstHist==0) {
    mfile->ls();    
    std::cout << "Histogram " << label << " not found" << std::endl;
    return;
  }

  hstHistCanvas=new 
    TCanvas("HstHistCanvas","HstHist",10,10,700,500);

  // Loop displaying the histograms

  while (hstHist!=0) {
    hstHist->Draw("BOX");
    hstHistCanvas->Modified();
    hstHistCanvas->Update();

    gSystem->Sleep(sleepMs);
    if (gSystem->ProcessEvents()) break;

    delete hstHist;
    hstHist=(TH2F*)mfile->Get(label);
  }
}
