#ifndef HstGeneOff_HH
#define HstGeneOff_HH

#include <string>

#include "TROOT.h"
#include "TApplication.h"
#include "TCanvas.h"

// dual/inc/rcd
#include "RcdRecord.hh"
#include "RcdMultiUserRO.hh"


class HstGeneOff : public RcdMultiUserRO {

public:
  HstGeneOff(bool i=true) : _interactive(i), _application(0) {

    if(_interactive) {
      _application=new TApplication("HstGeneOff Application",0,0);
      gROOT->Reset();
    }

    gROOT->SetStyle("Plain");

    _canvas=new TCanvas("label","label",400,10,400,600);

    _hstPut=new HstPut;
    addUser(*_hstPut);

    _canvas->Update();
  }

  virtual ~HstGeneOff() {
    _hstPut->update();
    _canvas->Update();
    _canvas->Print("HstGeneOff.ps");

    delete _application;
    _application=0;
    delete _hstPut;
  }

  bool interactive() const {
    return _interactive;
  }

private:
  const bool _interactive;
  TApplication *_application;
  TCanvas *_canvas;
  HstPut *_hstPut;
};

#endif
