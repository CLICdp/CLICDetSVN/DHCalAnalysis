#ifdef HST_MAP
#include "HstLalHodoscopeMap.hh"
typedef HstLalHodoscopeMap HstLalHodoscope;

#else
#include "HstLalHodoscopeShm.hh"
typedef HstLalHodoscopeShm HstLalHodoscope;
#endif
