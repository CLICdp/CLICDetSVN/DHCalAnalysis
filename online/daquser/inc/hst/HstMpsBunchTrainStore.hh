#ifndef HstMpsBunchTrainStore_HH
#define HstMpsBunchTrainStore_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstArray.hh"


class HstMpsBunchTrainStore : public HstFlags {

public:
  enum {
    shmKey=0x7654ddba
  };

  HstMpsBunchTrainStore() {
    reset();
  }

  void reset() {
    _size.reset();
    _errors.reset();
    for(unsigned s(0);s<4;s++) {
      _sensorId[s]=0;
      for(unsigned i(0);i<4;i++) {
	_sensor[s][i].reset();
      }
    }
  }

  void reset(unsigned n) {
    for(unsigned s(0);s<4;s++) {
      for(unsigned i(n);i<4;i++) {
	_sensor[s][i].reset();
      }
    }
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "HstMpsBunchTrainStore::print()" << std::endl;

    HstFlags::print(o,s+" ");

    return o;
  }

  HstArray<10> _size;
  HstArray<10> _errors;
  unsigned _sensorId[4];
  HstArray2<168,168> _sensor[4][4];

private:
};

#endif
