#ifdef HST_MAP
#include "HstBeTrgHistoryMap.hh"
typedef HstBeTrgHistoryMap HstBeTrgHistory;

#else
#include "HstBeTrgHistoryShm.hh"
typedef HstBeTrgHistoryShm HstBeTrgHistory;
#endif
