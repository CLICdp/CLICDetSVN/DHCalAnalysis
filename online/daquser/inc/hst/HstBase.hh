#ifndef HstBase_HH
#define HstBase_HH

#include <string>

#include "TROOT.h"
#include "TApplication.h"

// dual/inc/rcd
#include "RcdRecord.hh"
#include "RcdUserRO.hh"


class HstBase : public RcdUserRO {

public:
  HstBase(bool i=true) : _interactive(i), _application(0) {

    if(_interactive) {
      _application=new TApplication("HstBase Application",0,0);
      gROOT->Reset();
    }

    gROOT->SetStyle("Plain");
  }

  virtual ~HstBase() {
    delete _application;
    _application=0;
  }

  virtual bool update() = 0;

  virtual bool postscript(std::string) = 0;

  bool interactive() const {
    return _interactive;
  }

private:
  const bool _interactive;
  TApplication *_application;
};

#endif
