#ifndef HstTrgEvent_HH
#define HstTrgEvent_HH

#include <cassert>

#include "TMapFile.h"
#include "TH1F.h"

#include "RcdUserRO.hh"
#include "SubAccessor.hh"
#include "TrgSpillPollData.hh"


class HstTrgEvent : public RcdUserRO {

public:
  HstTrgEvent() {

    _hstTrgEventJobTime=new TH1F("HstTrgEventJobTime",
				 "Spill poll time in job (secs)",
				 100,0,0.2);
    _hstTrgEventRunTime=new TH1F("HstTrgEventRunTime",
				 "Spill poll time in run (secs)",
				 100,0,0.2);
    _hstTrgEventJobTriggers=new TH1F("HstTrgEventJobTriggers",
				     "Number of triggers per spill in job",
				     100,0,1000);
    _hstTrgEventRunTriggers=new TH1F("HstTrgEventRunTriggers",
				     "Number of triggers per spill in run",
				     100,0,1000);
    _hstTrgEventJobPolls=new TH1F("HstTrgEventJobPolls",
				  "Number of polls per spill in job",
				  100,0,10000);
    _hstTrgEventRunPolls=new TH1F("HstTrgEventRunPolls",
				  "Number of polls per spill in run",
				  100,0,10000);
  }

  virtual ~HstTrgEvent() {
    delete _hstTrgEventJobTime;
    delete _hstTrgEventRunTime;
    delete _hstTrgEventJobTriggers;
    delete _hstTrgEventRunTriggers;
    delete _hstTrgEventJobPolls;
    delete _hstTrgEventRunPolls;
  }

  /*
  void update() {
    _hstTrgEventJobBytes.Draw();
  }
  */
  /*
  void extend(TH1F *hst, unsigned extraBins) {
    double *c(new double[hst->GetNbinsX()]);

    for(int i(0);i<hst->GetNbinsX();i++) {
      c[i]=hst->GetBinContent(i+1);
    }

    const TAxis *axis(hst->GetXaxis());

    std::cout << "Extending from " << hst->GetNbinsX() << " bins, x range "
	      << axis->GetXmin() << " - " << axis->GetXmax() << " to " 
	      << hst->GetNbinsX()+extraBins << " bins, x range "
	      << axis->GetXmin() << " - "
	      << axis->GetXmax()+extraBins*hst->GetBinWidth(1) << std::endl;

    int oldBins(hst->GetNbinsX());
    hst->SetBins(hst->GetNbinsX()+extraBins,
		 axis->GetXmin(),
		 axis->GetXmax()+extraBins*hst->GetBinWidth(1));

    for(int i(0);i<oldBins;i++) {
      hst->SetBinContent(i+1,c[i]);
    }

    delete [] c;
  }
  */

  bool record(const RcdRecord &r) {
    /*
    if(_startJob.seconds()==0) {
      _startJob=r.recordTime();
      _startRun=r.recordTime();
    }
    */

    if(r.recordType()==RcdHeader::runStart) {

      // Reset all run hists
      _hstTrgEventRunTime->Reset();
      _hstTrgEventRunTriggers->Reset();
      _hstTrgEventRunPolls->Reset();
    }
    /*
    if(r.recordType()==RcdHeader::spill) {
      SubAccessor accessor(r);
      std::vector<const CrcLocationData<TrgSpillPollData>* >
        v(accessor.access< CrcLocationData<TrgSpillPollData> >());

      if(v.size()>0) {
	v[0]->print(std::cout,"HstTrgEvent  ");

	_hstTrgEventJobTime->Fill(v[0]->data()->actualPollTime().deltaTime());
	_hstTrgEventRunTime->Fill(v[0]->data()->actualPollTime().deltaTime());
	_hstTrgEventJobTriggers->Fill(v[0]->data()->actualNumberOfEvents());
	_hstTrgEventRunTriggers->Fill(v[0]->data()->actualNumberOfEvents());
	_hstTrgEventJobPolls->Fill(v[0]->data()->numberOfPolls());
	_hstTrgEventRunPolls->Fill(v[0]->data()->numberOfPolls());
      }
    }
    */
    return true;
  }

private:
  TH1F *_hstTrgEventJobTime;
  TH1F *_hstTrgEventRunTime;
  TH1F *_hstTrgEventJobTriggers;
  TH1F *_hstTrgEventRunTriggers;
  TH1F *_hstTrgEventJobPolls;
  TH1F *_hstTrgEventRunPolls;
};

#endif
