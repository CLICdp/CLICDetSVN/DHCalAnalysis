void HstAhcEvent(const float cut=100.0, const unsigned sleepMs=100) {

  // Histogram consumer script. Create a canvas and 3 pads. Connect
  // to memory mapped file "hsimple.map", that was created by hprod.C.
  // It reads the histograms from shared memory and displays them
  // in the pads (sleeping for 0.1 seconds before starting a new read-out
  // cycle). This script runs in an infinite loop, so use ctrl-c to stop it.
  
  gROOT->Reset();
  
  // Open the memory mapped file "hsimple.map" in "READ" (default) mode.
  TMapFile *mfile = TMapFile::Create("HstGeneric.map");

  // Print status of mapped file and list its contents
  mfile->Print();
  //mfile->ls();
  
  // Create a new canvas
  TCanvas *hstAhcEventCanvas(0);
   
  // Create pointer to the object in shared memory.
  TH1F *hstCrcEvent(0);
  hstCrcEvent=(TH1F*)mfile->Get("HstCrcEventCrate1");
  if(hstCrcEvent==0) return;

  hstAhcEventCanvas=new 
    TCanvas("HstAhcEventCanvas","HstAhcEvent",10,10,710,710);
  gPad->Range(0.0,-0.6,1.6,1.0);

  double e(0.002);

  TBox bgXY(0.1-e,0.1-e,0.9+e,0.9+e);
  bgXY.SetFillColor(1);
  bgXY.Draw("f");

  unsigned nBox(0);

  TBox *bXY[220];
  for(unsigned j(10);j<20;j++) {
    for(unsigned i(10);i<20;i++) {
      bXY[nBox]=new TBox(0.1+e+0.8*i/30.0,
			 0.1+e+0.8*j/30.0,
			 0.1-e+0.8*(i+1)/30.0,
			 0.1-e+0.8*(j+1)/30.0);
      bXY[nBox]->SetFillColor(0);
      bXY[nBox]->Draw("f");
      nBox++;
    }
  }

  for(unsigned j(2);j<13;j++) {
    for(unsigned i(2);i<13;i++) {
      if(i<5 || i>9 || j<5 || j>9) {
	bXY[nBox]=new TBox(0.1+e+1.6*i/30.0,
			   0.1+e+1.6*j/30.0,
			   0.1-e+1.6*(i+1)/30.0,
			   0.1-e+1.6*(j+1)/30.0);
	bXY[nBox]->SetFillColor(0);
	bXY[nBox]->Draw("f");
	nBox++;
      }
    }
  }

  for(unsigned i(2);i<12;i+=2) {
    bXY[nBox]=new TBox(0.1+e+1.6*i/30.0,
		       0.1+e+1.6*0/30.0,
		       0.1-e+1.6*(i+2)/30.0,
		       0.1-e+1.6*(0+2)/30.0);
    bXY[nBox]->SetFillColor(0);
    bXY[nBox]->Draw("f");
    nBox++;
  }

  for(unsigned i(2);i<12;i+=2) {
    bXY[nBox]=new TBox(0.1+e+1.6*13/30.0,
		       0.1+e+1.6*i/30.0,
		       0.1-e+1.6*(13+2)/30.0,
		       0.1-e+1.6*(i+2)/30.0);
    bXY[nBox]->SetFillColor(0);
    bXY[nBox]->Draw("f");
    nBox++;
  }

  for(unsigned i(3);i<13;i+=2) {
    bXY[nBox]=new TBox(0.1+e+1.6*i/30.0,
		       0.1+e+1.6*13/30.0,
		       0.1-e+1.6*(i+2)/30.0,
		       0.1-e+1.6*(13+2)/30.0);
    bXY[nBox]->SetFillColor(0);
    bXY[nBox]->Draw("f");
    nBox++;
  }

  for(unsigned i(3);i<13;i+=2) {
    bXY[nBox]=new TBox(0.1+e+1.6*0/30.0,
		       0.1+e+1.6*i/30.0,
		       0.1-e+1.6*(0+2)/30.0,
		       0.1-e+1.6*(i+2)/30.0);
    bXY[nBox]->SetFillColor(0);
    bXY[nBox]->Draw("f");
    nBox++;
  }

  bXY[nBox]=new TBox(0.1+e+1.6*0/30.0,
		     0.1+e+1.6*0/30.0,
		     0.1-e+1.6*(0+2)/30.0,
		     0.1-e+1.6*(0+3)/30.0);
  bXY[nBox]->SetFillColor(11);
  bXY[nBox]->Draw("f");
  nBox++;

  bXY[nBox]=new TBox(0.1+e+1.6*0/30.0,
		     0.1+e+1.6*13/30.0,
		     0.1-e+1.6*(0+3)/30.0,
		     0.1-e+1.6*(13+2)/30.0);
  bXY[nBox]->SetFillColor(11);
  bXY[nBox]->Draw("f");
  nBox++;

  bXY[nBox]=new TBox(0.1+e+1.6*13/30.0,
		     0.1+e+1.6*12/30.0,
		     0.1-e+1.6*(13+2)/30.0,
		     0.1-e+1.6*(12+3)/30.0);
  bXY[nBox]->SetFillColor(11);
  bXY[nBox]->Draw("f");
  nBox++;

  bXY[nBox]=new TBox(0.1+e+1.6*12/30.0,
		     0.1+e+1.6*0/30.0,
		     0.1-e+1.6*(12+3)/30.0,
		     0.1-e+1.6*(0+2)/30.0);
  bXY[nBox]->SetFillColor(11);
  bXY[nBox]->Draw("f");
  nBox++;

  std::cout << "XY nBox = " << nBox << std::endl;

  
  TBox bgXZ(0.1-e,-(0.1-e),0.9+e,-(0.5+e));
  bgXZ.SetFillColor(1);
  bgXZ.Draw("f");

  TBox *bXZ[18][40];
  for(unsigned j(0);j<40;j++) {
    nBox=0;

    for(unsigned i(10);i<20;i++) {
      bXZ[nBox][j]=new TBox(0.1+e+0.8*i/30.0,
			    -(0.1+e+0.4*j/40.0),
			    0.1-e+0.8*(i+1)/30.0,
			    -(0.1-e+0.4*(j+1)/40.0));
      bXZ[nBox][j]->SetFillColor(0);
      bXZ[nBox][j]->Draw("f");
      nBox++;
    }

    for(unsigned i(2);i<13;i++) {
      if(i<5 || i>9) {
	bXZ[nBox][j]=new TBox(0.1+e+1.6*i/30.0,
			      -(0.1+e+0.4*j/40.0),
			      0.1-e+1.6*(i+1)/30.0,
			   -(0.1-e+0.4*(j+1)/40.0));
	bXZ[nBox][j]->SetFillColor(0);
	bXZ[nBox][j]->Draw("f");
	nBox++;
      }
    }

    bXZ[nBox][j]=new TBox(0.1+e+3.2*0/30.0,
			  -(0.1+e+0.4*j/40.0),
			  0.1-e+3.2*(0+1)/30.0,
			  -(0.1-e+0.4*(j+1)/40.0));
    bXZ[nBox][j]->SetFillColor(0);
    bXZ[nBox][j]->Draw("f");
    nBox++;

    bXZ[nBox][j]=new TBox(0.1+e+1.6*13/30.0,
			  -(0.1+e+0.4*j/40.0),
			  0.1-e+1.6*(13+2)/30.0,
			  -(0.1-e+0.4*(j+1)/40.0));
    bXZ[nBox][j]->SetFillColor(0);
    bXZ[nBox][j]->Draw("f");
    nBox++;
  }
  
  std::cout << "XZ nBox = " << nBox << std::endl;

  TBox bgYZ(1.1-e,0.1-e,1.5+e,0.9+e);
  bgYZ.SetFillColor(1);
  bgYZ.Draw("f");

  TBox *bYZ[216][40];
  for(unsigned i(0);i<30;i++) {
    for(unsigned j(0);j<40;j++) {
      bYZ[i][j]=new TBox(1.1+e+0.4*j/40.0,
		         0.1+e+0.8*i/30.0,
			 1.1-e+0.4*(j+1)/40.0,
		         0.1-e+0.8*(i+1)/30.0);
      bYZ[i][j]->SetFillColor(0);
      bYZ[i][j]->Draw("f");
    }
  }
  
  TPaveLabel label(1.1,-0.3,1.5,-0.2,"NO DATA");
  label.Draw();

  TArrow aXYx(0.05,0.05,0.15,0.05);
  TArrow aXYy(0.05,0.05,0.05,0.15);
  aXYx.Draw();
  aXYy.Draw();

  TArrow aXZx(0.05,-0.05,0.15,-0.05);
  TArrow aXZz(0.05,-0.05,0.05,-0.15);
  aXZx.Draw();
  aXZz.Draw();

  TArrow aYZz(1.05,0.05,1.15,0.05);
  TArrow aYZy(1.05,0.05,1.05,0.15);
  aYZz.Draw();
  aYZy.Draw();

  unsigned map[216][40];

  for(unsigned i(0);i<216;i++) {
    for(unsigned j(0);j<40;j++) {
      map[i][j]=0;
    }
  }

  for(unsigned i(0);i<216;i++) {
    map[i][0]=12*8*18*12+3*18*12+i;
    map[i][1]=12*8*18*12+6*18*12+i;
    map[i][2]=12*8*18*12+7*18*12+i;
  }

  while (hstCrcEvent!=0) {
    //hstCrcEvent->Draw();

    /*
      std::cout << "Bin 1 = " << hstCrcEvent->GetBinContent(1) << std::endl;
      std::cout << "Bin 2 = " << hstCrcEvent->GetBinContent(2) << std::endl;
      std::cout << "Bin 3 = " << hstCrcEvent->GetBinContent(3) << std::endl;
      std::cout << "Bin 4 = " << hstCrcEvent->GetBinContent(4) << std::endl;
      std::cout << "Bin 5 = " << hstCrcEvent->GetBinContent(5) << std::endl;
      std::cout << "Bin 6 = " << hstCrcEvent->GetBinContent(6) << std::endl;
      std::cout << "Bin 7 = " << hstCrcEvent->GetBinContent(7) << std::endl;
      std::cout << "Bin 8 = " << hstCrcEvent->GetBinContent(8) << std::endl;
    */
    
    unsigned n[4];

    for(unsigned i(0);i<4;i++) n[i]=(unsigned)hstCrcEvent->GetBinContent(i+1);
    unsigned runNumber=(n[3]<<24)+(n[2]<<16)+(n[1]<<8)+(n[0]);
    
    for(unsigned i(0);i<4;i++) n[i]=(unsigned)hstCrcEvent->GetBinContent(i+5);
    unsigned evtNumber=(n[3]<<24)+(n[2]<<16)+(n[1]<<8)+(n[0]);
    
    //if(hstCrcEvent->GetBinContent(1)>0.5) {
    if(runNumber>0 || evtNumber>0) {
      
      // Set colours all to white
      for(unsigned i(0);i<216;i++) {
	bXY[i]->SetFillColor(0);
      }

      for(unsigned i(0);i<18;i++) {
	for(unsigned j(0);j<40;j++) {
	  bXZ[i][j]->SetFillColor(0);
	  bYZ[i][j]->SetFillColor(0);
	}
      }

      for(unsigned i(0);i<216;i++) {
	for(unsigned j(0);j<40;j++) {
	  
	  float signal(0.0);
	  if(map[i][j]>0) {
	    signal=hstCrcEvent->GetBinContent(1+map[i][j]);
              //std::cout << "signal = " << signal << std::endl;
	  }
	  
	  //Float_t py;
	  //gRandom->Rannor(signal,py);

	  if(signal<-cut) {
	    bXY[i]->SetFillColor(4);

	    if(i<100) bXZ[i%10][j]->SetFillColor(4);

	    if(i==100 || i==111 || i==122 || i==133 || i==139 ||
	       i==145 || i==151 || i==157 || i==163 || i==174 || i==185)
	      bXZ[10][j]->SetFillColor(4);
      /*	
	    if(i==101 || i==112 || i==123 || i==134 || i==140 ||
	       i==146 || i==152 || i==158 || i==164 || i==175 || i==186)
	      bXZ[11][j]->SetFillColor(4);
	    if(i==102 || i==113 || i==124 || i==135 || i==141 ||
	       i==147 || i==153 || i==159 || i==165 || i==176 || i==187)
	      bXZ[12][j]->SetFillColor(4);

	    if(i==103 || i==114 || i==125 || i==166 || i==177 || i==188) {
	      bXZ[0][j]->SetFillColor(4);
	      bXZ[1][j]->SetFillColor(4);
	    }
	    if(i==104 || i==115 || i==126 || i==167 || i==178 || i==189) {
	      bXZ[2][j]->SetFillColor(4);
	      bXZ[3][j]->SetFillColor(4);
	    }
	    if(i==105 || i==116 || i==127 || i==168 || i==179 || i==190) {
	      bXZ[4][j]->SetFillColor(4);
	      bXZ[5][j]->SetFillColor(4);
	    }
	    if(i==106 || i==117 || i==128 || i==169 || i==180 || i==191) {
	      bXZ[6][j]->SetFillColor(4);
	      bXZ[7][j]->SetFillColor(4);
	    }
	    if(i==107 || i==118 || i==129 || i==170 || i==181 || i==192) {
	      bXZ[8][j]->SetFillColor(4);
	      bXZ[9][j]->SetFillColor(4);
	    }

	    if(i==108 || i==119 || i==130 || i==136 || i==142 ||
	       i==148 || i==154 || i==160 || i==171 || i==182 || i==193)
	      bXZ[13][j]->SetFillColor(4);
	    if(i==109 || i==120 || i==131 || i==137 || i==143 ||
	       i==149 || i==155 || i==161 || i==172 || i==183 || i==194)
	      bXZ[14][j]->SetFillColor(4);
	    if(i==110 || i==121 || i==132 || i==138 || i==144 ||
	       i==150 || i==156 || i==162 || i==173 || i==184 || i==195)
	      bXZ[15][j]->SetFillColor(4);


	    //bYZ[j][k]->SetFillColor(4);

      */
	  }
	  if(signal>cut) {
	    bXY[i]->SetFillColor(2);
	    if(i<100) bXZ[i%10][j]->SetFillColor(2);

	    if(i==100 || i==111 || i==122 || i==133 || i==139 ||
	       i==145 || i==151 || i==157 || i==163 || i==174 || i==185)
	      bXZ[10][j]->SetFillColor(2);
	    /*
	    if(i==101 || i==112 || i==123 || i==134 || i==140 ||
	       i==146 || i==152 || i==158 || i==164 || i==175 || i==186)
	      bXZ[11][j]->SetFillColor(2);
	    if(i==102 || i==113 || i==124 || i==135 || i==141 ||
	       i==147 || i==153 || i==159 || i==165 || i==176 || i==187)
	      bXZ[12][j]->SetFillColor(2);

	    if(i==103 || i==114 || i==125 || i==166 || i==177 || i==188) {
	      bXZ[0][j]->SetFillColor(2);
	      bXZ[1][j]->SetFillColor(2);
	    }
	    if(i==104 || i==115 || i==126 || i==167 || i==178 || i==189) {
	      bXZ[2][j]->SetFillColor(2);
	      bXZ[3][j]->SetFillColor(2);
	    }
	    if(i==105 || i==116 || i==127 || i==168 || i==179 || i==190) {
	      bXZ[4][j]->SetFillColor(2);
	      bXZ[5][j]->SetFillColor(2);
	    }
	    if(i==106 || i==117 || i==128 || i==169 || i==180 || i==191) {
	      bXZ[6][j]->SetFillColor(2);
	      bXZ[7][j]->SetFillColor(2);
	    }
	    if(i==107 || i==118 || i==129 || i==170 || i==181 || i==192) {
	      bXZ[8][j]->SetFillColor(2);
	      bXZ[9][j]->SetFillColor(2);
	    }

	    if(i==108 || i==119 || i==130 || i==136 || i==142 ||
	       i==148 || i==154 || i==160 || i==171 || i==182 || i==193)
	      bXZ[13][j]->SetFillColor(2);
	    if(i==109 || i==120 || i==131 || i==137 || i==143 ||
	       i==149 || i==155 || i==161 || i==172 || i==183 || i==194)
	      bXZ[14][j]->SetFillColor(2);
	    if(i==110 || i==121 || i==132 || i==138 || i==144 ||
	       i==150 || i==156 || i==162 || i==173 || i==184 || i==195)
	      bXZ[15][j]->SetFillColor(2);

	      //bYZ[j][k]->SetFillColor(2);
      */
	  }
	}
      }
      
      std::ostringstream sout;
      sout << "Run   " << runNumber << ", Event " << evtNumber << std::endl;
      std::string temp(sout.str());
      label.SetLabel(temp.c_str());
      
      hstAhcEventCanvas->Modified();
      hstAhcEventCanvas->Update();
      
    } else {
      std::cout << "Bin 1 = " << hstCrcEvent->GetBinContent(1) << std::endl;
      std::cout << "Bin 2 = " << hstCrcEvent->GetBinContent(2) << std::endl;
      std::cout << "Bin 3 = " << hstCrcEvent->GetBinContent(3) << std::endl;
      std::cout << "Bin 4 = " << hstCrcEvent->GetBinContent(4) << std::endl;
      std::cout << "Bin 5 = " << hstCrcEvent->GetBinContent(5) << std::endl;
      std::cout << "Bin 6 = " << hstCrcEvent->GetBinContent(6) << std::endl;
      std::cout << "Bin 7 = " << hstCrcEvent->GetBinContent(7) << std::endl;
      std::cout << "Bin 8 = " << hstCrcEvent->GetBinContent(8) << std::endl;
    }

    gSystem->Sleep(sleepMs);
    if (gSystem->ProcessEvents()) break;

    delete hstCrcEvent;
    hstCrcEvent=(TH1F*)mfile->Get("HstCrcEventCrate1");
  }
}
