#ifndef HstSingleChannel_HH
#define HstSingleChannel_HH

#include <cassert>

//#include "TROOT.h"
#include "TMapFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TRandom.h"

#include "DaqRunStart.hh"

#include "RcdUserRO.hh"
#include "RcdWriterDmy.hh"
#include "RcdWriterAsc.hh"
#include "RcdWriterBin.hh"

#include "UtlRollingAverage.hh"
#include "SubAccessor.hh"


class HstSingleChannel : public RcdUserRO {

public:
  HstSingleChannel(unsigned c, unsigned s, unsigned f, unsigned a, unsigned m) :
  _crate(c), _slot(s), _fe(f), _chip(a), _channel(m) {
    _signal=new TH1F("SingleChannel","Single Channel",65536,0,65536);
    _signal->Draw();
  }

  virtual ~HstSingleChannel() {
    delete _signal;
  }

  bool record(const RcdRecord &r) {
    //r.RcdHeader::print(std::cout);

    if(r.recordType()==RcdHeader::event) {
      SubAccessor accessor(r);

      std::vector<const CrcLocationData<CrcVlinkEventData>* >
	v(accessor.access< CrcLocationData<CrcVlinkEventData> >());

      for(unsigned i(0);i<v.size();i++) {
	if(((v[i]->crateNumber()==0xec && _crate==0) ||
	    (v[i]->crateNumber()==0xac && _crate==1)) &&
	   v[i]->slotNumber()==_slot) {
	  const CrcVlinkFeData *fd(v[i]->data()->feData(_fe));
	  if(fd!=0) {
	    const CrcVlinkAdcSample *as(fd->adcSample(_channel));
	    if(as!=0) {
	      _signal->Fill(as->adc(_chip));
	    }
	  }
	}
      }

      return true;
    }

    return true;
  }

private:
  unsigned _crate,_slot,_fe,_chip,_channel;
  TH1F *_signal;
};

#endif
