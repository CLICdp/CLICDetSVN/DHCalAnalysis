#ifndef HstSlwMonAdm1025_HH
#define HstSlwMonAdm1025_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphErrors.h"
#include "TPostScript.h"

#include "UtlAverage.hh"
#include "RcdRecord.hh"
#include "EmcCercFeEventData.hh"
#include "HodData.hh"
#include "HodTrack.hh"
#include "DaqEvent.hh"
#include "SubExtracter.hh"
#include "HstBase.hh"
#include "HstTGraph.hh"


class HstSlwMonAdm1025 : public HstBase {

public:
  HstSlwMonAdm1025(CercLocationData d, bool i=true) : 
    HstBase(i), _cld(d), _firstRecordTime(0) {

    _canvas[0]=new TCanvas("Lm82Canvas[0]","Lm82Canvas[0]",400, 20,600,400);
    _canvas[1]=new TCanvas("Lm82Canvas[1]","Lm82Canvas[1]",420, 40,600,400);
    _canvas[2]=new TCanvas("Lm82Canvas[2]","Lm82Canvas[2]",400, 60,600,400);
    _canvas[3]=new TCanvas("Lm82Canvas[3]","Lm82Canvas[3]",380, 80,600,400);
    _canvas[4]=new TCanvas("Lm82Canvas[4]","Lm82Canvas[4]",360,100,600,400);
    _canvas[5]=new TCanvas("Lm82Canvas[5]","Lm82Canvas[5]",340,120,600,400);
    _canvas[6]=new TCanvas("Lm82Canvas[6]","Lm82Canvas[6]",320,140,600,400);
    _canvas[7]=new TCanvas("Lm82Canvas[7]","Lm82Canvas[7]",300,160,600,400);
    _canvas[8]=new TCanvas("Lm82Canvas[8]","Lm82Canvas[8]",280,180,600,400);

  }

  virtual ~HstSlwMonAdm1025() {
  }

  bool record(const RcdRecord &r) {
    if(r.recordType()!=SubHeader::startUp &&
       r.recordType()!=SubHeader::slowControl) return true;

    if(_firstRecordTime==0) {
      _firstRecordTime=r.recordTime().seconds();

      for(unsigned i(0);i<9;i++) {
	ostringstream sout;
	sout << "Crate " << (unsigned)_cld.crateNumber() << ", Slot " 
	     << (unsigned)_cld.slotNumber() << ", CERC " 
	     << _cld.cercComponentName() << " ADM1025A ";
	
	if(i==0) sout << "Local Temperature";
	if(i==1) sout << "Remote Temperature";
	if(i==2) sout << " 1.8V Voltage";
	if(i==3) sout << " Vccp Voltage";
	if(i==4) sout << " 3.3V Voltage";
	if(i==5) sout << " 5.0V Voltage";
	if(i==6) sout << " 12.0V Voltage";
	if(i==7) sout << " Vcc Voltage";
	if(i==8) sout << "Status";

	sout << " vs Time (Days) since " << r.recordTime();

	_graph[i].SetTitle(sout.str().c_str());
	_graph[i].SetMarkerColor(1);
	_graph[i].SetMarkerStyle(20);
      }
    }

    double deltat((r.recordTime().seconds()-_firstRecordTime)/(24.0*3600.0));

    SubExtracter extracter(r);

    if(r.recordType()==SubHeader::startUp) {
      std::vector<const SlwCercAdm1025StartupData*>
        d(extracter.extract<SlwCercAdm1025StartupData>());
      assert(d.size()==1);

      if(printLevel()>0) d[0]->print(std::cout);
      
      return true;
    }

    std::vector<const SlwCercAdm1025SlowControlsData*>
      c(extracter.extract<SlwCercAdm1025SlowControlsData>());
    assert(c.size()==1);

    if(printLevel()>2) c[0]->print(std::cout);
      
    _graph[0].AddPoint(deltat,c[0]->localTemperature());
    _graph[1].AddPoint(deltat,c[0]->remoteTemperature());
    _graph[8].AddPoint(deltat,c[0]->status());

    SlwCercAdm1025Voltages v(c[0]->voltages());
    _graph[2].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::v25));
    _graph[3].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::vccp));
    _graph[4].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::v33));
    _graph[5].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::v50));
    _graph[6].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::v120));
    _graph[7].AddPoint(deltat,v.dVoltage(SlwCercAdm1025Voltages::vcc));

    return true;
  }

  bool update() {
    for(unsigned i(0);i<9;i++) {
      _canvas[i]->Clear();
      _graph[i].Draw("AP");
      _canvas[i]->Update();
    }

    return true;
  }

  bool postscript(std::string file) {
    std::cout << "HstSlwMonAdm1025::postscript()  Writing to file "
              << file << std::endl;

    TPostScript ps(file.c_str(),112);
    update();
    ps.Close();

    return true;
  }

private:
  CercLocationData _cld;
  unsigned _firstRecordTime;
  TCanvas *_canvas[9];
  HstTGraph _graph[9];
};

#endif
