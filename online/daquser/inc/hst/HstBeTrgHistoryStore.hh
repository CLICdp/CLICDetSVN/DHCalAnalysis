#ifndef HstBeTrgHistoryStore_HH
#define HstBeTrgHistoryStore_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstArray.hh"


class HstBeTrgHistoryStore : public HstFlags {

public:
  enum {
    shmKey=0x7654aaba
  };

  HstBeTrgHistoryStore() {
    reset();
  }

  void reset() {
    _size.reset();
    _errors.reset();
    for(unsigned i(0);i<32;i++) _history[i].reset();
    for(unsigned i(0);i<RcdHeader::endOfRecordTypeEnum;i++) _timers[i].reset();
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "HstBeTrgHistoryStore::print()" << std::endl;

    HstFlags::print(o,s+" ");
    /*
    for(unsigned i(0);i<10;i++) {
      o << s << "Rate for bin " << i << " = "
	<< _rate[1][RcdHeader::event][i] << std::endl;
    }
    */
    return o;
  }

  HstArray<10> _size;
  HstArray<10> _errors;
  HstArray<253> _history[32];

private:
};

#endif
