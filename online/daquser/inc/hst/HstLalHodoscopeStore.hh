#ifndef HstLalHodoscopeStore_HH
#define HstLalHodoscopeStore_HH

#include <iostream>

#include "HstFlags.hh"


class HstLalHodoscopeStore : public HstFlags {

public:
  enum {
    shmKey=0x7654addd
  };

  HstLalHodoscopeStore() {
    reset();
  }

  void reset() {
    for(unsigned xy(0);xy<2;xy++) {
      _digitalHits[xy].reset();
      for(unsigned ch(0);ch<128;ch++) {
	_analogueHits[xy][ch].reset();
      }
    }
  }

  std::ostream& print(std::ostream &o, std::string s) const {
    o << s << "HstLalHodoscopeStore::print()" << std::endl;

    HstFlags::print(o,s+" ");

    return o;
  }

  HstArray<128> _digitalHits[2];
  HstAverage _analogueHits[2][128];

private:
};

#endif
