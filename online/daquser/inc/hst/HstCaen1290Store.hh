#ifndef HstCaen1290Store_HH
#define HstCaen1290Store_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstArray.hh"


class HstCaen1290Store : public HstFlags {

public:
  enum {
    shmKey=0x7654aaa9,
    maximumNumberOfTdcs=2,
    maximumNumberOfChannels=16
  };

  enum Errors {
    header,
    eob
  };

  HstCaen1290Store() {
    reset();
  }

  void reset() {
    _size.reset();
    for(unsigned i(0);i<maximumNumberOfTdcs;i++) {
      _location[i]=BmlLocation(0,0,0);
      _sizePerTdc[i].reset();
      _labels[i][0].reset();
      _labels[i][1].reset();
      _labels[i][2].reset();
      _channels[i].reset();
      _errors[i].reset();
      _numberOfWords[i].reset();

      for(unsigned j(0);j<maximumNumberOfChannels;j++) {
	_numberOfWordsPerChannel[i][j][0].reset();
	_numberOfWordsPerChannel[i][j][1].reset();
	_tdcValues[i][j][0].reset();
	_tdcValues[i][j][1].reset();
      }
    }

    for(unsigned i(0);i<RcdHeader::endOfRecordTypeEnum;i++) {
      _processTime[i].reset();
    }    
  }

  std::ostream& print(std::ostream &o, std::string s) const {
    o << s << "HstCaen1290Store::print()" << std::endl;

    HstFlags::print(o,s+" ");
    _size.print(o,s+" ");

    return o;
  }

  unsigned _numberOfTdcs;
  BmlLocation _location[maximumNumberOfTdcs];

  HstArray<10> _size;
  HstArray<maximumNumberOfTdcs+1> _tdcs;

  HstArray<10> _sizePerTdc[maximumNumberOfTdcs];
  HstArray<10> _labels[maximumNumberOfTdcs][3];
  HstArray<100> _channels[maximumNumberOfTdcs];
  HstArray<10> _errors[maximumNumberOfTdcs];
  HstArray<100> _numberOfWords[maximumNumberOfTdcs];

  HstArray<100> _numberOfWordsPerChannel[maximumNumberOfTdcs][maximumNumberOfChannels][3];
  HstArray<4000> _tdcValues[maximumNumberOfTdcs][maximumNumberOfChannels][2];
  //HstArray<16000> _tdcValues[maximumNumberOfTdcs][maximumNumberOfChannels][2];

  HstArray<10000> _processTime[RcdHeader::endOfRecordTypeEnum];

private:
};

#endif
