#ifndef HstRecordStore_HH
#define HstRecordStore_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstArray.hh"
#include "HstArray2.hh"


class HstRecordStore : public HstFlags {

public:
  enum {
    shmKey=0x7654aaae
  };

  HstRecordStore() {
    reset();
  }

  void reset() {
    reset(0);
    reset(1);
    reset(2);
    reset(3);
    reset(4);
  }

  void reset(unsigned r) {
    assert(r<5);

    if(r<4) {
      if(r==0) _rateRange[0]=1000.0;
      if(r==1) _rateRange[1]=100.0;
      if(r==2) _rateRange[2]=10.0;
      if(r==3) _rateRange[3]=1.0;
      
      memset(&(_rate[r][0][0]),0,RcdHeader::endOfRecordTypeEnum*1000*sizeof(unsigned));
      memset(&(_data[r][0][0]),0,RcdHeader::endOfRecordTypeEnum*1000*sizeof(unsigned));

    } else {
      //_size.reset();
      //_time.reset();
    }
  }

  void rescale(unsigned r) {
    assert(r<4);
    for(unsigned i(0);i<RcdHeader::endOfRecordTypeEnum;i++) {
      for(unsigned j(0);j<500;j++) {
	_rate[r][i][j]=_rate[r][i][2*j]+_rate[r][i][2*j+1];
	_data[r][i][j]=_data[r][i][2*j]+_data[r][i][2*j+1];
      }
      for(unsigned j(500);j<1000;j++) {
	_rate[r][i][j]=0;
	_data[r][i][j]=0;
      }
    }
    _rateRange[r]*=2.0;
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "HstRecordStore::print()" << std::endl;

    HstFlags::print(o,s+" ");

    for(unsigned i(0);i<100;i++) {
      o << s << " X bin 5, Y bin " << i << " = "
	<< _time.contents(5,i) << std::endl;
    }

    return o;
  }

  UtlTime _firstTime[4];
  double _rateRange[4];
  unsigned _rate[4][RcdHeader::endOfRecordTypeEnum][1000];
  unsigned _data[4][RcdHeader::endOfRecordTypeEnum][1000];
  //  unsigned _time[RcdHeader::endOfRecordTypeEnum][10000];

  HstArray2<200,RcdHeader::endOfRecordTypeEnum> _size;
  HstArray2<100,RcdHeader::endOfRecordTypeEnum> _time;

private:
};

#endif
