#ifdef HST_MAP
#include "HstCrcChannelMap.hh"
typedef HstCrcChannelMap HstCrcChannel;

#else
#include "HstCrcChannelShm.hh"
typedef HstCrcChannelShm HstCrcChannel;
#endif
