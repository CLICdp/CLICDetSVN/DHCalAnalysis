#ifdef HST_MAP
#include "HstMpsBunchTrainMap.hh"
typedef HstMpsBunchTrainMap HstMpsBunchTrain;

#else
#include "HstMpsBunchTrainShm.hh"
typedef HstMpsBunchTrainShm HstMpsBunchTrain;
#endif
