#ifndef HstCrcNoiseStore_HH
#define HstCrcNoiseStore_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstAverage.hh"


class HstCrcNoiseStore : public HstFlags {

public:
  enum {
    shmKey=0x7654aaaa
  };

  HstCrcNoiseStore() {
    _crcs[0].word(0xffffffff);
    _crcs[1].word(0xffffffff);
    _crcs[2].word(0xffffffff);
    reset();
  }

  void reset() {
    for(unsigned c(0);c<3;c++) {
      for(unsigned s(0);s<22;s++) {
	_unverified[c][s]=0;

	if(_crcs[c].bit(s)) {
	  for(unsigned f(0);f<8;f++) {
	    for(unsigned a(0);a<12;a++) {
	      if(c<2) {
		for(unsigned m(0);m<19;m++) {
		  _average[c][s][f][a][m].reset();
		}
	      } else {
		for(unsigned m(0);m<65;m++) {
		  _average64[s][f][a][m].reset();
		}
	      }
	    }

	    _mean[c][s][f].reset();
	    _rms[c][s][f].reset();
	  }
	}
      }
    }
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "HstCrcNoiseStore::print()" << std::endl;

    HstFlags::print(o,s+" ");

    o << s << " ECAL   bits set to " << printHex(_crcs[0]) << std::endl;
    o << s << " HCAL   bits set to " << printHex(_crcs[1]) << std::endl;
    o << s << " DHECAL bits set to " << printHex(_crcs[2]) << std::endl;

    for(unsigned i(0);i<3;i++) {
      for(unsigned j(0);j<22;j++) {
	if(_crcs[i].bit(j)) {
	  o << s << " Crate " << i << ", slot " << std::setw(2) << j
	    << ", unverified events = " << _unverified[i][j] << std::endl;
	}
      }
    }
 
    return o;
  }

  UtlPack _crcs[3];
  unsigned _unverified[3][22];
  HstAverage _average[2][22][8][12][19];
  HstAverage _average64[22][8][12][65];
  HstArray<100> _mean[3][22][8];
  HstArray<100> _rms[3][22][8];

private:
};

#endif
