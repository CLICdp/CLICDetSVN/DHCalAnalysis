#ifdef HST_MAP
#include "HstCrcNoiseMap.hh"
typedef HstCrcNoiseMap HstCrcNoise;

#else
#include "HstCrcNoiseShm.hh"
typedef HstCrcNoiseShm HstCrcNoise;
#endif
