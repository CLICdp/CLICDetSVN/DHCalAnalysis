#ifndef HstLc1176Store_HH
#define HstLc1176Store_HH

#include <iostream>

#include "HstFlags.hh"


class HstLc1176Store : public HstFlags {

public:
  enum {
    shmKey=0x7654aaac
  };

  HstLc1176Store() {
    reset();
  }

  void reset() {
    memset(_size,0,10*sizeof(unsigned));
    memset(_numberOfWords,0,260*sizeof(unsigned));
    memset(_bufferNumber,0,32*sizeof(unsigned));
    memset(_numberOfWordsPerChannel,0,16*2*20*sizeof(unsigned));
    memset(_tdc,0,16*2*10000*sizeof(unsigned));
  }

  std::ostream& print(std::ostream &o, std::string s) const {
    o << s << "HstLc1176Store::print()" << std::endl;

    HstFlags::print(o,s+" ");

    return o;
  }

  unsigned _size[10];
  unsigned _numberOfWords[260];
  unsigned _bufferNumber[32];
  unsigned _numberOfWordsPerChannel[16][2][20];
  unsigned _tdc[16][2][10000];

private:
};

#endif
