#ifdef HST_MAP
#include "HstRecordMap.hh"
typedef HstRecordMap HstRecord;

#else
#include "HstRecordShm.hh"
typedef HstRecordShm HstRecord;
#endif
