#ifndef HstPut_HH
#define HstPut_HH

#include <cassert>

//#include "TROOT.h"
#include "TMapFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TRandom.h"

#include "DaqRunStart.hh"

#include "RcdUserRO.hh"
#include "RcdWriterDmy.hh"
#include "RcdWriterAsc.hh"
#include "RcdWriterBin.hh"

#include "SubAccessor.hh"


class HstPut : public RcdUserRO {

public:
  HstPut() :
    hpsize("hpsize","This is the nWords distribution",100,0,10000),
    hptime("hptime","This is the delta-t distribution",100,0,1200) {
    hpx    = new TH1F("hpx","This is the px distribution",100,-4,4);
   hpxpy  = new TH2F("hpxpy","py vs px",40,-4,4,40,-4,4);
   hprof  = new TProfile("hprof","Profile of pz versus px",100,-4,4,0,20);
                                                                                
   // Set a fill color for the TH1F
   hpx->SetFillColor(48);
                                                                                
  }

  virtual ~HstPut() {
  }

  void update() {
    hpsize.Draw();
  }

  bool record(const RcdRecord &r) {
    //r.RcdHeader::print(std::cout);
    if(_start.seconds()==0) _start=r.recordTime();



    hpsize.Fill(r.numberOfWords());
    hptime.Fill((r.recordTime()-_start).deltaTime());

    Float_t px, py, pz;

    gRandom->Rannor(px,py);
    pz = px*px + py*py;
    hpx->Fill(px);
    hpxpy->Fill(px,py);
    hprof->Fill(px,pz);
    
    return true;
  }

private:
  TH1F hpsize;
  TH1F hptime;
  TH1F *hpx;
  TH2F *hpxpy;
  TProfile *hprof;
  UtlTime _start;
};

#endif
