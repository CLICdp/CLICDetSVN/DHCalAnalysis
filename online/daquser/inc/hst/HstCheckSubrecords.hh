#ifndef HstCheckSubrecords_HH
#define HstCheckSubrecords_HH

#include <cassert>

//#include "TROOT.h"
#include "TMapFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TRandom.h"

#include "DaqRunStart.hh"

#include "RcdUserRO.hh"
#include "RcdWriterDmy.hh"
#include "RcdWriterAsc.hh"
#include "RcdWriterBin.hh"

#include "UtlAverage.hh"
#include "SubAccessor.hh"


class HstCheckSubrecords : public RcdUserRO {

public:
  HstCheckSubrecords() {
    _subrecords[0]=new TH1F("HstCheckSubrecords0",
			    "Total number of subrecords",
			    0x900,0,0x900);
    _subrecords[1]=new TH1F("HstCheckSubrecords1",
			    "Total number of verification checks",
			    0x900,0,0x900);
    _subrecords[2]=new TH1F("HstCheckSubrecords2",
			    "Total number of verification errors",
			    0x900,0,0x900);
  }

  virtual ~HstCheckSubrecords() {
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "HstCheckSubrecords::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    SubAccessor accessor(r);

    std::vector<const SubHeader*> v(accessor.access());
    for(unsigned i(0);i<v.size();i++) {
      _subrecords[0]->Fill((v[i]->subRecordType())>>4);
    }

    {std::vector<const CrcLocationData<CrcVlinkEventData>*>
        v(accessor.access< CrcLocationData<CrcVlinkEventData> >());
    for(unsigned i(0);i<v.size();i++) {
      _subrecords[1]->Fill((subRecordType< CrcLocationData<CrcVlinkEventData> >())>>4); 
      if(!v[i]->verify() || !v[i]->data()->verify()) {
	_subrecords[2]->Fill((subRecordType< CrcLocationData<CrcVlinkEventData> >())>>4); 
	//v[i]->print(std::cout,"NOT VERIFIED ") << std::endl;
      }
    }}
    
    return true;
  }

private:
  TH1F *_subrecords[3];
};

#endif
