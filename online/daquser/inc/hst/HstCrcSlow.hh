#ifndef HstCrcSlow_HH
#define HstCrcSlow_HH

#include <cassert>

//#include "TROOT.h"
#include "TMapFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"
#include "TRandom.h"

#include "CrcAdm1025SlowReadoutData.hh"

#include "RcdUserRO.hh"
#include "RcdWriterDmy.hh"
#include "RcdWriterAsc.hh"
#include "RcdWriterBin.hh"

#include "UtlAverage.hh"
#include "SubAccessor.hh"


class HstCrcSlow : public RcdUserRO {

public:
  HstCrcSlow() {
    for(unsigned i(0);i<=21;i++) {
      for(unsigned j(0);j<6;j++) {
	_voltage[i][j]=0;
      }
    }
  }

  virtual ~HstCrcSlow() {
    for(unsigned i(0);i<=21;i++) {
      for(unsigned j(0);j<6;j++) {
	if(_voltage[i][j]!=0) delete _voltage[i][j];
	_voltage[i][j]=0;
      }
    }
    _firstRun=true;
  }

  /*
  void update() {
    _pedestal.Draw();
    _noise.Draw();
  }
  */

  bool record(const RcdRecord &r) {

    if(r.recordType()==RcdHeader::runStart && _firstRun) {
      _firstRun=false;

      SubAccessor accessor(r);
      std::vector<const CrcLocationData<CrcVmeRunData>* >
	v(accessor.access< CrcLocationData<CrcVmeRunData> >());
      
      CrcAdm1025Voltages volts;
      volts.maximum();
      
      for(unsigned i(0);i<v.size();i++) {
	if(v[i]->slotNumber()<22) {
	  unsigned slot(v[i]->slotNumber());

	  {
	  std::ostringstream mLabel;
	  mLabel << "HstCrcSlowNumber";
	  if(slot<10) mLabel << "0";
	  mLabel << slot;
	  
	  std::ostringstream mTitle;
	  mTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 2.5V";
	  _voltage[slot][0]=new 
	    TH1F(mLabel.str().c_str(),mTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::v25));

	  std::ostringstream pLabel;
	  pLabel << "HstCrcSlowPedestal";
	  if(slot<10) pLabel << "0";
	  pLabel << slot;
	  
	  std::ostringstream pTitle;
	  pTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 Vccp";
	  _voltage[slot][1]=new 
	    TH1F(pLabel.str().c_str(),pTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::vccp));

	  std::ostringstream nLabel;
	  nLabel << "HstCrcSlowNoise";
	  if(slot<10) nLabel << "0";
	  nLabel << slot;
	  
	  std::ostringstream nTitle;
	  nTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 3.3V";
	  _voltage[slot][2]=new 
	    TH1F(nLabel.str().c_str(),nTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::v33));
	  }
	  {
	  std::ostringstream mLabel;
	  mLabel << "HstCrcSlowNumber";
	  if(slot<10) mLabel << "0";
	  mLabel << slot;
	  
	  std::ostringstream mTitle;
	  mTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 5.0V";
	  _voltage[slot][3]=new 
	    TH1F(mLabel.str().c_str(),mTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::v50));

	  std::ostringstream pLabel;
	  pLabel << "HstCrcSlowPedestal";
	  if(slot<10) pLabel << "0";
	  pLabel << slot;
	  
	  std::ostringstream pTitle;
	  pTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 12.0V";
	  _voltage[slot][4]=new 
	    TH1F(pLabel.str().c_str(),pTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::v120));

	  std::ostringstream nLabel;
	  nLabel << "HstCrcSlowNoise";
	  if(slot<10) nLabel << "0";
	  nLabel << slot;
	  
	  std::ostringstream nTitle;
	  nTitle << "Crate " << std::hex << std::setw(2)
		 << (unsigned)v[i]->crateNumber()
		 << std::dec << " Slot " << std::setw(2) << slot
		 << " ADM1025 Vcc";
	  _voltage[slot][5]=new 
	    TH1F(nLabel.str().c_str(),nTitle.str().c_str(),
		 128,0,volts.dVoltage(CrcAdm1025Voltages::vcc));
	  }
	}
      }

      return true;
    }

    if(r.recordType()==RcdHeader::slowReadout) {
      SubAccessor accessor(r);
      std::vector<const CrcLocationData<CrcAdm1025SlowReadoutData>* >
	v(accessor.access< CrcLocationData<CrcAdm1025SlowReadoutData> >());

      for(unsigned i(0);i<v.size();i++) {
	if(v[i]->slotNumber()<22) {
	  unsigned slot(v[i]->slotNumber());
	  if(_voltage[slot][0]!=0 && _voltage[slot][1]!=0 &&
	     _voltage[slot][2]!=0 && _voltage[slot][3]!=0 && 
	     _voltage[slot][4]!=0 && _voltage[slot][5]!=0) {

	    CrcAdm1025Voltages volts(v[i]->data()->voltages());
	    for(unsigned j(0);j<6;j++) {
	      voltage[slot][j]->Fill(volts.dVoltage((CrcAdm1025Voltages::Voltage)j));
	    }
	  }
	}
      }
    }

    return true;
  }

private:
  bool _firstRun;
  TH1F *_voltage[22][6];
};

#endif
