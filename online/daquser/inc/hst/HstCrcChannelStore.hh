#ifndef HstCrcChannelStore_HH
#define HstCrcChannelStore_HH

#include <iostream>

#include "HstFlags.hh"
#include "HstAverage.hh"


class HstCrcChannelStore : public HstFlags {

public:
  enum {
    shmKey=0x7654aaab
  };

  HstCrcChannelStore() {
    _crate=0;
    _slot=0;
    _fe=0;
    _chip=0;
    _chan=0;
    reset();
  }

  void reset() {
    for(unsigned c(0);c<65536;c++) {
      _adc[c]=0;
      _bad[c]=0;
    }
  }

  std::ostream& print(std::ostream &o, std::string s) const {
    o << s << "HstCrcChannelStore::print()" << std::endl;

    HstFlags::print(o,s+" ");

    return o;
  }

  unsigned _crate,_slot,_fe,_chip,_chan;
  unsigned _adc[65536],_bad[65536];

private:
};

#endif
