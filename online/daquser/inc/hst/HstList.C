#include "TMapFile.h"

void HstList() {
   // Open the memory mapped file "hsimple.map" in "READ" (default) mode.
   TMapFile *mfile = TMapFile::Create("HstGeneric.map");

   // Print status of mapped file and list its contents
   mfile->Print();
   mfile->ls();
}
