#ifndef HstRecordMap_HH
#define HstRecordMap_HH

#include <cassert>

#include "TMapFile.h"
#include "TH1F.h"
#include "TH2D.h"

#include "RcdUserRO.hh"


#define HST_RATE

class HstRecordMap : public RcdUserRO {

public:
  class HstRate : public TH1F {

  public:
    HstRate(const char* label, const char* title, int bins, double hi) :
      TH1F(label,title,bins,0.0,hi), _originalHi(hi), _bins(bins) {
      _currentHi=_originalHi;
      _weight=_bins/_originalHi;
    }
    
    Int_t Fill(double t, double w=1.0) {

      // Check if overflow
      if(t>_currentHi) {
	for(int i(0);i<GetNbinsX();i++) {
	  if(i<(GetNbinsX()+1)/2) {
	    SetBinContent(i+1,0.5*(GetBinContent(2*i+1)+GetBinContent(2*i+2)));
	  } else {                   
	    SetBinContent(i+1,0.0);
	  }
	}

	// Set new upper edge
	_currentHi*=2.0;
	_weight/=2.0;
	GetXaxis()->SetLimits(0.0,_currentHi);
      }
	
      // Now fill
      TH1F::Fill(t,_weight*w);
      return 0;
    }
    
    void Reset() {
      TH1F::Reset();
      GetXaxis()->SetLimits(0.0,_originalHi);
      _currentHi=_originalHi;
      _weight=_bins/_originalHi;
    }
    
  private:
    const double _originalHi;
    const int _bins;
    double _currentHi;
    double _weight;
  };
  
public:
  HstRecordMap() {
    _lab[0]="Job";
    _lab[1]="Run";
    _lab[2]="Cfg";
    _lab[3]="Acq";
    _tit[0]="job";
    _tit[1]="run";
    _tit[2]="configuration";
    _tit[3]="acquisition";

    unsigned maxKBytes(sizeof(RcdArena)/1024);
    _hstRecordJobRSize=new TH1F("HstRecordJobRSize",
				"Record size in job (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordRunRSize=new TH1F("HstRecordRunRSize",
				"Record size in run (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordCfgRSize=new TH1F("HstRecordCfgRSize",
				"Record size in configuration (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordAcqRSize=new TH1F("HstRecordAcqRSize",
				"Record size in acquisition (kBytes)",
				maxKBytes,0,maxKBytes);
 
    _hstRecordJobTSize=new TH1F("HstRecordJobTSize",
				"Trigger size in job (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordRunTSize=new TH1F("HstRecordRunTSize",
				"Trigger size in run (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordCfgTSize=new TH1F("HstRecordCfgTSize",
				"Trigger size in configuration (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordAcqTSize=new TH1F("HstRecordAcqTSize",
				"Trigger size in acquisition (kBytes)",
				maxKBytes,0,maxKBytes);
    
    _hstRecordJobESize=new TH1F("HstRecordJobESize",
				"Event size in job (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordRunESize=new TH1F("HstRecordRunESize",
				"Event size in run (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordCfgESize=new TH1F("HstRecordCfgESize",
				"Event size in configuration (kBytes)",
				maxKBytes,0,maxKBytes);
    _hstRecordAcqESize=new TH1F("HstRecordAcqESize",
				"Event size in acquisition (kBytes)",
				maxKBytes,0,maxKBytes);
    
#ifndef HST_RATE
    _hstRecordJobRRate=new TH1F("HstRecordJobRRate",
				"Record rate (records/sec) in job vs time (sec)",
				100,0,2000);
    _hstRecordRunRRate=new TH1F("HstRecordRunRRate",
				"Record rate (records/sec) in run vs time (sec)",
				100,0,100);
    _hstRecordCfgRRate=new TH1F("HstRecordCfgRRate",
				"Record rate (records/sec) in configuration vs time (sec)",
				100,0,100);
    _hstRecordAcqRRate=new TH1F("HstRecordAcqRRate",
				"Record rate (records/sec) in acquisition vs time (sec)",
				100,0,100);

    _hstRecordJobTRate=new TH1F("HstRecordJobTRate",
				"Trigger+Event rate (events/sec) in job vs time (sec)",
				100,0,2000);
    _hstRecordRunTRate=new TH1F("HstRecordRunTRate",
				"Trigger+Event rate (events/sec) in run vs time (sec)",
				100,0,100);
    _hstRecordCfgTRate=new TH1F("HstRecordCfgTRate",
				"Trigger+Event rate (events/sec) in configuration vs time (sec)",
				100,0,100);
    _hstRecordAcqTRate=new TH1F("HstRecordAcqTRate",
				"Trigger+Event rate (events/sec) in acquisition vs time (sec)",
				100,0,100);

    _hstRecordJobERate=new TH1F("HstRecordJobERate",
				"Event rate (events/sec) in job vs time (sec)",
				100,0,2000);
    _hstRecordRunERate=new TH1F("HstRecordRunERate",
				"Event rate (events/sec) in run vs time (sec)",
				100,0,100);
    _hstRecordCfgERate=new TH1F("HstRecordCfgERate",
				"Event rate (events/sec) in configuration vs time (sec)",
				100,0,100);
    _hstRecordAcqERate=new TH1F("HstRecordAcqERate",
				"Event rate (events/sec) in acquisition vs time (sec)",
				100,0,100);

    _hstRecordJobDRate=new TH1F("HstRecordJobDRate",
				"Data rate (MBytes/sec) in job vs time (sec)",
				100,0,2000);
    _hstRecordRunDRate=new TH1F("HstRecordRunDRate",
				"Data rate (MBytes/sec) in run vs time (sec)",
				100,0,100);
    _hstRecordCfgDRate=new TH1F("HstRecordCfgDRate",
				"Data rate (MBytes/sec) in configuration vs time (sec)",
				100,0,100);
    _hstRecordAcqDRate=new TH1F("HstRecordAcqDRate",
				"Data rate (MBytes/sec) in acquisition vs time (sec)",
				100,0,100);
#else
    double jobHi(60.0*60.0);
    double runHi(15.0*60.0);
    double cfgHi( 5.0*60.0);
    double acqHi(      2.0);

    _hstRecordJobRRate=new HstRate("HstRecordJobRRate",
				"Record rate (records/sec) in job vs time (sec)",
				200,jobHi);
    _hstRecordRunRRate=new HstRate("HstRecordRunRRate",
				"Record rate (records/sec) in run vs time (sec)",
				200,runHi);
    _hstRecordCfgRRate=new HstRate("HstRecordCfgRRate",
				"Record rate (records/sec) in configuration vs time (sec)",
				200,cfgHi);
    _hstRecordAcqRRate=new HstRate("HstRecordAcqRRate",
				"Record rate (records/sec) in acquisition vs time (sec)",
				200,acqHi);

    _hstRecordJobTRate=new HstRate("HstRecordJobTRate",
				"Trigger+Event rate (events/sec) in job vs time (sec)",
				200,jobHi);
    _hstRecordRunTRate=new HstRate("HstRecordRunTRate",
				"Trigger+Event rate (events/sec) in run vs time (sec)",
				200,runHi);
    _hstRecordCfgTRate=new HstRate("HstRecordCfgTRate",
				"Trigger+Event rate (events/sec) in configuration vs time (sec)",
				200,cfgHi);
    _hstRecordAcqTRate=new HstRate("HstRecordAcqTRate",
				"Trigger+Event rate (events/sec) in acquisition vs time (sec)",
				200,acqHi);

    _hstRecordJobERate=new HstRate("HstRecordJobERate",
				"Event rate (events/sec) in job vs time (sec)",
				200,jobHi);
    _hstRecordRunERate=new HstRate("HstRecordRunERate",
				"Event rate (events/sec) in run vs time (sec)",
				200,runHi);
    _hstRecordCfgERate=new HstRate("HstRecordCfgERate",
				"Event rate (events/sec) in configuration vs time (sec)",
				200,cfgHi);
    _hstRecordAcqERate=new HstRate("HstRecordAcqERate",
				"Event rate (events/sec) in acquisition vs time (sec)",
				200,acqHi);

    _hstRecordJobDRate=new HstRate("HstRecordJobDRate",
				"Data rate (MBytes/sec) in job vs time (sec)",
				200,jobHi);
    _hstRecordRunDRate=new HstRate("HstRecordRunDRate",
				"Data rate (MBytes/sec) in run vs time (sec)",
				200,runHi);
    _hstRecordCfgDRate=new HstRate("HstRecordCfgDRate",
				"Data rate (MBytes/sec) in configuration vs time (sec)",
				200,cfgHi);
    _hstRecordAcqDRate=new HstRate("HstRecordAcqDRate",
				"Data rate (MBytes/sec) in acquisition vs time (sec)",
				200,acqHi);
#endif

    _lastHeader.recordType(RcdHeader::shutDown);
    for(unsigned i(0);i<4;i++) {
      _hstRecordTime[0][i]=new TH2D((std::string("HstRecordShortTime")+_lab[i]).c_str(),
				    (std::string("Processing time in ")+_tit[i]).c_str(),
				    100,0.0,0.01,
				    RcdHeader::endOfRecordTypeEnum,0,RcdHeader::endOfRecordTypeEnum);
      _hstRecordTime[1][i]=new TH2D((std::string("HstRecordLongTime")+_lab[i]).c_str(),
				    (std::string("Processing time in ")+_tit[i]).c_str(),
				    100,0.0,1.0,
				    RcdHeader::endOfRecordTypeEnum,0,RcdHeader::endOfRecordTypeEnum);
    }
  }

  virtual ~HstRecordMap() {
    delete _hstRecordJobRSize;
    delete _hstRecordRunRSize;
    delete _hstRecordCfgRSize;
    delete _hstRecordAcqRSize;
    
    delete _hstRecordJobESize;
    delete _hstRecordRunESize;
    delete _hstRecordCfgESize;
    delete _hstRecordAcqESize;
    
    delete _hstRecordJobRRate;
    delete _hstRecordRunRRate;
    delete _hstRecordCfgRRate;
    delete _hstRecordAcqRRate;
    
    delete _hstRecordJobTSize;
    delete _hstRecordRunTSize;
    delete _hstRecordCfgTSize;
    delete _hstRecordAcqTSize;
    
    delete _hstRecordJobERate;
    delete _hstRecordRunERate;
    delete _hstRecordCfgERate;
    delete _hstRecordAcqERate;
    
    delete _hstRecordJobDRate;
    delete _hstRecordRunDRate;
    delete _hstRecordCfgDRate;
    delete _hstRecordAcqDRate;
    
    delete _hstRecordJobTRate;
    delete _hstRecordRunTRate;
    delete _hstRecordCfgTRate;
    delete _hstRecordAcqTRate;
    
    for(unsigned i(0);i<4;i++) {
      delete _hstRecordTime[0][i];
      delete _hstRecordTime[1][i];
    }
  }

  void extend(TH1F *hst, unsigned extraBins) {
    double *c(new double[hst->GetNbinsX()]);

    for(int i(0);i<hst->GetNbinsX();i++) {
      c[i]=hst->GetBinContent(i+1);
    }

    const TAxis *axis(hst->GetXaxis());

    /*
    std::cout << "Extending from " << hst->GetNbinsX() << " bins, x range "
	      << axis->GetXmin() << " - " << axis->GetXmax() << " to " 
	      << hst->GetNbinsX()+extraBins << " bins, x range "
	      << axis->GetXmin() << " - "
	      << axis->GetXmax()+extraBins*hst->GetBinWidth(1) << std::endl;
    */

    int oldBins(hst->GetNbinsX());
    hst->SetBins(hst->GetNbinsX()+extraBins,
		 axis->GetXmin(),
		 axis->GetXmax()+extraBins*hst->GetBinWidth(1));

    for(int i(0);i<oldBins;i++) {
      hst->SetBinContent(i+1,c[i]);
    }

    delete [] c;
  }

  bool record(const RcdRecord &r) {

    if(_startJob.seconds()==0) {
      _startJob=r.recordTime();

      {std::ostringstream sout;
      sout << "Record rate (records/sec) in job vs time (sec) since "
	   << r.recordTime();
      _hstRecordJobRRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Trigger+Event rate (events/sec) in job vs time (sec) since "
	   << r.recordTime();
      _hstRecordJobTRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Event rate (events/sec) in job vs time (sec) since "
	   << r.recordTime();
      _hstRecordJobERate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Data rate (MBytes/sec) in job vs time (sec) since "
	   << r.recordTime();
      _hstRecordJobDRate->SetTitle(sout.str().c_str());}

      _startRun=r.recordTime();
      _startCfg=r.recordTime();
      _startAcq=r.recordTime();
    }

    if(r.recordType()==RcdHeader::runStart) {
      _startRun=r.recordTime();

      // Reset all run hists
      _hstRecordRunRSize->Reset();
      _hstRecordRunTSize->Reset();
      _hstRecordRunESize->Reset();

#ifndef HST_RATE
      _hstRecordRunRRate->SetBins(100,0,100);
      _hstRecordRunTRate->SetBins(100,0,100);
      _hstRecordRunERate->SetBins(100,0,100);
      _hstRecordRunDRate->SetBins(100,0,100);
#endif
      _hstRecordRunRRate->Reset();
      _hstRecordRunTRate->Reset();
      _hstRecordRunERate->Reset();
      _hstRecordRunDRate->Reset();

      _hstRecordTime[0][1]->Reset();
      _hstRecordTime[1][1]->Reset();

      {std::ostringstream sout;
      sout << "Record rate (records/sec) in run vs time (sec) since "
	   << r.recordTime();
      _hstRecordRunRRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Trigger+Event rate (events/sec) in run vs time (sec) since "
	   << r.recordTime();
      _hstRecordRunTRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Event rate (events/sec) in run vs time (sec) since "
	   << r.recordTime();
      _hstRecordRunERate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Data rate (MBytes/sec) in run vs time (sec) since "
	   << r.recordTime();
      _hstRecordRunDRate->SetTitle(sout.str().c_str());}
    }

    if(r.recordType()==RcdHeader::configurationStart) {
      _startCfg=r.recordTime();

      // Reset all configuration hists
      _hstRecordCfgRSize->Reset();
      _hstRecordCfgTSize->Reset();
      _hstRecordCfgESize->Reset();

#ifndef HST_RATE
      _hstRecordCfgRRate->SetBins(100,0,100);
      _hstRecordCfgTRate->SetBins(100,0,100);
      _hstRecordCfgERate->SetBins(100,0,100);
      _hstRecordCfgDRate->SetBins(100,0,100);
#endif
      _hstRecordCfgRRate->Reset();
      _hstRecordCfgTRate->Reset();
      _hstRecordCfgERate->Reset();
      _hstRecordCfgDRate->Reset();

      _hstRecordTime[0][2]->Reset();
      _hstRecordTime[1][2]->Reset();

      {std::ostringstream sout;
      sout << "Record rate (records/sec) in configuration vs time (sec) since "
	   << r.recordTime();
      _hstRecordCfgRRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Trigger+Event rate (events/sec) in configuration vs time (sec) since "
	   << r.recordTime();
      _hstRecordCfgTRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Event rate (events/sec) in configuration vs time (sec) since "
	   << r.recordTime();
      _hstRecordCfgERate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Data rate (MBytes/sec) in configuration vs time (sec) since "
	   << r.recordTime();
      _hstRecordCfgDRate->SetTitle(sout.str().c_str());}
    }

    if(r.recordType()==RcdHeader::acquisitionStart) {
      _startAcq=r.recordTime();

      // Reset all acquisition hists
      _hstRecordAcqRSize->Reset();
      _hstRecordAcqTSize->Reset();
      _hstRecordAcqESize->Reset();

#ifndef HST_RATE
      _hstRecordAcqRRate->SetBins(100,0,100);
      _hstRecordAcqTRate->SetBins(100,0,100);
      _hstRecordAcqERate->SetBins(100,0,100);
      _hstRecordAcqDRate->SetBins(100,0,100);
#endif
      _hstRecordAcqRRate->Reset();
      _hstRecordAcqTRate->Reset();
      _hstRecordAcqERate->Reset();
      _hstRecordAcqDRate->Reset();

      _hstRecordTime[0][3]->Reset();
      _hstRecordTime[1][3]->Reset();

      {std::ostringstream sout;
      sout << "Record rate (records/sec) in acquisition vs time (sec) since "
	   << r.recordTime();
      _hstRecordAcqRRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Trigger+Event rate (events/sec) in acquisition vs time (sec) since "
	   << r.recordTime();
      _hstRecordAcqTRate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Event rate (events/sec) in acquisition vs time (sec) since "
	   << r.recordTime();
      _hstRecordAcqERate->SetTitle(sout.str().c_str());}

      {std::ostringstream sout;
      sout << "Data rate (MBytes/sec) in acquisition vs time (sec) since "
	   << r.recordTime();
      _hstRecordAcqDRate->SetTitle(sout.str().c_str());}
    }

    _hstRecordJobRSize->Fill(r.totalNumberOfBytes()/1024.0);
    _hstRecordRunRSize->Fill(r.totalNumberOfBytes()/1024.0);
    _hstRecordCfgRSize->Fill(r.totalNumberOfBytes()/1024.0);
    _hstRecordAcqRSize->Fill(r.totalNumberOfBytes()/1024.0);

    double jobDt((r.recordTime()-_startJob).deltaTime());
    double runDt((r.recordTime()-_startRun).deltaTime());
    double cfgDt((r.recordTime()-_startCfg).deltaTime());
    double acqDt((r.recordTime()-_startAcq).deltaTime());

#ifndef HST_RATE
    const TAxis *jAxis(_hstRecordJobRRate->GetXaxis());
    while(jobDt>jAxis->GetXmax()) {
      extend(_hstRecordJobRRate,100);
      extend(_hstRecordJobDRate,100);
      extend(_hstRecordJobTRate,100);
      extend(_hstRecordJobERate,100);
    }

    const TAxis *rAxis(_hstRecordRunRRate->GetXaxis());
    while(runDt>rAxis->GetXmax()) {
      extend(_hstRecordRunRRate,100);
      extend(_hstRecordRunDRate,100);
      extend(_hstRecordRunTRate,100);
      extend(_hstRecordRunERate,100);
    }

    const TAxis *cAxis(_hstRecordCfgRRate->GetXaxis());
    while(cfgDt>cAxis->GetXmax()) {
      extend(_hstRecordCfgRRate,100);
      extend(_hstRecordCfgDRate,100);
      extend(_hstRecordCfgTRate,100);
      extend(_hstRecordCfgERate,100);
    }

    const TAxis *aAxis(_hstRecordAcqRRate->GetXaxis());
    while(acqDt>aAxis->GetXmax()) {
      extend(_hstRecordAcqRRate,100);
      extend(_hstRecordAcqDRate,100);
      extend(_hstRecordAcqTRate,100);
      extend(_hstRecordAcqERate,100);
    }
#endif

#ifndef HST_RATE
    _hstRecordJobRRate->Fill(jobDt,0.05);
    _hstRecordRunRRate->Fill(runDt,1.00);
    _hstRecordCfgRRate->Fill(cfgDt,1.00);
    _hstRecordAcqRRate->Fill(acqDt,1.00);

    _hstRecordJobDRate->Fill(jobDt,0.05*r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordRunDRate->Fill(runDt,1.00*r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordCfgDRate->Fill(cfgDt,1.00*r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordAcqDRate->Fill(acqDt,1.00*r.totalNumberOfBytes()/(1024.0*1024.0));
#else
    _hstRecordJobRRate->Fill(jobDt);
    _hstRecordRunRRate->Fill(runDt);
    _hstRecordCfgRRate->Fill(cfgDt);
    _hstRecordAcqRRate->Fill(acqDt);

    _hstRecordJobDRate->Fill(jobDt,r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordRunDRate->Fill(runDt,r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordCfgDRate->Fill(cfgDt,r.totalNumberOfBytes()/(1024.0*1024.0));
    _hstRecordAcqDRate->Fill(acqDt,r.totalNumberOfBytes()/(1024.0*1024.0));
#endif

    if(r.recordType()==RcdHeader::trigger) {
      _hstRecordJobTSize->Fill(r.numberOfWords()/256.0);
      _hstRecordRunTSize->Fill(r.numberOfWords()/256.0);
      _hstRecordCfgTSize->Fill(r.numberOfWords()/256.0);
      _hstRecordAcqTSize->Fill(r.numberOfWords()/256.0);
    }

    if(r.recordType()==RcdHeader::trigger || r.recordType()==RcdHeader::event) {
#ifndef HST_RATE
      _hstRecordJobTRate->Fill(jobDt,0.05);
      _hstRecordRunTRate->Fill(runDt,1.00);
      _hstRecordCfgTRate->Fill(cfgDt,1.00);
      _hstRecordAcqTRate->Fill(acqDt,1.00);
#else
      _hstRecordJobTRate->Fill(jobDt);
      _hstRecordRunTRate->Fill(runDt);
      _hstRecordCfgTRate->Fill(cfgDt);
      _hstRecordAcqTRate->Fill(acqDt);
#endif
    }
    
    if(r.recordType()==RcdHeader::event) {
      _hstRecordJobESize->Fill(r.numberOfWords()/256.0);
      _hstRecordRunESize->Fill(r.numberOfWords()/256.0);
      _hstRecordCfgESize->Fill(r.numberOfWords()/256.0);
      _hstRecordAcqESize->Fill(r.numberOfWords()/256.0);

#ifndef HST_RATE
      _hstRecordJobERate->Fill(jobDt,0.05);
      _hstRecordRunERate->Fill(runDt,1.00);
      _hstRecordCfgERate->Fill(cfgDt,1.00);
      _hstRecordAcqERate->Fill(acqDt,1.00);
#else
      _hstRecordJobERate->Fill(jobDt);
      _hstRecordRunERate->Fill(runDt);
      _hstRecordCfgERate->Fill(cfgDt);
      _hstRecordAcqERate->Fill(acqDt);
#endif
    }
    
    double dt((r.recordTime()-_lastHeader.recordTime()).deltaTime());
    for(unsigned i(0);i<4;i++) {
      _hstRecordTime[0][i]->Fill(dt,_lastHeader.recordType());
      _hstRecordTime[1][i]->Fill(dt,_lastHeader.recordType());
    }
    _lastHeader=r;

    return true;
  }

private:
  UtlTime _startJob;
  UtlTime _startRun;
  UtlTime _startCfg;
  UtlTime _startAcq;

  std::string _lab[4],_tit[4];

  TH1F *_hstRecordJobRSize;
  TH1F *_hstRecordRunRSize;
  TH1F *_hstRecordCfgRSize;
  TH1F *_hstRecordAcqRSize;

  TH1F *_hstRecordJobTSize;
  TH1F *_hstRecordRunTSize;
  TH1F *_hstRecordCfgTSize;
  TH1F *_hstRecordAcqTSize;

  TH1F *_hstRecordJobESize;
  TH1F *_hstRecordRunESize;
  TH1F *_hstRecordCfgESize;
  TH1F *_hstRecordAcqESize;

#ifndef HST_RATE
  TH1F *_hstRecordJobRRate;
  TH1F *_hstRecordRunRRate;
  TH1F *_hstRecordCfgRRate;
  TH1F *_hstRecordAcqRRate;

  TH1F *_hstRecordJobTRate;
  TH1F *_hstRecordRunTRate;
  TH1F *_hstRecordCfgTRate;
  TH1F *_hstRecordAcqTRate;

  TH1F *_hstRecordJobERate;
  TH1F *_hstRecordRunERate;
  TH1F *_hstRecordCfgERate;
  TH1F *_hstRecordAcqERate;

  TH1F *_hstRecordJobDRate;
  TH1F *_hstRecordRunDRate;
  TH1F *_hstRecordCfgDRate;
  TH1F *_hstRecordAcqDRate;
#else 
  HstRate *_hstRecordJobRRate;
  HstRate *_hstRecordRunRRate;
  HstRate *_hstRecordCfgRRate;
  HstRate *_hstRecordAcqRRate;

  HstRate *_hstRecordJobTRate;
  HstRate *_hstRecordRunTRate;
  HstRate *_hstRecordCfgTRate;
  HstRate *_hstRecordAcqTRate;

  HstRate *_hstRecordJobERate;
  HstRate *_hstRecordRunERate;
  HstRate *_hstRecordCfgERate;
  HstRate *_hstRecordAcqERate;

  HstRate *_hstRecordJobDRate;
  HstRate *_hstRecordRunDRate;
  HstRate *_hstRecordCfgDRate;
  HstRate *_hstRecordAcqDRate;
#endif

  RcdHeader _lastHeader;
  TH2D *_hstRecordTime[2][4];
};

#endif
