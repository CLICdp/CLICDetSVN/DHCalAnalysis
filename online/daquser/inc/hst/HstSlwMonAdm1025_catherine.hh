#ifndef HstSlwMonAdm1025_catherine_HH
#define HstSlwMonAdm1025_catherine_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "TROOT.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TGraphErrors.h"
#include "TPostScript.h"

#include "UtlAverage.hh"
#include "RcdRecord.hh"
#include "CrcFeEventData.hh"
#include "CrcLocationData.hh"
#include "CrcAdm1025SlowReadoutData.hh"
#include "DaqEvent.hh"
#include "HstBase.hh"
#include "HstTGraph.hh"
#include "SubAccessor.hh"

class HstSlwMonAdm1025_catherine : public HstBase {

public:
  HstSlwMonAdm1025_catherine(CrcLocationData<CrcAdm1025SlowReadoutData> d, bool i=true) : 
    HstBase(i), _cld(d), _firstRecordTime(0) {
    for(unsigned crc(0);crc<2;crc++){
      ostringstream sout0;
      sout0 << "Adm1025Canvas[0][" << crc << "]";
      ostringstream sout1;
      sout1 << "Adm1025Canvas[1][" << crc << "]";
      ostringstream sout2;
      sout2 << "Adm1025Canvas[2][" << crc << "]";
      ostringstream sout3;
      sout3 << "Adm1025Canvas[3][" << crc << "]";
      ostringstream sout4;
      sout4 << "Adm1025Canvas[4][" << crc << "]";
      ostringstream sout5;
      sout5 << "Adm1025Canvas[5][" << crc << "]";
      ostringstream sout6;
      sout6 << "Adm1025Canvas[6][" << crc << "]";
      ostringstream sout7;
      sout7 << "Adm1025Canvas[7][" << crc << "]";
      ostringstream sout8;
      sout8 << "Adm1025Canvas[8][" << crc << "]";
      _canvas[0][crc]=new TCanvas(sout0.str().c_str(),"Local temp",400, 20,600,400);
      _canvas[1][crc]=new TCanvas(sout1.str().c_str(),"Remote temp",420, 40,600,400);
      _canvas[2][crc]=new TCanvas(sout2.str().c_str(),"1.8V Voltage",400, 60,600,400);
      _canvas[3][crc]=new TCanvas(sout3.str().c_str(),"Vccp Voltage",380, 80,600,400);
      _canvas[4][crc]=new TCanvas(sout4.str().c_str(),"3.3V Voltage",360,100,600,400);
      _canvas[5][crc]=new TCanvas(sout5.str().c_str(),"5.0V Voltage",340,120,600,400);
      _canvas[6][crc]=new TCanvas(sout6.str().c_str(),"12.0V Voltage",320,140,600,400);
      _canvas[7][crc]=new TCanvas(sout7.str().c_str(),"Vcc Voltage",300,160,600,400);
      _canvas[8][crc]=new TCanvas(sout8.str().c_str(),"Status",280,180,600,400);
    }

  }

  virtual ~HstSlwMonAdm1025_catherine() {
  }

  bool record(const RcdRecord &r) {
 
    if(_firstRecordTime==0) {
      _firstRecordTime=r.recordTime().seconds();

      for(unsigned i(0);i<9;i++) {
	for(unsigned crc(0);crc<2;crc++){
	  ostringstream sout;
	  sout << "Crate " << (unsigned)_cld.crateNumber() << ", Slot ";
	  if(crc==0) sout << "12, ";
	  if(crc==1) sout << "17, "; 
	  sout << "CRC ADM1025A ";	  
	  if(i==0) sout << "Local Temperature";
	  if(i==1) sout << "Remote Temperature";
	  if(i==2) sout << " 1.8V Voltage";
	  if(i==3) sout << " Vccp Voltage";
	  if(i==4) sout << " 3.3V Voltage";
	  if(i==5) sout << " 5.0V Voltage";
	  if(i==6) sout << " 12.0V Voltage";
	  if(i==7) sout << " Vcc Voltage";
	  if(i==8) sout << "Status";
	  sout << " vs Time (Days) since " << r.recordTime();
	  _graph[i][crc].SetTitle(sout.str().c_str());
	  _graph[i][crc].SetMarkerColor(crc+2);
	  _graph[i][crc].SetMarkerStyle(22);
	}
      }
    }

    double deltat((r.recordTime().seconds()-_firstRecordTime)/(24.0*3600.0));
    
    SubAccessor extracter(r);
    
    if(r.recordType()==RcdHeader::slowReadout) {
      std::vector<const CrcLocationData<CrcAdm1025SlowReadoutData>*>
        c(extracter.extract< CrcLocationData<CrcAdm1025SlowReadoutData> >());
      
      if(printLevel()>2) c[0]->print(std::cout);
            
      // read in data      
      int crc(0);
      for(unsigned st(0);st<c.size();st++){
	if(c[st]->crcComponent()==CrcLocation::vmeAdm1025){
	if(c[st]->slotNumber()==12){
	  crc=0;
	  _graph[0][crc].AddPoint(deltat,c[st]->data()->localTemperature());
	  _graph[1][crc].AddPoint(deltat,c[st]->data()->remoteTemperature());
	  _graph[8][crc].AddPoint(deltat,c[st]->data()->status());
	  CrcAdm1025Voltages v(c[st]->data()->voltages());
	  _graph[2][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v25));
	  _graph[3][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::vccp));
	  _graph[4][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v33));
	  _graph[5][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v50));
	  _graph[6][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v120));
	  _graph[7][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::vcc));
	}
	if(c[st]->slotNumber()==17){
	  crc=1;
	  _graph[0][crc].AddPoint(deltat,c[st]->data()->localTemperature());
	  _graph[1][crc].AddPoint(deltat,c[st]->data()->remoteTemperature());
	  _graph[8][crc].AddPoint(deltat,c[st]->data()->status());
	  
	  CrcAdm1025Voltages v(c[st]->data()->voltages());
	  _graph[2][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v25));
	  _graph[3][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::vccp));
	  _graph[4][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v33));
	  _graph[5][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v50));
	  _graph[6][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::v120));
	  _graph[7][crc].AddPoint(deltat,v.dVoltage(CrcAdm1025Voltages::vcc));
	}
	}
      }
    }
    return true;
  }
  
  bool update() {
    for(unsigned i(0);i<9;i++) {
      for(unsigned crc(0);crc<2;crc++){
	_canvas[i][crc]->Clear();
	_graph[i][crc].Draw("AP");
	_canvas[i][crc]->Update();
      }
    }

    return true;
  }

  bool postscript(std::string file) {
    std::cout << "HstSlwMonAdm1025_catherine::postscript()  Writing to file "
              << file << std::endl;

    TPostScript ps(file.c_str(),112);
    update();
    ps.Close();

    return true;
  }

private:
  CrcLocationData<CrcAdm1025SlowReadoutData> _cld;
  unsigned _firstRecordTime;
  TCanvas *_canvas[9][2];
  HstTGraph _graph[9][2];  // 9 variables, 2 CRCs
};

#endif
