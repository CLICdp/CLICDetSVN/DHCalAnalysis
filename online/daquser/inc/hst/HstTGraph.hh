#ifndef HstTGraph_HH
#define HstTGraph_HH

#include "TGraph.h"


class HstTGraph : public TGraph {

public:
  HstTGraph() : TGraph() {
    SetMarkerColor(1);
    SetMarkerStyle(20);
  }

  virtual ~HstTGraph() {
  }

  void AddPoint(Double_t x, Double_t y) {
    Int_t n(GetN());
    Set(n+1);
    SetPoint(n,x,y);
  }
};

#endif
