#ifndef RunsReader_HH
#define RunsReader_HH

#include <vector>

#include "RcdUserRW.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"


class RunsReader : public RcdUserRW {

public:
  enum File {
    bin,asc
  };

  RunsReader() : _reader(0), _vectorNumber(0) {
    _reader=new RcdReader;
  }

  virtual ~RunsReader() {
  }

  void runNumber(unsigned n, File b=bin) {
    _runNumber.push_back(n);
    _runBin.push_back(b);
  }

  void runNumber(unsigned nLo, unsigned nHi, File b=bin) {
    for(unsigned i(nLo);i<=nHi;i++) {
      _runNumber.push_back(i);
      _runBin.push_back(b);
    }
  }

  bool record(RcdRecord &r) {

    while(!_reader->read(r)) {
      _reader->close();

      // Done all the runs
      if(_vectorNumber>=_runNumber.size()) return false;

      // Open output file
      std::ostringstream sout;
      sout << "data/dat/Run" << _runNumber[_vectorNumber];

      delete _reader;
      if(_runBin[_vectorNumber]==bin) _reader=new RcdReaderBin;
      else                            _reader=new RcdReaderAsc;

      _reader->open(sout.str());

      _vectorNumber++;
    }

    return true;
  }

  // For convenience of converting from RcdReader
  bool read(RcdRecord &r) {
    return record(r);
  }


private:
  RcdReader *_reader;

  unsigned _vectorNumber;
  std::vector<unsigned> _runNumber;
  std::vector<File> _runBin;
};

#endif
