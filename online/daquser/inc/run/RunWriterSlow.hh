#ifndef RunWriterSlow_HH
#define RunWriterSlow_HH

#include <cassert>

#include "DaqRunStart.hh"

#include "RcdUserRO.hh"
#include "RcdWriterBin.hh"

#include "SubAccessor.hh"


class RunWriterSlow : public RcdUserRO {

public:
  RunWriterSlow() : _slowRecords(true) {
  }

  virtual ~RunWriterSlow() {
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "RunWriterSlow::record()" << std::endl;
      r.RcdHeader::print(std::cout," ");
    }

    // At start of job, open new slow file
    if(r.recordType()==RcdHeader::startUp) {
      std::ostringstream sout;
      sout << "data/slw/Slw" << r.recordTime().seconds();
      assert(_writer.open(sout.str()));
    }

    // Everything below acquisition doesn't get written to slow file
    if(r.recordType()==RcdHeader::acquisitionStart ||
       r.recordType()==RcdHeader::bunchTrain) {
      _slowRecords=false;
    }

    // Catch non-acquisition runs
    if(r.recordType()==RcdHeader::configurationEnd) {
      _slowRecords=true;
    }

    // Write out records
    if(_slowRecords || r.recordType()==RcdHeader::slowReadout) assert(_writer.write(r));

    // Turn back on records to slow file
    if(r.recordType()==RcdHeader::acquisitionEnd) {
      _slowRecords=true;
    }

    // Close file at end of job
    if(r.recordType()==RcdHeader::shutDown) {
      assert(_writer.close());
    }

    return true;
  }

  /*
  unsigned numberOfBytes() const {
    return _writer->numberOfBytes();
  }
  */

private:
  RcdWriterBin _writer;
  bool _slowRecords;
};

#endif
