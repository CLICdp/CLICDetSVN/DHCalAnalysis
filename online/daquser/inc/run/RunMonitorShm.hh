#ifndef RunMonitorShm_HH
#define RunMonitorShm_HH

#include <sys/types.h>
#include <unistd.h>
#include <time.h>

#include "DaqRunStart.hh"
#include "IlcRunStart.hh"
#include "RunCount.hh"


class RunMonitorShm {

public:
  enum {
    shmKey=0x76543213
  };

  RunMonitorShm() {
  }

#ifndef DAQ_ILC_TIMING
  DaqRunStart& runStart() {
    return _runStart;
  }
 
  void runStart(const DaqRunStart d) {
    _runStart=d;
  }
 
#else
  IlcRunStart& runStart() {
    return _runStart;
  }

  void runStart(const IlcRunStart d) {
    _runStart=d;
  }
#endif

  RunCount& runCount() {
    return _runCount;
  }

private:
#ifndef DAQ_ILC_TIMING
  DaqRunStart _runStart;
#else
  IlcRunStart _runStart;
#endif
  RunCount _runCount;
};

#endif
