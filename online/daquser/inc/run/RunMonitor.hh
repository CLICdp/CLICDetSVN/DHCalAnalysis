#ifndef RunMonitor_HH
#define RunMonitor_HH

#include <cassert>

#include "ShmObject.hh"
#include "RunMonitorShm.hh"

#include "RcdUserRO.hh"
#include "SubAccessor.hh"

class RunMonitor : public RcdUserRO {

public:
  RunMonitor() : _shmRunMonitor(RunMonitorShm::shmKey) {

    // Connect to run monitor shared memory
    _runMonitor=_shmRunMonitor.payload();
    assert(_runMonitor!=0);
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "RunMonitor::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    _runMonitor->runCount().increment(r);

    if(r.recordType()==RcdHeader::runStart) {
      // Access the DaqRunStart
      SubAccessor accessor(r);
#ifndef DAQ_ILC_TIMING
      std::vector<const DaqRunStart*>
        v(accessor.extract<DaqRunStart>());
#else
      std::vector<const IlcRunStart*>
        v(accessor.extract<IlcRunStart>());
#endif
      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;
      
      _runMonitor->runStart(*(v[0]));
    }

    return true;
  }

private:
  ShmObject<RunMonitorShm> _shmRunMonitor;
  RunMonitorShm *_runMonitor;
};

#endif
