#ifndef ClientSocket_HH
#define ClientSocket_HH

#include "Socket.hh"
#include "SocketException.hh"


class ClientSocket : private Socket {
public:

  ClientSocket ( std::string host, int port ) {
    if ( ! Socket::create() )
      {
	throw SocketException ( "ClientSocket could not create client socket." );
      }    
    if ( ! Socket::connect ( host, port ) )
      {
	throw SocketException ( "ClientSocket could not bind to port." );
      }
  }

  virtual ~ClientSocket(){
  }


  template<class T> bool ClientSocket::send( const T* t, int n = 1 ) const {
    if ( ! Socket::send(t,n) )
      {
        throw SocketException ( "ClientSocket could not write to socket.");
      }
    return true;
  }

  template<class T> int ClientSocket::recv( T* t, int m ) const {
    int n=Socket::recv(t,m);
    if (n <= 0)
      {
        throw SocketException ( "ClientSocket could not read from socket.");
      }
    return n;
  }

  template<class T> bool ClientSocket::send( const T& t ) const {
    //    if ( ! Socket::send(t) )
    //      {
    //        throw SocketException ( "ClientSocket could not write to socket.");
    //      }
    //    return true;
    return send(&t,1);
  }

  template<class T> int ClientSocket::recv( T& t ) const {
    //    if ( ! Socket::recv(t) )
    //      {
    //        throw SocketException ( "ClientSocket could not read from socket.");
    //      }
    //    return true;
    return recv(&t,1);
  }

  void print(ostream &o) const {
    Socket::print(o);
  }
};


#endif
