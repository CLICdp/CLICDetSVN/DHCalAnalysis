The maximum amount of data which can be sent to a socket without being
received at the other end seems to be around 135kbytes. After this, the
socket send call hangs until a recv occurs.

The maximum packet which can be send in one go appears to be around
48kbytes. However, reliable transmission seems to require 16kbytes.
