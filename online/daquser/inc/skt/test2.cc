#include <unistd.h>
#include <cstdio>
#include <iostream>
#include "DuplexSocket.hh"

using namespace std;

int main() {
  char c;

  if(fork()==0) {
    unsigned i(0);
    while(true) {
      cout << '\r' << "Counted to " << i << " so enter something" << flush;
      i++;
      sleep(1);
    }
    return 0;
  }


  while(true) {
    cin >> c;
    cout << "Command was " << c << endl;
  }

  return 0;
}
