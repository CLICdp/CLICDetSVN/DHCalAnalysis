#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "DuplexSocket.hh"

using namespace std;

class Big {
public:
  char data[20*1024];
};


void sh(int signal) {
 std::cout << "Process " << getpid() << " received signal "
	   << signal << std::endl; 
}

int main() {
  cout << "sizeof(Big) = " << sizeof(Big) << endl;
  Big b;

  if(fork()!=0) {
    DuplexSocket d(1124);
    sleep(1);
    d.print(cout);
    for(unsigned i(0);i<1000;i++) {
      usleep(100000);
      cout << "Sending packet = " << i << endl;
      try {
	d.send(&b,1);
      } catch(SocketException &e) {
      }
      perror(0);
    }
  } else {
    DuplexSocket d("lx09",1124,10);
    d.print(cout);
    sleep(3);
    cout << "Woken from sleep" << endl;
        for(unsigned i(0);i<1000;i++) {
          d.recv(&b,1);
	  cout << "Received packet = " << i << endl;
        }
  }

  return 0;
}
