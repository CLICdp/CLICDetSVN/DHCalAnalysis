#ifndef RecordSocket_HH
#define RecordSocket_HH

#include "Record.hh"
#include "DuplexSocket.hh"

class RecordSocket : public DuplexSocket {

public:
  RecordSocket(int port, bool singleSocket=true)
    : DuplexSocket(port,signelSocket) {
  };

  RecordSocket(std::string host, int port, int tries = 0)
    : DuplexSocket(host,port,tries) {
  };

  bool send(const Record* r) const {
    return send((unsigned*)r,r->totalNumberOfWords());
  }
}

#endif
