#ifndef ListenerSocket_HH
#define ListenerSocket_HH

#include <cstdio>

#include <iostream>

#include "Socket.hh"
#include "SocketException.hh"

class ListenerSocket : public Socket
{
 public:

  ListenerSocket(int port) {
    if ( ! Socket::create() )
      {
	throw SocketException ( "ListenerSocket could not create server socket." );
      }

    if ( ! Socket::bind ( port ) )
      {
	perror(0);
	throw SocketException ( "ListenerSocket could not bind to port." );
      }

    if ( ! Socket::listen() )
      {
	throw SocketException ( "ListenerSocket could not listen to socket." );
      }
  };

  ListenerSocket() {
  };

  virtual ~ListenerSocket() {
  };

  void print(std::ostream &o) const {
    Socket::print(o);
  }
};

#endif
