#ifndef SockAddrIn_HH
#define SockAddrIn_HH

#include <sys/types.h>
#include <sys/socket.h>

class SockAddrIn : public sockaddr_in {

public:
  SockAddrIn() {
    memset(this,0,sizeof(SockAddrIn));
  };

  void print(std::ostream &o) const {
    o << "SockAddrIn address family (sin_family) = " 
      << sin_family << std::endl;
    o << "SockAddrIn port (sin_port) = " << sin_port
      << ", ntohs(sin_port) = " << ntohs(sin_port) << std::endl;

    char dst[128];
    o << "SockAddrIn internet address (sin_addr) = " << sin_addr.s_addr
      << ", inet_ntop(sin_family,sin_addr,,) = "
      << inet_ntop(sin_family,&sin_addr,dst,128) << std::endl;

    int *sinZero((int*)(&sin_zero));
    o << "SockAddrIn padding (sin_zero) = 0x" << std::hex << sinZero[0] 
      << " 0x" << sinZero[1] << std::dec << std::endl;
  }
};

#endif
