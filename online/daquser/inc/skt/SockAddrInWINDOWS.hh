#ifndef SockAddrIn_HH
#define SockAddrIn_HH

#include <iostream>

#include <sys/types.h>
#ifndef WINDOWS_CRAP
#include <sys/sockets.h>
#else
#include <winsock.h>
#endif

class SockAddrIn : public sockaddr_in {

public:
  SockAddrIn() {
    memset(this,0,sizeof(SockAddrIn));
  };

  void print(std::ostream &o) const {
    o << "SockAddrIn address family (sin_family) = " 
      << sin_family << std::endl;
    o << "SockAddrIn port (sin_port) = " << sin_port
#ifndef WINDOWS_CRAP
      << ", ntohs(sin_port) = " << ntohs(sin_port) << std::endl;
#else
      << std::endl;
#endif

#ifndef WINDOWS_CRAP
    char dst[128];
#endif
    o << "SockAddrIn internet address (sin_addr) = " << sin_addr.s_addr
      << ", inet_ntop(sin_family,sin_addr,,) = "
#ifndef WINDOWS_CRAP
      << inet_ntop(sin_family,&sin_addr,dst,128) << std::endl;
#else
      << std::endl;
#endif

    int *sinZero=(int*)(&sin_zero);
    o << "SockAddrIn padding (sin_zero) = 0x" << std::hex << sinZero[0] 
      << " 0x" << sinZero[1] << std::dec << std::endl;
  }
};

#endif
