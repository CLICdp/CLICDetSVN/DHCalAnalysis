#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include "DuplexSocket.hh"

using namespace std;

void sh(int signal) {
 std::cout << "Process " << getpid() << " received signal "
	   << signal << std::endl; 
}

int main() {
  signal(SIGIO,sh);
  perror(0);

  DuplexSocket d(1124);
  char n[]="abd";
  //  d.recv(n,1);
  sleep(100);
  int fd(d.fileDescriptor());
  cout << "Signal = " << fcntl(fd,F_GETFL) << " SIGIO = " << SIGIO << endl;

  return 0;
}
