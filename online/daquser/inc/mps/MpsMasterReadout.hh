#ifndef MpsMasterReadout_HH
#define MpsMasterReadout_HH

#include <iostream>
#include <fstream>

#include "RcdUserRW.hh"

#include "SubRecordType.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "UsbDaqMasterDevice.hh"
#include "MpsUsbDaqSpillPollData.hh"


class MpsMasterReadout : public RcdUserRW {

public:
  MpsMasterReadout(unsigned s=0) : _spillPoll(false), _maxSpillPollWait(20) {

    _usbDaqMaster=new UsbDaqMasterDevice;
    if(_usbDaqMaster->alive()) {
	MpsUsbDaqRunData x;
	assert(_usbDaqMaster->readMpsUsbDaqRunData(x));

	_location=MpsLocation(s,x.usbDaqAddress(),x.sensorId());
	_location.usbDaqMasterFirmware(true);

    } else {
      delete _usbDaqMaster;
      _usbDaqMaster=0;
    }
  }

  virtual ~MpsMasterReadout() {
    delete _usbDaqMaster;
  }
  
  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "MpsMasterReadout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
    
    SubInserter inserter(r);
    
    // Check record type
    switch (r.recordType()) {
      
      // Run start
    case RcdHeader::runStart: {
      
      // Reset whole system and then readback run data
      if(_usbDaqMaster!=0) _usbDaqMaster->runReset();
      readRunData(r);
      
      break;
    }
 
      // Configuration start is used to set up system
    case RcdHeader::configurationStart: {
      SubAccessor accessor(r);

      std::vector<const MpsLocationData<MpsUsbDaqMasterConfigurationData>*>
        m(accessor.access< MpsLocationData<MpsUsbDaqMasterConfigurationData> >());
      
      if(doPrint(r.recordType(),1)) {
	std::cout << " MpsMasterReadout::record()  configurationStart"
		  << ", number of MpsUsbDaqMasterConfigurationData objects found = "
		  << m.size() << std::endl << std::endl;
      }
      
      assert(m.size()<=1);

      if(_usbDaqMaster!=0 && m.size()==1) {
	if(doPrint(r.recordType(),2)) {
	  std::cout << " MpsMasterReadout::record()"
		    << "  Write MpsUsbDaqMasterConfigurationData object"
		    << " to master USB_DAQ" << std::endl;
	  m[0]->print(std::cout," ") << std::endl;
	}

	_spillPoll=m[0]->data()->spillVetoEnable();
	_usbDaqMaster->writeMpsUsbDaqMasterConfigurationData(*(m[0]->data()));
      }

      readConfigurationData(r);
      
      break;
    }
      
    case RcdHeader::bunchTrain: {
      unsigned n(0);
      
      if(_usbDaqMaster!=0) {

	// Poll for the spill
	if(_spillPoll) {
	  MpsLocationData<MpsUsbDaqSpillPollData>
	    *m(inserter.insert< MpsLocationData<MpsUsbDaqSpillPollData> >(true));
	  m->data()->maximumTime(_maxSpillPollWait);
	  m->location(_location);

	  assert(_usbDaqMaster->mpsUsbDaqSpillPoll(*(m->data())));
	  
	  if(doPrint(r.recordType(),1)) {
	    std::cout << " MpsMasterReadout::record()"
		      << "  MpsSpillPollData object"
		      << " from master USB_DAQ" << std::endl;
	    m->print(std::cout," ") << std::endl;
	  }
	}

	// Fire the USB_DAQ master
	assert(_usbDaqMaster->takeBunchTrain());
      
	// Read out USB_DAQ master data
	MpsLocationData<MpsUsbDaqBunchTrainData>
	  *m(inserter.insert< MpsLocationData<MpsUsbDaqBunchTrainData> >());
	m->location(_location);
	
	assert(_usbDaqMaster->readMpsUsbDaqBunchTrainData(*(m->data())));
	inserter.extend(m->data()->numberOfExtraBytes());
	
	if(doPrint(r.recordType(),2)) {
	  std::cout << " MpsMasterReadout::record()"
		    << "  Read MpsUsbDaqBunchTrainData object"
		    << " from master USB_DAQ" << std::endl;
	  m->print(std::cout," ") << std::endl;
	}

	MpsLocationData<MpsEudetBunchTrainData>
	  *e(inserter.insert< MpsLocationData<MpsEudetBunchTrainData> >());
	e->location(_location);
	
	assert(_usbDaqMaster->readMpsEudetBunchTrainData(*(e->data())));
	inserter.extend(e->data()->numberOfExtraBytes());
	
	if(doPrint(r.recordType(),2)) {
	  std::cout << " MpsMasterReadout::record()"
		    << "  Read MpsEudetBunchTrainData object"
		    << " from master USB_DAQ" << std::endl;
	  e->print(std::cout," ") << std::endl;
	}

	n++;
      }

      if(doPrint(r.recordType(),1)) {
	std::cout << " MpsMasterReadout::record()  bunchTrain"
		  << ", number of MpsUsbDaqBunchTrainData objects read = "
		  << n << std::endl << std::endl;
      
	std::cout << " MpsMasterReadout::record()  bunchTrain"
		  << ", number of MpsEudetBunchTrainData objects read = "
		  << n << std::endl << std::endl;
      }

      break;
    }

    case RcdHeader::configurationEnd: {
      readConfigurationData(r);
      break;
    }
 
      // Run end
    case RcdHeader::runEnd: {
      readRunData(r);
      break;
    }
 
    default: {
      break;
    }
    };
    
    return true;
  }

private:
  bool readRunData(RcdRecord &r) {
    unsigned n(0);

    if(_usbDaqMaster!=0) {
      SubInserter inserter(r);

      MpsLocationData<MpsUsbDaqRunData>
	*m(inserter.insert< MpsLocationData<MpsUsbDaqRunData> >());
      m->location(_location);

      assert(_usbDaqMaster->readMpsUsbDaqRunData(*(m->data())));
      
      if(doPrint(r.recordType(),2)) {
	std::cout << " MpsMasterReadout::readRunData()"
		  << "  Read MpsUsbDaqRunData object"
		  << " from master USB_DAQ" << std::endl;
	m->print(std::cout," ") << std::endl;
      }

      n++;
    }

    if(doPrint(r.recordType(),1)) {
      std::cout << " MpsMasterReadout::readRunData()"
		<< "  Number of MpsUsbDaqRunData objects read = "
		<< n << std::endl << std::endl;
    }
    
    return true;
  }

  bool readConfigurationData(RcdRecord &r) {
    unsigned n(0);

    if(_usbDaqMaster!=0) {
      SubInserter inserter(r);

	MpsLocationData<MpsUsbDaqMasterConfigurationData>
	  *m(inserter.insert< MpsLocationData<MpsUsbDaqMasterConfigurationData> >());
	m->location(_location);
	
	assert(_usbDaqMaster->readMpsUsbDaqMasterConfigurationData(*(m->data())));
	m->data()->spillVetoEnable(_spillPoll);
	
	if(doPrint(r.recordType(),2)) {
	  std::cout << " MpsMasterReadout::readConfigurationData()"
		    << "  Read MpsUsbDaqMasterConfigurationData object"
		    << " from master USB_DAQ" << std::endl;
	  m->print(std::cout," ") << std::endl;
	}
	
	n++;
    }

    if(doPrint(r.recordType(),1)) {
      std::cout << " MpsMasterReadout::readConfigurationData()"
		<< "  Number of MpsUsbDaqMasterConfigurationData objects read = "
		<< n << std::endl << std::endl;
    }

    return true;
  }


  UsbDaqMasterDevice *_usbDaqMaster;
  MpsLocation _location;

  bool _spillPoll;
  UtlTimeDifference _maxSpillPollWait;
};

#endif
