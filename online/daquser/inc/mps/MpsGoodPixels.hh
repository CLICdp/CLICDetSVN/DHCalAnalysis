#ifndef MpsGoodPixels_HH
#define MpsGoodPixels_HH

#include <iostream>
#include <string>

#include "UtlPack.hh"


class MpsGoodPixels {

public:
  MpsGoodPixels();

  bool good(unsigned x, unsigned y) const;
  void good(unsigned x, unsigned y, bool g);

  void goodGroup(unsigned r, unsigned g, unsigned y, bool b);
  void goodColumn(unsigned x, bool g);
  void goodSensor(bool g);

  unsigned goodNumber() const;
  double efficiency() const;

  bool readFile(const std::string &s);
  bool writeFile(const std::string &s) const;

  bool readRunLayer( unsigned r, unsigned l, const std::string &s="data/pxl/");
  bool writeRunLayer(unsigned r, unsigned l, const std::string &s="") const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  UtlPack _flags[882];
};


#ifdef CALICE_DAQ_ICC


MpsGoodPixels::MpsGoodPixels() {
  memset(this,0xff,sizeof(MpsGoodPixels));
}

bool MpsGoodPixels::good(unsigned x, unsigned y) const {
  assert(x<168);
  assert(y<168);

  unsigned i(168*y+x);
  return _flags[i/32].bit(i%32);
}

void MpsGoodPixels::good(unsigned x, unsigned y, bool g) {
  assert(x<168);
  assert(y<168);

  unsigned i(168*y+x);
  _flags[i/32].bit(i%32,g);
}

void MpsGoodPixels::goodGroup(unsigned r, unsigned g, unsigned y, bool b) {
  assert(r<4);
  assert(g<7);
  assert(y<168);
  
  for(unsigned x(0);x<6;x++) {
    good(42*r+6*g+x,y,b);
  }
}

void MpsGoodPixels::goodColumn(unsigned x, bool g) {
  assert(x<168);

  for(unsigned y(0);y<168;y++) {
    good(x,y,g);
  }
}

void MpsGoodPixels::goodSensor(bool g) {
  for(unsigned y(0);y<168;y++) {
    for(unsigned x(0);x<168;x++) {
      good(x,y,g);
    }
  }
}

unsigned MpsGoodPixels::goodNumber() const {
  unsigned n(0);
  for(unsigned y(0);y<168;y++) {
    for(unsigned x(0);x<168;x++) {
      if(good(x,y)) n++;
    }
  }
  return n;
}

double MpsGoodPixels::efficiency() const {
  return goodNumber()/(168.0*168.0);
}

bool MpsGoodPixels::readFile(const std::string &s) {
  std::cout << "MpsGoodPixels::readFile()  Reading file " << s << std::endl;

  std::ifstream fin(s.c_str());
  if(!fin) return false;

  fin >> std::hex;
  unsigned f;
  for(unsigned i(0);i<882;i++) {
    fin >> f;
    _flags[i].word(f);
  }
  if(!fin) return false;

  fin >> f;
  return !fin;
}

bool MpsGoodPixels::writeFile(const std::string &s) const {
  std::ofstream fout(s.c_str());
  if(!fout) return false;

  fout << std::hex;
  for(unsigned i(0);i<882;i++) {
    fout << std::setw(8) << std::setfill('0') << _flags[i].word() << std::endl;
  }

  return fout;
}

bool MpsGoodPixels::readRunLayer(unsigned r, unsigned l, const std::string &s) {
  assert(l<6);
  std::ostringstream sout;
  sout << "Run" << std::setw(6) << std::setfill('0') << r << "Layer" << l;
  return readFile(s+sout.str()+".pxl");
}

bool MpsGoodPixels::writeRunLayer(unsigned r, unsigned l, const std::string &s) const {
  assert(l<6);
  std::ostringstream sout;
  sout << "Run" << std::setw(6) << std::setfill('0') << r << "Layer" << l;
  return writeFile(s+sout.str()+".pxl");
}

std::ostream& MpsGoodPixels::print(std::ostream &o, std::string s) const {
  o << s << "MpsGoodPixels::print()" << std::endl;

  o << s << " Total number of good pixels = " << goodNumber() << "/28224" << std::endl;

  for(unsigned y(0);y<168;y++) {
    o << s << "  Row " << std::setw(3) << y;
    for(unsigned x(0);x<168;x++) {
      if((x%42)==0) o << " ";
      if(good(x,y)) o << "1";
      else          o << "0";
    }    
    o << std::endl;
  }

  return o;
}

#endif
#endif
