#ifndef MpsSensor1HitLists_HH
#define MpsSensor1HitLists_HH

#include <iostream>
#include <fstream>
#include <cassert>


class MpsSensor1HitLists {

public:
  MpsSensor1HitLists(const MpsSensor1BunchTrainData &m) {

    for(unsigned region(0);region<4;region++) {
      const MpsSensor1BunchTrainDatum *p(m.regionData(region));

      for(unsigned i(0);i<m.numberOfRegionHits(region);i++) {
	unsigned j(m.numberOfRegionHits(region)-i-1);

	unsigned y(p[j].row());
	unsigned xStub(42*region+6*p[j].group());
	if(y<168 && xStub<162) {

	  for(unsigned c(0);c<6;c++) {
	    if(p[j].channel(c)) {
	      unsigned x(xStub+c);

	      bool print(false);
	      //bool print(x==35 && y==65);
	      //bool print(x<84);

	      if(_vHitLists[x][y].size()>0 &&
		 _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(1)+(unsigned)1==p[j].timeStamp()) {

		if(print) {
		  std::cout << "Extending  x,y " << x << ", " << y
			    << " hit " << _vHitLists[x][y].size()-1 << " from "
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(0) << "-"
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(1);
		}

		_vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(1,p[j].timeStamp());

		if(print) {
		  std::cout << " to "
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(0) << "-"
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(1) << std::endl;
		}


	      } else {
		_vHitLists[x][y].push_back(0x00010001*p[j].timeStamp());

		if(print) {
		  std::cout << "Initiating x,y " << x << ", " << y
			    << " hit " << _vHitLists[x][y].size()-1 << " from "
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(0) << "-"
			    << _vHitLists[x][y][_vHitLists[x][y].size()-1].halfWord(1) << std::endl;
		}

	      }
	    }
	  }
	}
      }
    }
  }

  virtual ~MpsSensor1HitLists() {
  }

  /*
  unsigned fullTimeStamp(unsigned region, unsigned row, unsigned group) const {
    assert(region<4);
    assert(row<168);
    assert(group<7);

    return _fullTimeStamp[region][row][group];
  }

  unsigned fullTimeStamp(unsigned x, unsigned y) const {
    assert(x<168);
    assert(y<168);

    unsigned region(x/42);
    unsigned group((x%42)/6);
    return _fullTimeStamp[region][y][group];
  }

  void fullTimeStamp(unsigned region, unsigned row, unsigned group, unsigned t) {
    assert(region<4);
    assert(row<168);
    assert(group<7);

    _fullTimeStamp[region][row][group]=t;
  }
  */

private:
public:
  std::vector<UtlPack> _vHitLists[168][168];
};

#endif
