#ifndef MpsAnalysis_HH
#define MpsAnalysis_HH

#include <cassert>

#include "RcdMultiUserRO.hh"

#include "IlcAnalysis.hh"
#include "MpsAnalysisCheck.hh"
#include "MpsAnalysisNoise.hh"
#include "MpsAnalysisPcbConfigurationScan.hh"
#include "MpsAnalysisSource.hh"
#include "MpsAnalysisTrimScan.hh"
#include "MpsAnalysisThresholdScan.hh"
#include "MpsAnalysisPmtThresholdScan.hh"
#include "MpsAnalysisCosmics.hh"
#include "MpsAnalysisHitOverride.hh"
#include "MpsAnalysisLaser.hh"
#include "MpsAnalysisUsbDaqConfigurationScan.hh"
#include "MpsAnalysisBeamThresholdScan.hh"
#include "MpsAnalysisBeam.hh"
#include "MpsAnalysisMaskThresholdScan.hh"
#include "MpsAnalysisLaserPositionScan.hh"
#include "MpsAnalysisLaserThresholdScan.hh"
#include "MpsAnalysisLaserCoordinates.hh"
#include "MpsAnalysisLaserTimeScan.hh"


class MpsAnalysis : public RcdMultiUserRO {

public:

  // Set default bits to enable required histograms
  enum {
    defaultMpsAnalysisBits=0xfffffbfd
  };


  MpsAnalysis(const UtlPack bits=defaultMpsAnalysisBits) {

    _ignorRunType=true;

    // Add modules
    if(bits.bit( 0)) addUser(*(new IlcAnalysis()));
    else std::cout << "IlcAnalysis disabled" << std::endl;

    if(bits.bit( 1)) addUser(*(new MpsAnalysisCheck()));
    else std::cout << "MpsAnalysisCheck disabled" << std::endl;

    if(bits.bit( 2)) addUser(*(new MpsAnalysisNoise()));
    else std::cout << "MpsAnalysisNoise disabled" << std::endl;

    if(bits.bit( 3)) addUser(*(new MpsAnalysisPcbConfigurationScan()));
    else std::cout << "MpsAnalysisPcbConfigurationScan disabled" << std::endl;

    if(bits.bit( 4)) addUser(*(new MpsAnalysisSource()));
    else std::cout << "MpsAnalysisSource disabled" << std::endl;

    if(bits.bit( 5)) addUser(*(new MpsAnalysisTrimScan()));
    else std::cout << "MpsAnalysisTrimScan disabled" << std::endl;

    if(bits.bit( 6)) addUser(*(new MpsAnalysisPmtThresholdScan()));
    else std::cout << "MpsAnalysisPmtThresholdScan disabled" << std::endl;

    if(bits.bit( 7)) addUser(*(new MpsAnalysisThresholdScan()));
    else std::cout << "MpsAnalysisThresholdScan disabled" << std::endl;

    if(bits.bit( 8)) addUser(*(new MpsAnalysisCosmics()));
    else std::cout << "MpsAnalysisCosmics disabled" << std::endl;

    if(bits.bit( 9)) addUser(*(new MpsAnalysisHitOverride()));
    else std::cout << "MpsAnalysisHitOverride disabled" << std::endl;

    if(bits.bit(10)) addUser(*(new MpsAnalysisLaser()));
    else std::cout << "MpsAnalysisLaser disabled" << std::endl;

    if(bits.bit(11)) addUser(*(new MpsAnalysisUsbDaqConfigurationScan()));
    else std::cout << "MpsAnalysisUsbDaqConfigurationScan disabled" << std::endl;

    if(bits.bit(12)) addUser(*(new MpsAnalysisBeamThresholdScan()));
    else std::cout << "MpsAnalysisBeamThresholdScan disabled" << std::endl;

    if(bits.bit(13)) addUser(*(new MpsAnalysisMaskThresholdScan()));
    else std::cout << "MpsAnalysisMaskThresholdScan disabled" << std::endl;

    if(bits.bit(14)) addUser(*(new MpsAnalysisLaserPositionScan()));
    else std::cout << "MpsAnalysisLaserPositionScan disabled" << std::endl;

    if(bits.bit(15)) addUser(*(new MpsAnalysisLaserThresholdScan()));
    else std::cout << "MpsAnalysisLaserThresholdScan disabled" << std::endl;

    if(bits.bit(16)) addUser(*(new MpsAnalysisLaserCoordinates()));
    else std::cout << "MpsAnalysisLaserCoordinates disabled" << std::endl;

    if(bits.bit(17)) addUser(*(new MpsAnalysisLaserTimeScan()));
    else std::cout << "MpsAnalysisLaserTimeScan disabled" << std::endl;

    if(bits.bit(18)) addUser(*(new MpsAnalysisBeam()));
    else std::cout << "MpsAnalysisBeam disabled" << std::endl;
  }

  virtual ~MpsAnalysis() {
    deleteAll();
  }

  virtual bool record(const RcdRecord &r) {

    // Possibly set print level
    if(!_ignorRunType) {
      if(r.recordType()==RcdHeader::runStart) {
	SubAccessor accessor(r);
	std::vector<const IlcRunStart*>
	  v(accessor.extract<IlcRunStart>());
        if(v.size()==1) printLevel(v[0]->runType().printLevel());
      }
    }

    return RcdMultiUserRO::record(r);
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }

private:
  bool _ignorRunType;
};

#endif
