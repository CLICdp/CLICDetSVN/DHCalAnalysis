#ifndef MpsConfigurationFeedback_HH
#define MpsConfigurationFeedback_HH


#include <iostream>
#include <fstream>

#include "RcdUserRO.hh"

#include "SubRecordType.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "MpsConfiguration.hh"


class MpsConfigurationFeedback : public RcdUserRO {

public:
  MpsConfigurationFeedback(MpsLocation::Site s) : _site(s), _firstRunStart(true) {
  }

  virtual ~MpsConfigurationFeedback() {
  }
  
  virtual bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "MpsConfigurationFeedback::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
    
    // Check record type
    switch (r.recordType()) {
      
      // Run start
    case RcdHeader::runStart: {
      if(_firstRunStart) {
	SubAccessor accessor(r);
      
	std::vector<const MpsLocationData<MpsUsbDaqRunData>* >
	  c(accessor.access< MpsLocationData<MpsUsbDaqRunData> >());
	
	if(doPrint(r.recordType(),1)) {
	  std::cout << " MpsConfigurationFeedback::record()  runStart"
		    << ", number of MpsUsbDaqRunData objects found = "
		    << c.size() << std::endl << std::endl;
	  for(unsigned i(0);i<c.size();i++) {
	    if(doPrint(r.recordType(),2)) c[i]->print(std::cout," ") << std::endl;
	  }
	}

	for(unsigned i(0);i<c.size();i++) {
	  if(!c[i]->usbDaqMasterFirmware()) {
	    //MpsLocation l(_site,c[i]->data()->usbDaqAddress(),c[i]->data()->sensorId());
	    //l.usbDaqMaster(c[i]->data()->master());
	  
	    // Tell configuration which sensors are connected
	    MpsConfiguration::addLocation(c[i]->location());
	  }
	}

	_firstRunStart=false;
      }
      
      break;
    }

    default: {
      break;
    }
    };
    
    return true;
  }

private:
  const MpsLocation::Site _site;
  bool _firstRunStart;
};

#endif
