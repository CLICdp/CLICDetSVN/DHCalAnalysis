#ifndef MpsTrackHit_HH
#define MpsTrackHit_HH

#include <iostream>
#include <string>


class MpsTrackHit {

public:
  MpsTrackHit();
  MpsTrackHit(double a, double b, double c, double d, double e);

  double x() const;
  void   x(double c);

  double y() const;
  void   y(double c);

  double z() const;
  void   z(double c);

  double xError() const;
  void   xError(double e);

  double yError() const;
  void   yError(double e);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _x;
  double _y;
  double _z;
  double _xError;
  double _yError;
};


#ifdef CALICE_DAQ_ICC


MpsTrackHit::MpsTrackHit() {
}

MpsTrackHit::MpsTrackHit(double a, double b, double c, double d, double e)
  : _x(a), _y(b), _z(c), _xError(d), _yError(e) {
}

double MpsTrackHit::x() const {
  return _x;
}

void MpsTrackHit::x(double c) {
  _x=c;
}

double MpsTrackHit::y() const {
  return _y;
}

void MpsTrackHit::y(double c) {
  _y=c;
}

double MpsTrackHit::z() const {
  return _z;
}

void MpsTrackHit::z(double c) {
  _z=c;
}

double MpsTrackHit::xError() const {
  return _xError;
}

void MpsTrackHit::xError(double e) {
  _xError=e;
}

double MpsTrackHit::yError() const {
  return _yError;
}

void MpsTrackHit::yError(double e) {
  _yError=e;
}

std::ostream& MpsTrackHit::print(std::ostream &o, std::string s) const {
  o << s << "MpsTrackHit::print()" << std::endl;
  o << s << " Hit z = " << _z << " mm" << std::endl;
  o << s << " Hit x = " << _x << " +/- " << _xError << " mm" << std::endl;
  o << s << " Hit y = " << _y << " +/- " << _yError << " mm" << std::endl;

  return o;
}

#endif
#endif
