#ifndef MpsFullMemory_HH
#define MpsFullMemory_HH

#include <iostream>
#include <string>

#include "UtlPack.hh"


class MpsFullMemory {

public:
  MpsFullMemory();

  bool full(unsigned x, unsigned y, unsigned t) const;

  void setFull(const MpsSensor1BunchTrainData &d);

  double efficiency(unsigned t) const;

  unsigned fullNumber(unsigned t) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned short _fullTime[4][168];
};


#ifdef CALICE_DAQ_ICC


MpsFullMemory::MpsFullMemory() {
  memset(this,0x00,sizeof(MpsFullMemory));
}

bool MpsFullMemory::full(unsigned x, unsigned y, unsigned t) const {
  assert(x<168);
  assert(y<168);
  assert(t<8192);
  return t>=_fullTime[x/42][y];
}

void MpsFullMemory::setFull(const MpsSensor1BunchTrainData &d) {
  for(unsigned r(0);r<4;r++) {
    for(unsigned y(0);y<168;y++) _fullTime[r][y]=8192;

    unsigned row(999);
    unsigned nRow(0);
    unsigned ts(0);

    const MpsSensor1BunchTrainDatum *p(d.regionData(r));

    for(unsigned j(0);j<d.numberOfRegionHits(r);j++) {
      if(p[j].row()<168) {
	if(p[j].row()==row) {
	  nRow++;
	  if(nRow>=19) _fullTime[r][row]=ts;
	  if(nRow>19) std::cerr << "MpsFullMemory::setFull()  ERROR region "
				<< r << " row " << row << " number of hits " 
				<< nRow << std::endl;

	} else {
	  row=p[j].row();
	  nRow=1;
	  ts=p[j].timeStamp();
	}

      } else {
	//std::cerr << "MpsFullMemory::setFull()  ERROR bad row = " 
	//  << p[j].row() << std::endl;
      }
    }
  }
}
			  
double MpsFullMemory::efficiency(unsigned t) const {
  unsigned n(0);
  for(unsigned y(0);y<168;y++) {
    for(unsigned r(0);r<4;r++) {
      if(t<_fullTime[r][y]) n++;
    }
  }
  return n/(4.0*168.0);
}
			  
unsigned MpsFullMemory::fullNumber(unsigned t) const {
  unsigned n(0);
  for(unsigned y(0);y<168;y++) {
    for(unsigned x(0);x<168;x++) {
      if(full(x,y,t)) n++;
    }
  }
  return n;
}

std::ostream& MpsFullMemory::print(std::ostream &o, std::string s) const {
  o << s << "MpsFullMemory::print()" << std::endl;

  for(unsigned y(0);y<168;y++) {
    o << s << "  Row " << std::setw(3) << y << ", region full times ";
    for(unsigned r(0);r<4;r++) o << std::setw(8) << _fullTime[r][y];
    o << std::endl;
  }

  return o;
}

#endif
#endif
