#ifndef MpsAlignment_HH
#define MpsAlignment_HH

#include <iostream>
#include <string>

#include "MpsAlignmentSensor.hh"


class MpsAlignment {

public:
  MpsAlignment();

  unsigned numberOfSensors() const;
  void     numberOfSensors(unsigned n);

  const MpsAlignmentSensor& sensor(unsigned s) const;
  void                      sensor(unsigned s, const MpsAlignmentSensor &a);

  bool  readFile(const std::string &f);
  bool writeFile(const std::string &f) const;

  bool  readRun(unsigned r, const std::string &s="data/aln/");
  bool writeRun(unsigned r, const std::string &s="") const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  unsigned _numberOfSensors;
  MpsAlignmentSensor _sensor[8];
};


#ifdef CALICE_DAQ_ICC


MpsAlignment::MpsAlignment() {
  _numberOfSensors=0;
}

unsigned MpsAlignment::numberOfSensors() const {
  return _numberOfSensors;
}

void MpsAlignment::numberOfSensors(unsigned n) {
  assert(n<8);
  _numberOfSensors=n;
}

const MpsAlignmentSensor& MpsAlignment::sensor(unsigned s) const {
  assert(s<_numberOfSensors);
  return _sensor[s];
}

void MpsAlignment::sensor(unsigned s, const MpsAlignmentSensor &a) {
  assert(s<_numberOfSensors);
  _sensor[s]=a;
}

bool MpsAlignment::readFile(const std::string &f) {
  std::cout << "MpsAlignment::readFile()  Reading " << f << std::endl;

  std::ifstream fin(f.c_str());
  if(!fin) return false;

  fin >> _numberOfSensors;
  if(_numberOfSensors>=8) {
    _numberOfSensors=0;
    return false;
  }

  for(unsigned i(0);i<_numberOfSensors;i++) {
    if(!_sensor[i].read(fin)) return false;
  }

  return fin;
}

bool MpsAlignment::writeFile(const std::string &f) const {
  std::cout << "MpsAlignment::writeFile()  Writing " << f << std::endl;

  std::ofstream fout(f.c_str());
  if(!fout) return false;

  fout << _numberOfSensors << std::endl;

  for(unsigned i(0);i<_numberOfSensors;i++) {
    if(!_sensor[i].write(fout)) return false;
  }

  return fout;
}

bool MpsAlignment::readRun(unsigned r, const std::string &s) {
  std::ostringstream sout;
  sout << s << "Run" << std::setw(6) << std::setfill('0') << r << ".aln";
  return readFile(sout.str());
}

bool MpsAlignment::writeRun(unsigned r, const std::string &s) const {
  std::ostringstream sout;
  sout << s << "Run" << std::setw(6) << std::setfill('0') << r << ".aln";
  return writeFile(sout.str());
}

std::ostream& MpsAlignment::print(std::ostream &o, std::string s) const {
  o << s << "MpsAlignment::print()" << std::endl;

  o << s << " Number of sensors = " << _numberOfSensors << std::endl;
  for(unsigned i(0);i<_numberOfSensors;i++) {
    o << s << "  Layer " << i << std::endl;
    _sensor[i].print(o,s+"   ");
  }

  return o;
}

#endif
#endif
