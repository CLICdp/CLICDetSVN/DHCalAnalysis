#ifndef MpsTrack_HH
#define MpsTrack_HH

#include <iostream>
#include <string>

#include "MpsTrack1D.hh"


class MpsTrack {

public:
  MpsTrack();

  const MpsTrack1D& xTrack() const;
  void              xTrack(const MpsTrack1D &t);

  const MpsTrack1D& yTrack() const;
  void              yTrack(const MpsTrack1D &t);

  std::pair<double, double> projection(double z) const;
  std::pair<double, double> projectionError(double z) const;

  unsigned totalNumberOfDof() const;
  double totalChiSquared() const;

  //double probability() const;
  //void   probability(double p);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  MpsTrack1D _xTrack;
  MpsTrack1D _yTrack;
};


#ifdef CALICE_DAQ_ICC

#include <cstring>

#include "UtlPrintHex.hh"


MpsTrack::MpsTrack() {
  memset(this,0,sizeof(MpsTrack));
}

const MpsTrack1D& MpsTrack::xTrack() const {
  return _xTrack;
}

void MpsTrack::xTrack(const MpsTrack1D &t) {
  _xTrack=t;
}

const MpsTrack1D& MpsTrack::yTrack() const {
  return _yTrack;
}

void MpsTrack::yTrack(const MpsTrack1D &t) {
  _yTrack=t;
}

std::pair<double, double> MpsTrack::projection(double z) const {
  return std::pair<double, double>(_xTrack.projection(z),_yTrack.projection(z));
}

std::pair<double, double> MpsTrack::projectionError(double z) const {
  return std::pair<double, double>(_xTrack.projectionError(z),_yTrack.projectionError(z));
}

unsigned MpsTrack::totalNumberOfDof() const {
  return _xTrack.numberOfDof()+_yTrack.numberOfDof();
}

double MpsTrack::totalChiSquared() const {
  return _xTrack.chiSquared()+_yTrack.chiSquared();
}

std::ostream& MpsTrack::print(std::ostream &o, std::string s) const {
  o << s << "MpsTrack::print()" << std::endl;

  o << s << " X track" << std::endl;
  _xTrack.print(o,s+" ");
  o << s << " Y track" << std::endl;
  _yTrack.print(o,s+" ");

  o << s << " Total chi-squared/DOF = " << totalChiSquared()
    << "/" << totalNumberOfDof() << std::endl;

  return o;
}

#endif
#endif
