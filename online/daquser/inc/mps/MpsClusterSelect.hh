// Just add this to the end of the public section of any MpsAnalysis.hh file (e.g. MpsAnalysisBeamThresholdScan.hh).
// Designed to take as input a vector of MpsSensor1Hits and return as output a vector containing only those hits
// in the input vector which have at least one adjacent hit also in the input vector. 

std::vector<MpsSensor1Hit> ClusterSelect(std::vector<MpsSensor1Hit> hitVec){
  std::cout << "Using ClusterSelect" << std::endl;
  //first register all the pixel that have been hit in this bunchtrain and layer in a 2D array (note:this assumes a 168x168 sensor)
  bool LocArray[168][168];
  
  for(int i(0);i<168;i++){
    for(int j(0);j<168;j++){
      LocArray[i][j]=0;
    }
  }
  for(unsigned i(0);i<hitVec.size();i = i+1){
    int x(hitVec[i].pixelX());
    int y(hitVec[i].pixelY());
    LocArray[x][y]=1;
  }
  //create an output vector
  std::vector<MpsSensor1Hit> outVec;
  
  //now cycle over the all the hits again to see if they have any neighbours
  for(unsigned i(0);i<hitVec.size();i = i+1){
    int x(hitVec[i].pixelX());
    int y(hitVec[i].pixelY());
    int n(0);
    n = LocArray[x+1][y+1] + LocArray[x+1][y] + LocArray[x+1][y-1] + LocArray[x][y+1] + LocArray[x][y-1] + LocArray[x-1][y+1] + LocArray[x-1][y] + LocArray[x-1][y-1];
    //if it has neighbours, record the hit in the output vector
    if(n>0){
      outVec.push_back (hitVec[i]);
    }
  }
  
  //and output
  return outVec;
}

