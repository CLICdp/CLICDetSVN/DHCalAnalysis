#ifndef MpsTrack1D_HH
#define MpsTrack1D_HH

#include <iostream>
#include <string>

#include "UtlPack.hh"


class MpsTrack1D {

public:
  MpsTrack1D();

  double intercept() const;
  void   intercept(double i);

  double gradient() const;
  void   gradient(double g);

  double error(unsigned i) const;
  void   error(unsigned i, double e);

  unsigned numberOfDof() const;
  void     numberOfDof(unsigned n);

  double chiSquared() const;
  void   chiSquared(double c);

  //double probability() const;
  //void   probability(double p);

  double projection(double z) const;
  double projectionError(double z) const;

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:
  double _intercept;
  double _gradient;

  double _error[3];

  unsigned _numberOfDof;
  double _chiSquared;
};


#ifdef CALICE_DAQ_ICC


MpsTrack1D::MpsTrack1D() {
}

double MpsTrack1D::intercept() const {
  return _intercept;
}

void MpsTrack1D::intercept(double i) {
  _intercept=i;
}

double MpsTrack1D::gradient() const {
  return _gradient;
}

void MpsTrack1D::gradient(double g) {
  _gradient=g;
}

double MpsTrack1D::error(unsigned i) const {
  assert(i<3);
  return _error[i];
}

void MpsTrack1D::error(unsigned i, double e) {
  assert(i<3);
  _error[i]=e;
}

unsigned MpsTrack1D::numberOfDof() const {
  return _numberOfDof;
}

void MpsTrack1D::numberOfDof(unsigned n) {
  _numberOfDof=n;
}

double MpsTrack1D::chiSquared() const {
  return _chiSquared;
}

void MpsTrack1D::chiSquared(double c) {
  _chiSquared=c;
}

double MpsTrack1D::projection(double z) const {
  return _intercept+z*_gradient;
}

double MpsTrack1D::projectionError(double z) const {
  return sqrt(_error[0]+2.0*z*_error[1]+z*z*_error[2]);
}

std::ostream& MpsTrack1D::print(std::ostream &o, std::string s) const {
  o << s << "MpsTrack1D::print()" << std::endl;

  o << s << " Track = (" << _intercept << " +/- " << sqrt(_error[0])
    << ") + z * (" << _gradient << " +/- " << sqrt(_error[2]) << ")" << std::endl;
  o << s << " Error matrix = " << std::setw(12) << _error[0]
    << " " << std::setw(12) << _error[1] << std::endl;
  o << s << "                " << std::setw(12) << _error[1]
    << " " << std::setw(12) << _error[2] << std::endl;

  o << s << " Chi-squared/DOF = " << _chiSquared << "/"
    << _numberOfDof << std::endl;

  return o;
}

#endif
#endif
