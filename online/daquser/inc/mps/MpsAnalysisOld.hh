#ifndef MpsAnalysis_HH
#define MpsAnalysis_HH

#include <cassert>

#include "RcdUserRO.hh"
#include "SubAccessor.hh"


class MpsAnalysis : public RcdUserRO {

public:
  MpsAnalysis() {
  }

  virtual ~MpsAnalysis() {
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "MpsAnalysis::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    SubAccessor accessor(r);

    switch (r.recordType()) {
  
    case RcdHeader::runStart: {

      // Get list of IlcRunStart subrecords
      // There should only be one and it should only
      // be in the runStart record
      std::vector<const IlcRunStart*>
        vs(accessor.access<IlcRunStart>());
      assert(vs.size()==1);
      
      for(unsigned i(0);i<vs.size();i++) {
        vs[i]->print(std::cout) << std::endl;
      }
      break;
    }
 
    case RcdHeader::runEnd: {
      // Get list of IlcRunEnd subrecords
      // There should only be one and it should only
      // be in the runEnd record
      std::vector<const IlcRunEnd*>
        ve(accessor.access<IlcRunEnd>());
      assert(ve.size()==1);

      for(unsigned i(0);i<ve.size();i++) {
        ve[i]->print(std::cout) << std::endl;
      }
      break;
    }
  
    case RcdHeader::configurationStart: {
      break;
    }
  
    case RcdHeader::bunchTrain: {
      std::vector<const MpsLocationData<MpsSensor1BunchTrainData>* >
	v(accessor.access< MpsLocationData<MpsSensor1BunchTrainData> >());
      
      assert(v.size()<=1);

      unsigned nMpsBunchTrain(0);

      for(unsigned i(0);i<v.size();i++) {
	nMpsBunchTrain++;

	std::vector<MpsSensor1Hit> vh(v[i]->data()->hitVector());
	for(unsigned j(0);j<vh.size();j++) {
	  for(unsigned k(0);k<4;k++) {
	    //_pShm->_sensor[k].fill(vh[j].pixelX(),vh[j].pixelY());
	  }
	}
      }
      break;
    }
  
    default: {
      break;
    }
    };

    return true;
  }

private:
};

#endif
