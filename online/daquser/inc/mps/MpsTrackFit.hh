#ifndef MpsTrackFit_HH
#define MpsTrackFit_HH

#include <iostream>
#include <string>
#include <vector>

#include "MpsTrack.hh"
#include "MpsTrackHit.hh"


class MpsTrackFit {

public:
  MpsTrackFit();

  MpsTrack fit(std::vector<MpsTrackHit> &v);

  std::ostream& print(std::ostream &o=std::cout, std::string s="") const;


private:

};


#ifdef CALICE_DAQ_ICC


MpsTrackFit::MpsTrackFit() {
}

MpsTrack MpsTrackFit::fit(std::vector<MpsTrackHit> &v) {
  MpsTrack track;
  if(v.size()<2) return track;

  // Special case
  if(v.size()==2) {
    double z[2]={v[0].z(),v[1].z()};

    for(unsigned xy(0);xy<2;xy++) {
      double a[2];
      if(xy==0) {
	a[0]=v[0].x();
	a[1]=v[1].x();
      } else {
	a[0]=v[0].y();
	a[1]=v[1].y();
      }

      double dz(z[1]-z[0]);

      MpsTrack1D t1d;
      t1d.intercept((a[0]*z[1]-a[1]*z[0])/dz);
      t1d.gradient((a[1]-a[0])/dz);
    
      double dz2(dz*dz);
      double sigma2[2];
      if(xy==0) {
	sigma2[0]=v[0].xError()*v[0].xError();
	sigma2[1]=v[1].xError()*v[1].xError();
      } else {
	sigma2[0]=v[0].yError()*v[0].yError();
	sigma2[1]=v[1].yError()*v[1].yError();
      }

      t1d.error(0, (sigma2[0]*z[1]*z[1]+sigma2[1]*z[0]*z[0])/dz2);
      t1d.error(1,-(sigma2[0]*z[1]     +sigma2[1]*z[0]     )/dz2);
      t1d.error(2, (sigma2[0]          +sigma2[1]          )/dz2);
      
      t1d.numberOfDof(0);
      t1d.chiSquared(0.0);
      //t1d.print();
    
      if(xy==0) track.xTrack(t1d);
      else      track.yTrack(t1d);
    }

    return track;
  }

  for(unsigned xy(0);xy<2;xy++) {
    double s[3]={0.0,0.0,0.0};
    double t[3]={0.0,0.0,0.0};

    if(xy==0) {
      for(unsigned i(0);i<v.size();i++) {
	s[0]+=              1.0/(v[i].xError()*v[i].xError());
	s[1]+=         v[i].z()/(v[i].xError()*v[i].xError());
	s[2]+=v[i].z()*v[i].z()/(v[i].xError()*v[i].xError());
	
	t[0]+=         v[i].x()/(v[i].xError()*v[i].xError());
	t[1]+=v[i].x()*v[i].z()/(v[i].xError()*v[i].xError());
	t[2]+=v[i].x()*v[i].x()/(v[i].xError()*v[i].xError());
      }
    } else {
      for(unsigned i(0);i<v.size();i++) {
	s[0]+=              1.0/(v[i].yError()*v[i].yError());
	s[1]+=         v[i].z()/(v[i].yError()*v[i].yError());
	s[2]+=v[i].z()*v[i].z()/(v[i].yError()*v[i].yError());
	
	t[0]+=         v[i].y()/(v[i].yError()*v[i].yError());
	t[1]+=v[i].y()*v[i].z()/(v[i].yError()*v[i].yError());
	t[2]+=v[i].y()*v[i].y()/(v[i].yError()*v[i].yError());
      }
    }

    double det(s[0]*s[2]-s[1]*s[1]);
    if(det==0.0) return track;

    double e[3]={s[2]/det,-s[1]/det,s[0]/det};
    assert(e[0]>=0.0);
    assert(e[2]>=0.0);

    /*
      std::cout << "Unit = " << s[0]*e[0]+s[1]*e[1] << " "
      << s[1]*e[0]+s[2]*e[1] << std::endl;
      std::cout << "       " << s[0]*e[1]+s[1]*e[2] << " "
      << s[1]*e[1]+s[2]*e[2] << std::endl;
    */

    double p[2]={e[0]*t[0]+e[1]*t[1],e[1]*t[0]+e[2]*t[1]};

    double chiSq(t[2]-t[0]*p[0]-t[1]*p[1]);
    //assert(chiSq>=0.0);
    if(chiSq<0.0) {
      std::cerr << "MpsTrackFit::fit() ERROR Chi-sq = " << chiSq << " < 0.0" << std::endl;
      chiSq=0.0;
    }

    MpsTrack1D t1d;
    t1d.intercept(p[0]);
    t1d.gradient(p[1]);
    
    t1d.error(0,e[0]);
    t1d.error(1,e[1]);
    t1d.error(2,e[2]);
    
    t1d.numberOfDof(v.size()-2);
    t1d.chiSquared(chiSq);
    //t1d.print();
    
    if(xy==0) track.xTrack(t1d);
    else      track.yTrack(t1d);
  }

  return track;
}

std::ostream& MpsTrackFit::print(std::ostream &o, std::string s) const {
  o << s << "MpsTrackFit::print()" << std::endl;

  return o;
}

#endif
#endif
