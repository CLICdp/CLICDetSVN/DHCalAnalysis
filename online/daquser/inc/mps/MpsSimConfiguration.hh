#ifndef MpsSimConfiguration_HH
#define MpsSimConfiguration_HH

// Sets up geometry and beam for TPAC beam test simulation

#include <vector>
#include <fstream>
#include <iostream>

#include "RcdUserRW.hh"

#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "MpsAlignment.hh"


class MpsSimConfiguration : public RcdUserRW {

public:
  MpsSimConfiguration() {
  }

  virtual ~MpsSimConfiguration() {
  }

  bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "MpsSimConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    SubInserter inserter(r);

    // Check record type
    switch (r.recordType()) {

    case RcdHeader::runStart: {
      SubAccessor accessor(r);

      // Get run start information
      std::vector<const IlcRunStart*>
	v(accessor.access<IlcRunStart>());
      assert(v.size()==1);
      _runStart=*(v[0]);

      // Check run type is correct for this module
      if(_runStart.runType().majorType()==IlcRunType::mps) {
	SubInserter inserter(r);

	// Set up the geometrical sensor positions
	MpsAlignment ma;

	// CERN Aug 2009 beam test
#ifdef MPS_CERN09

	// Slots used in stack
	int slot[6]={14,13,11,7,5,4};
	ma.numberOfSensors(6);

	for(unsigned l(0);l<6;l++) {
	  MpsAlignmentSensor mas;
	  mas.layer(l);

	  // Set up orientation
	  if(l==0 || l==4) mas.orientation(6);
	  if(l==1 || l==5) mas.orientation(2);
	  if(l==2 || l==3) mas.orientation(5);

	  // z definition has first used slot centre at z=0
	  if(mas.frontFacing()) mas.z(6.0*(slot[0]-slot[l])-0.9);
	  else                  mas.z(6.0*(slot[0]-slot[l])+0.9);

	  // Add misalignments in x, y and time
	  mas.xDelta(0.01*l);
	  mas.yDelta(0.02*l);
	  mas.angle( 0.001*l);
	  mas.tDelta(1.5);

	  // Store result in local store and in runStart record
	  ma.sensor(l,mas);
	  inserter.insert<MpsAlignmentSensor>(mas);
	}
#endif

	// DESY Mar 2010 beam test
#ifdef MPS_DESY10

	// Slots used in stack
	int slot[6]={19,18,16,7,5,4};
	ma.numberOfSensors(6);

	for(unsigned l(0);l<6;l++) {
	  MpsAlignmentSensor mas;
	  mas.layer(l);

	  // Set up orientation
	  if(l==0 || l==4) mas.orientation(6);
	  if(l==1 || l==5) mas.orientation(2);
	  if(l==2 || l==3) mas.orientation(5);

	  // z definition has first used slot centre at z=0
	  if(mas.frontFacing()) mas.z(6.0*(slot[0]-slot[l])-0.9);
	  else                  mas.z(6.0*(slot[0]-slot[l])+0.9);

	  // Add misalignments in x, y and time
	  mas.xDelta(0.01*l);
	  mas.yDelta(0.02*l);
	  mas.angle( 0.001*l);
	  mas.tDelta(1.5);

	  // Store result in local store and in runStart record
	  ma.sensor(l,mas);
	  inserter.insert<MpsAlignmentSensor>(mas);
	}
#endif

	if(doPrint(r.recordType(),1)) ma.print() << std::endl;


	// Now set up the simulation material volumes
	SimDetectorBox box;
	SimDetectorData *d(0);

	// CERN Aug 2009 beam test
#ifdef MPS_CERN09

	// Three trigger scintillators

	// Size and position of each scintillator
	box.xCentre(0.0);
	box.xWidth(10.0);
	box.yCentre(0.0);
	box.yWidth(10.0);
	box.zThickness(6.0);
	box.radiationLength(0);
	box.type(SimDetectorBoxId::triggerScintillator);
	box.material(SimDetectorBox::scintillator);

	// Define overall volume for front two scintillators
	d=inserter.insert<SimDetectorData>(true);
	d->type(SimDetectorData::triggerVolume);
	d->layer(0);
	d->material(SimDetectorBox::air);

	// Place front scintillator
	box.zCentre(-94.5);
	box.layer(0);
	d->addBox(box);

	// Place second scintillator
	box.zCentre(-86.5);
	box.layer(1);
	d->addBox(box);

	// Adjust overall volume to correct envelope
	d->resize();
	d->radiationLength(0);
	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

	// Define overall volume for rear scintillator
	d=inserter.insert<SimDetectorData>(true);
	d->type(SimDetectorData::triggerVolume);
	d->layer(1);
	d->material(SimDetectorBox::air);

	// Place rear scintillator
	box.xCentre(-1.0);
	box.zCentre(96.5);
	box.layer(2);
	d->addBox(box);

	// Adjust overall volume to correct envelope
	d->resize();
	d->radiationLength(0);
	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;
#endif

	// ECAL sensors, PCBs and tungsten

	// Define overall volume
	d=inserter.insert<SimDetectorData>(true);
	d->type(SimDetectorData::ecalVolume);
	d->layer(0);
	d->material(SimDetectorBox::air);

	// Add in sensor layers and their material

	double z(0.0);

	for(unsigned l(0);l<6;l++) {
	  MpsAlignmentSensor mas;
	  mas=ma.sensor(l);

	  double sign(-1.0);
	  if(mas.frontFacing()) sign=1.0;

	  z=mas.z();

	  // Epitaxial layer of sensor
	  box.xCentre(0.0);
	  box.xWidth(12.0);
	  box.yCentre(0.0);
	  box.yWidth(12.0);
	  box.zThickness(0.012);
	  box.zCentre(z+sign*box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiEpitaxial);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);
	  
	  z+=sign*box.zThickness();

	  // Bulk silicon of sensor
	  box.xCentre(0.0);
	  box.xWidth(12.0);
	  box.yCentre(0.0);
	  box.yWidth(12.0);
	  box.zThickness(0.400-0.012);
	  box.zCentre(z+sign*box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiBulk);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);
	  
	  z+=sign*box.zThickness();

	  // PCB supporting sensor; needs hole 8x8mm2 under sensor
	  box.xCentre(0.0);
	  box.xWidth(100.0);
	  box.yCentre(0.0);
	  box.yWidth(100.0);
	  box.zThickness(1.0);
	  box.zCentre(z+sign*box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalPcb);
	  box.layer(l);
	  box.material(SimDetectorBox::pcb);
	  d->addBox(box);
	}

	// CERN Aug 2009 beam test
#ifdef MPS_CERN09
#ifdef MPS_CONVERTER

	// Tungsten stack of plates, total 30mm thick
	box.xCentre(0.0);
	box.xWidth(80.0);
	box.yCentre(0.0);
	box.yWidth(80.0);
	box.zThickness(30.0);
	box.zCentre(-27.0);
	box.radiationLength(box.zThickness()/3.5);
	box.type(SimDetectorBoxId::ecalConverter);
	box.layer(0);
	box.material(SimDetectorBox::tungsten);
	d->addBox(box);
#endif
#endif

	// Adjust overall volume to correct envelope
	d->resize();
	d->radiationLength(0);

	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;


	// DESY Mar 2010 beam test
#ifdef MPS_DESY10

	// EUDET telescope

	// Define overall volume
	d=inserter.insert<SimDetectorData>(true);
	d->type(SimDetectorData::trackerVolume);
	d->layer(0);
	d->material(SimDetectorBox::air);

	// Size of the EUDET silicon strip detectors
	box.xCentre(0.0);
	box.xWidth(100.0);
	box.yCentre(0.0);
	box.yWidth(100.0);
	box.zThickness(0.5);
	box.radiationLength(0);
	box.type(SimDetectorBoxId::trackerSiSensor);
	box.material(SimDetectorBox::silicon);

	// Place each layer; needs correcting when geometry known
	for(unsigned l(0);l<6;l++) {
	  box.layer(l);

	  if(l==0) box.zCentre(-5000.0);
	  if(l==1) box.zCentre(-4500.0);
	  if(l==2) box.zCentre(-4000.0);
	  if(l==3) box.zCentre(-3000.0);
	  if(l==4) box.zCentre(-2500.0);
	  if(l==5) box.zCentre(-2000.0);

	  d->addBox(box);
	}

	// Size of EUDET trigger scintillators
	box.xCentre(0.0);
	box.xWidth(100.0);
	box.yCentre(0.0);
	box.yWidth(100.0);
	box.zThickness(10.0);
	box.radiationLength(0);
	box.type(SimDetectorBoxId::trackerScintillator);
	box.material(SimDetectorBox::scintillator);

	// Place two scintillators near centre of telescope
	box.layer(0);
	box.zCentre(-3500.0-0.5*box.zThickness()-0.1);
	d->addBox(box);

	box.layer(1);
	box.zCentre(-3500.0+0.5*box.zThickness()+0.1);
	d->addBox(box);

	// Adjust overall volume to correct envelope
	d->resize();
	d->radiationLength(0);

	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;
#endif
	

	// Set up the parameters for the primary (beam) particle
	SimPrimaryData *p(inserter.insert<SimPrimaryData>(true));
	  
	// Only allow mpsBeam types
	if(_runStart.runType().type()==IlcRunType::mpsBeam) {

	  // Set particle type using lower 3 bits of version

	  // e+
	  if((_runStart.runType().version()%8)==0) {
	    p->particleId(-11);
	    p->mass(0.51099906);
	  }

	  // e-
	  if((_runStart.runType().version()%8)==1) {
	    p->particleId( 11);
	    p->mass(0.51099906);
	  }

	  // mu+
	  if((_runStart.runType().version()%8)==2) {
	    p->particleId(-13);
	    p->mass(105.6584);
	  }

	  // mu-
	  if((_runStart.runType().version()%8)==3) {
	    p->particleId( 13);
	    p->mass(105.6584);
	  }
	  
	  // pi+
	  if((_runStart.runType().version()%8)==4) {
	    p->particleId( 211);
	    p->mass(139.5700);
	  }
	  
	  // pi-
	  if((_runStart.runType().version()%8)==5) {
	    p->particleId(-211);
	    p->mass(139.5700);
	  }

	  // p+
	  if((_runStart.runType().version()%8)==6) {
	    p->particleId( 2212);
	    p->mass(938.27231);
	  }
	  
	  // p-
	  if((_runStart.runType().version()%8)==7) {
	    p->particleId(-2212);
	    p->mass(938.27231);
	  }
	  
	  // CERN Aug 2009 beam test
#ifdef MPS_CERN09

	  // Particle starting position and beam size
	  double zStart=-10000.0;

	  p->x(0.0);
	  p->y(0.0);
	  p->z(zStart);
	  
	  p->smearMatrix(0,0,5.0);
	  p->smearMatrix(1,1,5.0);

	  // Particle momentum, pz set by upper 5 bits of version x 5GeV
	  p->px(0.0);
	  p->py(0.0);
	  p->pz(5000.0*(_runStart.runType().version()/8));
#endif
	  
	  // DESY Mar 2010 beam test
#ifdef MPS_DESY10

	  // Particle starting position and beam size
	  double zStart=-10000.0;

	  p->x(0.0);
	  p->y(0.0);
	  p->z(zStart);
	  
	  p->smearMatrix(0,0,5.0);
	  p->smearMatrix(1,1,5.0);

	  // Particle momentum, pz set by upper 5 bits of version x 0.25GeV
	  p->px(0.0);
	  p->py(0.0);
	  p->pz(250.0*(_runStart.runType().version()/8));
#endif
	  
	  if(doPrint(r.recordType(),1)) p->print(std::cout," ") << std::endl;
	  
	} else {

	  // Don't allow any mps run types except mpsBeam
	  assert(false);
	}
      }

      break;
    }
      
    case RcdHeader::configurationStart: {
     
      if(_runStart.runType().majorType()==IlcRunType::mps) {
      }

      break;
    }
      
    default: {
      break;
    }
    };

    return true;
  }

private:
  IlcRunStart _runStart;
};

#endif
