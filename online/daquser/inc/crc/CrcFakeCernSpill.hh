#ifndef CrcFakeCernSpill_HH
#define CrcFakeCernSpill_HH

#include <iostream>

#include "UtlPrintHex.hh"


class CrcFakeCernSpill {

public:
  enum {
    shmKey  =0x00654545,
  };
  
  CrcFakeCernSpill() {
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "CrcFakeCernSpill::print()" << std::endl;
    return o;
  }

  bool _level;
  bool _invert;

private:
};

#endif
