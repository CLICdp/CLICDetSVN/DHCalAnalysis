#ifndef CrcReadout_HH
#define CrcReadout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "DaqRunStart.hh"
#include "CrcVlinkEventData.hh"
#include "SubAccessor.hh"
#include "SubInserter.hh"


class CrcReadout : public RcdUserRW {

public:
  CrcReadout(unsigned pci, unsigned char c) : _location(c,0,0,0) {
  }

  virtual ~CrcReadout() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << std::endl << "CrcReadout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ");
    }

    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level 
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
      if(v.size()==1)  _printLevel=v[0]->runType().printLevel();
    }

    if(r.recordType()==RcdHeader::configurationStart) {
      SubAccessor accessor(r);

      {std::vector<const CrcReadoutConfigurationData*>
        v(accessor.extract<CrcReadoutConfigurationData>());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of CrcReadoutConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}

      {std::vector<const TrgReadoutConfigurationData*>
        v(accessor.extract<TrgReadoutConfigurationData>());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of TrgReadoutConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}

      {std::vector<const CrcLocationData<CrcBeConfigurationData>*>
        v(accessor.extract< CrcLocationData<CrcBeConfigurationData> >());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of CrcBeConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}

      {std::vector<const CrcLocationData<CrcBeTrgConfigurationData>*>
        v(accessor.extract< CrcLocationData<CrcBeTrgConfigurationData> >());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of CrcBeTrgConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}

      {std::vector<const CrcLocationData<CrcFeConfigurationData>*>
        v(accessor.extract< CrcLocationData<CrcFeConfigurationData> >());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of CrcFeConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}
    }


    if(r.recordType()==RcdHeader::slowReadout) {

      // Add in some dummy data
      SubInserter inserter(r);
      inserter.insert< CrcLocationData<CrcLm82SlowReadoutData> >(true);
    }

    if(r.recordType()==RcdHeader::event) {

      // Add in some dummy data
      SubInserter inserter(r);
      inserter.insert< CrcLocationData<CrcVlinkEventData> >();
      inserter.extend(24*1024);
    }

    return true;
  }
protected:
  CrcLocation _location;

};

#endif
