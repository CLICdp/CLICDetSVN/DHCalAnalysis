#ifndef EmcEventAdc_HH
#define EmcEventAdc_HH

#include <cstdlib>

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "UtlPrintHex.hh"
#include "EmcPhysicalPad.hh"


class EmcEventAdc {

public:
  EmcEventAdc() {
  }

  void reset() {
    for(unsigned z(0);z<30;z++) {
      for(unsigned y(0);y<18;y++) {
	for(unsigned x(0);x<18;x++) {
	  _adc[18*(18*z+y)+x]=-32768;
	}
      }
    }
  }

  short int adc(EmcPhysicalPad p) const {
    assert(p.value()<18*18*30);
    return _adc[p.value()];
  }
  
  void adc(EmcPhysicalPad p, short int a) {
    assert(p.value()<18*18*30);
    _adc[p.value()]=a;
  }
  
  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "EmcEventAdc::print()" << std::endl;

    for(unsigned z(0);z<30;z++) {
      for(unsigned y(0);y<18;y++) {
	for(unsigned x(0);x<18;x++) {
	  if(_adc[18*(18*z+y)+x]!=-32768) {
	    o << s << " X,Y,Z " << std::setw(3) << x << std::setw(3) << y << std::setw(3) << z 
	      << std::setw(8) << _adc[18*(18*z+y)+x] << std::endl;
	  }
	}
      }
    }

    return o;
  }

private:
  short int _adc[18*18*30];
};

#endif
