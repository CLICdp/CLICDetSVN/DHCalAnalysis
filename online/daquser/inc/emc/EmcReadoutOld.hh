#ifndef EmcReadout_HH
#define EmcReadout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"


class EmcReadout : public RcdUserRW {

public:
  EmcReadout() {
  }

  virtual ~EmcReadout() {
  }

  virtual bool record(RcdRecord &r) {
    return true;
  }
};

#endif
