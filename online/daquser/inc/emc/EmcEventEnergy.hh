#ifndef EmcEventEnergy_HH
#define EmcEventEnergy_HH

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "UtlPrintHex.hh"
#include "EmcPhysicalPad.hh"


class EmcEventEnergy {

public:
  EmcEventEnergy() {
  }

  void addEnergy(EmcPhysicalPad p, double e) {
    assert(p.value()<18*18*30);
    _energy[p.value()]+=e;
  }

  double energy(EmcPhysicalPad p) const {
    assert(p.value()<18*18*30);
    return _energy[p.value()];
  }
  
  void energy(EmcPhysicalPad p, double e) {
    assert(p.value()<18*18*30);
    _energy[p.value()]=e;
  }
  
  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "EmcEventEnergy::print()" << std::endl;

    for(unsigned z(0);z<30;z++) {
      for(unsigned y(0);y<18;y++) {
	for(unsigned x(0);x<18;x++) {
	  o << s << " X,Y,Z " << std::setw(3) << x << std::setw(3) << y << std::setw(3) << z 
	    << std::setw(12) << _energy[18*(18*z+y)+x] << " MeV" << std::endl;
	}
      }
    }

    return o;
  }

private:
  double _energy[18*18*30];
};

#endif
