#ifndef EmcPhysicalPad_HH
#define EmcPhysicalPad_HH

#include <iostream>
#include <fstream>
#include <string>


class EmcPhysicalPad {

public:
  EmcPhysicalPad() : _value(0xffff) {
  }

  EmcPhysicalPad(unsigned short v) : _value(v) {
  }

  EmcPhysicalPad(unsigned h, unsigned v, unsigned l) : _value(18*(18*l+v)+h) {
    assert(h<18 && v<18 && l<30);
  }

  unsigned short value() const {
    return _value;
  }

  void value(unsigned short v) {
    _value=v;
  }

  unsigned horizontal() const {
    return _value%18;
  }

  unsigned vertical() const {
    return (_value/18)%18;
  }

  unsigned layer() const {
    return _value/(18*18);
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "EmcPhysicalPad::print()" << std::endl;
    o << s << " Value = " << printHex(_value) << std::endl;
    o << s << "  Hoizontal (x) = " << std::setw(2) << horizontal() << std::endl;
    o << s << "  Vertical (y)  = " << std::setw(2) << vertical() << std::endl;
    o << s << "  Layer (z)     = " << std::setw(2) << layer() << std::endl;
    return o;
  }

private:
  unsigned short _value;
};

#endif
