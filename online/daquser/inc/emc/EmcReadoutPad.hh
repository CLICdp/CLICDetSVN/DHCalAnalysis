#ifndef EmcReadoutPad_HH
#define EmcReadoutPad_HH

#include <iostream>
#include <fstream>
#include <string>


class EmcReadoutPad {

public:
  EmcReadoutPad() : _value(0xffff) {
  }

  EmcReadoutPad(unsigned short v) : _value(v) {
  }

  EmcReadoutPad(unsigned s, unsigned f, unsigned c, unsigned m) : 
    _value(18*(12*(8*s+f)+c)+m) {
    assert(s<22 && f<8 && c<12 && m<18);
  }

  unsigned short value() const {
    return _value;
  }


  void value(unsigned short v) {
    _value=v;
  }

  unsigned slot() const {
    return _value/(8*12*18);
  }

  unsigned frontend() const {
    return (_value/(12*18))%8;
  }

  unsigned chip() const {
    return (_value/18)%12;
  }

  unsigned channel() const {
    return _value%18;
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    o << s << "EmcReadoutPad::print()" << std::endl;
    o << s << " Value = " << printHex(_value) << std::endl;
    o << s << "  Slot     = " << std::setw(2) << slot() << std::endl;
    o << s << "  Frontend = " << std::setw(2) << frontend() << std::endl;
    o << s << "  Chip     = " << std::setw(2) << chip() << std::endl;
    o << s << "  Channel  = " << std::setw(2) << channel() << std::endl;
    return o;
  }

private:
  unsigned short _value;
};

#endif
