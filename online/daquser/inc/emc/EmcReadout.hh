#ifndef EmcReadout_HH
#define EmcReadout_HH

#include <iostream>
#include <fstream>

#include "CrcReadout.hh"


class EmcReadout : public CrcReadout {

public:
  EmcReadout(unsigned b) : CrcReadout(b,0xec) {
  }

  EmcReadout(unsigned b, unsigned char c) : CrcReadout(b,c) {
  }

  virtual ~EmcReadout() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "EmcReadout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
                                                                                                                             
    SubInserter inserter(r);

    UtlPack tid;
    tid.halfWord(1,SubHeader::emc);
    tid.byte(2,_location.crateNumber());
    DaqTwoTimer *t(inserter.insert<DaqTwoTimer>(true));
    t->timerId(tid.word());

    bool reply(CrcReadout::record(r));

    // Close off overall timer
    t->setEndTime();
    if(doPrint(r.recordType())) t->print(std::cout," ") << std::endl;
    
    return reply;
  }

private:
};

#endif
