#include <iostream>

#include "EmcMap.hh"
#include "EmcCalibration.hh"

using namespace std;

int main() {
  EmcMap m;
  m.print(cout);
  EmcCalibration c;
  c.print(cout);
  return 0;
}
