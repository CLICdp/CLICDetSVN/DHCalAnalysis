#ifndef UtlMinimise_HH
#define UtlMinimise_HH

#include <cassert>
#include <vector>


class UtlMinimise {

public:
  UtlMinimise() {
    _print=true;
  }

  virtual ~UtlMinimise() {
  }

  void initialise(const std::vector<double> &v,
		  const std::vector<double> &s) {
    assert(v.size()==s.size());
    _value=v;
    _step=s;
  }

  void minimise() {
    unsigned nFree(0);
    for(unsigned i(0);i<_step.size();i++) {
      if(_step[i]>0.0) nFree++;
    }

    for(unsigned i(0);i<6*nFree;i++) {
      /*
      if(i>0) {
	for(unsigned j(0);j<_step.size();j++) {
	  _step[j]/=2.0;
	}
      }
      */
      //if(_print) {
	std::cout << "Step size step " << i << std::endl
		  << " Values" << std::endl;
	for(unsigned j(0);j<_value.size();j++) {
	  std::cout << " " << _value[j];
	}
	std::cout << std::endl << " Steps" << std::endl;
	for(unsigned j(0);j<_step.size();j++) {
	  std::cout << " " << _step[j];
	}
	std::cout << std::endl;
	//}

      bool complete(false);
      for(unsigned j(0);j<10000 && !complete;j++) {
	complete=vary();
      } 
      //assert(complete);
      //vary();
    }

    //if(_print) {
      std::cout << "Step size final" << std::endl
		<< " Values" << std::endl;
      for(unsigned j(0);j<_value.size();j++) {
	std::cout << " " << _value[j];
      }
      std::cout << std::endl << " Steps" << std::endl;
      for(unsigned j(0);j<_step.size();j++) {
	std::cout << " " << _step[j];
      }
      std::cout << std::endl;
      //}

    // Final call
    _fcn=fcn(_value,true);
  }

  bool vary() {
    assert(_value.size()==_step.size());
    _fcn=fcn(_value);

    double fBest(_fcn);
    int kBest(0);
    unsigned iBest(0xffffffff);

    double fWorst(2.0*_fcn);
    unsigned iWorst(0xffffffff);

    // Vary each up and down
    for(unsigned i(0);i<_value.size();i++) {
      if(_step[i]>0.0) {
	double fW(0.0);
	for(int k(-1);k<=1;k+=2) {
	  std::vector<double> v(_value);
	  v[i]+=k*_step[i];
	  
	  double f(fcn(v));
	  fW+=f;
	  
	  if(f<fBest) {
	    fBest=f;
	    kBest=k;
	    iBest=i;
	  }
	}
	
	if(fW>fWorst) {
	  fWorst=fW;
	  iWorst=i;
	}
      }
    }

    if(kBest==0) {
      if(_print) {
	std::cout << "vary() found no improvement, fcn = " << _fcn << ", values" << std::endl;
	for(unsigned j(0);j<_value.size();j++) {
	  std::cout << " " << std::setw(12) << _value[j];
	}
	std::cout << std::endl;
	for(unsigned j(0);j<_value.size();j++) {
	  std::cout << " " << std::setw(12) << _step[j];
	}
	std::cout << std::endl;
      }
      
      if(iWorst<_step.size()) {
	_step[iWorst]/=sqrt(10.0);
	if(_print) {
	  std::cout << "Worst variable = " << iWorst << ", step decreased to " << _step[iWorst] << std::endl;
	  std::cout << std::endl;
	}	
      } else {
	if(_print) {
	  std::cout << "No parameter dependence, stopping" << std::endl;
	  std::cout << std::endl;
	}
      }
      return true;
    }

    assert(iBest<_value.size());
    _value[iBest]+=kBest*_step[iBest];
    _fcn=fcn(_value);

    if(_print) {
      std::cout << "vary() found improvement, i,k = " << iBest << ","<< kBest
		<< ", _fcn = " << fBest << ", new values" << std::endl;
      for(unsigned j(0);j<_value.size();j++) {
	std::cout << " " << _value[j];
      }
      std::cout << std::endl;
    }
    return false;
  }

  virtual double fcn(const std::vector<double> &v, bool final=false) {
    double f(0.0);
    for(unsigned i(0);i<v.size();i++) f+=v[i]*v[i];
    return f;
  }

  double function() const {
    return _fcn;
  }


protected:
  bool _print;

  double _fcn;
  std::vector<double> _value;
  std::vector<double> _step;
};

#endif
