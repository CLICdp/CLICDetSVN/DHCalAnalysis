#ifndef UtlLogger_HH
#define UtlLogger_HH

#include <string>
#include <iostream>

class UtlLogger {

public:
  UtlLogger();
  virtual ~UtlLogger();

  std::ostream& outStream();
  void outStream(std::ostream &o);

  std::ostream& errStream();
  void errStream(const std::string &msg);
  void errStream(std::ostream &e);

protected:
  bool _print;
  bool _outErr;
  std::ostream *_outStream;
  std::ostream *_errStream;
};


#ifdef CALICE_DAQ_ICC


UtlLogger::UtlLogger() :  _outErr(true),
			 _outStream(&std::cout), _errStream(&std::cerr) {
}

UtlLogger::~UtlLogger() {
}

std::ostream& UtlLogger::outStream() {
  return *_outStream;
}

void UtlLogger::outStream(std::ostream &o) {
  _outStream=&o;
}

std::ostream& UtlLogger::errStream() {
  return *_errStream;
}

void UtlLogger::errStream(const std::string &msg) {
  *_errStream << msg << std::flush;
}

void UtlLogger::errStream(std::ostream &e) {
  _errStream=&e;
}

#endif
#endif
