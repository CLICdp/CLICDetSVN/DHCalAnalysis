#ifndef AhcVfeSlowControlsFile_HH
#define AhcVfeSlowControlsFile_HH

#include <string>
#include <fstream>
#include <iostream>

#include "UtlPack.hh"
#include "UtlPrintHex.hh"

#include "AhcVfeSlowControlsData.hh"


class AhcVfeSlowControlsFile {

public:
  AhcVfeSlowControlsFile() {
  }

  bool read(const std::string &file, AhcVfeSlowControlsData &a) {
    ifstream fin(file.c_str());
    if(!fin) return false;
    
    std::string header;
    fin >> header;
    
    unsigned n(0),slot,fe,hab,d,value;
    
    while(fin.good()) {
      fin >> slot >> fe >> hab >> d >> value;
      
      if(fin) {
	n++;
	a.dac(hab,d,value);
      }
    }
    
    fin.close();
    
    return n==12*18;
  }
  
  bool write(const std::string &file) {
    return true;
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    return o;
  }
  

private:
};

#endif
