#ifndef AhcVfeSlowControlsFile_HH
#define AhcVfeSlowControlsFile_HH

#include <string>
#include <fstream>
#include <iostream>

#include "UtlPack.hh"
#include "UtlPrintHex.hh"

#include "AhcVfeSlowControlsData.hh"


class AhcVfeSlowControlsFile {

public:
  AhcVfeSlowControlsFile() {
  }

  bool read(const std::string &file, AhcVfeSlowControlsData &a, unsigned readFE) {
    std::ifstream fin(file.c_str());
    if(!fin) 
      {
	std::cout << "error opening " << file << std::endl;
	return false;
      }

    for (unsigned hab=0;hab<12;hab++) 
      for (unsigned chan=0;chan<18;chan++) a.dac(hab,chan,0);
    
    
    bool dataStart(false);
    std::string strBuffer;
    char charBuffer[512];
    unsigned n(0),slot,fe,hab,d,value;



    
    while(fin.good()) {
      if (!dataStart)
	{
	  fin >> strBuffer;
	  if (strBuffer[0] == '#')
	    {
	      fin.getline(charBuffer,sizeof(charBuffer));
	    }
	  if (strBuffer=="--data--" || strBuffer=="--data-V2--") dataStart = true;
	}
      else
	{

	  if (strBuffer=="--data--") 
	    {
	      fin >> slot >> fe >> hab >> d >> value;
	      if(fin && fe == readFE) {
		n++;
		a.dac(hab,d,value);
	      }
	    }
	  else if (strBuffer=="--data-V2--") 
	    {
	      fin >> hab >> d >> value;
	      if (fin)
		{
		  n++;
		  a.dac(hab,d,value);
		}
	    }
	}
    }
    
    fin.close();
    std::cout << "read " << n << " DAC values for FE=" << readFE << " from "<< file << std::endl; 
    return (n==12*18 || n==141);
  }


  bool read(const std::string &file, AhcVfeStartUpDataCoarse &a, unsigned readFE) {
    std::ifstream fin(file.c_str());
    if(!fin) 
      {
	std::cout << "error opening " << file << std::endl;
	return false;
      }

    //    std::cout << "opened file" << file <<std::endl;

    for (unsigned hab=0;hab<12;hab++) 
      if ((hab < 4) || (hab > 7))
	for (unsigned chan=0;chan<18;chan++) {
	  //	  std::cout << "presetting hab " << hab << " channel " << chan <<std::endl;
	  a.dac(hab,chan,0);
	}
    
    
    bool dataStart(false);
    std::string strBuffer;
    char charBuffer[512];
    unsigned n(0),slot,fe,hab,d,value;



    
    while(fin.good()) {
      if (!dataStart)
	{
	  fin >> strBuffer;
	  if (strBuffer[0] == '#')
	    {
	      fin.getline(charBuffer,sizeof(charBuffer));
	    }
	  if (strBuffer=="--data--" || strBuffer=="--data-V2--") dataStart = true;
	}
      else
	{

	  if (strBuffer=="--data--") 
	    {
	      fin >> slot >> fe >> hab >> d >> value;
	      if(fin && fe == readFE) {
		n++;
		//		if (hab>3) hab = hab - 4;
		a.dac(hab,d,value);
	      }
	    }
	  else if (strBuffer=="--data-V2--") 
	    {
	      fin >> hab >> d >> value;
	      if (fin)
		{
		  n++;
		  //		  std::cout << "reading DAC: " << hab << " " << d << std::endl;
		  //		  if (hab>3) hab = hab - 4;
		  a.dac(hab,d,(char)value);
		}
	    }
	}
    }
    
    fin.close();
    std::cout << "read " << n << " coarse module DAC values for FE=" << readFE << " from "<< file << std::endl; 
    return (n==141);
  }

  
  bool write(const std::string &file) {
    return true;
  }

  std::ostream& print(std::ostream &o, std::string s="") const {
    return o;
  }
  

private:
};

#endif
