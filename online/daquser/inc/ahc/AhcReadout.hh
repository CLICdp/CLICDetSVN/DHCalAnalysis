#ifndef AhcReadout_HH
#define AhcReadout_HH

#include <iostream>
#include <fstream>

#include "CrcReadout.hh"


class AhcReadout : public CrcReadout {

public:
  AhcReadout(unsigned b) : CrcReadout(b,0xac) {
  }

  AhcReadout(unsigned b, unsigned char c) : CrcReadout(b,c) {
  }

  virtual ~AhcReadout() {
  }

private:
};

#endif
