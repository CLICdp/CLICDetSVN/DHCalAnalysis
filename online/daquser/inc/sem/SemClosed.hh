#ifndef SemClosed_HH
#define SemClosed_HH

#include "SemOpen.hh"

class SemClosed {

public:
  SemClosed(unsigned range=SEM_VALUE_MAX, unsigned initialValue=0) :
    _standardSemOpen(initialValue), _invertedSemOpen(range-initialValue) {
  }

  void operator++(int) {
    _invertedSemOpen--;
    _standardSemOpen++;
  }

  void operator--(int) {
    _standardSemOpen--;
    _invertedSemOpen++;
  }

  unsigned value() const {
    return _standardSemOpen.value();
  }


private:
  SemOpen _standardSemOpen;
  SemOpen _invertedSemOpen;
};

#endif
