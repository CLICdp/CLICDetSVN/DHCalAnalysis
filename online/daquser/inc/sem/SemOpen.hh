#ifndef SemOpen_HH
#define SemOpen_HH

#include <semaphore.h>

#include "SemData.hh"

class SemOpen {

public:
  SemOpen(unsigned initialValue=0) {
    if(initialValue>SEM_VALUE_MAX) initialValue=SEM_VALUE_MAX;
    if(sem_init(&_semData,0,initialValue)<0) cerr <<"ERROR" << endl;
  }

  ~SemOpen() {
    sem_destroy(&_semData);
  }

  void operator++(int) {
    sem_post(&_semData);
  }

  void operator--(int) {
    sem_wait(&_semData);
  }

  unsigned value() const {
    int v;
    sem_getvalue((SemData*)&_semData,&v);
    return v;
  }


private:
  SemData _semData;
};

#endif
