#ifndef BmlLc1176Readout_HH
#define BmlLc1176Readout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "DaqRunStart.hh"
#include "CrcVlinkEventData.hh"
#include "SubAccessor.hh"
#include "SubInserter.hh"


class BmlLc1176Readout : public RcdUserRW {

public:
  BmlLc1176Readout(unsigned pci, unsigned char c) : _baseAddress(c) {
  }

  virtual ~BmlLc1176Readout() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << std::endl << "BmlLc1176Readout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ");
    }

    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level 
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
      if(v.size()==1)  _printLevel=v[0]->runType().printLevel();
    }

    if(r.recordType()==RcdHeader::configurationStart) {
      SubAccessor accessor(r);

      {std::vector<const BmlLc1176ConfigurationData*>
        v(accessor.extract<BmlLc1176ConfigurationData>());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of BmlLc1176ConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}
    }

    if(r.recordType()==RcdHeader::event) {

      // Add in some dummy data
      SubInserter inserter(r);
      inserter.insert<BmlLc1176EventData>(true);
      inserter.extend(256);
    }

    return true;
  }

protected:
  const unsigned char _baseAddress;
};

#endif
