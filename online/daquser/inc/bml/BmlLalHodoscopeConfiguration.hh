#ifndef BmlLalHodoscopeConfiguration_HH
#define BmlLalHodoscopeConfiguration_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"


class BmlLalHodoscopeConfiguration : public RcdUserRW {

public:
  BmlLalHodoscopeConfiguration() {
  }

  virtual ~BmlLalHodoscopeConfiguration() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "BmlLalHodoscopeConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
    
    // Check record type
    switch (r.recordType()) {
      
      // Run start 
    case RcdHeader::runStart: {

      // Access the DaqRunStart
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*>
        v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;

      _runType=v[0]->runType();

      break;
    }
      
      // Configuration start 
    case RcdHeader::configurationStart: {
      
      // Handle the runs which need work
      if(_runType.majorType()==DaqRunType::bml ||
	 _runType.beamType()) {

	// Access the DaqConfigurationStart
	SubAccessor accessor(r);
	std::vector<const DaqConfigurationStart*>
	  v(accessor.extract<DaqConfigurationStart>());
	assert(v.size()==1);
	if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;
	
	bool storeData(false);
	BmlLalHodoscopeConfigurationData c;

	//const unsigned char v(_runType.version());

	switch(_runType.type()) {

	case DaqRunType::bmlTest: {
	  storeData=true;
	  break;
	}
      
	case DaqRunType::bmlNoise: {
	  storeData=true;
	  break;
	}
      
	case DaqRunType::bmlInternalTest: {
	  storeData=true;
	  break;
	}
      
	case DaqRunType::emcBeam:
	case DaqRunType::emcBeamHoldScan:

	case DaqRunType::sceBeam:
	case DaqRunType::sceBeamHoldScan:
	case DaqRunType::sceBeamStage:
	case DaqRunType::sceBeamStageScan:

	case DaqRunType::ahcBeam:
	case DaqRunType::ahcBeamHoldScan:
	case DaqRunType::ahcBeamStage:
	case DaqRunType::ahcBeamStageScan:

	case DaqRunType::beamTest:
	case DaqRunType::beamNoise:
	case DaqRunType::beamData:
	case DaqRunType::beamHoldScan:
	case DaqRunType::beamStage:
	case DaqRunType::beamStageScan: {
	  // Read out for pedestals and LED parts also!
	  //if((v[0]->configurationNumberInRun()%3)==2) {
	  storeData=true;
	  //}
	  break;
	}
	  
	default: {
	  break;
	}
	};
	
	// Put the configuration object into the record
	if(storeData) {
	  SubInserter inserter(r);
	  inserter.insert<BmlLalHodoscopeConfigurationData>(c);
	  if(doPrint(r.recordType(),1)) c.print(std::cout," ") << std::endl;
	}
      }

      break;
    }
      
    default: {
      break;
    }
    };
    
    return true;
  }


private:
  DaqRunType _runType;
};

#endif
