#ifndef BmlCaen767Readout_HH
#define BmlCaen767Readout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "DaqRunStart.hh"
#include "CrcVlinkEventData.hh"
#include "SubAccessor.hh"
#include "SubInserter.hh"


class BmlCaen767Readout : public RcdUserRW {

public:
  BmlCaen767Readout(unsigned pci, unsigned char c) : _baseAddress(c) {
  }

  virtual ~BmlCaen767Readout() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << std::endl << "BmlCaen767Readout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ");
    }

    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level 
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
      if(v.size()==1)  _printLevel=v[0]->runType().printLevel();
    }

    if(r.recordType()==RcdHeader::configurationStart) {
      SubAccessor accessor(r);

      {std::vector<const BmlLocationData<BmlLc1176ConfigurationData>*>
        v(accessor.extract< BmlLocationData<BmlLc1176ConfigurationData> >());
 
      if(doPrint(r.recordType(),1)) {
        std::cout << " Number of BmlLc1176ConfigurationData subrecords accessed = "
                  << v.size() << std::endl;
        for(unsigned i(0);i<v.size();i++) {
          v[i]->print(std::cout," ") << std::endl;
        }
      }}
    }

    if(r.recordType()==RcdHeader::event) {

      // Add in some dummy data
      /*
      SubInserter inserter(r);
      inserter.insert<BmlLc1176EventData>(true);
      inserter.extend(256);
      */
    }

    return true;
  }

protected:
  const unsigned char _baseAddress;
};

#endif
