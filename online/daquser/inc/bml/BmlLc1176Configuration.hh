#ifndef BmlLc1176Configuration_HH
#define BmlLc1176Configuration_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"

#include "DaqRunType.hh"

#include "BmlLc1176ConfigurationData.hh"


class BmlLc1176Configuration : public RcdUserRW {

public:
  BmlLc1176Configuration() {
  }

  virtual ~BmlLc1176Configuration() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "BmlLc1176Configuration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
    
    // Check record type
    switch (r.recordType()) {
      
      // Run start 
    case RcdHeader::runStart: {

      // Access the DaqRunStart
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*>
        v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;

      _runType=v[0]->runType();

      // Disable?
      break;
    }
      
      // Configuration start 
    case RcdHeader::configurationStart: {
      
      // Handle the runs which need work
      if(_runType.majorType()==DaqRunType::bml ||
	 /*
	 _runType.majorType()==DaqRunType::beam ||

	 _runType.type()==DaqRunType::emcBeam ||
	 _runType.type()==DaqRunType::emcBeamHoldScan ||

	 _runType.type()==DaqRunType::ahcBeam ||
	 _runType.type()==DaqRunType::ahcBeamHoldScan ||
	 _runType.type()==DaqRunType::ahcBeamStage ||
	 _runType.type()==DaqRunType::ahcBeamStageScan) {
	 */
	 _runType.beamType()) {

	// Access the DaqConfigurationStart
	SubAccessor accessor(r);
	std::vector<const DaqConfigurationStart*>
	  v(accessor.extract<DaqConfigurationStart>());
	assert(v.size()==1);
	if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;
	
	BmlLc1176ConfigurationData c;
	c.label(1);

	//const unsigned char v(_runType.version());

	switch(_runType.type()) {

	case DaqRunType::bmlTest: {
	  c.csr(0x00008600);
	  break;
	}
      
	case DaqRunType::bmlNoise: {
	  c.csr(0x00008600);
	  break;
	}
      
	case DaqRunType::bmlInternalTest: {
	  c.csr(0x000040ff);
	  break;
	}
      
	case DaqRunType::emcBeam:
	case DaqRunType::emcBeamHoldScan:

	case DaqRunType::sceBeam:
	case DaqRunType::sceBeamHoldScan:
	case DaqRunType::sceBeamStage:
	case DaqRunType::sceBeamStageScan:

	case DaqRunType::ahcBeam:
	case DaqRunType::ahcBeamHoldScan:
	case DaqRunType::ahcBeamStage:
	case DaqRunType::ahcBeamStageScan:

	case DaqRunType::beamTest:
	case DaqRunType::beamNoise:
	case DaqRunType::beamData:
	case DaqRunType::beamHoldScan:
	case DaqRunType::beamStage:
	case DaqRunType::beamStageScan: {
	  if((v[0]->configurationNumberInRun()%3)==2) c.csr(0x00008600);
	  else c.label(0); // Easy way to prevent object being stored
	  break;
	}
	  
	default: {
	  break;
	}
	};
	
	// Put the configuration object into the record
	if(c.label()==1) {
	  SubInserter inserter(r);
	  inserter.insert<BmlLc1176ConfigurationData>(c);
	  if(doPrint(r.recordType(),1)) c.print(std::cout," ") << std::endl;
	}
      }

      break;
    }
      
    default: {
      break;
    }
    };
    
    return true;
  }


private:
  DaqRunType _runType;
};

#endif
