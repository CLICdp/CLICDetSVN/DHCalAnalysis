#include <iostream>

#include "TimeVal.hh"
#include "DeltaTimeVal.hh"

using namespace std;

int main() {
  TimeVal t(true);
  t.print(cout);
  sleep(1);

  DeltaTimeVal d(t);
  d.print(cout);

  return 0;
}
