#ifndef RcdMultiUserRO_HH
#define RcdMultiUserRO_HH

#include <iostream>
#include <fstream>
#include <vector>

// dual/inc/rcd
#include "RcdUserRO.hh"


class RcdMultiUserRO : public RcdUserRO {

public:
  RcdMultiUserRO() : RcdUserRO() {
  }

  RcdMultiUserRO(unsigned p) : RcdUserRO(p) {
  }

  virtual ~RcdMultiUserRO() {
  }

  void addUser(RcdUserRO &b) {
    b.printLevel(_printLevel);
    _vUser.push_back(&b);
  }

  void printLevel(unsigned p) {
    _printLevel=p;
    for(unsigned i(0);i<_vUser.size();i++) _vUser[i]->printLevel(p);
  }

  virtual bool record(const RcdRecord &r) {
    bool result(true);

    for(unsigned i(0);i<_vUser.size();i++) {
      if(!_vUser[i]->record(r)) result=false;
    }

    return result;
  }

  void deleteAll() {
    for(unsigned i(0);i<_vUser.size();i++) {
      delete _vUser[i];
    }
    _vUser.clear();
  }

private:
  std::vector<RcdUserRO*> _vUser;
};

#endif
