#include <limits.h>

#include <iostream>

#include "RcdArena.hh"
#include "RcdReaderBin.hh"
#include "RcdWriterBin.hh"

using namespace std;

int main() {
  cout << "SSIZE_MAX = " << SSIZE_MAX << endl;

  RcdReaderBin r;
  RcdWriterBin w;

  r.open("test.bin");
  w.open("test2.bin");

  RcdArena a;
  while(r.read(&a)) {
    a.print(cout);
    a.updateRecordTime();
    w.write(&a);
  }

  r.close();
  w.close();
}
