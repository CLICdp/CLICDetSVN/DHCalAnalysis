#ifndef RcdMultiUserRW_HH
#define RcdMultiUserRW_HH

#include <iostream>
#include <fstream>
#include <vector>

// dual/inc/rcd
#include "RcdUserRW.hh"


class RcdMultiUserRW : public RcdUserRW {

public:
  RcdMultiUserRW() : RcdUserRW() {
  }

  RcdMultiUserRW(unsigned p) : RcdUserRW(p) {
  }

  virtual ~RcdMultiUserRW() {
  }

  void addUser(RcdUserRW &b) {
    b.printLevel(_printLevel);
    _vUser.push_back(&b);
  }

  void printLevel(unsigned p) {
    _printLevel=p;
    for(unsigned i(0);i<_vUser.size();i++) _vUser[i]->printLevel(p);
  }

  virtual bool record(RcdRecord &r) {
    bool result(true);

    for(unsigned i(0);i<_vUser.size();i++) {
      if(!_vUser[i]->record(r)) result=false;
    }

    return result;
  }

private:
  std::vector<RcdUserRW*> _vUser;
};

#endif
