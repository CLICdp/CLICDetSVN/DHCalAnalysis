#ifndef RcdSktUserRW_HH
#define RcdSktUserRW_HH

#include <iostream>
#include <fstream>
#include <string>

#include "RcdUserRW.hh"
#include "RcdIoSkt.hh"


class RcdSktUserRW : public RcdUserRW {

public:
  RcdSktUserRW(std::string host, unsigned port, unsigned tries) {
    _socket.open(host,port,tries);
  }

  virtual ~RcdSktUserRW() {
    _socket.close();
  }

  virtual bool record(RcdRecord &r) {
    assert(_socket.write(r));
    assert(_socket.read(r));
    if(r.recordType()==RcdHeader::shutDown) assert(_socket.close());
    return true;
  }

private:
  RcdIoSkt _socket;
};

#endif
