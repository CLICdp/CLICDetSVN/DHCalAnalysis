#include <iostream>

#include "OnlSubRecord.hh"
#include "RcdArena.hh"
#include "RcdWriterBin.hh"

using namespace std;

main(int argc, char *argv[]) {
  RcdArena a;
  unsigned i(0),*p((unsigned*)&a);
  memset(&a,0,sizeof(RcdArena));
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }

  std::cout << argv[0] << ": sizeof(RcdSubHeader) = " 
	    << sizeof(RcdSubHeader) << endl;
  std::cout << argv[0] << ": sizeof(OnlSubRecord<unsigned>) = " 
	    << sizeof(OnlSubRecord<unsigned>) << endl;
  std::cout << argv[0] << ": sizeof(RcdHeader) = " 
	    << sizeof(RcdHeader) << endl;
  std::cout << argv[0] << ": sizeof(RcdRecord) = " 
	    << sizeof(RcdRecord) << endl;
  std::cout << argv[0] << ": sizeof(RcdArena) = " 
	    << sizeof(RcdArena) << endl;
  a.updateRecordTime();
  a.recordType(RcdHeader::startRun);
  a.print(cout);
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }
  cout << endl;

  RcdSubHeader *h(a.lastSubHeader());
  h->subRecordType(987);
  h->numberOfBytes(8);
  a.appendLastSubHeader(h);
  a.print(cout);
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }
  cout << endl;

  OnlSubRecord<unsigned> s;
  a.appendSubHeader(&s);
  a.print(cout);
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }
  cout << endl;

  OnlSubRecord<unsigned,2> s2;
  *(s2.payload((unsigned)0))=56;
  *(s2.payload((unsigned)1))=78;
  a.appendSubHeader(&s2);
  a.print(cout);
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }
  cout << endl;

  OnlSubRecord<TrgConfiguration> tc;
  a.appendSubHeader(&tc);
  a.print(cout);
  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }
  cout << endl;

  for(h=a.firstSubHeader();h!=0;h=h->nextSubHeader()) {
    OnlSubRecord<unsigned,2> *r(OnlSubRecord<unsigned,2>::safeCast(h));
    if(r==0) {
      cout << "Header at " << h << " is not of right type" << endl;
    } else {
      r->print(cout);
    }
  }

  for(i=0;i<16*1024;i++) {
    if(i<16 || p[i]!=0) cout << "i = " << i << " data = " << p[i] 
			     << " = " << hex << p[i] << dec << endl;
  }


  RcdArena b(a);
  unsigned *q((unsigned*)&b);

  for(i=0;i<16*1024;i++) {
    if(i<16 || q[i]!=0) cout << "i = " << i << " data = " << q[i] 
			     << " = " << hex << q[i] << dec << endl;
  }

  RcdWriterBin w;
  w.open("test.bin");
  w.write(&a);
  w.write(&b);
  w.close();


  /*
  RcdSubRecord<double> s;
  RcdSubHeader *h(&s);
  RcdSubRecord<double> *p(RcdSub<double>::checkSubRecord(h));
  cout << "Pointer = " << p << endl;
  s.getPayload()=7.89;
  cout << "Identity = " << s.subRecordTypeIdentity<double>() << endl;
  cout << "Number of bytes = " << s.numberOfBytes() << ", "
       << s.totalNumberOfBytes() << endl;
  cout << "Payload 0 = " << s.getPayload(0) << endl;
  */

  return 0;
}
