#ifndef RcdUserRW_HH
#define RcdUserRW_HH

#include <iostream>
#include <fstream>

// dual/inc/rcd
#include "RcdRecord.hh"
#include "RcdUserBase.hh"


class RcdUserRW : public RcdUserBase {

public:
  RcdUserRW() : RcdUserBase() {
  }

  RcdUserRW(unsigned p) : RcdUserBase(p) {
  }

  virtual ~RcdUserRW() {
  }

  virtual bool record(RcdRecord &r) = 0;
};

#endif
