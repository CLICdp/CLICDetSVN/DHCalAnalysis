#ifndef RcdUserBase_HH
#define RcdUserBase_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"


class RcdUserBase {

public:
  RcdUserBase() : _printLevel(0) {
  }

  RcdUserBase(unsigned p) : _printLevel(p) {
  }

  virtual ~RcdUserBase() {
  }

  void printLevel(unsigned p) {
    _printLevel=p;
  }

  bool doPrint(RcdHeader::RecordType t, unsigned inc=0) {
    if(_printLevel==0) return false;
 
    switch (t) {
    case RcdHeader::startUp:            return _printLevel>( 0+inc);
    case RcdHeader::shutDown:           return _printLevel>( 0+inc);
    case RcdHeader::sequenceStart:      return _printLevel>( 3+inc);
    case RcdHeader::sequenceEnd:        return _printLevel>( 3+inc);
    case RcdHeader::runStart:           return _printLevel>( 6+inc);
    case RcdHeader::runEnd:             return _printLevel>( 6+inc);
    case RcdHeader::configurationStart: return _printLevel>( 9+inc);
    case RcdHeader::configurationEnd:   return _printLevel>( 9+inc);
    case RcdHeader::slowReadout:        return _printLevel>(12+inc);
    case RcdHeader::acquisitionStart:   return _printLevel>(12+inc);
    case RcdHeader::acquisitionEnd:     return _printLevel>(12+inc);
    case RcdHeader::bunchTrain:         return _printLevel>(15+inc);
    case RcdHeader::spillStart:         return _printLevel>(15+inc);
    case RcdHeader::spillEnd:           return _printLevel>(15+inc);
    case RcdHeader::transferStart:      return _printLevel>(15+inc);
    case RcdHeader::transferEnd:        return _printLevel>(15+inc);
    case RcdHeader::trigger:            return _printLevel>(18+inc);
    case RcdHeader::triggerBurst:       return _printLevel>(18+inc);
    case RcdHeader::event:              return _printLevel>(18+inc);
      
    default: return false;
    };
  }

  void setPrint(RcdHeader::RecordType t, unsigned inc=0) {

    switch (t) {
    case RcdHeader::startUp:            _printLevel= 1+inc;
    case RcdHeader::shutDown:           _printLevel= 1+inc;
    case RcdHeader::sequenceStart:      _printLevel= 4+inc;
    case RcdHeader::sequenceEnd:        _printLevel= 4+inc;
    case RcdHeader::runStart:           _printLevel= 7+inc;
    case RcdHeader::runEnd:             _printLevel= 7+inc;
    case RcdHeader::configurationStart: _printLevel=10+inc;
    case RcdHeader::configurationEnd:   _printLevel=10+inc;
    case RcdHeader::slowReadout:        _printLevel=13+inc;
    case RcdHeader::acquisitionStart:   _printLevel=13+inc;
    case RcdHeader::acquisitionEnd:     _printLevel=13+inc;
    case RcdHeader::bunchTrain:         _printLevel=16+inc;
    case RcdHeader::spillStart:         _printLevel=16+inc;
    case RcdHeader::spillEnd:           _printLevel=16+inc;
    case RcdHeader::transferStart:      _printLevel=16+inc;
    case RcdHeader::transferEnd:        _printLevel=16+inc;
    case RcdHeader::trigger:            _printLevel=19+inc;
    case RcdHeader::triggerBurst:       _printLevel=19+inc;
    case RcdHeader::event:              _printLevel=19+inc;
      
    default:                            _printLevel=255;
    };
  }

protected:
  unsigned _printLevel;
};

#endif
