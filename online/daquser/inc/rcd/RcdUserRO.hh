#ifndef RcdUserRO_HH
#define RcdUserRO_HH

#include <iostream>
#include <fstream>

// dual/inc/rcd
#include "RcdRecord.hh"
#include "RcdUserBase.hh"


class RcdUserRO : public RcdUserBase {

public:
  RcdUserRO() : RcdUserBase() {
  }

  RcdUserRO(unsigned p) : RcdUserBase(p) {
  }

  virtual ~RcdUserRO() {
  }

  virtual bool record(const RcdRecord &r) = 0;
};

#endif
