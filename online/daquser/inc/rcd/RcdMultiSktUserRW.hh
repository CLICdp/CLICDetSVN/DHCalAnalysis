#ifndef RcdMultiSktUserRW_HH
#define RcdMultiSktUserRW_HH

#include <iostream>
#include <fstream>
#include <string>

#include "RcdUserRW.hh"
#include "RcdIoSkt.hh"


template <unsigned NumberOfSockets> class RcdMultiSktUserRW : public RcdUserRW {

public:

  RcdMultiSktUserRW(std::vector< std::pair<std::string, unsigned> > v, unsigned tries=100) {
    open(v,tries);
  }

  // Temp for 3
  RcdMultiSktUserRW(std::string host0, unsigned port0,
		    std::string host1, unsigned port1,
		    std::string host2, unsigned port2, unsigned tries=100) {

    std::vector< std::pair<std::string, unsigned> > v;
    v.push_back(std::pair<std::string, unsigned>(host0,port0));
    v.push_back(std::pair<std::string, unsigned>(host1,port1+1));
    v.push_back(std::pair<std::string, unsigned>(host2,port2+2));
    open(v,tries);
  }

  virtual ~RcdMultiSktUserRW() {
    // Clean up if problem (no assert on close as error)
    for(unsigned i(0);i<NumberOfSockets;i++) _socket[i].close();
    delete _arena;
  }

  void open(std::vector< std::pair<std::string, unsigned> > v, unsigned tries) {
    _serial=true;

    assert(v.size()==NumberOfSockets);
    for(unsigned i(0);i<NumberOfSockets;i++) assert(_socket[i].open(v[i].first,v[i].second,tries));

    // Local copy space
    _arena=new RcdArena;
  }

  void serial(bool s) {
    _serial=s;
  }

  virtual bool record(RcdRecord &r) {
    //SubAccessor accessor(r);
    //accessor.print(std::cout,"Orig ");

    if(_serial) {
      for(unsigned i(0);i<NumberOfSockets;i++) {
	assert(_socket[i].write(r));
	assert(_socket[i].read(r));
      }

    } else {
      const unsigned nw(r.numberOfWords());
      //const unsigned *newData(r.newData());

      for(unsigned i(0);i<NumberOfSockets;i++) {
	//std::cout << "About to write to socket " << i << std::endl;
	assert(_socket[i].write(r));
	//std::cout << "Written to socket " << i << std::endl;
      }

      assert(_socket[0].read(r));
      for(unsigned i(1);i<NumberOfSockets;i++) {
	//std::cout << "About to read from socket " << i << std::endl;
	assert(_socket[i].read(*_arena));
	//std::cout << "Read from socket " << i << std::endl;
	unsigned newWords(_arena->numberOfWords()-nw);
	r.extend(newWords,_arena->newData()-newWords);
      }
    }

    if(r.recordType()==RcdHeader::shutDown) {
      for(unsigned i(0);i<NumberOfSockets;i++) assert(_socket[i].close());
    }

    return true;
  }

private:
  RcdIoSkt _socket[NumberOfSockets];
  bool _serial;
  RcdArena *_arena;
};

#endif
