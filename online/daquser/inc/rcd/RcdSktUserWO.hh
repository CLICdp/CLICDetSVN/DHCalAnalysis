#ifndef RcdSktUserWO_HH
#define RcdSktUserWO_HH

#include <iostream>
#include <fstream>
#include <string>

#include "RcdUserRO.hh"
#include "RcdIoSkt.hh"


class RcdSktUserWO : public RcdUserRO {

public:
  RcdSktUserWO(std::string host, unsigned port, unsigned tries) {
    _socket.open(host,port,tries);
  }

  virtual ~RcdSktUserWO() {
    _socket.close();
  }

  virtual bool record(const RcdRecord &r) {
    assert(_socket.write(r));
    if(r.recordType()==RcdHeader::shutDown) assert(_socket.close());
    return true;
  }

private:
  RcdIoSkt _socket;
};

#endif
