#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include "ShmObject.hh"
#include "ShmSingleton.hh"

class Simple {
public:
  Simple() : _n(999) {
    std::cout << "Simple::ctor " << this << std::endl;
  }

  ~Simple() {
    std::cout << "Simple::dtor " << this << std::endl;
  }

  unsigned n() const {
    return _n;
  }

  void n(unsigned n) {
    _n=n;
  }

private:
  unsigned _n;
};

//const key_t ShmSingleton<Simple>::theKey(123456786);

main(int argc, char *argv[]) {
    //ShmSingleton<Simple> shmU;
    //std::cout << argv[0] << "Child: Created ShmSingleton<Simple>" << endl;

    const ShmObject<Simple,2> shmU(123456786);
    const Simple* const p(shmU.payload());
    std::cout << argv[0] << " Child: Created ShmObject<Simple,2> "
	      << p << endl;

    for(unsigned i(0);i<12;i++) {
      sleep(1);
      std::cout << argv[0] << " Child: n = " << p[0].n()
		<< "  n*n = " << p[1].n() << endl;
    }

  return 0;
}
