#ifndef IlcGeneric_HH
#define IlcGeneric_HH

#include <cassert>

#include "RcdMultiUserRO.hh"


class IlcGeneric : public RcdMultiUserRO {

public:

  // Set default bits to enable required modules
  enum {
    //defaultIlcGenericBits=0x1763 // = 0b 00010111 01100011
    defaultIlcGenericBits=0x3107 // = 0b 00110001 00000111
  };


  IlcGeneric(const UtlPack bits=defaultIlcGenericBits) : _ignorRunType(false) {

    // Add modules

    if(bits.bit( 0)) {
      //_vModule.push_back(new SOMEMODULE);
      //addUser(*(_vModule[_vModule.size()-1]));
    } else {
      std::cout << "IlcGeneric::ctor() SOMEMODULE disabled" << std::endl;
    }
  }

  virtual ~IlcGeneric() {
    for(unsigned i(0);i<_vModule.size();i++) delete _vModule[i];
  }

  virtual bool record(const RcdRecord &r) {

    // Possibly disable histogramming
    if(r.recordType()==RcdHeader::runStart) {
      if(!_ignorRunType) {
	SubAccessor accessor(r);
	std::vector<const IlcRunStart*>
	  v(accessor.extract<IlcRunStart>());

        if(v.size()==1) {
          printLevel(v[0]->runType().printLevel());
        }
      }
    }

    if(!RcdMultiUserRO::record(r)) return false;

    return true;
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }


private:
  bool _ignorRunType;
  std::vector<RcdUserRO*> _vModule;
};

#endif
