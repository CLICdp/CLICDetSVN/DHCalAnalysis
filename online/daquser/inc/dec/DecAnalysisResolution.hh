#ifndef DecAnalysisResolution_HH
#define DecAnalysisResolution_HH

#include <sstream>
#include <cassert>

#include "TH3F.h"

#include "RcdUserRO.hh"
#include "SubAccessor.hh"

#include "DecAnalysisBase.hh"


class DecAnalysisResolution : public DecAnalysisBase {

public:
  DecAnalysisResolution() : DecAnalysisBase("DecAnalysisResolution") {
    _doneFiles=true;
  }

  virtual ~DecAnalysisResolution() {
    if(!_doneFiles) doFiles();
    endRoot();
  }

  virtual bool decAnalysisValidRun(IlcRunType rt) const {
    return rt.majorType()==IlcRunType::dec;
  }

  bool runStart(const RcdRecord &r) {
    if(!_doneFiles) doFiles();
    _doneFiles=false;
    TH1::SetDefaultSumw2();
    
    // Should be determined from the sim file
    _nLayers=16;
    _nThinTungsten=10;
    
    // TPAC - can be varied here
    _tNineSensor=10;
    _tSensorSize=25.0;
    _tSensorGap=1.0;
    
    // 4T - can be varied here
    _fSingleSensor=16;
    _fSensorSize=50.0;
    _fSensorGap=0.5;
    //_fSensorGap=0.0;
    
    TH1F *hWeight=new TH1F("Weight","Digital weights",20,0.0,20.0);
    
    for(unsigned i(0);i<_nLayers;i++) {
      if(i<_nThinTungsten) _weight[i]=0.0124;
      else                 _weight[i]=0.0248;

      if(_runStart.runNumber()==910010) {
	_weight[i]=0.02;
      }

      if(_runStart.runNumber()==910011) {
	if(i<_nThinTungsten) _weight[i]=0.0124;
	else                 _weight[i]=0.0372;
      }
      
      hWeight->Fill(i,_weight[i]);
    }
    
    return true;
  }

  bool runEnd(const RcdRecord &r) {
    if(!_doneFiles) doFiles();
    return true;
  }

  void doFiles() {
    _doneFiles=true;
  }

  bool configurationStart(const RcdRecord &r) {
    _hPRadius=new TH1F("PRadius","Primary position radius",
		       100,0.0,50.0);
    _hERadius=new TH1F("ERadius","ECAL entry position radius",
		       100,0.0,50.0);

    _hEvent=new TH3F("Event",
		     "Event",
		     20,0,20,100,-200,200,100,-200,200);

    for(unsigned l(0);l<20;l++) {
      std::ostringstream sLabel,sTitle;
      sLabel << "Perfect" << l;
      sTitle << "Perfect Radius cut " << 2*(l+1) << " mm";

      _hNPerfect[l]=new TProfile((sLabel.str()+"Number").c_str(),
				 (sTitle.str()+";Layer").c_str(),
				 20,0.0,20.0);
      _hEnergyPerfect[l]=new TH1F((sLabel.str()+"Energy").c_str(),
				  (sTitle.str()+";Energy (GeV)").c_str(),
				  500,0.0,0.005*_vSimPrimaryData[0].energy());
    }
    
    for(unsigned k(0);k<3;k++) {
      for(unsigned l(0);l<20;l++) {
	std::ostringstream sLabel,sTitle;
	if(k==0) {
	  sLabel << "tNineSensor" << l;
	  sTitle << "TPAC number of nine sensor layers " << l;
	}
	if(k==1) {
	  sLabel << "tSensorSize" << l;
	  sTitle << "TPAC sensor size " << 40.0+2.5*l << " mm";
	}
	if(k==2) {
	  sLabel << "tSensorGap" << l;
	  sTitle << "TPAC sensor gap " << 0.1*l << " mm";
	}

	_hTEnergy[k][l]=new TH1F((sLabel.str()+"Energy").c_str(),
				 (sTitle.str()+";Energy (GeV)").c_str(),
				 500,0.0,0.005*_vSimPrimaryData[0].energy());

	/*
	for(unsigned i(0);i<_nLayers;i++) {
	  std::ostringstream sout;
	  sout << i;

	  _hTXY[k][l][i]=new TH2F((sLabel.str()+"DXY"+sout.str()).c_str(),
	  (sTitle.str()+"Layer "+sout.str()+" X,Y of hits;x (mm);y (mm)").c_str(),
	  100,-100.0,100.0,100,-100.0,100.0);
	  }
	*/
      }
    }
    
    for(unsigned k(0);k<3;k++) {
      for(unsigned l(0);l<20;l++) {
	std::ostringstream sLabel,sTitle;
	if(k==0) {
	  sLabel << "fSingleSensor" << l;
	  sTitle << "4T number of single sensor layers " << l;
	}
	if(k==1) {
	  sLabel << "fSensorSize" << l;
	  sTitle << "4T sensor size " << 40.0+2.5*l;
	}
	if(k==2) {
	  sLabel << "fSensorGap" << l;
	  sTitle << "4T sensor gap " << 0.1*l;
	}

	_hFEnergy[k][l]=new TH1F((sLabel.str()+"Energy").c_str(),
				 (sTitle.str()+";Energy (GeV)").c_str(),
				 500,0.0,0.005*_vSimPrimaryData[0].energy());

	/*
	for(unsigned i(0);i<_nLayers;i++) {
	  std::ostringstream sout;
	  sout << i;

	  _hFXY[k][l][i]=new TH2F((sLabel.str()+"DXY"+sout.str()).c_str(),
				  (sTitle.str()+"Layer "+sout.str()+" X,Y of hits;x (mm);y (mm)").c_str(),
				  100,-100.0,100.0,100,-100.0,100.0);
	}
	*/
      }
    }

    return true;
  }
  
  bool bunchTrain(const RcdRecord &r) {
    SubAccessor accessor(r);

    std::vector<const SimParticle*>
      w(accessor.access<SimParticle>());
    assert(w.size()>0);
    assert(w[0]->primary());

    double rp(sqrt(w[0]->x()*w[0]->x()+w[0]->y()*w[0]->y()));
    _hPRadius->Fill(rp);

    for(unsigned i(1);i<w.size();i++) {
      if(w[i]->inward() && w[i]->boundary()==SimDetectorBoxId::ecal) {
	double rp(sqrt(w[i]->x()*w[i]->x()+w[i]->y()*w[i]->y()));
	_hERadius->Fill(rp);
      }
    }

    std::vector<const SimLayerTrack*>
      v(accessor.access<SimLayerTrack>());

    std::vector<const SimLayerTrack*> vTrack[20];
    
    for(unsigned i(0);i<v.size();i++) {
      assert(v[i]->layer()<_nLayers);
      vTrack[v[i]->layer()].push_back(v[i]);
    }

    double energyPerfect[20];

    double tEnergy[3][20],fEnergy[3][20];
    for(unsigned k(0);k<3;k++) {
      for(unsigned l(0);l<20;l++) {
	energyPerfect[l]=0.0;
	tEnergy[k][l]=0.0;
	fEnergy[k][l]=0.0;
      }
    }

    for(unsigned i(0);i<_nLayers;i++) {
      unsigned nPerfect[20]={0,0,0,0,0,0,0,0,0,0,
			     0,0,0,0,0,0,0,0,0,0};

      for(unsigned j(0);j<vTrack[i].size();j++) {
	if(vTrack[i][j]->type()==SimDetectorBoxId::ecalSiEpitaxial &&
	   vTrack[i][j]->totalEnergy()>0.0) {

	  if(_bunchTrain.bunchTrainNumberInRun()==0) 
	    _hEvent->Fill(i,vTrack[i][j]->xAverage(),vTrack[i][j]->yAverage());
	  
	  double radius(sqrt(vTrack[i][j]->xAverage()*vTrack[i][j]->xAverage()+
			     vTrack[i][j]->yAverage()*vTrack[i][j]->yAverage()));

	  for(unsigned l(0);l<20;l++) {
	    double radiusCut(2.0*(l+1));
	    if(radius<radiusCut) {
	      nPerfect[l]++;
	      energyPerfect[l]+=_weight[i];
	    }
	  }
	  
	  for(unsigned k(0);k<3;k++) {
	    for(unsigned l(0);l<20;l++) {
	      unsigned tNineSensor(_tNineSensor);
	      double tSensorSize(_tSensorSize);
	      double tSensorGap(_tSensorGap);
	    
	      if(k==0) tNineSensor=l;
	      if(k==1) tSensorSize=20.0+0.5*l;
	      if(k==2) tSensorGap=0.1*l;
	      
	      bool inSensor(false);
	      int nS(2);
	      if(i<tNineSensor) nS=1;

	      for(int x(0);x<=nS;x++) {
		if(fabs(vTrack[i][j]->xAverage())>=(x-0.5)*tSensorSize+x*tSensorGap &&
		   fabs(vTrack[i][j]->xAverage())< (x+0.5)*tSensorSize+x*tSensorGap) {
		  for(int y(0);y<=nS;y++) {
		    if(fabs(vTrack[i][j]->yAverage())>=(y-0.5)*tSensorSize+y*tSensorGap &&
		       fabs(vTrack[i][j]->yAverage())< (y+0.5)*tSensorSize+y*tSensorGap) {
		      inSensor=true;
		    }
		  }
		}
	      }
	      
	      if(inSensor) {
		tEnergy[k][l]+=_weight[i];
		//_hTXY[k][l][i]->Fill(vTrack[i][j]->xAverage(),vTrack[i][j]->yAverage());
	      }
	    }
	  }
	  
	  for(unsigned k(0);k<3;k++) {
	    for(unsigned l(0);l<20;l++) {
	      unsigned fSingleSensor(_fSingleSensor);
	      double fSensorSize(_fSensorSize);
	      double fSensorGap(_fSensorGap);
	    
	      if(k==0) fSingleSensor=l;
	      if(k==1) fSensorSize=40.0+2.5*l;
	      if(k==2) fSensorGap=0.1*l;
	      
	      bool inSensor;
	      if(i<fSingleSensor) {
		inSensor=fabs(vTrack[i][j]->xAverage())<0.5*fSensorSize &&
		  fabs(vTrack[i][j]->yAverage())<0.5*fSensorSize;
	      } else {
		inSensor=fabs(vTrack[i][j]->xAverage())<fSensorSize+0.5*fSensorGap &&
		  fabs(vTrack[i][j]->yAverage())<fSensorSize+0.5*fSensorGap &&
		  fabs(vTrack[i][j]->xAverage())>=0.5*fSensorGap &&
		  fabs(vTrack[i][j]->yAverage())>=0.5*fSensorGap;
	      }
	      
	      if(inSensor) {
		fEnergy[k][l]+=_weight[i];
		//_hFXY[k][l][i]->Fill(vTrack[i][j]->xAverage(),vTrack[i][j]->yAverage());
	      }
	    }
	  }

	}
      }

      for(unsigned l(0);l<20;l++) {
	_hNPerfect[l]->Fill(i,nPerfect[l]);
      }
    }

    for(unsigned l(0);l<20;l++) {
      _hEnergyPerfect[l]->Fill(energyPerfect[l]);
    }

    for(unsigned k(0);k<3;k++) {
      for(unsigned l(0);l<20;l++) {
	_hTEnergy[k][l]->Fill(tEnergy[k][l]);
	_hFEnergy[k][l]->Fill(fEnergy[k][l]);
      }
    }

    return true;
  }
  
private:
  bool _doneFiles;
  
  unsigned _nLayers;
  unsigned _nThinTungsten;

  double _weight[20];

  // TPAC
  unsigned _tNineSensor;
  double _tSensorSize;
  double _tSensorGap;

  // 4T
  unsigned _fSingleSensor;
  double _fSensorSize;
  double _fSensorGap;

  TH1F *_hPRadius;
  TH1F *_hERadius;

  TH3F *_hEvent;

  TProfile *_hNPerfect[20];
  TH1F *_hEnergyPerfect[20];

  TH1F *_hTNRatio[3][20];
  TH1F *_hTEnergy[3][20];
  TH2F *_hTXY[3][20][20];

  TH1F *_hFNRatio[3][20];
  TH1F *_hFEnergy[3][20];
  TH2F *_hFXY[3][20][20];
};

#endif
