#ifndef DecConfiguration_HH
#define DecConfiguration_HH

#include <vector>
#include <fstream>
#include <iostream>

#include "RcdUserRW.hh"

#include "SubInserter.hh"
#include "SubAccessor.hh"


class DecConfiguration : public RcdUserRW {

public:
  DecConfiguration() {
  }

  virtual ~DecConfiguration() {
  }

  bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "DecConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    SubInserter inserter(r);

    // Check record type
    switch (r.recordType()) {
      
    case RcdHeader::runStart: {

      SubAccessor accessor(r);
      std::vector<const IlcRunStart*>
	v(accessor.access<IlcRunStart>());
      assert(v.size()==1);
      _runStart=*(v[0]);

      if(_runStart.runType().majorType()==IlcRunType::dec) {
	SubInserter inserter(r);

	SimDetectorData *d(inserter.insert<SimDetectorData>(true));
	d->type(SimDetectorData::ecalVolume);
	d->layer(0);
	d->material(SimDetectorBox::air);

	double boxX0[3]={3.5,0.0,0.0};

	// Basic parameters
	unsigned nLayers=16;
	unsigned nThinTungsten=10;
	double thinTungsten=3.0;
	double ratioTungsten=2.0;
	double zGap=3.0;
	double zStart=-1.0;

	// Changes
	zStart=-50000.0;


	// Fill detector here
	SimDetectorBox box;

	double z(0.0);
	double x0(0.0);

	for(unsigned l(0);l<nLayers;l++) {
	  double dz(thinTungsten);
	  if(l>=nThinTungsten) dz=ratioTungsten*thinTungsten;

	  box.xCentre(0.0);
	  box.xWidth(250.0);
	  box.yCentre(0.0);
	  box.yWidth(250.0);
	  box.zThickness(dz);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(dz/boxX0[0]);
	  box.type(SimDetectorBoxId::ecalConverter);
	  box.layer(l);
	  box.material(SimDetectorBox::tungsten);
	  d->addBox(box);

	  z+=box.zThickness()+1.0;
	  x0+=dz/boxX0[0];

	  box.xCentre(0.0);
	  box.xWidth(250.0);
	  box.yCentre(0.0);
	  box.yWidth(250.0);
	  box.zThickness(0.7);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalPcb);
	  box.layer(l);
	  box.material(SimDetectorBox::pcb);
	  d->addBox(box);

	  z+=box.zThickness();

	  box.xCentre(0.0);
	  box.xWidth(200.0);
	  box.yCentre(0.0);
	  box.yWidth(200.0);
	  box.zThickness(0.300-0.012);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiBulk);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);

	  z+=box.zThickness();

	  box.xCentre(0.0);
	  box.xWidth(200.0);
	  box.yCentre(0.0);
	  box.yWidth(200.0);
	  box.zThickness(0.012);
	  box.zCentre(z+box.zHalfSize());
	  box.radiationLength(0);
	  box.type(SimDetectorBoxId::ecalSiEpitaxial);
	  box.layer(l);
	  box.material(SimDetectorBox::silicon);
	  d->addBox(box);

	  if(l<15) z+=box.zThickness()+zGap;
	  else     z+=box.zThickness();
	}

	d->resize();
	d->radiationLength(x0);

	inserter.extend(d->numberOfExtraBytes());

	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;


	SimPrimaryData *p(inserter.insert<SimPrimaryData>(true));
	
	if(_runStart.runType().type()==IlcRunType::decPhoton) {
	  p->particleId( 22);
	  p->mass(0.0);
	}
	if(_runStart.runType().type()==IlcRunType::decPositron) {
	  p->particleId(-11);
	  p->mass(0.51099906);
	}
	if(_runStart.runType().type()==IlcRunType::decElectron) {
	  p->particleId( 11);
	  p->mass(0.51099906);
	}
	/*
	if(_runStart.runType().type()==IlcRunType::decMuPlus) {
	  p->particleId(-13);
	  p->mass(105.6584);
	}
	if(_runStart.runType().type()==IlcRunType::decMuMinus) {
	  p->particleId( 13);
	  p->mass(105.6584);
	}
	if(_runStart.runType().type()==IlcRunType::decPiPlus) {
	  p->particleId( 211);
	  p->mass(139.5700);
	}
	if(_runStart.runType().type()==IlcRunType::decPiMinus) {
	  p->particleId(-211);
	  p->mass(139.5700);
	}
	if(_runStart.runType().type()==IlcRunType::decPiZero) {
	  p->particleId( 111);
	  p->mass(134.9764);
	}
	if(_runStart.runType().type()==IlcRunType::decProton) {
	  p->particleId( 2212);
	  p->mass(938.27231);
	}
	if(_runStart.runType().type()==IlcRunType::decAntiProton) {
	  p->particleId(-2212);
	  p->mass(938.27231);
	}
	if(_runStart.runType().type()==IlcRunType::decNeutron) {
	  p->particleId( 2112);
	  p->mass(939.56563);
	}
	if(_runStart.runType().type()==IlcRunType::decAntiNeutron) {
	  p->particleId(-2112);
	  p->mass(939.56563);
	}
	*/
	p->x(0.0);
	p->y(0.0);
	p->z(zStart);

	p->smearMatrix(0,0,1.0);
	p->smearMatrix(1,1,1.0);

	p->px(0.0);
	p->py(0.0);
	p->pz(1000.0*_runStart.runType().version());

	if(doPrint(r.recordType(),1)) p->print(std::cout," ") << std::endl;
      }

      break;
    }
      
    case RcdHeader::configurationStart: {
     
      if(_runStart.runType().majorType()==IlcRunType::dec) {
      }

      break;
    }
      
    default: {
      break;
    }
    };

    return true;
  }

private:
  IlcRunStart _runStart;
};

#endif
