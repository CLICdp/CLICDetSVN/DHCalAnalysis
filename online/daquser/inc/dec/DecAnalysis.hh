#ifndef DecAnalysis_HH
#define DecAnalysis_HH

#include <cassert>

#include "RcdMultiUserRO.hh"

//#include "DecAnalysisPrimaries.hh"
//#include "DecAnalysisLayerTracks.hh"
#include "DecAnalysisResolution.hh"
//#include "DecAnalysisHits.hh"


class DecAnalysis : public RcdMultiUserRO {

public:

  // Set default bits to enable required histograms
  enum {
    defaultDecAnalysisBits=0x3
  };


  DecAnalysis(const UtlPack bits=defaultDecAnalysisBits) {

    _ignorRunType=true;

    // Add modules
    //if(bits.bit( 0)) addUser(*(new DecAnalysisDigitisation()));
    //else std::cout << "DecAnalysisDigitisation disabled" << std::endl;

    //if(bits.bit( 0)) addUser(*(new DecAnalysisPrimaries()));
    //else std::cout << "DecAnalysisHits disabled" << std::endl;

    if(bits.bit( 1)) addUser(*(new DecAnalysisResolution()));
    else std::cout << "DecAnalysisResolution disabled" << std::endl;

    //if(bits.bit( 2)) addUser(*(new DecAnalysisHits()));
    //else std::cout << "DecAnalysisHits disabled" << std::endl;
  }

  virtual ~DecAnalysis() {
    deleteAll();
  }

  virtual bool record(const RcdRecord &r) {

    // Possibly set print level
    if(!_ignorRunType) {
      if(r.recordType()==RcdHeader::runStart) {
	SubAccessor accessor(r);
	std::vector<const IlcRunStart*>
	  v(accessor.extract<IlcRunStart>());
        if(v.size()==1) printLevel(v[0]->runType().printLevel());
      }
    }

    return RcdMultiUserRO::record(r);
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }

private:
  bool _ignorRunType;
};

#endif
