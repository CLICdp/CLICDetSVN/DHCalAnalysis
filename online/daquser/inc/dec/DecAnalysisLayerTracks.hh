#ifndef DecAnalysisLayerTracks_HH
#define DecAnalysisLayerTracks_HH

#include <sstream>
#include <cassert>

#include "RcdUserRO.hh"
#include "SubAccessor.hh"

#include "DecAnalysisBase.hh"

#include "FindMatrix.cc"
#include "TVectorD.h"


class DecAnalysisLayerTracks : public DecAnalysisBase {

public:
  DecAnalysisLayerTracks() : DecAnalysisBase("DecAnalysisLayerTracks") {
    _nHits025=new unsigned[4096*4096];
    _nHits050=new unsigned[2048*2048];
    _nHits100=new unsigned[1024*1024];
    _nHits200=new unsigned[ 512* 512];
    _doneFiles=true;
  }

  virtual ~DecAnalysisLayerTracks() {
    if(!_doneFiles) doFiles();
    endRoot();
  }

  virtual bool decAnalysisValidRun(IlcRunType rt) const {
    return rt.majorType()==IlcRunType::dec;
    //return true;
  }

  bool runStart(const RcdRecord &r) {
    if(!_doneFiles) doFiles();
    _doneFiles=false;

    _foutD.open((decAnalysisName()+"DEvent"+_runLabel+".txt").c_str());
    _foutA.open((decAnalysisName()+"AEvent"+_runLabel+".txt").c_str());
    _foutP.open((decAnalysisName()+"PEvent"+_runLabel+".txt").c_str());

    TH1::SetDefaultSumw2();

    double dwNaive[30]=
      {0.0106,0.0106,0.0106,0.0106,0.0106,
       0.0106,0.0106,0.0106,0.0106,0.0106,
       0.0106,0.0106,0.0106,0.0106,0.0106,
       0.0106,0.0106,0.0106,0.0106,0.0159,
       0.0212,0.0212,0.0212,0.0212,0.0212,
       0.0212,0.0212,0.0212,0.0212,0.0159};
   double dwOpt[30]=
     {0.0127699,0.0116588,0.00984833,0.010807,0.0105522,
      0.0103449,0.0106169,0.010431,0.0104194,0.0105355,
      0.0102702,0.010153,0.0105746,0.0104268,0.0103391,
      0.0106429,0.0113137,0.0116009,0.0122451,0.0144361,
      0.0170243,0.0184835,0.0194802,0.0211594,0.0208018,
      0.020763,0.023471,0.0250162,0.0244273,0.0612095};
   double dwReOpt[30]=
  {0.0129719,0.0115536,0.0101643,0.0110185,0.0109019,
     0.0106937,0.0109833,0.010791,0.0108393,0.0108068,
     0.0106776,0.0104519,0.0108365,0.0107455,0.0106588,
     0.0109494,0.0114827,0.0118214,0.0126061,0.0147735,
     0.0173733,0.0188669,0.019964,0.0215348,0.0212753,
     0.0211925,0.024028,0.0252205,0.0251589,0.0673582};
   double awNaive[30]=
     {0.0375,0.0375,0.0375,0.0375,0.0375,
      0.0375,0.0375,0.0375,0.0375,0.0375,
      0.0375,0.0375,0.0375,0.0375,0.0375,
      0.0375,0.0375,0.0375,0.0375,0.0562,
      0.0750,0.0750,0.0750,0.0750,0.0750,
      0.0750,0.0750,0.0750,0.0750,0.0562};
   double awOpt[30]=
  {0.0669559,0.0295526,0.0372927,0.0371939,0.0373242,
     0.0365076,0.0369706,0.0367537,0.0361062,0.0360607,
     0.0363555,0.0361178,0.0367021,0.0358151,0.0371471,
     0.0388385,0.0398056,0.0408036,0.0442727,0.048062,
     0.0570916,0.06352,0.0673586,0.0729336,0.0747842,
     0.0741498,0.0859879,0.0916199,0.096787,0.166336};
   
   TH1F *hDWNaive=new TH1F("DWeightsNaive",
			   "Naive digital weights",
			   30,0.0,30.0);
   TH1F *hDWOpt=new TH1F("DWeightsOpt",
			 "Optimised digital weights",
			 30,0.0,30.0);
   TH1F *hDWReOpt=new TH1F("DWeightsReOpt",
			   "Re-optimised digital weights",
			   30,0.0,30.0);
   TH1F *hAWNaive=new TH1F("AWeightsNaive",
			   "Naive analogue weights",
			   30,0.0,30.0);
   TH1F *hAWOpt=new TH1F("AWeightsOpt",
			 "Optimised analogue weights",
			 30,0.0,30.0);

   for(unsigned i(0);i<30;i++) {
     _dWeightNaive[i]=dwNaive[i];
     _dWeight[i]=dwOpt[i];
     _dWeightReOpt[i]=dwReOpt[i];
      _aWeightNaive[i]=awNaive[i];
      _aWeight[i]=awOpt[i];

      hDWNaive->Fill(i,dwNaive[i]);
      hDWOpt->Fill(i,dwOpt[i]);
      hDWReOpt->Fill(i,dwReOpt[i]);
      hAWNaive->Fill(i,awNaive[i]);
      hAWOpt->Fill(i,awOpt[i]);
    }

    /*
      _hErg=new TH1F((_runLabel+"Erg").c_str(),
      (_runTitle+", Weighted hit estimator").c_str(),
      100,0.0,500.0*_runStart.runType().version());

      _hErg2=new TH1F((_runLabel+"Erg2").c_str(),
      (_runTitle+", Weighted track estimator").c_str(),
      100,0.0,500.0*_runStart.runType().version());

      _hHvsT=new TH2F((_runLabel+"HvsT").c_str(),
      (_runTitle+", Number of pixel hits vs tracks").c_str(),
      100,0.0,50.0*_runStart.runType().version(),
      100,0.0,50.0*_runStart.runType().version());

      for(unsigned i(0);i<30;i++) {
      std::ostringstream sLabel;
      sLabel << _runLabel << "Layer" << std::setw(2) << std::setfill('0')
      << i << "Nhits";
      std::ostringstream sTitle;
      sTitle << _runTitle << ", Layer" << std::setw(2) << i
      << ", Number of pixel hits";

      _hNHits[i]=new TH1F(sLabel.str().c_str(),sTitle.str().c_str(),
      100,0.0,100.0);
      }

      for(unsigned i(0);i<30;i++) {
      std::ostringstream sLabel;
      sLabel << _runLabel << "Layer" << std::setw(2) << std::setfill('0')
      << i << "XYhits";
      std::ostringstream sTitle;
      sTitle << _runTitle << ", Layer" << std::setw(2) << i
      << ", X,Y of pixel hits";

      _hXYHits[i]=new TH2F(sLabel.str().c_str(),sTitle.str().c_str(),
      40,-200.0,200.0,40,-200.0,200.0);
      }
    */

    return true;
  }

  bool runEnd(const RcdRecord &r) {
    if(!_doneFiles) doFiles();
    return true;
  }

  void doFiles() {
    for(unsigned i(0);i<30;i++) {
      _hTLayerAll->Fill(i,_hTLayer[i]->GetBinContent(1));
    }

    _doneFiles=true;

    _foutD.close();
    _foutA.close();
    _foutP.close();

    std::cout << "Sizes = "
	      << _vDTracks.size() << " "
	      << _vATracks.size() << std::endl;

    TBTrack::FindMatrix fm;
    double sigmaCut(12.0);

    if(fm.findMatrix(_vDTracks,sigmaCut)) {
      TVectorD mean(fm.mean());
      TMatrixDSym eRawHit(fm.matrix());
        
      std::ofstream fout((decAnalysisName()+"D"+_runLabel+".txt").c_str());
      fout << std::setprecision(12);
      fout << fm.number() << " "
	   << TBTrack::FindMatrix::normalisationRatio(30,sigmaCut) << " "
	   << TBTrack::FindMatrix::meanSquareRatio(30,sigmaCut) << std::endl;
      for(unsigned i(0);i<30;i++) {
	fout << mean(i) << std::endl;
      }
      for(unsigned i(0);i<30;i++) {
	for(unsigned j(0);j<30;j++) {
	  fout << eRawHit(i,j) << std::endl;
	}
      }
    }

    if(fm.findMatrix(_vATracks,sigmaCut)) {
      TVectorD mean(fm.mean());
      TMatrixDSym eRawHit(fm.matrix());
      
      std::ofstream fout((decAnalysisName()+"A"+_runLabel+".txt").c_str());
      fout << std::setprecision(12);
      fout << fm.number() << " "
	   << TBTrack::FindMatrix::normalisationRatio(30,sigmaCut) << " "
	   << TBTrack::FindMatrix::meanSquareRatio(30,sigmaCut) << std::endl;
      for(unsigned i(0);i<30;i++) {
	fout << mean(i) << std::endl;
      }
      for(unsigned i(0);i<30;i++) {
	for(unsigned j(0);j<30;j++) {
	  fout << eRawHit(i,j) << std::endl;
	}
      }
    }

    if(fm.findMatrix(_vDPixels,sigmaCut)) {
      TVectorD mean(fm.mean());
      TMatrixDSym eRawHit(fm.matrix());
      
      std::ofstream fout((decAnalysisName()+"P"+_runLabel+".txt").c_str());
      fout << std::setprecision(12);
      fout << fm.number() << " "
	   << TBTrack::FindMatrix::normalisationRatio(30,sigmaCut) << " "
	   << TBTrack::FindMatrix::meanSquareRatio(30,sigmaCut) << std::endl;
      for(unsigned i(0);i<30;i++) {
	fout << mean(i) << std::endl;
      }
      for(unsigned i(0);i<30;i++) {
	for(unsigned j(0);j<30;j++) {
	  fout << eRawHit(i,j) << std::endl;
	}
      }
    }
  }

  bool configurationStart(const RcdRecord &r) {
    unsigned nLayer((unsigned)(0.02*_vSimPrimaryData[0].energy()));
    unsigned nLayerBins(nLayer);
    if(nLayerBins>1000) nLayerBins=1000;
  
    unsigned naiveBins(150);
    if(_vSimPrimaryData[0].energy()<8000.0) naiveBins=75;

    _hDNaiveEnergy=new TH1F("DNaiveEnergy",
		       (_runTitle+";DECAL energy").c_str(),
		       naiveBins,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hANaiveEnergy=new TH1F("ANaiveEnergy",
		       (_runTitle+";AECAL energy").c_str(),
		       naiveBins,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hDEnergy=new TH1F("DEnergy",
		       (_runTitle+";DECAL energy").c_str(),
		       150,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hAEnergy=new TH1F("AEnergy",
		       (_runTitle+";AECAL energy").c_str(),
		       150,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hDPixelEnergy=new TH1F("DPixelEnergy",
		       (_runTitle+";DECAL energy").c_str(),
		       150,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hDReOptEnergy=new TH1F("DReOptEnergy",
		       (_runTitle+";DECAL energy").c_str(),
		       150,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hEcalEnergy=new TH1F("EcalEnergy",
		       (_runTitle+";Deposited energy").c_str(),
		       150,0.0,0.0015*_vSimPrimaryData[0].energy());
    _hDEnergyOverEcal=new TH1F("DEnergyOverEcal",
			       (_runTitle+";DECAL energy/deposited energy").c_str(),
			       150,0.0,1.5);
    _hAEnergyOverEcal=new TH1F("AEnergyOverEcal",
			       (_runTitle+";AECAL energy/deposited energy").c_str(),
			       150,0.0,1.5);
    
    _hNLayerAll=new TH2F("NLayerAll",
			 (_runTitle+";Number of tracks vs layer").c_str(),
			 30,0.0,30.0,100,0.0,0.05*_vSimPrimaryData[0].energy());
    _hDLayerAll=new TH2F("DLayerAll",
			 (_runTitle+";Number of epitaxial tracks vs layer").c_str(),
			 30,0.0,30.0,100,0.0,0.025*_vSimPrimaryData[0].energy());
    _hALayerAll=new TH2F("ALayerAll",
			 (_runTitle+";Analogue energy vs layer").c_str(),
			 30,0.0,30.0,100,0.0,0.01*_vSimPrimaryData[0].energy());
    _hBLayerAll=new TH2F("BLayerAll",
			 (_runTitle+";Track entry/exit codes vs layer").c_str(),
			 30,0.0,30.0,4,0.0,4.0);
    _hILayerAll=new TH2F("ILayerAll",
			 (_runTitle+";Track particle id vs layer").c_str(),
			 30,0.0,30.0,5000,-2500.0,2500.0);
    _hRLayerAll=new TH2F("RLayerAll",
			 (_runTitle+";Track radius (mm) vs layer").c_str(),
			 30,0.0,30.0,100,0.0,1.0);
    _hTLayerAll=new TH1F("TLayerAll",
			 (_runTitle+";Core density vs layer").c_str(),
			 30,0.0,30.0);
    _hPLayerAll[0]=new TH2F("PLayer0All",
			    (_runTitle+";Number of tracks/25mu pixels vs layer").c_str(),
			    30,0.0,30.0,20,0.0,20.0);
    _hPLayerAll[1]=new TH2F("PLayer1All",
			    (_runTitle+";Number of tracks/50mu pixels vs layer").c_str(),
			    30,0.0,30.0,20,0.0,20.0);
    _hPLayerAll[2]=new TH2F("PLayer2All",
			    (_runTitle+";Number of tracks/100mu pixels vs layer").c_str(),
			    30,0.0,30.0,20,0.0,20.0);
    _hPLayerAll[3]=new TH2F("PLayer3All",
			    (_runTitle+";Number of tracks/200mu pixels vs layer").c_str(),
			    30,0.0,30.0,20,0.0,20.0);
    _hSLayerAll[0]=new TH2F("SLayer0All",
			    (_runTitle+";Number of hit 25mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
    _hSLayerAll[1]=new TH2F("SLayer1All",
			    (_runTitle+";Number of hit 50mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
    _hSLayerAll[2]=new TH2F("SLayer2All",
			    (_runTitle+";Number of hit 100mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
    _hSLayerAll[3]=new TH2F("SLayer3All",
			    (_runTitle+";Number of hit 200mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);



    for(unsigned i(0);i<30;i++) {
      std::ostringstream sout;
      sout << i;

      _hNLayer[i]=new TH1F((std::string("NLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of tracks").c_str(),
			   100,0.0,0.05*_vSimPrimaryData[0].energy());

      _hDLayer[i]=new TH1F((std::string("DLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of epitaxial tracks").c_str(),
			   100,0.0,0.025*_vSimPrimaryData[0].energy());

      _hALayer[i]=new TH1F((std::string("ALayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Analogue energy").c_str(),
			   100,0.0,0.01*_vSimPrimaryData[0].energy());

      _hBLayer[i]=new TH1F((std::string("BLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Track entry/exit code").c_str(),
			   4,0.0,4.0);

      _hILayer[i]=new TH1F((std::string("ILayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Track particle id").c_str(),
			   5000,-2500.0,2500.0);

      _hRLayer[i]=new TH1F((std::string("RLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Track radius (mm)").c_str(),
			   100,0.0,1.0);
      _hHLayer[i]=new TH1F((std::string("HLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Track radius (mm);Track density/mm^{2}").c_str(),
			   100,0.0,1.0);
      _hTLayer[i]=new TH1F((std::string("TLayer")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Track radius (mm);Track density/mm^{2}").c_str(),
			   100,0.0,1.0);
      _hPLayer[i][0]=new TH1F((std::string("PLayer0")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of tracks/25mu pixel").c_str(),
			   20,0.0,20.0);
      _hPLayer[i][1]=new TH1F((std::string("PLayer1")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of tracks/50mu pixel").c_str(),
			   20,0.0,20.0);
      _hPLayer[i][2]=new TH1F((std::string("PLayer2")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of tracks/100mu pixel").c_str(),
			   20,0.0,20.0);
      _hPLayer[i][3]=new TH1F((std::string("PLayer3")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of tracks/200mu pixel").c_str(),
			   20,0.0,20.0);
      _hSLayer[i][0]=new TH2F((std::string("SLayer0")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of hit 25mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
      _hSLayer[i][1]=new TH2F((std::string("SLayer1")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of hit 50mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
      _hSLayer[i][2]=new TH2F((std::string("SLayer2")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of hit 100mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
      _hSLayer[i][3]=new TH2F((std::string("SLayer3")+sout.str()).c_str(),
			   (_runTitle+", Layer "+sout.str()+";Number of hit 200mu pixels vs epitaxial tracks").c_str(),
			    nLayerBins,0,nLayer,nLayerBins,0,nLayer);
    }

    return true;
  }
  
  bool bunchTrain(const RcdRecord &r) {
    SubAccessor accessor(r);

    std::vector<const SimParticle*>
      w(accessor.access<SimParticle>());
    assert(w.size()>0);
    assert(w[0]->boundary()==0);

    std::vector<const SimLayerTrack*>
      v(accessor.access<SimLayerTrack>());

    std::vector<const SimLayerTrack*> vTrack[30];
    
    for(unsigned i(0);i<v.size();i++) {
      assert(v[i]->layer()<30);
      vTrack[v[i]->layer()].push_back(v[i]);
    }

    std::vector<const SimDetectorEnergies*>
      ve(accessor.access<SimDetectorEnergies>());
    assert(ve.size()==1);

    double ecalEnergyTotal(0.001*ve[0]->totalEnergy());
    _hEcalEnergy->Fill(ecalEnergyTotal);

    double dEnergySum[30],aEnergySum[30];
    for(unsigned i(0);i<ve[0]->numberOfBoxes();i++) {
      if(ve[0]->boxEnergy(i)->type()==SimDetectorBoxId::ecalSiEpitaxial) {
	assert(ve[0]->boxEnergy(i)->layer()<30);
	dEnergySum[ve[0]->boxEnergy(i)->layer()]=ve[0]->boxEnergy(i)->energy();
      }
      if(ve[0]->boxEnergy(i)->type()==SimDetectorBoxId::ecalSiBulk) {
	assert(ve[0]->boxEnergy(i)->layer()<30);
	aEnergySum[ve[0]->boxEnergy(i)->layer()]=ve[0]->boxEnergy(i)->energy();
      }
    }

    double dEnergy(0.0);
    double aEnergy(0.0);
    double dWEnergy(0.0);
    double aWEnergy(0.0);
    double dWNaiveEnergy(0.0);
    double aWNaiveEnergy(0.0);

    TVectorD dTracks(30);
    TVectorD aTracks(30);

    for(unsigned i(0);i<30;i++) {
      _hNLayerAll->Fill(i,vTrack[i].size());
      _hNLayer[i]->Fill(vTrack[i].size());

      unsigned dTrack(0);
      double aTrack(0.0);

      for(unsigned j(0);j<vTrack[i].size();j++) {
	if(vTrack[i][j]->type()==SimDetectorBoxId::ecalSiEpitaxial &&
	   vTrack[i][j]->totalEnergy()>0.0) {
	  dTrack++;

	  unsigned e(0);
	  if(vTrack[i][j]->entry()) e+=1;
	  if(vTrack[i][j]->exit())  e+=2;
	  _hBLayerAll->Fill(i,e);
	  _hBLayer[i]->Fill(e);
	  
	  _hILayerAll->Fill(i,vTrack[i][j]->particleId());
	  _hILayer[i]->Fill(vTrack[i][j]->particleId());
	  
	  double dx(vTrack[i][j]->xAverage()-w[0]->x());
	  double dy(vTrack[i][j]->yAverage()-w[0]->y());
	  double r2(dx*dx+dy*dy);
	  double r(sqrt(r2));

	  _hRLayerAll->Fill(i,r);
	  _hRLayer[i]->Fill(r);
	  _hHLayer[i]->Fill(r,1.0/(2.0*acos(-1.0)*0.01*r));
	  
	  unsigned nBin((unsigned)(r/0.01));
	  double area(acos(-1.0)*0.01*0.01*(2.0*nBin+1.0));
	  _hTLayer[i]->Fill(r,1/area);
	}

	aTrack+=vTrack[i][j]->totalEnergy();
      }

      if(fabs(aEnergySum[i] + dEnergySum[i] -aTrack)/(aTrack+0.0000001) > 0.001) {
	std::cout << "ATRACK" << aTrack << " "
		  << aEnergySum[i] << " "
		  << dEnergySum[i] << " "
		  << aEnergySum[i] + dEnergySum[i] << " "
		  << aEnergySum[i] + dEnergySum[i] -aTrack<< std::endl;
      }

      dTracks(i)=dTrack;
      aTracks(i)=aTrack;

      _foutD << " " << dTrack;
      _foutA << " " << aTrack;

      _hDLayerAll->Fill(i,dTrack);
      _hDLayer[i]->Fill(dTrack);
      _hALayerAll->Fill(i,aTrack);
      _hALayer[i]->Fill(aTrack);

      dEnergy+=dTrack;
      aEnergy+=aTrack;
      dWNaiveEnergy+=_dWeightNaive[i]*dTrack;
      aWNaiveEnergy+=_aWeightNaive[i]*aTrack;
      dWEnergy+=_dWeight[i]*dTrack;
      aWEnergy+=_aWeight[i]*aTrack;
    }

    _foutD << std::endl;
    _foutA << std::endl;

      if(fabs(ve[0]->totalEnergy(SimDetectorBoxId::ecalSiBulk)+
	      ve[0]->totalEnergy(SimDetectorBoxId::ecalSiEpitaxial)-aEnergy)/(aEnergy+0.000001) > 0.001) {
	std::cout << "AENERGY" << aEnergy << " "
		  << ve[0]->totalEnergy(SimDetectorBoxId::ecalSiBulk) << " "
		  << ve[0]->totalEnergy(SimDetectorBoxId::ecalSiEpitaxial) << " "
		  << ve[0]->totalEnergy(SimDetectorBoxId::ecalSiBulk)+
	  ve[0]->totalEnergy(SimDetectorBoxId::ecalSiEpitaxial) <<  " "
		  << ve[0]->totalEnergy(SimDetectorBoxId::ecalSiBulk)+
	  ve[0]->totalEnergy(SimDetectorBoxId::ecalSiEpitaxial)-aEnergy << std::endl;
      }

    _hDNaiveEnergy->Fill(dWNaiveEnergy);
    _hANaiveEnergy->Fill(aWNaiveEnergy);

    _hDEnergy->Fill(dWEnergy);
    _hAEnergy->Fill(aWEnergy);
    _hDEnergyOverEcal->Fill(dWEnergy/ecalEnergyTotal);
    _hAEnergyOverEcal->Fill(aWEnergy/ecalEnergyTotal);

    _vDTracks.push_back(dTracks);
    _vATracks.push_back(aTracks);


    double dWPixelEnergy(0.0);
    double dWReOptEnergy(0.0);

    _pixelSize=0.025;
    for(unsigned i(0);i<30;i++) {
      std::vector< std::pair<unsigned,unsigned> > nHits025;
      std::vector< std::pair<unsigned,unsigned> > nHits050;
      std::vector< std::pair<unsigned,unsigned> > nHits100;
      std::vector< std::pair<unsigned,unsigned> > nHits200;

      unsigned nTrack(0);
      for(unsigned j(0);j<vTrack[i].size();j++) {
	if(vTrack[i][j]->type()==SimDetectorBoxId::ecalSiEpitaxial &&
	   vTrack[i][j]->totalEnergy()>0.0 &&
	   vTrack[i][j]->xAverage()>=-2048.0*_pixelSize &&
	   vTrack[i][j]->xAverage()<  2048.0*_pixelSize &&
	   vTrack[i][j]->yAverage()>=-2048.0*_pixelSize &&
	   vTrack[i][j]->yAverage()<  2048.0*_pixelSize) {

	  nTrack++;
	  unsigned x((unsigned)((vTrack[i][j]->xAverage()+2048.0*_pixelSize)/_pixelSize));
	  unsigned y((unsigned)((vTrack[i][j]->yAverage()+2048.0*_pixelSize)/_pixelSize));
	  assert(x<4096);
	  assert(y<4096);

	  bool found;

	  found=false;
	  for(unsigned k(0);k<nHits025.size() && !found;k++) {
	    if(nHits025[k].first==4096*x+y) {
	      found=true;
	      nHits025[k].second++;
	    }
	  }
	  if(!found) {
	    nHits025.push_back(std::pair<unsigned,unsigned>(4096*x+y,1));
	  }

	  found=false;
	  for(unsigned k(0);k<nHits050.size() && !found;k++) {
	    if(nHits050[k].first==2048*(x/2)+(y/2)) {
	      found=true;
	      nHits050[k].second++;
	    }
	  }
	  if(!found) {
	    nHits050.push_back(std::pair<unsigned,unsigned>(2048*(x/2)+(y/2),1));
	  }

	  found=false;
	  for(unsigned k(0);k<nHits100.size() && !found;k++) {
	    if(nHits100[k].first==1024*(x/4)+(y/4)) {
	      found=true;
	      nHits100[k].second++;
	    }
	  }
	  if(!found) {
	    nHits100.push_back(std::pair<unsigned,unsigned>(1024*(x/4)+(y/4),1));
	  }

	  found=false;
	  for(unsigned k(0);k<nHits200.size() && !found;k++) {
	    if(nHits200[k].first== 512*(x/8)+(y/8)) {
	      found=true;
	      nHits200[k].second++;
	    }
	  }
	  if(!found) {
	    nHits200.push_back(std::pair<unsigned,unsigned>( 512*(x/8)+(y/8),1));
	  }
	}
      }

      for(unsigned k(0);k<nHits025.size();k++) {
	_hPLayerAll[0]->Fill(i,nHits025[k].second);
	_hPLayer[i][0]->Fill(nHits025[k].second);
      }
      _hSLayerAll[0]->Fill(nTrack,nHits025.size());
      _hSLayer[i][0]->Fill(nTrack,nHits025.size());

      for(unsigned k(0);k<nHits050.size();k++) {
	_hPLayerAll[1]->Fill(i,nHits050[k].second);
	_hPLayer[i][1]->Fill(nHits050[k].second);
      }
      _hSLayerAll[1]->Fill(nTrack,nHits050.size());
      _hSLayer[i][1]->Fill(nTrack,nHits050.size());

      for(unsigned k(0);k<nHits100.size();k++) {
	_hPLayerAll[2]->Fill(i,nHits100[k].second);
	_hPLayer[i][2]->Fill(nHits100[k].second);
      }
      _hSLayerAll[2]->Fill(nTrack,nHits100.size());
      _hSLayer[i][2]->Fill(nTrack,nHits100.size());

      for(unsigned k(0);k<nHits200.size();k++) {
	_hPLayerAll[3]->Fill(i,nHits200[k].second);
	_hPLayer[i][3]->Fill(nHits200[k].second);
      }
      _hSLayerAll[3]->Fill(nTrack,nHits200.size());
      _hSLayer[i][3]->Fill(nTrack,nHits200.size());

      dTracks(i)=nHits050.size();
      _foutP << " " << nHits050.size();

      dWPixelEnergy+=_dWeightNaive[i]*dTracks(i);
      dWReOptEnergy+=_dWeightReOpt[i]*dTracks(i);
    }
    _foutP << std::endl;

    _hDPixelEnergy->Fill(dWPixelEnergy);
    _hDReOptEnergy->Fill(dWReOptEnergy);
    _vDPixels.push_back(dTracks);

    /*
    _pixelSize=0.025;
    for(unsigned i(0);i<30;i++) {

      unsigned nTrack(0);
      memset(_nHits025,0,4096*4096*sizeof(unsigned));
      memset(_nHits050,0,2048*2048*sizeof(unsigned));
      memset(_nHits100,0,1024*1024*sizeof(unsigned));
      memset(_nHits200,0, 512* 512*sizeof(unsigned));
      for(unsigned j(0);j<vTrack[i].size();j++) {
	if(vTrack[i][j]->type()==SimDetectorBoxId::ecalSiEpitaxial &&
	   vTrack[i][j]->totalEnergy()>0.0 &&
	   vTrack[i][j]->xAverage()>=-2048.0*_pixelSize &&
	   vTrack[i][j]->xAverage()<  2048.0*_pixelSize &&
	   vTrack[i][j]->yAverage()>=-2048.0*_pixelSize &&
	   vTrack[i][j]->yAverage()<  2048.0*_pixelSize) {

	  nTrack++;
	  unsigned x((unsigned)((vTrack[i][j]->xAverage()+2048.0*_pixelSize)/_pixelSize));
	  unsigned y((unsigned)((vTrack[i][j]->yAverage()+2048.0*_pixelSize)/_pixelSize));
	  assert(x<4096);
	  assert(y<4096);
	  _nHits025[4096* x   + y   ]++;
	  _nHits050[2048*(x/2)+(y/2)]++;
	  _nHits100[1024*(x/4)+(y/4)]++;
	  _nHits200[ 512*(x/8)+(y/8)]++;
	}
      }

      unsigned nH;

      nH=0;
      for(unsigned j(0);j<4096;j++) {
	for(unsigned k(0);k<4096;k++) {
	  if(_nHits025[4096*j+k]>0) {
	    _hPLayerAll[0]->Fill(i,_nHits025[4096*j+k]);
	    _hPLayer[i][0]->Fill(_nHits025[4096*j+k]);
	    nH++;
	  }
	}
      }
      _hSLayerAll[0]->Fill(nTrack,nH);
      _hSLayer[i][0]->Fill(nTrack,nH);

      nH=0;
      for(unsigned j(0);j<2048;j++) {
	for(unsigned k(0);k<2048;k++) {
	  if(_nHits050[2048*j+k]>0) {
	    _hPLayerAll[1]->Fill(i,_nHits050[2048*j+k]);
	    _hPLayer[i][1]->Fill(_nHits050[2048*j+k]);
	    nH++;
	  }
	}
      }
      _hSLayerAll[1]->Fill(nTrack,nH);
      _hSLayer[i][1]->Fill(nTrack,nH);

      nH=0;
      for(unsigned j(0);j<1024;j++) {
	for(unsigned k(0);k<1024;k++) {
	  if(_nHits100[1024*j+k]>0) {
	    _hPLayerAll[2]->Fill(i,_nHits100[1024*j+k]);
	    _hPLayer[i][2]->Fill(_nHits100[1024*j+k]);
	    nH++;
	  }
	}
      }
      _hSLayerAll[2]->Fill(nTrack,nH);
      _hSLayer[i][2]->Fill(nTrack,nH);

      nH=0;
      for(unsigned j(0);j< 512;j++) {
	for(unsigned k(0);k< 512;k++) {
	  if(_nHits200[ 512*j+k]>0) {
	    _hPLayerAll[3]->Fill(i,_nHits200[ 512*j+k]);
	    _hPLayer[i][3]->Fill(_nHits200[ 512*j+k]);
	    nH++;
	  }
	}
      }
      _hSLayerAll[3]->Fill(nTrack,nH);
      _hSLayer[i][3]->Fill(nTrack,nH);
    }
*/
    /*










    _hErg2->Fill(erg2);


    std::vector<const SimLayerHits*>
    w(accessor.access<SimLayerHits>());
    assert(w.size()==30);

    bool layer[30];
    for(unsigned i(0);i<30;i++) layer[i]=false;

    double erg(0.0);
    for(unsigned i(0);i<w.size();i++) {
    if(doPrint(r.recordType(),1)) w[i]->print(std::cout) << std::endl;

    assert(w[i]->layer()<30);
    assert(w[i]->layer()==i);
    assert(!layer[w[i]->layer()]);

    layer[w[i]->layer()]=true;
    _hNHits[w[i]->layer()]->Fill(w[i]->numberOfHits());
    _hHvsT->Fill(layerTracks[i],w[i]->numberOfHits());

    const SimPixelHit *sph(w[i]->hits());
    for(unsigned j(0);j<w[i]->numberOfHits();j++) {
    _hXYHits[w[i]->layer()]->Fill(sph[j].i(),sph[j].j());
    }

    if(i<20) erg+=w[i]->numberOfHits();
    else     erg+=2.0*w[i]->numberOfHits();
    }

    _hErg->Fill(erg);
    */
    return true;
  }

  std::pair<int,double> bin(double xy) {
    std::pair<int,double> ret;    
    if(xy>=0.0) {
      ret.first=(int)(xy/_pixelSize);
      ret.second=xy-ret.first*_pixelSize;
    } else {
      ret.first=(int)(xy/_pixelSize)-1;
      ret.second=xy-ret.first*_pixelSize;
    }
    return ret;
  }
  
private:
  bool _doneFiles;
  
  double _dWeightNaive[30];
  double _aWeightNaive[30];
  double _dWeight[30];
  double _aWeight[30];
  double _dWeightReOpt[30];

  std::ofstream _foutD;
  std::ofstream _foutA;
  std::ofstream _foutP;

  std::vector<TVectorD> _vDTracks;
  std::vector<TVectorD> _vATracks; 
  std::vector<TVectorD> _vDPixels;
 
  TH1F *_hDNaiveEnergy;
  TH1F *_hANaiveEnergy;
  TH1F *_hDEnergy;
  TH1F *_hAEnergy;
  TH1F *_hDPixelEnergy;
  TH1F *_hDReOptEnergy;
  TH1F *_hEcalEnergy;
  TH1F *_hDEnergyOverEcal;
  TH1F *_hAEnergyOverEcal;

  TH2F *_hNLayerAll;
  TH2F *_hDLayerAll;
  TH2F *_hALayerAll;
  TH2F *_hBLayerAll;
  TH2F *_hILayerAll;
  TH2F *_hRLayerAll;
  TH1F *_hTLayerAll;
  TH2F *_hPLayerAll[4];
  TH2F *_hSLayerAll[4];

  TH1F *_hNLayer[30];
  TH1F *_hDLayer[30];
  TH1F *_hALayer[30];
  TH1F *_hBLayer[30];
  TH1F *_hILayer[30];
  TH1F *_hRLayer[30];
  TH1F *_hHLayer[30];
  TH1F *_hTLayer[30];
  TH1F *_hPLayer[30][4];
  TH2F *_hSLayer[30][4];
  
  double _pixelSize;
  unsigned *_nHits025;
  unsigned *_nHits050;
  unsigned *_nHits100;
  unsigned *_nHits200;
  /*
    TH1F *_hErg;
    TH1F *_hErg2;
    TH2F *_hHvsT;
    TH1F *_hNHits[30];
    TH2F *_hXYHits[30];
  */
};

#endif
