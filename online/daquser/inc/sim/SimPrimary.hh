#ifndef SimPrimary_HH
#define SimPrimary_HH

#include <fstream>

#include "globals.hh"

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4UserRunAction.hh"
#include "G4UserEventAction.hh"
#include "G4UserTrackingAction.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4LorentzVector.hh"
#include "Randomize.hh"


//#include "DecalEvent.hh"
//#include "DecalRadiationLengths.hh"

#include "G4Run.hh"
#include "G4Event.hh"
#include "G4EventManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4VHitsCollection.hh"
#include "G4TrajectoryContainer.hh"
#include "G4Trajectory.hh"
#include "G4VVisManager.hh"
#include "G4SDManager.hh"
#include "G4UImanager.hh"
#include "G4ios.hh"

//#include "DecalFlags.hh"

//#include "SimPrimary.hh"
//#include "DecalEvent.hh"
//#include "DecalRadiationLengths.hh"
//#include "DecalRunNumber.hh"
//#include "DecalRun.hh"

#include "RcdArena.hh"
#include "SubInserter.hh"
//#include "RunMonitor.hh"
#include "RunWriterDataMultiFile.hh"


extern G4double expHall_energy;
extern G4double converter_energy;
extern G4double bulk_energy;
extern G4double epitaxial_energy;
extern G4double lost_energy;

class SimPrimary {
public:
  //SimPrimary(RcdRecord &r) : _record(r) {
  SimPrimary() {
    G4int n_particle = 1;
    particleGun = new G4ParticleGun(n_particle);

    _particleTable = G4ParticleTable::GetParticleTable();
  }

  ~SimPrimary() {
    delete particleGun;
  }
  /*
  bool runStart() {
    assert(_record.type()==RcdHeader::runStart);
    SubInserter inserter(_record);

    SimPrimaryRunData *sprd(inserter.insert<SimPrimaryRunData>(true));
    sprd->print(std::cout);
  }
  */
#ifndef DAQ_ILC_TIMING
  void runInfo(DaqRunType rt) {
    _runType=rt;
  }
#else
  void runInfo(IlcRunType rt) {
    _runType=rt;
  }
#endif

  void generatePrimaries(G4Event* anEvent) {

    if(_runType.majorType()!=IlcRunType::sim) return;

    std::cout << "SimPrimary primary" << std::endl;

    G4String particleName("geantino");
    if(_runType.type()==IlcRunType::simPhoton    ) particleName="gamma";
    if(_runType.type()==IlcRunType::simPositron  ) particleName="e+";
    if(_runType.type()==IlcRunType::simElectron  ) particleName="e-";
    if(_runType.type()==IlcRunType::simMuPlus    ) particleName="mu+";
    if(_runType.type()==IlcRunType::simMuMinus   ) particleName="mu-";
    if(_runType.type()==IlcRunType::simPiPlus    ) particleName="pi+";
    if(_runType.type()==IlcRunType::simPiMinus   ) particleName="pi-";
    if(_runType.type()==IlcRunType::simProton    ) particleName="proton";
    if(_runType.type()==IlcRunType::simAntiProton) particleName="anti_proton";

    particleGun->SetParticleDefinition(_particleTable->FindParticle(particleName));

    unsigned nrg=_runType.version();
    particleGun->SetParticleEnergy(nrg*GeV);
    particleGun->SetParticlePosition(G4ThreeVector(0.0, 0.0, -1.0*mm));
    particleGun->SetParticleMomentumDirection(G4ThreeVector(0.0,0.0,1.0));

    particleGun->GeneratePrimaryVertex(anEvent);
  }

private:
  //RcdRecord &_record;

  G4ParticleTable* _particleTable;

#ifndef DAQ_ILC_TIMING
  DaqRunType _runType;
#else
  IlcRunType _runType;
#endif

  G4ParticleGun* particleGun;
  G4double _energy[2];
  G4ThreeVector _threeVector[2];
};

#endif
