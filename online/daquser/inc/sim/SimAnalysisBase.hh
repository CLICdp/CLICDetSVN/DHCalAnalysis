#ifndef SimAnalysisBase_HH
#define SimAnalysisBase_HH

#include <cassert>

#include "TFile.h"
#include "TF1.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TProfile.h"

#include "RcdUserRO.hh"
#include "SubAccessor.hh"


class SimAnalysisBase : public RcdUserRO {

public:
  SimAnalysisBase(const std::string &name) :
    _simAnalysisName(name), _validRunType(true), _rootFile(0) {

    for(unsigned x(0);x<168;x++) {
      for(unsigned y(0);y<168;y++) {
	std::ostringstream pLabel;
	pLabel << "X" << std::setw(3) << std::setfill('0') << x
	       << "Y" << std::setw(3) << std::setfill('0') << y;
	_pixelLabel[x][y]=pLabel.str();

	std::ostringstream pTitle;
	pTitle << "X " << x << ", Y " << y;
	_pixelTitle[x][y]=pTitle.str();
      }
    }
  }

  virtual ~SimAnalysisBase() {

    // Close ROOT file when runEnd was not seen
    endRoot();
  }

  const std::string& simAnalysisName() const {
    return _simAnalysisName;
  }

  virtual bool simAnalysisValidRun(IlcRunType rt) const {
    return false;
  }

  virtual bool runStart(const RcdRecord &r) {
    return true;
  }

  virtual bool runEnd(const RcdRecord &r) {
    return true;
  }

  virtual bool configurationStart(const RcdRecord &r) {
    return true;
  }

  virtual bool configurationEnd(const RcdRecord &r) {
    return true;
  }

  virtual bool slowReadout(const RcdRecord &r) {
    return true;
  }

  virtual bool bunchTrain(const RcdRecord &r) {
    return true;
  }

  bool record(const RcdRecord &r) {
    if(doPrint(r.recordType()) && _validRunType) {
      std::cout << _simAnalysisName << "::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    SubAccessor accessor(r);

    switch (r.recordType()) {
  
    case RcdHeader::runStart: {

      // Find runStart object
      std::vector<const IlcRunStart*>
        vs(accessor.access<IlcRunStart>());
      assert(vs.size()==1);
      
      // Get local copy
      _runStart=*(vs[0]);
      
      // Check if this run is to be analysed
      _validRunType=simAnalysisValidRun(_runStart.runType());
      
      if(_validRunType) {
	if(doPrint(r.recordType(),1)) {
	  _runStart.print(std::cout," ") << std::endl;
	}
	
	// Open root file
        std::ostringstream sFile;
        sFile << _simAnalysisName << std::setfill('0') << std::setw(6)
              << _runStart.runNumber() << ".root";
	_rootFileName=sFile.str();
	
	std::cout << _simAnalysisName << "::record()  Creating ROOT file "
		  << _rootFileName << std::endl << std::endl;
        _rootFile = new TFile(sFile.str().c_str(),"RECREATE");
	
	// Make useful run labels and titles for histograms, etc.
	std::ostringstream rLabel;
	rLabel << "Run" << std::setfill('0') << std::setw(6)
	       << _runStart.runNumber();
	_runLabel=rLabel.str();
	
	std::ostringstream rTitle;
	rTitle << "Run " << _runStart.runNumber();
	_runTitle=rTitle.str();
	
	// Print out all the labels and titles
	if(doPrint(r.recordType(),1)) {
	  std::cout << " Run label = " << _runLabel
		    << " and title = " << _runTitle << std::endl;
	}
	
	std::vector<const SimPrimaryData*>
	  vp(accessor.access<SimPrimaryData>());
	
	_vSimPrimaryData.clear();
	for(unsigned i(0);i<vp.size();i++) {
	  if(doPrint(r.recordType(),1)) vp[i]->print(std::cout) << std::endl;
	  
	  if(vp[i]->primary()) _vSimPrimaryData.push_back(*(vp[i]));
	}

	// Call analysis method
	_rootFile->cd();
	runStart(r);
	
	
      } else {
	
	// Not a run to be analysed by this class
	  if(doPrint(r.recordType(),1)) {
	    std::cout << _simAnalysisName << "::record()  Run ignored"
		      << std::endl << std::endl;
	  }
      }

      break;
    }
      
    case RcdHeader::runEnd: {
      if(_validRunType) {

	// Print out runEnd information
	std::vector<const IlcRunEnd*>
	  ve(accessor.access<IlcRunEnd>());
	assert(ve.size()==1);
	if(doPrint(r.recordType(),1)) ve[0]->print(std::cout) << std::endl;

	// Call analysis method
	_rootFile->cd();
	runEnd(r);

	// Close root file
	endRoot();
      }

      // Reset run analysis switch
      _validRunType=true;
      break;
    }
  
    case RcdHeader::configurationStart: {
      if(_validRunType) {

	// Find configurationStart object
	std::vector<const IlcConfigurationStart*>
	  vs(accessor.access<IlcConfigurationStart>());
	assert(vs.size()==1);
	
	// Get local copy
	_configurationStart=*(vs[0]);

	if(doPrint(r.recordType(),1)) {
	  _configurationStart.print(std::cout," ") << std::endl;
	}

	// Make useful labels and titles
	std::ostringstream cLabel;
	cLabel << "Cfg" << std::setfill('0') << std::setw(6)
	       << _configurationStart.configurationNumberInRun();
	_cfgLabel=cLabel.str();
	
	std::ostringstream cTitle;
	cTitle << "Cfg " << _configurationStart.configurationNumberInRun();
	_cfgTitle=cTitle.str();

	if(doPrint(r.recordType(),1)) {
	  std::cout << " Configuration label = " << _cfgLabel
		    << " and title = " << _cfgTitle
		    << std::endl << std::endl;
	}

	// Call analysis method
	_rootFile->cd();
	configurationStart(r);
      }

      break;
    }
      
    case RcdHeader::configurationEnd: {
      if(_validRunType) {

	// Print out configurationEnd information
	std::vector<const IlcConfigurationEnd*>
	  ve(accessor.access<IlcConfigurationEnd>());
	assert(ve.size()==1);
	if(doPrint(r.recordType(),1)) {
	  ve[0]->print(std::cout," ") << std::endl;
	}

	// Call analysis method
	_rootFile->cd();
	configurationEnd(r);
      }

      break;
    }
  
    case RcdHeader::slowReadout: {
      if(_validRunType) {

	// Get local copy of slow readout object
	std::vector<const IlcSlowReadout*>
	  vs(accessor.access<IlcSlowReadout>());
	assert(vs.size()==1);

	_slowReadout=*(vs[0]);
	if(doPrint(r.recordType())) {
	  _slowReadout.print(std::cout," ") << std::endl;
	}

	// Call analysis method
	_rootFile->cd();
	slowReadout(r);
      }

      break;
    }
      
    case RcdHeader::bunchTrain: {
      if(_validRunType) {

	// Get local copy of bunch train object
	std::vector<const IlcBunchTrain*>
	  vb(accessor.access<IlcBunchTrain>());
	assert(vb.size()==1);

	_bunchTrain=*(vb[0]);
	if(doPrint(r.recordType())) {
	  _bunchTrain.print(std::cout," ") << std::endl;
	}

	// Make useful labels and titles
	std::ostringstream bLabel;
	bLabel << "Bnt" << std::setfill('0') << std::setw(6)
	       << _bunchTrain.bunchTrainNumberInRun();
	_bntLabel=bLabel.str();
	
	std::ostringstream bTitle;
	bTitle << "Bnt " << _bunchTrain.bunchTrainNumberInRun();
	_bntTitle=bTitle.str();
	
	if(doPrint(r.recordType(),1)) {
	  std::cout << " Bunch train label = " << _bntLabel
		    << " and title = " << _bntTitle
		    << std::endl << std::endl;
	}

	// Call analysis method
	_rootFile->cd();
	bunchTrain(r);
      }
      break;
    }
      
      
    default: {
      break;
    }
    };
    
    return true;
  }

  virtual void endRoot() {
    if(_rootFile!=0) {
      std::cout << _simAnalysisName << "::endRoot()  "
		<< "Closing ROOT file " << _rootFileName << std::endl;

      _rootFile->cd();
      _rootFile->Write();
      _rootFile->Close();

      delete _rootFile;
      _rootFile=0;
    }
  }


private:
  std::string _simAnalysisName;
  bool _validRunType;

protected:
  TFile* _rootFile;
  std::string _rootFileName;

  // Filled in ctor
  std::string _pixelLabel[168][168];
  std::string _pixelTitle[168][168];

  // Filled at runStart
  IlcRunStart _runStart;
  std::string _runLabel;
  std::string _runTitle;

  // Filled at configurationStart
  IlcConfigurationStart _configurationStart;
  std::string _cfgLabel;
  std::string _cfgTitle;

  std::vector<SimPrimaryData> _vSimPrimaryData;

  // Filled at slowReadout
  IlcSlowReadout _slowReadout;

  // Filled at bunchTrain
  IlcBunchTrain _bunchTrain;
  std::string _bntLabel;
  std::string _bntTitle;
};

#endif
