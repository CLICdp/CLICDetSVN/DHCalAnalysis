#ifndef SimClustering_HH
#define SimClustering_HH

#include <cassert>

#include "RcdUserRW.hh"
#include "SubAccessor.hh"
#include "SubInserter.hh"


class SimClustering : public RcdUserRW {

public:
  class Pack {
  public:
    
    Pack() {
      for(unsigned i(0);i<3;i++) {
	for(unsigned j(0);j<9;j++) {
	  _n[i][j]=0;
	}
      }
    }
    
    ~Pack() {}
    unsigned _n[3][9];
  };


  SimClustering() {
  }

  virtual ~SimClustering() {
    std::ofstream fout("SimClustering.txt");
    for(unsigned i(0);i<30;i++) {
      for(unsigned j(0);j<9;j++) {
	fout << " " << _nnArray[i][j];
      }
      fout << std::endl;
    }

    {std::ofstream fout("SimClustering2.txt");
    for(unsigned i(0);i<_vPack.size();i++) {
      for(unsigned j(0);j<3;j++) {
	for(unsigned k(0);k<9;k++) {
	  fout << " " << _vPack[i]._n[j][k];
	}
      }
      fout << std::endl;
    }}
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "SimClustering::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    if(r.recordType()==RcdHeader::runStart) {
      for(unsigned i(0);i<30;i++) {
	for(unsigned j(0);j<9;j++) {
	  _nnArray[i][j]=0;
	}
      }
    }

    if(r.recordType()==RcdHeader::runEnd) {
    }

    if(r.recordType()==RcdHeader::bunchTrain) {
      SubAccessor accessor(r);
     
      Pack pack;

      std::vector<const SimLayerHits*>
	v(accessor.access<SimLayerHits>());
      assert(v.size()==30);
      
      for(unsigned i(0);i<v.size();i++) {
	if(doPrint(r.recordType(),1)) v[i]->print(std::cout) << std::endl;
	assert(v[i]->layer()==i);

	const SimPixelHit *sph(v[i]->hits());
	for(unsigned j(0);j<v[i]->numberOfHits();j++) {
	  unsigned nn(0);
	  for(unsigned k(0);k<v[i]->numberOfHits();k++) {
	    if(k!=j) {
	      if(sph[k].i()==sph[j].i()+1 ||
		 sph[k].i()==sph[j].i()   ||
		 sph[k].i()==sph[j].i()-1) {
		if(sph[k].j()==sph[j].j()+1 ||
		   sph[k].j()==sph[j].j()   ||
		   sph[k].j()==sph[j].j()-1) {
		  nn++;
		}
	      }
	    }
	  }
	  assert(nn<9);
	  if(         i<10) pack._n[0][nn]++;
	  if(i>=10 && i<20) pack._n[1][nn]++;
	  if(i>=20        ) pack._n[2][nn]++;
	  _nnArray[i][nn]++;
	}
      }
      
      _vPack.push_back(pack);

      SubInserter inserter(r);

      /*
      for(unsigned i(0);i<30;i++) {
	SimLayerHits *slh(inserter.insert<SimLayerHits>(true));
	slh->layer(i);
	for(unsigned j(0);j<vLayer[i].size();j++) {
	  int ii((int)(vLayer[i][j]->xAverage()/0.050));
	  if(vLayer[i][j]->xAverage()<0.0) ii--;
	  int jj((int)(vLayer[i][j]->yAverage()/0.050));
	  if(vLayer[i][j]->yAverage()<0.0) jj--;
	  
	  if(ii>=-65536 && ii<65536 &&
	     jj>=-65536 && jj<65536) {
	    SimPixelHit sph(ii,jj);
	    slh->addHit(sph);
	  }
	}
	inserter.extend(slh->numberOfHits()*sizeof(SimPixelHit));
      }
      */
    }
    
    return true;
  }


private:
  unsigned _nnArray[30][9];
  std::vector<Pack> _vPack;
};

#endif
