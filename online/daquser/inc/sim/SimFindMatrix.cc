#include <cassert>
#include <iomanip>

#include "TMath.h"

#include "SimFindMatrix.hh"

namespace TBTrack {

  SimFindMatrix::SimFindMatrix() {
  }
  
  SimFindMatrix::~SimFindMatrix() {
  }
  
  bool SimFindMatrix::findMatrix(const std::vector<TVectorD> &v, double cutProb, bool findMean) {
    //TMatrixDSym TBTrackAligner::findError(const std::vector<TVectorD> &v, double cutProb, bool mean, TVectorD &m) {

    // Check inputs
    if(cutProb<0.0 || cutProb>=1.0) return false;
    if(v.size()<=2) return false;

    const int n(v[0].GetNrows());
    if(n<1) return false;
    if(cutProb>0.0 && n>6) return false;

    for(int i(1);i<v.size();i++) {
      if(v[i].GetNrows()!=n) return false;
    }
    
    // All OK
    bool print(false);
    if(print) std::cout << "findMatrix() "
			<< "Size of TVectorD = " << n << std::endl;

    double cFactor(1.0);
    cFactor=meanSquareRatio(n,cutProb);

  //double correctionFactor(1.024); // 0.01
  //double correctionFactor(1.155); // 0.1
  //double correctionFactor(1.0/0.8929); // cutProb=0.1, 3x3 matrix
  //double correctionFactor(1.0/0.9112); // cutProb=0.1, 4x4 matrix
  //double correctionFactor(1.289); // 0.2

  unsigned nNew(0),nOld(0);

  TVectorD m(n),o(n);
  //TVectorD o(n);
  TMatrixDSym e(n),w(n);

  for(unsigned l(0);l<100;l++) {
    if(print) std::cout << "findError() "
			<< "Starting iteration = " << l << std::endl;
    
    nNew=0;
    m.Zero();
    e.Zero();
    
    for(unsigned i(0);i<v.size();i++) {
      double p(1.0);
      if(l>0) p=TMath::Prob((v[i]-o)*(w*(v[i]-o)),n);
      if(p>=cutProb) {      // remove outliers
	nNew++;

	// Calculating mean and variance
	m+=v[i];
	for(unsigned j(0);j<n;j++) {
	  for(unsigned k(0);k<n;k++) {
	    e(j,k)+=v[i](j)*v[i](k);
	  }
	}
      }
    }
    
    if(print) 
      std::cout << "findError() "
		<< "Iteration = " << l << " used " << nNew
		<< " values" << std::endl;
    
    if(nNew<2) return false;
    
    // Normalise means and variance matrix
    m*=(1.0/nNew);
    for(unsigned j(0);j<n;j++) {
      for(unsigned k(0);k<n;k++) {
	e(j,k)-=nNew*m(j)*m(k);
      }
    }
    // scale the error matrix...
    e*=(cFactor/(nNew-1));
    
    // make sure it's invertable, then invert
    w=e;
    for(unsigned j(0);j<n;j++) {
      if(w(j,j)<=0.0) {
	for(unsigned k(0);k<n;k++) {
	  w(j,k)=0.0;
	  w(k,j)=0.0;
	}
	w(j,j)=1.0;
      }
    }
    w.Invert();
    
    if(print) {
      std::cout << "findError() "
		<< "Iteration = " << l << std::endl
		<< " Means, probability of being zero = " 
		<< TMath::Prob(m*(w*m)*nNew,n) << std::endl;
      for(unsigned j(0);j<n;j++) {
	std::cout << std::setw(13) << m(j);
      }

      std::cout << std::endl << " Error matrix" << std::endl;
      for(unsigned j(0);j<n;j++) {
	for(unsigned k(0);k<n;k++) {
	  std::cout << std::setw(13) << e(j,k);
	}
	std::cout << std::endl;
      }
      
      std::cout << std::endl << " 1d errors" << std::endl;
      for(unsigned j(0);j<n;j++) {
	std::cout << std::setw(13) << sqrt(e(j,j));
      }
      
      std::cout << std::endl << " Correlation matrix" << std::endl;
      for(unsigned j(0);j<n;j++) {
	for(unsigned k(0);k<n;k++) {
	  std::cout << std::setw(13) << e(j,k)/sqrt(e(j,j)*e(k,k));
	}
	std::cout << std::endl;
      }
    }

    //if(nNew==nOld) return e; // no more outliers to remove
    if(nNew==nOld) return true; // no more outliers to remove
    
    if(findMean) o=m;
    else o.Zero();
    nOld=nNew;
  }
  
  return false;
  }

  const TVectorD& SimFindMatrix::mean() const {
    return _mean;
  }

  const TMatrixDSym& SimFindMatrix::matrix() const {
    return _matrix;
  }

  const std::vector<double>& SimFindMatrix::probabilities() const {
    return _vProbability;
  }
  
  double SimFindMatrix::gaussianMomentIntegral(unsigned n) {
    if(n==0) return sqrt(0.5*acos(-1.0));
    if(n==1) return 1.0;
    return (n-1.0)*gaussianMomentIntegral(n-2);
  }

  double SimFindMatrix::gaussianMomentIntegral(unsigned n, double a) {
    if(a<=0.0) return 0.0;

    if(n==0) return sqrt(0.5*acos(-1.0))*erf(a/sqrt(2.0));
    if(n==1) return (1.0-exp(-0.5*a*a));
    return (n-1.0)*gaussianMomentIntegral(n-2,a)-pow(a,n-1)*exp(-0.5*a*a);
  }

  double SimFindMatrix::normalisationRatio(unsigned d, double sigmaCut) {
    if(d==0) return 0.0;
    if(sigmaCut<=0.0) return 0.0;

    return gaussianMomentIntegral(d-1,sigmaCut)/gaussianMomentIntegral(d-1);
  }
  
  double SimFindMatrix::meanSquareRatio(unsigned d, double sigmaCut) {
    if(d==0) return 0.0;
    if(sigmaCut<=0.0) return 0.0;

    return gaussianMomentIntegral(d+1,sigmaCut)/(d*gaussianMomentIntegral(d-1,sigmaCut));
  }

  std::ostream& SimFindMatrix::print(std::ostream &o, const std::string &s) const {
    o << s << "SimFindMatrix::print()" << std::endl;
    /*
      for(unsigned xy(0);xy<2;xy++) {
      if(xy==0) o << " X fit: ";
      else      o << " Y fit: ";
      _fitter[xy][0].printInitialisation(o);
      }
  */
    return o;
  }

}
