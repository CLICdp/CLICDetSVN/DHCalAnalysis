#include <cassert>
#include <iomanip>

#include "TMath.h"

#include "FindMatrix.hh"

namespace TBTrack {

  FindMatrix::FindMatrix() {
  }
  
  FindMatrix::~FindMatrix() {
  }
  
  bool FindMatrix::findMatrix(const std::vector<TVectorD> &v, double sigmaCut, bool findMean) {

    // Check inputs
    if(sigmaCut<=0.0) return false;
    if(v.size()<2) return false;

    const int n(v[0].GetNrows());
    if(n<1) return false;

    for(unsigned i(1);i<v.size();i++) {
      if(v[i].GetNrows()!=n) return false;
    }
    
    // All OK
    const bool print(true);
    if(print) std::cout << "findMatrix() "
			<< "Size of TVectorD = " << n << std::endl;

    const double nFactor(normalisationRatio(n,sigmaCut));
    const double probabilityCut(1.0-nFactor);
    if(print) std::cout << "findMatrix() "
			<< "Normalisation factor = " << nFactor
			<< ", probability cut = " << probabilityCut << std::endl;

    const double cFactor(1.0/meanSquareRatio(n,sigmaCut));
    if(print) std::cout << "findMatrix() "
			<< "Mean square factor = " << cFactor << std::endl;

    unsigned nNew(0),nOld(0);
    
    bool converged(false);
    TVectorD m(n),o(n);
    TMatrixDSym e(n),w(n);
    _vProbability.resize(v.size());

    std::vector<bool> vUsed(v.size());
    for(unsigned i(0);i<v.size();i++) vUsed[i]=false;

    for(unsigned l(0);l<100 && !converged;l++) {
      if(print) std::cout << "findMatrix() "
			  << "Starting iteration = " << l << std::endl;
      
      nNew=0;
      converged=true;
      m.Zero();
      e.Zero();
    
      for(unsigned i(0);i<v.size();i++) {
	double p(1.0);
	if(l>0) p=TMath::Prob((v[i]-o)*(w*(v[i]-o)),n);
	_vProbability[i]=p;

	// Remove outliers
	if(p>=probabilityCut) {
	  nNew++;

	  if(!vUsed[i]) converged=false;
	  vUsed[i]=true;

	  // Calculating mean and variance
	  m+=v[i];
	  for(int j(0);j<n;j++) {
	    for(int k(0);k<n;k++) {
	      e(j,k)+=v[i](j)*v[i](k);
	    }
	  }
	} else {
	  if(vUsed[i]) converged=false;
	  vUsed[i]=false;
	}
      }
    
      if(print) 
	std::cout << "findMatrix() "
		  << "Iteration = " << l << " used " << nNew
		  << " values" << std::endl;
    
      if(nNew<2) return false;
    
      // Normalise means and variance matrix
      m*=(1.0/nNew);
      for(int j(0);j<n;j++) {
	for(int k(0);k<n;k++) {
	  e(j,k)-=nNew*m(j)*m(k);
	}
      }

      // Scale the error matrix to correct for probability cut
      if(l==0) e*=(1.0/(nNew-1.0));
      else e*=(cFactor/(nNew-1.0));
    
      // Make sure it's invertable, then invert
      w=e;
      for(int j(0);j<n;j++) {
	if(w(j,j)<=0.0) {
	  for(int k(0);k<n;k++) {
	    w(j,k)=0.0;
	    w(k,j)=0.0;
	  }
	  w(j,j)=1.0;
	}
      }
      w.Invert();
    
      if(print) {
	std::cout << "findMatrix() "
		  << "Iteration = " << l << std::endl
		  << " Means, probability of being zero = " 
		  << TMath::Prob(m*(w*m)*nNew,n) << std::endl;
	for(int j(0);j<n;j++) {
	  std::cout << std::setw(13) << m(j);
	}
	std::cout << std::endl;

	std::cout << " Error matrix" << std::endl;
	for(int j(0);j<n;j++) {
	  for(int k(0);k<n;k++) {
	    std::cout << std::setw(13) << e(j,k);
	  }
	  std::cout << std::endl;
	}
      
	std::cout << " Diagonal errors" << std::endl;
	for(int j(0);j<n;j++) {
	  std::cout << std::setw(13) << sqrt(e(j,j));
	}
	std::cout << std::endl;
      
	std::cout << " Correlation matrix" << std::endl;
	for(int j(0);j<n;j++) {
	  for(int k(0);k<n;k++) {
	    std::cout << std::setw(13) << e(j,k)/sqrt(e(j,j)*e(k,k));
	  }
	  std::cout << std::endl;
	}
	std::cout << std::endl;
      }

      // No more outliers to remove
      //if(nNew==nOld) return e;
      //if(nNew==nOld) {
      //converged=true;

      //} else {
      if(!converged) {
	if(findMean) o=m;
	else o.Zero();
	nOld=nNew;
      }
    }

    if(!converged) return false;

    // Fill storage
    _number=nNew;
    _mean.ResizeTo(m);
    _mean=m;
    _matrix.ResizeTo(e);
    _matrix=e;

    return true;
  }

  unsigned FindMatrix::number() const {
    return _number;
  }

  const TVectorD& FindMatrix::mean() const {
    return _mean;
  }

  const TMatrixDSym& FindMatrix::matrix() const {
    return _matrix;
  }

  const std::vector<double>& FindMatrix::probabilities() const {
    return _vProbability;
  }
  
  double FindMatrix::gaussianMomentIntegral(unsigned n) {
    if(n==0) return sqrt(0.5*acos(-1.0));
    if(n==1) return 1.0;
    return (n-1.0)*gaussianMomentIntegral(n-2);
  }

  double FindMatrix::gaussianMomentIntegral(unsigned n, double a) {
    if(a<=0.0) return 0.0;

    if(n==0) return sqrt(0.5*acos(-1.0))*erf(a/sqrt(2.0));
    if(n==1) return (1.0-exp(-0.5*a*a));
    return (n-1.0)*gaussianMomentIntegral(n-2,a)-pow(a,n-1)*exp(-0.5*a*a);
  }

  double FindMatrix::normalisationRatio(unsigned d, double sigmaCut) {
    if(d==0) return 0.0;
    if(sigmaCut<=0.0) return 0.0;

    return gaussianMomentIntegral(d-1,sigmaCut)/gaussianMomentIntegral(d-1);
  }
  
  double FindMatrix::meanSquareRatio(unsigned d, double sigmaCut) {
    if(d==0) return 0.0;
    if(sigmaCut<=0.0) return 0.0;

    return gaussianMomentIntegral(d+1,sigmaCut)/(d*gaussianMomentIntegral(d-1,sigmaCut));
  }

  std::ostream& FindMatrix::print(std::ostream &o, const std::string &s) const {
    o << s << "FindMatrix::print()" << std::endl;
    /*
      for(unsigned xy(0);xy<2;xy++) {
      if(xy==0) o << " X fit: ";
      else      o << " Y fit: ";
      _fitter[xy][0].printInitialisation(o);
      }
    */
    return o;
  }

}
