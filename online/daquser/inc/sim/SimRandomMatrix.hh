#ifndef SimRandomMatrix_HH
#define SimRandomMatrix_HH

#include "TRandom.h"
#include "TMatrixD"
#include "TVectorD"


class SimRandomMatrix {
  
public:
  SimRandomMatrix(int seed=12345) : _random(seed) {

  }

  ~SimRandomMatrix() {
  }

  void parameters(const TVectorD &m, const TMatrixSymD &e) {
    assert(m.getNRows()==e.getNRows());
    _number=m.getNRows();
    _mean=m;
    _error=e;
  }

  TVectorD random() {
    TVectorD r(_number);
    for(unsigned i(0);i<_number;i++) {
      r(i)=_random.gaus(_mean(i),sqrt(_error(i,i)));
    }
    return r;
  }
  
private:
  TRandom _random;
  
  unsigned _number;
  TVectorD _mean;
  TMatrixSymD _error;
};

#endif
