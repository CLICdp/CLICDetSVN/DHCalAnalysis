#ifndef SimAnalysis_HH
#define SimAnalysis_HH

#include <cassert>

#include "RcdMultiUserRO.hh"

#include "SimAnalysisPrimaries.hh"
#include "SimAnalysisLayerTracks.hh"
#include "SimAnalysisHits.hh"


class SimAnalysis : public RcdMultiUserRO {

public:

  // Set default bits to enable required histograms
  enum {
    defaultSimAnalysisBits=0x3
  };


  SimAnalysis(const UtlPack bits=defaultSimAnalysisBits) {

    _ignorRunType=true;

    // Add modules
    //if(bits.bit( 0)) addUser(*(new SimAnalysisDigitisation()));
    //else std::cout << "SimAnalysisDigitisation disabled" << std::endl;

    if(bits.bit( 0)) addUser(*(new SimAnalysisPrimaries()));
    else std::cout << "SimAnalysisHits disabled" << std::endl;

    if(bits.bit( 1)) addUser(*(new SimAnalysisLayerTracks()));
    else std::cout << "SimAnalysisHits disabled" << std::endl;

    if(bits.bit( 2)) addUser(*(new SimAnalysisHits()));
    else std::cout << "SimAnalysisHits disabled" << std::endl;
  }

  virtual ~SimAnalysis() {
    deleteAll();
  }

  virtual bool record(const RcdRecord &r) {

    // Possibly set print level
    if(!_ignorRunType) {
      if(r.recordType()==RcdHeader::runStart) {
	SubAccessor accessor(r);
	std::vector<const IlcRunStart*>
	  v(accessor.extract<IlcRunStart>());
        if(v.size()==1) printLevel(v[0]->runType().printLevel());
      }
    }

    return RcdMultiUserRO::record(r);
  }

  void ignorRunType(bool b) {
    _ignorRunType=b;
  }

private:
  bool _ignorRunType;
};

#endif
