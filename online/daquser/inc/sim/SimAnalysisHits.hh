#ifndef SimAnalysisHits_HH
#define SimAnalysisHits_HH

#include <sstream>
#include <cassert>

#include "RcdUserRO.hh"
#include "SubAccessor.hh"

#include "SimAnalysisBase.hh"


class SimAnalysisHits : public SimAnalysisBase {

public:
  SimAnalysisHits() : SimAnalysisBase("SimAnalysisHits") {
  }

  virtual ~SimAnalysisHits() {
    endRoot();
  }

  virtual bool simAnalysisValidRun(IlcRunType rt) const {
    //return rt.majorType()==IlcRunType::sim;
    return true;
  }

  bool runStart(const RcdRecord &r) {
    _hErg=new TH1F((_runLabel+"Erg").c_str(),
		   (_runTitle+", Weighted hit estimator").c_str(),
		   100,0.0,500.0*_runStart.runType().version());

    _hErg2=new TH1F((_runLabel+"Erg2").c_str(),
		   (_runTitle+", Weighted track estimator").c_str(),
		    100,0.0,500.0*_runStart.runType().version());

    _hHvsT=new TH2F((_runLabel+"HvsT").c_str(),
		   (_runTitle+", Number of pixel hits vs tracks").c_str(),
		    100,0.0,50.0*_runStart.runType().version(),
		    100,0.0,50.0*_runStart.runType().version());

    for(unsigned i(0);i<30;i++) {
      std::ostringstream sLabel;
      sLabel << _runLabel << "Layer" << std::setw(2) << std::setfill('0')
	     << i << "Nhits";
      std::ostringstream sTitle;
      sTitle << _runTitle << ", Layer" << std::setw(2) << i
	     << ", Number of pixel hits";

      _hNHits[i]=new TH1F(sLabel.str().c_str(),sTitle.str().c_str(),
			  100,0.0,100.0);
    }

    for(unsigned i(0);i<30;i++) {
      std::ostringstream sLabel;
      sLabel << _runLabel << "Layer" << std::setw(2) << std::setfill('0')
	     << i << "XYhits";
      std::ostringstream sTitle;
      sTitle << _runTitle << ", Layer" << std::setw(2) << i
	     << ", X,Y of pixel hits";

      _hXYHits[i]=new TH2F(sLabel.str().c_str(),sTitle.str().c_str(),
			    40,-200.0,200.0,40,-200.0,200.0);
    }

    return true;
  }

  bool runEnd(const RcdRecord &r) {
    return true;
  }

  bool configurationStart(const RcdRecord &r) {
    return true;
  }
  
  bool bunchTrain(const RcdRecord &r) {
    SubAccessor accessor(r);
    
    std::vector<const SimParticle*>
      u(accessor.access<SimParticle>());
    
    const SimParticle *pPrm(0);
    for(unsigned i(0);i<u.size();i++) {
      if(doPrint(r.recordType(),2)) u[i]->print(std::cout) << std::endl;

      if(u[i]->primary()) {
	assert(pPrm==0);
	pPrm=u[i];
      }
    }
    assert(pPrm!=0);
    bool nearOrigin(fabs(pPrm->x())<0.05 && fabs(pPrm->y())<0.05);
    
    std::vector<const SimLayerTrack*>
      v(accessor.access<SimLayerTrack>());
    
    double erg2(0.0);
    
    unsigned layerTracks[30];
    for(unsigned i(0);i<30;i++) layerTracks[i]=0;
    
    for(unsigned i(0);i<v.size();i++) {
      assert(v[i]->layer()<30);
      
      layerTracks[v[i]->layer()]++;

      if(v[i]->layer()<20) erg2+=1.0;
      else                 erg2+=2.0;
    }
    
    _hErg2->Fill(erg2);

    std::vector<const SimLayerTrack*> vTrack[30];
    
    for(unsigned i(0);i<v.size();i++) {
      assert(v[i]->layer()<30);
      vTrack[v[i]->layer()].push_back(v[i]);
    }

    if(nearOrigin) {
      std::cout << _bntTitle << " is near origin" << std::endl;

      for(unsigned i(0);i<30;i++) {
	std::ostringstream sLayer;
	sLayer << i;
	
	unsigned nPixels(100);
	TH2F *h(new TH2F((_bntLabel+"HitsLayer"+sLayer.str()).c_str(),
			 (_bntTitle+" Layer "+sLayer.str()+" Hits").c_str(),
			 2*nPixels,-0.05*nPixels,0.05*nPixels,
			 2*nPixels,-0.05*nPixels,0.05*nPixels));

	//std::cout << "Layer " << i << std::endl;

	for(unsigned j(0);j<vTrack[i].size();j++) {
	  if(doPrint(r.recordType(),2)) vTrack[i][j]->print(std::cout) << std::endl;

	  if(vTrack[i][j]->type()==SimDetectorBoxId::ecalSiEpitaxial &&
	     vTrack[i][j]->totalEnergy()>0.0) {
	    
	    h->Fill(vTrack[i][j]->xAverage(),
		    vTrack[i][j]->yAverage(),
		    vTrack[i][j]->totalEnergy());
	  }
	}
      }
    }

    std::vector<const SimLayerHits*>
      w(accessor.access<SimLayerHits>());
    //assert(w.size()==30);

    bool layer[30];
    for(unsigned i(0);i<30;i++) layer[i]=false;

    double erg(0.0);
    for(unsigned i(0);i<w.size();i++) {
      if(doPrint(r.recordType(),1)) w[i]->print(std::cout) << std::endl;

      assert(w[i]->layer()<30);
      assert(w[i]->layer()==i);
      assert(!layer[w[i]->layer()]);

      layer[w[i]->layer()]=true;
      _hNHits[w[i]->layer()]->Fill(w[i]->numberOfHits());
      _hHvsT->Fill(layerTracks[i],w[i]->numberOfHits());

      const SimPixelHit *sph(w[i]->hits());
      for(unsigned j(0);j<w[i]->numberOfHits();j++) {
	_hXYHits[w[i]->layer()]->Fill(sph[j].i(),sph[j].j());
      }

      if(i<20) erg+=w[i]->numberOfHits();
      else     erg+=2.0*w[i]->numberOfHits();
    }

    _hErg->Fill(erg);

    return true;
  }


private:
  TH1F *_hErg;
  TH1F *_hErg2;
  TH2F *_hHvsT;
  TH1F *_hNHits[30];
  TH2F *_hXYHits[30];
};

#endif
