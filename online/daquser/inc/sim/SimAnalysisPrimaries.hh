#ifndef SimAnalysisPrimaries_HH
#define SimAnalysisPrimaries_HH

#include <sstream>
#include <cassert>

#include "RcdUserRO.hh"
#include "SubAccessor.hh"

#include "SimAnalysisBase.hh"


class SimAnalysisPrimaries : public SimAnalysisBase {

public:
  SimAnalysisPrimaries() : SimAnalysisBase("SimAnalysisPrimaries") {
  }

  virtual ~SimAnalysisPrimaries() {
    endRoot();
  }

  virtual bool simAnalysisValidRun(IlcRunType rt) const {
    //return rt.majorType()==IlcRunType::sim;
    return true;
  }

  bool runStart(const RcdRecord &r) {
    return true;
  }

  bool runEnd(const RcdRecord &r) {
    return true;
  }

  bool configurationStart(const RcdRecord &r) {
    SubAccessor accessor(r);

    /*
    std::vector<const SimPrimaryData*>
      v(accessor.access<SimPrimaryData>());

    for(unsigned i(0);i<v.size();i++) {
      if(doPrint(r.recordType(),1)) v[i]->print(std::cout) << std::endl;
    }

    assert(v.size()==1);
    _simPrimaryData=*(v[0]);
    */
    assert(_vSimPrimaryData.size()==1);
    _simPrimaryData=_vSimPrimaryData[0];

    _hBPrimary=new TH1F("BPrimary",
		    (_runTitle+";Boundary of primaries").c_str(),
		    41,-20.0,21.0);
		    //SimParticle::endOfEnum,0.0,SimParticle::endOfEnum);
    
    _hIPrimary=new TH1F("IPrimary",
		    (_runTitle+";Particle id of primaries (GeV)").c_str(),
		    5000,-2500,2500);

    _hEPrimary=new TH1F("EPrimary",
		    (_runTitle+";Kinetic energy of primaries (GeV)").c_str(),
		    120,0.0,0.0012*_simPrimaryData.energy());
    _hRPrimary=new TH1F("RPrimary",
		    (_runTitle+";Kinetic energy ratio of primaries (GeV)").c_str(),
		    100,0.8,1.2);
    
    _hXPrimary=new TH1F("XPrimary",
		    (_runTitle+";X position of primaries (mm)").c_str(),
		    100,-20.0,20.0);
    _hYPrimary=new TH1F("YPrimary",
		    (_runTitle+";Y position of primaries (mm)").c_str(),
		    100,-20.0,20.0);
    _hZPrimary=new TH1F("ZPrimary",
		    (_runTitle+";Z position of primaries (mm)").c_str(),
		    100,-10.0,10.0);
    
    _hPXPrimary=new TH1F("PXPrimary",
		    (_runTitle+";X momentum of primaries (GeV/c)").c_str(),
		     100,-0.0002*_simPrimaryData.energy(),0.0002*_simPrimaryData.energy());
    _hPYPrimary=new TH1F("PYPrimary",
		    (_runTitle+";Y momentum of primaries (GeV/c)").c_str(),
		     100,-0.0002*_simPrimaryData.energy(),0.0002*_simPrimaryData.energy());
    _hPZPrimary=new TH1F("PZPrimary",
		    (_runTitle+";Z momentum of primaries (GeV/c)").c_str(),
		    120,0.0,0.0012*_simPrimaryData.energy());
    
    for(unsigned i(1);i<SimDetectorBoxId::endOfDetectorEnum;i++) {
      for(unsigned j(0);j<2;j++) {
	std::string name(SimDetectorBoxId::detectorName((SimDetectorBoxId::Detector)i));
	std::string dir("Inward");
	if(j==1) dir="Outward";

	_hI[i][j]=new TH1F((std::string("I")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";Particle Id of particles (GeV)").c_str(),
			   5000,-2500,2500);
	
	_hE[i][j]=new TH1F((std::string("E")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";Kinetic energy of particles (GeV)").c_str(),
			   120,0.0,0.0012*_simPrimaryData.energy());
	
	_hR[i][j]=new TH1F((std::string("R")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";Kinetic energy ratio of particles (GeV)").c_str(),
			   100,0.8,1.2);
	
	_hX[i][j]=new TH1F((std::string("X")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";X position of particles (mm)").c_str(),
			   110,-1100.0,1100.0);
	_hY[i][j]=new TH1F((std::string("Y")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";Y position of particles (mm)").c_str(),
			   110,-1100.0,1100.0);
	_hZ[i][j]=new TH1F((std::string("Z")+name+dir).c_str(),
			   (_runTitle+", Boundary "+name+" "+dir+";Z position of particles (mm)").c_str(),
			   100,-10.0,190.0);
	
	_hPX[i][j]=new TH1F((std::string("PX")+name+dir).c_str(),
			    (_runTitle+", Boundary "+name+" "+dir+";X momentum of particles (GeV/c)").c_str(),
			    100,-0.0002*_simPrimaryData.energy(),0.0002*_simPrimaryData.energy());
	_hPY[i][j]=new TH1F((std::string("PY")+name+dir).c_str(),
			    (_runTitle+", Boundary "+name+" "+dir+";Y momentum of particles (GeV/c)").c_str(),
			    100,-0.0002*_simPrimaryData.energy(),0.0002*_simPrimaryData.energy());
	_hPZ[i][j]=new TH1F((std::string("PZ")+name+dir).c_str(),
			    (_runTitle+", Boundary "+name+" "+dir+";Z momentum of particles (GeV/c)").c_str(),
			    120,0.0,0.0012*_simPrimaryData.energy());
      }
    }

    return true;
  }
  
  bool bunchTrain(const RcdRecord &r) {
    SubAccessor accessor(r);

    std::vector<const SimParticle*>
      v(accessor.access<SimParticle>());
    assert(v.size()>0);

    for(unsigned i(0);i<v.size();i++) {
      if(doPrint(r.recordType(),1)) v[i]->print(std::cout) << std::endl;

      //_hBPrimary->Fill(j);

      if(v[i]->primary()) {
	_hIPrimary->Fill(v[i]->particleId());
	_hEPrimary->Fill(0.001*v[i]->kineticEnergy());
	_hRPrimary->Fill(v[i]->kineticEnergy()/_simPrimaryData.kineticEnergy());
	
	_hXPrimary->Fill(v[i]->x());
	_hYPrimary->Fill(v[i]->y());
	_hZPrimary->Fill(v[i]->z());

	_hPXPrimary->Fill(0.001*v[i]->px());
	_hPYPrimary->Fill(0.001*v[i]->py());
	_hPZPrimary->Fill(0.001*v[i]->pz());
	
      } else {
	unsigned j(v[i]->boundary());
	unsigned k(0);
	if(!v[i]->inward()) k=1;
	
	_hI[j][k]->Fill(v[i]->particleId());
	_hE[j][k]->Fill(0.001*v[i]->kineticEnergy());
	_hR[j][k]->Fill(v[i]->kineticEnergy()/_simPrimaryData.kineticEnergy());
	
	_hX[j][k]->Fill(v[i]->x());
	_hY[j][k]->Fill(v[i]->y());
	_hZ[j][k]->Fill(v[i]->z());
	
	_hPX[j][k]->Fill(0.001*v[i]->px());
	_hPY[j][k]->Fill(0.001*v[i]->py());
	_hPZ[j][k]->Fill(0.001*v[i]->pz());
      }
    }

    return true;
  }


private:
  SimPrimaryData _simPrimaryData;

  TH1F *_hBPrimary;
  TH1F *_hEPrimary,*_hE[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hIPrimary,*_hI[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hRPrimary,*_hR[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hXPrimary,*_hX[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hYPrimary,*_hY[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hZPrimary,*_hZ[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hPXPrimary,*_hPX[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hPYPrimary,*_hPY[SimDetectorBoxId::endOfDetectorEnum][2];
  TH1F *_hPZPrimary,*_hPZ[SimDetectorBoxId::endOfDetectorEnum][2];
};

#endif
