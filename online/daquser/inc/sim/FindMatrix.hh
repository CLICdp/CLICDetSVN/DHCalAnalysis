#ifndef TBTrack_FindMatrix_HH
#define TBTrack_FindMatrix_HH

#include <string>
#include <iostream>
#include <vector>

#include "TVectorD.h"
#include "TMatrixD.h"


namespace TBTrack {

  class FindMatrix {
    
  public:
    FindMatrix();
    ~FindMatrix();  

    // Does the matrix calculation    
    bool findMatrix(const std::vector<TVectorD> &v, double cutProb, bool findMean=true);
    
    // Access the results
    unsigned number() const;
    const TVectorD& mean() const;
    const TMatrixDSym& matrix() const;
    const std::vector<double>& probabilities() const;

    // Calculates \int_0^\infty x^n e^{-x^2/2} dx
    static double gaussianMomentIntegral(unsigned n);

    // Calculates \int_0^a x^n e^{-x^2/2} dx
    static double gaussianMomentIntegral(unsigned n, double a);
    
    static double normalisationRatio(unsigned d, double sigmaCut);
    static double meanSquareRatio(unsigned d, double sigmaCut);

    std::ostream& print(std::ostream &o=std::cout, const std::string &s="") const;
    
    
  private:
    unsigned _number;
    TVectorD _mean;
    TMatrixDSym _matrix;    
    std::vector<double> _vProbability;
  };

}

#endif
