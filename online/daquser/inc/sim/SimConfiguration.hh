#ifndef SimConfiguration_HH
#define SimConfiguration_HH

#include <vector>
#include <fstream>
#include <iostream>

#include "RcdUserRW.hh"

#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "IlcRunStart.hh"
#include "IlcRunEnd.hh"
#include "IlcBunchTrain.hh"


class SimConfiguration : public RcdUserRW {

public:
  enum Counter {
    cfgInRun,
    slwInRun,slwInCfg,
    bntInRun,bntInCfg,
    endOfCounterEnum
  };

  SimConfiguration() {
  }

  virtual ~SimConfiguration() {
  }

#ifndef DAQ_ILC_TIMING
  DaqRunStart runStart() const {
    return _runStart;
  }

  void runStart(DaqRunStart r) {
    _runStart=r;
    assert(_runStart.runType().knownType());
  }
#else
  IlcRunStart runStart() const {
    return _runStart;
  }

  void runStart(IlcRunStart r) {
    _runStart=r;
    assert(_runStart.runType().knownType());
  }
#endif

  bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "SimConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
 
    SubInserter inserter(r);

    // Check record type
    switch (r.recordType()) {
      
    case RcdHeader::runStart: {
      _count[cfgInRun]=0; //c
      _count[slwInRun]=0; //sr
      _count[bntInRun]=0; //b

      /*
      unsigned runNumber;
      if(_runStart.runType().writeRun()) {
	runNumber=daqReadRunNumber();
	daqWriteRunNumber(runNumber+1);
      } else {
	runNumber=r.recordTime().seconds();
      }
      _runStart.runNumber(runNumber);
      */

      if(_runStart.runNumber()==999999) {
	unsigned runNumber(daqReadRunNumber());
	if(_runStart.runType().writeRun()) daqWriteRunNumber(runNumber+1);
	_runStart.runNumber(runNumber);
      }

#ifndef DAQ_ILC_TIMING
      DaqRunStart temp(_runStart);
#else
      IlcRunStart temp(_runStart);
#endif
      setRun(temp);
      _runStart=temp;

      _runStartTime=r.recordTime();
      
      SubInserter inserter(r);
#ifndef DAQ_ILC_TIMING
      inserter.insert<DaqRunStart>(_runStart);
#else
      inserter.insert<IlcRunStart>(_runStart);
#endif
      if(doPrint(r.recordType(),1)) _runStart.print(std::cout," ") << std::endl;
      
      break;
    }
      
    case RcdHeader::runEnd: {

      SubInserter inserter(r);
#ifndef DAQ_ILC_TIMING
      DaqRunEnd *d(inserter.insert<DaqRunEnd>(true));
#else
      IlcRunEnd *d(inserter.insert<IlcRunEnd>(true));
      d->actualNumberOfBunchTrainsInRun(_count[bntInRun]);
#endif
      d->runNumber(_runStart.runNumber());
      d->runType(_runStart.runType());
      d->actualNumberOfConfigurationsInRun(_count[cfgInRun]);
      d->actualNumberOfSlowReadoutsInRun(_count[slwInRun]);
      d->actualTimeOfRun(r.recordTime()-_runStartTime);

      if(doPrint(r.recordType(),1)) d->print(std::cout," ");

      break;
    }
   
    case RcdHeader::configurationStart: {
      _count[slwInCfg]=0; //a
      _count[bntInCfg]=0; //b
          
      _configurationStart.reset();
      _configurationStart.configurationNumberInRun(_count[cfgInRun]);
      setConfiguration(_configurationStart);
     
      _configurationStartTime=r.recordTime();

      SubInserter inserter(r);
#ifndef DAQ_ILC_TIMING
      inserter.insert<DaqConfigurationStart>(_configurationStart);
#else
      inserter.insert<IlcConfigurationStart>(_configurationStart);
#endif
     
      if(doPrint(r.recordType(),1)) _configurationStart.print(std::cout," ") << std::endl;
      
      break;
    }

    case RcdHeader::configurationEnd: {

      SubInserter inserter(r);
#ifndef DAQ_ILC_TIMING
      IlcConfigurationEnd *d(inserter.insert<IlcConfigurationEnd>(true));
#else
      IlcConfigurationEnd *d(inserter.insert<IlcConfigurationEnd>(true));
      d->actualNumberOfBunchTrainsInConfiguration(_count[bntInCfg]);
#endif
      d->configurationNumberInRun(_count[cfgInRun]);
      d->actualNumberOfSlowReadoutsInConfiguration(_count[slwInCfg]);
      d->actualTimeOfConfiguration(r.recordTime()-_configurationStartTime);

      if(doPrint(r.recordType(),1)) d->print(std::cout," ");

      // Increment counts
      _count[cfgInRun]++;

      break;
    }

    case RcdHeader::slowReadout: {
      SubInserter inserter(r);
      IlcSlowReadout *d(inserter.insert<IlcSlowReadout>(true));

      d->slowReadoutNumberInRun(_count[slwInRun]);
      d->slowReadoutNumberInConfiguration(_count[slwInCfg]);
      
      if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

      // Increment counts
      _count[slwInRun]++;
      _count[slwInCfg]++;
 
      break;
    }

    case RcdHeader::event: {
      SubInserter inserter(r);
      DaqEvent *d(inserter.insert<DaqEvent>(true));
      
      d->eventNumberInRun(_count[bntInRun]);
      d->eventNumberInConfiguration(_count[bntInCfg]);
      
      if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

      // Increment counts
      _count[bntInRun]++;
      _count[bntInCfg]++;
      
      break;
    }

    case RcdHeader::bunchTrain: {
      SubInserter inserter(r);
      IlcBunchTrain *d(inserter.insert<IlcBunchTrain>(true));
      
      d->bunchTrainNumberInRun(_count[bntInRun]);
      d->bunchTrainNumberInConfiguration(_count[bntInCfg]);
      
      if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

      // Increment counts
      _count[bntInRun]++;
      _count[bntInCfg]++;
      
      break;
    }
      
    default: {
      break;
    }
    };

    return true;
  }


#ifndef DAQ_ILC_TIMING
  virtual void setRun(DaqRunStart &d) const {
#else
  virtual void setRun(IlcRunStart &d) const {
#endif
    //const unsigned char v(_runStart.runType().version());

    const UtlPack v(_runStart.runType().version());

    // Overall defaults
    d.maximumNumberOfConfigurationsInRun(1);
    //d.maximumNumberOfBunchTrainsInRun(10000);

    switch(_runStart.runType().type()) {
      
    case IlcRunType::mpsBeam: {
      break;
    }

    case IlcRunType::genTest: {
      break;
    }
    case IlcRunType::genPhoton: {
      break;
    }
    case IlcRunType::genPositron: {
      break;
    }
    case IlcRunType::genElectron: {
      break;
    }
    case IlcRunType::genMuPlus: {
      break;
    }
    case IlcRunType::genMuMinus: {
      break;
    }
    case IlcRunType::genPiPlus: {
      break;
    }
    case IlcRunType::genPiMinus: {
      break;
    }
    case IlcRunType::genPiZero: {
      break;
    }
    case IlcRunType::genProton: {
      break;
    }
    case IlcRunType::genAntiProton: {
      break;
    }
    case IlcRunType::genNeutron: {
      break;
    }
    case IlcRunType::genAntiNeutron: {
      break;
    }

    case IlcRunType::decTest: {
      break;
    }
    case IlcRunType::decPhoton: {
      break;
    }
    case IlcRunType::decPositron: {
      break;
    }
    case IlcRunType::decElectron: {
      break;
    }

    default: {
      // We missed a run type
      d.print(std::cerr);
      assert(false);
      break;
    }
    };

    // Reset limits if endless run from user
    /*
    if(_runStart.runType().endlessRun()) {
      d.maximumNumberOfConfigurationsInRun(0xffffffff);
      d.maximumNumberOfBunchTrainsInRun(0xffffffff);
      d.maximumTimeOfRun(UtlTimeDifference(0x7fffffff,999999));
    }
    */

    // Reset limits if smaller from user
    if(d.maximumNumberOfConfigurationsInRun()>
       _runStart.maximumNumberOfConfigurationsInRun())
      d.maximumNumberOfConfigurationsInRun(_runStart.maximumNumberOfConfigurationsInRun());

#ifndef DAQ_ILC_TIMING
    if(d.maximumNumberOfEventsInRun()>
       _runStart.maximumNumberOfEventsInRun())
      d.maximumNumberOfEventsInRun(_runStart.maximumNumberOfEventsInRun());
#else
    if(d.maximumNumberOfBunchTrainsInRun()>
       _runStart.maximumNumberOfBunchTrainsInRun())
      d.maximumNumberOfBunchTrainsInRun(_runStart.maximumNumberOfBunchTrainsInRun());
#endif
  }

#ifndef DAQ_ILC_TIMING
  virtual void setConfiguration(DaqConfigurationStart &d) const {
#else
  virtual void setConfiguration(IlcConfigurationStart &d) const {
#endif
    //const unsigned iCfg(d.configurationNumberInRun());

    const UtlPack v(_runStart.runType().version());

    // Overall defaults
    //d.maximumTimeOfConfiguration(UtlTimeDifference(60*60)); // 1hour
    
    // Select on run type
    switch(_runStart.runType().type()) {

    case IlcRunType::mpsBeam: {
      break;
    }

    case IlcRunType::genTest: {
      break;
    }
    case IlcRunType::genPhoton: {
      break;
    }
    case IlcRunType::genPositron: {
      break;
    }
    case IlcRunType::genElectron: {
      break;
    }
    case IlcRunType::genMuPlus: {
      break;
    }
    case IlcRunType::genMuMinus: {
      break;
    }
    case IlcRunType::genPiPlus: {
      break;
    }
    case IlcRunType::genPiMinus: {
      break;
    }
    case IlcRunType::genPiZero: {
      break;
    }
    case IlcRunType::genProton: {
      break;
    }
    case IlcRunType::genAntiProton: {
      break;
    }
    case IlcRunType::genNeutron: {
      break;
    }
    case IlcRunType::genAntiNeutron: {
      break;
    }

    case IlcRunType::decTest: {
      break;
    }
    case IlcRunType::decPhoton: {
      break;
    }
    case IlcRunType::decPositron: {
      break;
    }
    case IlcRunType::decElectron: {
      break;
    }

    default: {
      // We missed a run type
      assert(false);
      break;
    }
    };
  }

#ifndef DAQ_ILC_TIMING
  unsigned maximumNumberOfEventsRemainingInConfiguration() const {
    assert(_configurationStart.maximumNumberOfEventsInConfiguration()>=_count[bntInCfg]);
    return _configurationStart.maximumNumberOfEventsInConfiguration()- _count[bntInCfg];
  }

  unsigned maximumNumberOfEventsRemainingInRun() const {
    assert(_runStart.maximumNumberOfEventsInRun()>=_count[bntInRun]);
    return _runStart.maximumNumberOfEventsInRun()- _count[bntInRun];
  }
#else
  unsigned maximumNumberOfBunchTrainsRemainingInConfiguration() const {
    assert(_configurationStart.maximumNumberOfBunchTrainsInConfiguration()>=_count[bntInCfg]);
    return _configurationStart.maximumNumberOfBunchTrainsInConfiguration()- _count[bntInCfg];
  }

  unsigned maximumNumberOfBunchTrainsRemainingInRun() const {
    assert(_runStart.maximumNumberOfBunchTrainsInRun()>=_count[bntInRun]);
    return _runStart.maximumNumberOfBunchTrainsInRun()- _count[bntInRun];
  }
#endif

protected:

#ifndef DAQ_ILC_TIMING
  DaqRunStart _runStart;
  DaqConfigurationStart _configurationStart;
#else
  IlcRunStart _runStart;
  IlcConfigurationStart _configurationStart;
#endif

  UtlTime _runStartTime;
  UtlTime _configurationStartTime;

private:
  unsigned _count[endOfCounterEnum];
};

#endif
