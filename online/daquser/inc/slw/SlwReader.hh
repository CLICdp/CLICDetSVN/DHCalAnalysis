#ifndef SlwReader_HH
#define SlwReader_HH

#include <vector>

#include "RcdUserRW.hh"
#include "RcdReaderAsc.hh"
#include "RcdReaderBin.hh"


class SlwReader : public RcdUserRW {

public:
  SlwReader() : _reader(0), _vectorNumber(0) {
    _reader=new RcdReader;
  }

  virtual ~SlwReader() {
  }

  void slwNumber(unsigned n, bool b=true) {
    _slwNumber.push_back(n);
    _slwBin.push_back(b);
  }

  void slwNumber(unsigned nLo, unsigned nHi, bool b) {
    for(unsigned i(nLo);i<=nHi;i++) {
      _slwNumber.push_back(i);
      _slwBin.push_back(b);
    }
  }

  bool record(RcdRecord &r) {

    while(!_reader->read(r)) {
      _reader->close();

      // Done all the runs
      if(_vectorNumber>=_slwNumber.size()) return false;

      // Open output file
      std::ostringstream sout;
      sout << "data/slw/Slw" << _slwNumber[_vectorNumber];

      if(_slwBin[_vectorNumber]) _reader=new RcdReaderBin;
      else                       _reader=new RcdReaderAsc;

      _reader->open(sout.str());

      _vectorNumber++;
    }
    return true;
  }


private:
  RcdReader *_reader;

  unsigned _vectorNumber;
  std::vector<unsigned> _slwNumber;
  std::vector<bool> _slwBin;
};

#endif
