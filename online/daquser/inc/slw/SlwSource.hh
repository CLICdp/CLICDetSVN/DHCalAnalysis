#ifndef SlwSource_HH
#define SlwSource_HH

#include <vector>
#include <fstream>
#include <iostream>

// dual/inc/rcd
#include "RcdUserRW.hh"
#include "DaqStateData.hh"
#include "SubModifier.hh"
#include "SubInserter.hh"




class SlwSource : public RcdUserRW {

public:
  SlwSource() {
  }

  virtual ~SlwSource() {
  }

  bool record(RcdRecord &r) {
    SubInserter inserter(r);
    
    DaqStateData *dsd(inserter.insert<DaqStateData>(true));

    // Check record type
    switch (dsd->initialState()) {
      
    case 0: {
      r.initialise(RcdHeader::startUp);
      break;
    }

    case 1: {
      if(dsd->target()>1) {
	r.initialise(RcdHeader::slowStart);
      } else {
	r.initialise(RcdHeader::shutdown);
      }
      break;
    }

    case 2: {
      if(dsd->target()>1) {
	r.initialise(RcdHeader::slowControl);
      } else {
	r.initialise(RcdHeader::slowEnd);
      }
      break;
    }

    case 3: {
      if(dsd->target()>3) {
	r.initialise(RcdHeader::slowReadout);
      } else {
	r.initialise(RcdHeader::slowEnd);
      }
      break;
    }

    default: {
      assert(false);
      break;
    }
    };
    
    return true;
  }
  
private:
};

#endif
