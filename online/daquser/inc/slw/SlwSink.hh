#ifndef SlwSink_HH
#define SlwSink_HH

#include <vector>
#include <fstream>
#include <iostream>

// dual/inc/rcd
#include "RcdUserRW.hh"
#include "SlwWriter.hh"
#include "SubModifier.hh"
#include "DaqStateData.hh"


class SlwSink : public RcdUserRW {

public:
  SlwSink() {
  }

  virtual ~SlwSink() {
  }

  bool record(RcdRecord &r) {
    SubModifier accessor(r);

    std::vector<DaqStateData*> v(accessor.access<DaqStateData>());
    assert(v.size()==1);

    _writer.record(r);
    if(_writer.numberOfBytes()>10000000) v[0]->target(2,1);

    // Check record type
    switch (r.recordType()) {

    case RcdHeader::startUp: {
      v[0]->finalState(1);
      break;
    }

    case RcdHeader::slowStart: {
      v[0]->finalState(2);
      break;
    }
      
    case RcdHeader::slowControl: {
      v[0]->finalState(3);
      break;
    }
      
    case RcdHeader::slowReadout: {
      v[0]->finalState(3);
      break;
    }
/*      
    case RcdHeader::slowUncontrol: { 
      break;
    }
*/
    case RcdHeader::slowEnd: {
      v[0]->finalState(1);
      break;
    }

    case RcdHeader::shutdown: {
      v[0]->finalState(0);
      break;
    }

    default: {
      assert(false);
      break;
    }
    };

    v[0]->print(std::cout);
    return true;
  }
  
private:
  SlwWriter _writer;
};

#endif
