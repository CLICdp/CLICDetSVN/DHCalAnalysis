#ifndef EventRecord_HH
#define EventRecord_HH

#include "Event.hh"
#include "Record.hh"

class EventRecord : public Record {

 public:
  EventRecord() {}

  unsigned eventNumber() const {
    return _runNumber;
  }

  void eventNumber(unsigned n) {
    _runNumber=n;
  }

  unsigned configurationEventNumber() const {
    return _recordNumber;
  }

  void configurationEventNumber(unsigned n) {
    _recordNumber=n;
  }

  const Event* event() const {
    return &_event;
  }

  bool event(const Event *e) {
    if(e==0) return false;
    _event = (*e);
    return true;
  }


private:
  Event _event;

};

#endif
