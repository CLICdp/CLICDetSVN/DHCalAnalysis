#ifndef Configuration_HH
#define Configuration_HH

#include <iostream>

#include "RobConfiguration.hh"
#include "TrgConfiguration.hh"

class Configuration {

 public:
  Configuration() {
    reset();
  }

  void reset() {
    _trgConfiguration.reset();
    for(unsigned i(0);i<15;i++) _robConfiguration[i].reset(i);
  }

  const RobConfiguration* robConfiguration(unsigned rob) const {
    if(rob>=15) return 0;
    return _robConfiguration+rob;
  }

  bool robConfiguration(unsigned rob, const RobConfiguration* p) {
    if(rob>=15 || p==0) return false;
    _robConfiguration[rob]=(*p);
    return true;
  }

  const TrgConfiguration* trgConfiguration() const {
    return &_trgConfiguration;
  }

  bool trgConfiguration(const TrgConfiguration* p) {
    if(p==0) return false;
    _trgConfiguration=(*p);
    return true;
  }

  void print() const {
    std::cout << "Configuration::print" << endl;
    _trgConfiguration.print();
    for(unsigned i(0);i<15;i++) {
      std::cout << "  ROB " << i << std::endl;
      _robConfiguration[i].print();
    }
  }

 private:
  TrgConfiguration _trgConfiguration;
  RobConfiguration _robConfiguration[15];
};

#endif
