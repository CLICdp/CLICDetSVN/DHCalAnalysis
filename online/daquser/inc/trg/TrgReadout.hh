#ifndef TrgReadout_HH
#define TrgReadout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "DaqRunStart.hh"
#include "CrcBeTrgEventData.hh"
#include "SubAccessor.hh"
#include "SubInserter.hh"


class TrgReadout : public RcdUserRW {

public:
  TrgReadout(unsigned, unsigned char, unsigned char) {
  }

  virtual ~TrgReadout() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << std::endl << "TrgReadout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    if(r.recordType()==RcdHeader::runStart) {

      // Access the DaqRunStart to get print level
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;

      _printLevel=v[0]->runType().printLevel();

      _nCount[0]=0;
      _nCount[1]=0;
      _nCount[2]=0;

      _spillInvert=false;
    }

    if(r.recordType()==RcdHeader::configurationStart) {
      SubAccessor accessor(r);
      std::vector<const TrgReadoutConfigurationData*>
	v(accessor.extract<TrgReadoutConfigurationData>());

      if(doPrint(r.recordType(),1)) {
	std::cout << " Number of TrgReadoutConfigurationData subrecords accessed = "
		  << v.size() << std::endl;
	for(unsigned i(0);i<v.size();i++) {
	  v[i]->print(std::cout," ") << std::endl;
	}
      }

      if(v.size()>0) _spillInvert=v[0]->spillInvert();

      _nCount[1]=0;
      _nCount[2]=0;
    }

    if(r.recordType()==RcdHeader::acquisitionStart) {
      _nCount[2]=0;
    }

    if(r.recordType()==RcdHeader::spillStart) {

      SubInserter inserter(r);
      CrcLocationData<CrcBeTrgPollData>* d(inserter.insert< CrcLocationData<CrcBeTrgPollData> >(true));
      d->data()->update(false);

      unsigned n(0);
      while(!inSpill()) n++;

      d->data()->update(true);
      d->data()->numberOfPolls(n);

      if(doPrint(r.recordType(),1)) {
	std::cout << " Writing CrcBeTrgPollData subrecord" << std::endl;
	d->print(std::cout," ") << std::endl;
      }
    }

    if(r.recordType()==RcdHeader::trigger) {
      SubInserter inserter(r);
      DaqTrigger *dt(inserter.insert<DaqTrigger>(true));

      dt->triggerNumberInRun(_nCount[0]);
      dt->triggerNumberInConfiguration(_nCount[1]);
      dt->triggerNumberInAcquisition(_nCount[2]);
      dt->crcBufferFull(_nCount[2]>=2000);
      dt->inSpill(inSpill());

      if(doPrint(r.recordType(),1)) dt->print(std::cout," ") << std::endl;

      _nCount[0]++;
      _nCount[1]++;
      _nCount[2]++;
    }

    return true;
  }

  bool inSpill() const {
    UtlTime t(true);
    bool b((t.seconds()%45)==0 && t.microseconds()<300000);
    if(_spillInvert) return !b;
    else             return  b;
  }

private:
  unsigned _nCount[3];
  bool _spillInvert;
};

#endif
