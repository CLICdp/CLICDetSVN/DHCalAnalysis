#ifndef Event_HH
#define Event_HH

#include <iostream>

#include "TrgEvent.hh"
#include "RobEvent.hh"

class Event {

 public:
  Event() {
  }

  void print() const {
    std::cout << "Event::print ";
  }

  bool trgEvent(const TrgEvent *e) {
    if(e==0) return false;
    _trgEvent=(*e);
    return true;
  }

  const TrgEvent* trgEvent() const {
    return &_trgEvent;
  }

  bool robEvent(unsigned rob, const RobEvent *e) {
    if(rob>=15 || e==0) return false;
    _robEvent[rob]=(*e);
    return true;
  }

  const RobEvent* robEvent(unsigned rob) const {
    if(rob>=15) return 0;
    return &_robEvent[rob];
  }


 private:
    TrgEvent _trgEvent;
    RobEvent _robEvent[15];
};

#endif
