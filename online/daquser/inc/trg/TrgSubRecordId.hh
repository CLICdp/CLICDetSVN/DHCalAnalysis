#ifndef TrgSubRecordId_HH
#define TrgSubRecordId_HH

#include "RcdSubHeader.hh"
#include "TrgConfiguration.hh"

template<> unsigned SubRecord<TrgConfiguration,1>::subRecordTypeId() {
  return RcdSubHeader::trg+0;
}

#endif
