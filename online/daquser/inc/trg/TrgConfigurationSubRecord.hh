#ifndef TrgConfigurationSubRecord_HH
#define TrgConfigurationSubRecord_HH

#include "SubRecord.hh"

class TrgConfigurationSubRecord : public SubRecord {

public:
  TrgConfigurationSubRecord() {
    set();
  }

  void set() {
    subRecordType(SubRecord::trigger);
    numberOfWords((sizeof(TrgConfigurationSubRecord)-sizeof(SubRecord))/4);
    _ignorTrigger=0;
  }

  bool ignorTrigger() const {
    return (_ignorTrigger!=0);
  }

private:
  unsigned _ignorTrigger;
};

#endif
