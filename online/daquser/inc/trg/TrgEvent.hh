#ifndef TrgEvent_HH
#define TrgEvent_HH

#include <iostream>

class TrgEvent {

 public:
  TrgEvent() {
    reset();
  }

  void reset() {
    for(unsigned i(0);i<15;i++) _status[i]=0;
  }

  unsigned short* channels() {
    return _array;
  }


  void print() const {
    std::cout << "TrgEvent::print ";
  }


 private:
  unsigned short _status[15];
  unsigned short _array[9720];
};

#endif
