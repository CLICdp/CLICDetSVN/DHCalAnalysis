#ifndef ConfigurationRecord_HH
#define ConfigurationRecord_HH

#include <ctime>

#include "Configuration.hh"

class ConfigurationRecord {

#include "Record.ihh"
  Configuration _configuration;

 public:
  ConfigurationRecord() {}

  unsigned configurationNumber() const {
    return _word4;
  }

  void configurationNumber(unsigned n) {
    _word4=n;
  }

  unsigned numberOfConfigurationEvents() {
    return _word5;
  }

  void numberOfConfigurationEvents(unsigned n) {
    _word5=n;
  }

  void numberOfShortWords(unsigned n=(sizeof(Configuration)+1)/2) {
    _numberOfShortWords=n;
  }

  const Configuration* configuration() const {
    return &_configuration;
  }

  bool configuration(const Configuration *c) {
    if(c==0) return false;
    _configuration = (*c);
    return true;
  }
};

#endif
