#ifndef SubConfigurationStatus_HH
#define SubConfigurationStatus_HH

#include <iostream>

#include "SubRunStatus.hh"

class SubConfigurationStatus : public SubRunStatus {

public:
  SubConfigurationStatus() {
    reset();
  }

  SubConfigurationStatus(const SubRunStatus &r) : SubRunStatus(r) {
    _configurationNumberInRun=0;
    _configurationType=0;
    _maximumNumberOfSpillsInConfiguration=1000000000;
    _maximumNumberOfEventsInConfiguration=1000000000;
  }

  void reset() {
    SubRunStatus::reset();
    _configurationNumberInRun=0;
    _configurationType=0;
    _maximumNumberOfSpillsInConfiguration=1000000000;
    _maximumNumberOfEventsInConfiguration=1000000000;
  }

  unsigned configurationNumberInRun() const {
    return _configurationNumberInRun;
  }

  void configurationNumberInRun(unsigned n) {
    _configurationNumberInRun=n;
  }

  unsigned configurationType() const {
    return _configurationType;
  }

  void configurationType(unsigned t) {
    _configurationType=t;
  }

  unsigned maximumNumberOfSpillsInConfiguration() const {
    return _maximumNumberOfSpillsInConfiguration;
  }

  void maximumNumberOfSpillsInConfiguration(unsigned m) {
    _maximumNumberOfSpillsInConfiguration=m;
  }

  unsigned maximumNumberOfEventsInConfiguration() const {
    return _maximumNumberOfEventsInConfiguration;
  }

  void maximumNumberOfEventsInConfiguration(unsigned m) {
    _maximumNumberOfEventsInConfiguration=m;
  }

  bool increment() {
    _configurationNumberInRun++;
    return true;
  }

  void stopRun() {
    _maximumNumberOfConfigurationsInRun=_configurationNumberInRun;
    _configurationNumberInRun=0;
  }

  void print(ostream &o) const {
    SubRunStatus::print(o);
    o << "SubConfigurationStatus::print() Configuration number "
      << _configurationNumberInRun << " type " << _configurationType 
      << ", maximum numbers of spills "
      << _maximumNumberOfSpillsInConfiguration << " events "
      << _maximumNumberOfEventsInConfiguration << std::endl;
  }

protected:
  unsigned _configurationNumberInRun;
  unsigned _configurationType;

  unsigned _maximumNumberOfSpillsInConfiguration;
  unsigned _maximumNumberOfEventsInConfiguration;
};

#endif
