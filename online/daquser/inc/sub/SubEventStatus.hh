#ifndef SubEventStatus_HH
#define SubEventStatus_HH

#include <iostream>

#include "SubSpillStatus.hh"

class SubEventStatus : public SubSpillStatus {

public:
  SubEventStatus() {
    reset();
  }
  
  SubEventStatus(const SubSpillStatus &s) : SubSpillStatus(s) {
    _eventNumberInRun=0;
    _eventNumberInConfiguration=0;
    _eventNumberInSpill=0;
  }

  unsigned eventNumberInRun() const {
    return _eventNumberInRun;
  }
  
  unsigned eventNumberInConfiguration() const {
    return _eventNumberInConfiguration;
  }
  
  unsigned eventNumberInSpill() const {
    return _eventNumberInSpill;
  }
  
  void reset() {
    SubSpillStatus::reset();
    _eventNumberInRun=0;
    _eventNumberInConfiguration=0;
    _eventNumberInSpill=0;
  }

  bool increment() {
    _eventNumberInRun++;
    _eventNumberInConfiguration++;
    _eventNumberInSpill++;
    return true;
  }

  /*
  bool increment(RunState s) {
    switch(s) {
    case inactive:
      return false;

      //    case active:
      if(_configurationNumberInRun<_maximumNumberOfConfigurationsInRun
      &&         _spillNumberInRun<_maximumNumberOfSpillsInRun
      &&         _eventNumberInRun<_maximumNumberOfEventsInRun) {
	_configurationNumberInRun++;
	_spillNumberInConfiguration=0;
	_eventNumberInConfiguration=0;
	return false;
      }
      return false;

    case running:
      if(_spillNumberInConfiguration<_maximumNumberOfSpillsInConfiguration
      && _eventNumberInConfiguration<_maximumNumberOfEventsInConfiguration) {
	_spillNumberInRun++;
	_spillNumberInConfiguration++;
	_eventNumberInSpill=0;
	return true;
      }
      return false;

      //    case spill:
      if(_eventNumberInSpill<_maximumNumberOfEventsInSpill) {
	_eventNumberInRun++;
	_eventNumberInConfiguration++;
	_eventNumberInSpill++;
	return true;
      }
      return false;
      
    default:
      return false;
      break;
   };
  }
  */

  void stopSpill() {
    _maximumNumberOfEventsInSpill=_eventNumberInSpill;
    _eventNumberInSpill=0;
  }

  void stopConfiguration() {
    _maximumNumberOfEventsInConfiguration=_eventNumberInConfiguration;
    _eventNumberInConfiguration=0;
    SubSpillStatus::stopConfiguration();
  }

  void stopRun() {
    _maximumNumberOfEventsInRun=_eventNumberInRun;
    _eventNumberInRun=0;
    SubSpillStatus::stopRun();
  }

  void print(ostream &o) const {
    SubSpillStatus::print(o);
    o << "SubEventStatus::print() Event numbers in run "
      << _eventNumberInRun << ", in configuration "
      << _eventNumberInConfiguration << ", in spill "
      << _eventNumberInSpill << std::endl;
  }

private:
  unsigned _eventNumberInRun;
  unsigned _eventNumberInConfiguration;
  unsigned _eventNumberInSpill;
};

#endif
