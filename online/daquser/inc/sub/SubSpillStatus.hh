#ifndef SubSpillStatus_HH
#define SubSpillStatus_HH

#include <iostream>

#include "SubConfigurationStatus.hh"

class SubSpillStatus : public SubConfigurationStatus {

public:
  SubSpillStatus() {
    reset();
  }

  SubSpillStatus(const SubConfigurationStatus &c) : SubConfigurationStatus(c) {
    _spillNumberInRun=0;
    _spillNumberInConfiguration=0;
    _maximumNumberOfEventsInSpill=1000000000;
  }

  void reset() {
    SubConfigurationStatus::reset();
    _spillNumberInRun=0;
    _spillNumberInConfiguration=0;
    _maximumNumberOfEventsInSpill=1000000000;
  }

  unsigned spillNumberInRun() const {
    return _spillNumberInRun;
  }

  unsigned spillNumberInConfiguration() const {
    return _spillNumberInConfiguration;
  }

  unsigned maximumNumberOfEventsInSpill() const {
    return _maximumNumberOfEventsInSpill;
  }

  void maximumNumberOfEventsInSpill(unsigned m) {
    _maximumNumberOfEventsInSpill=m;
  }

  bool increment() {
    _spillNumberInRun++;
    _spillNumberInConfiguration++;
    return true;
  }

  void stopConfiguration() {
    _maximumNumberOfSpillsInConfiguration=_spillNumberInConfiguration;
    _spillNumberInConfiguration=0;
  }

  void stopRun() {
    _maximumNumberOfSpillsInRun=_spillNumberInRun;
    _spillNumberInRun=0;
    SubConfigurationStatus::stopRun();
  }

  void print(ostream &o) const {
    SubConfigurationStatus::print(o);
    o << "SubSpillStatus::print() Spill numbers in run "
      << _spillNumberInRun << ", in configuration "
      << _spillNumberInConfiguration
      << ", maximum number of events "
      << _maximumNumberOfEventsInSpill << std::endl;
  }

protected:
  unsigned _spillNumberInRun;
  unsigned _spillNumberInConfiguration;

  unsigned _maximumNumberOfEventsInSpill;
};

#endif
