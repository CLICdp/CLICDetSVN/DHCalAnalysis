#ifndef SubRunStatus_HH
#define SubRunStatus_HH

#include <iostream>

class SubRunStatus {

public:
  // MOVE!!!

  enum RunState {
    inactive,
    running,
    triggerable,
    readable,
    paused,
    runningPaused,
    triggerablePaused,
    readablePaused
  };

  SubRunStatus() {
    reset();
  }

  void reset() {
    _runNumber=0;
    _runType=0;
    _maximumNumberOfConfigurationsInRun=1000000000;
    _maximumNumberOfSpillsInRun=1000000000;
    _maximumNumberOfEventsInRun=1000000000;
  }
  
  int runNumber() const {
    return _runNumber;
  }

  void runNumber(int n) {
    _runNumber=n;
  }

  unsigned runType() const {
    return _runType;
  }

  void runType(unsigned t) {
    _runType=t;
  }

  unsigned maximumNumberOfConfigurationsInRun() const {
    return _maximumNumberOfConfigurationsInRun;
  }

  void maximumNumberOfConfigurationsInRun(unsigned m) {
    _maximumNumberOfConfigurationsInRun=m;
  }

  unsigned maximumNumberOfSpillsInRun() const {
    return _maximumNumberOfSpillsInRun;
  }

  void maximumNumberOfSpillsInRun(unsigned m) {
    _maximumNumberOfSpillsInRun=m;
  }

  unsigned maximumNumberOfEventsInRun() const {
    return _maximumNumberOfEventsInRun;
  }

  void maximumNumberOfEventsInRun(unsigned m) {
    _maximumNumberOfEventsInRun=m;
  }

  void print(ostream &o) const {
    o << "SubRunStatus::print()  Run number " << _runNumber
      << " type " << _runType << ", maximum numbers of configurations "
      << _maximumNumberOfConfigurationsInRun << ", spills "
      << _maximumNumberOfSpillsInRun << ", events "
      << _maximumNumberOfEventsInRun << std::endl;
  }

protected:
  int      _runNumber; // Allow neagtive run numbers for MC
  unsigned _runType;

  unsigned _maximumNumberOfConfigurationsInRun;
  unsigned _maximumNumberOfSpillsInRun;
  unsigned _maximumNumberOfEventsInRun;
};

#endif
