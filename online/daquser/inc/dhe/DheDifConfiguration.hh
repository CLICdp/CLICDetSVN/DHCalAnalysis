#ifndef DheDifConfiguration_HH
#define DheDifConfiguration_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"

#include "SubAccessor.hh"
#include "SubInserter.hh"

#include "DaqRunType.hh"


class DheDifConfiguration : public RcdUserRW {

public:
  DheDifConfiguration() {
  }

  virtual ~DheDifConfiguration() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "DheDifConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }
    
    // Check record type
    switch (r.recordType()) {
      
      // Run start 
    case RcdHeader::runStart: {

      // Access the DaqRunStart
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*>
        v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;

      _runStart=*(v[0]);

      break;
    }
      
      // Configuration start 
    case RcdHeader::configurationStart: {

      // Access the DaqConfigurationStart
      SubAccessor accessor(r);
      std::vector<const DaqConfigurationStart*>
	v(accessor.extract<DaqConfigurationStart>());

      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;
      
      if(v[0]->configurationNumberInRun()==0) {
	DheDifConfigurationData c;
	c.write(true);
	c.readoutCommandEnable(_runStart.runType().bufferRun());

	if(!c.readoutCommandEnable()) {
	  c.skipEventReadout(_runStart.runType().majorType()==DaqRunType::ahc);
	  c.triggerLimit(0);
	}

	// Put the configuration object into the record
	SubInserter inserter(r);
	inserter.insert<DheDifConfigurationData>(c);
	if(doPrint(r.recordType(),1)) c.print(std::cout," ") << std::endl;
      }
      
      break;
    }
      
    default: {
      break;
    }
    };
    
    return true;
  }


private:
  DaqRunStart _runStart;
};

#endif
