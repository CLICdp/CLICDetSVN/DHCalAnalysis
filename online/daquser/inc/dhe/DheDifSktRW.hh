#ifndef DheDifSktRW_HH
#define DheDifSktRW_HH

#include <iostream>
#include <fstream>
#include <string>

#include "RcdUserRW.hh"
#include "RcdIoSkt.hh"

#include "SubAccessor.hh"
#include "DaqRunStart.hh"


class DheDifSktRW : public RcdUserRW {

public:
  DheDifSktRW(std::string host, unsigned port, unsigned tries) : _skipRun(false) {
    _socket.open(host,port,tries);
  }

  virtual ~DheDifSktRW() {
    _socket.close();
  }

  virtual bool record(RcdRecord &r) {

    // Find if run should be skipped or not
    if(r.recordType()==RcdHeader::runStart) {
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*> v(accessor.access<DaqRunStart>());
      if(v.size()==1) {
	_skipRun=
	  v[0]->runType().majorType()==DaqRunType::ahc ||
	  v[0]->runType().majorType()==DaqRunType::slow;
      }
    }

    // Never send trigger records and any records in runs which are unneeded
    if(!_skipRun && r.recordType()!=RcdHeader::trigger) {
      assert(_socket.write(r));
      assert(_socket.read(r));
    }

    // Reset skip flag at end of run
    if(r.recordType()==RcdHeader::runEnd) _skipRun=false;

    if(r.recordType()==RcdHeader::shutDown) assert(_socket.close());

    return true;
  }

private:
  RcdIoSkt _socket;
  bool _skipRun;
};

#endif
