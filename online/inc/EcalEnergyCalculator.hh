//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalEnergyCalculator_HH
#define EcalEnergyCalculator_HH

#include <iostream>

// dual/inc/emc
#include "EcalCalibration.hh"
#include "EcalAdc.hh"
#include "EcalEnergy.hh"
#include "EcalPhysicalPad.hh"

//////////////////////////////////////////////////////////////////////////////

/// Class to calculate the energy values of the ECAL channels.
/**
*/
class EcalEnergyCalculator 
{

   public:

//............................................................................
   /// Constructor, an EcalCalibration is required.
   /**
   */
   EcalEnergyCalculator(const EcalCalibration &c) : _cal(c) 
   {
   }
//............................................................................
   /// Destructor.
   /**
   */
   virtual ~EcalEnergyCalculator() 
   {
   }
//............................................................................
   /// Calculate the energy values of the ECAL channels.
   /** Output is stored in EcalEnergy.
   */
   bool calculate(const EcalAdc &a, EcalEnergy &e) 
   {
      for(EcalPhysicalPad pp(0);pp.isValid();pp++)
      {
	 e.energy(pp,_cal.energy(pp,a.adc(pp)));
      }
      return true;
   }
//............................................................................

   private:

   const EcalCalibration &_cal;

};//end class EcalEnergyCalculator

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
