//
// $Id: EbDhcRecordsTrig.hh,v 1.2 2009/19/09 14:55:00 jacobsmith Exp $
// modify
// changed 
// any 16-byte data pack that has corruption or chksum error is ignored
//

#ifndef EbDhcRecordsTrig_HH
#define EbDhcRecordsTrig_HH

// c++
#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <cmath>
#include <fstream>

// daq
#include "RcdRecord.hh"
#include "DaqEvent.hh"
#include "SubAccessor.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"

// dhc
#include "DhcReadoutConfigurationData.hh"
#include "DhcFeHitData.hh"

// ecal
#include "EcalMap.hh"
#include "EcalAdc.hh"
//#include "EbEcalRawAdcExtractor.hh"
#include "EcalPedestalCalculator.hh"
#include "EcalCalibration.hh"
#include "EcalEnergyCalculator.hh"
#include "EcalEnergy.hh"
#include "DatMgr.hh"

// crc
#include "CrcBeTrgHistoryHit.hh"
#include "CrcVlinkTrgData.hh"

// dhc event builder
#include "EbDhcEventTrig.hh"
#include "EbDhcDTCalculator.hh"
//#include "EbDhcGeom.hh"

// root
#include "TROOT.h"
#include "TH1.h"
#include "TFile.h"
#include "TCanvas.h"

class EbDhcRecordsTrig {

public:
    //EbDhcRecordsTrig() 
    EbDhcRecordsTrig(const unsigned &r, 
                                     const std::string& w, 
                                     unsigned &t,
                                     unsigned &emt,
                                     const double &m)
    {
        _runNumber = r;
        _aout << w << "run" << r << ".beam.txt";
        std::cout<<std::endl << "ascii file name: " << _aout.str().c_str();
        std::ofstream tfile;
        tfile.open(_aout.str().c_str(),ofstream::trunc);
        tfile.close();
        
        _rout << w << "run" << r << ".root";
        std::cout<<std::endl << "root file name: " << _rout.str().c_str() <<std::endl;
        std::cout<<std::endl; 

    _rfile = new TFile(_rout.str().c_str(), "recreate");
//  _cdhts = _rfile->mkdir("hts");

        _hHitTS = new TH1I("hHitTS","Hit-Trg Timestamps All Events",101,-50.5,50.5);
        _hNTrgPacks = new TH1I("hNTrgPacks","No. Trg Packs All Events",300,0,300);
        _hCorruptLog = new TH1I("hCorruptLog","Corruption Log",20,0,20);
        _hErrLog = new TH1I("hErrLog","Error Log",256,0,256);
        _hErrLogAd = new TH1I("hErrLogAd","Error Log Address",11520,0.,11520.);

        _timestampRange = t;
        _setBadLevelZero = false;
        _setBadLevelOne = false;
        _setGood = false;
        _eventRecordCount = 0;
        _badSubRecord = 0;
        _NTotalEventSubRecords = 0;
        _corruptEventCount = 0;
        _printTimes = 0;
        _errorBL0 = 0;
        _eventHitOutOfTSRangeCount = 0;
        _trgOutOfTSRangeCount = 0;
        _trgOutOfTSRangeEventCount = 0;
        _emptyEventCount = 0;
        _goodEventCount = 0;
        _wrongCountTrgPackCount = 0;
        _wrongCountTrgPackEmptyCount = 0;
        _countEmptyEvents = 0;
        _countEmptyEventsPostWrite = 0;
        _countEmptyEventsPostDec = 0;
        for (int i(0);i<8;i++) { _ttCounter[i]=0;}
        for (int i(0);i<32;i++) { _lhCounter[i]=0;}
        for (int i(0);i<3;i++) { _tcmtConfigCounter[i]=0;}
        for (int i(0);i<20;i++) { _corruptLog[i]=0;}

// data for tcmt ::event
        _configCounter = -1;

// data for tcmt calibration
        _mipThreshold = m;
        _workingslot = 12;
        _showdebug = false;
    for(int ilayer = 0 ; ilayer < 16; ilayer++) {
        for(int istrip = 0 ; istrip < 20 ; istrip++) {
            char workingname[256];
            sprintf(workingname,"pedestal_%d_%d",ilayer+1,istrip);
            _hped[ilayer][istrip]  = new TH1I(workingname,workingname,32*1024,0,32*1024);
        }
    }


 // constructor for ecal
     DatMgr *mg =  &mg->getInstance();
 
     string ecalMapFile    = 
         mg->getEcalMapFile(_runNumber,"ecal/");
         //"/home/jacobsmith/ecal/data/mapping/"+mg->getEcalMapFile(_runNumber);
     _cableMap = new EcalMap();
     ecalMapFile = "ecal/data/mapping/"+ecalMapFile;
     assert((*_cableMap).read(ecalMapFile));

    std::cout<<std::endl<<"ecal map file "<<ecalMapFile.c_str()<<std::endl;
 
     string ecalCalibrFile = 
         mg->getEcalCalibrFile(_runNumber,"ecal/");
     _ecalCalibr = new EcalCalibration(*_cableMap);
     ecalCalibrFile = "ecal/data/calibration/"+ecalCalibrFile;
     assert((*_ecalCalibr).read(ecalCalibrFile));
 
    std::cout<<std::endl<<"ecal cal file "<<ecalCalibrFile.c_str()<<std::endl;
 
     //objects to extract adcs and to calculate pedestals and energy
 //  _adcExtr = new EbEcalRawAdcExtractor(*_cableMap);
     _pedCalc = new EcalPedestalCalculator(*_ecalCalibr);
     _energyCalc = new EcalEnergyCalculator(*_ecalCalibr);
     if (emt ==1) _ecalThreshold = 0.5;
     else _ecalThreshold = 0.5;
     _ecalIn = false;
 // end of ecal constructor data


    _nCrates = 2;
    for (unsigned i = 0; i<_nCrates; i++) {
        //_vmeadEnable[i] = 0;
        //_dcoladEnable[i] = new unsigned[20];
        //_dconadEnable[i] = new unsigned*[20];
        for (unsigned j = 0; j<20; j++) {
            //_dcoladEnable[i][j] = 0;
            //_dconadEnable[i][j] = new unsigned[12];
            for (unsigned k = 0; k<12; k++) {
                //_dconadEnable[i][j][k] = 0;
                _configEnable[i][j][k] = 0;
            }
        }
    }

    } // end of constructor


    virtual ~EbDhcRecordsTrig() {
    }

    void writeRoot();
    void loadTcmtCalibration();

    void report() { // change variable names probably
        std::cout<<std::endl<< " ndcon: "<<_nDcon;
        std::cout<<std::endl<< " | | | | | | | | | | | | | | | | | | | | ";
        std::cout<<std::endl<< " | | | | EventBuilder Run Report | | | | ";
        std::cout<<std::endl<< " | | | | | | | | | | | | | | | | | | | | ";
        std::cout<<std::endl; 

        //std::cout<<std::endl<<" Percentage trashed events ";
        std::cout<<std::endl 
                         << " Total Event SubRecords in Run: " 
                         << _NTotalEventSubRecords;
        //std::cout<< std::endl
        //         << " Bad Events: " << _badSubRecord;
        std::cout<< std::endl 
                         << " Written Events: " << _goodEventCount;
                         //<< " Good Events: " << _goodEventCount;
        std::cout<<std::endl 
                         << " Empty Events: " << _emptyEventCount;
        std::cout<<std::endl 
                         << " Non-Empty Events : " 
                         << _NTotalEventSubRecords - _emptyEventCount;
        std::cout<<std::endl;
        std::cout<<std::endl 
                         << " Events containing corrupt data: " 
                         << _corruptEventCount
                         << std::endl
                         <<" (corrupt data packs not written in event) "
                         << std::endl
                         <<" (corrupt data packs not used as seeds ) "
                         << std::endl
                         <<" ( if a real event exists it is salvaged and written) ";
        std::cout<<std::endl 
                         //<< " Events trashed due to"
                         << " Events with"
                         << " Hit TS Out Of Range: " 
                         << _eventHitOutOfTSRangeCount 
                         << std::endl << "    hit-trg ts (inclusive) range = "
                         << _timestampRange
                         << std::endl << "    (default trg-hit delta ts = 18"
                         << std::endl << "    this count excludes unconfigurable chip"
                         << std::endl << "    crate 220,slot5,dcon11,chip11"
                         << std::endl << "    Warning, this is only chip id as bad)";
        std::cout<<std::endl 
                         //<< " Events trashed due to"
                         << " Trg packs with"
                         << " Trg TS Out Of Range: " 
                         << _trgOutOfTSRangeCount 
                         << std::endl 
                         << " if any, occurs in "
                         << _trgOutOfTSRangeEventCount << " Events"
                         << std::endl<< "    trg-trg (inclusive) range = 0"; // check calc below
        std::cout<<std::endl 
                         << " Events with"
                         << " wrong number of trg packs: " 
                         << _wrongCountTrgPackCount
                         << std::endl <<" (in events not containing errors) "; 
        if (_wrongCountTrgPackEmptyCount > 0) {
        std::cout<<std::endl 
                         << " number of empty events with wrong trg pack count "
                         << _wrongCountTrgPackEmptyCount;
        }
        std::cout<<std::endl<<"Event Count by Type";
        for (int i(0);i<8;i++) {
            if (i == 0) std::cout<<std::endl<<" hadrons only: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 1) std::cout<<std::endl<<" inner only: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 2) std::cout<<std::endl<<" outer only: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 3) std::cout<<std::endl<<" inner and outer: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 4) std::cout<<std::endl<<" muon only: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 5) std::cout<<std::endl<<" inner and muon: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else if (i == 6) std::cout<<std::endl<<" outer and muon: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
            else std::cout<<std::endl<<" inner, outer, and muon: "<<_ttCounter[i]
                                    <<", "<<100*_ttCounter[i]/(double)_tcmtConfigCounter[2]<<" %";
        }
        std::cout<<std::endl<<" any inner: "<< _ttCounter[1]+_ttCounter[3]+_ttCounter[5]+_ttCounter[7]
                         << ", "
                         << 100*(_ttCounter[1]+_ttCounter[3]+_ttCounter[5]+_ttCounter[7])/(double)_tcmtConfigCounter[2]
                         << " %";
        std::cout<<std::endl<<" not inner: "<< _ttCounter[0]+_ttCounter[2]+_ttCounter[4]+_ttCounter[6]
                         << ", "
                         << 100*(_ttCounter[0]+_ttCounter[2]+_ttCounter[4]+_ttCounter[6])/(double)_tcmtConfigCounter[2]
                         << " %";
        std::cout<<std::endl<<" not muon: "<< _ttCounter[0]+_ttCounter[1]+_ttCounter[2]+_ttCounter[3]
                         << ", "
                         << 100*(_ttCounter[0]+_ttCounter[1]+_ttCounter[2]+_ttCounter[3])/(double)_tcmtConfigCounter[2]
                         << " %";
        std::cout<<std::endl<<"Event Count by TCMT Config Type";
        for (int i(0);i<8;i++) {
            if (i == 0) std::cout<<std::endl<<" Pedestal Calibration: "<<_tcmtConfigCounter[i];
            else if (i == 1) std::cout<<std::endl<<" LED Calibration: "<<_tcmtConfigCounter[i];
            else if (i == 2) std::cout<<std::endl<<" Beam Data: "<<_tcmtConfigCounter[i];
        }

        std::cout<<std::endl;
        std::cout<<std::endl<<"Crc Line History Report";
        for (int i = 0; i<32;i++) {
            std::cout<<std::endl<<"line: "<<i<<", "<<_lhCounter[i];
        }
        std::cout<<std::endl;
        std::cout<<std::endl<<" | | | | | end eb run report | | | | | ";
        std::cout<<std::endl<<" see this run's root file for histograms on timestamps";
        std::cout<<std::endl<<"tcmt mip threshold: "<<_mipThreshold;
        std::cout<<std::endl<<"Number of Empty Events (opt1): "<<_countEmptyEvents;
        std::cout<<std::endl<<"Number of Empty Events (opt2): "<<_countEmptyEventsPostDec;
        std::cout<<std::endl<<"Number of Empty Events (opt3): "<<_countEmptyEventsPostWrite;
        std::cout<<std::endl<<"Percentage Empty: "<<((double)_countEmptyEvents)/((double)_NTotalEventSubRecords)*100;
        std::cout<<std::endl;
        std::cout<<std::endl;
    }

    void event(RcdRecord &r);
    void eventIP(RcdRecord &r);
    void writeEvents();

private:

    int _countEmptyEventsPostDec;
    int _countEmptyEventsPostWrite;


// data for tcmt calibration
    int _getstripfromfechipchan[2][12][18];
    int _getlayerfromfechipchan[2][12][18];
    ifstream _pedestalfile;
    ifstream _chipchannelfile;
    ifstream _calibrationfile;
    int _tlayer;
    int _tstrip;
    int _tchip;
    int _tchannel;
    bool _showdebug;
    int  _getpedfromlayerstrip[16][20];
    double _getmipfromfechipchan[2][12][18];
    double  _getpedfromfechipchan[2][12][18];
    int  _getmipfromlayerstrip[16][20];
    TH1I *_hped[16][20];
    int  _getpedrmsfromlayerstrip[16][20];
    double _mipThreshold;
    int _workingslot;

// data for data checking
    unsigned _nDcon;
    unsigned _nCrates;
    unsigned _configEnable[2][20][12];

// data for tcmt ::event
    int _configCounter;
    unsigned _ttCounter[8];
    unsigned _lhCounter[32];
    unsigned _tcmtConfigCounter[3];

// data for ecal
    EcalMap*               _cableMap;
    EcalCalibration*       _ecalCalibr;
//  EbEcalRawAdcExtractor*    _adcExtr;
    EcalPedestalCalculator* _pedCalc;
    EcalEnergyCalculator*   _energyCalc;
    bool _ecalIn;
    double _ecalThreshold;

// data for dhcal 
    TFile* _rfile;

    DhcFeHitData* _copy;
    TH1I* _hHitTS;
    TH1I* _hNTrgPacks;
    TH1I* _hCorruptLog;
    TH1I* _hErrLog;
    TH1I* _hErrLogAd;
//  TDirectory* _cdhts;

    std::vector<EbDhcEventTrig*> _events;
    std::vector<DhcFeHitData*> _tempHitPacks;
    std::vector<DhcFeHitData*> _tempTrgPacks;
    std::ostringstream _aout;
    std::ostringstream _rout;
    std::ofstream _afile;

// cerenkov vector used to store values from Ttm
    std::vector<unsigned> _cerenkov;

    unsigned _timestampRange;
    unsigned _runNumber;
    unsigned _NTotalEventSubRecords;
    unsigned _eventRecordCount; // counts event subrecords
    unsigned _badSubRecord;
    unsigned _errorBL0;
    unsigned _corruptEventCount;
    unsigned _corruptLog[20];
    unsigned _printTimes;
    unsigned _emptyEventCount;
    unsigned _goodEventCount;
    unsigned _trgOutOfTSRangeCount;
    unsigned _trgOutOfTSRangeEventCount;
    unsigned _wrongCountTrgPackCount;
    unsigned _wrongCountTrgPackEmptyCount;
    unsigned _eventHitOutOfTSRangeCount;
    unsigned _countEmptyEvents;

    bool _setBadLevelZero;
    bool _setBadLevelOne;
    bool _setGood;

};

#ifdef CALICE_DAQ_ICC

void EbDhcRecordsTrig::event(RcdRecord &r) {
        
        SubAccessor accessor(r);
     
        switch (r.recordType()) { // check record type

        // contains dhcal hit data and trg timestamps
        // also contains tcmt data (including crc trigger signals)
        case RcdHeader::configurationStart: {

            //std::cout<<"processing configurationStart record"<<std::endl;

         if (_eventRecordCount == 0) {
            std::vector<const DhcReadoutConfigurationData*>
                vcd(accessor.access<DhcReadoutConfigurationData>());
        
            // vcd size is number of crates

            for(unsigned i(0);i<vcd.size();i++) { // one for each crate
                //_vmeadEnable[i] = 1;
                for(unsigned s(2);s<=21;s++) { // one for each dcol in a crate
                    if (s>3) {
                        if (vcd[i]->slotEnable(s)){
                            //_dcoladEnable[i][s-4] = 1;

                            for (unsigned f(0);f<12;f++) {
                                if (vcd[i]->slotFeEnable(s,f)) {
                                    _nDcon+=1;
                                    //_dconadEnable[i][s-4][f] = 1;
                                    _configEnable[i][s-4][f] = 1;
                                } 
                            }
                        }
                    }
                } // end slot (dcol)loop
            } // end vcd loop
            } // end condition for eventRecordCount==0

            _configCounter++;
            break;
        }

        // spillStart occurs before a set of (trigger,event) record headers
        // there are an equal number of trigger and event type record headers
        // these are matched according to timestamp and order
        case RcdHeader::spillStart: {
            //std::cout<<"processing spillStart record"<<std::endl;

            if (_cerenkov.size() > 0) {
                cout<<endl<<"WARNING";
                cout<<endl<<"WARNING";
                cout<<endl<<"WARNING";
                cout<<endl<<"mismatch between number of triggers and number of events";
            }
            _cerenkov.clear();
            break;
        }

        case RcdHeader::trigger: {


            unsigned ttmCerType = 0;

            std::vector<const TtmLocationData<TtmTriggerData>*>
                v(accessor.access<TtmLocationData<TtmTriggerData> >());

 
            unsigned NwordsTtm = v[0]->data()->numberOfWords();



            if (NwordsTtm > 0){


                const unsigned *d(v[0]->data()->data());
                unsigned NwordsA = 0, NwordsB = 0, NwordsTrg = 0;

                NwordsTrg = d[0];
                NwordsA = d[1+NwordsTrg];
                NwordsB = d[1+NwordsTrg+NwordsA+1];

                if (NwordsA > 0) ttmCerType+=2; // outer
                if (NwordsB > 0) ttmCerType+=1; // inner

                // ttmCerType is now ready to be matched to its event
                _cerenkov.push_back(ttmCerType);

            }


            break;
        }

        case RcdHeader::event: {
            //std::cout<<"processing event record"<<std::endl;


        _eventRecordCount+=1;
        EbDhcEventTrig* event = new EbDhcEventTrig();

/*------------------------------------------------------------
------------ TCMT ----------- DATA --------------------------
---------------------BELOW----------------------------------*/

        std::vector<const CrcLocationData<CrcVlinkEventData>* > 
    cv(accessor.access< CrcLocationData<CrcVlinkEventData> >());

        // trigger data data
        unsigned trgType = 0;
        int eventConfigType = -1;
        //int trigpattern[16][4];

//std::cout<<"vector cv size = "<<cv.size()<<std::endl;
//std::cout<<"vector cv size = "<<cv.size()<<std::endl;

        for(unsigned i(0); i < cv.size();i++){
            const CrcVlinkTrgData *td(cv[i]->data()->trgData());
            //const CrcVlinkTrgData *td(vecCrcLoc[i]->data()->trgData());
            if(td && ((_configCounter % 3) == 2)) { // beam data
                _tcmtConfigCounter[2]++;
                eventConfigType = 0;
                std::vector<CrcBeTrgHistoryHit> trigx(td->lineHistory(19));
                assert(trigx.size()<=1);

                // process trigger data
                if(trigx.size()==1) { 
                    for(int ttt = 0 ; ttt < 32 ; ttt++) {
                        std::vector<CrcBeTrgHistoryHit> trigv(td->lineHistory(ttt));
                        if (trigv.size() > 0) {
                            _lhCounter[ttt]++;
                            // outline[ttt]++;
                            // ttt == 7 (lvds 1) inner cerenkov (positron/electron)
                            // ttt == 8 (lvds 2) outer cerenkov (positron/electron)
                            // ttt == 12 (lvds 5) muon (last scint hit)
                            // ttt == 13 (lvds 6)
                            // ttt == 1 (lvds 3)
                            // binary format, 0th pos innter c`
                            //                1st pos outer c`
                            //                2nd pos scint

                            switch (ttt) {
                                case 7: {
                                    trgType+=1;
                                    break;
                                }
                                case 8: {
                                    trgType+=2;
                                    break;
                                }
                                case 1: {
                                    if (_runNumber > 630101) trgType+=4;
                                    break;
                                }
                                case 12: { // october runs put muonB to lvds 5, bkpanel 12
                                    if (_runNumber < 600137) trgType+=4;
                                    break;
                                }
                                default: {
                                break;
                                }
                            };

                        } // end condition trigy 
                    } // done looping through crc trg lines
                _ttCounter[trgType]++;
                event->trgType(trgType);
                } // end condition trigx

                // now get ecal data!
                
            } // end condition, beam trigger
            if (td && ((_configCounter % 3) == 0)) { // pedestal cal
                eventConfigType = 100;
                _tcmtConfigCounter[0]++;
            }
            if (td && ((_configCounter % 3) == 1)) { // led cal
                eventConfigType = 200;
                _tcmtConfigCounter[1]++;
            }
        } // done looping through crcs

        event->tcmtConfig(eventConfigType);

/*------------------ABOVE------------------------------------
------------ TCMT ----------- DATA --------------------------
------------------------------------------------------------*/

 /*------------------------------------------------------------
 ------------ ECAL ----------- DATA --------------------------
 ---------------------BELOW----------------------------------*/
 
         EcalAdc ecalAdc;
         EcalEnergy ecalEnergy;
         if (_ecalIn) {
             int channelsRead(0);
             event->setEcalThreshold(_ecalThreshold);
             //bool goodAdcReadout = (*_adcExtr).extract(r,ecalAdc);
             ecalAdc.reset();
             std::vector<const CrcLocationData<CrcVlinkEventData>*>
                 ev(accessor.extract< CrcLocationData<CrcVlinkEventData> >());

//std::cout<<"vector ev size = "<<ev.size()<<std::endl;
//std::cout<<"vector ev size = "<<ev.size()<<std::endl;

             // Loop over boards = VME slots
             for(unsigned i(0);i<ev.size();i++) {
                 const unsigned slot(ev[i]->slotNumber());
                 int crate(ev[i]->crateNumber());

//std::cout<<"slot number = "<<slot<<std::endl;
//std::cout<<"slot number = "<<slot<<std::endl;

//std::cout<<"crate number = "<<crate<<std::endl;
//std::cout<<"crate number = "<<crate<<std::endl;

                 if(crate != 236 ) continue;
                 // Loop over front ends = connectors and get pointer to data from this front end
                 unsigned int thischannel(0);
                 for(unsigned fe(0);fe<8;fe++) {
                     const CrcVlinkFeData *fd(ev[i]->data()->feData(fe));
                     if(fd!=0 && _cableMap->isConnected(slot,fe)) {
                         // Loop over the 18 channels per VFE chip and get pointer to time sample
                         for(unsigned chan(0);
                                 chan<ev[i]->data()->feNumberOfAdcSamples(fe) && 
                                 chan<18;
                                 chan++) {
                             const CrcVlinkAdcSample *as(fd->adcSample(chan));
                             if(as!=0) {
                                 // Loop over the 12 VFE chips per time sample
                                 for(int chip=0;chip<12;chip++) {
                                     EcalReadoutPad rp(slot,fe,chip,chan);
                                        if(_cableMap->physical(rp) && ecalAdc.adc(_cableMap->map(rp))==-32768) {
                                            // Find physical pad for this readout channel and 
                                            //   get ADC value
                                            ecalAdc.adc(_cableMap->map(rp),as->adc(chip));
                                            channelsRead++;
                                        }
                                 } // end loop over 12 VFE chips 
                             } // end condition as not empty
                         } // end loop over 18 channels
                     } // condition its connected
                 } // end loop over front ends 
             } // end loop over boards   
             bool goodAdcReadout(channelsRead == _cableMap->numberOfChannels());
             if (!goodAdcReadout) {
                 std::cout<<std::endl<<"something wrong"; 
                 std::cout<<std::endl<<"something wrong"; 
             }
             else { // readout of ecal is ok
                 if (_configCounter % 3 == 0) { // pedestal
                     // reset pedestals at the 1st of every set of pedestal events
                     if((*_pedCalc).isOFF()) {
                         (*_pedCalc).reset();
                         (*_pedCalc).setON();
                     }
                     //calculate the pedestals
                     (*_pedCalc).calculate(ecalAdc);
                 }
                 if (_configCounter %3 == 2) { // beam data
                     //turnoff pedestal calculator 
                     if((*_pedCalc).isON()) {
                         (*_pedCalc).setOFF();
                     }
                     //subtract pedestals and 
                     //  use calibration to express energy in mips
                     (*_energyCalc).calculate(ecalAdc,ecalEnergy);
                     //add ecalEnergy to event
                     event->addEcalEnergy(ecalEnergy);
                 }
             }
            
         }
 
 /*------------------ABOVE------------------------------------
 ------------ ECAL ----------- DATA --------------------------
 ------------------------------------------------------------*/

/*------------------------------------------------------------
------------ DHCAL ----------- DATA --------------------------
---------------------BELOW----------------------------------*/

        if (_eventRecordCount == 1) _setBadLevelOne = true;

        _setGood = false;
        _setBadLevelZero = false;

        std::vector<const DhcLocationData<DhcEventData>*>
    v(accessor.access<DhcLocationData<DhcEventData> >());

        //std::cout<<std::endl<<"started event "<<_eventRecordCount;
        EbDhcDTCalculator* calc = new EbDhcDTCalculator();
        bool firsteventhit = true;
        bool firsteventtrg = true;
        bool trgswitch = false;
        bool  hitswitch = false; // switches to find misplaced hit packs
        bool emptyEvent = true; // assume event is empty until 
                                                        // a hit pack is seen
        unsigned firsthitts = 0;
        unsigned firsttrgts = 0;
        bool corrupt = false;
        unsigned trgPackCount = 0;
        unsigned hitOutOfTSRangeCount = 0;
        bool badChip = false;
        bool hitTSrange = true;
        bool trgTSrange = true;
        bool trgTSrangeEvent = true;
        bool corruptEvent = false;
        unsigned nHitPacks = 0;
        
        event->setBadLevelOne(_setBadLevelOne);
//    _cdhts->cd();


        unsigned dconTrgPckCnt = 0; // count dcon's trg packs
        for (unsigned s(0); s<v.size(); s++) { //v size is number of DCOLs

            trgswitch = false;
            hitswitch = false;

            if ((v[s]->data()->numberOfWords())>0) { // checks if dcol has data
                //_hDcolData->Fill(s);
                for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) { 
                //numberOfWords/4 is total number of data packages in a DCOL

                    // reset counts and package status
                    corrupt = false;

                    // extracts data from arena 
                    const DhcFeHitData* hits(v[s]->data()->feData(i));
                    unsigned hitvmead = ((unsigned)v[s]->location().crateNumber() - 220);
                    badChip = false;
                    hitTSrange = true;
                    trgTSrange = true;

                    // ignore this data pack if it contains chksum error
                    if (!hits->verify()) continue;

                    if (hits->trg()) dconTrgPckCnt+=1;

                    // check for data corruption
                    // corruption is unrealistic addressess for trg and hit 
                    // hits
                    if (!hits->trg()) {
                         
                        nHitPacks++;

                        hitswitch = true;
                        emptyEvent = false;
                        if (!hits->verify()) _hCorruptLog->Fill(12);
                        if (hits->err() > 0) { 
                            if (hits->verify()) {
                                _hErrLog->Fill(hits->err());
                                if (hits->dcolad() < 20 && hits->dconad() < 12 && hitvmead < _nCrates) {
                                //if (hits->dcolad() < 20 && hits->dconad() < 12 && hits->vmead() < _nCrates) {
                                    if (hitvmead == 0) {
                                    //if (hits->vmead() == 0) {
                                    }
                                    else {
                                    }
                                    _hErrLogAd->Fill((hitvmead*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad()));
                                    //_hErrLogAd->Fill((hits->vmead()*12*20*24)+(hits->dcolad()*12*24)+(hits->dconad()*24)+(hits->dcalad()));
                                }
                            }
                            corrupt = true; 
                            _corruptLog[0]++;
                            _hCorruptLog->Fill(0);
                            if (!hits->verify()) _hCorruptLog->Fill(13);
                            if (hits->err() == 1 )_hCorruptLog->Fill(8);
                            if (hits->err() == 2 )_hCorruptLog->Fill(9);
                            if (hits->err() == 3 )_hCorruptLog->Fill(10);
                            if (hits->err() == 4 )_hCorruptLog->Fill(11);
                        } // errors
                        if (hitvmead > (_nCrates-1)) { 
                        //if (hits->vmead() > (_nCrates-1)) { 
                            corrupt = true; 
                            _corruptLog[1]++;
                            _hCorruptLog->Fill(1);
                        }// vmead
                        if (hitvmead < _nCrates) {
                        //if (hits->vmead() < _nCrates) {
                            if (hits->dcolad() < 20) {

                                if (hits->dconad() < 12) {
                                    //if (_dconadEnable[hitvmead][hits->dcolad()][hits->dconad()]==0) {
                                    if (_configEnable[hitvmead][hits->dcolad()][hits->dconad()]==0) {
                                    //if (_dconadEnable[hits->vmead()][hits->dcolad()][hits->dconad()]==0) {
                                        corrupt = true;// dconad
                                        _corruptLog[3]++;
                                        _hCorruptLog->Fill(3); // WARNING, vmead is always 0, only _copy sets vmead
                                    }
                                }
                                else _hCorruptLog->Fill(7);
                            }
                            else _hCorruptLog->Fill(6);
                        }
                        if (hits->dcalad() > 23) { 
                            corrupt = true; 
                            _corruptLog[4]++;// dcalad
                            _hCorruptLog->Fill(4);
                        }
                    }

                    // trgs
                    // trg dcalad default is 31
                    if (hits->trg() && hits->dcalad() != 31) {
                        corrupt = true; // trg pack wrong dcalad
                        _corruptLog[5]++;
                        _hCorruptLog->Fill(5);
                    }

                    // corruption summary
                    if (corrupt) {
                        corruptEvent = true;
                        //_setBadLevelZero = true; // event is bad 
                    }
                    //if (corrupt) break; // ignore this pack if corrupt
                    if (corrupt) continue; // ignore this pack if corrupt
                    
//
// Remainder of this pack's calculations are only completed
// if data is not corrupt
// Pack is NOT used if corrupt 
//

                    if (hits->trg()) {
                        trgPackCount++;
                        trgswitch = true;
                        // only need to operate if this event has a chance of being written
                        // only if data not corrupt
                        //oldway//if (!_setBadLevelZero) { // data not corrupt
                                                                         // implies data has been checked for corruption
                            // set seed for calulating trg ts differences
                            // uses first trg pack to calculate ts difference for trg packs
                            // the first non-corrupt trg serves as the seed
                            if (firsteventtrg) { // event starts with this as true
                                firsttrgts = hits->timestamp();
                                firsteventtrg = false;
                            }
                            // calculates ts difference between trg ts only
                            // this event is set as bad if trg ts go out of range
                            // uses same range as hit packs
                            if (fabs(calc->dt( firsttrgts , hits->timestamp()) ) > 0) {

                     
                                trgTSrange = false;
                                trgTSrangeEvent = false;
                                _trgOutOfTSRangeCount++;
                                //_setBadLevelZero = true; //assuming all trg packs exist and Dts==0
                            }
                        //} // oldway, now dont care only if corrupt data
                    }

                    // check hit data for consistency
                    // only if data not corrupt 
                    // AND only if trg ts not out of sync
                    //oldway//if (!hits->trg() && !_setBadLevelZero ) { // check hit data for consistency
                    if (!hits->trg()) { // check hit data for consistency
                        trgswitch = false;
                        //hitswitch = true;
                        /*if (i == 0) {
                            _setBadLevelZero = true; 
                        }*/

                        if (firsteventhit) {
                            firsthitts = hits->timestamp();
                            firsteventhit = false;
                        }

                        if (hits->dcalad() == 11 
                                && hits->dcolad() == 1 
                                && hits->dconad() == 11) { // bad chip gives high noise
                            badChip = true;
                        }

                        if (!badChip) { 
                            //_rfile->Open();
                            _hHitTS->Fill(calc->dt( firsttrgts, hits->timestamp() ));
                            //if (_eventRecordCount < 50) std::cout<<std::endl<<" value going into hist: "<<calc->dt( firsttrgts, hits->timestamp() );
                            //_rfile->Close();
                        }

                        // check timestamp range
                        // based on first trg timestamp, if error from trg ts change this
                        if ( (fabs(calc->dt( firsttrgts, hits->timestamp()+18 ))) 
                                                                 > _timestampRange) {
                         
                            hitTSrange = false;
                            if (!badChip) { // dont count packs from bad chip
                                hitOutOfTSRangeCount++;
                            }
                        }

                    } // done checking if this event is bad level 0

                    if (!hits->trg()  && hitTSrange) { 

                        // copies data from arena to my own copy
                        _copy = new DhcFeHitData;
                        assert(_copy!=0);
                        memmove(_copy,hits,sizeof(DhcFeHitData));

                        // set vmead
                        // crate number starts at 220
                        _copy->vmead(((unsigned)v[s]->location().crateNumber()) - 220);

                        // add data to event
                        event->addHitPack(_copy);
                        //cout<<endl<<"added dhcal hit pack to event";
                    } // done filling event with hit data

                } // finished going through data packages in this dcol
            } // end conditions if dcol has data
        } // finished going through dcols


        if (nHitPacks == 0) _countEmptyEvents++;

        //_hDconTrgPckCnt->Fill(dconTrgPckCnt);
        // set event timestamp
        // assumes all trg ts are same
        // do this first!!
        event->timestamp(firsttrgts);

        // count events containing hit ts out of range
        // i.e. counting events with noise
        // does not count noisy bad chip (see above)
        if (hitOutOfTSRangeCount > 0) _eventHitOutOfTSRangeCount++;

        // counts events containing corrupt packs
        // does not count specific problems
        if (corruptEvent) _corruptEventCount++;

        // event is bad only if there arent the right number of trg packs
        // WARNING:: if event set as bad trgPackCount is not accurate
        // 228 = 19 x 12, 9 dcols with 12 dcons and 2 dcols with 6 dcons
        // 228 = number of front end boards
        if (!corrupt) _hNTrgPacks->Fill(trgPackCount);
        if (trgPackCount != _nDcon && !corrupt ) { 
        //oldway//if (trgPackCount != 228 && !_setBadLevelZero) { 

            _wrongCountTrgPackCount++;
            if (emptyEvent) _wrongCountTrgPackEmptyCount++;
            //_setBadLevelZero = true;

            /*if (_printTimes < 20 ) {
                                std::cout<<std::endl<<" trg pack count: "<<trgPackCount;
                                std::cout<<std::endl<<" in record: "<<_eventRecordCount;
                                std::cout<<std::endl<<" with ts: "<<event->timestamp();
                                _printTimes++;
            }*/

        }
        if (!trgTSrangeEvent) _trgOutOfTSRangeEventCount++;

        // this event is empty
        if (emptyEvent) {
            _emptyEventCount++;
        }

        // determine event quality levels 

        // this record is labeled bad
        if (_setBadLevelZero) { // a bad subrecord

            _setBadLevelOne = true;

            if (!_events.empty()) {
                _events.back()->setBadLevelOne(_setBadLevelZero);
            }
            _badSubRecord++;
        }

        // this record is ok
        if (!_setBadLevelZero) { // this subrecord is ok
                                                         // previous subrecord is ok

            // first set previous event
            // there must be an event in the vector
            // and it must not have been set bad previously
            // currently (10282010) all events set as good
            if (!_events.empty()) {
                if (!_events.back()->badEvent()) {
                _setGood = true;
                _events.back()->setGood(_setGood);
                }
            }

            // set last event as good if necessary
            /*if (_eventRecordCount == _NTotalEventSubRecords) {
                event->setGood(_setGood);
            }*/
            
            // set upcoming event
            _setBadLevelOne = false;
        }

        event->setBadLevelZero(_setBadLevelZero);

        event->setStage(_runNumber);

        // set cerenkov values according to Ttm
        unsigned cerenkov = _cerenkov.at(0);
        _cerenkov.erase(_cerenkov.begin());
        event->setCerenkov(cerenkov);
        _events.push_back(event);

        // check number of hits in the event
        if (event->nhitPacks() == 0) _countEmptyEventsPostDec++;

        delete calc;

/*------------------ABOVE------------------------------------
------------ DHCAL ----------- DATA --------------------------
------------------------------------------------------------*/


        break;
        }  // end case

        }; //end switch

} //end void event (RcdRecord &r)


void EbDhcRecordsTrig::writeEvents() {
    if (_events.size() > 7) {
        std::vector<EbDhcEventTrig*>::iterator eit;
        for (eit = _events.begin(); eit != _events.end(); eit++) {
            //std::cout<<std::endl<<"this event has" <<(*eit)->nhitPacks()<<" hit packs";
            if ((*eit)->goodEvent() && !(*eit)->write()) {
                (*eit)->writeAscii(_aout); //internally flags its write history
                 (*eit)->writeAsciiEcal(_aout);
                if ((*eit)->nhitPacks() == 0) _countEmptyEventsPostWrite++;
                //this->writeRoot();
                _goodEventCount++;
            }
        }
        /*for (unsigned i(0);i<_events.size()-5;i++) {
        delete _events[i];
        }*/
        // please dont erase the next to last event

        //for (int i = 0; i < _events.size()-4;i++)
        //delete _events[0];

        for ( std::vector<EbDhcEventTrig*>::iterator it = _events.begin(); it!=_events.end()-5; it++) {
    delete (*it); 
        }

        _events.erase (_events.begin(),_events.end()-5);
            
    }

    if (_eventRecordCount == _NTotalEventSubRecords) { // last record
        std::vector<EbDhcEventTrig*>::iterator eit;
        for (eit = _events.begin(); eit != _events.end(); eit++) {
            //std::cout<<std::endl<<"this event has" <<(*eit)->nhitPacks()<<" hit packs";
            if ((*eit)->goodEvent() && !(*eit)->write() ) {
                (*eit)->writeAscii(_aout); //internally flags its write history
                 (*eit)->writeAsciiEcal(_aout);
                if ((*eit)->nhitPacks() == 0) _countEmptyEventsPostWrite++;
                delete (*eit);
                //this->writeRoot();
                _goodEventCount++;
            }
        }
        // please dont erase the next to last event
    }

} // end writeEvents()

void EbDhcRecordsTrig::eventIP(RcdRecord &r) {

        SubAccessor accessor(r);

        // Check record type
        switch (r.recordType()) {

//start case
        case RcdHeader::event: {

            _NTotalEventSubRecords++;

        std::vector<const DhcLocationData<DhcEventData>*>
    v(accessor.access<DhcLocationData<DhcEventData> >());

        for (unsigned s(0); s<v.size(); s++) { //v size is number of DCOLs
            if ((v[s]->data()->numberOfWords())>0) { // checks if dcol has data
                for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) { 
                    const DhcFeHitData* hits(v[s]->data()->feData(i));
                    if (!hits->trg()) {
                        /*_hCrate[s%13]->Fill((unsigned)v[s]->location().crateNumber());
                        // first just look at hits
                        EbDhcGeom* geom = new EbDhcGeom();
                        _geomcopy = new DhcFeHitData;
                        memmove(_geomcopy,hits,sizeof(DhcFeHitData));
                        geom->init(_geomcopy,(unsigned)v[s]->location().crateNumber() );
                        //_hCrate[s%13]->Fill((unsigned)v[s]->location().crateNumber());
                        if (geom->layerNumber() < 9) {
                        unsigned hitXsize = geom->hitXvec().size();
                        for (unsigned i(0);i<hitXsize;i++) {
                            _hist[geom->layerNumber()]->Fill(geom->hitXvec().at(i));
                        }
                        }

                        delete geom;*/
    
                    }
                }
            }
        }

        break;
        }  // end case
        default: {
            break;
        }
        };//ends switch

}

void EbDhcRecordsTrig::writeRoot() {
 
    //_hHitTS->Write(); 
    _rfile->Write();

}

void EbDhcRecordsTrig::loadTcmtCalibration() {

    char fileopenname[256];
    const char *subdirname = "../../jacobsmith/beam/src/data";
    //char *subdirname = "./src/data";
    for(int xchip = 0 ; xchip < 12; xchip++) {
        for(int xchan = 0 ; xchan < 18 ; xchan++) {
            _getstripfromfechipchan[0][xchip][xchan] = -1;
            _getlayerfromfechipchan[0][xchip][xchan] = -1;
            _getstripfromfechipchan[1][xchip][xchan] = -1;
            _getlayerfromfechipchan[1][xchip][xchan] = -1;
        }
    }
    
    sprintf(fileopenname,"%s/%s",subdirname,"tcmt-channels.dat");
    _chipchannelfile.open(fileopenname);
    std::cout << "after file open: " << fileopenname << std::endl;
    
    char dummy[256]; 
    _chipchannelfile >> dummy;
    _chipchannelfile >> dummy;
    _chipchannelfile >> dummy;
    _chipchannelfile >> dummy;
    int tfe;
 for(int layer = 1 ; layer < 17 ; layer++) { 
     for(int strip = 0 ; strip < 20 ; strip++) {
         _chipchannelfile >> _tlayer;
         _chipchannelfile >> _tstrip;
         _chipchannelfile >> _tchip;
         _chipchannelfile >> _tchannel;
         if(_showdebug == true) std::cout << "tlayer=" << _tlayer;
         if(_showdebug == true) std::cout << " tstrip=" << _tstrip;
         if(_showdebug == true) std::cout << " tchip=" << _tchip;
         if(_showdebug == true) std::cout << " tchannel=" << _tchannel;
         if(_showdebug == true) std::cout << std::endl;
         if(_tlayer < 9) tfe = 0;
         else tfe = 1;
         _getstripfromfechipchan[tfe][_tchip][_tchannel] = _tstrip;
         _getlayerfromfechipchan[tfe][_tchip][_tchannel] = _tlayer;
     }
 }
 _chipchannelfile.close();
 
 //_pedestalfile.open("./src/data/tcmtPeds.dat");
 sprintf(fileopenname,"%s/%s",subdirname,"tcmtPeds.dat");
 _pedestalfile.open(fileopenname);
 std::cout << "after file open: " << fileopenname << std::endl;

    _pedestalfile >> dummy;
    _pedestalfile >> dummy;
    _pedestalfile >> dummy;
    _pedestalfile >> dummy;
    _pedestalfile >> dummy;
    _pedestalfile >> dummy;
    
    // int tfe;
    int tslot;
    double tmean;
    double trms;
     for(int strip = 0 ; strip < 320 ; strip++) {
         _pedestalfile >> tslot;
         _pedestalfile >> _tlayer;
         _pedestalfile >> _tchip;
         _pedestalfile >> _tchannel;
         _pedestalfile >> tmean;
         _pedestalfile >> trms; 
         if(_showdebug == true) {
             std::cout << "tslot=" << tslot;
             std::cout << " tfrontend=" << _tlayer;
             std::cout << " tchip=" << _tchip;
             std::cout << " tchannel=" << _tchannel;
             std::cout << " tmean=" << tmean;
             std::cout << " trms=" << trms;
             std::cout << std::endl;
         }
         if(_tlayer < 6) tfe = 0;
         else tfe = 1;
         _getpedfromfechipchan[tfe][_tchip][_tchannel] = tmean;
     }
     _pedestalfile.close();

     //_pedestalfile.open("./src/data/pedestals.txt");
 //std::cout << "after file open" << std::endl;
 sprintf(fileopenname,"%s/%s",subdirname,"pedestals.txt");
 _pedestalfile.open(fileopenname);
 std::cout << "after file open: " << fileopenname << std::endl;

 int t2layer;
 int t2strip;
 double t2mean;
 double t2rms;
     for(int strip = 0 ; strip < 320 ; strip++) {
         _pedestalfile >> t2layer;
         _pedestalfile >> t2strip;
         _pedestalfile >> t2mean;
         _pedestalfile >> t2rms;
         if(_showdebug == true) {
         std::cout << " tfrontend=" << t2layer;
         std::cout << " tchip=" << t2strip;
         std::cout << " tmean=" << t2mean;
         std::cout << " trms=" << t2rms;
         std::cout << std::endl;
         }
         //if(t2layer < 9) tfe = 0;
         //else tfe = 1;
         _getpedfromlayerstrip[t2layer-1][t2strip] = (int)t2mean;
         _getpedfromlayerstrip[t2layer-1][t2strip] = (int)0;
     }
     _pedestalfile.close();
     for(int ilayer = 0 ; ilayer < 16; ilayer++) { 
        for(int istrip = 0 ; istrip < 20 ; istrip++) {
        }
     }
    
    //_calibrationfile.open("./src/data/tcmtCalibration.dat");
    sprintf(fileopenname,"%s/%s",subdirname,"tcmtCalibration.dat");
    _calibrationfile.open(fileopenname);
    std::cout << "after file open: " << fileopenname << std::endl;
    
    _calibrationfile >> dummy;
    _calibrationfile >> dummy;
    _calibrationfile >> dummy;
    _calibrationfile >> dummy;
 int tmodule;
 double tmip;
     for(int strip = 0 ; strip < 320 ; strip++) {
         _calibrationfile >> tmodule;
         _calibrationfile >> _tchip;
         _calibrationfile >> _tchannel;
         _calibrationfile >> tmip;
         if(_showdebug == true) {
             std::cout << "tmodule=" << tmodule;
             std::cout << " tchip=" << _tchip;
             std::cout << " tchannel=" << _tchannel;
             std::cout << " tmip=" << tmip;
             std::cout << std::endl;
         }
         if(tmodule < 9) tfe = 0;
         else tfe = 1;
         _getmipfromfechipchan[tfe][_tchip][_tchannel] = tmip;
     }
     _calibrationfile.close();


    sprintf(fileopenname,"%s/%s",subdirname,"calibration.txt");
    _calibrationfile.open(fileopenname);
    std::cout << "after file open: " << fileopenname << std::endl;
    
        int tstrip3;
        int tlayer3;
        int tmip3;
        for(int strip = 0 ; strip < 320 ; strip++) {
                _calibrationfile >> tlayer3;
                _calibrationfile >> tstrip3;
                _calibrationfile >> tmip3;
                _getmipfromlayerstrip[tlayer3-1][tstrip3] = tmip3;
                if(_showdebug == false) {
                        std::cout << "tlayer=" << tlayer3;
                        std::cout << " tstrip=" << tstrip3;
                        std::cout << " tmip=" << tmip3;
                        std::cout << " tmip=" << _getmipfromlayerstrip[tlayer3-1][tstrip3];
                        std::cout << std::endl;
                }
        }
        _calibrationfile.close();
        std::cout << "completed loading calibration data" << std::endl;
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcRecordsTrig_HH
