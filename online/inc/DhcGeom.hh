//
// $Id$
//

#ifndef DhcGeom_HH
#define DhcGeom_HH

#include <string>
#include <vector>

#include "TGeoManager.h"
#include "TGeoMatrix.h"


class DhcView {

public:
  DhcView(const std::string& name,
	  const double& theta, const double& phi, const double& psi, const double& zoom) :
    name(name), theta(theta), phi(phi), psi(psi), zoom(zoom)
  {}

public:
  std::string name;
  double theta;
  double phi;
  double psi;
  double zoom;
};


class DhcGeom {

public:
  DhcGeom() {}
  virtual ~DhcGeom();

  virtual TGeoVolume* layout(double&) = 0;
  virtual const std::vector<DhcView*>& views() const;
  virtual void transform(TGeoCombiTrans*);

protected:
  std::vector<DhcView*> _views;
  TGeoCombiTrans* _combiTrans;
};


class DhcCuboidGeom : public DhcGeom {

public:
  DhcCuboidGeom();
  virtual ~DhcCuboidGeom() {}

  TGeoVolume* layout(double&);
  
};

class DhcCubicTCGeom : public DhcGeom {

public:
  DhcCubicTCGeom();
  virtual ~DhcCubicTCGeom() {}

  TGeoVolume* layout(double&);
  
};

class DhcCubicWTCGeom : public DhcGeom {

public:
  DhcCubicWTCGeom();
  virtual ~DhcCubicWTCGeom() {}

  TGeoVolume* layout(double&);
  
};

class DhcCubicGeom : public DhcGeom {

public:
  DhcCubicGeom();
  virtual ~DhcCubicGeom() {}

  TGeoVolume* layout(double&);
  
};

class DhcSliceGeom : public DhcGeom {

public:
  DhcSliceGeom();
  virtual ~DhcSliceGeom() {}

  TGeoVolume* layout(double&);

};


#ifdef CALICE_DAQ_ICC

const std::vector<DhcView*>& DhcGeom::views() const {
  return _views;
}

void DhcGeom::transform(TGeoCombiTrans* t) {
  _combiTrans = t;
}

DhcGeom::~DhcGeom()
{
  _views.clear();
}

DhcCuboidGeom::DhcCuboidGeom()
{
  _views.push_back(new DhcView("Cuboid Top", 90, 90, 180, 1.5));
  _views.push_back(new DhcView("Cuboid Front", 0, 0, -90, 1.5));
  _views.push_back(new DhcView("Cuboid Side", -90, 0, -90, 1.5));
  _views.push_back(new DhcView("Cuboid Perspec1", -45, -45, -45, 2.));
}

TGeoVolume* DhcCuboidGeom::layout(double& bbox)
{
   TGeoManager *geom = new TGeoManager("geom","DHCAL RPC Cuboid geometry");

   TGeoMaterial *vacuum=new TGeoMaterial("vacuum",0,0,0);  
   TGeoMedium *Air=new TGeoMedium("Vacuum",0,vacuum);

   TGeoVolume *top=geom->MakeBox("top",Air,bbox,bbox,bbox);
   geom->SetTopVolume(top);   
   geom->SetTopVisible(0); 

   int NofPadsX = 96;
   int NofPadsY = 96;
   int NofLayers= 50;
   int ShowLayer = 1;

   float padSizeX = 1.;//cm
   float padSizeY = 1.;//cm
   float padThickness = .2;//cm
      
   float layerThickness = 2.54;//cm
   float plateThickness = 1.;//cm
      
   float detlength = NofLayers*layerThickness;
   float detsizeX  = NofPadsX*padSizeX;
   float detsizeY  = NofPadsY*padSizeY;

   float offsetX = 0;
   float offsetY = 0;
   float offsetZ = 0;

   TGeoVolume *layer;
   TGeoVolume *plate;
   TGeoVolume *chamber;
   TGeoVolume *sliceX;
   TGeoVolume *sliceY;

   char nB[100];
   float faceZ = offsetZ;
   for(int k=0; k<NofLayers; ++k) 
     {
       if (k%ShowLayer == 0)
	 {
	   // draw layer
	   float xx = offsetX;
	   float yy = offsetY;
	   float zz = faceZ;

	   sprintf(nB,"L%d",k);
	   layer=geom->MakeBox(nB,Air,1.07*detsizeX/2,1.07*detsizeY/2,layerThickness/2);
	   top->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));

	   float zc = -padThickness/2; 
	   sprintf(nB,"C%d",k);
	   chamber=geom->MakeBox(nB,Air,1.02*detsizeX/2,1.02*detsizeY/2,1.5*padThickness/2);
	   chamber->SetLineColor(30); // green
	   layer->AddNode(chamber,1,new TGeoTranslation(xx,yy,zc));

	   sliceY=chamber->Divide("chamber",2,3,0,0);
	   sliceY->SetLineColor(30); // green
	   //sliceX=sliceY->Divide("board",1,2,0,0);
	   //sliceX->SetLineColor(30); // green
	 }
       faceZ += layerThickness;
     }

   geom->CloseGeometry(); 

   return top;
}

DhcCubicTCGeom::DhcCubicTCGeom()
{
  _views.push_back(new DhcView("Cubic+TC Top", 90, 90, 180, 1.4));
  _views.push_back(new DhcView("Cubic+TC Front", 0, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic+TC Side", -90, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic+TC Perspec1", -45, -45, -45, 1.75));
}

TGeoVolume* DhcCubicTCGeom::layout(double& bbox)
{
   TGeoManager *geom = new TGeoManager("geom","DHCAL RPC Cubic+TC geometry");

   TGeoMaterial *vacuum=new TGeoMaterial("vacuum",0,0,0);  
   TGeoMedium *Air=new TGeoMedium("Vacuum",0,vacuum);

   TGeoVolume *top=geom->MakeBox("top",Air,bbox,bbox,bbox);
   geom->SetTopVolume(top);   
   geom->SetTopVisible(0); 

   int NofPadsX = 96;
   int NofPadsY = 96;
   int NofLayers= 52;
   int ShowLayer = 1;

   float padSizeX = 1.;//cm
   float padSizeY = 1.;//cm
   float padThickness = .2;//cm
      
   float layerThickness = 3.1;//cm
   float plateThickness = 1.;//cm
      
   float detSeparation = 40.;//cm
   float detsizeX  = NofPadsX*padSizeX;
   float detsizeY  = NofPadsY*padSizeY;

   float offsetX = 0;
   float offsetY = 0;
   float offsetZ = 0;

   TGeoVolume *layer;
   TGeoVolume *plate;
   TGeoVolume *chamber;
   TGeoVolume *sliceX;
   TGeoVolume *sliceY;

   TGeoVolume* dhcal=geom->MakeBox("dhcal",Air, bbox, bbox, bbox);
   top->AddNode(dhcal, 1, _combiTrans);

   TGeoVolume* tcmt=geom->MakeBox("tcmt",Air, bbox, bbox, bbox);
   top->AddNode(tcmt, 1, new TGeoTranslation(0, 0, 38*layerThickness+detSeparation));

   char nB[100];
   float faceZ = offsetZ;
   for(int k=0; k<NofLayers; ++k) 
     {
       // detector separation displacement,
       if (k == 38 )
	 {
	   faceZ = offsetZ - layerThickness;
	   layerThickness += 1.9;
	 }

       if (k == 47 )
	 {
	   faceZ += layerThickness + 3.;
	   layerThickness += 8;
	 }

       if (k%ShowLayer == 0)
	 {
	   // draw layer
	   float xx = offsetX;
	   float yy = offsetY;
	   float zz = faceZ;

	   sprintf(nB,"L%d",k);
	   layer=geom->MakeBox(nB,Air,1.07*detsizeX/2,1.07*detsizeY/2,layerThickness/2);
	   if (k < 38)
	     dhcal->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));
	   else
	     tcmt->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));

	   float zc = -padThickness/2; 
	   sprintf(nB,"C%d",k);
	   chamber=geom->MakeBox(nB,Air,1.02*detsizeX/2,1.02*detsizeY/2,1.5*padThickness/2);
	   chamber->SetLineColor(30); // green
	   layer->AddNode(chamber,1,new TGeoTranslation(xx,yy,zc));

	   sliceY=chamber->Divide("chamber",2,3,0,0);
	   sliceY->SetLineColor(30); // green
	   //sliceX=sliceY->Divide("board",1,2,0,0);
	   //sliceX->SetLineColor(30); // green
	 }
       faceZ += layerThickness;
     }

   geom->CloseGeometry(); 

   return top;
}

DhcCubicWTCGeom::DhcCubicWTCGeom()
{
  _views.push_back(new DhcView("Cubic+TC Top", 90, 90, 180, 1.4));
  _views.push_back(new DhcView("Cubic+TC Front", 0, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic+TC Side", -90, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic+TC Perspec1", -45, -45, -45, 1.75));
}

TGeoVolume* DhcCubicWTCGeom::layout(double& bbox)
{
   TGeoManager *geom = new TGeoManager("geom","DHCAL RPC Cubic+TC geometry");

   TGeoMaterial *vacuum=new TGeoMaterial("vacuum",0,0,0);  
   TGeoMedium *Air=new TGeoMedium("Vacuum",0,vacuum);

   TGeoVolume *top=geom->MakeBox("top",Air,bbox,bbox,bbox);
   geom->SetTopVolume(top);   
   geom->SetTopVisible(0); 

   int NofPadsX = 96;
   int NofPadsY = 96;
   int NofLayers= 54;
   int ShowLayer = 1;

   float padSizeX = 1.;//cm
   float padSizeY = 1.;//cm
   float padThickness = .2;//cm
      
   float layerThickness = 2.7;//cm
   float plateThickness = 1.;//cm
      
   float detSeparation = 23.5;//cm
   float detsizeX  = NofPadsX*padSizeX;
   float detsizeY  = NofPadsY*padSizeY;

   float offsetX = 0;
   float offsetY = 0;
   float offsetZ = 0;

   TGeoVolume *layer;
   TGeoVolume *plate;
   TGeoVolume *chamber;
   TGeoVolume *sliceX;
   TGeoVolume *sliceY;

   TGeoVolume* dhcal=geom->MakeBox("dhcal",Air, bbox, bbox, bbox);
   top->AddNode(dhcal, 1, _combiTrans);

   TGeoVolume* tcmt=geom->MakeBox("tcmt",Air, bbox, bbox, bbox);
   top->AddNode(tcmt, 1, new TGeoTranslation(0, 0, 39*layerThickness+detSeparation));

   char nB[100];
   float faceZ = offsetZ;
   for(int k=0; k<NofLayers; ++k) 
     {
       // detector separation displacement,
       if (k == 39 )
	 {
	   faceZ = offsetZ - layerThickness;
	   layerThickness = 5.3;
	 }

       if (k == 47 )
	 {
	   faceZ += 7.7;
	   layerThickness = 13;
	 }

       if (k%ShowLayer == 0)
	 {
	   // draw layer
	   float xx = offsetX;
	   float yy = offsetY;
	   float zz = faceZ;

	   sprintf(nB,"L%d",k);
	   layer=geom->MakeBox(nB,Air,1.07*detsizeX/2,1.07*detsizeY/2,layerThickness/2);
	   if (k < 39)
	     dhcal->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));
	   else
	     tcmt->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));

	   float zc = -padThickness/2; 
	   sprintf(nB,"C%d",k);
	   chamber=geom->MakeBox(nB,Air,1.02*detsizeX/2,1.02*detsizeY/2,1.5*padThickness/2);
	   chamber->SetLineColor(30); // green
	   layer->AddNode(chamber,1,new TGeoTranslation(xx,yy,zc));

	   sliceY=chamber->Divide("chamber",2,3,0,0);
	   sliceY->SetLineColor(30); // green
	   //sliceX=sliceY->Divide("board",1,2,0,0);
	   //sliceX->SetLineColor(30); // green
	 }
       faceZ += layerThickness;
     }

   geom->CloseGeometry(); 

   return top;
}

DhcCubicGeom::DhcCubicGeom()
{
  _views.push_back(new DhcView("Cubic Top", 90, 90, 180, 1.4));
  _views.push_back(new DhcView("Cubic Front", 0, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic Side", -90, 0, -90, 1.4));
  _views.push_back(new DhcView("Cubic Perspec1", -30, 0, -90, 1.2));
  _views.push_back(new DhcView("Cubic Perspec2", 30,  0, -90, 1.2));
}

TGeoVolume* DhcCubicGeom::layout(double& bbox)
{
   TGeoManager *geom = new TGeoManager("geom","DHCAL RPC Cubic geometry");

   TGeoMaterial *vacuum=new TGeoMaterial("vacuum",0,0,0);  
   TGeoMedium *Air=new TGeoMedium("Vacuum",0,vacuum);

   TGeoVolume *top=geom->MakeBox("top",Air,bbox,bbox,bbox);
   geom->SetTopVolume(top);   
   geom->SetTopVisible(0); 

   int NofPadsX = 96;
   int NofPadsY = 96;
   int NofLayers= 38;
   int ShowLayer = 1;

   float padSizeX = 1.;//cm
   float padSizeY = 1.;//cm
   float padThickness = .2;//cm
      
   float layerThickness = 3.1;//cm
   float plateThickness = 1.;//cm
      
   float detlength = NofLayers*layerThickness;
   float detsizeX  = NofPadsX*padSizeX;
   float detsizeY  = NofPadsY*padSizeY;

   float offsetX = 0;
   float offsetY = 0;
   float offsetZ = 0;

   TGeoVolume *layer;
   TGeoVolume *plate;
   TGeoVolume *chamber;
   TGeoVolume *sliceX;
   TGeoVolume *sliceY;

   char nB[100];
   float faceZ = offsetZ;
   for(int k=0; k<NofLayers; ++k) 
     {
       if (k%ShowLayer == 0)
	 {
	   // draw layer
	   float xx = offsetX;
	   float yy = offsetY;
	   float zz = faceZ;

	   sprintf(nB,"L%d",k);
	   layer=geom->MakeBox(nB,Air,1.07*detsizeX/2,1.07*detsizeY/2,layerThickness/2);
	   top->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));

	   float zc = -padThickness/2; 
	   sprintf(nB,"C%d",k);
	   chamber=geom->MakeBox(nB,Air,1.02*detsizeX/2,1.02*detsizeY/2,1.5*padThickness/2);
	   chamber->SetLineColor(30); // green
	   layer->AddNode(chamber,1,new TGeoTranslation(xx,yy,zc));

	   sliceY=chamber->Divide("chamber",2,3,0,0);
	   sliceY->SetLineColor(30); // green
	   //sliceX=sliceY->Divide("board",1,2,0,0);
	   //sliceX->SetLineColor(30); // green
	 }
       faceZ += layerThickness;
     }

   geom->CloseGeometry(); 

   return top;
}

DhcSliceGeom::DhcSliceGeom()
{
  _views.push_back(new DhcView("Slice Top", 90, -180, 0, 1.4));
  _views.push_back(new DhcView("Slice Front", 90, -90, 0, 1.4));
  _views.push_back(new DhcView("Slice Side", 0, 0, -90, 1.4));
  _views.push_back(new DhcView("Slice Perspec1", 60, -120, 0, 1.2));
  _views.push_back(new DhcView("Slice Perspec2", 60, -240, 0, 1.2));
}

TGeoVolume* DhcSliceGeom::layout(double& bbox)
{
   TGeoManager *geom = new TGeoManager("geom","DHCAL RPC Slice geometry");

   TGeoMaterial *vacuum=new TGeoMaterial("vacuum",0,0,0);  
   TGeoMedium *Air=new TGeoMedium("Vacuum",0,vacuum);

   TGeoVolume *top=geom->MakeBox("top",Air,bbox,bbox,bbox);
   geom->SetTopVolume(top);   
   geom->SetTopVisible(0); 

   int NofPadsX = 96;
   int NofPadsY = 32;
   int NofLayers= 12;
      
   float padSizeX = 1.;//cm
   float padSizeY = 1.;//cm
   float padThickness = .2;//cm
      
   float layerThickness = 10.;//cm
      
   float detlength = NofLayers*layerThickness;
   float detsizeX  = NofPadsX*padSizeX;
   float detsizeY  = NofPadsY*padSizeY;

   float offsetX = 0;
   float offsetY = 0;
   float offsetZ = 0;

   TGeoVolume *layer;
   TGeoVolume *chamber;
   TGeoVolume *sliceX;

   char nB[100];
   float faceZ = offsetZ;
   for(int k=0; k<NofLayers; ++k) 
     {
       // draw layer
       float xx = offsetX;
       float yy = offsetY;
       float zz = faceZ;

       sprintf(nB,"L%d",k);
       layer=geom->MakeBox(nB,Air,1.07*detsizeX/2,1.07*detsizeY/2,layerThickness/2);
       top->AddNode(layer,1,new TGeoTranslation(xx,yy,zz));

       float zc = -padThickness/2; 
       sprintf(nB,"C%d",k);
       chamber=geom->MakeBox(nB,Air,1.02*detsizeX/2,1.02*detsizeY/2,1.5*padThickness/2);
       chamber->SetLineColor(30); // green
       layer->AddNode(chamber,1,new TGeoTranslation(xx,yy,zc));

       //sliceX=chamber->Divide("board",1,2,0,0);
       //sliceX->SetLineColor(30); // gray

       faceZ += layerThickness;
     }

   geom->CloseGeometry(); 

   return top;
}

#endif // CALICE_DAQ_ICC
#endif // DhcGeom_HH
