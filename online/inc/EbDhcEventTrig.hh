//
// $Id: EbDhcEventTrig.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcEventTrig_HH
#define EbDhcEventTrig_HH

#include <string>
#include <iostream>
#include <utility>
#include <vector>
#include <cmath>

#include "EbDhcDTCalculator.hh"
#include "DhcFeHitData.hh"

// ecal classes
#include "EcalEnergy.hh"

// tcmt classes
#include "EbDhcTcmtPack.hh"

#include "TH1D.h"
#include "TFile.h"

class EbDhcEventTrig {

public:

    EbDhcEventTrig();
    ~EbDhcEventTrig(){};

    void addHitPack(DhcFeHitData* hitP);//y
    void addTrgPack(DhcFeHitData* trgP);//y
    void addTcmtPack(double te, int tl, int ts);
    void addEcalEnergy(EcalEnergy ecalEnergy);
    void setEcalThreshold(double &emt);
    void writeAscii(std::ostringstream &aout);
    void writeAsciiEcal(std::ostringstream &aout);
    bool write();

    unsigned layerNumber(DhcFeHitData* &data);
    unsigned posX(unsigned vmead, unsigned tempX);
    unsigned posY(unsigned dconad, unsigned posX, unsigned tempY);
    void trgType(unsigned tt);
    void tcmtConfig(int tc);

    void setBadLevelZero(bool &badlevelzero);
    void setBadLevelOne(bool &badlevelone);
    void setGood(bool &good);

    bool goodEvent();
    bool badEvent();

    std::vector<DhcFeHitData*>& hitPacks(); // y
    std::vector<DhcFeHitData*>& trgPacks(); // y
    unsigned nhitPacks();
    unsigned ntrgPacks();

    unsigned timestamp();
    void timestamp(unsigned timestamp);

    void setStage(unsigned &runNumber);
    void setCerenkov(unsigned &cerenkov);


private:

    bool _isTertiaryBeam;
    bool _badLevelZero;
    bool _badLevelOne;
    bool _good;
    bool _write;
    unsigned _timestamp;
    unsigned _trgType; // cerenkov from tcmt
    int _tcmtConfig;

    unsigned _cerenkov; // cerenkov from Ttm

    // ecal data
    EcalEnergy _ecalEnergy;
    double _ecalThreshold;

    std::vector<DhcFeHitData*> _hitPacks;
    std::vector<DhcFeHitData*> _trgPacks;
    std::vector<EbDhcTcmtPack*> _tcmtPack;

    std::ofstream _afile;

};  // end of class

#ifdef CALICE_DAQ_ICC


EbDhcEventTrig::EbDhcEventTrig() {

    _badLevelZero = false;
    _badLevelOne = false;
    _good = false;
    _write = false;
    _isTertiaryBeam = false;

}

bool EbDhcEventTrig::write() {
    return _write;
}

void EbDhcEventTrig::timestamp(unsigned timestamp) {
    _timestamp = timestamp;
}

unsigned EbDhcEventTrig::timestamp() {
    return _timestamp;
}

void EbDhcEventTrig::setBadLevelZero(bool &badlevelzero) {
    _badLevelZero = badlevelzero;
}

void EbDhcEventTrig::setBadLevelOne(bool &badlevelone) {
    _badLevelOne = badlevelone;
}

void EbDhcEventTrig::setGood(bool &good) {
    _good = good;
}

void EbDhcEventTrig::setStage(unsigned &runNumber) {

    if (runNumber > 650221) _isTertiaryBeam = true;


}

void EbDhcEventTrig::setCerenkov(unsigned &cerenkov) {

    _cerenkov = cerenkov;


}

bool EbDhcEventTrig::goodEvent() {
    return _good;
}

bool EbDhcEventTrig::badEvent() {

    bool bad = false;
    if (_badLevelZero) bad = true;
    else if (_badLevelOne) bad = true;
    else bad = false;

    return bad;
}

/*
** for dhcal runs
** oct2010, jan2011, apr2011, jun2011
*/
unsigned EbDhcEventTrig::layerNumber(DhcFeHitData* &data) {
// WARNING
// WARNING
// check EbDhcRecordsTrig.hh that no addresses are labeled corrupt
// WARNING
// WARNING
    unsigned dcolad = data->dcolad();
    unsigned dconad = data->dconad();
    unsigned layer = 100;

    // cubic meter
    // dcolad 0-9 == slot 4-13
    if (dcolad < 10) {
    layer = 37-((dcolad*4) + (dconad/3));
        if (data->vmead()==0) { // for tcmt rpcs using m^3 dcols
            if (dcolad == 9) {
                if (dconad == 8) layer = 40;
                if (dconad > 8) layer = 41;
            }
        }
    }
    else { // rpc-tcmt starts in dcolad=10
        //layer = 38 + (dconad/3);
        layer = 38 + ((dcolad-10)*4) + (dconad/3);
    }

    if (_isTertiaryBeam) layer = (dcolad*4) + (dconad/3);

    return layer;

}


/*
** for tertiary runs nov2011
*/
/*
unsigned EbDhcEventTrig::layerNumber(DhcFeHitData* &data) {
// WARNING
// WARNING
// check EbDhcRecordsTrig.hh that no addresses are labeled corrupt
// WARNING
// WARNING
    unsigned dcolad = data->dcolad();
    unsigned dconad = data->dconad();
    unsigned layer = 100;

    layer = (dcolad*4) + (dconad/3);

    return layer;

}
*/

unsigned EbDhcEventTrig::posX(unsigned vmead, unsigned tempX) {

    // size of left is number of dcols with left sided FEBs
    unsigned x=0;

    switch (vmead) {
        case 0: { // left side crate 220
            x = (95-tempX);
            break;
        }
        case 1: { // right side crate 221
            x = tempX;
            break;
        }
        default: {break;}
    };

    return x;
}

unsigned EbDhcEventTrig::posY(unsigned dconad, unsigned posX, unsigned tempY) {
    unsigned y = 0;
    unsigned top[4] = {0,3,6,9};
    unsigned middle[4] = {1,4,7,10};
    unsigned bottom[4] = {2,5,8,11};
    for (unsigned i = 0; i < 4; i++) {
        if (dconad == top[i]) {
            // right top
            if (posX < 48) y = (95 - tempY);
            // left top
            else y = (tempY + 64);
         break;
        }
        if (dconad == middle[i]) {
            // right middle
            if (posX < 48) y = (63 - tempY);
            // left middle
            else y = (tempY + 32);
         break;
        }
        if (dconad == bottom[i]) {
            // right bottom
            if (posX < 48) y = (31 - tempY);
            // left bottom
            else y = (tempY);
         break;
        }
    }
    return y;
}

void EbDhcEventTrig::trgType(unsigned tt) {
    _trgType = tt;
/*

Sc(12)  Outer(8)    Inner(7)    dec
0   0       0       0
0   0       1       1
0   1       0       2
0   1       1       3
1   0       0       4
1   0       1       5
1   1       0       6
1   1       1       7

*/
}

void EbDhcEventTrig::tcmtConfig(int tc) {
    _tcmtConfig = tc;
}

void EbDhcEventTrig::writeAscii(std::ostringstream &aout) {

    int cerenkovValue = -1;
    cerenkovValue = _trgType+(unsigned)_tcmtConfig;

    if (_isTertiaryBeam) {
        //cerenkovValue = -1; // used before cerenkov was working for tertiary stage
        cerenkovValue = _cerenkov;
        _tcmtConfig = 0;
    }

    if (_tcmtConfig == 0) {
//void EbDhcEventTrig::writeAscii(std::ofstream &aout) {

    _afile.open(aout.str().c_str(),ofstream::app);

    // produces seg fault from empty events
    //EbDhcDTCalculator* calc = new EbDhcDTCalculator();
    //std::cout<<std::endl<<"delta ts between event ts and hit ts: "
    //         << fabs(calc->dt( _timestamp , (*(_hitPacks.begin()))->timestamp() ));
    //delete calc;


    //std::cout<<std::endl<<" start writing an event";
        const unsigned MAX_CHAN = 64;
        unsigned g[8][8] = {
                        {44,40,36,32,31,27,23,19},
                        {45,41,37,33,30,26,22,18},
                        {46,42,38,34,29,25,21,17},
                        {47,43,39,35,28,24,20,16},
                        {48,52,56,60,3,7,11,15},
                        {49,53,57,61,2,6,10,14},
                        {50,54,58,62,1,5,9,13},
                        {51,55,59,63,0,4,8,12},
                        };
            unsigned posY, posX, posZ;
            unsigned tempY, tempX;
            unsigned dcalX, dcalY;

    std::vector<DhcFeHitData*>::iterator datait;

        //std::cout<<std::endl<<"event ts "<<_timestamp;
        _afile<<std::dec<< _timestamp <<" -1 "<< cerenkovValue <<" -1 "<<std::endl;
        //_afile<<std::dec<< _timestamp <<" -1 "<< _trgType+(unsigned)_tcmtConfig <<" -1 "<<std::endl;

    for (datait = _hitPacks.begin(); datait != _hitPacks.end(); datait++) {

            dcalX = ((*datait)->dcalad())%6;
            dcalY = ((*datait)->dcalad())/6;
            posZ = this->layerNumber((*datait));

        //if (posZ > 41) std::cout<<std::endl<<"layer: "<<posZ;
            UtlPack hitsHi((*datait)->hitsHi());
            UtlPack hitsLo((*datait)->hitsLo());

            for (unsigned c(0); c<MAX_CHAN; c++) {
                    double hit = c<32 ?
                        (hitsLo.bit(c%32) ? 1.0 : 0.0) :
                        (hitsHi.bit(c%32) ? 1.0 : 0.0);
                    if (hit > 0) { // now find its position
                    for (unsigned x = 0; x < 8; x++) {
                         for (unsigned y = 0; y < 8; y++) {
                                if (c == g[x][y]) { // found a hit!
                                     tempX = dcalX*8 + x; // position from IC address 01
                                     tempY = dcalY*8 + y; // position from IC address 01
                                     // determines x,y for cubic meter
                                     // currently, as of 9-17-2010, for ANL temp cubic meter
                                     // update for test beam wiring scheme
                                     // for test beam posX needs crate# (ie vmead)
                                     //posX = this->posX((*datait)->dcolad(),tempX);
                                                 // cassete test stand way
                                     posX = this->posX((*datait)->vmead(),tempX);
                                     posY = this->posY((*datait)->dconad(),posX,tempY);
                                     // end of cubic meter configuration

                                         /*if (posZ == 51) {
                                     if (_isTertiaryBeam) {
                                         // Michigan RPC in layer 50
                                             //posX+=13;
                                             //posY+=32;// put feb in dcol channel 11, ie. its seen as a bottom board
                                             posZ = posZ -1;
                                         }
                                     }*/

                                     _afile<<std::dec<<(*datait)->timestamp()<<" "
                                         <<posX<<" "<<posY<<" "<<posZ<<std::endl;
                                }
                         }
                    } //end for x,y
                    } // end hit cond
            } // end channels(64) loop (i.e. found x,y)
        //delete hits in vector
    } // end loop through hits
    //std::cout<<std::endl<<" finished writing an event";

    // Write any tcmt(scint) data
    std::vector<EbDhcTcmtPack*>::iterator tpit;
    for (tpit = _tcmtPack.begin();tpit != _tcmtPack.end(); tpit++) {
        // insert threshold condition for mip value
        _afile<<std::dec<<(*tpit)->energy()<<" "<<(*tpit)->xstrip()<<" "
                    <<(*tpit)->ystrip()<<" "<<(*tpit)->layer()<<std::endl;
    }

    // mark this event as written and not good anymore
    // i.e. dont re-write it
    // AND its ok to erase
    _write = true;
    _good = false;

    _afile.close();

    }
} // end writeAscii()

void EbDhcEventTrig::writeAsciiEcal(std::ostringstream &aout) {
    if (_tcmtConfig == 0) {

        _afile.open(aout.str().c_str(),ofstream::app);
        // loop through pads
        for(EcalPhysicalPad pp(0);pp.isValid();pp++){
            if(_ecalEnergy.energy(pp) > _ecalThreshold) { // now ok to write
                _afile<<std::dec<<std::setprecision(3)
                            <<-1*_ecalEnergy.energy(pp)<<" "
                            <<pp.horizontal()<<" "
                            <<pp.vertical()<<" "
                            <<pp.layer()<<std::endl;
            }
        }
        _afile.close();

    }

} // end writeAsciiEcal()

void EbDhcEventTrig::addHitPack(DhcFeHitData* hitP) {
        _hitPacks.push_back(hitP);
}

void EbDhcEventTrig::addTrgPack(DhcFeHitData* trgP) {
        _trgPacks.push_back(trgP);
}

void EbDhcEventTrig::addTcmtPack(double te, int tl, int ts) {
    EbDhcTcmtPack* tp = new EbDhcTcmtPack(tl);
    tp->energy(te);
    //tp->layer(tl);
    tp->strip(ts);
    _tcmtPack.push_back(tp);

}

void EbDhcEventTrig::addEcalEnergy(EcalEnergy ecalEnergy) {
    _ecalEnergy = ecalEnergy;
}

void EbDhcEventTrig::setEcalThreshold(double &emt) {
    _ecalThreshold = emt;
}

std::vector<DhcFeHitData*>& EbDhcEventTrig::hitPacks() {
    return _hitPacks;
}

std::vector<DhcFeHitData*>& EbDhcEventTrig::trgPacks() {
    return _trgPacks;
}

unsigned EbDhcEventTrig::nhitPacks() {
    return _hitPacks.size();
}

unsigned EbDhcEventTrig::ntrgPacks() {
    return _trgPacks.size();
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcEventTrig_HH
