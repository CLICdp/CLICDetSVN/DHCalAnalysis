//
// $Id: EbDhcRecords.hh,v 1.2 2009/19/09 14:55:00 jacobsmith Exp $
//

#ifndef EbDhcRecords_HH
#define EbDhcRecords_HH

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <math.h>
#include <fstream.h>

#include "TROOT.h"
#include "TFile.h"
#include "TStyle.h"
#include "TApplication.h"
#include "TCanvas.h"
#include "TH1.h"

#include "RcdRecord.hh"
#include "DaqEvent.hh"
#include "SubAccessor.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"
#include "DhcReadoutConfigurationData.hh"

#include "EbDhcBuild.hh"
#include "UtlLocation.hh"

#define NHIST 8

class EbDhcRecords {

public:
  //EbDhcRecords(const unsigned &t) 
  EbDhcRecords() 
  {
   //_tsThreshold = t;
   //_tsThreshold = 30;
   _countBuildCycles = 0;
   _eventRecordCount = 0;
   _packsUseCount = 0;
   _NEventRecords = 0;
   _parNrecords = 10; //max limit of number of aquisitions an event can span

   _mergeFlag = false;
   _lastRecord = false;

   _ndcols = 0;
   _ndpacksIP = 0;
   _ndpacksOK = 0;
   _ndpacksOKadded = 0;
   _ndpacks = 0;

  //data to histogram number of packs in each record
    //_canvas = new TCanvas("EventBuilder", "Event Builder",
      //                         40,40,780,800);
    //gPad->SetGrid();
    //gStyle->SetOptStat(0);
    //gStyle->SetPalette(1);
    //gStyle->SetCanvasColor(33);
    //gStyle->SetFrameFillColor(18);

    //gStyle->SetOptFit(1111);
      // initialize hTs
      std::stringstream tempName, tempTitle;
      for (unsigned i(0);i<2;i++) {
        _hTs[i] = new TH1F();
        tempName.str("");
        tempName << "hTs" << std::setw(1) << i;
        tempTitle.str("");
        if (i == 0) tempTitle << "init ts dist" << std::setw(1) << i;
        if (i == 1) tempTitle << "merged ts dist" << std::setw(1) << i;
        _hTs[i]->SetNameTitle(tempName.str().c_str(),tempTitle.str().c_str());
        _hTs[i]->SetBins(10000,0.,10000000.);
      }

  }

  virtual ~EbDhcRecords() {
  }

  void update() {
  //_canvas->cd();
  //_hNpacks.Draw();
  //_canvas->Update();
  }

  void openWriter(const unsigned &runNumber, bool &runType) {
    std::cout<<std::endl << "opened a file to write";
    //_file << "output/";
    //if (runType)  _file << "run" << runNumber << "."<<_tsThreshold<<".cosmic.txt";
    //else  _file << "run" << runNumber << "."<<_tsThreshold<<".noise.txt";
    if (runType)  _file << "run" << runNumber << ".cosmic.txt";
    else  _file << "run" << runNumber << ".noise.txt";
    std::cout<<std::endl << "runNumber: " << _file.str().c_str() <<std::endl;
    //_aout.open(_file.str().c_str());

    //_rout << "run" << runNumber << "." <<_tsThreshold<<".root";
    _rout << "run" << runNumber << ".root";
    std::cout<<std::endl << "root file name: " << _rout.str().c_str() <<std::endl;
     std::cout<<std::endl;

    _rfile = new TFile(_rout.str().c_str(), "recreate");
    
  }

  void closeWriter() {
    //_aout.close();
    std::cout<<std::endl << "closed a file to write";
    std::cout<<std::endl << "runNumber: " << _file.str().c_str() <<std::endl;

    unsigned percent = (unsigned)(100*((double)_eventRecordCount)/((double)_NEventRecords));
    std::cout<<std::endl<<"percentage records considered: "<<percent;
    std::cout<<std::endl<<"record "<<_eventRecordCount<<" of "<<_NEventRecords;
    std::cout<<std::endl<<"ndpacks considered: "<<_ndpacks;
    std::cout<<std::endl<<"percentage considered: "<<100*((double)_ndpacks/(double)_ndpacksIP);
    std::cout<<std::endl<<"ndpacks verified (not containing chksum error): "<<_ndpacksOK;
    std::cout<<std::endl<<"percentage verified: "<<100*((double)_ndpacksOK/(double)_ndpacksIP);
    std::cout<<std::endl<<"ndpacks verfied and added: "<<_ndpacksOKadded;
    std::cout<<std::endl<<"percentage verified and added: "<<100*((double)_ndpacksOKadded/(double)_ndpacksOK);
    std::cout<<std::endl<<"ndpacks in initialization: "<<_ndpacksIP;
    std::cout<<std::endl;

    std::vector<EbDhcBuild*>::iterator bit;
    //for (bit = _build.begin();bit != _build.end(); bit++) {
    unsigned npacksTot = 0;
    for (unsigned i(0);i<_build.size();i++) {
      npacksTot += _build.at(i)->nPacks();
      std::cout<<std::endl<<"npacks in dcol "<<i<<", "<<_build.at(i)->nPacks();
    }
    std::cout<<std::endl<<"tot npacks remaining in all dcols "<<npacksTot;

    for (unsigned i(0);i<2;i++) {
      _hTs[i]->Write();
    }
    for (unsigned i(0);i<2;i++) {
      delete _hTs[i];
    }
    delete _rfile;

  }

  void eventIP(RcdRecord &r) {
    SubAccessor accessor(r);
   
    // Check record type
    switch (r.recordType()) {

//start case
    case RcdHeader::configurationStart: {
     if (_NEventRecords == 0) {
      std::vector<const DhcReadoutConfigurationData*>
        vcd(accessor.access<DhcReadoutConfigurationData>());

      // determine number of active dcols
      for(unsigned i(0);i<vcd.size();i++) { // one for each crate
        for(unsigned s(2);s<=21;s++) { // one for each dcol in a crate
          if (vcd[i]->slotEnable(s)){
            _ndcols ++;
          } 
        } // end slot (dcol)loop
      } // end vcd loop
      std::cout<<std::endl<<"configStartIP found "<<_ndcols<<" dcols";

      // initialize hNpacks
      std::stringstream tempName, tempTitle;
      for (unsigned i(0);i<_ndcols;i++) {
        _hNpacks[i] = new TH1F();
        tempName.str("");
        tempName << "hNPacksDCOL" << std::setw(1) << i;
        tempTitle.str("");
        tempTitle << "NPacksDCOL" << std::setw(1) << i;
        _hNpacks[i]->SetNameTitle(tempName.str().c_str(),tempTitle.str().c_str());
        _hNpacks[i]->SetBins(10000,0.,10000.);

        EbDhcBuild* build = new EbDhcBuild();
        _build.push_back(build);
      }
     }
    break;
    }

    case RcdHeader::event: {
      _NEventRecords++;
    const std::vector<const DhcLocationData<DhcEventData>*>
	v(accessor.access<DhcLocationData<DhcEventData> >());
    _Ndcols = v.size();
    if (_Ndcols != _ndcols) std::cout<<std::endl<<"CANCEL NOW, NDOLS MISMATCH";
    if (_Ndcols != _ndcols) std::cout<<std::endl<<"CANCEL NOW, NDOLS MISMATCH";
    //_recordPacks = new unsigned[_Ndcols];

    for (unsigned s(0); s<v.size(); s++) { //v size is number of DCOLs
      //EbDhcBuild* build = new EbDhcBuild();
      //_build.push_back(build);
      /*_hNpacks[s] = new TH1F();
      tempName.str("");
      tempName << "hNPacksDCOL" << std::setw(1) << s;
      tempTitle.str("");
      tempTitle << "NPacksDCOL" << std::setw(1) << s;
      _hNpacks[s]->SetNameTitle(tempName.str().c_str(),tempTitle.str().c_str());
      _hNpacks[s]->SetBins(2000,0,2000);*/
      if ((v[s]->data()->numberOfWords())>0) { // checks if dcol has data
      //_recordPacks[s] += v[s]->data()->numberOfWords()/4;
        _hNpacks[s]->Fill(v[s]->data()->numberOfWords()/4);
        _ndpacksIP+=v[s]->data()->numberOfWords()/4;
        _build.at(s)->nPacksTot(_ndpacksIP);
      }
      //_hNpacks[s]->Fill(_recordPacks[s]); 
    }
    break;
    }  // end case
    default: {
      break;
    }
    };//ends switch
  } //ends eventIP()

  void event(RcdRecord &r, bool &runType) {
    SubAccessor accessor(r);
   
    // Check record type
    switch (r.recordType()) {

//start case
// only applies to each instance of FeHitData event
    case RcdHeader::event: {

    _eventRecordCount++;
   
    if (_NEventRecords == _eventRecordCount) _lastRecord = true;

      const std::vector<const DhcLocationData<DhcEventData>*>
	v(accessor.access<DhcLocationData<DhcEventData> >());
 
    for (unsigned s(0); s<v.size(); s++) { //v size is number of DCOLs
// is v.size the number of unique locations?? i.e. Ndcols*Ncrates?
     //_bBuffer[s]->setCrateNumber((unsigned)v[s]->location().crateNumber());

      if ((v[s]->data()->numberOfWords())>0) { // checks if dcol has data
        for (unsigned i(0); i<v[s]->data()->numberOfWords()/4; i++) { 
          _ndpacks++;
          if (v[s]->data()->feData(i)->verify()) {
          //if (_Npacks == _parPackLimit) {
          //  _build->order();
          //  _build->build(_aout, _maxNpacks, runType); // auto write
          //  _Npacks = _build->nPacks(); // to reset number of packs remaining in build
            
          //}
          //else {
            // copy fehitdata and add to buffer 
            copy = new DhcFeHitData;
            assert(copy!=0);
            const DhcFeHitData* hits(v[s]->data()->feData(i));
            memmove(copy,hits,sizeof(DhcFeHitData));

            _build.at(s)->addPack( copy, (unsigned)v[s]->location().crateNumber());
            _ndpacksOK++;
            _hTs[0]->Fill(copy->timestamp());
            
          //}
          }
        } // finished going through data packages
        
      }
    } // finished going through dcols

    // check that all builders have passed their threshold
    /*for (unsigned s(0); s<_Ndcols; s++) {
      if (_Npacks[s] >= _parPackLimit[s]) {
        threshCount += 1;
      }
    }*/
    // count to flag dcols ready for merging
    unsigned threshCount = 0;
    std::vector<EbDhcBuild*>::iterator bit;
    for (bit = _build.begin();bit != _build.end(); bit++) {
      // if all packs pass threshold
      if ((*bit)->full()) threshCount += 1;
      //if ((*bit)->nearEOF() && _eventRecordCount==_NEventRecords) _mergeFlag = true;

      /*
      if ((*bit)->full() ||
          (*bit)->nearEOF())//CHANGE THIS SO THAT ONLY ONE
      */
      // or if npacks remaining is less than threshold (near end)
      // i.e. near the end of file
    }

    // reset count so that a builder isnt counted twice as having passed its threshold
    if (threshCount != _Ndcols) {
      /*if (_eventRecordCount > 300 && _eventRecordCount < 350) {
        std::cout<<std::endl<<"threshCount: "<<threshCount;
      }*/
      threshCount = 0;
    }
  
    // all dcols reached threshold
    if (threshCount == _Ndcols)  _mergeFlag = true;

    if (_lastRecord) _mergeFlag = true;

    // time order each builder only if all have passed their threshold
    // finished time ordering all builders
    if (_mergeFlag) {
      std::vector<EbDhcBuild*>::iterator bit;
      for (bit = _build.begin();bit != _build.end(); bit++) {
        (*bit)->order();
      }
    }

    // Merge builders' packs
    EbDhcEvent* evt = new EbDhcEvent();
    EbDhcDTCalculator* calc = new EbDhcDTCalculator();

    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 1";

    while (_mergeFlag) { // ready to merge builders' packs into events
      std::vector<EbDhcBuild*>::iterator bit;
      bool packSearchFlag = true;
      bool evtAddFlag = false;
      bool minTSinit = false;
      unsigned minTSvalue = 0;
      unsigned minTSbuilder = 0;
      unsigned cnt = 0;
    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 2";

      cnt = 0;
      // initialize minTSvalue, data must exist
      for (bit = _build.begin();bit != _build.end(); bit++) {
       if ((*bit)->nPacks() > 0) {
         minTSvalue = ((*bit)->timestamp(0));
         minTSbuilder = cnt;
         minTSinit = true;
       }
       if (minTSinit) break;
        cnt++;
      }
    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 3";
      if (!minTSinit) { // then there's no data and nothing left to do
        break;
        //std::cout<<std::endl<<"no data left in any dcol";
        //std::cout<<std::endl<<"no data left in any dcol, confirm";
      }
      
      // loop through builders to get min ts
      cnt = 0;
      for (bit = _build.begin();bit != _build.end(); bit++) {
       if ((*bit)->nPacks() > 0) {

           // MISSING HIGHER VALUES???
        /*
        // minTS finder
        if (minTSvalue > ((*bit)->timestamp(0))) {
          minTSvalue = ((*bit)->timestamp(0));
          minTSbuilder = cnt;
        }
        */

        // new minTS finder
        //std::cout<<std::endl<<"mintsvalue "<<minTSvalue;
        //std::cout<<std::endl<<"current ts value "<<(*bit)->timestamp(0);
        if ( (calc->dt((*bit)->timestamp(0),minTSvalue)) > 0) {
          minTSvalue = (*bit)->timestamp(0);
          minTSbuilder = cnt;
        }

       }
         cnt++;
      } 
    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 4";
      // minTSbuilder is now dcol with minimum TS value

/*
          std::cout<<std::endl<<"min found in dcol "<<minTSbuilder;
          std::cout<<", with ts "<<minTSvalue;
          std::cout<<std::endl<<"npacks in dcol "<<minTSbuilder<<", "
            <<(*(_build.begin()+minTSbuilder))->nPacks();
          std::cout<<" ,max n packs "
            <<(*(_build.begin()+minTSbuilder))->maxNpacks();
*/
      // pack ready for writting
      // remove pack from dcol
      evt->addHitPack((*(_build.begin()+minTSbuilder))->dataPack(0));
      (*(_build.begin()+minTSbuilder))->erasePack(0);
      _ndpacksOKadded++;
      _hTs[1]->Fill((*(_build.begin()+minTSbuilder))->timestamp(0)); 
    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 5";

      /*
      // get all packs equal to this min time
      while (packSearchFlag) {
        evtAddFlag = false;
        unsigned count = 0;
        for (bit = _build.begin();bit != _build.end(); bit++) {
          if ((*bit)->nPacks() > 0) { // this builder should have data
          // add all packs matching the min ts
          if (minTSvalue == (*bit)->timestamp(0)) {
            evt->addHitPack((*bit)->dataPack(0));
            evtAddFlag = true;
            (*bit)->erasePack(0);
            _ndpacksOKadded++;
          }
          }
          count++;
        } // minTSbuilder is now dcol with minimum TS value
        if (!evtAddFlag) {
          packSearchFlag = false;
        }
      }
      */
 
      // check if any builder is almost empty
      if (!_lastRecord) {
      for (bit = _build.begin();bit != _build.end(); bit++) {
        //if ((*bit)->nPacks() < (*bit)->maxNpacks()) {
        //if ((*bit)->nearEmpty() && !(*bit)->nearEOF()) {
        if ((*bit)->nearEmpty()) {
          _mergeFlag = false;
         //std::cout<<std::endl<<"MERGE FLAG FALSE";
         //std::cout<<std::endl<<"MERGE FLAG FALSE";
          //std::cout<<std::endl<<"npacks in dcol "<<pcount<<", "<<(*bit)->nPacks();
          //std::cout<<" max n packs "<<(*bit)->maxNpacks();
        }
        // if size < maxNpacks then mergeFlag = false
      }
      }
    //std::cout<<std::endl<<"made it here";
    //std::cout<<std::endl<<"made it here, confirm 6";
      /*
      if (!_mergeFlag) {
        unsigned pcount = 0;
         std::cout<<std::endl<<"STOP MERGING";
        for (bit = _build.begin();bit != _build.end(); bit++) {
          std::cout<<std::endl<<"npacks in dcol "<<pcount<<", "<<(*bit)->nPacks();
          std::cout<<" max n packs "<<(*bit)->maxNpacks();
          pcount++;
        }
      }
      */
    /* old way, cuts data into events
      EbDhcEvent* evt = new EbDhcEvent();
      unsigned maxTSmatched;
      EbDhcDTCalculator* calc = new EbDhcDTCalculator();
      bool packSearchFlag = true;
      bool evtAddFlag = false;

      // find min timestamp in builders' first pack
      // find most min ts between dcols, not within
      // this assumes first ts in each dcol is already the min (pre-ordered)
      unsigned minTSvalue = (_build.at(0)->timestamp());
      unsigned minTSbuilder = 0;
      for  (unsigned s(0); s<_Ndcols; s++) {
        if (minTSvalue > (_build.at(s)->timestamp())) {
          minTSvalue = (_build.at(s)->timestamp());
          minTSbuilder = s;
        }
      } // minTSbuilder is now dcol with minimum TS value
        // minTSvalue is now minimum TS in all first builders
      maxTSmatched = minTSvalue;

      // search builders until no more matches found using minTSvalue as 
      // the only reference (i.e. not a TS growing event)
      while (packSearchFlag) {
        std::vector<EbDhcBuild*>::iterator bit;
        evtAddFlag = false;
        for (bit = _build.begin();bit != _build.end(); bit++) {
          unsigned currTSvalue = ((*bit)->timestamp());
          int tsdiff = calc->dt(minTSvalue, currTSvalue);

          if (tsdiff < _tsThreshold && tsdiff > -1){
            evt->addHitPack((*bit)->dataPack(0));
            evtAddFlag = true;
            (*bit)->erasePack(0);
            if (maxTSmatched < currTSvalue) maxTSmatched = currTSvalue;
          }
        }
        if (!evtAddFlag) { // now, done clearing top level with respect to first min TS
                           // next, repeat if maxTSmatched within 80 of new min
          // find new min
          minTSvalue = (_build.at(0)->timestamp());
          for  (unsigned s(0); s<_Ndcols; s++) {
            if (minTSvalue > (_build.at(s)->timestamp())) {
              minTSvalue = (_build.at(s)->timestamp());
            }
          } //minTSvalue is now new minimum TS value

          // check if new min is also part of this event
          //if (fabs(calc->dt(maxTSmatched,minTSvalue)) > 79) 
          if (fabs(calc->dt(maxTSmatched,minTSvalue)) > (_tsThreshold-1)) 
            packSearchFlag = false;
          // otherwise while loop repeats using new min value
          // this allows the event to grow beyond TSdiff=80
        }
      } // end while packSearchFlag
      // Q: Now what to do with the event?
      // A: the event just merged is ready to be written 
      evt->write(_aout);
      evt->clean();
      delete evt;
      delete calc;
      // Q: Now what about rest of packs?
      // events need to be built until Npacks left < maxNpacks 
      //   for any builder
      // keep merging until one of dcols drops under threshold
      std::vector<EbDhcBuild*>::iterator bit;
      for (bit = _build.begin();bit != _build.end(); bit++) {
        if ((*bit)->nPacks() < (*bit)->maxNpacks()) 
          _mergeFlag = false;
        // if size < maxNpacks then mergeFlag = false
      }
    */
    } // finished merging
      _aout.open(_file.str().c_str(),ofstream::out | ofstream::app);
      evt->write(_aout);
      _aout.close();
      evt->clean();
    delete evt;
    delete calc;
    //std::cout<<std::endl<<"made it here"<<_eventRecordCount;
    //std::cout<<std::endl<<"made it here, confirm 7";
    /*unsigned percent = (unsigned)(100*((double)_eventRecordCount)/((double)_NEventRecords));
    if ( percent%25 == 0 ) {
    std::cout<<std::endl<<"percentage records considered: "<<percent;
    std::cout<<std::endl<<"record "<<_eventRecordCount<<" of "<<_NEventRecords;
    std::cout<<std::endl<<"ndpacks considered: "<<_ndpacks;
    std::cout<<std::endl<<"percentage considered: "<<100*((double)_ndpacks/(double)_ndpacksIP);
    std::cout<<std::endl<<"ndpacks verified (not containing chksum error): "<<_ndpacksOK;
    std::cout<<std::endl<<"percentage verified: "<<100*((double)_ndpacksOK/(double)_ndpacksIP);
    std::cout<<std::endl<<"ndpacks OK and added: "<<_ndpacksOKadded;
    std::cout<<std::endl<<"percentage OK and added: "<<100*((double)_ndpacksOKadded/(double)_ndpacksOK);
    std::cout<<std::endl<<"ndpacks in initialization: "<<_ndpacksIP;
    std::cout<<std::endl;
    }*/


    //_Npacks[s] = _build.at(s)->nPacks(); // to reset number of packs remaining in build

    // now deal with remaining data packs if on the last event subrecord
    // build events out of remaining data packs

    break;
    }  // end case

    // process configuration data for event building
    case RcdHeader::configurationStart: {
     if (_eventRecordCount == 0) {
      std::vector<const DhcReadoutConfigurationData*>
        vcd(accessor.access<DhcReadoutConfigurationData>());
    std::cout<<std::endl<<"vcd size: "<<vcd.size()<<std::endl<<std::endl;

       _Ntotdcon = 0;
       unsigned ndcols = 0;

      for(unsigned i(0);i<vcd.size();i++) { // one for each crate
        for(unsigned s(2);s<=21;s++) { // one for each dcol in a crate
        if (vcd[i]->slotEnable(s)){
          ndcols ++;

        //std::cout<<std::endl<<"new bdcol initiated";
         for (unsigned f(0);f<12;f++) 
           if (vcd[i]->slotFeEnable(s,f)) {
             _Ntotdcon +=1;
             //std::cout<<std::endl<<"number of dcon increased";
           }
        } 
        } // end slot (dcol)loop
      } // end vcd loop
     std::cout<<std::endl<<"finished initialization ";
     std::cout<<std::endl<<"ndcols enabled =  "<<ndcols;

     // initialize parameters for triggerless event builder
      //_hNpacks.Fit("gaus");
      // need to use max = mean + 10*sigma
      unsigned maxNpacks = 0;
      unsigned parPackLimit = 0 ;
    std::cout<<std::endl<<"_Ndcols: "<<_Ndcols<<std::endl<<std::endl;
    if (_Ndcols != _ndcols) std::cout<<std::endl<<"CANCEL NOW, NDOLS MISMATCH";
    if (_Ndcols != _ndcols) std::cout<<std::endl<<"CANCEL NOW, NDOLS MISMATCH";

      for (unsigned s(0); s<_Ndcols; s++) { //v size is number of DCOLs
        //EbDhcBuild* build = new EbDhcBuild();
        maxNpacks = (unsigned)_hNpacks[s]->GetMean()+(unsigned)(5*_hNpacks[s]->GetRMS());
        //_maxNpacks[s] = (unsigned)_hNpacks[s]->GetMean()+(unsigned)(20*_hNpacks[s]->GetRMS());
        _build.at(s)->maxNpacks(maxNpacks);
        //_build.at(s)->maxNpacks(_maxNpacks[s]);

        parPackLimit =  maxNpacks * (_parNrecords);
        _build.at(s)->parPackLimit(parPackLimit);

        //_build.at(s)->parPackLimit(_parPackLimit[s]);
    //std::cout<<std::endl<<"packLim: "<<_parPackLimit[s]<<" for dcol: "<<s<<std::endl<<std::endl;
    std::cout<<std::endl<<"packLim: "<<_build.at(s)->parPackLimit()<<" for dcol: "<<s<<std::endl;
    //std::cout<<std::endl<<"packLim: "<<_build.at(s)->parPackLimit()<<" for dcol: "<<s<<std::endl<<std::endl;
        //_build.push_back(build);
      }
      std::cout<<std::endl<<_Ndcols<<" dcols total";

    std::cout<<std::endl<<"finished processing configurationStart data "<<std::endl;
    std::cout<<std::endl<<"ndpacks in initialization: "<<_ndpacksIP;
     }
    for (unsigned i(0);i<_ndcols;i++) {
      _hNpacks[i]->Write();
    }

    _rfile->Write();
    for (unsigned i(0);i<_ndcols;i++) {
      delete _hNpacks[i];
    }
    break;
    }
    default: {
      break;
    }
    }; //end switch
//         std::cout<<std::endl<<"finished initialization ";
  
  } //end void event (RcdRecord &r)

// can put other methods here
//see HstDhcEventBuilder.hh for example

private:

//declare histograms, constants, vectors
//declare!=define, all definitions belong in their respective methods

//data for event()
  std::vector<EbDhcBuild*> _build;
  //short unsigned _tsThreshold;
  unsigned _Ntotdcon; // number of enabled dcol channels for A dcol
  unsigned _eventRecordCount; // counts event records 
  unsigned _packsUseCount; // counts event records 
  unsigned _countBuildCycles;
  unsigned _NEventRecords;
  unsigned _parNrecords;
  unsigned _Ndcols;
  DhcFeHitData* copy;

  bool _mergeFlag;
  bool _lastRecord;

  std::ofstream _aout;
  std::ostringstream _file;

  // data for number of packs histograms
//  TCanvas *_canvas;
  TH1F *_hNpacks[40];
  TH1F *_hTs[2];

  unsigned _ndcols;
  unsigned _ndpacksIP;
  unsigned _ndpacksOK;
  unsigned _ndpacksOKadded;
  unsigned _ndpacks;
  TFile* _rfile;
   std::ostringstream _rout;

};

#endif // EbDhcRecords_HH
