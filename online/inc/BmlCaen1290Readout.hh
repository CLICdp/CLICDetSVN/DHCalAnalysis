#ifndef BmlCaen1290Readout_HH
#define BmlCaen1290Readout_HH

#include <iostream>
#include <fstream>

#include "RcdHeader.hh"
#include "RcdUserRW.hh"
#include "SubInserter.hh"
#include "SubAccessor.hh"

#include "DaqBusAdapter.hh"
#include "VMEAddressTableASCIIReader.hh"

#include "BmlCaen1290VmeDevice.hh"


class BmlCaen1290Readout : public RcdUserRW {

public:
  BmlCaen1290Readout(unsigned b, unsigned char c, unsigned short a0=0x00c5, unsigned short a1=0) : 
    _busAdapter(0),
    _addressTableReader("online/hal/Caen1290VmeAddress.hal"),
    _addressTable("CAEN 1290 VME Address Table",_addressTableReader) {

    // Set up locations
    _location[0].crateNumber(c);
    _location[1].crateNumber(c);
    _location[0].baseAddress(a0);
    _location[1].baseAddress(a1);
    _location[0].label(0);
    _location[1].label(0);
    
    // Zero device pointers
    for(unsigned i(0);i<2;i++) _device[i]=0;

    // Catch exceptions locally here in case PCI card not installed
    UtlPack _interface(b);
    try {
#ifdef CAEN_BUS_ADAPTER
      UtlPack _interface(b);
      _busAdapter=new DaqBusAdapter(HAL::CAENLinuxBusAdapter::V2718,
				    _interface.byte(1), _interface.byte(0));
#else
      _busAdapter=new DaqBusAdapter(b);
#endif
    } catch ( std::exception e ) { 
      std::ostringstream message;
      message << " BmlCaen1290Readout::ctor() "
	      << " PCI card " << (unsigned)_interface.byte(1)
	      << " chain " << (unsigned)_interface.byte(0)
	      << " NOT found ";
      std::cerr << message.str() << std::endl;
      _busAdapter=0;
    }
    
    if(_busAdapter!=0) {
      for(unsigned i(0);i<2;i++) {
	_device[i]=new
	  BmlCaen1290VmeDevice(_addressTable,*_busAdapter,_location[i].baseAddress());
	
	if(_device[i]->alive()) {
	  std::cout << "BmlCaen1290Readout::ctor()  PCI card " << b
		    << ", VME base address 0x" << std::hex
		    << (unsigned)_location[i].baseAddress() << std::dec 
		    << "0000 found alive" << std::endl;
	} else {
	  delete _device[i];
	  _device[i]=0;
	}

	  if(_device[i]==0) 
	  std::cout << "BmlCaen1290Readout::ctor()  PCI card " << b
	  << ", VME base address 0x" << std::hex
	  << (unsigned)_location[i].baseAddress() << std::dec 
	  << "0000 not found alive" << std::endl;

      }
    }
  }
  
  virtual ~BmlCaen1290Readout() {
  }

  bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "BmlCaen1290Readout::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    SubInserter inserter(r);

    UtlPack tid;
    tid.halfWord(1,SubHeader::bml);

    _daqMultiTimer=inserter.insert<DaqMultiTimer>(true);
    _daqMultiTimer->timerId(tid.word());
    inserter.extend(32*sizeof(UtlTime));


    // Check record type
    switch (r.recordType()) {
      
    case RcdHeader::startUp: {

      // Use default value of opcode data
      BmlCaen1290OpcodeData ocd;
      if(doPrint(r.recordType(),1)) ocd.print(std::cout," ") << std::endl;
    
      for(unsigned i(0);i<2;i++) {
	if(_device[i]!=0) {
	  
	  // This needs a 2 sec (!) recovery time so only do at startUp
	  assert(_device[i]->reset());

	  // Have to do opcode configuration here as it
	  // takes too long to do every configuration
	  assert(_device[i]->writeOpcodeData(ocd));
	}
      }
      break;
    }
      
    case RcdHeader::runStart: {
      assert(readRunData(r));
     
      // Have to do opcode configuration here as it takes too long to do every configuration
      // Hence, have to treat this as "run" data, i.e. doesn't change during a run
      /*
      SubAccessor accessor(r);
      
      std::vector<const BmlLocationData<BmlCaen1290OpcodeData>*>
	b(accessor.access< BmlLocationData<BmlCaen1290OpcodeData> >());
      
      for(unsigned i(0);i<2;i++) {
	if(_device[i]!=0) {
	  
	  if(b.size()==1 && b[0]->label()==1) {
	    if(doPrint(r.recordType(),1)) b[0]->print(std::cout," ") << std::endl;
	    assert(_device[i]->writeOpcodeData(*(b[0]->data())));
	  }
	}
      }
      */
	  
      for(unsigned i(0);i<2;i++) {
	if(_device[i]!=0) {
	  
	  // Do opcode data
	  BmlLocationData<BmlCaen1290OpcodeData>
	    *e(inserter.insert< BmlLocationData<BmlCaen1290OpcodeData> >());
	  e->location(_location[i]);
	  
	  assert(_device[i]->readOpcodeData(*(e->data())));
	  if(doPrint(r.recordType(),1)) e->print(std::cout," ") << std::endl;
	}
      }      
      break;
    }
      
    case RcdHeader::runEnd: {
      assert(readRunData(r));

      for(unsigned i(0);i<2;i++) {
	if(_device[i]!=0) {

	  // Do opcode data
	  BmlLocationData<BmlCaen1290OpcodeData>
	    *e(inserter.insert< BmlLocationData<BmlCaen1290OpcodeData> >());
	  e->location(_location[i]);
	  
	  assert(_device[i]->readOpcodeData(*(e->data())));
	  if(doPrint(r.recordType(),1)) e->print(std::cout," ") << std::endl;
	}
      }
      break;
    }
      
      // Configuration start is used to set up system
    case RcdHeader::configurationStart: {
      SubAccessor accessor(r);
      
      // Get configuration readout
      std::vector<const BmlCaen1290ReadoutConfigurationData*>
	rc(accessor.access<BmlCaen1290ReadoutConfigurationData>());
      
      if(doPrint(r.recordType(),1))
	std::cout << " Number of BmlCaen1290ReadoutConfigurationData"
		  << " subrecords found " << rc.size() << std::endl << std::endl;

      assert(rc.size()<=1);
      if(rc.size()==1) _readoutConfiguration=*(rc[0]);
      if(doPrint(r.recordType(),1)) _readoutConfiguration.print(std::cout," ") << std::endl;
      
      // Now write the configuration data
      if(_readoutConfiguration.enable()) {
	
	// Get data from record
	std::vector<const BmlLocationData<BmlCaen1290ConfigurationData>*>
	  b(accessor.access< BmlLocationData<BmlCaen1290ConfigurationData> >());
	
	if(doPrint(r.recordType(),1))
	  std::cout << " Number of BmlCaen1290ConfigurationData subrecords found = "
		    << b.size() << std::endl << std::endl;
	for(unsigned i(0);i<b.size();i++) {
	  if(doPrint(r.recordType(),1)) b[i]->print(std::cout," ") << std::endl;
	}
	
	// Do address broadcast
	for(unsigned i(0);i<b.size();i++) {
	  if(b[i]->addressBroadcast() && b[i]->label()==1) {
	    for(unsigned j(0);j<2;j++) {
	      if(_device[j]!=0) {
		assert(_device[j]->writeConfigurationData(*(b[i]->data())));
	      }	
	    }
	  }
	}
	
	// Do individual addresses
	for(unsigned i(0);i<b.size();i++) {
	  if(!b[i]->addressBroadcast() && b[i]->label()==1) {
	    for(unsigned j(0);j<2;j++) {
	      if(_device[j]!=0) {
		if(_device[j]->baseAddress()==b[i]->baseAddress()) {
		  if(doPrint(r.recordType(),1)) b[i]->print(std::cout," ") << std::endl;
		  assert(_device[j]->writeConfigurationData(*(b[i]->data())));
		}	
	      }
	    }
	  }
	}
	
	for(unsigned i(0);i<2;i++) {
	  if(_device[i]!=0) assert(_device[i]->clear());
	}

	// Read back configuration
	assert(readConfigurationData(r));
      }
      break;
    }
      
      // Configuration end reads back setup
    case RcdHeader::configurationEnd: {
      assert(readConfigurationData(r));
      break;
    }
      
    case RcdHeader::acquisitionStart: {
      if(_readoutConfiguration.enable()) {

	SubAccessor accessor(r);
	
	for(unsigned i(0);i<2;i++) {
	  if(_device[i]!=0) {
	    
	    // Must load test data here as clear() destroys it
	    std::vector<const BmlLocationData<BmlCaen1290TestData>*>
	      b(accessor.access< BmlLocationData<BmlCaen1290TestData> >());
	    
	    if(doPrint(r.recordType(),1))
	      std::cout << " Number of BmlCaen1290TestData subrecords found = "
			<< b.size() << std::endl << std::endl;
	    
	    //for(unsigned j(0);j<b.size();j++) { ??? Dunno why this fails
	    for(unsigned j(0);j<b.size() && j<1;j++) {
	      if(doPrint(r.recordType(),1)) b[j]->print(std::cout," ") << std::endl;
	      assert(_device[i]->writeTestData(*(b[j]->data())));
	    }
	    
	    /*
	    //JUNK TEST TO SEE IF SPURIOUS TRIGGERS
	    for(unsigned j(0);j<10;j++) {
	    BmlCaen1290TriggerData xxx;
	    assert(_device[i]->readTriggerData(xxx));
	    xxx.print(std::cout," SLEEP ");
	    sleep(1);
	    }
	    */	    
	    
	  }
	}
	
	// Get trigger data to check buffers are empty
	assert(readTriggerData(r));
      }
      _nTrgs=0;
      
      break;
    }
      
    case RcdHeader::acquisitionEnd: {
      if(_readoutConfiguration.enable()) {

	// Get trigger data to check buffers are correct before clearing
	assert(readTriggerData(r));
	
	for(unsigned i(0);i<2;i++) {
	  if(_device[i]!=0) assert(_device[i]->clear());
	}
      }

      break;
    }
      
    case RcdHeader::trigger: {
      if(_readoutConfiguration.enable()) {

	// Do software trigger
	for(unsigned i(0);i<2;i++) {
	  if(_device[i]!=0) {
	    if(_readoutConfiguration.softTrigger()) {
	      if(doPrint(r.recordType(),1)) std::cout << " Caen1290 soft trigger" << std::endl;
	      assert(_device[i]->softTrigger());
	    }
	  }
	}
	
	_daqMultiTimer->addTimer();
	
	// Readout trigger data if right period
	_bufferAlmostFull=false;
	if(_readoutConfiguration.readPeriod()>0 &&
	   (_nTrgs%_readoutConfiguration.readPeriod())==0) {
	  assert(readTriggerData(r));
	  //assert(readEventData(r));
	}

	// Flag full buffer to stop spill
	if(_bufferAlmostFull) {
	  SubAccessor accessor(r);
	  std::vector<const DaqTrigger*> v(accessor.access<DaqTrigger>());
	  assert(v.size()==1);

	  // Cludge; make non-const pointer
	  DaqTrigger *dt((DaqTrigger*)v[0]);
	  dt->tdcBufferFull(true);
	}
      }

      _nTrgs++;
      break;
    }
      

    // WON'T WORK FOR S/W TRIGGER YET!
    case RcdHeader::triggerBurst: { 
      SubAccessor accessor(r);
      
      std::vector<const DaqBurstStart*>
	b(accessor.access<DaqBurstStart>());
      assert(b.size()==1);

      _daqMultiTimer->addTimer();
	
      if(_readoutConfiguration.enable()) {
	_bufferAlmostFull=false;
	assert(readTriggerData(r));
	
	// Flag full buffer to stop spill
	if(_bufferAlmostFull) {
	  std::vector<const DaqBurstEnd*> v(accessor.access<DaqBurstEnd>());
	  assert(v.size()==1);

	  // Cludge; make non-const pointer
	  DaqBurstEnd *dbe((DaqBurstEnd*)v[0]);
	  dbe->tdcBufferFull(true);
	}
      }

      _nTrgs+=1+b[0]->maximumNumberOfExtraTriggersInBurst();
      break;
    }

    case RcdHeader::event: {
      if(_readoutConfiguration.enable()) {
      
	assert(readEventData(r));
      }
      
      _nEvents++;
      break;
    }
      
    default: {
      break;
    }
    };
    
    // Close off overall timer
    _daqMultiTimer->addTimer();
    if(doPrint(r.recordType())) _daqMultiTimer->print(std::cout," ") << std::endl;

    return true;
  }

  /////////////////////////////////////////////////////////////////////////////////////
  
  bool readRunData(RcdRecord &r) {
    SubInserter inserter(r);
    
    for(unsigned i(0);i<2;i++) {
      if(_device[i]!=0) {
	  
	BmlLocationData<BmlCaen1290RunData>
	  *d(inserter.insert< BmlLocationData<BmlCaen1290RunData> >());
	d->location(_location[i]);
	
	assert(_device[i]->readRunData(*(d->data())));
	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;
      }
    }

    return true;
  }

  bool readConfigurationData(RcdRecord &r) {
    SubInserter inserter(r);

    for(unsigned i(0);i<2;i++) {
      if(_device[i]!=0) {
	  
	BmlLocationData<BmlCaen1290ConfigurationData>
	  *d(inserter.insert< BmlLocationData<BmlCaen1290ConfigurationData> >());
	d->location(_location[i]);

	assert(_device[i]->readConfigurationData(*(d->data())));
	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;
      }
    }
    return true;
  }

  bool readEventData(RcdRecord &r) {
    SubInserter inserter(r);

    for(unsigned i(0);i<2;i++) {
      if(_device[i]!=0) {
	    
	BmlLocationData<BmlCaen1290EventData>
	  *d(inserter.insert< BmlLocationData<BmlCaen1290EventData> >());
	d->location(_location[i]);
	    
	assert(_device[i]->readEventData(*(d->data()),
					 _readoutConfiguration.bltReadout(),
					 _readoutConfiguration.arrayReadout()));
	inserter.extend(d->data()->numberOfWords()*4);
	    
	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

	_daqMultiTimer->addTimer();
      }
    }

    return true;
  }

  bool readTriggerData(RcdRecord &r) {
    SubInserter inserter(r);

    for(unsigned i(0);i<2;i++) {
      if(_device[i]!=0) {
	BmlLocationData<BmlCaen1290TriggerData>
	  *d(inserter.insert< BmlLocationData<BmlCaen1290TriggerData> >());
	d->location(_location[i]);
	
	assert(_device[i]->readTriggerData(*(d->data())));
	if(doPrint(r.recordType(),1)) d->print(std::cout," ") << std::endl;

	BmlCaen1290StatusRegister s(d->data()->statusRegister());

	if(s.anyTdcError()) {
	  BmlLocationData<BmlCaen1290TdcErrorData>
	    *e(inserter.insert< BmlLocationData<BmlCaen1290TdcErrorData> >());
	  e->location(_location[i]);
	  
	  assert(_device[i]->readTdcErrorData(*(e->data())));
	  if(doPrint(r.recordType(),1)) e->print(std::cout," ") << std::endl;
	}

	if(s.bufferAlmostFull()) {
	  _bufferAlmostFull=true;
	  std::cout << "TDC" << i << " almost full at "
		    << _nTrgs << " triggers" << std::endl;
	  d->print(std::cout) << std::endl;
	  std::cerr << "TDC" << i << " almost full at "
		    << _nTrgs << " triggers" << std::endl;
	  d->print(std::cerr) << std::endl;
	}

	_daqMultiTimer->addTimer();
      }
    }

    return true;
  }
  
  
private:
  DaqBusAdapter *_busAdapter;
  HAL::VMEAddressTableASCIIReader _addressTableReader;
  HAL::VMEAddressTable _addressTable;
  
  BmlCaen1290VmeDevice *_device[2];
  BmlLocation _location[2];

  BmlCaen1290ReadoutConfigurationData _readoutConfiguration;

  bool _bufferAlmostFull;

  DaqMultiTimer *_daqMultiTimer;

  unsigned _nTrgs;
  unsigned _nEvents;
};

#endif
