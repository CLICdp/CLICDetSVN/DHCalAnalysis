//
// $Id: EbDhcBuild.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcBuild_HH
#define EbDhcBuild_HH

#include <string>
#include <iostream>
#include <sstream>
#include <utility>
#include <vector>
#include <math.h>

#include "EbDhcDTCalculator.hh"
#include "EbDhcEvent.hh"
#include "UtlPack.hh"
#include "UtlArguments.hh"

#include "TROOT.h"
#include "TStyle.h"
#include "TApplication.h"


//declare class
class EbDhcBuild {

//declare objects and data in public
public:

//examples
/*
  static double cmax;
  EbDhcBuild();
  unsigned vmead() const; 
*/
  EbDhcBuild();
  unsigned nPacks();
  unsigned nPacksTot();
  unsigned nPacksAdded();
  void nPacksTot(unsigned ndp);
  void order();
  void build(std::ofstream &aout, unsigned &maxNpacks, bool &runType);
  void addPack(DhcFeHitData* data,unsigned crateNumber);
  unsigned timestamp(unsigned index);
  DhcFeHitData* dataPack(unsigned index);
  void erasePack(unsigned ep);
  void maxNpacks(unsigned mnp);
  unsigned maxNpacks();
  void parPackLimit(unsigned ppl);
  unsigned parPackLimit();
  bool nearEOF();
  bool full();
  bool nearEmpty();

private:
//example
//  UtlPack _data[4];
  std::vector <DhcFeHitData*> _buffer;
  unsigned _maxNpacks;
  unsigned _parPackLimit;
  unsigned _nPacksTot;
  unsigned _nPacksAdded;

//data for writeAscii()
  std::ofstream aout;
//  std::ofstream bout;

};  // end of class

#ifdef CALICE_DAQ_ICC

// define methods, data, strings, pairs
// see DhcFeHitData.hh for examples

//example
/*
EbDhcBuild::EbDhcBuild() {
}
unsigned EbDhcBuild::vmead() const {
  return _data[0].bits(29,30);
}
*/
EbDhcBuild::EbDhcBuild() {
  _nPacksTot = 0;
  _nPacksAdded = 0;
  _parPackLimit = 0;
  _maxNpacks = 0;
}

bool EbDhcBuild::nearEOF() {
 bool flag = false;
 if ( ((this->nPacksTot()) - (this->nPacksAdded())) < this->parPackLimit() )
   flag = true;

 return flag;
}

bool EbDhcBuild::full() {
  bool flag = false;
  if (this->nPacks() >= this->parPackLimit())
    flag = true;

  return flag;
}

bool EbDhcBuild::nearEmpty() {
  bool flag = false;
  if (this->nPacks() < this->maxNpacks())
    flag = true;

  return flag;
  
}

unsigned EbDhcBuild::nPacks() {

  return _buffer.size();

}

unsigned EbDhcBuild::nPacksTot() {
  return _nPacksTot;
}

unsigned EbDhcBuild::nPacksAdded() {
  return _nPacksAdded;
}

void EbDhcBuild::nPacksTot(unsigned ndp) {
  _nPacksTot = _nPacksTot + ndp;
}

void EbDhcBuild::maxNpacks(unsigned mnp) {
  _maxNpacks = mnp;
}
unsigned EbDhcBuild::maxNpacks() {
  return _maxNpacks;
}
 
void EbDhcBuild::parPackLimit(unsigned ppl) {
  _parPackLimit = ppl;
}
unsigned EbDhcBuild::parPackLimit() {
  return _parPackLimit;
}
 
unsigned EbDhcBuild::timestamp(unsigned index) {
  return ((*(_buffer.begin()+index))->timestamp());
}

DhcFeHitData* EbDhcBuild::dataPack(unsigned index) {
  // return _buffer.at(index);
  return ((*(_buffer.begin()+index)));
}

void EbDhcBuild::erasePack(unsigned ep) {
  _buffer.erase(_buffer.begin()+ep);
}

void EbDhcBuild::addPack(DhcFeHitData* data,unsigned crateNumber) {
  _nPacksAdded++;
  // first write vmead to the hit package
  switch (crateNumber) {
    case 220: {
      data->vmead(0);
    break;
    }
    case 221: {
      data->vmead(1);
    break;
    }
    default:{break;}
  };
  _buffer.push_back(data);
}

void EbDhcBuild::order() {
  if (this->nPacks() > 1) {
  bool swapping;
  EbDhcDTCalculator* calc = new EbDhcDTCalculator();

  while (1) {
    swapping = false;
    std::vector<DhcFeHitData*>::iterator fehdi;
    for (fehdi = _buffer.begin(); fehdi != (_buffer.end()-1); fehdi++) {
      if (calc->dt( (*fehdi)->timestamp() , (*(fehdi+1))->timestamp() ) < 0) {
        iter_swap( fehdi, fehdi+1);
        swapping = true;
      }
    }
    if (!swapping) break;
  }
  }
}

void EbDhcBuild::build(std::ofstream &aout, unsigned &maxNpacks, bool &runType) {

  //std::cout<<std::endl<<"started build ";
  //std::cout<<std::endl<<"started build ";
  EbDhcDTCalculator* calc = new EbDhcDTCalculator();
  EbDhcEvent* evt = new EbDhcEvent();
  unsigned count = 0; // element location in the vector, i.e.0,1,2,...
  bool erase = false;

  std::vector<DhcFeHitData*>::iterator fehdi;
  for (fehdi = _buffer.begin(); fehdi != (_buffer.end()-1); fehdi++) {
//  std::cout<<std::endl<<"count = "<<count << "maxNpacks "<<maxNpacks;;

    // check if reached last pack in current event    
    if (fabs( calc->dt( (*fehdi)->timestamp()  , 
			(*(fehdi+1))->timestamp()  ) ) > 79  ) { 
      evt->addHitPack(*fehdi);
      // analyze event
      // delete or write
        // if delete, only delete packs in event, internally
      if (runType) { // only check status if its a cosmic run
      if (evt->status()) {
        evt->write(aout);
      }
      }
      else evt->write(aout);

      delete evt;

      if ( count > (_buffer.size() - maxNpacks)) { // check if buffer ready to be refilled
      //  std::cout<<std::endl<<"on count = "<<count;
        fehdi = (_buffer.end()-2);
        erase = true;
      }
      else {
        evt = new EbDhcEvent();
      }
    }
    else {
      evt->addHitPack(*fehdi);
    }

    count += 1;
  }
    if (erase) _buffer.erase(_buffer.begin(),_buffer.begin()+count);

  else { //build all remaining packs into events
         // this is the last call to build
    count = 0;

    for (fehdi = _buffer.begin(); fehdi != (_buffer.end()-1); fehdi++) {
      if (fabs( calc->dt( (*fehdi)->timestamp()  ,
			  (*(fehdi+1))->timestamp()  ) ) > 79  ) {
        evt->addHitPack(*fehdi);
        if (runType) { // only check status if its a cosmic run
        if (evt->status()) {
          if (count >= (_buffer.size() - 2)) { // _buffer[count] is next to last element
            //delete evt;
          }
          else evt->write(aout);
        }
        }
        else {
          if (count >= (_buffer.size() - 2)) { // _buffer[count] is next to last element
            //delete evt;
          }
          else evt->write(aout);
        }

        assert (evt!=0);
        //if (evt!=0) delete evt;
        
        evt = new EbDhcEvent();
      }
      else {
        if (count >= (_buffer.size() - 2)) { // _buffer[count] is next to last element
          //if (evt!=0) delete evt;
        }
        else evt->addHitPack(*fehdi);
      }
      count += 1;
      //note: if the very last data pack is a new event it doesnt matter
      // because its not a cosmic ray since its only one layer

    }
    _buffer.erase(_buffer.begin(),_buffer.begin()+count);
  }
  
  delete calc;
//  std::cout<<std::endl<<"delete event";
 // std::cout<<std::endl<<"delete event";
  //if (evt!=0) delete evt;
}

#endif // CALICE_DAQ_ICC
#endif // EbDhcBuild_HH
