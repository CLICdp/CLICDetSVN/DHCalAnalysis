#ifndef BmlCaen1290VmeDevice_HH
#define BmlCaen1290VmeDevice_HH

#include <unistd.h>

#include <iostream>
#include <vector>

// hal
#include "VMEDevice.hh"
#include "VMEAddressTable.hh"
#include "VMEBusAdapterInterface.hh"

// dual/inc/bml
#include "BmlCaen1290RunData.hh"
#include "BmlCaen1290ConfigurationData.hh"
#include "BmlCaen1290OpcodeData.hh"
#include "BmlCaen1290EventData.hh"
#include "BmlCaen1290TriggerData.hh"
#include "BmlCaen1290TestData.hh"
#include "BmlCaen1290TdcErrorData.hh"


class BmlCaen1290VmeDevice : public HAL::VMEDevice {
public:
  BmlCaen1290VmeDevice(HAL::VMEAddressTable &t,
		       HAL::VMEBusAdapterInterface &b,
		       unsigned short a=0x00c5,
		       bool an=true)
    : HAL::VMEDevice(t,b,a<<16), _baseAddress(a), _1290n(an) {
  }

  unsigned short baseAddress() const {
    return _baseAddress;
  }

  bool alive() {
    /*unsigned long*/ uint32_t value;
    try {
      UtlPack bid(0);
      for(unsigned i(0);i<5;i+=2) {
	read("BoardId",&value,2*i);
	//std::cout << "BoardId+" << std::setw(2) << i
	//	  << " = " << printHex((unsigned)value) << std::endl;
	bid.byte(2-(i/2),value&0xff);
      }
      return UtlPack(0x0000050a)==bid;

    } catch ( HAL::HardwareAccessException& e ) {
      //std::cout << "*** HA Exception occurred : " << e.what() << std::endl;
      return false;

    } catch ( std::exception e ) {
      //std::cout << "*** Unknown exception occurred" << std::endl;
      return false;
    }
  }

  bool reset() {
    //std::cout << "ModuleReset" << std::endl;
    write("ModuleReset",0);    
    sleep(2); // NEEDED?
    
    //std::cout << "Set keep token" << std::endl;
    assert(writeOpcode(0x03)); // Not readable?!?

    //std::cout << "Set TDC error mark" << std::endl;
    assert(writeOpcode(0x35)); // Not readable?!?

    //std::cout << "Set TDC error bypass" << std::endl;
    assert(writeOpcode(0x37)); // Not readable?!?

    //std::cout << "Disable test mode" << std::endl;
    assert(writeOpcode(0xc6)); // Not readable?!?

    //std::cout << "GeoAddress" << std::endl;
    write("GeoAddress",_baseAddress&0x1f);  // Sets geo address in data

    //std::cout << "reset done" << std::endl;

    return true;
  }

  bool softTrigger() {
    ///*unsigned long*/ uint32_t value;
    //read("EventCounter",&value);
    //std::cout << "Event counter = " << value << std::endl;

    //write("SoftwareTrigger",1);
    write("SoftwareTrigger",0);

    //read("EventCounter",&value);
    //std::cout << "Event counter = " << value << std::endl;

    return true;
  }

  bool clear() {
    write("SoftwareClear",0);

    write("SoftwareEventReset",0);
    /*unsigned long*/ uint32_t value;
    read("Buffer",&value);
    read("Buffer",&value);

      /*
    bool empty(false);
    for(unsigned i(0);i<100000 && !empty;i++) {
      read("Buffer",&value);
      read("Status2",&value);
      empty=((value&1)==1);
    }

    return empty;
    */
    return true;
  }

  bool readRunData(BmlCaen1290RunData &d) {
    /*unsigned long*/ uint32_t value;

    for(unsigned i(0);i<20;i++) {
      read("CRRomStart",&value,4*i);
      //std::cout << "CR ROM byte " << std::setw(2) << i << " = "
      //	<< printHex((unsigned)value) << std::endl;
      d.crRom(i,value&0xff);
    }

    for(unsigned i(0);i<2;i++) {
      read("SerialNumber",&value,4*i);
      //std::cout << "SerialNumber " << i 
      //	<< printHex((unsigned)value) << std::endl;
      d.serialNumber(i,value&0xff);
    }

    read("McstAddress",&value);
    //std::cout << "MCST address " << printHex((unsigned)value) << std::endl;
    d.mcstAddress(value&0xffff);

    read("FirmwareRevision",&value);
    //std::cout << "Firmware revision " << printHex((unsigned)value) << std::endl;
    d.vmeFirmwareRevision(value&0xff);

    read("OutputProgControl",&value);
    //std::cout << "Output programmable control " << printHex((unsigned)value) << std::endl;
    d.outputControl(value&0x7);

    UtlPack y[4];
    unsigned short *x((unsigned short*)y);

    for(unsigned i(0);i<4;i++) {
      y[0].word(0);
      if(i<2) assert(readOpcodeData(0x60,i,x,2));
      //std::cout << "Opcode 0x" << std::hex << 0x6000+i << std::dec << " = " << printHex(y[0]) << std::endl;
      d.tdcId(i,y[0].word());
    }

    assert(readOpcodeData(0x61,x));
    //std::cout << "Opcode 0x6100 = " << printHex(x[0]) << std::endl;
    d.firmwareRevision(y[0].byte(0));

    assert(readOpcodeData(0xc2,x,4));
    for(unsigned i(0);i<4;i++) {
      //std::cout << "Opcode 0xc200 " << i << " = " << printHex(x[i]) << std::endl;
    }
    d.microRevision(x[0]);
    d.microDay(x[1]);
    d.microMonth(x[2]);
    d.microYear(x[3]);

    return true;
  }

  bool writeOpcodeData(const BmlCaen1290OpcodeData &d) {
    UtlPack y[4];
    unsigned short *x((unsigned short*)y);
    short *z((short*)y);

    if(d.triggerMatching())   assert(writeOpcode(0x00));
    if(d.continuousStorage()) assert(writeOpcode(0x01));

    x[0]=d.windowWidth();
    assert(writeOpcodeData(0x10,x,1));
    z[0]=d.windowOffset();
    assert(writeOpcodeData(0x11,x,1));
    x[0]=d.extraMargin();
    assert(writeOpcodeData(0x12,x,1));
    x[0]=d.rejectMargin();
    assert(writeOpcodeData(0x13,x,1));
    if(d.subtractTriggerTime()) assert(writeOpcode(0x14));
    else                        assert(writeOpcode(0x15));

    if(d.pairMode())         x[0]=0;
    if(d.fallingEdgesOnly()) x[0]=1;
    if(d.risingEdgesOnly())  x[0]=2;
    if(d.bothEdges())        x[0]=3;
    assert(writeOpcodeData(0x22,x,1));
    if(d.pairMode()) {
      x[0]=d.lsbResolution();
      assert(writeOpcodeData(0x24,x,1));
    } else {
      x[0]=d.lsbResolution()+256*d.pairResolution();
      assert(writeOpcodeData(0x25,x,1));
    }
    x[0]=d.deadTime();
    assert(writeOpcodeData(0x28,x,1));

    if(d.headerAndTrailer()) assert(writeOpcode(0x30));
    else                     assert(writeOpcode(0x31));
    x[0]=d.maximumHits();
    assert(writeOpcodeData(0x33,x,1));
    x[0]=d.tdcInternalErrorEnable();
    assert(writeOpcodeData(0x39,x,1));
    x[0]=d.readoutFifoSize();
    assert(writeOpcodeData(0x3b,x,1));

    if(d.lsbResolution()==3) {
      y[0].word(d.enablePattern());
      if(_1290n) assert(writeOpcodeData(0x44,x,1));
      else       assert(writeOpcodeData(0x44,x,2));

    } else {
      for(unsigned i(0);i<4;i++) {
	if(!_1290n || i<2) {
	  y[0].word(d.enablePattern32(i));
	  assert(writeOpcodeData(0x46,i,x,2)); // Disables channels?
	}
      }
    }

    x[0]=d.globalOffset();
    y[0].bits(16,20,y[0].bits(11,15));
    y[0].bits(11,15,0);
    assert(writeOpcodeData(0x50,x,2));

    for(unsigned i(0);i<32;i++) {
      if(!_1290n || i<16) {
	x[0]=d.channelAdjust(i);
	assert(writeOpcodeData(0x52,i,x,1));
      }
    }

    for(unsigned i(0);i<4;i++) {
      if(!_1290n || i<2) {
	x[0]=d.tdcRcAdjust(i);
	assert(writeOpcodeData(0x54,i,x,1));
      }
    }

    return true;
  }

  bool readOpcodeData(BmlCaen1290OpcodeData &d) {
    UtlPack y[4];
    unsigned short *x((unsigned short*)y);
    short *z((short*)y);

    assert(readOpcodeData(0x02,x));
    //std::cout << "Opcode 0x0200 = " << printHex(x[0]) << std::endl;
    d.acquisitionMode(y[0].bit(0));

    assert(readOpcodeData(0x16,x,5));
    for(unsigned i(0);i<5;i++) {
      //std::cout << "Opcode 0x1600 " << i << " = " << printHex(x[i]) << std::endl;
    }
    d.windowWidth(x[0]);
    d.windowOffset(z[1]);
    d.extraMargin(x[2]);
    d.rejectMargin(x[3]);
    d.subtractTriggerTime(y[1].bit(0));

    assert(readOpcodeData(0x23,x));
    //std::cout << "Opcode 0x2300 = " << printHex(x[0]) << std::endl;
    d.edgeDetection(y[0].bits(0,1));

    assert(readOpcodeData(0x26,x));
    //std::cout << "Opcode 0x2600 = " << printHex(x[0]) << std::endl;
    d.resolution(x[0]);

    assert(readOpcodeData(0x29,x));
    //std::cout << "Opcode 0x2900 = " << printHex(x[0]) << std::endl;
    d.deadTime(y[0].bits(0,1));

    assert(readOpcodeData(0x32,x));
    //std::cout << "Opcode 0x3200 = " << printHex(x[0]) << std::endl;
    d.headerAndTrailer(y[0].bit(0));

    assert(readOpcodeData(0x34,x));
    //std::cout << "Opcode 0x3400 = " << printHex(x[0]) << std::endl;
    d.maximumHits(y[0].bits(0,3));

    assert(readOpcodeData(0x3a,x));
    //std::cout << "Opcode 0x3a00 = " << printHex(x[0]) << std::endl;
    d.tdcInternalErrorEnable(x[0]);

    assert(readOpcodeData(0x3c,x));
    //std::cout << "Opcode 0x3c00 = " << printHex(x[0]) << std::endl;
    d.readoutFifoSize(y[0].bits(0,2));

    y[0].word(0);
    if(_1290n) assert(readOpcodeData(0x45,x,1));
    else       assert(readOpcodeData(0x45,x,2));
    //std::cout << "Opcode 0x4500 = " << printHex(y[0]) << std::endl;
    d.enablePattern(y[0].word());

    //const unsigned nTdc(_1290n ? 2 : 4);

    for(unsigned i(0);i<4;i++) {
      y[0].word(0);
      if(!_1290n || i<2) assert(readOpcodeData(0x47,i,x,2));
      //std::cout << "Opcode 0x" << std::hex << 0x4700+i << std::dec << " = " << printHex(y[0]) << std::endl;
    }

    assert(readOpcodeData(0x51,x,2));
    //std::cout << "Opcode 0x5100 = " << printHex(y[0]) << std::endl;
    y[0].bits(11,15,y[0].bits(16,20));
    d.globalOffset(x[0]);

    for(unsigned i(0);i<32;i++) {
      x[0]=0;
      if(!_1290n || i<16) assert(readOpcodeData(0x53,i,x,1));
      //std::cout << "Opcode 0x" << std::hex << 0x5300+i << std::dec << " = " << printHex(x[0]) << std::endl;
      d.channelAdjust(i,y[0].byte(0));
    }

    for(unsigned i(0);i<4;i++) {
      x[0]=0;
      if(!_1290n || i<2) assert(readOpcodeData(0x55,i,x,1));
      //std::cout << "Opcode 0x" << std::hex << 0x5500+i << std::dec << " = " << printHex(x[0]) << std::endl;
      d.tdcRcAdjust(i,x[0]);
    }

    return true;
  }

  bool readTdcErrorData(BmlCaen1290TdcErrorData &d) {
    unsigned short x;

    for(unsigned i(0);i<4;i++) {
      assert(readOpcodeData(0x74,i,&x));
      //std::cout << "Opcode 0x74 TDC " << i << " = " << printHex(x) << std::endl;
      d.tdcErrors(i,x);
    }

    return true;
  }

  bool writeConfigurationData(const BmlCaen1290ConfigurationData &d) {

    write("Control",d.controlRegister().halfWord(0));
    write("InterruptLevel",d.interruptRegister().halfWord(0));
    write("InterruptVector",d.interruptRegister().halfWord(1));
    write("AlmostFullLevel",d.almostFullLevel());
    write("BltEventNumber",d.bltEventNumber());

    // Special case
    /*
    if(d.memoryTest()) assert(writeOpcode(0x01));
    else               assert(writeOpcode(0x02));
    usleep(10000);
    */
    return true;
  }

  bool readConfigurationData(BmlCaen1290ConfigurationData &d) {
    uint32_t value;

    read("Control",&value);
    //std::cout << "Control1 = " << printHex((unsigned)value) << std::endl;
    d.controlRegister(0,value&0xffff);

    read("InterruptLevel",&value);
    //std::cout << "InterruptLevel = " << printHex((unsigned)value) << std::endl;
    d.interruptLevel(value&0xffff);

    read("InterruptVector",&value);
    //std::cout << "InterruptVector = " << printHex((unsigned)value) << std::endl;
    d.interruptVector(value&0xffff);

    read("AlmostFullLevel",&value);
    //std::cout << "AlmostFullLevel = " << printHex((unsigned)value) << std::endl;
    d.almostFullLevel(value&0xffff);

    read("BltEventNumber",&value);
    //std::cout << "BltEventNumber = " << printHex((unsigned)value) << std::endl;
    d.bltEventNumber(value&0xff);

    return true;
  }

  bool writeTestData(const BmlCaen1290TestData &d) {
    UtlPack *p((UtlPack*)d.data());

    for(unsigned r(0);r<d.numberOfRepeats();r++) {
      for(unsigned i(0);i<d.numberOfWords();i++) {
	write("TestwordLo",p[i].halfWord(0));
	write("TestwordHi",p[i].halfWord(1));
      }    
    }

    return true;
  }

  bool readTriggerData(BmlCaen1290TriggerData &d) {
    /*unsigned long*/ uint32_t value;

    read("Status",&value);
    d.statusRegister(0,value&0xffff);
    read("EventFifoStatus",&value);
    d.statusRegister(1,value&0xffff);
    read("EventCounter",&value);
    d.eventCounter(value);
    read("EventStored",&value);
    d.eventStored(value&0xffff);
    read("EventFifoStored",&value);
    d.eventFifoStored(value&0xffff);

    return true;
  
  }

  bool readEventData(BmlCaen1290EventData &d, bool blt=true, bool array=false) {
    /*unsigned long*/ uint32_t value;

    d.numberOfWords(0);

    read("Status",&value);
    d.statusRegister(0,value&0xffff);
    read("EventFifoStatus",&value);
    d.statusRegister(1,value&0xffff);

    BmlCaen1290EventDatum *p(d.data());
    /*unsigned long*/ uint32_t *q((/*unsigned long*/ uint32_t*)d.data());

    unsigned nWords(64*1024);

    // Block transfer
    if(blt) {
      if(array) {
	readBlock("BufferBltArray",256,(char*)_blt,HAL::HAL_DO_INCREMENT);
	for(unsigned i(0);i<128;i++) {
	  std::cout << i << " " << _blt[i].print(std::cout," RAW ");
	}

	for(unsigned i(0);i<64 && i<nWords;i++) {
	  p[i]=_blt[2*i];
	  if((p[i].label()==BmlCaen1290EventDatum::globalTrailer) ||
	     (p[i].label()==BmlCaen1290EventDatum::filler))
	    nWords=i+1;
	}
	nWords=64;

      } else {
	memset(_blt,0,128*4);
	p[0].setInvalid();
	//readBlock("BufferBlt",8,(char*)_blt,HAL::HAL_NO_INCREMENT);
	readBlock("BufferBlt",256,(char*)_blt,HAL::HAL_NO_INCREMENT);
	//std::cout << std::dec; // Set to hex by readBlock call above
	//read("Buffer",&value);
	//for(unsigned i(0);i<128;i++) {
	//  std::cout << i << " ";
	//  _blt[i].print(std::cout," RAW ");
	//}
	//
	//read("BitSet",&value);
	//std::cout << "BitSet = " << printHex((unsigned)value) << std::endl;

	for(unsigned i(0);i<64 && i<nWords;i++) {
	  p[i]=_blt[i];
	  if((p[i].label()==BmlCaen1290EventDatum::globalTrailer) ||
	     (p[i].label()==BmlCaen1290EventDatum::filler))
	    nWords=i+1;
	}
	//nWords=64;
      }

    // Standard read
    } else {
      for(unsigned i(0);i<32*1024 && i<nWords;i++) {
	if(array) read("BufferArray",q+i,4*i);
	else      read("Buffer",q+i);
	//std::cout << "Buffer = " << printHex((unsigned)q[i]) << std::endl;
	
	if((p[i].label()==BmlCaen1290EventDatum::globalTrailer) ||
	   (p[i].label()==BmlCaen1290EventDatum::filler))
	  nWords=i+1;
      }
    }

    d.numberOfWords(nWords);

    return true;
  }

  void print(std::ostream &o) {
    o << "BmlCaen1290VmeDevice::print()" << std::endl;
    /*
    unsigned long value;
    read("FirmwareId",&value);
    o << hex << "  FirmwareId = 0x" << value << dec << std::endl;
    */
    o << std::endl;

  }

private:
  bool writeOpcode(unsigned char c) {
    return writeOpcode(c,0);
  }

  bool writeOpcode(unsigned char c, unsigned char o) {
    //std::cout << "Writing 0x" << std::hex << std::setfill('0') << std::setw(2) << (unsigned)c 
    //      << std::setw(2) << (unsigned)o << std::setfill(' ') << std::dec << std::endl;

    /*unsigned long*/ uint32_t value;
    for(unsigned i(0);i<1000000;i++) {
      read("MicroHandshake",&value);
      //std::cout << "writeOpcode " << printHex((unsigned)value) << std::endl;
      
      // Check for write bit
      if((value&1)!=0) {
	usleep(10000); // NOT NEEDED?
	write("MicroRegister",(c<<8)+o);
	return true;
      }
    }
    return false;
  }

  bool writeOpcodeData(unsigned char c, unsigned short *d, unsigned n=1) {
    return writeOpcodeData(c,0,d,n);
  }

  bool writeOpcodeData(unsigned char c, unsigned char o, unsigned short *d, unsigned n=1) {

    // Check that there is data if requested
    assert(n>0 && d!=0);

    // First write the opcode itself
    writeOpcode(c,o);
    
    //std::cout << " Writing opcode data" << std::endl;
    //for(unsigned j(0);j<n;j++) {
      //std::cout << "  Data word " << j << " = " << printHex(d[j]) << std::endl;
    //}    
    
    // Now write the data
    /*unsigned long*/ uint32_t value;
    for(unsigned j(0);j<n;j++) {
      
      // Loop until write bit seen
      bool done(false);
      for(unsigned i(0);i<1000000 && !done;i++) {
	read("MicroHandshake",&value);
	//std::cout << "writeOpcode " << printHex((unsigned)value) << std::endl;
	
	// Check for write bit
	if((value&1)!=0) {
	  done=true;
	  usleep(10000); // NOT NEEDED?
	  write("MicroRegister",d[j]);
	}
      }
	
      // If read bit never appeared, give up
      if(!done) return false;
    }
    
    return true;
  }

  bool readOpcodeData(unsigned char c, unsigned short *d, unsigned n=1) {
    return readOpcodeData(c,0,d,n);
  }

  bool readOpcodeData(unsigned char c, unsigned char o, unsigned short *d, unsigned n=1) {

    // Check some data to read back are requested
    assert(n>0);

    //std::cout << "Reading 0x" << std::hex << std::setfill('0') << std::setw(2) << (unsigned)c 
    //      << std::setw(2) << (unsigned)o << std::setfill(' ') << std::dec << std::endl;
    //std::cout << " Reading opcode data" << std::endl;

    // Write the opcode to start the transfer
    writeOpcode(c,o);

    // Now read each requested word in turn
    /*unsigned long*/ uint32_t value;
    for(unsigned j(0);j<n;j++) {

      // Loop until read bit seen
      bool done(false);
      for(unsigned i(0);i<1000000 && !done;i++) {
	read("MicroHandshake",&value);
	//std::cout << "readOpcodeHandshake " << printHex((unsigned)value) << std::endl;
	
	// Check for read bit
	if((value&2)!=0) {
	  done=true;
	  usleep(10000); // NOT NEEDED?

	  // Now read data from register
	  read("MicroRegister",&value);
	  //std::cout << "readOpcodeRegister " << printHex((unsigned)value) << std::endl;
	  d[j]=value&0xffff;
	  //std::cout << "  Data word " << j << " = " << printHex(d[j]) << std::endl;
	}
      }

      // If read bit never appeared, give up
      if(!done) return false;
    }

    return true;
  }

  const unsigned short _baseAddress;
  const bool _1290n;
  BmlCaen1290EventDatum _blt[128];
};

#endif
