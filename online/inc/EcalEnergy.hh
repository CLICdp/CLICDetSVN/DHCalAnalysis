//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalEnergy_HH
#define EcalEnergy_HH

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

#include "EcalPhysicalPad.hh"

//////////////////////////////////////////////////////////////////////////////

/// Class to hold the energy content of ECAL.
/** Navigation is through EcalPhysicalPad.
*/
class EcalEnergy 
{

   public:

//............................................................................	
   /// Constructor.
   /**
   */
   EcalEnergy() 
   {
   }
//............................................................................	
   /// Destructor.
   /**
   */
   virtual ~EcalEnergy() 
   {
   }
//............................................................................
   /// Return energy value of given channel.
   /**
   */
   double energy(EcalPhysicalPad p) const 
   {
      assert(p.isValid());
      return _energy[p.value()];
   }
//............................................................................  
   /// Set energy value of given channel.
   /**
   */
   void energy(EcalPhysicalPad p, double e) 
   {
      assert(p.isValid());
      _energy[p.value()]=e;
   }
//............................................................................  
   /// Print out energy content of ECAL.
   /**
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalEnergy::print()" << std::endl;

      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
	 out << " X,Y,Z,energy " << std::setw(3) << pp.horizontal()
		              	 << std::setw(3) << pp.vertical()
			      	 << std::setw(3) << pp.layer()
	    	     	      	 << std::setw(12)<< _energy[pp.value()]  
				 << std::endl;
      }

      return out;
   }
//............................................................................

   private:

   /// Array to hold the energy content of ECAL.
   /** Navigation is through EcalPhysicalPad.
   */
   double _energy[18*18*30];

};//end class EcalEnergy

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
