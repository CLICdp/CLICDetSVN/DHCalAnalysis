//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef EcalMap_HH
#define EcalMap_HH

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <list>


#include "EcalPhysicalPad.hh"
#include "EcalReadoutPad.hh"
#include "EcalPcbPad.hh"

using namespace std;

//////////////////////////////////////////////////////////////////////////////

/// Class to map ECAL hardware <-> physical <-> DAQ channels.
/** See also EcalPcbPad, EcalPhysicalPad, EcalReadoutPad.
*/
class EcalMap 
{

   public:
	
//............................................................................   
   /// Constructor.
   /**
   */
   EcalMap() 
   {
      reset();
   }
//............................................................................
   /// Destructor.
   /** 
   */  
   virtual ~EcalMap()
   {
   } 
//............................................................................
   /// Reset maps.
   /**
   */
   void reset() 
   {      
      _numberOfChannels = 0;
      
      for(int i=0;i<22*8;i++)
      {  _slotfe[i]=false;
      	 _slotfeNofChannels[i]=0;
      }
      

      EcalPhysicalPad dummyPhysPad;
      EcalReadoutPad dummyReadoutPad;
      EcalPcbPad dummyPcbPad;
      
      
      for(int i=0;i<22*8*12*18;i++)
      {
      	 _physical[i]=dummyPhysPad;
      }
      
      for(int i=0;i<18*18*30;i++)
      {
      	 _readout[i]=dummyReadoutPad;
      }
      
      for(int i=0;i<18*18*30;i++)
      {
      	 _pcbpad[i]=dummyPcbPad;
      }
      
      
   }
//............................................................................
   bool isConnected(int slot, int fe) const
   {  assert(slot<=21);
      assert(fe<8);
      return _slotfe[slot*8 + fe];
   } 
//............................................................................
   int numberOfChannels(int slot, int fe) const
   {  assert(slot<=21);
      assert(fe<8);
      return _slotfeNofChannels[slot*8 + fe];
   } 
//............................................................................
   /// Map given physical and readout channels.
   /**
   */
   void map(EcalPhysicalPad p, EcalReadoutPad r) 
   {
      assert(r.isValid());
      assert(p.isValid());

      _physical[r.value()]=p;
      _readout[p.value()]=r;
   }
//............................................................................
   /// Map given physical and readout channels.
   /**
   */
   void map(EcalReadoutPad r, EcalPhysicalPad p) 
   {
      map(p,r);
   }
//............................................................................
   /// Return the corresponding physical channel.
   /**
   */
   EcalPhysicalPad map(EcalReadoutPad r) const 
   {
      assert(r.isValid());
      return _physical[r.value()];
   }
//............................................................................
   /// Return the corresponding daq channel.
   /**
   */
   EcalReadoutPad map(EcalPhysicalPad p) const 
   {
      assert(p.isValid());
      return _readout[p.value()];
   }
//............................................................................
   /// Check if given daq channel is connected to a physical one. 
   /**
   */
   bool physical(EcalReadoutPad r) const 
   {
      assert(r.isValid());
      return _physical[r.value()].value()!=0xffff;
   }
//............................................................................
   /// Check if given physical channel is connected to a daq one.
   /**
   */
   bool readout(EcalPhysicalPad p) const 
   {
      assert(p.isValid());
      return _readout[p.value()].value()!=0xffff;
   }
//............................................................................
   /// Map given physical and hardware channels.
   /**
   */
   void map(EcalPhysicalPad phyP, EcalPcbPad pcbP) 
   {
      assert(phyP.isValid());
      assert(pcbP.isValid());

      _physOfpcb[pcbP.value()]=phyP;
      _pcbpad[phyP.value()]=pcbP;
   }
//............................................................................
   /// Map given physical and hardware channels.
   /**
   */
   void map(EcalPcbPad pcbP, EcalPhysicalPad phyP) 
   {
      map(phyP,pcbP);
   }
//............................................................................
   /// Return the corresponding physical channel.
   /**
   */
   EcalPhysicalPad getPhysPad(EcalPcbPad pcbP) const 
   {
      assert(pcbP.isValid());
      return _physOfpcb[pcbP.value()];
   }
//............................................................................
   /// Return the corresponding hardware channel.
   /**
   */
   EcalPcbPad getPcbPad(EcalPhysicalPad phyP) const 
   {
      assert(phyP.isValid());
      return _pcbpad[phyP.value()];
   }
//............................................................................
   /// Check if given hardware channel is connected to a physical one.
   /**
   */
   bool physical(EcalPcbPad pcbP) const 
   {
      assert(pcbP.isValid());
      return _physOfpcb[pcbP.value()].value()!=0xffff;
   }
//............................................................................
   /// Check if given physical channel is connected to a hardware one.
   /**
   */
   bool pcbpad(EcalPhysicalPad phyP) const 
   {
      assert(phyP.isValid());
      return _pcbpad[phyP.value()].value()!=0xffff;
   }
//............................................................................
   /// Print out mapping.
   /** 
   */
   std::ostream& print(std::ostream &out) const 
   {
      out << " EcalMap::print()" << std::endl;

      for(EcalPhysicalPad pp(0); pp.isValid(); pp++)
      {
	 if(readout(pp)) 
	 {
	    out << " X,Y,Z " << std::setw(3) << pp.horizontal()
		             << std::setw(3) << pp.vertical()
		             << std::setw(3) << pp.layer() 
	      	<< " = Slot,Fe,Chip,Channel "
	      	<< std::setw(3) << _readout[pp.value()].slot()
	      	<< std::setw(3) << _readout[pp.value()].frontend()
	      	<< std::setw(3) << _readout[pp.value()].chip()
	      	<< std::setw(3) << _readout[pp.value()].channel() 
	      	<< " = Pcb,Type,Chip,Channel "
	      	<< std::setw(3) << _pcbpad[pp.value()].pcb()
	      	<< std::setw(3) << _pcbpad[pp.value()].pcbtype()
	      	<< std::setw(3) << _pcbpad[pp.value()].chip()
	      	<< std::setw(3) << _pcbpad[pp.value()].channel() 
		<< std::endl;
	 }
      }
      return out;
   }
//............................................................................
   /// Read mapping from dat file.
   /** It also counts the number of channels that are connected to the daq.
   */
   bool read(std::string fileName) 
   {
      std::cout << "EcalMap::read() " << fileName << std::endl;
      std::ifstream input(fileName.c_str(),std::ios::in);
      if(!input) return false;

      reset();

      //read the comments first, 12 strings are expected
      std::string comment;
      for(int i=0;i<12;i++)
      {
         input >> comment;
         //cout << comment << endl;
      }

       
      unsigned s,f,b,l,a;
      std::string pcbname;
      
      int cnt(0);
      while(input >> s >> f >> b >> l >> a >> pcbname) 
      {
      	 assert(s<=21);
      	 assert(f<8 && b<2 && l<30 && a<2);
	 
	 
	 _slotfe[s*8 + f]=true;
	 

      	 // Check LH/RH PCB only in LH/RH connector; NEEDS CHECKING!!!
      	 //if(a==0) assert(b!=(l%2));
      
      	 unsigned cLo(0),cHi(12);

      	 // If half PCB, then restrict channel range; NEEDS CHECKING!!!
      	 if(a==0) 
	 {
	    if(b==0) cHi=6;
	    else     cLo=6;
       	 }

      	 for(unsigned c(cLo);c<cHi;c++) 
	 {
	    for(unsigned m(0);m<18;m++) 
	    {

	       EcalReadoutPad r(s,f,c,m);

	       // Calculate the local numbering on the PCB
	       unsigned x=3*(c%6)+(m%3);
	       unsigned y=6*(c/6)-(m/3)+5;
      	       
	       //GM for the slabs at the bottom part that are 
	       //   connected to a RH connector
      	       if(a==0 && b==1) y=y-6;

	       // Global y depends on which way up the PCB is
	       if((l%2)==1) y=6*a+5-y;
	       
      	       //GM for the slabs at the bottom part that are 
	       //   connected to a RH connector the daq is 
	       //   reading the chips in reverse order (from 11 to 6)
	       if(a==0 && b==1) x=3*((11+6-c)%6)+(m%3);
      	       	       

	       // Central alveoli are offset in y
	       y+=6*a;

	  
	       EcalPhysicalPad p(x,y,l);
	       map(p,r);
	       
	       _slotfeNofChannels[s*8+f]++;
	       
	       unsigned pcb = atoi(pcbname.c_str())-1;
	       unsigned pcbtype = a;
	       unsigned chip = c;
	       unsigned chan = m;
	       assert(pcb<40 && pcbtype<2 && chip<12 && chan<18);
	       
	       EcalPcbPad pcbP(pcb,pcbtype,chip,chan);
	       map(p,pcbP);  
	    }
      	 }
         cnt++; 
      }

      //cout << "EcalMap::read() ended, " << cnt << " input lines read"  << endl; 


      _numberOfChannels = 0;
      for(EcalPhysicalPad pp(0);pp.isValid();pp++) 
      {
     	 if(readout(pp)) _numberOfChannels++;
      }

      return true;
   }
//............................................................................
   /// Return the number of channels connected to the daq.
   /**
   */
   int numberOfChannels() const
   {
      return _numberOfChannels;
   }
//............................................................................
   list<int> getListOfSlots() const
   {
      list<int> l;
      for(int i=0;i<22*8;i++)
      {  
      	 int slot,fe;
	 slot=i/8;
	 fe=i%8;
      	 if(_slotfe[i]) l.push_back(slot);
      }
      
      //make list with unique elements   
      l.sort();
      l.unique();
   
      return l;
   }   
//............................................................................

   private:
   
   /// physical of readout channel.
   /**
   */
   EcalPhysicalPad _physical[22*8*12*18];
   
   /// readout of physical channel.
   /**
   */
   EcalReadoutPad _readout[18*18*30];
   
   /// physical of pcb channel.
   EcalPhysicalPad _physOfpcb[40*2*12*18];
   
   /// pcb channel of physical channel.
   EcalPcbPad _pcbpad[18*18*30];
      
   /// number of channels connected to the daq.
   /**
   */
   int _numberOfChannels;
   
   bool _slotfe[22*8];
   int  _slotfeNofChannels[22*8];

};//end class EcalMap

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
