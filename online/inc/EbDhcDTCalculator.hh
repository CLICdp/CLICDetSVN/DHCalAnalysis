//
// $Id: EbDhcDTCalculator.hh,v 0.1 2010/03/01 13:46:30 jacobsmith Exp $
//

#ifndef EbDhcDTCalculator_HH
#define EbDhcDTCalculator_HH

#include <string>
#include <iostream>
#include <utility>
#include <vector>

#include "DhcFeHitData.hh"

//declare class
class EbDhcDTCalculator {

//declare objects and data in public
public:
  
  EbDhcDTCalculator();

  int dt(unsigned start, unsigned end);

 private:

   int _range;
 
};  // end of class

#ifdef CALICE_DAQ_ICC

// define methods, data, strings, pairs
// see DhcFeHitData.hh for examples

EbDhcDTCalculator::EbDhcDTCalculator() {
  _range = 10000000;
}

int EbDhcDTCalculator::dt(unsigned start, unsigned end) {
    int dt = 0;
    if (start < end)
    {
//      if ((end - start) < _range*0.8)
      if (((int)end - (int)start) < ((int)start + _range - (int)end))
      {
        dt = ((int)end - (int)start);
      }
      else
      {
        dt = ((int)end - (int)start - _range);
      }
    }
    else
    {
//      if ((start - end) < _range*0.8)
      if (((int)start - (int)end) < ((int)end + _range - (int)start))
      {
        dt = ((int)end - (int)start);
      }
      else
      {
        dt = ((int)end + _range - (int)start);
      }
    }

    return dt;

}

#endif // CALICE_DAQ_ICC
#endif // EbDhcDTCalculator_HH
