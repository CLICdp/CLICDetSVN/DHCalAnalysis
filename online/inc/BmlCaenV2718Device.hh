#ifndef BmlCaenV2718Device_HH
#define BmlCaenV2718Device_HH

#include "CAENLinuxBusAdapter.hh"
#include "CAENVMElib.h"


class BmlCaenV2718Device {

public:
  BmlCaenV2718Device(int link, int device);
  virtual ~BmlCaenV2718Device();

  bool alive();
  bool pulseOutput(unsigned short mask);
  bool setOutput(unsigned short mask);
  bool clearOutput(unsigned short mask);

  void resetScaler(CVIOSources Hit);
  void resetStartSpill();
  void resetEndSpill();

  int readScaler();
  int readInput();
  bool inSpill();

private:
  int     _link;
  int     _device;
  int32_t _handle;
  bool    _spilling;

};

BmlCaenV2718Device::BmlCaenV2718Device(int link, int device) 
  : _link(link), _device(device), _handle(0)
{}

BmlCaenV2718Device::~BmlCaenV2718Device() {

  std::pair<int,int> cont(std::make_pair(_link,_device));
  if (HAL::CAENLinuxBusAdapter::_handleMap.find(cont) ==
      HAL::CAENLinuxBusAdapter::_handleMap.end())
    CAENVME_End(_handle);
}

bool
BmlCaenV2718Device::alive() {

  std::pair<int,int> cont(std::make_pair(_link,_device));
  if (HAL::CAENLinuxBusAdapter::_handleMap.find(cont) ==
      HAL::CAENLinuxBusAdapter::_handleMap.end())
    if ( CAENVME_Init(cvV2718, _link, _device, &_handle) != cvSuccess ) 
      return false;
  else
    _handle = HAL::CAENLinuxBusAdapter::_handleMap[cont];

  CVIOPolarity OutPol = cvDirect;
  CVLEDPolarity LEDPol = cvActiveHigh;
  CVIOSources Source = cvManualSW;

  CVOutputSelect OutSel = cvOutput0;
  CAENVME_SetOutputConf(_handle, OutSel, OutPol, LEDPol, Source);

  OutSel = cvOutput1;
  CAENVME_SetOutputConf(_handle, OutSel, OutPol, LEDPol, Source);

  return true;
}

bool
BmlCaenV2718Device::pulseOutput(unsigned short mask)
{
  CAENVME_PulseOutputRegister(_handle, mask);

  return true;
}

bool
BmlCaenV2718Device::setOutput(unsigned short mask)
{
  CAENVME_SetOutputRegister(_handle, mask);

  return true;
}

bool
BmlCaenV2718Device::clearOutput(unsigned short mask)
{
  CAENVME_ClearOutputRegister(_handle, mask);

  return true;
}

void
BmlCaenV2718Device::resetScaler(CVIOSources Hit)
{
  CAENVME_SetScalerConf(_handle, 1023, 1, Hit, cvManualSW, cvManualSW);

  CAENVME_ResetScalerCount(_handle);
  CAENVME_EnableScalerGate(_handle);
}

void
BmlCaenV2718Device::resetStartSpill()
{
  resetScaler(cvInputSrc0);
  _spilling = false;
}

void
BmlCaenV2718Device::resetEndSpill()
{
  resetScaler(cvCoincidence);
  _spilling = true;
}

int
BmlCaenV2718Device::readScaler() {

  unsigned int Data;
  CVRegisters Reg = cvScaler1;
  CAENVME_ReadRegister(_handle, Reg, &Data);

  return Data;
}

int
BmlCaenV2718Device::readInput() {

  unsigned int Data;
  CVRegisters Reg = cvInputReg;
  CAENVME_ReadRegister(_handle, Reg, &Data);

  return Data;
}

bool
BmlCaenV2718Device::inSpill() {

  return (readInput() > 0);
}

#endif // BmlCaenV2718Device_HH
