#ifndef DHCHIT_HH
#define DHCHIT_HH
#include "TObject.h"

#include <sstream>
#include <string>

class DhcHit : public TObject {

private:
    Int_t    Px;
    Int_t    Py;
    Int_t    Pz;
    Int_t    Ts;

public:
    DhcHit() {}
    DhcHit(Int_t x, Int_t y, Int_t z, Int_t s)
        : TObject()
        , Px(x)
        , Py(y)
        , Pz(z)
        , Ts(s) {

        }
    virtual ~DhcHit() {}

    Int_t     GetPx() const { return Px; }
    Int_t     GetPy() const { return Py; }
    Int_t     GetPz() const { return Pz; }
    Int_t     GetTs() const { return Ts; }
    inline int getPx() const { return Px; }
    inline int getPy() const { return Py; }
    inline int getPz() const { return Pz; }
    inline int getTs() const { return Ts; }

    std::string toString() const {
    	std::stringstream ss;
    	ss << "(" << Px << ", " << Py << ", " << Pz << ")";
    	return ss.str();
    }

    ClassDef(DhcHit,2)  //DhcHit structure
};

#endif
