//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>GMavromanolakis
#ifndef DATMGR_HH
#define DATMGR_HH

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cmath>
#include <cstring>
#include <unistd.h>
#include <vector>

using namespace std;

//////////////////////////////////////////////////////////////////////////////

/// Singleton class to manage calibration and mapping dat input files 
/** 
*/
class DatMgr
{
   public:
   
//............................................................................   
   ~DatMgr()
   {
   }
//............................................................................   
   DatMgr& getInstance()
   {  
      if(!_instance) 
      {  Init();
      }
      return *_instance;
   }
//............................................................................   
   string getHcalMapFile(int run)
   {
      return getFromIndex(run, "./data/mapping/HcalMapIndex.dat"); 
   }
//............................................................................   
   string getHcalPedFile(int run)
   {
      return getFromIndex(run, "./data/calibration/HcalPedIndex.dat"); 
   }
//............................................................................   
   string getEcalMapFile(int run, string dir="./")
   {
      return getFromIndex(run, dir+"./data/mapping/EcalMapIndex.dat"); 
   }
//............................................................................   
   string getTdcMapFile(int run)
   {
      return getFromIndex(run, "./data/mapping/TdcMapIndex.dat"); 
   }
//............................................................................   
   string getTdcMapFileCAEN(int run)
   {
      return getFromIndex(run, "./data/mapping/TdcMapIndexCAEN.dat"); 
   }
//............................................................................   
   string getVetoMapFile(int run)
   {
      return getFromIndex(run, "./data/mapping/VetoMapIndex.dat"); 
   }
//............................................................................   
   string getTriggerEventMapFile(int run)
   {
      return getFromIndex(run, "./data/mapping/TriggerEventMapIndex.dat"); 
   }
//............................................................................   
   string getHcalCalibrFile(int run)
   {
      return getFromIndex(run, "./data/calibration/HcalCalibrIndex.dat"); 
   }
//............................................................................   
   string getEcalCalibrFile(int run, string dir="./")
   {
      return getFromIndex(run, dir+"./data/calibration/EcalCalibrIndex.dat"); 
   }
//............................................................................
   string getTcmtLayersFile(int run)
   {
      return getFromIndex(run, "./data/mapping/tcmtIndexLayers.dat");
   }
//............................................................................
   string getTcmtChannelsFile(int run)
   {
      return getFromIndex(run, "./data/mapping/tcmtIndexChannels.dat");
   }
//............................................................................
   string getTcmtPedFile(int run)
   {
      return getFromIndex(run, "./data/calibration/TcmtPedIndex.dat"); 
   }
//............................................................................
   string getTcmtCalibrFile(int run)
   {
      return getFromIndex(run, "./data/calibration/TcmtCalibrIndex.dat"); 
   }
//............................................................................
   string getEcalScintLayersFile(int run)
   {
      return getFromIndex(run, "./data/mapping/EcalScintIndexLayers.dat");
   }
//............................................................................
   string getEcalScintChannelsFile(int run)
   {
      return getFromIndex(run, "./data/mapping/EcalScintIndexChannels.dat");
   }
//............................................................................
   string getEcalScintPedFile(int run)
   {
      return getFromIndex(run, "./data/calibration/EcalScintPedIndex.dat"); 
   }
//............................................................................
   string getEcalScintCalibrFile(int run)
   {
      return getFromIndex(run, "./data/calibration/EcalScintCalibrIndex.dat"); 
   }
//............................................................................   
   string getFromIndex(int runNum, std::string indexfileName)
   {
      std::cout << "read " << indexfileName << std::endl;
      std::ifstream input(indexfileName.c_str(),std::ios::in);
      if(!input) return "ERROR_CANNOT_FIND_"+indexfileName;
      
      //read the comments first, zzz strings are expected
      string comment;
      for(int i=0;i<2;i++)
      {
         input >> comment;
         cout << comment << endl;
      }
     
      vector<int> vecrun;
      vector<string> vecfil;
      int run;
      string filename;   

      while(input >> run >> filename )
      {
      	 vecrun.push_back(run);
	 vecfil.push_back(filename);
      }
      
      unsigned int pos=999999;
      int min=999999;
      for(unsigned int i=0;i<vecrun.size();i++)
      {
      	 int dif = runNum - vecrun[i];	 
      	 if(dif >=0 && dif <= min)
      	 {
      	    pos = i;
	    min = dif;
      	 }
      }

      if(pos < vecfil.size()) return vecfil[pos];
      else return "ERROR_RUN_IS_OUT_OF_RANGE_OF_"+indexfileName;

   }
//............................................................................   

   private:

   DatMgr() 
   {
   }

   void Init()
   {
      if(!_instance) _instance = new DatMgr();
   }
   
   static DatMgr* _instance;
   

};
DatMgr* DatMgr::_instance = 0;

#endif
//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
