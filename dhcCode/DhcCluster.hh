#ifndef DHCCLUSTER_HH
#define DHCCLUSTER_HH

#include "DhcHit.hh"
#include <string>
#include <set>

class DhcCluster {
public:
    DhcCluster() :
        _clx(0.), _cly(0.), _clz(0.), _maxX(0.), _maxY(0.), _maxZ(0.), _minX(0.), _minY(0.), _minZ(0.), _hitList(), _isCalculated(false)
    {}

    DhcCluster(const DhcCluster& cluster);

    void addHit(const DhcHit* hit);
    void mergeCluster(const DhcCluster& otherCluster);
    const std::set<const DhcHit*>& getHits() const;
    unsigned getSize() const;
    double getX() const;
    double getY() const;
    double getZ() const;
    int getMaxX() const;
    int getMaxY() const;
    int getMaxZ() const;
    int getMinX() const;
    int getMinY() const;
    int getMinZ() const;
    std::string toString() const;

protected:
    std::set<const DhcHit*>  _hitList;
    mutable bool _isCalculated;
    mutable double _clx;
    mutable double _cly;
    mutable double _clz;
    mutable int _maxX;
    mutable int _maxY;
    mutable int _maxZ;
    mutable int _minX;
    mutable int _minY;
    mutable int _minZ;
    
    void calculateProperties() const;
	
};

#endif
