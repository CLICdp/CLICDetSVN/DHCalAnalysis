#ifndef EVENT_HH
#define EVENT_HH

#include "TObject.h"
#include "TClonesArray.h"
#include "TBits.h"
#include "TTimeStamp.h"
#include "DhcCluster.hh"

#include <vector>
#include <list>
#include <map>


class EventHeader {

private:
    Int_t      fEvtNum;
    TTimeStamp fEvtTime;

public:
    EventHeader() : fEvtNum(0), fEvtTime(0) { }
    virtual ~EventHeader() { }
    void   Set(Int_t i, TTimeStamp& t) { fEvtNum = i; fEvtTime = t; }
    Int_t  GetEvtNum() const { return fEvtNum; }
    TTimeStamp* GetEvtTime() { return &fEvtTime; }
    ClassDef(EventHeader,1)  //Event Header
};


class Event : public TObject {
private:
    EventHeader    fEvtHdr;
    Int_t          nHits;            //Number of hits
    Int_t          nHitLayers;       //Number of hit layers
    Int_t          nMaxHitsPerLayer; //Maximum hit count in single layer
    TClonesArray  *fHits;            //->array with all hits
    TBits          fTriggerBits;     //Bits triggered by this event.
    static TClonesArray *fgHits;
    std::map<int, std::set<const DhcHit*> > layerHitsMap;
    std::map<int, std::vector<DhcCluster> > clusterList2D;
    std::vector<DhcCluster> clusterList3D;

public:
    Event();
    virtual ~Event() {}

    void          Clear(Option_t *option ="");

    std::map<int, std::set<const DhcHit*> > getLayerHitsMap() const;
    std::set<const DhcHit*> getHitsInLayer(int z) const;
    std::vector<DhcCluster> get2DClustersInLayer(int z) const;
    std::map<int, std::vector<DhcCluster> > get2DClusters(int minHits = 0) const;
    std::vector<DhcCluster> get3DClusters(int minHits = 0) const;

    EventHeader  *GetHeader() { return &fEvtHdr; }
    Int_t         GetNHits() const { return nHits; }
    Int_t         GetNHitLayers() const { return nHitLayers; }
    Int_t         GetMaxHitsPerLayer() const { return nMaxHitsPerLayer; }
    TClonesArray *GetHits() const {return fHits;}
    TBits&        GetTriggerBits() { return fTriggerBits; }
    // const std::list<std::vector<const DhcHit*> >& 
    void findClusters(int marginX = 10, int marginY = 10, int marginZ = 28);
    int getInteractionLayer(int minHits = 3);
    double getBaryCenterZ();
    double getDensity();
    void printClusters();
    ClassDef(Event,3)  //Event structure
};

#endif
