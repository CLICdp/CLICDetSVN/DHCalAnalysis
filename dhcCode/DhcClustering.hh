#ifndef DHCCLUSTERING_HH
#define DHCCLUSTERING_HH

#include "DhcHit.hh"
#include "DhcCluster.hh"
#include "Event.hh"

#include <map>
#include <vector>
#include <set>

namespace DhcClustering {
	bool compareHitsInZ(const DhcHit* h1, const DhcHit* h2);
	std::vector <DhcCluster> find3DClusters(const std::map<int, std::vector<DhcCluster> >& clusterMap2D, int marginX = 10, int marginY = 10, int marginZ = 28);
	std::map<int, std::vector<DhcCluster> > findAllLayerClusters(const Event* event, int marginX = 10, int marginY = 10);
	std::vector<DhcCluster> findClustersXY(const std::set<const DhcHit*>& hits, int marginX = 10, int marginY = 10);
	std::map<int, std::set<const DhcHit*> > sortHitsInZ(const std::set<const DhcHit*>& hits);
	
	bool consistentInX(const DhcHit& h1, const DhcHit& h2, int margin = 10);
	bool consistentInY(const DhcHit& h1, const DhcHit& h2, int margin = 10);
	bool consistentInZ(const DhcHit& h1, const DhcHit& h2, int margin = 28);

	bool consistentInX(const DhcCluster& c1, const DhcCluster& c2, int margin = 10);
	bool consistentInY(const DhcCluster& c1, const DhcCluster& c2, int margin = 10);
	bool consistentInZ(const DhcCluster& c1, const DhcCluster& c2, int margin = 28);
}
#endif
