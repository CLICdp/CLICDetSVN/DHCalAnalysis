//
// $Id: DhcConfiguration.hh,v 1.12 2008/04/01 17:58:24 jls Exp $
//

#ifndef DhcConfiguration_HH
#define DhcConfiguration_HH

#include <iostream>
#include <fstream>

// dual/inc/daq
#include "DaqRunStart.hh"
#include "DaqConfigurationStart.hh"
#include "DaqSpillStart.hh"
#include "DaqRunType.hh"

// dual/inc/rcd
#include "RcdUserRW.hh"
#include "RcdArena.hh"
#include "RcdReaderAsc.hh"

// dual/inc/sub
#include "SubInserter.hh"
#include "SubAccessor.hh"
#include "SubModifier.hh"

#include "DhcLocation.hh"
#include "DhcBeConfigurationData.hh"
#include "DhcDcConfigurationData.hh"
#include "DhcFeConfigurationData.hh"
#include "DhcReadoutConfigurationData.hh"
#include "DhcTriggerConfigurationData.hh"

#include "TtmLocation.hh"
#include "TtmLocationData.hh"
#include "TtmConfigurationData.hh"


class DhcConfiguration : public RcdUserRW {
public:
  DhcConfiguration()
    : _runType(), _configurationNumber(0) {
  }

  virtual ~DhcConfiguration() {
  }

  virtual bool record(RcdRecord &r) {
    if(doPrint(r.recordType())) {
      std::cout << "DhcConfiguration::record()" << std::endl;
      r.RcdHeader::print(std::cout," ") << std::endl;
    }

    // Check record type
    switch (r.recordType()) {

      // Run start 
    case RcdHeader::runStart: {

      // Access the DaqRunStart
      SubAccessor accessor(r);
      std::vector<const DaqRunStart*>
	v(accessor.extract<DaqRunStart>());
      assert(v.size()==1);

      _runType=v[0]->runType();

      switch(_runType.type()) {

      case DaqRunType::dhcNoise: {
	_configType = "noise";
	break;
      }

      case DaqRunType::dhcQinj:
      case DaqRunType::dhcQinjScan: {
	_configType = "qinj";
	break;
      }

      case DaqRunType::dhcCosmics: {
	_configType = "cosmic";
	break;
      }

      case DaqRunType::dhcBeam:
      case DaqRunType::beamData: {
	_configType = "beam";
	break;
      }

      default: {
	_configType = "generic";
	break;
      }
      };

      break;
    }

    // Configuration start is used to set up system
    case RcdHeader::configurationStart: {

      // Access the DaqConfigurationStart
      SubAccessor accessor(r);
      std::vector<const DaqConfigurationStart*>
	v(accessor.extract<DaqConfigurationStart>());
      assert(v.size()==1);
      if(doPrint(r.recordType(),1)) v[0]->print(std::cout," ") << std::endl;

      switch(_runType.majorType()) {

      case DaqRunType::slow:
      case DaqRunType::beam:
      case DaqRunType::dhc: {

	_configurationNumber=v[0]->configurationNumberInRun();

	RcdArena *p=new RcdArena;
	RcdArena &c(*p);
	RcdReaderAsc reader;

	reader.printLevel(0);
	assert(reader.open("cfg/dhcal/"+_configType+"/dhcConfig"));
	assert(reader.read(c));

	// Do Trg
	trgConfiguration(c,r);

	// Do BE 
	beConfiguration(c,r);

	// Do DC 
	dcConfiguration(c,r);

	// Do FE
	feConfiguration(c,r);

	// Do readout
	readoutConfiguration(c,r);

	assert(reader.close());

	delete p;
	break;
      }

      default: {
	break;
      }

      }; // switch(type)
      break;
    }

      // Run end
    case RcdHeader::runEnd: {
      _configurationNumber=0;
      break;
    }

    default: {
      break;
    }
    };

    return true;
  }

  bool trgConfiguration(RcdRecord& c, RcdRecord &r) {

    SubModifier modifier(c);
    // vectors for configuration data
    std::vector< DhcTriggerConfigurationData* >
      vTcd(modifier.access<DhcTriggerConfigurationData>());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of DhcTriggerConfigurationData subrecords inserted = "
      << vTcd.size() << std::endl << std::endl;

    // Load configuration into record
    SubInserter inserter(r);
    
    for(unsigned i(0);i<vTcd.size();i++) {

      switch(_runType.type()) {

      case DaqRunType::dhcNoise: {
	vTcd[i]->triggerByWaiting(true);
	break;
      }

      case DaqRunType::dhcQinj:
      case DaqRunType::dhcQinjScan: {
	vTcd[i]->triggerInternally(true);
	break;
      }

      case DaqRunType::dhcCosmics: {
	break;
      }

      default: {
	break;
      }
      }; // switch(type)

      inserter.insert< DhcTriggerConfigurationData >(*vTcd[i]);
      if(doPrint(r.recordType(),1)) vTcd[i]->print(std::cout) << std::endl;
    }

    std::vector< TtmLocationData<TtmConfigurationData>* >
      vTtd(modifier.access< TtmLocationData<TtmConfigurationData> >());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of TtmConfigurationData subrecords inserted = "
      << vTtd.size() << std::endl << std::endl;
    
    for(unsigned i(0);i<vTtd.size();i++) {

      switch(_runType.type()) {

      case DaqRunType::dhcNoise: {
	vTtd[i]->data()->csRamSignal(1);
	vTtd[i]->data()->csInternalReset(0);
	vTtd[i]->data()->csInternalTcal(0);
	vTtd[i]->data()->csInternalTrigger(1);
	break;
      }

      case DaqRunType::dhcQinj:
      case DaqRunType::dhcQinjScan: {
	vTtd[i]->data()->csRamSignal(0);
	vTtd[i]->data()->csInternalReset(0);
	vTtd[i]->data()->csInternalTcal(1);
	vTtd[i]->data()->csInternalTrigger(1);
	vTtd[i]->data()->ramDepth(8);
	vTtd[i]->data()->ramLoad(4);
	break;
      }

      case DaqRunType::dhcCosmics: {
	vTtd[i]->data()->csRamSignal(0);
	vTtd[i]->data()->csInternalReset(0);
	vTtd[i]->data()->csInternalTcal(0);
	// if not using external trigger, i.e. triggerByWaiting, set TTM to internal trigger
	vTtd[i]->data()->csInternalTrigger(vTcd[0]->triggerByWaiting());
	break;
      }

      case DaqRunType::dhcBeam:
      case DaqRunType::beamData: {
	vTtd[i]->data()->csRamSignal(0);
	vTtd[i]->data()->csInternalReset(0);
	vTtd[i]->data()->csInternalTcal(0);
	vTtd[i]->data()->csInternalTrigger(0);
	break;
      }

      default: {
	break;
      }
      }; // switch(type)

      inserter.insert< TtmLocationData<TtmConfigurationData> >(vTtd[i]);
      if(doPrint(r.recordType(),1)) vTtd[i]->print(std::cout) << std::endl;
    }

    return true;
  }

  bool beConfiguration(RcdRecord& c, RcdRecord &r) {

    SubModifier modifier(c);
    // vector for configuration data
    std::vector< DhcLocationData<DhcBeConfigurationData>* >
      vBcd(modifier.access< DhcLocationData<DhcBeConfigurationData> >());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of DhcBeConfigurationData subrecords inserted = "
      << vBcd.size() << std::endl << std::endl;
    
    // Load configuration into record
    SubInserter inserter(r);
    
    for(unsigned i(0);i<vBcd.size();i++) {

      inserter.insert< DhcLocationData<DhcBeConfigurationData> >(*vBcd[i]);
      if(doPrint(r.recordType(),1)) vBcd[i]->print(std::cout) << std::endl;
    }

    return true;
  }

  bool dcConfiguration(RcdRecord& c, RcdRecord &r) {

    SubModifier modifier(c);
    // vector for configuration data
    std::vector< DhcLocationData<DhcDcConfigurationData>* >
      vDcd(modifier.access< DhcLocationData<DhcDcConfigurationData> >());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of DhcDcConfigurationData subrecords inserted = "
      << vDcd.size() << std::endl << std::endl;
    
    // Load configuration into record
    SubInserter inserter(r);
    
    for(unsigned i(0);i<vDcd.size();i++) {

      inserter.insert< DhcLocationData<DhcDcConfigurationData> >(*vDcd[i]);
      if(doPrint(r.recordType(),1)) vDcd[i]->print(std::cout) << std::endl;
    }

    return true;
  }

  bool feConfiguration(RcdRecord& c, RcdRecord &r) {

    SubModifier modifier(c);
    // vector for configuration data
    std::vector< DhcLocationData<DhcFeConfigurationData>* >
      vFcd(modifier.access< DhcLocationData<DhcFeConfigurationData> >());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of DhcFeConfigurationData subrecords inserted = "
      << vFcd.size() << std::endl << std::endl;
    
    // Load configuration into record
    SubInserter inserter(r);
    
    const unsigned char v(_runType.version());
    const UtlPack u(_runType.version());

    for(unsigned i(0);i<vFcd.size();i++) {

      switch(_runType.type()) {

      case DaqRunType::dhcNoise: {
	vFcd[i]->data()->dcrExternalTrigger(0);
	break;
      }

      case DaqRunType::dhcQinj: {
	vFcd[i]->data()->vtpd(v);
	vFcd[i]->data()->dcrExternalTrigger(1);

	unsigned char inj[8];
	for (unsigned j(0); j<sizeof(inj); j++)
	  inj[j] = 0x11<<_configurationNumber;
	vFcd[i]->data()->inj(inj);

	break;
      }

      case DaqRunType::dhcQinjScan: {
	vFcd[i]->data()->dcrExternalTrigger(1);

	unsigned char inj[8];
	for (unsigned j(0); j<sizeof(inj); j++)
	  inj[j] = 0x11<<(_configurationNumber%4);
	vFcd[i]->data()->inj(inj);

	vFcd[i]->data()->vtpd(*vFcd[i]->data()->vtpd() +
			     u.bits(0,3)*(_configurationNumber/4));
	break;
      }

      case DaqRunType::dhcCosmics: {
	break;
      }

      default: {
	break;
      }
      }; // switch(type)
      
      inserter.insert< DhcLocationData<DhcFeConfigurationData> >(*vFcd[i]);
      if(doPrint(r.recordType(),1)) vFcd[i]->print(std::cout) << std::endl;
    }

    return true;
  }

  bool readoutConfiguration(RcdRecord& c, RcdRecord &r) {

    SubModifier modifier(c);
    std::vector< DhcReadoutConfigurationData* >
      vRcd(modifier.access<DhcReadoutConfigurationData>());

    if(doPrint(r.recordType(),1)) std::cout 
      << " Number of DhcReadoutConfigurationData subrecords inserted = "
      << vRcd.size() << std::endl << std::endl;
    
    SubInserter inserter(r);
    for(unsigned i(0);i<vRcd.size();i++) {

      inserter.insert< DhcReadoutConfigurationData >(*vRcd[i]);
      if(doPrint(r.recordType(),1)) vRcd[i]->print(std::cout) << std::endl;
    }

    return true;
  }

protected:

  DaqRunType _runType;
  unsigned _configurationNumber;
  std::string _configType;
};


#endif // DhcConfiguration_HH
