#include "DhcCluster.hh"

#include <limits>
#include <sstream>

using std::numeric_limits;
using std::string;
using std::stringstream;

DhcCluster::DhcCluster(const DhcCluster& cluster) {
	_hitList.insert(cluster.getHits().begin(), cluster.getHits().end());
	_isCalculated = false;
}

void DhcCluster::addHit(const DhcHit* hit) {
	_hitList.insert(hit);
	_isCalculated = false;
}

void DhcCluster::mergeCluster(const DhcCluster& otherCluster) {
	std::set<const DhcHit*>::const_iterator otherHits_it = otherCluster.getHits().begin();
	while (otherHits_it != otherCluster.getHits().end()) {
		addHit(*otherHits_it);
		otherHits_it++;
	}
}

const std::set<const DhcHit*>& DhcCluster::getHits() const {
	return _hitList;
}

double DhcCluster::getX() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _clx;
}

double DhcCluster::getY() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _cly;
}

double DhcCluster::getZ() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _clz;
}

int DhcCluster::getMaxX() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _maxX;
}

int DhcCluster::getMaxY() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _maxY;
}

int DhcCluster::getMaxZ() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _maxZ;
}

int DhcCluster::getMinX() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _minX;
}

int DhcCluster::getMinY() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _minY;
}

int DhcCluster::getMinZ() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	return _minZ;
}

unsigned DhcCluster::getSize() const {
	return _hitList.size();
}

string DhcCluster::toString() const {
	if (!_isCalculated) {
		calculateProperties();
	}
	stringstream ss;
	ss << "(" << _clx << "," << _cly << "," << _clz << "),\t" << getSize() << " hits,\t";
	ss << "([" << _minX << "," << _maxX << "],\t[" << _minY << "," << _maxY << "],\t[" << _minZ << "," << _maxZ <<"])";
	return ss.str();
}



// Calculates the cluster mean, the cluster extent in x, y and z, and the principle components/
void DhcCluster::calculateProperties() const {
	// reset values
	double sumX(0.), sumY(0.), sumZ(0.);
	_minX = numeric_limits<int>::max();
	_minY = numeric_limits<int>::max();
	_minZ = numeric_limits<int>::max();
	_maxX = numeric_limits<int>::min();
	_maxY = numeric_limits<int>::min();
	_maxZ = numeric_limits<int>::min();

	// loop over all hits and calculate
	std::set<const DhcHit*>::const_iterator hit_it = _hitList.begin();
	for (; hit_it != _hitList.end(); hit_it++) {
		const DhcHit* hit = *hit_it;
		int x = hit->getPx();
		int y = hit->getPy();
		int z = hit->getPz();
		// Check if the hit is outside the current cluster extent
		if (x < _minX) _minX = x;
		if (y < _minY) _minY = y;
		if (z < _minZ) _minZ = z;
		if (x > _maxX) _maxX = x;
		if (y > _maxY) _maxY = y;
		if (z > _maxZ) _maxZ = z;
		sumX += x;
		sumY += y;
		sumZ += z;
	}

	int nHits = _hitList.size();
	_clx = sumX / nHits;
	_cly = sumY / nHits;
	_clz = sumZ / nHits;
	_isCalculated = true;
}
