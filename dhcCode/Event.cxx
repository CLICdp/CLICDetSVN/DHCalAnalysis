#include "Event.hh"
#include "DhcClustering.hh"

#include <cmath>
#include <iostream>
#include <algorithm>

using namespace std;
using DhcClustering::compareHitsInZ; 
TClonesArray *Event::fgHits = 0;

Event::Event() {
    if (!fgHits)
        fgHits = new TClonesArray("DhcHit", 100000);
    fHits = fgHits;
}

void Event::Clear(Option_t* option) {
    clusterList2D.clear();
    clusterList3D.clear();
    fHits->Clear("C");
}

map<int, set<const DhcHit*> > Event::getLayerHitsMap() const {
	return layerHitsMap;
}

set<const DhcHit*> Event::getHitsInLayer(int z) const {
	map<int, set<const DhcHit*> >::const_iterator entry = layerHitsMap.find(z);
	if (entry == layerHitsMap.end()) {
		return set<const DhcHit*>();
	}
	return entry->second;
}

vector<DhcCluster> Event::get2DClustersInLayer(int z) const {
	map<int, vector<DhcCluster> >::const_iterator entry = clusterList2D.find(z);
	if (entry == clusterList2D.end()) {
		return vector<DhcCluster>();
	}
	return entry->second;
}

map<int, vector<DhcCluster> > Event::get2DClusters(int minHits) const {
    if (minHits < 2){
    	return clusterList2D;
    }
    map<int, vector<DhcCluster> > reducedClusterMap;
    map<int, vector<DhcCluster> >::const_iterator map_it = clusterList2D.begin();
    for ( ; map_it != clusterList2D.end(); map_it++) {
    	int z = map_it->first;
    	const vector<DhcCluster>& layerClusters = map_it->second;
    	vector<DhcCluster> reducedLayerClusters;
    	vector<DhcCluster>::const_iterator cluster_it = layerClusters.begin();
    	for ( ; cluster_it != layerClusters.end(); cluster_it++) {
    		if (cluster_it->getSize() >= minHits) {
    			reducedLayerClusters.push_back(*cluster_it);
    		}
    	}
    	reducedClusterMap.insert(make_pair(z, reducedLayerClusters));
    }
    return reducedClusterMap;
}

vector<DhcCluster> Event::get3DClusters(int minHits) const {
	if (minHits < 2) {
		return clusterList3D;
	}
	vector<DhcCluster> reducedClusterList;
	vector<DhcCluster>::const_iterator cluster_it = clusterList3D.begin();
	for ( ; cluster_it != clusterList3D.end(); cluster_it++) {
		if (cluster_it->getSize() >= minHits) {
			reducedClusterList.push_back(*cluster_it);
		}
	}
	return reducedClusterList;
}

void Event::printClusters() {
    map<int, vector<DhcCluster> >::const_iterator map_it = clusterList2D.begin();
    cout << "2D Clusters:" << endl;
    for (; map_it != clusterList2D.end(); ++map_it) {
    	int z = map_it->first;
    	vector<DhcCluster> clusters = map_it->second;
    	cout << "\tz: " << z << ", clusters: " << clusters.size() << endl;
        vector<DhcCluster>::const_iterator cluster_it = clusters.begin();
        for (; cluster_it != clusters.end(); ++cluster_it) {
            cout << "\t\t" << cluster_it->toString() << endl;
        }
    }
    cout << "3D Clusters:" << endl;
    vector<DhcCluster>::const_iterator cluster_it = clusterList3D.begin();
    for (; cluster_it != clusterList3D.end(); ++cluster_it) {
        cout << "\t" << cluster_it->toString() << endl;
    }
}

// Sorts the hits in the event by z;
// Clusters the hits in an event. Simple NN clusterer.
// const list<vector<const DhcHit*> >&
void Event::findClusters(int marginX, int marginY, int marginZ) {
	if (layerHitsMap.empty()) {
		set<const DhcHit*> hits;
		for (int iHit = 0; iHit < nHits; ++iHit) {
			const DhcHit* hit = dynamic_cast<const DhcHit*>(fHits->At(iHit));
			hits.insert(hit);
		}
		layerHitsMap = DhcClustering::sortHitsInZ(hits);
	}
	clusterList2D = DhcClustering::findAllLayerClusters(this, marginX, marginY);
	clusterList3D = DhcClustering::find3DClusters(clusterList2D, marginX, marginY, marginZ);
}

int Event::getInteractionLayer(int minHits) {
	map<int, set<const DhcHit*> >::const_iterator entry = layerHitsMap.begin();
	for (; entry != layerHitsMap.end(); ++entry) {
		if (entry->second.size() >= minHits) {
			return entry->first;
		}
	}
	return 99999;
}

double Event::getBaryCenterZ() {
	map<int, set<const DhcHit*> >::const_iterator entry = layerHitsMap.begin();
	double zSum = 0.;
	for (; entry != layerHitsMap.end(); ++entry) {
		zSum += (double) (entry->first * entry->second.size());
	}
	return zSum / (double) nHits;
}

double Event::getDensity() {
	return (double) nHits / (double) nHitLayers;
}
