#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all namespaces;
#pragma link C++ nestedclasses;

#pragma link C++ namespace DhcClustering;

#include <set>
#include <map>
#include <list>
#pragma link C++ class DhcHit+;
#pragma link C++ class std::vector<DhcHit*>+;
#pragma link C++ class std::set<DhcHit*>+;
#pragma link C++ class std::vector<const DhcHit*>+;
#pragma link C++ class list<vector<const DhcHit*> >+;
#pragma link C++ class DhcCluster+;
#pragma link C++ class vector<DhcCluster>+;
#pragma link C++ class map<int, vector<DhcCluster> >+;
#pragma link C++ class map<int, vector<DhcCluster> >::iterator+;
#pragma link C++ class map<int, vector<DhcCluster> >::const_iterator+;
#pragma link C++ class pair<int, vector<DhcCluster> >+;
#pragma link C++ class EventHeader+;
#pragma link C++ class Event+;
#endif
