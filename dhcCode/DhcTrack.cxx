// 
// $Id$
// 
#include <set>
#include <map>
#include <vector>
#include <cmath>
#include <algorithm>

#include "DhcTree.hh"
#include "DhcTrack.hh"

const double CLUSTER_RADIUS = 15;

LineFitter::~LineFitter() {
	std::set<ClusterPt *>::const_iterator ip(_points.begin());
	for (; ip != _points.end(); ip++)
		delete *ip;
}

void LineFitter::addPoint(ClusterPt * p) {
	_points.insert(p);
}

bool LineFitter::fitPoints() {
	bool status(false);
	if (_points.size() > 1) {

		double ssxx(0);
		double ssyy(0);
		double ssxy(0);
		double avx(0);
		double avy(0);
		double n(_points.size());

		std::set<ClusterPt *>::const_iterator ip(_points.begin());
		for (; ip != _points.end(); ip++) {
			avx += (*ip)->first;
			avy += (*ip)->second;
		}
		avx = avx / n;
		avy = avy / n;

		for (ip = _points.begin(); ip != _points.end(); ip++) {
			ssxx += ((*ip)->first - avx) * ((*ip)->first - avx);
			ssyy += ((*ip)->second - avy) * ((*ip)->second - avy);
			ssxy += ((*ip)->first - avx) * ((*ip)->second - avy);
		}

		if (ssxy == 0) {
			slope = 1e6;
			intercept = -avx * slope;
			residual = 0;
		} else {
			slope = ssxy / ssxx;
			intercept = avy - slope * avx;
			residual = ssxy * ssxy / ssxx / ssyy;
		}

		chi2 = 0;
		for (ip = _points.begin(); ip != _points.end(); ip++)
			chi2 += pow(((*ip)->first - (((*ip)->second - intercept) / slope)), 2);
		chi2 /= 25 * (_points.size() - 2);

		status = true;
	}
	return status;
}

const std::set<DhcHit *>& DhcCluster::getHits() const {
	return _hitList;
}
void DhcCluster::addHit(DhcHit * hit) {
	_hitList.insert(hit);
}

const unsigned DhcCluster::getSize() const {
	return _hitList.size();
}
const double DhcCluster::getX() {
	if (isnan(_clx)) {
		if (_hitList.size()) {
			_clx = 0;
			std::set<DhcHit *>::const_iterator ip(_hitList.begin());
			for (; ip != _hitList.end(); ip++)
				_clx += (*ip)->GetPx();
			_clx /= _hitList.size();
		}
		return _clx;
	} else
		return _clx;
}

const double DhcCluster::getY() {
	if (isnan(_cly)) {
		if (_hitList.size()) {
			_cly = 0;
			std::set<DhcHit *>::const_iterator ip(_hitList.begin());
			for (; ip != _hitList.end(); ip++)
				_cly += (*ip)->GetPy();
			_cly /= _hitList.size();
		}
		return _cly;
	} else
		return _cly;
}

const double DhcCluster::getZ() {
	if (isnan(_clz)) {
		if (_hitList.size()) {
			_clz = 0;
			std::set<DhcHit *>::const_iterator ip(_hitList.begin());
			for (; ip != _hitList.end(); ip++)
				_clz += (*ip)->GetPz();
			_clz /= _hitList.size();
		}
		return _clz;
	} else
		return _clz;
}

const std::set<DhcHit *>& DhcTrack::getHits() const {
	return _hitList;
}
void DhcTrack::addHit(DhcHit * hit) {
	_hitList.insert(hit);
}

void DhcTrack::clear() {
	_hitList.clear();
}

const unsigned DhcTrack::getNLayers() const {
	std::set<DhcHit *>::const_iterator ip(_hitList.begin());
	std::vector<int> pz;
	std::vector<int>::const_iterator ipz;

	for (; ip != _hitList.end(); ip++) {
		ipz = find(pz.begin(), pz.end(), (*ip)->GetPz());
		if (ipz == pz.end())
			pz.push_back((*ip)->GetPz());
	}
	return pz.size();
}

const unsigned DhcTrack::getLayerNHits(int Pz) const {
	unsigned nHits(0);

	std::set<DhcHit *>::const_iterator ip(_hitList.begin());
	for (; ip != _hitList.end(); ip++) {
		if ((*ip)->GetPz() == Pz)
			nHits++;
	}
	return nHits;

}

std::set<DhcHit *>*DhcTrack::getLayerHits(int Pz) {
	std::set<DhcHit *>*hits = new std::set<DhcHit *>;
	std::set<DhcHit *>::const_iterator ip(_hitList.begin());
	for (; ip != _hitList.end(); ip++) {
		if ((*ip)->GetPz() == Pz)
			hits->insert(*ip);
	}
	return hits;
}

std::set<DhcCluster *>*DhcTrack::getLayerClusters(int Pz) {
	std::set<DhcHit *>*LayerHits = getLayerHits(Pz);
	std::set<DhcCluster *>*LayerClusters = new std::set<DhcCluster *>;

	while (LayerHits->size() > 0) {
		DhcCluster *cls = new DhcCluster(floor(Pz));
		int nfound = 1;

		while (nfound > 0) {
			nfound = 0;

			std::set<DhcHit *>::const_iterator ip(LayerHits->begin());
			for (; ip != LayerHits->end(); ip++) {
				if (cls->getSize() == 0) {
					cls->addHit(*ip);
					nfound += 1;
				} else {
					int flg = 0;
					std::set<DhcHit *> pts = cls->getHits();
					std::set<DhcHit *>::const_iterator jp(pts.begin());
					for (; jp != pts.end(); jp++) {
						// without corner touching hits
						if (sqrt(
								((*ip)->GetPx() - (*jp)->GetPx()) * ((*ip)->GetPx() - (*jp)->GetPx())
										+ ((*ip)->GetPy() - (*jp)->GetPy()) * ((*ip)->GetPy() - (*jp)->GetPy()))
								< CLUSTER_RADIUS)
							flg = 1;
					}

					if (flg > 0) {
						cls->addHit(*ip);
						nfound += 1;
					}
				}
			}

			std::set<DhcHit *> pts = cls->getHits();
			std::set<DhcHit *>::const_iterator jp(pts.begin());
			for (; jp != pts.end(); jp++) {
				LayerHits->erase(*jp);
			}
		}

		LayerClusters->insert(cls);

	}

	delete LayerHits;
	return LayerClusters;
}
