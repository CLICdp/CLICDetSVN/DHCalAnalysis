#!/bin/bash
#BSUB -J singleParticles[1-1]
#BSUB -q 1nh
#BSUB -o /afs/cern.ch/user/j/jfstrube/work/public/DHCAL/stdout.%J_%I
#BSUB -e /afs/cern.ch/user/j/jfstrube/work/public/DHCAL/stderr.%J_%I
#BSUB -R "type==SLC5_64"

source /afs/cern.ch/eng/clic/software/x86_64-slc5-gcc41/Calice/devel/envCalice.sh
source /afs/cern.ch/sw/lcg/contrib/gcc/4.7.2/x86_64-slc5-gcc47-opt/setup.sh

export MARLIN_DLL=/afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DHCalAnalysis/DHCalMarlin/lib/libCaliceDhcal.so:${MARLIN_DLL}
mkdir LSB_${LSB_JOBID}_${LSB_JOBINDEX}
cd LSB_${LSB_JOBID}_${LSB_JOBINDEX}
contents=($(cat /afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DHCalAnalysis/DHCalMarlin/steer/filelist.txt))
filename=${contents[${LSB_JOBINDEX} - 1]}
xrdcp root://castorpublic.cern.ch//castor/cern.ch/grid/ilc/user/c/cgrefe/calice/dhcal/lcio/Conversion-2013_02_18/${filename} -OSstagerHost=castorpublic\&svcClass=ilcdata -s .
cp /afs/cern.ch/user/j/jfstrube/work/public/DHCAL/DHCalAnalysis/DHCalMarlin/steer/BoxFinding.xml .
Marlin --global.LCIOInputFiles="${filename}" BoxFinding.xml
