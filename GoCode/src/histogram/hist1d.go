package histogram

import (
	"html/template"
	"os"
)

type Hist1D struct {
	xMin float64
	xMax float64
	step float64
	BinEntries []int
	BinLabels []float64
}

const representation = `
<!--
You are free to copy and use this sample in accordance with the terms of the
Apache license (http://www.apache.org/licenses/LICENSE-2.0.html)
-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <title>
      Google Visualization API Sample
    </title>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load('visualization', '1', {packages: ['corechart']});
    </script>
    <script type="text/javascript">
      function drawVisualization() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'xlabels');
        data.addColumn('number', 'nHits');
        data.addRows([ {{range $idx, $element := $.BinEntries}}{{if $idx}},{{end}}
        	['{{index $.BinLabels $idx | printf "%.2f"}}', {{$element}}] {{end}}
        ]);
      
        var options = {
          width: 600, height: 400,
          title: 'Number of hits / event',
          vAxis: {title: 'yTitle'},
          hAxis: {title: 'xTitle'},
          isStacked: true
        };
      
        var chart = new google.visualization.SteppedAreaChart(document.getElementById('visualization'));
        chart.draw(data, options);
      }
      

      google.setOnLoadCallback(drawVisualization);
    </script>
  </head>
  <body style="font-family: Arial;border: 0 none;">
    <div id="visualization" style="width: 600px; height: 400px;"></div>
  </body>
</html>
​`

func NewHist1D(min, max float64, nbins int) *Hist1D {
	binList := make([]int, nbins, nbins)
	// weights := make([]float64, nbins, nbins)
	stepsize := (max - min) / float64(nbins)
	binLabels := make([]float64, nbins, nbins)
	for x := range binLabels {
		binLabels[x] = min + (float64(x) + 0.5) * stepsize
	}
	hist := Hist1D{min, max, stepsize, binList, binLabels}
	return &hist
}


func (h *Hist1D) Fill(value float64) bool {
	if value < h.xMin || value > h.xMax {
		return false
	}
	for bin := range h.BinEntries {
		if value < h.xMin + float64(bin + 1) * h.step {
			h.BinEntries[bin] += 1
			break
		}
	}
	return true
}

func (h *Hist1D) ToFile(filename string) string {
	templ := template.Must(template.New("hist1d").Parse(representation))
	outf, _ := os.Create(filename)
	templ.Execute(outf, *h)
	return representation
}