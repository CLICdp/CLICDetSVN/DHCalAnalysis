package main
import (
    "fmt"
    "bufio"
    "os"
    "flag"
    "strings"
    "strconv"
    "sort"
    . "math"
    "histogram"
    "path/filepath"
    "runtime/pprof"
)
var memprof *string = flag.String("memprof", "", "name of the memprof output file")

type Point struct {
    x, y, z int
}

// we define an event as a set of points
// this allows us to define some methods on it
// for example, to allow sorting
type Event []Point

func (points Event) Len() int {
    return len(points)
}

// we want to sort the points in z
func (points Event) Less(i, j int) bool {
    return points[i].z < points[j].z
}

func (points Event) Swap(i, j int) {
    points[i], points[j] = points[j], points[i]
}


func addPoint(fields []string, events map[int]Event) *Point {
    trigger, errt := strconv.Atoi(fields[0])
    if errt != nil {
        fmt.Println("Could not encode Trigger", fields[0])
        return nil
    }
    x, errT := strconv.Atoi(fields[1])
    if errT != nil {
        fmt.Println("Could not encode x", fields[1])
        return nil
    }
    if x < 0 {
//        fmt.Println("Empty event", x)
        return nil
    }
    y, errT := strconv.Atoi(fields[2])
    if errT != nil {
        fmt.Println("Could not encode y", fields[2])
        return nil
    }
    z, errT := strconv.Atoi(fields[3])
    if errT != nil {
        fmt.Println("Could not encode z", fields[3])
        return nil
    }
    p := Point {x, y, z}
    // Let's check if the event exists, or at least if trigger +- 2 exists
    eventExists := false
    // since we're checking the neighbors, we need to keep track of which neighbor we're merging this point with
    eventID := trigger
    for i := trigger - 2; i <= trigger + 2; i++ {
        _, isPresent := events[i]
        if isPresent {
            eventID = i
            eventExists = true
            break
        }
    }
    // if we haven't encountered this event, yet, allocate some space for hits
    if (!eventExists) {
        events[eventID] = make([]Point, 0, 10)
    }
    events[eventID] = append(events[eventID], p)
    return &p
}


func EuclidianDistance(p1, p2 Point) float64 {
    return Sqrt(Pow(float64(p1.x - p2.x), 2) + Pow(float64(p1.y - p2.y), 2) + Pow(float64(p1.z - p2.z), 2))
}


func xyDist(p1, p2 Point) float64 {
    return Sqrt(Pow(float64(p1.x - p2.x), 2) + Pow(float64(p1.y - p2.y), 2))   
}

func IAbs(x int) int {
    if x < 0 {
        return -x
    }
    return x
}


// The clusterer splits the event into subevents
func clusterEventHits(event Event) []Event {
    sort.Sort(event)
    clusters := make([]Event, 0, 5)
    // When an invalid z position is encountered, start a new slice in z
    currentZ := -100
    var zSlice Event
    for _, point := range event {
        if currentZ < 0 {
            zSlice = make(Event, 0, 20)
            currentZ = point.z
        }
        if point.z == currentZ {
            zSlice = append(zSlice, point)
            // FIXME For now remove events with noisy boards (many hits with same z)
            if len(zSlice) > 10 {
                return make([]Event, 0, 0)
            }
        } else {
            currentZ = -100
        }
        // loop over all points in the cluster and see if there's one that's close enough to put the current point in
        // if not, this point is a new seed
        // we make use of the fact that everything is sorted in z. We don't need to look deeper than 3 in z.
        pointHasBeenAdded := false
        for clusterID, c := range clusters {
            for i := len(c)-1; i >= 0; i-- {
                clusterPoint := c[i]
                if IAbs(clusterPoint.z - point.z) > 3 {
                    break
                }
                xyDistance := xyDist(clusterPoint, point)
                // fmt.Println(distance)
                if xyDistance <= 2 && IAbs(clusterPoint.z - point.z) <= 3 {
                    // fmt.Println("Point has been added", point, clusterPoint)
                    clusters[clusterID] = append(clusters[clusterID], point)
                    pointHasBeenAdded = true
                    break
                }
            }
        // if the point has been added to an existing cluster, we're done.
        // Otherwise, it'll be a seed of a new cluster
            if pointHasBeenAdded {
                break
            }
        }
        if !pointHasBeenAdded {
            // fmt.Println("Point has not been added", point)
            clusters = append(clusters, make(Event, 0, 50))
            clusters[len(clusters)-1] = append(clusters[len(clusters)-1], point)
        }
    }
    // if len(clusters) > 13 {
    //     for _, c := range clusters {
    //         fmt.Println(c)
    //     } 
    // }
    return clusters
}



func main() {
    var fname *string = flag.String("filename", "", "name of the txt file to process")
    flag.Parse()
    fmt.Println("Opening", *fname)
    f, err := os.Open(*fname)
    if err != nil {
        fmt.Println(err)
        return
    }
    fileReader := bufio.NewReader(f)
    isInEvent := false
    events := make(map[int]Event)
    for { 
        line, isPrefix, err := fileReader.ReadLine()
        if (err != nil || isPrefix) {
            break
        }
        s := string(line)
        fields := strings.Fields(s)
        if (!isInEvent && len(fields) == 4) {
            isInEvent = true
        }
        if (len(fields) != 4) {
            fmt.Println(s)
            if (isInEvent) {
                isInEvent = false
                fmt.Println(len(events))
                events = make(map[int]Event)
            }
            continue
        }
        // break upon encountering a bad line in a file
        addPoint(fields, events)
        //fmt.Println(p)
        //if p == nil {
        //    fmt.Println("Error! Could not add point")
        //    break
       // }
    }
    f.Close()
    fmt.Println("Events read in")
    // fmt.Println(len(events), "events found")
    hist := histogram.NewHist1D(30, 200, 50)
    var averageLength float64
    for iEvent := range events {
        averageLength += float64(len(events[iEvent]))
        clusterList := clusterEventHits(events[iEvent])
        var nHitsInClusters float64
        for _, c := range clusterList {
            if len(c) > 3 {
                nHitsInClusters += float64(len(c))
            }
        }
        hist.Fill(nHitsInClusters)
        // fmt.Printf("%d\t", clusterLength)
    }
    hist.ToFile(filepath.Base(*fname)+".html")
    // fmt.Println(events[7531847])
    // clusterEventHits(events[7531847])
    // fmt.Println(events[7531847])
    if *memprof != "" {
        memfile, err := os.Create(*memprof)
        if err != nil {
            fmt.Println("Error! Could not create memfile")
        }
        pprof.WriteHeapProfile(memfile)
        memfile.Close()
    }

    fmt.Println("Got to the end", averageLength / float64(len(events)))
}
